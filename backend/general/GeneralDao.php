<?php

require_once(Config::PATH . Config::BACKEND_LIB . 'Sql.php');
require_once(Config::PATH . Config::BACKEND_LIB . 'DataAccess.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralVo.php');

class GeneralDao
{

    public $data = array();
    public $dataAccess;
    public $result;

    public function Connection()
    {

        $this->dataAccess = DataAccess::GetInstance(Config::HOST, Config::USER, Config::PASS, Config::BD);
        //$this->dataAccess = new DataAccess(Config::HOST, Config::USER, Config::PASS, Config::BD);
    }

    public function Set($generalVo)
    {
        $this->Connection();
        for ($i = 1; $i < count($generalVo->fieldsForDaoArray); $i++) {
            $data[$generalVo->fieldsForDaoArray[$i]] = $generalVo->{$generalVo->namesFieldsArray[$i]};
        }

        $sql = new Sql();
        $query = $sql->insert("$generalVo->nameTable ", $data);
        try {
            //print_r($query."<br>");
            $this->dataAccess->query($query);
            return $this->dataAccess->insert_id();
        } catch (Exception $exception) {
           if($_SERVER["HTTP_HOST"] == "localhost"){
               print_r("<div class='alert alert-danger text-center'>".$exception."</div>");
            }else{
              print_r("<script>MessageSwalBasic('Se Ha Presentado Un Error','ractifique la información insertada o ejecutada y/o contacte al proveedor del servicio','error') ;</script>");
            }
        }
    }

    public function Update($generalVo)
    {
        $this->Connection();
        for ($i = 1; $i < count($generalVo->fieldsForDaoArray); $i++) {
            $data[$generalVo->fieldsForDaoArray[$i]] = $generalVo->{$generalVo->namesFieldsArray[$i]};
        }

        $sql = new Sql();
        $query = $sql->update("$generalVo->nameTable ", $data, "$generalVo->nameTable.id", $generalVo->{$generalVo->namesFieldsArray[0]});

        try {
            $this->dataAccess->query($query);
            return $this->dataAccess->insert_id();
        } catch (Exception $exception) {
            print_r($exception);
        }
    }

    public function UpdateByField($generalVo)
    {
        $this->Connection();
        $query = "UPDATE $generalVo->nameTable ";
        if ($generalVo->typeFieldsArray[$generalVo->idUpdateField] == "VARCHAR" ||
            $generalVo->typeFieldsArray[$generalVo->idUpdateField] == "DATE" ||
            $generalVo->typeFieldsArray[$generalVo->idUpdateField] == "DATETIME"
        ) {
            $query .= " SET " . $generalVo->namesFieldsArray[$generalVo->idUpdateField] . " = '" . $generalVo->{$generalVo->namesFieldsArray[$generalVo->idUpdateField]} . "'";
        } else if ($generalVo->typeFieldsArray[$generalVo->idSearchField] == "INT") {
            $query .= " SET " . $generalVo->namesFieldsArray[$generalVo->idUpdateField] . " = " . $generalVo->{$generalVo->namesFieldsArray[$generalVo->idUpdateField]} . "";
        }

        if ($generalVo->isList == false) {
            if ($generalVo->typeFieldsArray[$generalVo->idSearchField] == "VARCHAR" ||
                $generalVo->typeFieldsArray[$generalVo->idSearchField] == "DATE" ||
                $generalVo->typeFieldsArray[$generalVo->idSearchField] == "DATETIME"
            ) {
                $query .= " WHERE $generalVo->nameTable.{$generalVo->namesFieldsArray[$generalVo->idSearchField]} = '" . $generalVo->{$generalVo->namesFieldsArray[$generalVo->idSearchField]} . "'";
            } else if ($generalVo->typeFieldsArray[$generalVo->idSearchField] == "INT") {
                $query .= " WHERE $generalVo->nameTable.{$generalVo->namesFieldsArray[$generalVo->idSearchField]} = " . $generalVo->{$generalVo->namesFieldsArray[$generalVo->idSearchField]};
            }
        }

        try {
            print_r($query);
            $this->result = $this->dataAccess->query($query);
        } catch (Exception $exception) {
            print_r($exception);
        }
    }

    public function GetById($generalVo)
    {
        $this->Connection();
        $query = " SELECT ";
        for ($i = 0; $i < count($generalVo->namesFieldsArray); $i++) {
            if ($i == count($generalVo->namesFieldsArray) - 1) {
                $query .= $generalVo->nameTable . "." . $generalVo->namesFieldsArray[$i];
            } else {
                $query .= $generalVo->nameTable . "." . $generalVo->namesFieldsArray[$i] . ", ";
            }
        }
        $query .= "
        FROM
        $generalVo->nameTable";

        if ($generalVo->isList == false) {
            $query .= " WHERE $generalVo->nameTable.{$generalVo->namesFieldsArray[0]} = " . $generalVo->id;
        }
      
        try {
            $this->result = $this->dataAccess->query($query);
            
        } catch (Exception $exception) {
            print_r($exception);
        }
        
     
    }

    public function GetByField($generalVo)
    {
        $this->Connection();
        $query = "SELECT ";
        for ($i = 0; $i < count($generalVo->namesFieldsArray); $i++) {
            if ($i == count($generalVo->namesFieldsArray) - 1) {
                $query .= $generalVo->nameTable . "." . $generalVo->namesFieldsArray[$i];
            } else {
                $query .= $generalVo->nameTable . "." . $generalVo->namesFieldsArray[$i] . ", ";
            }
        }
        $query .= "
                  FROM
                  $generalVo->nameTable";

        if ($generalVo->isList == false) {

            if ($generalVo->typeFieldsArray[$generalVo->idSearchField] == "VARCHAR" ||
                $generalVo->typeFieldsArray[$generalVo->idSearchField] == "DATE" ||
                $generalVo->typeFieldsArray[$generalVo->idSearchField] == "DATETIME"
            ) {
                $query .= " WHERE $generalVo->nameTable.{$generalVo->namesFieldsArray[$generalVo->idSearchField]} = '" . $generalVo->{$generalVo->namesFieldsArray[$generalVo->idSearchField]} . "'";
            } else if ($generalVo->typeFieldsArray[$generalVo->idSearchField] == "INT") {
                $query .= " WHERE $generalVo->nameTable.{$generalVo->namesFieldsArray[$generalVo->idSearchField]} = " . $generalVo->{$generalVo->namesFieldsArray[$generalVo->idSearchField]};
            }
        }

        try {
           // print_r($query);
            $this->result = $this->dataAccess->query($query);
        } catch (Exception $exception) {
            print_r($exception);
        }
    }

    public function CustomQuery($query)
    {
        $this->Connection();

        try {
            $this->result = $this->dataAccess->query($query);
        } catch (Exception $exception) {
            print_r($exception);
        }
    }

    public function Delete($generalVo)
    {
        $this->Connection();
        $query = "
        DELETE FROM
        $generalVo->nameTable
        WHERE $generalVo->nameTable.{$generalVo->namesFieldsArray[0]} = " . $generalVo->{$generalVo->namesFieldsArray[0]};

        try {
          
            $this->result = $this->dataAccess->query($query);

            return true;
        } catch (Exception $exception) {
          
            return false;
        }
    }

    public function DeleteByField($generalVo)
    {
        $this->Connection();
        $query = "
        DELETE FROM
        $generalVo->nameTable";

        if ($generalVo->isList == false) {
            if ($generalVo->typeFieldsArray[$generalVo->idSearchField] == "VARCHAR" ||
                $generalVo->typeFieldsArray[$generalVo->idSearchField] == "DATE" ||
                $generalVo->typeFieldsArray[$generalVo->idSearchField] == "DATETIME"
            ) {
                $query .= " WHERE $generalVo->nameTable.{$generalVo->namesFieldsArray[$generalVo->idSearchField]} = '" . $generalVo->{$generalVo->namesFieldsArray[$generalVo->idSearchField]} . "'";
            } else if ($generalVo->typeFieldsArray[$generalVo->idSearchField] == "INT") {
                $query .= " WHERE $generalVo->nameTable.{$generalVo->namesFieldsArray[$generalVo->idSearchField]} = " . $generalVo->{$generalVo->namesFieldsArray[$generalVo->idSearchField]};
            }
        }

        try {
            $this->result = $this->dataAccess->query($query);
            return true;
        } catch (Exception $exception) {
            print_r($exception);
            return false;
        }
    }

    /**
     * @param $generalVo
     * @return bool
     */
    public function GetVo($generalVo)
    {
        $this->Connection();
        $object = $this->dataAccess->fetch_object($this->result);
        while ($object) {
            $generalVo2 = clone $generalVo;
            for ($i = 0; $i < count($generalVo->namesFieldsArray); $i++) {
                $generalVo2->{$generalVo->namesFieldsArray[$i]} = $object->{$generalVo->namesFieldsArray[$i]};
            }
            //$this->dataAccess->CloseConnection();
            return $generalVo2;
            $this->dataAccess->CloseConnection();
        }
        //$this->dataAccess->CloseConnection();
        return false;
        
    }


    public function GetLength(){
        $this->Connection();
        $data = $this->dataAccess->num_rows($this->result);
        $this->dataAccess->CloseConnection();
        return $data;

    }

}
