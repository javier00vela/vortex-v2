<?php

class GeneralVo
{

  public $nameTable;
  public $isList;
  public $idSearchField;
  public $idUpdateField;
  public $namesFieldsArray;
  public $typeFieldsArray;
  public $fieldsForDaoArray;

  public function GetNameField($index) {
    return $this->namesFieldsArray[$index];
  }

  public function GetTypeField($index) {
    return $this->typeFieldsArray[$index];
  }

}
