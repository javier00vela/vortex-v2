<?php

require_once (Config::PATH . Config::BACKEND . 'general/GeneralVo.php');
class  ElementsMainteinceVo  extends GeneralVo {

    public $id;
    public $idProduct;
    public $typeProduct;
    public $name;
    public $brand;
    public $reference;
    public $presentation;
    public $amount;
    public $minimumAmount;
    public $typeCurrency;
    public $priceUnit;
    public $country;
    public $nameCompany;
    public $description;
    public $percent;
    public $quantity;
    public $idQuotation;
    public $timeDelivery;
    public $namePlace;
    public $IVA;
    public $nodo;
    public $FLETE;
    public $isFromCompany;

    public function __construct() {
        $this->id = null;
        $this->idProduct = null;
        $this->typeProduct = null;
        $this->name = null;
        $this->brand = null;
        $this->reference = null;
        $this->presentation = null;
        $this->amount = null;
        $this->minimumAmount = null;
        $this->typeCurrency = null;
        $this->priceUnit = null;
        $this->priceUnitIndividual = null;
        $this->country = null;
        $this->nameCompany = null;
        $this->description = null;
        $this->percent = null;
        $this->quantity = null;
        $this->idQuotation = null;
        $this->timeDelivery = null;
        $this->namePlace = null;
        $this->IVA = null;
        $this->nodo = null;
        $this->FLETE = null;
        $this->isFromCompany = null;

        $this->isList = false;
        $this->nameTable = "elementsmainteince";
        $this->SetNamesFieldsToList();
    }

    private function SetNamesFieldsToList() {
        $this->namesFieldsArray = array();
        $this->namesFieldsArray[0] = "id";
        $this->namesFieldsArray[1] = "idProduct";
        $this->namesFieldsArray[2] = "typeProduct";
        $this->namesFieldsArray[3] = "name";
        $this->namesFieldsArray[4] = "brand";
        $this->namesFieldsArray[5] = "reference";
        $this->namesFieldsArray[6] = "presentation";
        $this->namesFieldsArray[7] = "amount";
        $this->namesFieldsArray[8] = "minimumAmount";
        $this->namesFieldsArray[9] = "typeCurrency";
        $this->namesFieldsArray[10] = "priceUnit";
        $this->namesFieldsArray[11] = "country";
        $this->namesFieldsArray[12] = "nameCompany";
        $this->namesFieldsArray[13] = "description";
        $this->namesFieldsArray[14] = "percent";
        $this->namesFieldsArray[15] = "quantity";
        $this->namesFieldsArray[16] = "idQuotation";
        $this->namesFieldsArray[17] = "timeDelivery";
        $this->namesFieldsArray[18] = "namePlace";
        $this->namesFieldsArray[19] = "priceUnitIndividual";
        $this->namesFieldsArray[20] = "IVA";
        $this->namesFieldsArray[21] = "FLETE";
        $this->namesFieldsArray[22] = "nodo";
        $this->namesFieldsArray[23] = "isFromCompany";

        $this->typeFieldsArray = array();
        $this->typeFieldsArray[0] = "INT";
        $this->typeFieldsArray[1] = "INT";
        $this->typeFieldsArray[2] = "VARCHAR";
        $this->typeFieldsArray[3] = "VARCHAR";
        $this->typeFieldsArray[4] = "VARCHAR";
        $this->typeFieldsArray[5] = "VARCHAR";
        $this->typeFieldsArray[6] = "VARCHAR";
        $this->typeFieldsArray[7] = "INT";
        $this->typeFieldsArray[8] = "INT";
        $this->typeFieldsArray[9] = "VARCHAR";
        $this->typeFieldsArray[10] = "VARCHAR";
        $this->typeFieldsArray[11] = "VARCHAR";
        $this->typeFieldsArray[12] = "VARCHAR";
        $this->typeFieldsArray[13] = "VARCHAR";
        $this->typeFieldsArray[14] = "FLOAT";
        $this->typeFieldsArray[15] = "INT";
        $this->typeFieldsArray[16] = "INT";
        $this->typeFieldsArray[17] = "VARCHAR";
        $this->typeFieldsArray[18] = "VARCHAR";
        $this->typeFieldsArray[19] = "VARCHAR";
        $this->typeFieldsArray[20] = "INT";
        $this->typeFieldsArray[21] = "VARCHAR";
        $this->typeFieldsArray[22] = "VARCHAR";
        $this->typeFieldsArray[23] = "INT";


        $this->SetFieldsForDaoArray ();
    }

    private function SetFieldsForDaoArray () {
        $this->fieldsForDaoArray = array();
        for ($i=0; $i < count($this->namesFieldsArray); $i++) {
            if($this->typeFieldsArray[$i] == "VARCHAR" || $this->typeFieldsArray[$i] == "DATE" || $this->typeFieldsArray[$i] == "DATETIME"){
                $this->fieldsForDaoArray[$i] = "\"".$this->namesFieldsArray[$i];
            }else {
                $this->fieldsForDaoArray[$i] = $this->namesFieldsArray[$i];
            }
        }
    }
  

}
