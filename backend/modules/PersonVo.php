<?php
require_once (Config::PATH . Config::BACKEND . 'general/GeneralVo.php');
class  PersonVo  extends GeneralVo {

    public $id;
    public $names;
    public $lastNames;
    public $documentType;
    public $document;
    public $cellPhone;
    public $telephone;
    public $email;
    public $address;
    public $city;
    public $creationDate;
    public $idRole;
    public $idCompany;
     public $state;

    public function __construct() {
        $this->id = null;
        $this->names = null;
        $this->lastNames = null;
        $this->documentType = null;
        $this->document = null;
        $this->cellPhone = null;
        $this->telephone = null;
        $this->email = null;
        $this->address = null;
        $this->city = null;
        $this->creationDate = null;
        $this->idRole = null;
        $this->idCompany = null;
        $this->state = null;

        $this->isList = false;
        $this->nameTable = "person";
        $this->SetNamesFieldsToList();
    }

    private function SetNamesFieldsToList() {
        $this->namesFieldsArray = array();
        $this->namesFieldsArray[0] = "id";
        $this->namesFieldsArray[1] = "names";
        $this->namesFieldsArray[2] = "lastNames";
        $this->namesFieldsArray[3] = "documentType";
        $this->namesFieldsArray[4] = "document";
        $this->namesFieldsArray[5] = "cellPhone";
        $this->namesFieldsArray[6] = "telephone";
        $this->namesFieldsArray[7] = "email";
        $this->namesFieldsArray[8] = "address";
        $this->namesFieldsArray[9] = "city";
        $this->namesFieldsArray[10] = "creationDate";
        $this->namesFieldsArray[11] = "idRole";
        $this->namesFieldsArray[12] = "idCompany";
        $this->namesFieldsArray[13] = "state";

        $this->typeFieldsArray = array();
        $this->typeFieldsArray[0] = "INT";
        $this->typeFieldsArray[1] = "VARCHAR";
        $this->typeFieldsArray[2] = "VARCHAR";
        $this->typeFieldsArray[3] = "VARCHAR";
        $this->typeFieldsArray[4] = "VARCHAR";
        $this->typeFieldsArray[5] = "VARCHAR";
        $this->typeFieldsArray[6] = "VARCHAR";
        $this->typeFieldsArray[7] = "VARCHAR";
        $this->typeFieldsArray[8] = "VARCHAR";
        $this->typeFieldsArray[9] = "VARCHAR";
        $this->typeFieldsArray[10] = "DATETIME";
        $this->typeFieldsArray[11] = "INT";
        $this->typeFieldsArray[12] = "INT";
        $this->typeFieldsArray[13] = "INT";

        $this->SetFieldsForDaoArray ();
    }

    private function SetFieldsForDaoArray () {
        $this->fieldsForDaoArray = array();
        for ($i=0; $i < count($this->namesFieldsArray); $i++) {
            if($this->typeFieldsArray[$i] == "VARCHAR" || $this->typeFieldsArray[$i] == "DATE" || $this->typeFieldsArray[$i] == "DATETIME"){
                $this->fieldsForDaoArray[$i] = "\"".$this->namesFieldsArray[$i];
            }else {
                $this->fieldsForDaoArray[$i] = $this->namesFieldsArray[$i];
            }
        }
    }
}
