<?php

require_once (Config::PATH . Config::BACKEND . 'general/GeneralVo.php');
class  PercentCodesVo  extends GeneralVo {

    public $id;
    public $code;
    public $idPerson;
    public $dateCreate;

    public function __construct() {
        $this->id = null;
        $this->code = null;
        $this->idPerson = null;
        $this->dateCreate = null;


        $this->isList = false;
        $this->nameTable = "percentscodes";
        $this->SetNamesFieldsToList();
    }

    private function SetNamesFieldsToList() {
        $this->namesFieldsArray = array();
        $this->namesFieldsArray[0] = "id";
        $this->namesFieldsArray[1] = "code";
        $this->namesFieldsArray[2] = "idPerson";
        $this->namesFieldsArray[3] = "dateCreate";


        $this->typeFieldsArray = array();
        $this->typeFieldsArray[0] = "INT";
        $this->typeFieldsArray[1] = "VARCHAR";
        $this->typeFieldsArray[2] = "INT";
        $this->typeFieldsArray[3] = "DATETIME";



        $this->SetFieldsForDaoArray ();
    }

    private function SetFieldsForDaoArray () {
        $this->fieldsForDaoArray = array();
        for ($i=0; $i < count($this->namesFieldsArray); $i++) {
            if($this->typeFieldsArray[$i] == "VARCHAR" || $this->typeFieldsArray[$i] == "DATE" || $this->typeFieldsArray[$i] == "DATETIME"){
                $this->fieldsForDaoArray[$i] = "\"".$this->namesFieldsArray[$i];
            }else {
                $this->fieldsForDaoArray[$i] = $this->namesFieldsArray[$i];
            }
        }
    }
}
