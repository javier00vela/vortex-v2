<?php
require_once (Config::PATH . Config::BACKEND . 'general/GeneralVo.php');
class  RoleVo  extends GeneralVo {

    public $id;
    public $name;
    public $icon;
    public $color;

    public function __construct() {
        $this->id = null;
        $this->name = null;

        $this->isList = false;
        $this->nameTable = "role";
        $this->SetNamesFieldsToList();
    }

    private function SetNamesFieldsToList() {
        $this->namesFieldsArray = array();
        $this->namesFieldsArray[0] = "id";
        $this->namesFieldsArray[1] = "name";
         $this->namesFieldsArray[2] = "color";
        $this->namesFieldsArray[3] = "icon";

        $this->typeFieldsArray = array();
        $this->typeFieldsArray[0] = "INT";
        $this->typeFieldsArray[1] = "VARCHAR";
         $this->typeFieldsArray[2] = "VARCHAR";
          $this->typeFieldsArray[3] = "VARCHAR";

        $this->SetFieldsForDaoArray ();
    }

    private function SetFieldsForDaoArray () {
        $this->fieldsForDaoArray = array();
        for ($i=0; $i < count($this->namesFieldsArray); $i++) {
            if($this->typeFieldsArray[$i] == "VARCHAR" || $this->typeFieldsArray[$i] == "DATE" || $this->typeFieldsArray[$i] == "DATETIME"){
                $this->fieldsForDaoArray[$i] = "\"".$this->namesFieldsArray[$i];
            }else {
                $this->fieldsForDaoArray[$i] = $this->namesFieldsArray[$i];
            }
        }
    }
}
