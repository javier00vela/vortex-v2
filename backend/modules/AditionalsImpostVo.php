<?php

require_once (Config::PATH . Config::BACKEND . 'general/GeneralVo.php');
class  AditionalsImpostVo  extends GeneralVo {

    public $id;
    public $name;
    public $percent;
    public $idPricesCompany;
    public $CIF;
    public $FCA;
    public $DDP;

    public function __construct() {
        $this->id = null;
        $this->name = null;
        $this->percent = null;
        $this->idPricesCompany = null;
        $this->CIF = null;
        $this->FCA = null;
        $this->DDP = null;


        $this->isList = false;
        $this->nameTable = "aditionalsimposts";
        $this->SetNamesFieldsToList();
    }

    private function SetNamesFieldsToList() {
        $this->namesFieldsArray = array();
        $this->namesFieldsArray[0] = "id";
        $this->namesFieldsArray[1] = "name";
        $this->namesFieldsArray[2] = "percent";
        $this->namesFieldsArray[3] = "idPricesCompany";
        $this->namesFieldsArray[4] = "CIF";
        $this->namesFieldsArray[5] = "FCA";
        $this->namesFieldsArray[6] = "DDP";


        $this->typeFieldsArray = array();
        $this->typeFieldsArray[0] = "INT";
        $this->typeFieldsArray[1] = "VARCHAR";
        $this->typeFieldsArray[2] = "VARCHAR";
        $this->typeFieldsArray[3] = "INT";
        $this->typeFieldsArray[4] = "INT";
        $this->typeFieldsArray[5] = "INT";
        $this->typeFieldsArray[6] = "INT";

        $this->SetFieldsForDaoArray ();
    }

    private function SetFieldsForDaoArray () {
        $this->fieldsForDaoArray = array();
        for ($i=0; $i < count($this->namesFieldsArray); $i++) {
            if($this->typeFieldsArray[$i] == "VARCHAR" || $this->typeFieldsArray[$i] == "DATE" || $this->typeFieldsArray[$i] == "DATETIME"){
                $this->fieldsForDaoArray[$i] = "\"".$this->namesFieldsArray[$i];
            }else {
                $this->fieldsForDaoArray[$i] = $this->namesFieldsArray[$i];
            }
        }
    }
}
