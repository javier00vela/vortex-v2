<?php
require_once (Config::PATH . Config::BACKEND . 'general/GeneralVo.php');
class  AccessHistoryVo  extends GeneralVo {

    public $id;
    public $login;
    public $logout;
    public $numHours;
    public $ipAddress;
    public $device;
    public $browser;
    public $idUser;

    public function __construct() {
        $this->id = null;
        $this->login = null;
        $this->logout = null;
        $this->numHours = null;
        $this->ipAddress = null;
        $this->device = null;
        $this->browser = null;
        $this->idUser = null;

        $this->isList = false;
        $this->nameTable = "accesshistory";
        $this->SetNamesFieldsToList();
    }

    private function SetNamesFieldsToList() {
        $this->namesFieldsArray = array();
        $this->namesFieldsArray[0] = "id";
        $this->namesFieldsArray[1] = "login";
        $this->namesFieldsArray[2] = "logout";
        $this->namesFieldsArray[3] = "numHours";
        $this->namesFieldsArray[4] = "ipAddress";
        $this->namesFieldsArray[5] = "device";
        $this->namesFieldsArray[6] = "browser";
        $this->namesFieldsArray[7] = "idUser";

        $this->typeFieldsArray = array();
        $this->typeFieldsArray[0] = "INT";
        $this->typeFieldsArray[1] = "DATETIME";
        $this->typeFieldsArray[2] = "DATETIME";
        $this->typeFieldsArray[3] = "VARCHAR";
        $this->typeFieldsArray[4] = "VARCHAR";
        $this->typeFieldsArray[5] = "VARCHAR";
        $this->typeFieldsArray[6] = "VARCHAR";
        $this->typeFieldsArray[7] = "INT";

        $this->SetFieldsForDaoArray ();
    }

    private function SetFieldsForDaoArray () {
        $this->fieldsForDaoArray = array();
        for ($i=0; $i < count($this->namesFieldsArray); $i++) {
            if($this->typeFieldsArray[$i] == "VARCHAR" || $this->typeFieldsArray[$i] == "DATE" || $this->typeFieldsArray[$i] == "DATETIME"){
                $this->fieldsForDaoArray[$i] = "\"".$this->namesFieldsArray[$i];
            }else {
                $this->fieldsForDaoArray[$i] = $this->namesFieldsArray[$i];
            }
        }
    }
}
