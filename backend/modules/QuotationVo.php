<?php

require_once (Config::PATH . Config::BACKEND . 'general/GeneralVo.php');
class  QuotationVo  extends GeneralVo {

    public $id;
    public $code;
    public $typeMoney;
    public $generationDate;
    public $state;
    public $percent;
    public $total;
    public $idUser;
    public $idCompany;
    //------------ new items ---------
    public $offerValidity;
    public $placeDelivery;
    public $payment;
    public $typeQuotation;
    public $version;
    public $IVA;
    public $isDDP;
    public $checkeable;
    public $dateUpdate;

    //----------recodatorio de offerValidity
    public $reminder;
    public $aplicateIVA;

    public function __construct() {
        $this->id = null;
        $this->code = null;
        $this->typeMoney = null;
        $this->generationDate = null;
        $this->state = null;
        $this->total = null;
        $this->idUser = null;
        $this->idCompany = null;

        $this->offerValidity = null;
        $this->placeDelivery = null;
        $this->payment = null;
        $this->typeQuotation = null;
        $this->version = null;
        $this->IVA = null;
        $this->reminder = null;
        $this->checkeable = null;

        $this->isList = false;
        $this->nameTable = "quotation";
        $this->SetNamesFieldsToList();
    }

    private function SetNamesFieldsToList() {
        $this->namesFieldsArray = array();
        $this->namesFieldsArray[0] = "id";
        $this->namesFieldsArray[1] = "code";
        $this->namesFieldsArray[2] = "typeMoney";
        $this->namesFieldsArray[3] = "generationDate";
        $this->namesFieldsArray[4] = "state";
        $this->namesFieldsArray[5] = "total";
        $this->namesFieldsArray[6] = "idUser";
        $this->namesFieldsArray[7] = "idCompany";
        $this->namesFieldsArray[8] = "offerValidity";
        $this->namesFieldsArray[9] = "placeDelivery";
        $this->namesFieldsArray[10] = "payment";
        $this->namesFieldsArray[11] = "typeQuotation";
        $this->namesFieldsArray[12] = "version";
        $this->namesFieldsArray[13] = "IVA";
        $this->namesFieldsArray[14] = "reminder";
        $this->namesFieldsArray[15] = "isDDP";
        $this->namesFieldsArray[16] = "checkeable";
        $this->namesFieldsArray[17] = "dateUpdate";

        $this->typeFieldsArray = array();
        $this->typeFieldsArray[0] = "INT";
        $this->typeFieldsArray[1] = "INT";
        $this->typeFieldsArray[2] = "VARCHAR";
        $this->typeFieldsArray[3] = "DATETIME";
        $this->typeFieldsArray[4] = "VARCHAR";
        $this->typeFieldsArray[5] = "DOUBLE";
        $this->typeFieldsArray[6] = "INT";
        $this->typeFieldsArray[7] = "INT";
        $this->typeFieldsArray[8] = "VARCHAR";
        $this->typeFieldsArray[9] = "VARCHAR";
        $this->typeFieldsArray[10] = "VARCHAR";
        $this->typeFieldsArray[11] = "VARCHAR";
        $this->typeFieldsArray[12] = "INT";
        $this->typeFieldsArray[13] = "INT";
        $this->typeFieldsArray[14] = "INT";
        $this->typeFieldsArray[15] = "INT";
        $this->typeFieldsArray[16] = "INT";
        $this->typeFieldsArray[17] = "DATE";
        
        $this->SetFieldsForDaoArray ();
    }

    private function SetFieldsForDaoArray () {
        $this->fieldsForDaoArray = array();
        for ($i=0; $i < count($this->namesFieldsArray); $i++) {
            if($this->typeFieldsArray[$i] == "VARCHAR" || $this->typeFieldsArray[$i] == "DATE" || $this->typeFieldsArray[$i] == "DATETIME"){
                $this->fieldsForDaoArray[$i] = "\"".$this->namesFieldsArray[$i];
            }else {
                $this->fieldsForDaoArray[$i] = $this->namesFieldsArray[$i];
            }
        }
    }
}
