<?php

require_once (Config::PATH . Config::BACKEND . 'general/GeneralVo.php');
class  HistoryTRMVo  extends GeneralVo {
    public $id;
    public $typeMoney;
    public $money;
    public $date;
    public $generation;
    public $mailSend;

    public function __construct() {
        $this->id = null;
        $this->typeMoney = null;
        $this->money = null;
        $this->date = null;
        $this->generation = null;
        $this->mailSend = null;

        $this->isList = false;
        $this->nameTable = "historytrm";
        $this->SetNamesFieldsToList();
    }

    private function SetNamesFieldsToList() {
        $this->namesFieldsArray = array();
        $this->namesFieldsArray[0] = "id";
        $this->namesFieldsArray[1] = "typeMoney";
        $this->namesFieldsArray[2] = "money";
        $this->namesFieldsArray[3] = "date";
        $this->namesFieldsArray[4] = "generation";
        $this->namesFieldsArray[5] = "mailSend";
 

        $this->typeFieldsArray = array();
        $this->typeFieldsArray[0] = "INT";
        $this->typeFieldsArray[1] = "VARCHAR";
        $this->typeFieldsArray[2] = "VARCHAR";
        $this->typeFieldsArray[3] = "DATE";
        $this->typeFieldsArray[4] = "DATE";
        $this->typeFieldsArray[5] = "INT";
       
        $this->SetFieldsForDaoArray ();
    }

    private function SetFieldsForDaoArray () {
        $this->fieldsForDaoArray = array();
        for ($i=0; $i < count($this->namesFieldsArray); $i++) {
            if($this->typeFieldsArray[$i] == "VARCHAR" || $this->typeFieldsArray[$i] == "DATE" || $this->typeFieldsArray[$i] == "DATETIME"){
                $this->fieldsForDaoArray[$i] = "\"".$this->namesFieldsArray[$i];
            }else {
                $this->fieldsForDaoArray[$i] = $this->namesFieldsArray[$i];
            }
        }
    }
}
