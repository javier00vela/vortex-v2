<?php

require_once (Config::PATH . Config::BACKEND . 'general/GeneralVo.php');
class  ModuleEventVo  extends GeneralVo {

    public $id;
    public $idModule;
    public $idEvent;
    public $idRol;
    public $state;
    public $typeNotification;
    public $numberNotification;

    public function __construct() {
        $this->id = null;
        $this->idModule = null;
        $this->idEvent = null;
        $this->idRol = null;
        $this->state = null;
        $this->typeNotification = null;
        $this->numberNotification = null;
        

        $this->isList = false;
        $this->nameTable = "moduleevent";
        $this->SetNamesFieldsToList();
    }

    private function SetNamesFieldsToList() {
        $this->namesFieldsArray = array();
        $this->namesFieldsArray[0] = "id";
        $this->namesFieldsArray[1] = "idModule";
        $this->namesFieldsArray[2] = "idEvent";
        $this->namesFieldsArray[3] = "state";
        $this->namesFieldsArray[4] = "idRol";
        $this->namesFieldsArray[5] = "typeNotification";
        $this->namesFieldsArray[6] = "numberNotification";

        $this->typeFieldsArray = array();
        $this->typeFieldsArray[0] = "INT";
        $this->typeFieldsArray[1] = "INT";
        $this->typeFieldsArray[2] = "INT";
        $this->typeFieldsArray[3] = "INT";
        $this->typeFieldsArray[4] = "INT";
        $this->typeFieldsArray[5] = "VARCHAR";
        $this->typeFieldsArray[6] = "INT";

        $this->SetFieldsForDaoArray ();
    }

    private function SetFieldsForDaoArray () {
        $this->fieldsForDaoArray = array();
        for ($i=0; $i < count($this->namesFieldsArray); $i++) {
            if($this->typeFieldsArray[$i] == "VARCHAR" || $this->typeFieldsArray[$i] == "DATE" || $this->typeFieldsArray[$i] == "DATETIME"){
                $this->fieldsForDaoArray[$i] = "\"".$this->namesFieldsArray[$i];
            }else {
                $this->fieldsForDaoArray[$i] = $this->namesFieldsArray[$i];
            }
        }
    }
}
