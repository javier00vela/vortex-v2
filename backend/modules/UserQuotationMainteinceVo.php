<?php

require_once (Config::PATH . Config::BACKEND . 'general/GeneralVo.php');
class  UserQuotationMainteinceVo  extends GeneralVo {

    public $id;
    public $idPerson;
    public $completeName;
    public $idQuotation;
    public $idCompany;
    

    public function __construct() {
        $this->id = null;
        $this->idQuotation = null;
        $this->completeName = null;
        $this->idPerson = null;
        $this->idCompany = null;

        $this->isList = false;
        $this->nameTable = "usersquotationmainteince";
        $this->SetNamesFieldsToList();
    }

    private function SetNamesFieldsToList() {
        $this->namesFieldsArray = array();
        $this->namesFieldsArray[0] = "id";
        $this->namesFieldsArray[1] = "idPerson";
        $this->namesFieldsArray[2] = "completeName";
        $this->namesFieldsArray[3] = "idQuotation";
        

        $this->typeFieldsArray = array();
        $this->typeFieldsArray[0] = "INT";
        $this->typeFieldsArray[1] = "INT";
        $this->typeFieldsArray[2] = "VARCHAR";
        $this->typeFieldsArray[3] = "INT";
        
        $this->SetFieldsForDaoArray ();
    }

    private function SetFieldsForDaoArray () {
        $this->fieldsForDaoArray = array();
        for ($i=0; $i < count($this->namesFieldsArray); $i++) {
            if($this->typeFieldsArray[$i] == "VARCHAR" || $this->typeFieldsArray[$i] == "DATE" || $this->typeFieldsArray[$i] == "DATETIME"){
                $this->fieldsForDaoArray[$i] = "\"".$this->namesFieldsArray[$i];
            }else {
                $this->fieldsForDaoArray[$i] = $this->namesFieldsArray[$i];
            }
        }
    }
}
