<?php

require_once (Config::PATH . Config::BACKEND . 'general/GeneralVo.php');
class  AlertsVo  extends GeneralVo {

    public $id;
    public $tittle;
    public $description;
    public $idEvent;

    public function __construct() {
        $this->id = null;
        $this->tittle = null;
        $this->description = null;
        $this->idEvent = null;

        $this->isList = false;
        $this->nameTable = "alert";
        $this->SetNamesFieldsToList();
    }

    private function SetNamesFieldsToList() {
        $this->namesFieldsArray = array();
        $this->namesFieldsArray[0] = "id";
        $this->namesFieldsArray[1] = "tittle";
        $this->namesFieldsArray[2] = "description";
        $this->namesFieldsArray[3] = "idEvent";

        $this->typeFieldsArray = array();
        $this->typeFieldsArray[0] = "INT";
        $this->typeFieldsArray[1] = "VARCHAR";
        $this->typeFieldsArray[2] = "VARCHAR";
        $this->typeFieldsArray[3] = "INT";

        $this->SetFieldsForDaoArray ();
    }

    private function SetFieldsForDaoArray () {
        $this->fieldsForDaoArray = array();
        for ($i=0; $i < count($this->namesFieldsArray); $i++) {
            if($this->typeFieldsArray[$i] == "VARCHAR" || $this->typeFieldsArray[$i] == "DATE" || $this->typeFieldsArray[$i] == "DATETIME"){
                $this->fieldsForDaoArray[$i] = "\"".$this->namesFieldsArray[$i];
            }else {
                $this->fieldsForDaoArray[$i] = $this->namesFieldsArray[$i];
            }
        }
    }
}
