<?php

require_once (Config::PATH . Config::BACKEND . 'general/GeneralVo.php');
class  HistoryModificationMainteinceVo  extends GeneralVo {

    public $id;
    public $idUser;
    public $modificationDate;
    public $idQuotation;
     public $description;

    public function __construct() {
        $this->id = null;
        $this->idUser = null;
        $this->modificationDate = null;
        $this->idQuotation = null;
        $this->description = null;

        $this->isList = false;
        $this->nameTable = "historymodificationmainteince";
        $this->SetNamesFieldsToList();
    }

    private function SetNamesFieldsToList() {
        $this->namesFieldsArray = array();
        $this->namesFieldsArray[0] = "id";
        $this->namesFieldsArray[1] = "idUser";
        $this->namesFieldsArray[2] = "modificationDate";
        $this->namesFieldsArray[3] = "idQuotation";
          $this->namesFieldsArray[4] = "description";

        $this->typeFieldsArray = array();
        $this->typeFieldsArray[0] = "INT";
        $this->typeFieldsArray[1] = "INT";
        $this->typeFieldsArray[2] = "DATETIME";
        $this->typeFieldsArray[3] = "INT";
        $this->typeFieldsArray[4] = "VARCHAR";

        $this->SetFieldsForDaoArray ();
    }

    private function SetFieldsForDaoArray () {
        $this->fieldsForDaoArray = array();
        for ($i=0; $i < count($this->namesFieldsArray); $i++) {
            if($this->typeFieldsArray[$i] == "VARCHAR" || $this->typeFieldsArray[$i] == "DATE" || $this->typeFieldsArray[$i] == "DATETIME"){
                $this->fieldsForDaoArray[$i] = "\"".$this->namesFieldsArray[$i];
            }else {
                $this->fieldsForDaoArray[$i] = $this->namesFieldsArray[$i];
            }
        }
    }
}
