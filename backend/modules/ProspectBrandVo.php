<?php

require_once (Config::PATH . Config::BACKEND . 'general/GeneralVo.php');
class  ProspectBrandVo  extends GeneralVo {

    public $id;
    public $idPerson;
    public $nameBrand;
    public $description;

    public function __construct() {
        $this->id = null;
        $this->idPerson = null;
        $this->nameBrand = null;
        $this->description = null;

        $this->isList = false;
        $this->nameTable = "prospectbrand";
        $this->SetNamesFieldsToList();
    }

    private function SetNamesFieldsToList() {
        $this->namesFieldsArray = array();
        $this->namesFieldsArray[0] = "id";
        $this->namesFieldsArray[1] = "idPerson";
        $this->namesFieldsArray[2] = "nameBrand";
        $this->namesFieldsArray[3] = "description";

        $this->typeFieldsArray = array();
        $this->typeFieldsArray[0] = "INT";
        $this->typeFieldsArray[1] = "INT";
        $this->typeFieldsArray[2] = "VARCHAR";
        $this->typeFieldsArray[3] = "VARCHAR";

        $this->SetFieldsForDaoArray ();
    }

    private function SetFieldsForDaoArray () {
        $this->fieldsForDaoArray = array();
        for ($i=0; $i < count($this->namesFieldsArray); $i++) {
            if($this->typeFieldsArray[$i] == "VARCHAR" || $this->typeFieldsArray[$i] == "DATE" || $this->typeFieldsArray[$i] == "DATETIME"){
                $this->fieldsForDaoArray[$i] = "\"".$this->namesFieldsArray[$i];
            }else {
                $this->fieldsForDaoArray[$i] = $this->namesFieldsArray[$i];
            }
        }
    }
}
