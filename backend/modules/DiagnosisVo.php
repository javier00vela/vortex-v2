<?php

require_once (Config::PATH . Config::BACKEND . 'general/GeneralVo.php');
class  DiagnosisVo  extends GeneralVo {

    public $id;
    public $commentClient;
    public $machineCommentState;
    public $machineObservation;
    public $firmClient;
    public $firmMaintenance;
    public $idProduct;
    public $idStatusMachine;
    public $dateCreated;
    public $noteGeneral;

    public function __construct() {
        $this->id = null;
        $this->commentClient = null;
        $this->machineCommentState = null;
        $this->firmClient = null;
        $this->firmMaintenance = null;
        $this->idProduct = null;
        $this->idStatusMachine = null;
        $this->machineObservation = null;
        $this->noteGeneral = null;
        $this->isList = false;
        $this->nameTable = "diagnosis";
        $this->SetNamesFieldsToList();
    }

    private function SetNamesFieldsToList() {
        $this->namesFieldsArray = array();
        $this->namesFieldsArray[0] = "id";
        $this->namesFieldsArray[1] = "commentClient";
        $this->namesFieldsArray[2] = "machineCommentState";
        $this->namesFieldsArray[3] = "firmClient";
        $this->namesFieldsArray[4] = "firmMaintenance";
        $this->namesFieldsArray[5] = "idProduct";
        $this->namesFieldsArray[6] = "dateCreated";
        $this->namesFieldsArray[7] = "machineObservation";
        $this->namesFieldsArray[8] = "idStatusMachine";
        $this->namesFieldsArray[9] = "noteGeneral";

        $this->typeFieldsArray = array();
        $this->typeFieldsArray[0] = "INT";
        $this->typeFieldsArray[1] = "VARCHAR";
        $this->typeFieldsArray[2] = "VARCHAR";
        $this->typeFieldsArray[3] = "VARCHAR";
        $this->typeFieldsArray[4] = "VARCHAR";
        $this->typeFieldsArray[5] = "INT";
        $this->typeFieldsArray[6] = "VARCHAR";
        $this->typeFieldsArray[7] = "VARCHAR";
        $this->typeFieldsArray[8] = "INT";
        $this->typeFieldsArray[9] = "VARCHAR";

        $this->SetFieldsForDaoArray ();
    }

    private function SetFieldsForDaoArray () {
        $this->fieldsForDaoArray = array();
        for ($i=0; $i < count($this->namesFieldsArray); $i++) {
            if($this->typeFieldsArray[$i] == "VARCHAR" || $this->typeFieldsArray[$i] == "DATE" || $this->typeFieldsArray[$i] == "DATETIME"){
                $this->fieldsForDaoArray[$i] = "\"".$this->namesFieldsArray[$i];
            }else {
                $this->fieldsForDaoArray[$i] = $this->namesFieldsArray[$i];
            }
        }
    }
}
