<?php

require_once (Config::PATH . Config::BACKEND . 'general/GeneralVo.php');
class  ModuleActionVo  extends GeneralVo {

    public $id;
    public $rol;
    public $module;
    public $action;
    public $state;

    public function __construct() {
        $this->id = null;
        $this->idRol = null;
        $this->idModule = null;
        $this->action = null;
        $this->state = null;


        $this->isList = false;
        $this->nameTable = "moduleaction";
        $this->SetNamesFieldsToList();
    }

    private function SetNamesFieldsToList() {
        $this->namesFieldsArray = array();
        $this->namesFieldsArray[0] = "id";
        $this->namesFieldsArray[1] = "idRol";
        $this->namesFieldsArray[2] = "idModule";
        $this->namesFieldsArray[3] = "state";
        $this->namesFieldsArray[4] = "action";


        $this->typeFieldsArray = array();
        $this->typeFieldsArray[0] = "INT";
        $this->typeFieldsArray[1] = "INT";
         $this->typeFieldsArray[2] ="INT";
        $this->typeFieldsArray[3] = "INT";
        $this->typeFieldsArray[4] = "VARCHAR";


        $this->SetFieldsForDaoArray ();
    }

    private function SetFieldsForDaoArray () {
        $this->fieldsForDaoArray = array();
        for ($i=0; $i < count($this->namesFieldsArray); $i++) {
            if($this->typeFieldsArray[$i] == "VARCHAR" || $this->typeFieldsArray[$i] == "DATE" || $this->typeFieldsArray[$i] == "DATETIME"){
                $this->fieldsForDaoArray[$i] = "\"".$this->namesFieldsArray[$i];
            }else {
                $this->fieldsForDaoArray[$i] = $this->namesFieldsArray[$i];
            }
        }
    }
}
