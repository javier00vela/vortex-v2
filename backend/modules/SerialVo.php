<?php

require_once (Config::PATH . Config::BACKEND . 'general/GeneralVo.php');
class  SerialVo  extends GeneralVo {

    public $id;
    public $numberSerial;
    public $numberRow;
    public $idInventary;

    public function __construct() {
        $this->id = null;
        $this->numberSerial = null;
        $this->numberRow = null;
        $this->idInventary = null;


        $this->isList = false;
        $this->nameTable = "serial";
        $this->SetNamesFieldsToList();
    }

    private function SetNamesFieldsToList() {
        $this->namesFieldsArray = array();
        $this->namesFieldsArray[0] = "id";
        $this->namesFieldsArray[1] = "numberSerial";
        $this->namesFieldsArray[2] = "numberRow";
        $this->namesFieldsArray[3] = "idInventary";


        $this->typeFieldsArray = array();
        $this->typeFieldsArray[0] = "INT";
        $this->typeFieldsArray[1] = "VARCHAR";
        $this->typeFieldsArray[2] = "INT";
        $this->typeFieldsArray[3] = "INT";


        $this->SetFieldsForDaoArray ();
    }

    private function SetFieldsForDaoArray () {
        $this->fieldsForDaoArray = array();
        for ($i=0; $i < count($this->namesFieldsArray); $i++) {
            if($this->typeFieldsArray[$i] == "VARCHAR" || $this->typeFieldsArray[$i] == "DATE" || $this->typeFieldsArray[$i] == "DATETIME"){
                $this->fieldsForDaoArray[$i] = "\"".$this->namesFieldsArray[$i];
            }else {
                $this->fieldsForDaoArray[$i] = $this->namesFieldsArray[$i];
            }
        }
    }
}
