<?php

require_once (Config::PATH . Config::BACKEND . 'general/GeneralVo.php');
class  LicenceVo  extends GeneralVo {

    public $id;
    public $nickname;
    public $password;
    public $state;
    public $photo;
    public $idPerson;

    public function __construct() {
        $this->id = null;
        $this->initDate = null;
        $this->endDate = null;

        $this->isList = false;
        $this->nameTable = "licence";
        $this->SetNamesFieldsToList();
    }

    private function SetNamesFieldsToList() {
        $this->namesFieldsArray = array();
        $this->namesFieldsArray[0] = "id";
        $this->namesFieldsArray[1] = "initDate";
        $this->namesFieldsArray[2] = "endDate";

        $this->typeFieldsArray = array();
        $this->typeFieldsArray[0] = "INT";
        $this->typeFieldsArray[1] = "DATE";
        $this->typeFieldsArray[2] = "DATE";

        $this->SetFieldsForDaoArray ();
    }

    private function SetFieldsForDaoArray () {
        $this->fieldsForDaoArray = array();
        for ($i=0; $i < count($this->namesFieldsArray); $i++) {
            if($this->typeFieldsArray[$i] == "VARCHAR" || $this->typeFieldsArray[$i] == "DATE" || $this->typeFieldsArray[$i] == "DATETIME"){
                $this->fieldsForDaoArray[$i] = "\"".$this->namesFieldsArray[$i];
            }else {
                $this->fieldsForDaoArray[$i] = $this->namesFieldsArray[$i];
            }
        }
    }
}
