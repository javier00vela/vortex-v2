<?php

require_once (Config::PATH . Config::BACKEND . 'general/GeneralVo.php');
class  InventoryVo  extends GeneralVo {

    public $id;
    public $typeProduct;
    public $name;
    public $brand;
    public $reference;
    public $presentation;
    public $amount;
    public $minimumAmount;
    public $typeCurrency;
    public $priceUnit;
    public $country;
    public $nameCompany;
    public $description;
    public $namePlace;
    public $IVA;
    public $FLETE;

    public function __construct() {
        $this->id = null;
        $this->typeProduct = null;
        $this->name = null;
        $this->brand = null;
        $this->reference = null;
        $this->presentation = null;
        $this->amount = null;
        $this->minimumAmount = null;
        $this->typeCurrency = null;
        $this->priceUnit = null;
        $this->country = null;
        $this->nameCompany = null;
        $this->description = null;
        $this->namePlace = null;
        $this->IVA = null;
        $this->FLETE = null;

        $this->isList = false;
        $this->nameTable = "inventory";
        $this->SetNamesFieldsToList();
    }

    private function SetNamesFieldsToList() {
        $this->namesFieldsArray = array();
        $this->namesFieldsArray[0] = "id";
        $this->namesFieldsArray[1] = "typeProduct";
        $this->namesFieldsArray[2] = "name";
        $this->namesFieldsArray[3] = "brand";
        $this->namesFieldsArray[4] = "reference";
        $this->namesFieldsArray[5] = "presentation";
        $this->namesFieldsArray[6] = "amount";
        $this->namesFieldsArray[7] = "minimumAmount";
        $this->namesFieldsArray[8] = "typeCurrency";
        $this->namesFieldsArray[9] = "priceUnit";
        $this->namesFieldsArray[10] = "country";
        $this->namesFieldsArray[11] = "nameCompany";
        $this->namesFieldsArray[12] = "description";
        $this->namesFieldsArray[13] = "namePlace";
        $this->namesFieldsArray[14] = "IVA";
        $this->namesFieldsArray[15] = "FLETE";

        $this->typeFieldsArray = array();
        $this->typeFieldsArray[0] = "INT";
        $this->typeFieldsArray[1] = "VARCHAR";
        $this->typeFieldsArray[2] = "VARCHAR";
        $this->typeFieldsArray[3] = "VARCHAR";
        $this->typeFieldsArray[4] = "VARCHAR";
        $this->typeFieldsArray[5] = "VARCHAR";
        $this->typeFieldsArray[6] = "INT";
        $this->typeFieldsArray[7] = "INT";
        $this->typeFieldsArray[8] = "VARCHAR";
        $this->typeFieldsArray[9] = "VARCHAR";
        $this->typeFieldsArray[10] = "VARCHAR";
        $this->typeFieldsArray[11] = "VARCHAR";
        $this->typeFieldsArray[12] = "VARCHAR";
        $this->typeFieldsArray[13] = "VARCHAR";
        $this->typeFieldsArray[14] = "INT";
        $this->typeFieldsArray[15] = "VARCHAR";
        


        $this->SetFieldsForDaoArray ();
    }

    private function SetFieldsForDaoArray () {
        $this->fieldsForDaoArray = array();
        for ($i=0; $i < count($this->namesFieldsArray); $i++) {
            if($this->typeFieldsArray[$i] == "VARCHAR" || $this->typeFieldsArray[$i] == "DATE" || $this->typeFieldsArray[$i] == "DATETIME"){
                $this->fieldsForDaoArray[$i] = "\"".$this->namesFieldsArray[$i];
            }else {
                $this->fieldsForDaoArray[$i] = $this->namesFieldsArray[$i];
            }
        }
    }
}
