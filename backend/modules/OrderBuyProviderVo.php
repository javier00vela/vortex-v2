<?php

require_once (Config::PATH . Config::BACKEND . 'general/GeneralVo.php');
class  OrderBuyProviderVo  extends GeneralVo {

    public $id;
    public $description;
    public $percent;
    public $pay;
    public $idProvider;
    public $idQuotation;
    public $checked;

    public function __construct() {
        $this->id = null;
        $this->description = null;
        $this->percent = null;
        $this->pay = null;
        $this->idProvider = null;
        $this->idQuotation = null;
        $this->checked = null;

        $this->isList = false;
        $this->nameTable = "ordenbuyprovider";
        $this->SetNamesFieldsToList();
    }

    private function SetNamesFieldsToList() {
        $this->namesFieldsArray = array();
        $this->namesFieldsArray[0] = "id";
        $this->namesFieldsArray[1] = "description";
        $this->namesFieldsArray[2] = "percent";
        $this->namesFieldsArray[3] = "pay";
        $this->namesFieldsArray[4] = "idProvider";
        $this->namesFieldsArray[5] = "idQuotation";
        $this->namesFieldsArray[6] = "checked";

        $this->typeFieldsArray = array();
        $this->typeFieldsArray[0] = "INT";
        $this->typeFieldsArray[1] = "VARCHAR";
        $this->typeFieldsArray[2] = "VARCHAR";
        $this->typeFieldsArray[3] = "VARCHAR";
        $this->typeFieldsArray[4] = "INT";
        $this->typeFieldsArray[5] = "INT";
        $this->typeFieldsArray[6] = "INT";

        $this->SetFieldsForDaoArray ();
    }

    private function SetFieldsForDaoArray () {
        $this->fieldsForDaoArray = array();
        for ($i=0; $i < count($this->namesFieldsArray); $i++) {
            if($this->typeFieldsArray[$i] == "VARCHAR" || $this->typeFieldsArray[$i] == "DATE" || $this->typeFieldsArray[$i] == "DATETIME"){
                $this->fieldsForDaoArray[$i] = "\"".$this->namesFieldsArray[$i];
            }else {
                $this->fieldsForDaoArray[$i] = $this->namesFieldsArray[$i];
            }
        }
    }
}
