<?php

require_once (Config::PATH . Config::BACKEND . 'general/GeneralVo.php');
class  PhotosDiagnosisVo  extends GeneralVo {

    public $id;
    public $photo1;
    public $photo2;
    public $photo3;
    public $idDiagnosis;

    public function __construct() {
        $this->id = null;
        $this->photo1 = null;
        $this->photo2 = null;
        $this->photo3 = null;
        $this->idDiagnosis = null;

        $this->isList = false;
        $this->nameTable = "photodiagnosis";
        $this->SetNamesFieldsToList();
    }

    private function SetNamesFieldsToList() {
        $this->namesFieldsArray = array();
        $this->namesFieldsArray[0] = "id";
        $this->namesFieldsArray[1] = "photo1";
        $this->namesFieldsArray[2] = "photo2";
        $this->namesFieldsArray[3] = "photo3";
        $this->namesFieldsArray[4] = "idDiagnosis";

        $this->typeFieldsArray = array();
        $this->typeFieldsArray[0] = "INT";
        $this->typeFieldsArray[1] = "VARCHAR";
        $this->typeFieldsArray[2] = "VARCHAR";
        $this->typeFieldsArray[3] = "VARCHAR";
        $this->typeFieldsArray[4] = "INT";

        $this->SetFieldsForDaoArray ();
    }

    private function SetFieldsForDaoArray () {
        $this->fieldsForDaoArray = array();
        for ($i=0; $i < count($this->namesFieldsArray); $i++) {
            if($this->typeFieldsArray[$i] == "VARCHAR" || $this->typeFieldsArray[$i] == "DATE" || $this->typeFieldsArray[$i] == "DATETIME"){
                $this->fieldsForDaoArray[$i] = "\"".$this->namesFieldsArray[$i];
            }else {
                $this->fieldsForDaoArray[$i] = $this->namesFieldsArray[$i];
            }
        }
    }
}
