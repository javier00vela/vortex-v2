<?php

require_once (Config::PATH . Config::BACKEND . 'general/GeneralVo.php');
class  ViaticsPropetyVo  extends GeneralVo {

    public $id;
    public $name;
    public $percent;
    public $quantity;
    public $price;
    public $idViatic;
    public $typeViatic;

    public function __construct() {
        $this->id = null;
        $this->name = null;
        $this->percent = null;
        $this->quantity = null;
        $this->price = null;
        $this->idViatic = null;
        $this->typeViatic = null;

        $this->isList = false;
        $this->nameTable = "viaticspropety";
        $this->SetNamesFieldsToList();
    }

    private function SetNamesFieldsToList() {
        $this->namesFieldsArray = array();
        $this->namesFieldsArray[0] = "id";
        $this->namesFieldsArray[1] = "name";
        $this->namesFieldsArray[2] = "percent";
        $this->namesFieldsArray[3] = "quantity";
        $this->namesFieldsArray[4] = "price";
        $this->namesFieldsArray[5] = "idViatic";
        $this->namesFieldsArray[6] = "typeViatic";

        $this->typeFieldsArray = array();
        $this->typeFieldsArray[0] = "INT";
        $this->typeFieldsArray[1] = "VARCHAR";
        $this->typeFieldsArray[2] = "VARCHAR";
        $this->typeFieldsArray[3] = "INT";
        $this->typeFieldsArray[4] = "VARCHAR";
        $this->typeFieldsArray[5] = "INT";
        $this->typeFieldsArray[6] = "VARCHAR";

        $this->SetFieldsForDaoArray ();
    }

    private function SetFieldsForDaoArray () {
        $this->fieldsForDaoArray = array();
        for ($i=0; $i < count($this->namesFieldsArray); $i++) {
            if($this->typeFieldsArray[$i] == "VARCHAR" || $this->typeFieldsArray[$i] == "DATE" || $this->typeFieldsArray[$i] == "DATETIME"){
                $this->fieldsForDaoArray[$i] = "\"".$this->namesFieldsArray[$i];
            }else {
                $this->fieldsForDaoArray[$i] = $this->namesFieldsArray[$i];
            }
        }
    }
}
