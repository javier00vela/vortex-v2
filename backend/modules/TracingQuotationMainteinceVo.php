<?php

require_once (Config::PATH . Config::BACKEND . 'general/GeneralVo.php');
class  TracingQuotationMainteinceVo  extends GeneralVo {

    public $id;
    public $idQuotation;
    public $idResponsable;
    public $subject;
    public $description;
    public $dateRegistry;
    public $prediction;

    public function __construct() {
        $this->id = null;
        $this->idResponsable = null;
        $this->idQuotation = null;
        $this->description = null;
        $this->subject = null;
        $this->dateRegistry = null;
        $this->prediction = null;
        $this->isList = false;
        $this->nameTable = "tracingquotationmainteince";
        $this->SetNamesFieldsToList();
    }

    private function SetNamesFieldsToList() {
        $this->namesFieldsArray = array();
        $this->namesFieldsArray[0] = "id";
        $this->namesFieldsArray[1] = "idQuotation";
        $this->namesFieldsArray[2] = "idResponsable";
        $this->namesFieldsArray[3] = "subject";
        $this->namesFieldsArray[4] = "description";
        $this->namesFieldsArray[5] = "dateRegistry";
        $this->namesFieldsArray[6] = "prediction";

        $this->typeFieldsArray = array();
        $this->typeFieldsArray[0] = "INT";
        $this->typeFieldsArray[1] = "INT";
        $this->typeFieldsArray[2] = "INT";
        $this->typeFieldsArray[3] = "VARCHAR";
        $this->typeFieldsArray[4] = "VARCHAR";
        $this->typeFieldsArray[5] = "VARCHAR";
        $this->typeFieldsArray[6] = "VARCHAR";

        $this->SetFieldsForDaoArray ();
    }

    private function SetFieldsForDaoArray () {
        $this->fieldsForDaoArray = array();
        for ($i=0; $i < count($this->namesFieldsArray); $i++) {
            if($this->typeFieldsArray[$i] == "VARCHAR" || $this->typeFieldsArray[$i] == "DATE" || $this->typeFieldsArray[$i] == "DATETIME"){
                $this->fieldsForDaoArray[$i] = "\"".$this->namesFieldsArray[$i];
            }else {
                $this->fieldsForDaoArray[$i] = $this->namesFieldsArray[$i];
            }
        }
    }
}
