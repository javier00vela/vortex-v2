<?php

require_once (Config::PATH . Config::BACKEND . 'general/GeneralVo.php');
class  PricesCompanyVo  extends GeneralVo {

    public $id;
    public $typeMoney;
    //public $iva;
    public $tpm;
    public $ganacia;
    public $idCompany;

    public function __construct() {
        $this->id = null;
        $this->typeMoney = null;
        //$this->iva = null;
        $this->tpm = null;
        $this->ganancia = null;
        $this->idCompany = null;

        $this->isList = false;
        $this->nameTable = "pricescompany";
        $this->SetNamesFieldsToList();
    }

    private function SetNamesFieldsToList() {
        $this->namesFieldsArray = array();
        $this->namesFieldsArray[0] = "id";
        $this->namesFieldsArray[1] = "typeMoney";
        $this->namesFieldsArray[2] = "tpm";
        $this->namesFieldsArray[3] = "ganancia";
        $this->namesFieldsArray[4] = "idCompany";

        $this->typeFieldsArray = array();
        $this->typeFieldsArray[0] = "INT";
        $this->typeFieldsArray[1] = "VARCHAR";
        $this->typeFieldsArray[2] = "VARCHAR";
        $this->typeFieldsArray[3] = "VARCHAR";
        $this->typeFieldsArray[4] = "INT";
       
        $this->SetFieldsForDaoArray ();
    }

    private function SetFieldsForDaoArray () {
        $this->fieldsForDaoArray = array();
        for ($i=0; $i < count($this->namesFieldsArray); $i++) {
            if($this->typeFieldsArray[$i] == "VARCHAR" || $this->typeFieldsArray[$i] == "DATE" || $this->typeFieldsArray[$i] == "DATETIME"){
                $this->fieldsForDaoArray[$i] = "\"".$this->namesFieldsArray[$i];
            }else {
                $this->fieldsForDaoArray[$i] = $this->namesFieldsArray[$i];
            }
        }
    }
}
