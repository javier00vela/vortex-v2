<?php

require_once (Config::PATH . Config::BACKEND . 'general/GeneralVo.php');
class  ViaticsExpensiveVo  extends GeneralVo {

    public $id;
    public $name;
    public $reference;
    public $price;
    public $idViatic;
    public $idMainteince;
    public $typeViatic;

    public function __construct() {
        $this->id = null;
        $this->name = null;
        $this->reference = null;
        $this->price = null;
        $this->idViatic = null;
        $this->idMainteince = null;
        $this->typeViatic = null;

        $this->isList = false;
        $this->nameTable = "viaticsexpenses";
        $this->SetNamesFieldsToList();
    }

    private function SetNamesFieldsToList() {
        $this->namesFieldsArray = array();
        $this->namesFieldsArray[0] = "id";
        $this->namesFieldsArray[1] = "name";
        $this->namesFieldsArray[2] = "reference";
        $this->namesFieldsArray[3] = "price";
        $this->namesFieldsArray[4] = "idViatic";
        $this->namesFieldsArray[5] = "idMainteince";
        $this->namesFieldsArray[6] = "typeViatic";

        $this->typeFieldsArray = array();
        $this->typeFieldsArray[0] = "INT";
        $this->typeFieldsArray[1] = "VARCHAR";
        $this->typeFieldsArray[2] = "VARCHAR";
        $this->typeFieldsArray[3] = "VARCHAR";
        $this->typeFieldsArray[4] = "INT";
        $this->typeFieldsArray[5] = "INT";
        $this->typeFieldsArray[6] = "VARCHAR";

        $this->SetFieldsForDaoArray ();
    }

    private function SetFieldsForDaoArray () {
        $this->fieldsForDaoArray = array();
        for ($i=0; $i < count($this->namesFieldsArray); $i++) {
            if($this->typeFieldsArray[$i] == "VARCHAR" || $this->typeFieldsArray[$i] == "DATE" || $this->typeFieldsArray[$i] == "DATETIME"){
                $this->fieldsForDaoArray[$i] = "\"".$this->namesFieldsArray[$i];
            }else {
                $this->fieldsForDaoArray[$i] = $this->namesFieldsArray[$i];
            }
        }
    }
}
