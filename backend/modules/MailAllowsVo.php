<?php

require_once (Config::PATH . Config::BACKEND . 'general/GeneralVo.php');
class  MailAllowsVo  extends GeneralVo {

    public $id;
    public $idUser;
    public $mail;
    public $isAllow;
    public $module;
    

    public function __construct() {
        $this->id = null;
        $this->idUser = null;
        $this->mail = null;
        $this->isAllow = null;
        $this->module = null;

        $this->isList = false;
        $this->nameTable = "mailallows";
        $this->SetNamesFieldsToList();
    }

    private function SetNamesFieldsToList() {
        $this->namesFieldsArray = array();
        $this->namesFieldsArray[0] = "id";
        $this->namesFieldsArray[1] = "idUser";
        $this->namesFieldsArray[2] = "mail";
        $this->namesFieldsArray[3] = "isAllow";
        $this->namesFieldsArray[4] = "module";

        $this->typeFieldsArray = array();
        $this->typeFieldsArray[0] = "INT";
        $this->typeFieldsArray[1] = "INT";
        $this->typeFieldsArray[2] = "VARCHAR";
        $this->typeFieldsArray[3] = "INT";
        $this->typeFieldsArray[4] = "INT";

        $this->SetFieldsForDaoArray ();
    }

    private function SetFieldsForDaoArray () {
        $this->fieldsForDaoArray = array();
        for ($i=0; $i < count($this->namesFieldsArray); $i++) {
            if($this->typeFieldsArray[$i] == "VARCHAR" || $this->typeFieldsArray[$i] == "DATE" || $this->typeFieldsArray[$i] == "DATETIME"){
                $this->fieldsForDaoArray[$i] = "\"".$this->namesFieldsArray[$i];
            }else {
                $this->fieldsForDaoArray[$i] = $this->namesFieldsArray[$i];
            }
        }
    }
}
