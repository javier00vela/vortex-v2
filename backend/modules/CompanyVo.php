<?php

require_once (Config::PATH . Config::BACKEND . 'general/GeneralVo.php');
class  CompanyVo  extends GeneralVo {

    public $id;
    public $name;
    public $nit;
    public $phone;
    public $email;
    public $webSite;
    public $address;
    public $country;
    public $city;
    public $creationDate;
    public $idRoleCompany;
    public $state;

    public function __construct() {
        $this->id = null;
        $this->name = null;
        $this->nit = null;
        $this->phone = null;
        $this->email = null;
        $this->webSite = null;
        $this->address = null;
        $this->country = null;
        $this->city = null;
        $this->creationDate = null;
        $this->idRoleCompany = null;
        $this->state = null;

        $this->isList = false;
        $this->nameTable = "company";
        $this->SetNamesFieldsToList();
    }

    private function SetNamesFieldsToList() {
        $this->namesFieldsArray = array();
        $this->namesFieldsArray[0] = "id";
        $this->namesFieldsArray[1] = "name";
        $this->namesFieldsArray[2] = "nit";
        $this->namesFieldsArray[3] = "phone";
        $this->namesFieldsArray[4] = "email";
        $this->namesFieldsArray[5] = "webSite";
        $this->namesFieldsArray[6] = "address";
        $this->namesFieldsArray[7] = "country";
        $this->namesFieldsArray[8] = "city";
        $this->namesFieldsArray[9] = "creationDate";
        $this->namesFieldsArray[10] = "idRoleCompany";
        $this->namesFieldsArray[11] = "state";

        $this->typeFieldsArray = array();
        $this->typeFieldsArray[0] = "INT";
        $this->typeFieldsArray[1] = "VARCHAR";
        $this->typeFieldsArray[2] = "VARCHAR";
        $this->typeFieldsArray[3] = "VARCHAR";
        $this->typeFieldsArray[4] = "VARCHAR";
        $this->typeFieldsArray[5] = "VARCHAR";
        $this->typeFieldsArray[6] = "VARCHAR";
        $this->typeFieldsArray[7] = "VARCHAR";
        $this->typeFieldsArray[8] = "VARCHAR";
        $this->typeFieldsArray[9] = "DATETIME";
        $this->typeFieldsArray[10] = "INT";
        $this->typeFieldsArray[11] = "INT";

        $this->SetFieldsForDaoArray ();
    }

    private function SetFieldsForDaoArray () {
        $this->fieldsForDaoArray = array();
        for ($i=0; $i < count($this->namesFieldsArray); $i++) {
            if($this->typeFieldsArray[$i] == "VARCHAR" || $this->typeFieldsArray[$i] == "DATE" || $this->typeFieldsArray[$i] == "DATETIME"){
                $this->fieldsForDaoArray[$i] = "\"".$this->namesFieldsArray[$i];
            }else {
                $this->fieldsForDaoArray[$i] = $this->namesFieldsArray[$i];
            }
        }
    }
}
