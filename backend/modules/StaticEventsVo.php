<?php

require_once (Config::PATH . Config::BACKEND . 'general/GeneralVo.php');
class  StaticEventsVo  extends GeneralVo {

    public $id;
    public $nameEvent;
    public $date;
    public $dateFinish;
    public $idUser;
    public $hourInit;
    public $hourFinish;
   

    public function __construct() {
        $this->id = null;
        $this->nameEvent = null;
        $this->date = null;
        $this->idUser = null;
        $this->hourInit = null;
        $this->hourFinish = null;
        $this->dateFinish = null;
        

        $this->isList = false;
        $this->nameTable = "staticevents";
        $this->SetNamesFieldsToList();
    }

    private function SetNamesFieldsToList() {
        $this->namesFieldsArray = array();
        $this->namesFieldsArray[0] = "id";
        $this->namesFieldsArray[1] = "nameEvent";
        $this->namesFieldsArray[2] = "date";
        $this->namesFieldsArray[3] = "idUser";
        $this->namesFieldsArray[4] = "hourInit";
        $this->namesFieldsArray[5] = "hourFinish";
        $this->namesFieldsArray[6] = "dateFinish";
        

        $this->typeFieldsArray = array();
        $this->typeFieldsArray[0] = "INT";
        $this->typeFieldsArray[1] = "VARCHAR";
        $this->typeFieldsArray[2] = "DATE";
        $this->typeFieldsArray[3] = "INT";
        $this->typeFieldsArray[4] = "VARCHAR";
        $this->typeFieldsArray[5] = "VARCHAR";
        $this->typeFieldsArray[6] = "DATE";
        $this->SetFieldsForDaoArray ();
    }

    private function SetFieldsForDaoArray () {
        $this->fieldsForDaoArray = array();
        for ($i=0; $i < count($this->namesFieldsArray); $i++) {
            if($this->typeFieldsArray[$i] == "VARCHAR" || $this->typeFieldsArray[$i] == "DATE" || $this->typeFieldsArray[$i] == "DATETIME"){
                $this->fieldsForDaoArray[$i] = "\"".$this->namesFieldsArray[$i];
            }else {
                $this->fieldsForDaoArray[$i] = $this->namesFieldsArray[$i];
            }
        }
    }
}
