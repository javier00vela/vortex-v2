<?php

require_once (Config::PATH . Config::BACKEND . 'general/GeneralVo.php');
class  ViaticsVo  extends GeneralVo {

    public $id;
    public $place;
    public $days;
    public $isAccept;
    public $numberPersons;

    public function __construct() {
        $this->id = null;
        $this->place = null;
        $this->days = null;
        $this->numberPersons = null;
        $this->isAccept = null;

        $this->isList = false;
        $this->nameTable = "viatics";
        $this->SetNamesFieldsToList();
    }

    private function SetNamesFieldsToList() {
        $this->namesFieldsArray = array();
        $this->namesFieldsArray[0] = "id";
        $this->namesFieldsArray[1] = "place";
        $this->namesFieldsArray[2] = "days";
        $this->namesFieldsArray[3] = "isAccept";
        $this->namesFieldsArray[4] = "numberPersons";

        $this->typeFieldsArray = array();
        $this->typeFieldsArray[0] = "INT";
        $this->typeFieldsArray[1] = "VARCHAR";
        $this->typeFieldsArray[2] = "INT";
        $this->typeFieldsArray[3] = "INT";
        $this->typeFieldsArray[4] = "INT";

        $this->SetFieldsForDaoArray ();
    }

    private function SetFieldsForDaoArray () {
        $this->fieldsForDaoArray = array();
        for ($i=0; $i < count($this->namesFieldsArray); $i++) {
            if($this->typeFieldsArray[$i] == "VARCHAR" || $this->typeFieldsArray[$i] == "DATE" || $this->typeFieldsArray[$i] == "DATETIME"){
                $this->fieldsForDaoArray[$i] = "\"".$this->namesFieldsArray[$i];
            }else {
                $this->fieldsForDaoArray[$i] = $this->namesFieldsArray[$i];
            }
        }
    }
}
