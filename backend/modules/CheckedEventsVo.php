<?php

require_once (Config::PATH . Config::BACKEND . 'general/GeneralVo.php');
class  CheckedEventsVo  extends GeneralVo {

    public $id;
    public $titleEvent;
    public $checked;
    public $day;

    public function __construct() {
        $this->id = null;
        $this->titleEvent = null;
        $this->checked = null;
        $this->day = null;

        $this->isList = false;
        $this->nameTable = "checkedevents";
        $this->SetNamesFieldsToList();
    }

    private function SetNamesFieldsToList() {
        $this->namesFieldsArray = array();
        $this->namesFieldsArray[0] = "id";
        $this->namesFieldsArray[1] = "titleEvent";
        $this->namesFieldsArray[2] = "checked";
        $this->namesFieldsArray[3] = "day";

        $this->typeFieldsArray = array();
        $this->typeFieldsArray[0] = "INT";
        $this->typeFieldsArray[1] = "VARCHAR";
        $this->typeFieldsArray[2] = "INT";
        $this->typeFieldsArray[3] = "INT";

        $this->SetFieldsForDaoArray ();
    }

    private function SetFieldsForDaoArray () {
        $this->fieldsForDaoArray = array();
        for ($i=0; $i < count($this->namesFieldsArray); $i++) {
            if($this->typeFieldsArray[$i] == "VARCHAR" || $this->typeFieldsArray[$i] == "DATE" || $this->typeFieldsArray[$i] == "DATETIME"){
                $this->fieldsForDaoArray[$i] = "\"".$this->namesFieldsArray[$i];
            }else {
                $this->fieldsForDaoArray[$i] = $this->namesFieldsArray[$i];
            }
        }
    }
}
