<?php

require_once (Config::PATH . Config::BACKEND . 'general/GeneralVo.php');
class  VisitDescriptionVo  extends GeneralVo {

    public $id;
    public $description;
    public $idVisits;
    public $dateCreate;
    public $dateCita;
    public $responsable;

    public function __construct() {
        $this->id = null;
        $this->numberSerial = null;


        $this->isList = false;
        $this->nameTable = "visitsdescriptions";
        $this->SetNamesFieldsToList();
    }

    private function SetNamesFieldsToList() {
        $this->namesFieldsArray = array();
        $this->namesFieldsArray[0] = "id";
        $this->namesFieldsArray[1] = "description";
        $this->namesFieldsArray[2] = "idVisits";
        $this->namesFieldsArray[3] = "dateCreate";
        $this->namesFieldsArray[4] = "dateCita";
        $this->namesFieldsArray[5] = "responsable";


        $this->typeFieldsArray = array();
        $this->typeFieldsArray[0] = "INT";
        $this->typeFieldsArray[1] = "VARCHAR";
        $this->typeFieldsArray[2] = "INT";
        $this->typeFieldsArray[3] = "DATETIME";
        $this->typeFieldsArray[4] = "VARCHAR";
        $this->typeFieldsArray[5] = "INT";

        $this->SetFieldsForDaoArray ();
    }

    private function SetFieldsForDaoArray () {
        $this->fieldsForDaoArray = array();
        for ($i=0; $i < count($this->namesFieldsArray); $i++) {
            if($this->typeFieldsArray[$i] == "VARCHAR" || $this->typeFieldsArray[$i] == "DATE" || $this->typeFieldsArray[$i] == "DATETIME"){
                $this->fieldsForDaoArray[$i] = "\"".$this->namesFieldsArray[$i];
            }else {
                $this->fieldsForDaoArray[$i] = $this->namesFieldsArray[$i];
            }
        }
    }
}
