<?php

require_once (Config::PATH . Config::BACKEND . 'general/GeneralVo.php');
class  ClientUserVo  extends GeneralVo {

    public $id;
    public $idUser;
    public $idClient;
    public $prefijo;

    public function __construct() {
        $this->id = null;
        $this->idUser = null;
        $this->idClient = null;
        $this->prefijo = null;


        $this->isList = false;
        $this->nameTable = "clientuser";
        $this->SetNamesFieldsToList();
    }

    private function SetNamesFieldsToList() {
        $this->namesFieldsArray = array();
        $this->namesFieldsArray[0] = "id";
        $this->namesFieldsArray[1] = "idUser";
        $this->namesFieldsArray[2] = "idClient";
        $this->namesFieldsArray[3] = "prefijo";

        $this->typeFieldsArray = array();
        $this->typeFieldsArray[0] = "INT";
        $this->typeFieldsArray[1] = "INT";
        $this->typeFieldsArray[2] = "INT";
        $this->typeFieldsArray[3] = "VARCHAR";

        $this->SetFieldsForDaoArray ();
    }

    private function SetFieldsForDaoArray () {
        $this->fieldsForDaoArray = array();
        for ($i=0; $i < count($this->namesFieldsArray); $i++) {
            if($this->typeFieldsArray[$i] == "VARCHAR" || $this->typeFieldsArray[$i] == "DATE" || $this->typeFieldsArray[$i] == "DATETIME"){
                $this->fieldsForDaoArray[$i] = "\"".$this->namesFieldsArray[$i];
            }else {
                $this->fieldsForDaoArray[$i] = $this->namesFieldsArray[$i];
            }
        }
    }
}
