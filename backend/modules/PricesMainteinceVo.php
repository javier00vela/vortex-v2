<?php
require_once (Config::PATH . Config::BACKEND . 'general/GeneralVo.php');
class  PricesMainteinceVo  extends GeneralVo {

    public $id;
    public $name;
    public $description;
    public $ganance;
    public $time;
    public $price;

    public function __construct() {
        $this->id = null;
        $this->name = null;
        $this->description = null;
        $this->ganance = null;
        $this->time = null;
        $this->price = null;

        $this->isList = false;
        $this->nameTable = "pricesmainteince";
        $this->SetNamesFieldsToList();
    }

    private function SetNamesFieldsToList() {
        $this->namesFieldsArray = array();
        $this->namesFieldsArray[0] = "id";
        $this->namesFieldsArray[1] = "name";
         $this->namesFieldsArray[2] = "description";
        $this->namesFieldsArray[3] = "ganance";
        $this->namesFieldsArray[4] = "time";
        $this->namesFieldsArray[5] = "price";

        $this->typeFieldsArray = array();
        $this->typeFieldsArray[0] = "INT";
        $this->typeFieldsArray[1] = "VARCHAR";
         $this->typeFieldsArray[2] = "VARCHAR";
          $this->typeFieldsArray[3] = "VARCHAR";
          $this->typeFieldsArray[4] = "VARCHAR";
          $this->typeFieldsArray[5] = "VARCHAR";

        $this->SetFieldsForDaoArray ();
    }

    private function SetFieldsForDaoArray () {
        $this->fieldsForDaoArray = array();
        for ($i=0; $i < count($this->namesFieldsArray); $i++) {
            if($this->typeFieldsArray[$i] == "VARCHAR" || $this->typeFieldsArray[$i] == "DATE" || $this->typeFieldsArray[$i] == "DATETIME"){
                $this->fieldsForDaoArray[$i] = "\"".$this->namesFieldsArray[$i];
            }else {
                $this->fieldsForDaoArray[$i] = $this->namesFieldsArray[$i];
            }
        }
    }
}
