<?php

require_once (Config::PATH . Config::BACKEND . 'general/GeneralVo.php');
class  UserVo  extends GeneralVo {

    public $id;
    public $nickname;
    public $password;
    public $state;
    public $photo;
    public $idPerson;

    public function __construct() {
        $this->id = null;
        $this->nickname = null;
        $this->password = null;
        $this->state = null;
        $this->photo = null;
        $this->idPerson = null;

        $this->isList = false;
        $this->nameTable = "user";
        $this->SetNamesFieldsToList();
    }

    private function SetNamesFieldsToList() {
        $this->namesFieldsArray = array();
        $this->namesFieldsArray[0] = "id";
        $this->namesFieldsArray[1] = "nickname";
        $this->namesFieldsArray[2] = "password";
        $this->namesFieldsArray[3] = "state";
        $this->namesFieldsArray[4] = "photo";
        $this->namesFieldsArray[5] = "idPerson";

        $this->typeFieldsArray = array();
        $this->typeFieldsArray[0] = "INT";
        $this->typeFieldsArray[1] = "VARCHAR";
        $this->typeFieldsArray[2] = "VARCHAR";
        $this->typeFieldsArray[3] = "INT";
        $this->typeFieldsArray[4] = "VARCHAR";
        $this->typeFieldsArray[5] = "INT";

        $this->SetFieldsForDaoArray ();
    }

    private function SetFieldsForDaoArray () {
        $this->fieldsForDaoArray = array();
        for ($i=0; $i < count($this->namesFieldsArray); $i++) {
            if($this->typeFieldsArray[$i] == "VARCHAR" || $this->typeFieldsArray[$i] == "DATE" || $this->typeFieldsArray[$i] == "DATETIME"){
                $this->fieldsForDaoArray[$i] = "\"".$this->namesFieldsArray[$i];
            }else {
                $this->fieldsForDaoArray[$i] = $this->namesFieldsArray[$i];
            }
        }
    }
}
