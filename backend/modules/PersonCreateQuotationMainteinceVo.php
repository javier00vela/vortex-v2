<?php
require_once (Config::PATH . Config::BACKEND . 'general/GeneralVo.php');
class  PersonCreateQuotationMainteinceVo  extends GeneralVo {

    public $id;
    public $completeName;
    public $rolName;
    public $cellPhone;
    public $email;
    public $idPerson;
    public $idQuotation;

    public function __construct() {
        $this->id = null;
        $this->completeName = null;
         $this->rolName = null;
         $this->cellPhone = null;
         $this->email = null;
         $this->idQuotation = null;
         $this->idPerson = null;
         $this->state = null;
        $this->isList = false;
        $this->nameTable = "personcreatequotationmainteince";
        $this->SetNamesFieldsToList();
    }

    private function SetNamesFieldsToList() {
        $this->namesFieldsArray = array();
        $this->namesFieldsArray[0] = "id";
        $this->namesFieldsArray[1] = "completeName";
        $this->namesFieldsArray[2] = "rolName";
         $this->namesFieldsArray[3] = "cellPhone";
        $this->namesFieldsArray[4] = "email";
         $this->namesFieldsArray[5] = "idQuotation";
           $this->namesFieldsArray[6] = "state";
           $this->namesFieldsArray[7] = "idPerson";

        $this->typeFieldsArray = array();
        $this->typeFieldsArray[0] = "INT";
        $this->typeFieldsArray[1] = "VARCHAR";
        $this->typeFieldsArray[2] = "VARCHAR";
        $this->typeFieldsArray[3] = "VARCHAR";
        $this->typeFieldsArray[4] = "VARCHAR";
        $this->typeFieldsArray[5] = "INT";
        $this->typeFieldsArray[6] = "INT";
        $this->typeFieldsArray[7] = "INT";

        $this->SetFieldsForDaoArray ();
    }

    private function SetFieldsForDaoArray () {
        $this->fieldsForDaoArray = array();
        for ($i=0; $i < count($this->namesFieldsArray); $i++) {
            if($this->typeFieldsArray[$i] == "VARCHAR" || $this->typeFieldsArray[$i] == "DATE" || $this->typeFieldsArray[$i] == "DATETIME"){
                $this->fieldsForDaoArray[$i] = "\"".$this->namesFieldsArray[$i];
            }else {
                $this->fieldsForDaoArray[$i] = $this->namesFieldsArray[$i];
            }
        }
    }
}
