<?php
require_once (Config::PATH . Config::BACKEND . 'general/GeneralVo.php');
class  CrudVo  extends GeneralVo {

    public $idProduct;
    public $nameProduct;
    public $costProduct;
    public $providerProduct;
    public $companyProduct;
    public $dateProduct;


    public function __construct() {
        $this->idProduct = null;
        $this->nameProduct = null;
        $this->costProduct = null;
        $this->providerProduct = null;
        $this->companyProduct = null;
        $this->dateProduct = null;

        $this->isList = false;
        $this->nameTable = "test";
        $this->SetNamesFieldsToList();
    }

    private function SetNamesFieldsToList() {
        $this->namesFieldsArray = array();
        $this->namesFieldsArray[0] = "id";
        $this->namesFieldsArray[1] = "nameProduct";
        $this->namesFieldsArray[2] = "costProduct";
        $this->namesFieldsArray[3] = "providerProduct";
        $this->namesFieldsArray[4] = "companyProduct";
        $this->namesFieldsArray[5] = "dateProduct";

        $this->typeFieldsArray = array();
        $this->typeFieldsArray[0] = "INT";
        $this->typeFieldsArray[1] = "VARCHAR";
        $this->typeFieldsArray[2] = "VARCHAR";
        $this->typeFieldsArray[3] = "VARCHAR";
        $this->typeFieldsArray[4] = "VARCHAR";
        $this->typeFieldsArray[5] = "DATETIME";

        $this->SetFieldsForDaoArray ();
    }

    private function SetFieldsForDaoArray () {
        $this->fieldsForDaoArray = array();
        for ($i=0; $i < count($this->namesFieldsArray); $i++) {
            if($this->typeFieldsArray[$i] == "VARCHAR" || $this->typeFieldsArray[$i] == "DATE" || $this->typeFieldsArray[$i] == "DATETIME"){
                $this->fieldsForDaoArray[$i] = "\"".$this->namesFieldsArray[$i];
            }else {
                $this->fieldsForDaoArray[$i] = $this->namesFieldsArray[$i];
            }
        }
    }
}
