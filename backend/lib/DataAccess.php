<?php
/**
 * DataAccess -- Clase encargada de administrar el acceso a una base de datos MySql.
 *
 * @author Harold D Duque C
 * @version 1.0
 * @since Jan de 2015
 */
class DataAccess {
	private $link;

    private static $singletonInstance;


    public static function GetInstance($host, $usuario, $clave, $bd){
        if(!self::$singletonInstance){
            self::$singletonInstance = new self($host, $usuario, $clave, $bd);
        }
        return self::$singletonInstance;
    }


	public function __construct($host, $usuario, $clave, $bd) {
		if(empty($this->link)){
		$this->link = mysqli_connect($host,$usuario,$clave,$bd);
		}
		//mysql_query ("SET NAMES 'utf8'");
		if ($this->link === false) {
			throw new Exception("No se pudo conectar con MySql.");
		} elseif (mysqli_select_db($this->link, $bd) === false) {
			throw new Exception("No se pudo conectar con la base de datos.");
		}
	}

	public function query($sql) {
        $resource = mysqli_query($this->link, $sql);
		if ($resource === false) {
			//if($_SERVER['HTTP_HOST'] == "localhost"){
				print_r( $sql );
			  throw new Exception("<div class='box box-primary text-black text-center'>No se pudo ejecutar la sentencia:<br><br>".$sql."<br><br>".  mysqli_errno($this->link) . ": " . mysqli_errno($this->link) . "</div>");
			//}else{
			  //throw new Exception("<script>MessageSwalBasic('Se Ha Presentado Un Error','ractifique la información insertada o ejecutada y/o contacte al proveedor del servicio','error') ;</script>");
			//}
		} else {
			return $resource;
		}
		$this->CloseConnection();
	}

	public function insert_id() {
		return mysqli_insert_id($this->link);
	}

	public function fetch_object($result) {
    return mysqli_fetch_object($result);

	}

	public function fetch_array($result) {
    return mysqli_fetch_array($result);
	}

	public function data_seek($result, $row_number) {
		if ($row_number >= 0 and $row_number < mysqli_num_rows($result)) {
			$resultado = mysqli_data_seek($result, $row_number);
			if ($resultado === false) {
				throw new Exception("No se pudo mover el puntero.");
			}else{
        return $resultado;
      }
		}
	}

	public function num_rows($result) {
		return mysqli_num_rows($result);
	}

	public function CloseConnection(){
        mysqli_close($this->link);
    }
}
