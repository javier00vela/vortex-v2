<div class="content-wrapper">
    <section class="content-header">
        <h1>
             Orden de Compra y facturación cliente -  cotización#{$ordenNumber}
        </h1>
        <ol class="breadcrumb">
            <li><a href="Home"><i class="fa fa-dashboard"></i> Inicio</a></li>
            <li class="active"> Orden de Compra y facturación cliente -  cotización#{$ordenNumber}</li>
        </ol>
    </section>
    <input type="hidden" id="orderNumber" value="{$ordenNumber}">
    <section class="content">
        <div class="box">
            <nav class="navbar navbar-light">
                <div class="container-fluid">
                    <ul class="nav navbar-nav">
                            <li>
                                    <a style="cursor: pointer;" id="userAddBtn" name="userAddBtn" onclick="history.back();">
                                        <i class="fa fa-arrow-left"></i> Regresar
                                    </a>
                                </li>
                        <li>
                            <a style="cursor: pointer;" id="userAddBtn" name="userAddBtn" onclick="location.href = 'Home' ">
                                <i class="fa fa-home"></i> Menu Principal
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>

            <div class="box-body">
                <table id="QuotesTbl" name="QuotesTbl" class="table table-bordered table-striped dt-responsive tables dataTable no-footer dtr-inline" style="width: 100%;" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Código Cliente</th>
                            <th>Nombre Cliente</th>
                            <th>Tipo De documento</th>
                            <th>Teléfono Cliente</th>
                            <th>Correo Cliente</th>
                            <th>Codigó de Cotización</th>
                            <th>Porcentaje</th>
                            <th>Medio de pago</th>
                            <th>Observación</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </section>
</div>
<div id="descriptionPopUpAdd" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form id="impostForm" role="form" method="post" enctype="multipart/form-data">
                <!--=====================================
              Hidden
              ======================================-->
                <input id="optionInputForm" name="optionInputForm" type="hidden" value="9">
                <input id="idDataInputForm" name="idDataInputForm" type="hidden" value="0">
                <input id="isActionFromAjax" name="isActionFromAjax" type="hidden" value="false">
                <!--=====================================
              CABEZA DEL MODAL
              ======================================-->
                <div class="modal-header" style="background:#3c8dbc; color:white">
                    <button type="button" class="close" data-dismiss="modal" data-target='#impostPopUpAdd' onclick="ResetControls('impostForm')">&times;</button>
                    <h4 class="modal-title">Descripción</h4>
                </div>
                <!--=====================================
              CUERPO DEL MODAL
              ======================================-->
                <div class="modal-body">
                    <div class="box-body">
                        <div class="row">
                            <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <textarea required name="description" id="description" class="form-control" cols="30" rows="10" placeholder="Descripción para la orden o factura"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <!--=====================================
              PIE DEL MODAL
              ======================================-->
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal" data-target='#impostPopUpAdd' onclick="ResetControls('impostForm')">Salir</button>
                    <button type="submit" class="btn btn-primary">Guardar </button>
                </div>
            </form>

        </div>
    </div>
</div>

<div id="containerResponseAjax"></div>
<script src="view/html/modules/buylistquotesclient/BuyListQuotesClient.js?v= {$tempParameter}"></script>
{$jquery}