<?php
require_once(__DIR__ . '/../../../../controller/general/Config.php');
require_once(Config::PATH . Config::CONTROLLER.Config::GENERAL . 'ManageArrays.php');
require_once(Config::PATH . Config::BACKEND . 'modules/QuotationVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/ProductVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/CompanyVo.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralDao.php');
require_once(Config::PATH . Config::BACKEND . 'modules/OrderBuyClientVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/OrderBuyProviderVo.php');


class AjaxDataImpost{

    private $isActionFromAjax;
    private $optionInputForm;
    private $idDataInputForm;
    public $dataList = [];

    function __construct()
    {
        if (isset($_POST['isActionFromAjax'])) {
            $this->isActionFromAjax = $_POST['isActionFromAjax'];
            $this->optionInputForm = $_POST['optionInputForm'];
            $this->idDataInputForm = $_POST['idDataInputForm'];
            if(isset($_POST["listData"])){
                $this->dataList = $_POST["listData"];  
            }
            if ($this->isActionFromAjax == "true") {
                if ($this->optionInputForm == 3) {
                    $this->SetDataInControls();
                }else if($this->optionInputForm == 5){
                $this->GetLengthImpost();
                }else if($this->optionInputForm == 6){
                $this->UpdateOrdenCharacters();
                }else {
                    $this->GetDataInJsonForTbl();
                }
            }
        }
    }

    private function SetDataInControls(){
        $SetInputsDataFJQ = "";
      
        $OrderBuyClientVo = new OrderBuyClientVo();
        $OrderBuyClientVo->id = $this->idDataInputForm;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $OrderBuyClientVo);
        if($OrderBuyClientVo2 = $generalDao->GetVo($generalVo = clone $OrderBuyClientVo)){

            $SetInputsDataFJQ = "
                            <script>
                               (function($){
                                SetValueToInputText('description','{$OrderBuyClientVo2->description}');
                                SetValueToInputText('optionInputForm',9);
                                SetValueToInputText('idDataInputForm',$OrderBuyClientVo2->id);
                               })(jQuery);
                            </script>
                        ";
        }
        echo $SetInputsDataFJQ;
    }
    

    public function UpdateOrdenCharacters(){
        $OrderBuyClientVo = new OrderBuyClientVo();
        $generalDao = new GeneralDao();
        $OrderBuyClientVo->id = $_POST["id"];
        $OrderBuyClientVo->idSearchField = 0;
        if($_POST["type"] ==  "percent"){
            $OrderBuyClientVo->idUpdateField = 2;
            $OrderBuyClientVo->percent = $_POST["percent"];
        }else if($_POST["type"] ==  "pay"){
            $OrderBuyClientVo->idUpdateField = 3;
            $OrderBuyClientVo->pay = $_POST["pay"];
        }else{
            $OrderBuyClientVo->idUpdateField = 1;
            $OrderBuyClientVo->percent = $_POST["description"];
        }
        $generalDao->UpdateByField($generalVo = clone $OrderBuyClientVo);
    }

    public function GetDataInJsonForTbl()
    {
        $existData = false;
        $OrderBuyClientVo = new OrderBuyClientVo();
        $OrderBuyClientVo->idQuotation = $_POST["idQuote"];
        $OrderBuyClientVo->idSearchField = 5;
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $OrderBuyClientVo);
        $string = "
        {\"data\": [";
        while ($OrderBuyClientVo2 = $generalDao->GetVo($generalVo = clone $OrderBuyClientVo)) {
                $company = $this->GetCompany($OrderBuyClientVo2->idClient);

                $existData = true;
                $json = array();
                $json[] = $OrderBuyClientVo2->id;
                $json[] = htmlspecialchars(ucfirst(strtolower($company->name)));
                $json[] = $OrderBuyClientVo2->type;
                $json[] = $company->phone;
                $json[] = $company->email;
                $json[] = "#".$OrderBuyClientVo2->idClient;
                
                $json[] = "<input id=\"percent-{$OrderBuyClientVo2->id}\" type=\"number\" placeholder=\"Agregar porcentaje\" class=\"form-control\"  onchange=\"ChangePercent({$OrderBuyClientVo2->id} , 'percent')\" value=\"{$OrderBuyClientVo2->percent}\">";
                $json[] = "<select class=\"form-control\" id=\"pay-$OrderBuyClientVo2->id\" onchange=\"ChangePercent({$OrderBuyClientVo2->id} , 'pay')\">
                    <option value=\"Credito\" ".(($OrderBuyClientVo2->pay == 'Credito') ? "selected='true'" : "'false'"  )."  >Credito</option>
                    <option value=\"Debito\" ".(($OrderBuyClientVo2->pay == 'Debito') ? "selected='true'" : "'false'"  ).">Debito</option>
                    <option value=\"Anticipado\" ".(($OrderBuyClientVo2->pay == 'Anticipado') ? "selected='true'" : "'false'"  ).">Anticipado</option>
                </select>";

                $json[] = "<button class=\"btn btn-block btn-primary\" data-toggle='modal' data-target='#descriptionPopUpAdd' onclick='UpdateData({$OrderBuyClientVo2->id},\"buylistquotesclient\",\"BuyQuotesListClient\");SetValueToInputText(\"optionInputForm\",9);' >Descripcion</button>";

                $button = "";
                if(!in_array( "imprimir_orden_compra" , $this->dataList)){
                    if($OrderBuyClientVo2->type != "Factura"){
                        $button .= "<tip title='Generar {$OrderBuyClientVo2->type}' class='blue-tooltip' data-toggle='tooltip' data-placement='top'><button id='Update' onClick='PrintDataOrden({$OrderBuyClientVo2->idQuotation},\"{$company->name}\", \"{$OrderBuyClientVo2->id}\" , \"{$company->id}\")' name='OrdenPrintBtn' class='btn btn-primary btn-xs'><i class='fa fa-copy'></i></button> </tip>";
                    }else{
                        $button .= "<tip title='Generar {$OrderBuyClientVo2->type}' class='red-tooltip' data-toggle='tooltip' data-placement='top'><button id='Update' onClick='PrintDataFactura({$OrderBuyClientVo2->idQuotation},\"{$company->name}\", \"{$OrderBuyClientVo2->id}\", \"{$company->id}\",)' name='OrdenPrintBtn' class='btn btn-danger btn-xs'><i class='fa fa-copy'></i></button> </tip>"; 
                    }
                 }
                    $json[] = $button;
    
                $string .= json_encode($json) . ",";
                $arrayProvider[] = $company->id ;
            
           
        }
        $string .= "-]}-";

        if($existData){
            echo  str_replace(",-]}-", "]}", $string);
        }else{
            echo "{ \"data\": [] }";
        }
    }


    public function GetCompany($id){
        $arrayList = array();
        $CompanyVo = new CompanyVo();
        $CompanyVo->id = $id;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $CompanyVo);
        if($CompanyVo2 = $generalDao->GetVo($generalVo = clone $CompanyVo)) {            
            return $CompanyVo2;
        }  
        return false;
    }


    private function GetSizeProductsByQuote($id){
        $arrayList = array();
        $ProductVo = new ProductVo();
        $ProductVo->idQuotation = $id;
        $ProductVo->idSearchField = 16;
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $ProductVo);
        while($ProductVo2 = $generalDao->GetVo($generalVo = clone $ProductVo)) {            
            $arrayList[] =  $ProductVo2->id;
        }  
        return count($arrayList); 
    }

    
}

$ajaxDataImpost = new AjaxDataImpost();
