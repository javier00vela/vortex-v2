"use strict";
$(function() {
    var parameters = {
        "isActionFromAjax" : true,
        "idUser" : $("#idUserCtx").attr("value")
    };
    $('#accessHistoryTbl').DataTable( {
            language : {
                "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
            },
               "drawCallback": function( settings ) {
            $('[data-toggle="tooltip"]').tooltip()
        },
        "ajax": {
            url : "view/html/modules/accesshistory/AjaxDataAccessHistory.php",
            method : "POST",
            data : parameters
        }
    });
});