<div class="content-wrapper">
  <section class="content-header">
    <h1>
      Historial de Accesso de <b style="text-transform: uppercase;">({$nameUser})</b>
    </h1>
    <ol class="breadcrumb">
      <li><a href="Home"><i class="fa fa-dashboard"></i> Inicio</a></li>
      <li class="active">Administrar usuarios</li>
      <li class="active">Historial de Accesso</li>
    </ol>
  </section>
  <section class="content">
    {$idUserCtx}
    <div class="box">
      <div class="box-header">
          <nav class="navbar navbar-light">
              <div class="container-fluid">
                  <ul class="nav navbar-nav">
                      <li>
                          <a style="cursor: pointer;" id="userAddBtn" name="userAddBtn" onclick="location.href = 'Users' ">
                              <i class="fa fa-arrow-left"></i>
                              Regresar
                          </a>
                      </li>
                      <li>
                          <a style="cursor: pointer;" id="userAddBtn" name="userAddBtn" onclick="location.href = 'Home' ">
                              <i class="fa fa-home"></i>
                              Menu Principal
                          </a>
                      </li>
                  </ul>
              </div>
          </nav>
      </div>
      <div class="box-body">
        <table id="accessHistoryTbl" name="accessHistoryTbl" class="table table-bordered table-striped dt-responsive tables dataTable no-footer dtr-inline" style="width: 100%;" cellspacing="0">
            <thead>
                <tr>
                  <th>Id</th>
                  <th>Fecha Ingreso</th>
                  <th>Fecha Salida</th>
                  <th>Nro Horas</th>
                  <th>Dirección Ip</th>
                  <th>Dispositivo</th>
                  <th>Navegador</th>
                </tr>

            </thead>
        </table>
      </div>
    </div>
  </section>
</div>
<div id="containerResponseAjax"></div>
<script src="view/html/modules/accesshistory/AccessHistory.js?v= {$tempParameter}"></script>
{$jquery}