<?php

require_once(Config::PATH . Config::CONTROLLER . 'general/AccessManager.php');
require_once(Config::PATH . Config::CONTROLLER . 'general/UtilJquery.php');
require_once(Config::PATH . Config::VIEW_HTML . 'general/comps/GeneralLst.php');
require_once(Config::PATH . Config::VIEW_HTML . 'general/comps/GeneralWithDataLst.php');
require_once(Config::PATH . Config::BACKEND . 'modules/PercentCodesVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/PersonVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/PricesCompanyVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/UserVo.php');
require_once(Config::PATH . Config::VIEW_HTML . 'general/comps/GeneralCtx.php');
class ManagersView
{
    private $optionInputForm;
    private $isUpdateUserForm;
    private $idDataInputForm;

    private $utilJQ;
    private $pageTpl, $data, $core;
    /*=============================================
                Data Vars
    =============================================*/
    private $generalDao;
    private $codeCdx;
    private $pricectx;
    private $typepriceLs;


    function __construct($pageTpl, $data, $core)
    {
        $this->data = $data;
        $this->core = $core;
        $this->pageTpl = $pageTpl;

            $this->CreateComponents();
            $this->UseDataFromThePostback();
        
    }

    private function UseDataFromThePostback()
    {
        if (isset($_POST['optionInputForm'])) {
            $this->optionInputForm = $_POST['optionInputForm'];
            $this->idDataInputForm = $_POST['idDataInputForm'];
            $this->generalDao = new GeneralDao();
            switch ($this->optionInputForm) {
                     case "1": {
                    $this->SetData();
                    break;
                }
                case "2": {
                    $this->DeleteData();
                    break;
                }
                case "3": {
                    $this->SetUpdate();
                    break;
                }
                  case "4": {
                    $this->UpdateCompany();
                    break;
                }
            }
        }

        $this->AssignDataToTpl();

    }

    public function UpdateCompany(){
        $pricesVo = new PricesCompanyVo();
        $pricesVo2 = new PricesCompanyVo();
        $pricesVo->isList = true;
        $generalDao = new GeneralDao();
        $generalDao2 = new GeneralDao();
        $generalDao->GetById($generalVo = clone $pricesVo);
        $array = array(1,2 );
         while ($priceVo3 = $generalDao->GetVo($generalVo = clone $pricesVo)) {
            foreach($array as $ar) { 
                  $pricesVo2->id = $priceVo3->id;
                    $pricesVo2->idSearchField = 0;
                    $pricesVo2->tpm = $_POST["pricectx"];
                    $pricesVo2->typeMoney = $_POST["typepriceLs"];
                    $pricesVo2->idUpdateField = $ar;
                    $generalDao2->UpdateByField($generalVo = clone $pricesVo2);
            }

         }
          $message = 'MessageSwalBasic("Modificado!","Se actualizaron todos los proveedores Correctamente","success");';
            $this->utilJQ->AddFunctionJavaScript($message);
    }



      private function SetDataToVo()
    {
        $percentVo = new PercentCodesVo();
        $percentVo->id = $this->idDataInputForm;
        $percentVo->code = $_POST["codeCdx"];
        $percentVo->idPerson = $this->GetIdPersonSession();
        $percentVo->dateCreate = date("Y-m-d");
        return $percentVo;
    }


private function  GetIdPersonSession(){
            $personVo = unserialize($_SESSION['user']["person"]);
            return $personVo->id;

}


    private function SetData()
    {
        $percentVo = $this->SetDataToVo();
        $percentVo->id = null;
        $result = $this->generalDao->Set($generalVo = clone $percentVo);
            $message = 'MessageSwalBasic("Registrado!","Codigo Registrado Correctamente","success");';
            $this->utilJQ->AddFunctionJavaScript($message);
    }

    private function SetUpdate()
    {
        $result = $this->generalDao->Update($generalVo = clone $this->SetDataToVo());
        if (isset($result)) {
            $message = 'MessageSwalBasic("Modificado!","registro Modificado Correctamente","success");';
            $this->utilJQ->AddFunctionJavaScript($message);
        }
    }

    private function DeleteData()
    {
        $message = "";
        $PercentCodesVo = new PercentCodesVo();
        $PercentCodesVo->id = $this->idDataInputForm;
        if ($this->generalDao->Delete($generalVo = clone $PercentCodesVo)) {
            $message = 'MessageSwalBasic("Eliminado!","Registro Eliminado Correctamente","success");';
        } else {
            $message = 'MessageSwalBasic("Error!","Problema con la base de datos","error");';
        }
        $this->utilJQ->AddFunctionJavaScript($message);
    }


    private function CreateComponents()
    {
        $this->codeCdx = new  GeneralCtx("codeCdx","Agregar Codigo de descuentos por porcentaje",null,true);
       $this->pricectx  = new  GeneralCtx("pricectx","Precio para los proveedores",null,true);
       $dataFLETESelection =  array("COP" ,"USD" , "EUR");  
        $this->typepriceLs = new GeneralWithDataLst($dataFLETESelection, "typepriceLs", false,false,"--Tipo de Moneda--",true); 
        $this->utilJQ = new UtilJquery("Impost");
    }
 
    private function AssignDataToTpl()
    {
        $this->data->assign('jquery', $this->utilJQ->PaintJQ());
        $this->data->assign('codeCdx', $this->codeCdx->paint());
         $this->data->assign('pricectx', $this->pricectx->paint());
         $this->data->assign('typepriceLs', $this->typepriceLs->paint());
         $this->data->assign('tempParameter', time());
        $this->SetDataToTpl();
    }   

        private function SetDataToTpl()
    {
        echo $this->core->get($this->pageTpl, $this->data);
    }

 
  


}   