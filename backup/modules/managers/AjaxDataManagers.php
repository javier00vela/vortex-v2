<?php
require_once(__DIR__ . '/../../../../controller/general/Config.php');
require_once(Config::PATH . Config::CONTROLLER.Config::GENERAL . 'ManageArrays.php');
require_once(Config::PATH . Config::BACKEND . 'modules/PercentCodesVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/PersonVo.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralDao.php');

class AjaxDataManagers{

    private $isActionFromAjax;
    private $optionInputForm;
    private $idDataInputForm;

    function __construct()
    {
        if (isset($_POST['isActionFromAjax'])) {
            $this->isActionFromAjax = $_POST['isActionFromAjax'];
            $this->optionInputForm = $_POST['optionInputForm'];
            $this->idDataInputForm = $_POST['idDataInputForm'];

            if ($this->isActionFromAjax == "true") {
                if ($this->optionInputForm == 3) {
                    $this->SetDataInControls();
                }else if($this->optionInputForm == 5){
                $this->GetLengthImpost();
                }else {
                    $this->GetDataInJsonForTbl();
                }
            }
        }
    }

    private function SetDataInControls(){
        $SetInputsDataFJQ = "";
        $PercentCodesVo = new PercentCodesVo();
        $PercentCodesVo->id = $this->idDataInputForm;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $PercentCodesVo);
        if($PercentCodesVo2 = $generalDao->GetVo($generalVo = clone $PercentCodesVo)){
            $SetInputsDataFJQ = "
                            <script>
                               (function($){
                                SetValueToInputText('codeCdx','{$PercentCodesVo2->code}');
                               })(jQuery);
                            </script>
                        ";
        }
        echo $SetInputsDataFJQ;
    }

    public function GetDataInJsonForTbl()
    {
        $existData = false;
        $PercentCodesVo = new PercentCodesVo();
        $PercentCodesVo->isList = true;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $PercentCodesVo);

        $string = "
        {\"data\": [";
        $cont = 1;
        while ($PercentCodesVo2 = $generalDao->GetVo($generalVo = clone $PercentCodesVo)) {
            $existData = true;
            $json = array();

            $json[] = $PercentCodesVo2->id;
            $json[] = htmlspecialchars($PercentCodesVo2->code);
            $json[] = $this->GetPersonQuotation($PercentCodesVo2->idPerson);
            $json[] = $PercentCodesVo2->dateCreate;
            $json[] = "<tip title='Modificar' class='yellow-tooltip' data-toggle='tooltip' data-placement='top'><button id='updateBtn' name='updateBtn' class='btn btn-warning btn-xs' data-toggle='modal' data-target='#productPopUpAdd' title='Modificar' data-dismiss='modal' onclick='UpdateData({$PercentCodesVo2->id},\"managers\",\"managers\");'><i class='fa fa-pencil'></i></button> </tip>" .
            " <tip title='Eliminar' class='red-tooltip' data-toggle='tooltip' data-placement='top'><a id='deleteBtn' name='deleteBtn' class='btn btn-danger  btn-xs' title='Eliminar' onclick='$(\"#productPopUpAdd\").remove();DeleteData({$PercentCodesVo2->id},\"managers\")'><i class='fa fa-times'></i></a></tip> ";


            $string .= json_encode($json) . ",";
            $cont++;
        }
        $string .= "-]}-";

        if($existData){
            echo  str_replace(",-]}-", "]}", $string);
        }else{
            echo "{ \"data\": [] }";
        }
    }

       public function GetLengthImpost(){
        $arrayList = array();
        $personVo = new impostVo();
        $personVo->isList = true;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $personVo);
        while ($personVo2 = $generalDao->GetVo($generalVo = clone $personVo)) {
            $arrayList[] =$personVo2 ;
        }
        print_r( json_encode(count($arrayList)) );
    }

       private  function GetPersonQuotation($idPerson)
    {
        $nameUser="";
         $personVo= new PersonVo();
         $personVo->idSearchField = 0;
         $personVo->id = $idPerson;
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $personVo);
        while ($personVo2 = $generalDao->GetVo($generalVo = clone $personVo)) {
            $nameUser = $personVo2->names. ' '.$personVo2->lastNames ;
        }
        return $nameUser;

    }

    
}

$ajaxDataImpost = new AjaxDataManagers();
