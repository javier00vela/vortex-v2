// jshint unused:true


function LoadTable() {
    var parameters = {
        "isActionFromAjax" : true,
        "optionInputForm" : 0,
        "idDataInputForm" : 0
    };


    $('#codesTbl').DataTable( {
        responsive: true,
           "drawCallback": function( settings ) {
            $('[data-toggle="tooltip"]').tooltip()
        },
        language : {
                "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
            },
        "ajax": {
            url : "view/html/modules/managers/AjaxDataManagers.php",
            method : "POST",
            data : parameters
        }
    });
}

/*********************************CLICK EVENTS**************************/

$(function() {
    LoadTable();

});
