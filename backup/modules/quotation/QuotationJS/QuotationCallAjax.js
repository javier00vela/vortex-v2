/* -----------------------------Cargar Empresas  -------------------------------*/

function LoadData() {
    var con = 0;
    $("#quotationCompanyLst").change(function() {
        $("#quotationCompanyLst option:selected").each(function() {
            //if (GetValueToInputText('isContactsContainerShow') == 0) {
            if (GetValueSelect("quotationCompanyLst") != '') {
               SetIsSubDistirbuidor(GetValueSelect("quotationCompanyLst"));
               // ChangeValueData();
               //SetValueToInputText('isContactsContainerShow', 1);

                $('#contactsContainer').show();
                var parameters = {
                    "isActionFromAjax": true,
                    "optionInputForm": 5,
                    "idDataInputForm": GetValueSelect("quotationCompanyLst")
                };
                $.ajax({
                    url: "view/html/modules/quotation/AjaxDataQuotation.php",
                    type: 'post',
                    data: parameters,
                    success: function(data) {
                        info = $.parseJSON(data);
                        //  console.log(info);
                        if (info.length == null || info.length == 0) {
                            $("#contactsContainer ").html('<a href="#" data-toggle="modal" data-target="#personPopUpAdd"> Agregar Nuevo Contacto </a>' + "<h4><b style='alignment:center'>No hay usuarios registrados en esta empresa.</b></h4>");
                        } else {
                            elementHtml += '<a href="#" data-toggle="modal" data-target="#personPopUpAdd"> Agregar Nuevo Contacto </a>';
                            for (var i = 0; i < info.length; i++) {
                                elementHtml += "<div class='checkbox NoSelected' style='margin-left: 6px;'><label><input type='checkbox' class='" + info[i].idCompany + "' id='" + info[i].id + "' name='checkbox' value='" + info[i].name + "'>" + info[i].name + " - <b>" + info[i].nameRol + "</b>" + "<input type='hidden' name='idCompany' value='" + info[i].idCompany + "'><input type='hidden' name='idUser' value='" + info[i].id + "'></label></div>";
                                elementHtml += "<script>ValidateCheckbox('" + info[i].id + "');</script>";
                            }
                            $("#contactsContainer ").html(elementHtml);
                            elementHtml = "";
                        }
                    }
                })
            } else {
                $("#contactsContainer ").html("");
            }
            //}
        })

    });
}

function ChangeValueData() {
    $("[id*=porcent-]").each(function() {
        if (isSubDistribuidor != 1) {
            $(this).val(0);
        } else {

            $(this).val(20);
        }
    });
    //QueryPrices();
}

function SaveClient() {
    $("#InsertClient").click(function(e) {
        var form = $("#personForm");
        $("#personIdCompanyHidden").attr("value", $("#quotationCompanyLst").val());
        console.log($("#personForm").serializeArray());
        var parameters = {
            "isActionFromAjax": true,
            "optionInputForm": 11,
            "idDataInputForm": 0,
            "form": $("#personForm").serializeArray()
        };
        form.validate({
            messages: {
                required: "El campo es Requerido.",
            },
            invalidHandler: function(event, validator) {
                MessageSwalBasic("Completa los campos !!", "Por favor comprueba todos los campos para agregar un cliente ", "warning");
            }
        });
        console.log(parameters);
        if (form.valid()) {
            $.ajax({
                url: "view/html/modules/quotation/AjaxDataQuotation.php",
                type: "POST",
                data: parameters,
                beforeSend: function() {
                    $("#personPopUpAdd").modal("hide");
                },
                success: function(data) {
                    $("#quotationCompanyLst").trigger("change");
                }
            });
        }


    });
}

function SetIsSubDistirbuidor(id) {
    $("#personIdCompanyHidden").attr("value", $("#quotationCompanyLst").val());
    var parameters = {
        "isActionFromAjax": true,
        "optionInputForm": 12,
        "idDataInputForm": 0,
        "id": id
    };
    //console.log(parameters);
    $.ajax({
        url: "view/html/modules/quotation/AjaxDataQuotation.php",
        type: "POST",
        data: parameters,
        success: function(data) {
            isSubDistribuidor = data;
            if (isSubDistribuidor == 1) {
                $("#alert-subdistribuidor").attr("class", "alert alert-warning");
            } else {
                $("#alert-subdistribuidor").attr("class", "alert alert-warning hidden");
            }

        }
    });
}



/* -----------------------------Obtener Tipo Moneda -------------------------------*/


function GetTypeMoney(index, from, cont) {
    var top = null;
    var parameters = {
        "isActionFromAjax": true,
        "optionInputForm": 8,
        "index": index,
        "from": from
    };
    $.ajax({
        url: "Quotation",
        type: "POST",
        data: parameters,
        beforeSend: function() {
            /*   let timerInterval
                    swal({
                      title: 'Consultando Tasa de Cambio!',
                      html: 'cargando...',
                      timer: 6000,
                      showCancelButton: false, // There won't be any cancel button
                      showConfirmButton: false,
                      allowOutsideClick: false
                    })
    */
        },
        success: function(data) {
            //console.log(data);

            data = data.split("//////////////")[1];
            ////console.log(data);

            if (!data) {
                MessageSwalBasic("UPPS !!", "Problemas al conectar con la Tasa de Cambio, Consulte con el proveedor del servicio o espere más tarde ", "warning");
            } else {
                data = JSON.parse(data);
                ////console.log(data);
            }
            SetDataMoney(data.result.conversion, from);
        }
    });
}


/* -----------------------------Generar Cotizacion -------------------------------*/

function GenerateQuotation() {
    $("#generateQuotation").click(function() {

        var idProducts = [];
        var quotationData = [];
        var totalContacts = [];
        var contactsSelected = [];
        var quantity = [];
        var porcentProducts = [];
        var unityPrice = [];
        var timeDelivery = []
        var idPersonSelected;
        var nodo = [];
        var date = new Date().toLocaleString().replace("/", "-").replace("/", "-");

        idProducts = GetIdProducts();
        quotationData = GetDataQuotation();
        totalContacts = GetDataUsers();
        quantity = GetQuantity();
        porcentProducts = GetPorcentProducts();
        idPersonSelected = GetDataUsersList();
        unityPrice = GetUnityProducts();
        timeDelivery = GetTimeDeliveryProducts();
        nodo = GetNodosProducts();

        if (idProducts.length > 0) {
            if (validateFormulario() != false && validateLista() != false && GetValueToInputText("quotationPaymentLst") != "" && GetValueToInputText("quotationTypeLst") != "" && GetValueToInputText("quotationPaymentLst") != "" && GetValueToInputText("quotationTypeLst") != "" && GetValueToInputText("quotationOfferLst") != "" && GetValueToInputText("quotationUserLst") != "" && GetValueToInputText("quotationPlaceLst") != "") {
                //if (ValidateTypeProduct() != false) {
                if (totalContacts.length > 0 && validateFormulario() != false) {
                    $(".generateQuotation").attr("disabled", "true");
                    $(".generateQuotation").text("cargando...");
                    console.log(timeDelivery);
                    var parameters = {
                        "isActionFromAjax": true,
                        "optionInputForm": 5,
                        "idDataInputForm": 0,
                        "id": idProducts,
                        "quotationData": quotationData,
                        "usersSelected": totalContacts,
                        "idCompany": totalContacts[0].idCompany,
                        "timeDelivery": timeDelivery,
                        "porcentProducts": porcentProducts,
                        "quantity": quantity,
                        "person": idPersonSelected,
                        "prices": unityPrice,
                        "date": date,
                        "typeProduct": ValidateTypeProduct(),
                        "ivaValidate": IVAValidate,
                        "IVAList": IVAArray,
                        "typeQuotationBool": typeQuotationBool,
                        "check": Checkeable,
                        "dateUpdate": $("#dateUpdateCtx").val(),
                        "nodos": nodo
                    };




                    $.ajax ({
                        url: "Quotation",
                        type: 'post',
                        data: parameters,
                        beforeSend: function() {
                            MessageSwalWaiter("cargando... ", "Generando cotización!");
                        },
                        success: function(data) {
                            data = data.split("--split--")[1].split("_")[0];
                            if (!Checkeable) {

                                PrintData(data);
                                $(".generateQuotation").removeAttr("disabled");
                                $(".generateQuotation").text("Generar Cotización");
                            } else {
                                SendMail(data);
                                $(".generateQuotation").removeAttr("disabled");
                                $(".generateQuotation").text("Generar Cotización");
                                MessageSwalBasic("GENERADA !!", "Se ha generado la cotización , sin embargo esta a la espera a ser verificado los productos con una ganancia menor al 20% de esta cotización", "warning");
                                setTimeout(function() {
                                    window.location = "GenerateQuotes";
                                }, 7000);
                            }
                        }

                    });
                } else {
                    MessageSwalBasic("VERIFICA !!", "Debes agregar al menos un contacto ", "warning");
                }
                //      } else {
                //      MessageSwalBasic("VERIFICA !!", "Debes verificar si los productos son consumibles o equipos y/o servicios ", "warning");
                //    }

            } else {
                MessageSwalBasic("VERIFICA !!", "Debes completar todos los campos ", "warning");
            }
        } else {
            MessageSwalBasic("VERIFICA !!", "Debes agregar al menos un producto ", "warning");
        }

    });
}

function PrintData(id) {
    var parameters = {
        "isActionFromAjax": true,
        "optionInputForm": 3,
        "idDataInputForm": 0,
        "id": id
    }

    $.ajax ({
        url: "GenerateQuotes",
        type: 'post',
        data: parameters,

        beforeSend: function() {

            setTimeout(MessageSwalWaiter('cargando...', 'Abriendo cotización!'), 10000);
        },
        success: function(data) {
            data = data.split("--split--")[1];
            var windowOpen = window.open("view/docs/pdf/" + data + ".pdf?v=" + Math.random(), '_blank');
            if (windowOpen) {
                windowOpen.focus();
                //DeletePDFQuotation(data);
                MessageSwalRedirect("GENERADO !!", "Se ha abierto el documento de cotización ", "success", "Quotation");

            } else {
                MessageSwalBasic("VERIFICAR !!", "Debes habilitar permisos para abrir ventanas emergentes", "warning");
            }
        }

    });
}

/* -----------------------------Cargar Codigo Cotización -------------------------------*/

function LoadValueQuotation() {
    var parameters = {
        "isActionFromAjax": true,
        "optionInputForm": 4,
        "idDataInputForm": 0
    };

    $.ajax({
        url: "view/html/modules/quotation/AjaxDataQuotation.php",
        type: 'post',
        data: parameters,
        success: function(data) {
            //  //console.log(data);
            $("#code-quotation").val(data);

        }
    })

}

function VerifyCode(code, id, idPercent) {
    var parameters = {
        "isActionFromAjax": true,
        "optionInputForm": 9,
        "idDataInputForm": 0,
        "code": code.value
    };

    $.ajax({
        url: "view/html/modules/quotation/AjaxDataQuotation.php",
        type: 'post',
        data: parameters,
        success: function(data) {
            if (data == 0) {
                $("#" + id).val("20");
                ChangePercent(idPercent);
                MessageSwalBasic("Codigo Incorrecto!!", "verifique el codigo", "error");
            }

            if (data == 1) {
                MessageSwalBasic("Codigo Correcto!!", "se ha verificado el codigo", "success");
            }

            if (data == 2) {
                $("#" + id).val("20");
                var e = jQuery.Event("keydown");
                e.which = 50; // # Some key code value           
                $("#" + idPercent).trigger(e);
            }

        }
    });

}

/* -----------------------------Cargar Iva -------------------------------*/
function LoadIVA() {
    var parameters = {
        "isActionFromAjax": true,
        "optionInputForm": 7,
        "idDataInputForm": 0,
    };

    $.ajax({
        url: "view/html/modules/quotation/AjaxDataQuotation.php",
        type: 'post',
        data: parameters,
        success: function(data) {
            //console.log(data);
            SetIVA(data);
        }
    })
}


/* -----------------------------Cargar Tabla Inventario -------------------------------*/

function LoadTable() {

    var parameters = {
        "isActionFromAjax": true,
        "optionInputForm": 0,
        "idDataInputForm": 0
    };


    tbl = $('#quotationTbl').DataTable({
        "columnDefs": [{
                "targets": [5, 6, 8],
                "visible": false,
                "searchable": false
            },
            {
                "targets": [5, 6, 8],
                "visible": false
            }
        ],
        responsive: true,
        "drawCallback": function(settings) {
            $('[data-toggle="tooltip"]').tooltip()
        },
        language: {
            "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
        },
        "ajax": {
            url: "view/html/modules/quotation/AjaxDataQuotation.php",
            method: "POST",
            data: parameters
        }
    });
}



function SendMail(id) {
    var parameters = {
        "isActionFromAjax": "true",
        "optionInputForm": 9,
        "idDataInputForm": 0,
        "id": id
    }
    $.ajax ({
        url: "Quotation",
        type: 'post',
        data: parameters,

        success: function(data) {
            //console.log(data);
        }
    });
}

function LoadDataImpost() {
    var parameters = {
        "isActionFromAjax": true,
        "optionInputForm": 6,
        "idDataInputForm": 0
    };

    $.ajax({
        url: "view/html/modules/quotation/AjaxDataQuotation.php",
        type: 'post',
        data: parameters,
        success: function(data) {
            $("#impostContainer").append(data);

        }
    })

}


function AlterTableAjaxInventory(ind) {
    $("#wordKeyBtn").attr("disabled", "true");
    var parameters = {
        "isActionFromAjax": true,
        "optionInputForm": 8,
        "idDataInputForm": 0,
        "range": ind
    };
    tbl.destroy();
    tbl = $('#quotationTbl').DataTable({
        "columnDefs": [{
                "targets": [5, 6, 8],
                "visible": false,
                "searchable": false
            },
            {
                "targets": [5, 6, 8],
                "visible": false
            }
        ],
        responsive: true,
        "drawCallback": function(settings) {
            $('[data-toggle="tooltip"]').tooltip()
        },
        language: {
            "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
        },
        "ajax": {
            url: "view/html/modules/quotation/AjaxDataQuotation.php",
            method: "POST",
            data: parameters,

        }

    });
    $("#wordKeyBtn").removeAttr("disabled");
    $("html").css({ 'overflow': 'scroll' });
}


function DeletePDFQuotation(id) {
    var parameters = {
        "isActionFromAjax": "true",
        "optionInputForm": 6,
        "idDataInputForm": 0,
        "id": id
    }
    $.ajax ({
        url: "Quotation",
        type: 'post',
        data: parameters,

        success: function(data) {
            //console.log(data);
        }
    });
}