var arrayPricesCompany = [];
var arrayAditionalsImpost = [];
var priceTotal = 0;

var __precios = {
    queries: {
        //recibe lugard e producto y nombre compañia
        GetNewPriceUnitInternational: function(type, priceNormal, idCompany, typeQuotation, dataProduct, callBack) {
            // console.log(idCompany+"esta es la empresa de cotizaciones .... !!!!!!");
            var parameters = { "isActionFromAjax": true, "optionInputForm": 2, "idDataInputForm": idCompany };
            $.ajax({
                url: "view/html/modules/quotation/AjaxDataQuotation.php",
                type: 'post',
                data: parameters,
                success: function(data) {

                    //data = JSON.parse(data);
                    info = $.parseJSON(data);
                    arrayPricesCompany = info[0];
                    console.log(dataProduct);
                    arrayAditionalsImpost = info[1];

                    if (type == "Internacional") {
                        //console.log("-------------- es un producto internacional ---------------")
                        priceTotalVenta = __precios.GetCalculePrice(priceNormal, arrayAditionalsImpost, arrayPricesCompany, typeQuotation);


                        //console.log("precio total para vender "+priceTotalVenta);
                        priceTotalVentaCOP = __precios.GetPriceSaleCOP(priceTotalVenta, arrayPricesCompany[0].tpm, arrayPricesCompany[0].ganancia);
                        //console.log(priceTotalVentaCOP);
                        //console.log("precio total para vender en pesos colombianos "+priceTotalVentaCOP);

                    } else {
                        if (dataProduct["moneda"] == "USD" || dataProduct["moneda"] == "EUR") {
                            priceTotalVenta = priceNormal;

                            //console.log("precio total para vender "+priceTotalVenta);
                            priceTotalVentaCOP = priceNormal;

                        } else {
                            //console.log("--------------es un producto nacional---------------")
                            //console.log("precio normal = "+priceNormal)

                            priceTotalVenta = priceNormal;
                            //console.log("precio total para vender "+priceTotalVenta);
                            priceTotalVentaCOP = priceNormal;

                        }
                        //console.log("precio total para vender en pesos colombianos "+priceTotalVentaCOP);
                        console.log(priceTotalVentaCOP);
                    }

                    callBack(priceTotalVenta, priceTotalVentaCOP);
                }
            });

            return priceTotal;
        },
    },

    GetCalculePrice: function(priceNormal, arrayAditionalsImpost, arrayPricesCompany, typeQuotation) {
        impost = __precios.GetPriceWithImpost(priceNormal, arrayAditionalsImpost, typeQuotation);
        // hasta aqui se suman bien el precio
        priceTotalVenta = __precios.GetPriceSale(priceNormal, arrayPricesCompany[0].ganancia, impost); //se le suma la ganancia propuesta por el proveedor 
        //console.log("precio total con ganancia --> "+priceTotalVenta);
        return priceTotalVenta;
    },

    GetPriceWithImpost: function(price, porcentajes, typeQuotation) {

        totalPercent = 0;
        porcentaje = 0;
        for (i = 0; i < porcentajes.length; i++) {

            if (porcentajes[i][typeQuotation] == 1) {

                porcentaje += __precios.GetPercent(parseFloat(porcentajes[i].percent.toString().replace(",", ".")));
            }

        }

        return porcentaje;
    },

    GetPercent: function(percent) {
        percent2 = "";
        //console.log("porcentaje ingresado : "+percent);
        percent2 = (percent / 100);

        return percent2;
    },

    GetPriceSale: function(price, sale, impost) {
        //console.log("================================= se va a calcular el impuesto y el porcentaje ===================================");
        sale = __precios.GetPercent(sale);
        sale = impost + sale;
        //console.log("la suma del impuesto y el procentaje es ====== "+sale);
        price = price + (price * sale);
        //console.log("se obtiene el precio total con ganancia apartir de esta formulala  "+price+"+"+"("+price+"*"+sale+")");
        //console.log("el resultado obtenido es "+price);
        return price;
    },
    GetPriceSaleCOP: function(price, tpm, sale) {
        //console.log("tpm esta aqui ="+tpm);
        //console.log("================================= con el precio total se hace la conversion a precio colombiano ===================================");
        console.log("-- datos ingresados -- price :  " + price + " tpm " + tpm + " sale :" + sale)
        sale = __precios.GetPercent(sale);

        //console.log(price+" es este el precio normal");
        console.log("formula para obtener precio = " + price + "+(" + price + "*" + sale + ")");
        //console.log("retorna multiplicnado por la tasa :"+price+"*"+tpm);
        return price * tpm;
    },
    RemplazarCaracteres: function(text, busca, reemplaza) {
        while (text.toString().indexOf(busca) != -1) {
            text = text.toString().replace(busca, reemplaza);
        }
        return text;
    }
};
//__precios.GetNewPriceUnitInternational("$15,000","SQL server");