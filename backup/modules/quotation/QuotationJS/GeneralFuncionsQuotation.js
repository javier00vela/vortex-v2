var tbl;
var IVA;
var total = 0;
var info = [];
var elementHtml = "";
var typeMoneys = [];
var IVAValidate = "true";
var IVAArray = [];
var totalIVA = 0;
var money = [];
var listClonePLaces = $("#quotationPlaceLst").clone().wrap("<div />").html();
var typeQuotationBool = true;
var listExist = [];
var isSubDistribuidor = " ";
var Checkeable = 0;
var send = [];
var formatter = new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'USD',
    minimumFractionDigits: 3
})

/* -----------------------------Seteados en cotización -------------------------------*/


function SetIVA(data) { // setear la variable IVA 
    if (data) {
        IVA = data;
    } else {
        IVA = 19;
    }
}


function CompleteOptionLst(id) { // setear la lista de tiempo de entrega
    var data = [5, 15, 30, 45, 60, 90, 120, 150, 180];
    var text = " ";
    text = "<option value='--Tiempo de Entrega--'> --Tiempo de Entrega-- </option>";

    for (var i = 0; i < data.length; i++) {
        text += "<option value='" + data[i] + "'>" + data[i] + " días</option>";
    }

    $("#timeDelivery-" + id).html(text);
}


function SetListMoneys() { // setear la lista de monedas 
    var data = 0;
    var temp = [];
    var arrayTemp = ["cop", "usd", "eur"];
    for (var i = 0; i < 3; i++) {
        data = GetTypeMoney("full", arrayTemp[i], i);
    }

}

function SetDataInventory() {
    SetValueToInputText("productNameCtx", "licuadora");
    SetValueToInputText("quantityCtx", "1");
    SetValueToInputText("unitePriceCtx", "100");

    $("#quantityCtx").focus(function() {
        CalculatePrice();
    });
    $("#quantityCtx").focusout(function() {
        CalculatePrice();
    });
    $("#quantityCtx").hover(function() {
        CalculatePrice();
    });

}

function SetDataMoney(data, from) { // setear los datos de monedas
    var arrayTemp = ["COP", "USD", "EUR"];
    for (var i = 0; i < data.length; i++) {
        if (data[i].to == "USD") {
            typeMoneys[from + "-usd"] = { "from": from, "data": data[i] };
        }
        if (data[i].to == "COP") {
            typeMoneys[from + "-cop"] = { "from": from, "data": data[i] };
        }
        if (data[i].to == "EUR") {
            typeMoneys[from + "-eur"] = { "from": from, "data": data[i] };
        }


    }


}


function SetResponsive(tbl) { // alterar boton de agregar cuando se vuelve responsive la tabla
    var values = [];
    var rows = [];
    var id = [];
    $(window).resize(function() {
        var count = 0;

        $("[id*=product-id]").each(function() {

            id[count] = $(this).val();
            rows[count] = tbl.row($(this).val()).data();
            count++;
        });
        for (var i = 0; i < count; i++) {
            values[i] = rows[i][11].toString();
            values[i] = values[i].replace("none", " disabled ");
        }

        for (var i = 0; i < count; i++) {
            rows[i][11] = values[i];
            tbl.row(id[i]).data(rows[i]).draw();
        }

    });
}


/* -----------------------------Geteados en cotización -------------------------------*/


function GetNodosProducts() {
    var List = []
    $("[name=isDiv]").each(function() {
        $(this).each(function(val) {
            List.push($(this)[val].innerHTML);
        });
        // List.push($(this).attr("value"));
    });
    return List;
}

function GetUnityProducts() { // traer lista  precio por unidad 
    var Price = [];

    $("[id*=priceUnitarioCOP-]").each(function() {
        Price.push($(this).attr("value"));
    });
    return Price;
}

function GetDataUsersList() { // traer lista de usuarios responsables
    var idPersonSelected;
    $("#quotationUserLst option:selected").each(function() {
        idPersonSelected = $(this).val();
    });

    return idPersonSelected;
}

function GetIdProducts() { // traer lista de id de productos
    var idProducts = [];

    $("[data-id-producto*=id-]").each(function() {
        idProducts.push($(this).attr("data-id-producto").split("id-")[1]);
    });
    return idProducts;
}

function GetQuantity() { // traer lista de cantidades de productos
    var quantity = [];
    $("[name*=quantityCtx]").each(function() {
        quantity.push($(this).val());
    });

    return quantity;
}

function GetPorcentProducts() {
    var porcents = [];
    $("[name*=porcentCtx]").each(function() {
        porcents.push($(this).val());
    });

    return porcents;
}

function GetTimeDeliveryProducts() { // traer lista de tiempo de entrega de productos
    var timeDelivery = [];
    $("[name*=timeDelivery]").each(function() {
        timeDelivery.push($(this).val());
    });
    return timeDelivery;
}

function GetDataQuotation() { // traer todos los datos de la cotizaon
    var quotationData = [];
    quotationData.push((new Date().toLocaleString()).replace("/", "-").replace("/", "-"));
    quotationData.push(GetValueToInputText("code-quotation"));
    quotationData.push(GetValueToInputText("status-quotation"));
    quotationData.push(GetValueToInputText("porcent")); // porcent
    quotationData.push(remplazarCaracteres($("#totalPrice").val().split("$")[1], ",", ""));
    quotationData.push(GetValueToInputText("user-id"));
    quotationData.push($("[name=checkboxTypePrice]:checked").val());
    quotationData.push(GetValueToInputText("idQuotation"));
    quotationData.push(GetValueToInputText("quotationOfferLst")); //validez de la oferta
    quotationData.push(GetValueToInputText("quotationPaymentLst")); //forma de pago
    quotationData.push(GetValueToInputText("quotationTypeLst")); //tipo cotizacion
    quotationData.push(GetValueToInputText("quotationPlaceLst")); //lugar de entrega
    quotationData.push(GetValueToInputText("versionQuotation")); //version cotizacion
    quotationData.push(GetValueToInputText("code"));
    return quotationData;

}

function GetDataUsers() { // traer lista de contactos de empresa
    var totalContacts = [];
    var contactsSelected = [];
    $("#contactsContainer input[type='checkbox']").each(function() {
        if ($(this).parent().parent().hasClass("Selected")) {
            contactsSelected["idCompany"] = $(this).attr("class");
            contactsSelected["idUser"] = $(this).attr("id");
            contactsSelected["name"] = $(this).val();
            totalContacts.push({ "name": contactsSelected["name"], "idUser": contactsSelected["idUser"], "idCompany": contactsSelected["idCompany"] });
        }
    });
    return totalContacts;
}

function quotationCompanyLst() {
    $("#quotationCompanyLst").select2({
        placeholder: "-- Empresa a cotizar--"
    });
}s


/* -----------------------------Validaciones en cotización -------------------------------*/

function validateFormulario() { // valida si todos los campos estan selecionados con datos

    $(".box-body select").each(function() {
        if ($(this).val() == "" || $(this).val() == null) {
            $(this).css("border-style", "solid");
            $(this).css("border-color", "red");
            return false;
        } else {
            $(this).css("border-color", "#d2d6de");
        }
    });

    return true;
}

function validateLista() { // valida si todas las listas estan seleccionadas
    var validate = true;
    $("[name*=timeDelivery]").each(function() {
        if ($(this).val() == "--Tiempo de Entrega--") {
            $(this).css("border-style", "solid");
            $(this).css("border-color", "red");

            validate = false;
            return validate;
        }
    });

    return validate;
}

function ValidateTypeProduct() { // valida los tipos de productos que hay ya agregados
    var consumible = 0;
    var otro = 0;
    var type = [];
    $("[name*=totalPriceCtx]").each(function() {
        if ($(this).attr("data-typeproduct") == "Consumibles" || $(this).attr("data-typeproduct") == "consumibles") {

            consumible += 1;
        } else {
            otro += 1;
        }
    });
    if (consumible > 0 && otro > 0) {
        return false;
    } else {
        if (consumible > 0) {
            return "Consumibles";
        } else {
            return "Otro";
        }
    }
}


function ValidateCheckbox(idCheck) { // valida las check y cambia si clase

    $("#" + idCheck).click(function() {
        if ($('#' + idCheck).is(':checked')) {

            $("#" + idCheck).parent().parent().removeClass("NoSelected");
            $("#" + idCheck).parent().parent().addClass("Selected");

        } else {

            $("#" + idCheck).parent().parent().removeClass("Selected");
            $("#" + idCheck).parent().parent().addClass("NoSelected");

        }
    });
}


function ValidateNumberItem(type, id) { // valida el numero del item
    var item = [];
    var count = 0;
    switch (type) {
        case 1:
            $("[id*=content-]").each(function() {
                item[count] = $(this).attr("data-row-index");
                count++;
            });
            if (count < 2) {
                $("#productContainer").attr("style", " ");
            }
            return item.length + 1;
            break;
        case 2:
            count = 1;
            $("[id*=item-value-]").each(function() {
                var value = $(this).attr("id");
                $("#" + value + " b").html(count);

                count++;
            });
            if (count < 2) {
                $("#productContainer").attr("style", " ");
            }
            return;
            break;

    }
}


function validateSizeContainerProducts() {
    size = 0;
    size = $("#productContainer").height();


}



/* -----------------------------Otras Funciones en cotización -------------------------------*/

function GetTypeQuotation() {
    return GetValueToInputText("quotationTypeLst");
}

function ValidateProductData(data) {
    if (data["precioUnitario"] == 0) {
        MessageSwalBasic("VERIFICA !!", "El valor del producto debe ser mayor a $0 ", "warning");
        return false;
    } else if (GetValueToInputText("quotationTypeLst") == "") {
        MessageSwalBasic("VERIFICA !!", "Debes seleccionar el tipo de cotización para los calculos necesarios ", "warning");
        return false;
    }
    return true;
}

function AddPricesToData(precioUnitario, precioUnitarioCOP, data) {
    //console.log("+++" + precioUnitario, precioUnitarioCOP, data);
   // alert(data["moneda"]);
    // alert(data["lugarDeProducto"]);
    if(data["moneda"] == "EUR" || data["moneda"] == "USD" && data["lugarDeProducto"] == "Nacional" ){
      data["precioUnitario"] = data["precioColombiano"];
      //alert( data["precioUnitario"]);
    }else{
      data["precioUnitario"] = precioUnitario;
    }
    if(GetMoneyChecked() == "COP"){
        data["precioUnitarioCOP"] = precioUnitarioCOP;
    }else{
        if(GetMoneyChecked() != "COP" && data["lugarDeProducto"] == "Internacional"){
            data["precioUnitarioCOP"] = precioUnitarioCOP ;
        }else{
            data["precioUnitarioCOP"] = data["precioColombiano"];
        }
    }
    data["precioVenta"] = precioUnitario;
    return data;
}


function PrepareData(data) {
    dataNueva = [];

    dataNueva["referencia"] = data[0];
    dataNueva["nombre"] = data[1];
    dataNueva["presentacion"] = data[2];
    dataNueva["marca"] = data[3];
    dataNueva["cantidad"] = parseInt(data[11].split("<b>")[1].split("</b>")[0]); //se aplica el split para separar las et html y obtener la cantidad
    dataNueva["precioColombiano"] = data[4];
    dataNueva["precioUnitario"] = CleanNumber(data[5], "full");
    dataNueva["precioOriginal"] = CleanNumber(data[5], "full");
    dataNueva["moneda"] = data[6];
    dataNueva["tipoDeProducto"] = data[7];
    dataNueva["id"] = data[8];
    dataNueva["lugarDeProducto"] = data[9];
    dataNueva["proveedor"] = data[10];
    dataNueva["IVA"] = data[13];
    dataNueva["objBoton"] = data[14];
    dataNueva["index"] = data[20];
    return dataNueva;
}

function GetDataOfTable(obj) {
    if ($(window).width() > 2500) {
        data = tbl.row(obj.parents('tr')).data();
        data[20] = tbl.row(obj.parents('tr')).index();
    } else {
        data = tbl.row(obj).data();
        data[20] = tbl.row(obj).index();
    }
    return PrepareData(data);
}




function AddProduct() {
    $('#quotationTbl tbody').on('click', 'button', function() {

        data = GetDataOfTable($(this));
        if (listExist.indexOf(data["id"]) < 0) {
            if (isSubDistribuidor != " ") {
                if (ValidateProductData(data)) {
                    $(this).attr('disabled', true);
                    //console.log(data);
                    __precios.queries.GetNewPriceUnitInternational(data["lugarDeProducto"], data["precioUnitario"], data["proveedor"], GetTypeQuotation(), data, function(precioUnitario, precioUnitarioCOP) {

                        data = AddPricesToData(precioUnitario, precioUnitarioCOP, data);

                        data["precioUnitario"] = ConvertPriceToSelectedCurrency(data["precioUnitario"], data, 1); //cambiAr a tipo de moneda seleccionada

                        CreateContentProduct(data);

                        totalPorcent = GetProductPercentage(data["precioUnitario"], data["id"]); //devuelve la ganancia del producto

                        totalPrice = GetFullPriceOfProduct(data["precioUnitario"], totalPorcent); //devuelve el total de producto con la ganancia

                        SetFullPriceOfProduct(data["id"], totalPrice); // pintamos el total del producto en el input

                        TotalCountProducts(totalPrice, "+"); //calculamos el total de la cotización
                        listExist.push(data["id"])
                            // QueryPrices();
                    });
                }
            } else {
                MessageSwalBasic("VERIFICA !!", "Debes Seleccionar una empresa ", "warning");
            }
        } else {
            MessageSwalBasic("VERIFICA !!", "Este Producto ya se encuentra en la cotización ", "warning");
        }
    });
}

function QueryPrices() {
    $("[name*=totalPriceCtx]").each(function() {
        dataProduct = GetDataProduct($(this));

        __precios.queries.GetNewPriceUnitInternational(dataProduct["lugarDeProducto"], dataProduct["precioOriginal"], dataProduct["proveedor"], GetTypeQuotation(), dataProduct, function(precioUnitario, precioUnitarioCOP) {
            //console.log("precio unitario " + precioUnitario + " precio colombiano " + precioUnitarioCOP);
            dataProduct = AddPricesToData(precioUnitario, precioUnitarioCOP, dataProduct);
            // alert(precioUnitarioCOP);
            precioUnitario = ConvertPriceToSelectedCurrency(precioUnitario, dataProduct , 1); //cambiAr a tipo de moneda seleccionada

            if (dataProduct["lugarDeProducto"] == "Nacional") {
                $("#priceUnitarioCOP-" + dataProduct["id"]).attr("value", precioUnitario);

            } else {
                $("#priceUnitarioCOP-" + dataProduct["id"]).attr("value", precioUnitarioCOP);
            }
            $("#unitePriceCtx-" + dataProduct["id"]).attr("data-price", precioUnitario);

            CountquantityCtx(dataProduct["id"]);

            $("#priceUnitarioCOP-" + dataProduct["id"]).attr("value", precioUnitario);

        });
    });
}

function CreateContentProduct(data) {

    var value = ValidateNumberItem(1, null); //validar Numeración de los productos
    $("#productContainer").prepend('<div class="box box-info" name="isDiv"  id=' + data["id"] + '><div class="box-header with-border"><h3 class="box-title " id="item-value-' + data["id"] + '" data-id-producto="' + "id-" + data["id"] + '"><b>' + value + '</b> - ' + data["nombre"] + ' <br><p class="text-center"><b>' + data["presentacion"] + '</b><p></h3></div><form class="form-horizontal"><div class="box-body"><input type="hidden" id="product-id" value="' + data["index"] + '"><input type="hidden" id="priceUnitario-' + data["id"] + '" value="' + ((data["moneda"] == "EUR" || data["moneda"] == "USD" && data["lugarDeProducto"] == "Nacional" ) ?data["precioColombiano"]  : data["precioUnitario"])+ '"><input type="hidden" id="priceUnitarioCOP-' + data["id"] + '" value="' + ((data["moneda"] == "EUR" || data["moneda"] == "USD" && data["lugarDeProducto"] == "Nacional" ) ?data["precioColombiano"]  : data["precioUnitarioCOP"]) + '"><input type="hidden" id="precioColombiano-' + data["id"] + '" value="' + data["precioColombiano"] + '"><input type="hidden" id="type-' + data["id"] + '" value="' + data["lugarDeProducto"] + '"><div class="row"><div class="col-sm-12"><div style="border-radius:5px;margin:5px;" class=" bg-' + ((data["IVA"] == "SI") ? "null" : "orange") + ' text-center"> <input type="checkbox" name="checkbox" style="margin-top: 5px" class="hidden checked" id="IVA-' + data["id"] + '" data-iva="' + data["id"] + '" disabled  onclick="OnClickIVA(' + data["id"] + ')"  ' + ((data["IVA"] == "SI") ? "checked data-check='true'" + IVAArray.push(data["id"]) : "unchecked data-check='false' <label><i class='fa fa-warning'></i> Este producto No Aplica el IVA </label>") + ' </div></div></div><div class="row" data-row-index="' + data["index"] + '" id="content-' + data["id"] + '" id="contentProductQxt"><div class="col-sm-6 col-xs-12"><label for="">Cantidad</label> <div class="input-group"><span class="input-group-addon"><i class="fa fa-cube"></i></span><input id="quantityCtx-' + data["id"] + '" name="quantityCtx" type="number" min="0"  onchange="return CountquantityCtx(' + data["id"] + ')" class="form-control" value="1"></div></div><input  id="unitePriceCtx-' + data["id"] + '" name="unitePriceCtx" type="hidden" class="form-control" data-price="' + remplazarCaracteres(data["precioUnitarioCOP"], ",", "") + '" value="100"><input  id="precioOriginalCtx-' + data["id"] + '" name="precioOriginalCtx" type="hidden" class="form-control" data-price="' + data["precioOriginal"] + '" value="100"><input  id="precioVentaCtx-' + data["id"] + '" name="precioVentaCtx" type="hidden" class="form-control" data-price="' + data["precioVenta"] + '"><div class="col-sm-6 col-xs-12"><label for="">Ganancia</label> <div class="input-group"><span class="input-group-addon"><i class="  fa fa-bookmark-o"></i></span><input id="porcent-' + data["id"] + '" name="porcentCtx"  type="number" min="0"  class="form-control" data="' + data["id"] + '" onchange="ValidateChangePercents(' + data["id"] + ')" value="' + ((isSubDistribuidor == 1) ? 0 : 20) + '"></div></div></div><br><div class="row" data-row-index="' + data["index"] + '"  id="contentProductQxt"><input type="hidden" name ="precioUnitarioConGanancia" id="precioUnitarioConGanancia-' + data["id"] + '" value="' + GetPriceUnitWithPercent(data["precioUnitario"], data["id"], true) + '"><div class="col-sm-6 col-xs-12"><label for="">Tiempo Entrega</label> <div class="input-group"><span class="input-group-addon"><i class="fa fa-history"></i></span><select name="timeDelivery" id="timeDelivery-' + data["id"] + '" data="' + data["id"] + '" class="form-control"><option>--Tiempo de Entrega--</option></select></div></div><div class="col-sm-6 col-xs-12"><label for="">Precio</label> <div class="input-group"><span class="input-group-addon"><i class="fa fa-dollar"></i></span><input id="totalPriceCtx-' + data["id"] + '" valor-modificable='+toString(Math.round(CleanNumber(data["precioUnitario"], ","))).replace(/,/g, ".")+' name="totalPriceCtx-' + data["id"] + '" data-type="' + $("[name=checkboxTypePrice]:checked").val() + '" data-value="' + toString(CleanNumber(data["precioUnitario"], ",")).replace(/,/g, ".") + '" data-place="' + data["lugarDeProducto"] + '" data-money="' + data["moneda"] + '" data-typeProduct="' + data["tipoDeProducto"] + '" data-id="' + data["id"] + '" data-proveedor="' + data["proveedor"] + '" data-priceU="' + toString(CleanNumber(data["precioUnitario"], ",")).replace(/,/g, ".") + '"   type="text" class="form-control"  disabled LOL value="' + toString(Math.round(CleanNumber(data["precioUnitario"], ","))).replace(/,/g, ".") + '"></div></div></div></div><div class="box-footer"><button  type="button" onclick="DeleteProduct(' + data["id"] + ')" class="btn btn-danger btn-xs-block">Cancelar</button></div></form></div>');
    if (data["cantidad"] == 0) {
        CompleteOptionLst(data["id"]); //completa las opciones del desplegable de tiempo de entrega
        return;
    }
    SetOptionTimeDelivery(data["id"]);

    validateSizeContainerProducts(value); //valida el contenedorde los productos
    //obj.attr('class','btn btn-primary btn-xs disabled');


}

function GetProductPercentage(precioUnitario, id, condicion = false) {
    if (!condicion) {
        porcentaje = SumPercent(id); //divide por 100 la ganancia
    } else {
        porcentaje = 0.20;
    }
    return precioUnitario * parseFloat(porcentaje); //se obtiene la ganancia del producto
}

function GetFullPriceOfProduct(precioUnitario, ganancia) {
    return precioUnitario + parseFloat(ganancia);
}

function SetFullPriceOfProduct(id, precioFull) {
    if ($("[name=checkboxTypePrice]:checked").val() == "COP") {
        $("#totalPriceCtx-" + id).attr("valor-modificable", "$" + ConvertNumber(Math.round(precioFull)));
        $("#totalPriceCtx-" + id).attr("value", "$" + ConvertNumber(Math.round(precioFull)));
    } else {
        // console.log(formatter.format(precioFull));
        $("#totalPriceCtx-" + id).attr("valor-modificable", "$" + (formatter.format(precioFull).split("$")[1]).toString().replace(".", ","));
        $("#totalPriceCtx-" + id).attr("value", "$" + (formatter.format(precioFull).split("$")[1]).toString().replace(".", ",").slice(0, -1));
    }
    $("#totalPriceCtx-" + id).attr("data-value", precioFull);
}

function validateSizeContainerProducts(value) {
    if (value >= 3) {
        $("#productContainer").css("height", "500px");
        $("#productContainer").css("overflow", "scroll");
    }
}

function ConvertPriceToSelectedCurrency(price, data , op) { //type => moneda del producto, convert => moenda seleccionada
    convert = GetMoneyChecked();
    //alert(data["precioUnitarioCOP"]);
    if (data["lugarDeProducto"] == "Internacional" && convert == "COP") {
     
        var result = data["precioUnitarioCOP"];

    } else {

        if (data["lugarDeProducto"] == "Internacional") {
            //console.log(data["precioUnitarioCOP"] + "moneda internacional");
            var result = GetTypePrice(data["precioUnitarioCOP"], data["moneda"], convert);
        } else {
            //alert("tipo : " + data["moneda"] + "  convert : " + convert);
            if (data["moneda"] == "COP") {
                var result = GetTypePriceNacionalCOP(price, data["moneda"], convert);
            } else if (data["moneda"] == "USD") {
                var result = GetTypePriceNacionalOther(data["precioUnitarioCOP"], data["moneda"], convert , op);
            } else {
                var result = GetTypePriceNacionalOther(data["precioUnitarioCOP"], data["moneda"], convert , op);
            }
            if (convert == "COP" && data["moneda"] == "COP") {
                var result = data["precioUnitarioCOP"];
            }
        }


    }
    return result;
}

function GetTypePriceNacionalCOP(price, type, convert) {
    if (type == "COP") {
        if (convert == "USD") {
            price = price / typeMoneys["cop-usd"].data.rate;
        } else if (convert == "EUR") {
            price = price / typeMoneys["cop-eur"].data.rate;
        }
    } else if (type == "EUR") {

        if (convert == "USD") {

            price = price * typeMoneys["eur-usd"].data.rate;

        } else if (convert == "COP") {
            price = price * typeMoneys["eur-cop"].data.rate;
        } else {

            price = price * typeMoneys["usd-eur"].data.rate;
        }
    } else if (type == "USD") {

        if (convert == "EUR") {

            price = price * typeMoneys["usd-eur"].data.rate;
        } else if (convert == "COP") {

            price = price * typeMoneys["usd-cop"].data.rate;
        } else if (convert == "USD") {
            //console.log(typeMoneys);
            price = price * typeMoneys["usd-cop"].data.rate;
        }
    }
    return price;
}

function GetTypePriceNacionalOther(price, type, convert , op) {

    if (type == "COP") {
        if (convert == "USD") {
            price = price / typeMoneys["cop-usd"].data.rate;
        } else if (convert == "EUR") {
            price = price / typeMoneys["cop-eur"].data.rate;
        }
    } else if (type == "EUR") {

        if (convert == "USD") {
          
                price = price / typeMoneys["eur-usd"].data.rate;
            

        } else if (convert == "COP") {
            if(op == 1){
                price = price * typeMoneys["eur-cop"].data.rate;
            }
        } else {

            price = price / typeMoneys["usd-eur"].data.rate;
        }
    } else if (type == "USD") {

        if (convert == "EUR") {
            price = price / typeMoneys["usd-eur"].data.rate;
        } else if (convert == "COP") {
            
            if(op == 1){
                //alert("actualizar de usd a cop");
                price = price * typeMoneys["usd-cop"].data.rate;
            }

        } else if (convert == "USD") {
            // console.log(typeMoneys);
            price = price / typeMoneys["cop-usd"].data.rate;
        }
    }
    return price;
}

// type es la moneda del prodcuto y convert la moneda seleccionada de la cotizacion
function GetTypePrice(price, type, convert) {
    //console.log("@" + price);
    //console.log("@" + typeMoneys["usd-cop"]);

    if (type == "COP") {

        if (convert == "USD") {
            price = price / typeMoneys["cop-usd"].data.rate;
        } else if (convert == "EUR") {
            price = price / typeMoneys["cop-eur"].data.rate;
        }
    } else if (type == "EUR") {

        if (convert == "USD") {

            price = price / typeMoneys["eur-usd"].data.rate;

        } else if (convert == "COP") {
            
            price = price / typeMoneys["eur-cop"].data.rate;
        } else {

            price = price / typeMoneys["usd-eur"].data.rate;
        }
    } else if (type == "USD") {

        if (convert == "EUR") {

            price = price / typeMoneys["usd-eur"].data.rate;
        } else if (convert == "COP") {

            price = price / typeMoneys["usd-cop"].data.rate;
        } else if (convert == "USD") {
            // console.log(typeMoneys);
            price = price / typeMoneys["usd-cop"].data.rate;
        }
    }
    return price;
}

function CleanNumber(price, op) {
    var indices = [];
    if (op == "full") {
        for (var i = 0; i < price.length; i++) {
            if (price[i].toLowerCase() === ".") indices.push(i);
        }
        if (indices.length != 1) {
            price = parseFloat(remplazarCaracteres(price, ".", "").split('$')[1]);
        } else {
            price = parseFloat(price.toString().split('$')[1]);
        }




    } else if (op == "$") {
        price = parseFloat(price.split("$")[1]);
    } else if (op == ",") {
        price = toString(parseFloat(remplazarCaracteres(price, ".", ""))).replace(/,/g, ".");
    } else {
        price = parseFloat(remplazarCaracteres(price, ",", "").split('$')[1]);
    }

    return price;
}

function SetOptionTimeDelivery(id) {
    $("#timeDelivery-" + id).html("<option value='5 dias, salvo venta previa'>5 dias, salvo venta previa</option>");
}

function GetDataProduct(obj) {
    var data = [];
    data["id"] = obj.attr("data-id");
    data["precioVenta"] = parseInt($("#precioVentaCtx-" + data["id"]).attr("data-price")); //precio indivual del prodcuto internacional
    data["precioOriginal"] = parseInt($("#precioOriginalCtx-" + data["id"]).attr("data-price")); //precio indivual del prodcuto original
    data["precioColombiano"] = $("#precioColombiano-" + data["id"]).attr("value");
    data["precioUnitario+Ganancia"] = parseInt(obj.attr("data-value")); //precio total + ganancia
    data["proveedor"] = obj.attr("data-proveedor"); //proveedor
    data["moneda"] = obj.attr("data-money"); //moneda del producto
    data["lugarDeProducto"] = obj.attr("data-place"); //lugar del prodcuto
    data["precioUnitarioCOP"] = parseInt($("#priceUnitarioCOP-" + data["id"]).attr("value"));
    if(GetMoneyChecked() == "COP"){
        data["precioUnitarioCOP"] = parseInt($("#priceUnitarioCOP-" + data["id"]).attr("value"));
    }else{
        if(GetMoneyChecked() != "COP" && data["lugarDeProducto"] == "Internacional"){
          data["precioUnitarioCOP"] = parseInt($("#priceUnitarioCOP-" + data["id"]).attr("value"));
        }else{
            data["precioUnitarioCOP"] = $("#precioColombiano-" + data["id"]).attr("value");
        }
    }
    return data;
}

function GetMoneyChecked() {
    return $("[name=checkboxTypePrice]:checked").val();
}
// actualiza los totales de los productos ccuando se cambie de moneda
function UpdatePriceTotalProduct(obj, precioTotal, precioTotalGanancia, precioUnitario) {
    obj.attr("data-priceU", precioUnitario);
    obj.attr("data-type", GetMoneyChecked());
    obj.attr("value", "$" + ConvertNumber(Math.round(precioTotalGanancia)));
    obj.attr("data-value", precioTotalGanancia);
    idProduct = obj.attr("data-id");
    $("#unitePriceCtx-" + idProduct).attr("data-price", precioUnitario);
}
//se encarga de convertir el precio original de cada producto cuandos e cambie el tipo de moneda
function UpdateTypePrice() {
    $("[name=checkboxTypePrice]").click(function() {
        $("[name*=totalPriceCtx]").each(function() {
            //console.log(this);
            dataProduct = GetDataProduct($(this));


            if (dataProduct["lugarDeProducto"] == "Internacional") {
             
                if ("COP" == GetMoneyChecked()) {
                    precioPorProducto = ConvertPriceToSelectedCurrency(dataProduct["precioUnitarioCOP"], dataProduct , 1);
                    $("#unitePriceCtx-" + dataProduct["id"]).attr("data-price", precioPorProducto);
                    $("#priceUnitario-" + dataProduct["id"]).attr("value", precioPorProducto);
                } else {
                    precioPorProducto = ConvertPriceToSelectedCurrency(dataProduct["precioUnitarioCOP"], dataProduct , 1);
                    $("#unitePriceCtx-" + dataProduct["id"]).attr("data-price", precioPorProducto);
                    $("#priceUnitario-" + dataProduct["id"]).attr("value", precioPorProducto);
                }
            } else {
                dataProduct = GetDataProduct($(this));
                precioPorProducto = ConvertPriceToSelectedCurrency(dataProduct["precioUnitarioCOP"], dataProduct , null);

                //console.log(precioPorProducto + "&&&");
                $("#unitePriceCtx-" + dataProduct["id"]).attr("data-price", precioPorProducto);
            }




            CountquantityCtx(dataProduct["id"]);

        });
    });
}


function CountquantityCtx(id) {

    precioUnitarioIndividual = $("#unitePriceCtx-" + id).attr("data-price");
    // console.log("precioUnitarioIndividual" + precioUnitarioIndividual);
    //alert(precioUnitarioIndividual);
    cantidad = GetValueToInputText("quantityCtx-" + id);
    // console.log("xantidad" + cantidad);
    precioTotal = precioUnitarioIndividual * cantidad; //se calcula el precio sin ganancia
    totalPorcent = GetProductPercentage(precioTotal, id); //devuelve la ganancia del producto
    precioTotalGanancia = GetFullPriceOfProduct(precioTotal, totalPorcent); //devuelve el total de producto con la ganancia
    // console.log("preciototal" + precioTotal);
    // console.log("totalPorcent" + totalPorcent);
    // console.log("precioTotalGanancia" + precioTotalGanancia);

    SetFullPriceOfProduct(id, precioTotalGanancia); // pintamos el total del producto en el input

    $("#precioUnitarioConGanancia-" + id).attr("value", GetPriceUnitWithPercent(precioUnitarioIndividual, id)); //pintamos el precio individual con ganancia
    total = GetTotalProducts();

    TotalCountProducts(total, null);
}

function GetPriceUnitWithPercent(precioUnitario, id, condicion = false) {
    if (condicion) {
        totalPorcent = GetProductPercentage(precioUnitario, id, true); //devuelve la ganancia del producto por defecto 20%
    } else {
        totalPorcent = GetProductPercentage(precioUnitario, id); //devuelve la ganancia del producto
    }


    totalPrice = GetFullPriceOfProduct(precioUnitario, totalPorcent); //devuelve el total de producto con la ganancia
    return totalPrice;
}

function DeleteProduct(id) {
    var values = [];
    var value = 0;
    var total = 0;
    var rows = tbl.row($("#content-" + id).attr("data-row-index")).data();
    for (var i = 0; i < 15; i++) {

        values = rows[14].toString();
        values = values.replace("disabled", " none ");
        // value = ConvertNumber(parseInt($("#precioOriginalCtx-"+data["id"]).attr("data-price")));//precio indivual del prodcuto original);
    }
    for (var i = 0; i < 10; i++) {

        rows[14] = values;
        tbl.row($("#content-" + id).attr("data-row-index")).data(rows).draw();
    }

    
    $("#" + id).remove();
    $("#timeDelivery-" + id).parent().parent().remove();
    $("#totalPriceCtx-" + id).parent().parent().remove();
    $("[id*=totalPriceCtx]").each(function() {

        total += parseFloat(remplazarCaracteres($(this).attr("valor-modificable"), ",", "").split("$")[1]);
    });
    ValidateNumberItem(2, id);
    TotalCountProducts(total, null);
    listExist.removeItem(id); // 1 es la cantidad de elemento a eliminar
    IVAArray.removeItem(id);
    
    totalIVA = 0;
    console.log(listExist);
}

Array.prototype.removeItem = function (a) {
    for (var i = 0; i < this.length; i++) {
     if (this[i] == a) {
      for (var i2 = i; i2 < this.length - 1; i2++) {
       this[i2] = this[i2 + 1];
      }
      this.length = this.length - 1;
      return;
     }
    }
   };


function SumPercent(id) {
    var percent = parseFloat(GetValueToInputText("porcent-" + id));
    percent = (percent / 100);
    return percent;
}

function AddPorcentProduct() {
    var total = 0;
    var unitPrice = 0;
    var totalPorcent = 0;
    $("[name*=porcentCtx]").each(function() {
        unitPrice = $("#unitePriceCtx").attr("data-price");
        totalPorcent = (percent * (unitPrice.split('$')[1]));
        unitPrice = totalPorcent + (unitPrice.split('$')[1]);
        $("#totalPriceCtx-" + id).attr("valor-modificable", "$" + unitPrice);
        $("#totalPriceCtx-" + id).attr("value", "$" + unitPrice);
    })
    $("[name*=totalPriceCtx]").each(function() {
        total += parseFloat($(this).attr("valor-modificable").split('$')[1]);

    });

    TotalCountProducts("$" + total, null);

}



function GetTotalProducts() {
    var total = 0;
    $("[name*=totalPriceCtx]").each(function() {
        total += parseFloat(CleanNumber($(this).attr("valor-modificable"), "-"));
    });
    return total;
}

function ValidateChangePercents(id) {
    txtPercent = $("#porcent-" + id);
    id = $("#porcent-" + id).attr("data");

    if (txtPercent.val() < 20) {
        if (isSubDistribuidor != 1) {
            Checkeable = 1;
        } else {
            Checkeable = 0;
        }
        /* swal({
            title: 'Ingrese el codigo de verificación',
            input: 'text',
            allowOutsideClick: false,
            inputAttributes: {
                autocapitalize: 'off'
            },
            showCancelButton: true,
            confirmButtonText: 'Verificar',
            showLoaderOnConfirm: true,
            preConfirm: (login) => {
                return fetch(`//api.github.com/users/${login}`)
                .then(response => {
                    if (!response.ok) {
                    throw new Error(response.statusText)
                    }
                    return response.json()
                })
                .catch(error => {
                    swal.showValidationMessage(
                    `Request failed: ${error}`
                    )
                })
            },
            preConfirm: (login) => {
    
            },
            allowOutsideClick: () => !swal.isLoading()
                }).then((result) => {
                VerifyCode(result , txtPercent.attr("id"), id);
            }) */
    } else {
        Checkeable = 0;
    }


    ChangePercent(id);

}



function TotalCountProducts(price, op) {

    var imp = parseFloat(IVA / 100); //0.19
    //console.log("IVA : " + IVA);
    //alertalert(IVAArray.length );
    //alert(ValidateIfNotExistIVA());
    if (IVAArray.length != 0 && ValidateIfNotExistIVA()) {

        switch (op) {
            case "+":
                total += parseFloat(imp + parseFloat(price)); //se le agrega el IVA al precio total
                break;
            case "-":
                total -= parseFloat(imp + parseFloat(price));
                break;
            default:

                total = parseFloat(imp + parseFloat(price));
                break;
        }

        var totalTemp = ConvertNumber(Math.round(total)); //le agregamos los ". ," al producto

        if ($("[name=checkboxTypePrice]:checked").val() == "COP") {

            $("#totalIva").val("$" + ConvertNumber(Math.round(total * imp) / 2)); //mostramos el nuevo IVA

        } else {
            $("#totalIva").val("$" + ConvertNumber(Math.round((total * imp))).slice(0, -1));
        }

        if (typeQuotationBool == false) {
            $("#totalIva").val("$" + "0");
            imp = 0;
        }


        if ($("[name=checkboxTypePrice]:checked").val() == "COP") {
            $("#totalPrice").val("$" + ConvertNumber(Math.round((parseFloat(total) * imp) + parseFloat(total))));
        } else {
            $("#totalPrice").val("$" + ConvertNumber(Math.round(parseFloat(total) + (parseFloat(total) * imp))).slice(0, -1));
        }
    } else {

        var totalIVATemp = 0;
        var totalSinIVA = 0;
        $("[id*=totalPriceCtx-]").each(function() {
            var idTemp = $(this).attr("data-id");

            for (var i = 0; i < IVAArray.length; i++) {
                if (IVAArray[i] == idTemp) {
                    if (typeQuotationBool == false) {
                        totalIVA += parseFloat(0 * parseFloat($(this).attr("valor-modificable").toString().replace(/,/g, "").replace("$", "")));
                    } else {
                        //alert(parseFloat($(this).val().toString().replace(/,/g, "").replace("$", "")));
                        totalIVA += parseFloat(imp * parseFloat($(this).attr("valor-modificable").toString().replace(/,/g, "").replace("$", "")));
                    }

                } else {
                    totalIVA += 0;
                }
            }
            //console.log(IVAArray);

            if ($("[name=checkboxTypePrice]:checked").val() == "COP") {
                totalSinIVA += parseFloat($(this).attr("valor-modificable").toString().replace(/,/g, "").replace("$", ""));
            } else {
                totalSinIVA += parseFloat($(this).attr("valor-modificable").toString().replace(/,/g, "").replace("$", ""));
            }

        });
        //alert();
        if ($("[name=checkboxTypePrice]:checked").val() == "COP") {
            // alert(totalIVA);
            $("#totalIva").val("$" + ConvertNumber(parseFloat(Math.round(totalIVA)))); //mostramos el iva en 0
        } else {
            $("#totalIva").val("$" + ConvertNumber(Math.round((totalSinIVA * imp))).slice(0, -1));
        }
        //alert(typeQuotationBool);
        if (typeQuotationBool == false) {
            $("#totalIva").val("$" + "0");
        }
        if ($("[name=checkboxTypePrice]:checked").val() == "COP") {
            $("#totalPrice").val("$" + ConvertNumber(Math.round((totalSinIVA + totalIVA)))); //mostramos el total
        } else {
            $("#totalPrice").val("$" + ConvertNumber(Math.round((totalSinIVA + totalIVA))).slice(0, -1));
        }
        totalIVA = 0;
    }
}

function ValidateIfNotExistIVA() {
    var value = true;
    $("[id*=IVA-]").each(function() {

        if ($(this).attr("data-check") != "true" || $(this).attr("data-check") != true) {
            value = false;
        }
    });
    return value;
}

function ChangePercent(id) {
    CountquantityCtx(id);
}

function CalculatePrice() {
    var quantity = GetValueToInputText("quantityCtx");
    var unitPrice = GetValueToInputText("unitePriceCtx");
    SetValueToInputText("totalPriceCtx", (quantity * unitPrice));
}

function CleanPrices() {
    $("[name*=totalPriceCtx]").each(function() {
        $(this).attr("value", "Selecciona el tipo de cotización");
        $("#totalIva").val("Seleccione el tipo de cotización");
        $("#totalPrice").val("Selecciona el tipo de cotización");
        MessageSwalBasic("VERIFICA !!", "Debes seleccionar el tipo de cotización para los calculos necesarios ", "warning");
    });
}

function OnChangeTypeQuotation() {
    if ($("#quotationTypeLst option:selected").val() == "DDP") {
        $("#quotationPlaceLst option").each(function() {

            if ($(this).text() != "En sus Instalaciones") {
                
                $(this).remove();
            }
          
        });
        $("#quotationPlaceLst").append("<option value='En Las Instalaciones Vortex'> En Las Instalaciones Vortex </option>");
       
        typeQuotationBool = true;
    } else {
        typeQuotationBool = false;
        $("#quotationPlaceLst").empty().append();
        $("#quotationPlaceLst").append(listClonePLaces);
        QueryPrices();
    }

    if ($("#quotationTypeLst option:selected").val() != "") {
        QueryPrices();
    } else {
        CleanPrices();
        return false;
    }
}

function OnClickIVA(id) { // guardar el ID en un array si tiene IVA
    IVAArray = [];
    ($("#IVA-" + id).attr("data-check") == "true") ? $("#IVA-" + id).attr("data-check", false): $("#IVA-" + id).attr("data-check", true);
    $("[id*=IVA-]").each(function() {
        if ($(this).attr("data-check") == "true") {
            IVAArray.push($(this).attr("data-iva"));
        }
    });
    QueryPrices();
}

function EventsQuotation() {
    $("#quotationTypeLst").on("change", OnChangeTypeQuotation);
    $("#aplicaIVA").on("click", OnClickIVA);
}


function ChangeValeuFIlter() {
    $("#productListFilterLst ").change(function() {
        AlterTableAjaxInventoryWord($("#wordKey").val());
        $("html").css({ 'overflow': 'scroll' });
    });
}

function ChangeValeuFIlterBtn() {
    $("#wordKeyBtn ").click(function() {
        AlterTableAjaxInventoryWord($("#wordKey").val());
        $("html").css({ 'overflow': 'scroll' });
    });
}

function AlterTableAjaxInventoryWord(ind) {

    var parameters = {
        "isActionFromAjax": true,
        "optionInputForm": 10,
        "idDataInputForm": 0,
        "word": ind
    };
    tbl.destroy();
    tbl = $('#quotationTbl').DataTable({
        "columnDefs": [{
                "targets": [5, 6],
                "visible": false,
                "searchable": false
            },
            {
                "targets": [5, 6],
                "visible": false
            }
        ],
        responsive: true,
        "drawCallback": function(settings) {
            $('[data-toggle="tooltip"]').tooltip()
        },
        language: {
            "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
        },
        "ajax": {
            url: "view/html/modules/quotation/AjaxDataQuotation.php",
            method: "POST",
            data: parameters
        }
    });

}

function ChangeValeuFIlterWord() {
    /*$("#wordKey").change(function(){
        AlterTableAjaxInventoryWord($(this).val()); 
          $("html").css({'overflow':'scroll'});
    });*/
}

function SetDateQuotation() {
    $("#dateUpdateCtx").datepicker({
        setDate: new Date(),
        autoclose: true,
        "format": "yyyy-mm-dd"
    }).datepicker("update", new Date());
}