<?php
require_once(__DIR__ . '/../../../../controller/general/Config.php');
require_once(Config::PATH . Config::CONTROLLER . Config::GENERAL . 'ManageArrays.php');
require_once(Config::PATH . Config::BACKEND . 'modules/QuotationVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/InventoryVo.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralDao.php');
require_once(Config::PATH . Config::BACKEND . 'modules/CompanyVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/PersonVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/ClientUserVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/PricesCompanyVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/AditionalsImpostVo.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralDao.php');
require_once(Config::PATH . Config::BACKEND . 'modules/ImpostVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/HistoryTRMVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/RoleVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/PercentCodesVo.php');


class AjaxDataQuotation
{

    private $isActionFromAjax;
    private $optionInputForm;
    private $idDataInputForm;
    private $tasaEUR = 0;
    private $tasaUSD = 0;

    function __construct()
    {

        if (isset($_POST['isActionFromAjax'])) {

            $this->isActionFromAjax = $_POST['isActionFromAjax'];
            $this->optionInputForm = $_POST['optionInputForm'];
            $this->idDataInputForm = $_POST['idDataInputForm'];

            if ($this->isActionFromAjax == "true") {
                
                if ($this->optionInputForm == 2) {
                    $this->GetDataCompanyPrices($this->idDataInputForm);
                
                }else if ($this->optionInputForm == 3) {
                    $this->SetDataInControls();
                } else if ($this->optionInputForm == 4) {
                   $this->SetDataCountQuotation();
                }else if ($this->optionInputForm == 5) {

                    $this->GetDataContactsByIdCompany($this->idDataInputForm);

                } else if ($this->optionInputForm == 6) {

                    $this->GetDataImpost();
                 } else if ($this->optionInputForm == 7) {

                    $this->GetImpostIva();
                } else if ($this->optionInputForm == 9) {

                    $this->VerifyCode();
                      }else if ($this->optionInputForm == 8) {
                    $this->RecreateTableAjaxWord();
                   }else if ($this->optionInputForm == 10) {
                    $this->RecreateTableAjaxWord();
                }else if ($this->optionInputForm == 11) {
               
                    $this->SetData();
                }else if ($this->optionInputForm == 12) {
                    $this->ValidateIfIsSubdistribuidor();
                } else {
                    //$this->GetDataInJsonForTbl();
                    $this->GetDataInJsonForTblInventory();
                }
            }
        }
    }

    public function SetDataInControls()
    {
        $SetInputsDataFJQ = "";
        $quotationVo = new QuotationVo();
        $quotationVo->id = $this->idDataInputForm;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $quotationVo);
        if ($quotation2 = $generalDao->GetVo($generalVo = clone $quotationVo)) {
            $SetInputsDataFJQ = "
                            <script>
                               (function($){
                                SetValueToInputText('quotationCodeCtx','{$quotationVo2->code}');
                                SetValueToInputText('quotationStateCtx','{$quotationVo2->state}');
                                SetValueToInputText('quotationPercentCtx','{$quotationVo2->percent}');
                                SetValueToInputText('quotationTotalCtx','{$quotationVo2->total}');
                               })(jQuery);
                            </script>
                        ";
        }
        echo $SetInputsDataFJQ;
    }


     public function GetImpostIva()
    {
        $iva= 0;
        $impostVo = new ImpostVo();
        $impostVo->idSearchField = 1;
        $impostVo->name = "IVA";
        $generalDao = new GeneralDao();
         $generalDao->GetByField($generalVo = clone $impostVo);
        if($impostVo2 = $generalDao->GetVo($generalVo = clone $impostVo)){
                $iva = $impostVo2->percent;
        }else{
            $iva = 19;
        }
        print_r($iva);
       
    }

    private function SetDataToVo()
    {
        $personVo = new PersonVo();
        $personVo->id = $this->idDataInputForm;
        $personVo->names = $_POST["form"][3]["value"];
        $personVo->lastNames = $_POST["form"][4]["value"];                    
        $personVo->documentType = " ";                       
        $personVo->document = " ";                       
        $personVo->cellPhone = $_POST["form"][5]["value"];                       
        $personVo->telephone = $_POST["form"][6]["value"] ;                     
        $personVo->email = $_POST["form"][7]["value"]      ;                
        $personVo->address = $_POST["form"][8]["value"]     ;                 
        $personVo->city = $_POST["form"][9]["value"]     ;     
        $personVo->creationDate = date('Y-m-d H:i:s');
        $personVo->state = 1;
        $personVo->idRole = 7;
        $personVo->idCompany = $_POST["form"][14]["value"] ;                    

        return $personVo;
    }


   

    private function SetData()
    {
        $personVo = $this->SetDataToVo();
        $personVo->id = null;
        $generalDao = new GeneralDao();
        $result = $generalDao->Set($generalVo = clone $personVo);
        $this->SetDataClientUser($result);
    }

    private function SetDataToVoVClientUser($result)
    {
        $ClientUserVo = new ClientUserVo();
        $ClientUserVo->idUser = $_POST["form"][11]["value"];     
        $ClientUserVo->idClient = $result;
        $ClientUserVo->prefijo = $_POST["form"][12]["value"];     
        return $ClientUserVo;
    }

      private function SetDataClientUser($result)
    {
        $personVo = $this->SetDataToVoVClientUser($result);
        $personVo->id = null;
        $generalDao = new GeneralDao();
        $result = $generalDao->Set($generalVo = clone $personVo);

    }



     public function SetDataCountQuotation()
    {
        $allCode = [];
        $result= 0;
        $quotationVo = new QuotationVo();
        $quotationVo->isList = true;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $quotationVo);
        while($quotation2 = $generalDao->GetVo($generalVo = clone $quotationVo)){
             $allCode[]  = $quotation2->code;
        }
        if (count($allCode) > 0) {
            $result =  print_r(array_pop($allCode)+1);
        }else{
            $result = print_r(1);
        }
       return $result ;
    }



    public function GetDataInJsonForTbl()
    {

        $existData = false;
        $quotationVo = new QuotationVo();
        $quotationVo->isList = true;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $quotationVo);

        $string = "
        {\"data\": [";
        while ($quotationVo2 = $generalDao->GetVo($generalVo = clone $quotationVo)) {
            $existData = true;
            $json = array();
            $json[] = $quotationVo2->id;
            $json[] = $quotationVo2->code;
            $json[] = htmlspecialchars($quotationVo2->state);
            $json[] = htmlspecialchars($quotationVo2->percent);
            $json[] = $quotationVo2->total;
            $json[] = "<button id='updateBtn' name='updateBtn' class='btn btn-warning btn-xs' data-toggle='modal' data-target='#impostPopUpAdd' title='Modificar' onclick='UpdateData({$quotationVo2->id},\"quotation\",\"quotation\");'><i class='fa fa-pencil'></i></button>" .
                "<button id='deleteBtn' name='deleteBtn' class='btn btn-danger  btn-xs' title='Modificar' onclick='DeleteData({$quotationVo2->id},\"quotation\")'><i class='fa fa-times'></i></button>";


            $string .= json_encode($json) . ",";

        }
        $string .= "-]}-";

        if ($existData) {
            echo str_replace(",-]}-", "]}", $string);
        } else {
            echo "{ \"data\": [] }";
        }
    }

       public function GetTasa($field){
        $arrayList = array();
            $HistoryTRMVo = new HistoryTRMVo();
            $HistoryTRMVo->isList = true;
            $generalDao = new GeneralDao();
            $generalDao->CustomQuery("SELECT * FROM {$HistoryTRMVo->nameTable} WHERE DATE(DATE) >= CURDATE() AND typeMoney='{$field}' ORDER BY DATE DESC , id desc LIMIT 1");
            while($HistoryTRMVo2 = $generalDao->GetVo($generalVo = clone $HistoryTRMVo)) {
              return $HistoryTRMVo2->money;
            }   
            return 0;

    }


    public function GetDataInJsonForTblInventory()
    {
        $tasaEUR = $this->GetTasa("EUR");
        $tasaUSD = $this->GetTasa("USD");
        $existData = false;
        $inventoryVo3 = array();
        $inventoryVo = new InventoryVo();
        $inventoryVo->isList = true;
        $generalDao = new GeneralDao();
        $generalDao->CustomQuery("select * from  (select * from inventory Limit  1000 ) inventory ORDER BY id desc");
        $string = "
        {\"data\": [";
        while ($inventoryVo2 = $generalDao->GetVo($generalVo = clone $inventoryVo)) {
            $existData = true;
            array_push($inventoryVo3,$inventoryVo2);
        }
        $inventoryVo3 = $this->GetDataInJsonValidatePriceBigger($inventoryVo3);
        for($i=0; $i<count($inventoryVo3); $i++){
            $json = array();
            $json[] = htmlspecialchars($inventoryVo3[$i]->reference);
            $json[] = htmlspecialchars($inventoryVo3[$i]->name);
            $json[] = htmlspecialchars($inventoryVo3[$i]->presentation); 
            $json[] = htmlspecialchars($inventoryVo3[$i]->brand);
            if($inventoryVo3[$i]->typeCurrency == "EUR"){
                $json[] =  $tasaEUR * floatval(str_replace(".","",$inventoryVo3[$i]->priceUnit));
            }else if($inventoryVo3[$i]->typeCurrency == "USD"){
                $json[] =  str_replace(".","",  str_replace(",",".",number_format($tasaUSD * floatval($inventoryVo3[$i]->priceUnit))));     
            }else{
               $json[] =   $inventoryVo3[$i]->priceUnit;  
            }
          
            $json[] ="$".$inventoryVo3[$i]->priceUnit;  
          $json[] = $inventoryVo3[$i]->typeCurrency;
            
            $json[] = $inventoryVo3[$i]->typeProduct;
            $json[] = $inventoryVo3[$i]->id;
            $json[] = $inventoryVo3[$i]->namePlace;
            $json[] = $inventoryVo3[$i]->nameCompany;


            if ($inventoryVo3[$i]->amount <= $inventoryVo3[$i]->minimumAmount) {
                $json[] = "<p style='color: #ff5825;'><b>".$inventoryVo3[$i]->amount."</b></p>";
            } else {
                $json[] = "<p style='color: #51ba35;'><b>".$inventoryVo3[$i]->amount."</b></p>";
            }
            $json[] = "COP";
            $json[] = ( $inventoryVo3[$i]->IVA == 0 )?  "NO" : "SI";

            $json[] = "<button id='{$inventoryVo3[$i]->id}addProductBtn' name='{$inventoryVo3[$i]->id}addProductBtn' class='btn btn-primary btn-xs none'  title='Agregar'><i class='fa fa-plus'></i></button>";
            $string .= json_encode($json) . ",";
        }
        $string .= "-]}-";
        if ($existData) {
            echo str_replace(",-]}-", "]}", $string);
        } else {
            echo "{ \"data\": [] }";
        }
    }

     public function RecreateTableAjax()
    {
        $tasaEUR = $this->GetTasa("EUR");
        $tasaUSD = $this->GetTasa("USD");
        $existData = false;
        $inventoryVo3 = array();
        $inventoryVo = new InventoryVo();
        $inventoryVo->isList = true;
        $generalDao = new GeneralDao();
        $generalDao->CustomQuery("select * from inventory Limit ".($_POST["range"])." , 1000");
        $string = "
        {\"data\": [";
        while ($inventoryVo2 = $generalDao->GetVo($generalVo = clone $inventoryVo)) {
            $existData = true;
            array_push($inventoryVo3,$inventoryVo2);
        }
        $inventoryVo3 = $this->GetDataInJsonValidatePriceBigger($inventoryVo3);
        for($i=0; $i<count($inventoryVo3); $i++){
            $json = array();
            $json[] = htmlspecialchars($inventoryVo3[$i]->reference);
            $json[] = htmlspecialchars($inventoryVo3[$i]->name);
            $json[] = htmlspecialchars($inventoryVo3[$i]->presentation); 
            $json[] = htmlspecialchars($inventoryVo3[$i]->brand);
            if($inventoryVo3[$i]->typeCurrency == "EUR"){
                $json[] =  $tasaEUR * floatval(str_replace(".","",$inventoryVo3[$i]->priceUnit));
            }else if($inventoryVo3[$i]->typeCurrency == "USD"){
                $json[] =  $tasaUSD * floatval(str_replace(".","",$inventoryVo3[$i]->priceUnit));  
            }else{
            }

               $json[] ="$".$inventoryVo3[$i]->priceUnit;   
          $json[] = $inventoryVo3[$i]->typeCurrency;
            
            $json[] = $inventoryVo3[$i]->typeProduct;
            $json[] = $inventoryVo3[$i]->id;
            $json[] = $inventoryVo3[$i]->namePlace;
            $json[] = $inventoryVo3[$i]->nameCompany;
            if ($inventoryVo3[$i]->amount <= $inventoryVo3[$i]->minimumAmount) {
                $json[] = "<h4 class='text-center' style='margin: 0;padding:0;color: #ff5825;'><b>".$inventoryVo3[$i]->amount."</b></h4>";
            } else {
                $json[] = "<h4 class='text-center' style='margin: 0;padding:0;color: #51ba35;'><b>".$inventoryVo3[$i]->amount."</b></h4>";
            }
             $json[] = "COP";
             $json[] = ( $inventoryVo3[$i]->IVA == 0 )?  "NO" : "SI";
            $json[] = "<button id='{$inventoryVo3[$i]->id}addProductBtn' name='{$inventoryVo3[$i]->id}addProductBtn' class='btn btn-primary btn-xs none'  title='Agregar'><i class='fa fa-plus'></i></button>";
            $string .= json_encode($json) . ",";
        }
        $string .= "-]}-";
        if ($existData) {
            echo str_replace(",-]}-", "]}", $string);
        } else {
            echo "{ \"data\": [] }";
        }
    }

       public function RecreateTableAjaxWord()
    {
        $tasaEUR = $this->GetTasa("EUR");
        $tasaUSD = $this->GetTasa("USD");
        $existData = false;
        $inventoryVo3 = array();
        $inventoryVo = new InventoryVo();
        $inventoryVo->isList = true;
        $generalDao = new GeneralDao();
        if(empty($_POST["word"])){
            $_POST["word"] = " ";
        }
        $_POST["word"] = str_replace("'","\'",$_POST["word"]);
        $generalDao->CustomQuery("select * from inventory where name LIKE '%".$_POST["word"]."%' OR reference LIKE '%".$_POST["word"]."%' OR brand LIKE '%".$_POST["word"]."%'  OR presentation LIKE '%".$_POST["word"]."%' OR nameCompany LIKE '%".$_POST["word"]."%'  LIMIT 1000");
        $string = "
        {\"data\": [";
        while ($inventoryVo2 = $generalDao->GetVo($generalVo = clone $inventoryVo)) {
            $existData = true;
            array_push($inventoryVo3,$inventoryVo2);
        }
        $inventoryVo3 = $this->GetDataInJsonValidatePriceBigger($inventoryVo3);
        for($i=0; $i<count($inventoryVo3); $i++){
            $json = array();
            $json[] = htmlspecialchars($inventoryVo3[$i]->reference);
            $json[] = htmlspecialchars($inventoryVo3[$i]->name);
            $json[] = htmlspecialchars($inventoryVo3[$i]->presentation); 
            $json[] = htmlspecialchars($inventoryVo3[$i]->brand);
            if($inventoryVo3[$i]->typeCurrency == "EUR"){
                $json[] =  str_replace(",","",number_format($tasaEUR * (str_replace(",",".",$inventoryVo3[$i]->priceUnit))));
            }else if($inventoryVo3[$i]->typeCurrency == "USD"){
                $json[] =  str_replace(",","",number_format($tasaUSD * (str_replace(",",".",$inventoryVo3[$i]->priceUnit))));
            }else{
               $json[] =   $inventoryVo3[$i]->priceUnit;  
            }
              $json[] ="$".$inventoryVo3[$i]->priceUnit;    
          $json[] = $inventoryVo3[$i]->typeCurrency;
            
            $json[] = $inventoryVo3[$i]->typeProduct;
            $json[] = $inventoryVo3[$i]->id;
            $json[] = $inventoryVo3[$i]->namePlace;
            $json[] = $inventoryVo3[$i]->nameCompany;
            if ($inventoryVo3[$i]->amount <= $inventoryVo3[$i]->minimumAmount) {
                $json[] = "<h4 class='text-center' style='margin: 0;padding:0;color: #ff5825;'><b>".$inventoryVo3[$i]->amount."</b></h4>";
            } else {
                $json[] = "<h4 class='text-center' style='margin: 0;padding:0;color: #51ba35;'><b>".$inventoryVo3[$i]->amount."</b></h4>";
            }
               $json[] = "COP";
             $json[] = ( $inventoryVo3[$i]->IVA == 0 )?  "NO" : "SI";
            $json[] = "<button id='{$inventoryVo3[$i]->id}addProductBtn' name='{$inventoryVo3[$i]->id}addProductBtn' class='btn btn-primary btn-xs none'  title='Agregar'><i class='fa fa-plus'></i></button>";
            $string .= json_encode($json) . ",";
        }
        $string .= "-]}-";
        if ($existData) {
            echo str_replace(",-]}-", "]}", $string);
        } else {
            echo "{ \"data\": [] }";
        }
    }

    public function GetDataContactsByIdCompany($idCompany)
    {

        $json= array();
        $row = array();
        $personVo = new PersonVo();
        $personVo->idSearchField = 12;
        $personVo->idCompany = $idCompany;
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $personVo);

        while ($personVo2 = $generalDao->GetVo($generalVo = clone $personVo)) {
            /*return*/
            // $this->MakeFormContacs($personVo2->id, $personVo2->names . " " . $personVo2->lastNames);
            if($personVo2->state == 1){
                if($personVo2->idRole == 7 || $personVo2->idRole == 9 ){
                     $row = array('id' =>$personVo2->id ,"name"=> $personVo2->names." ".$personVo2->lastNames, "idCompany"=>$personVo2->idCompany , "nameRol"=>$this->GetTypeRole($personVo2->idRole) );
                    array_push($json, $row);
                 }
          
            }
        }
  
        echo json_encode($json);

    }

   
 
    public function GetDataImpost()
    {
        $impostVo = new ImpostVo();
        $generalDao = new GeneralDao();
        $impostVo->isList = true; //No le pasamos id, solo que traiga el listado
        $generalDao->GetById($generalVo = clone $impostVo);

        while ($impostVo2 = $generalDao->GetVo($generalVo = clone $impostVo)) {
            /*return*/
            //print_r("BB");
            $this->MakeFormImpost($impostVo2->id, $impostVo2->name ,$impostVo2->percent);
        }
    }

    public function MakeFormImpost($idImpost, $data , $percent)
    {
        $elementHtml = "<div class='checkbox NoSelected' style='margin-left: 6px;'><label><input type='checkbox' data-impost='$percent'  id='checkbox{$idImpost}' name='checkbox' value='{$data}'><id='$idImpost'>$data</label></div><script>ValidateCheckboxImpost('{$idImpost}');</script>";


        echo($elementHtml);
    }


    function GetTypeRole($idRole)
    {
        $roleVo = new RoleVo();
        $roleVo->id = $idRole;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $roleVo);
        if ($roleVo2 = $generalDao->GetVo($generalVo = clone $roleVo)) {
            return "" . htmlspecialchars($roleVo2->name);
        }
        return "Problema en la consulta";
    }

    public function ValidateIfIsSubdistribuidor(){
        $CompanyVo = new CompanyVo();
        $CompanyVo->id = $_POST["id"];
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $CompanyVo);
        if ($CompanyVo2 = $generalDao->GetVo($generalVo = clone $CompanyVo)) {
            if($CompanyVo2->idRoleCompany == 5){
                print(1);
            }else{
                print(0);
            }
        }
    }


    private function VerifyCode(){
    if(isset($_POST["code"])){
       $percentVo = new PercentCodesVo();
        $percentVo->code = $_POST["code"];
        $percentVo->idSearchField = 1;
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $percentVo);
        if ($percentVo2 = $generalDao->GetVo($generalVo = clone $percentVo)) {
            return print_r(1);
        }
        return print_r(0);
        }
    
return print_r(2);
}

    function GetDataInJsonValidatepriceBigger($objData){
        $arrayObj = $objData;
        $arrayObj2 = $objData;
       
        for($i=0; $i<count($arrayObj); $i++){
            for($e=0; $e<count($arrayObj2); $e++){
              
                if(array_key_exists($i,$arrayObj) && array_key_exists($e,$arrayObj2) && $arrayObj[$i]->name == $arrayObj2[$e]->name && $arrayObj[$i]->reference == $arrayObj2[$e]->reference && $arrayObj[$i]->brand == $arrayObj2[$e]->brand && $arrayObj[$i]->nameCompany != $arrayObj2[$e]->nameCompany){
                  
                    if($arrayObj[$i]->priceUnit < $arrayObj2[$e]->priceUnit){
                        unset($arrayObj[$i]);
                       
                        $arrayObj = array_values($arrayObj);
                   }
                }
            }   
        }
        $arrayObj = array_values($arrayObj);
        return $arrayObj;
        
    }

    function GetDataCompanyPrices($nameCompany){
        $dataPricesCompany = array();
        $idCompany = $this->GetIdCompanyForName($nameCompany);
        $dataPricesCompany = $this->GetDataPricesCompany($idCompany);
        $dataAditionalsImpost = $this->GetDataAditionalsImpostForCompany($dataPricesCompany[0]->id);
        $dataGlobalPrices = array($dataPricesCompany, $dataAditionalsImpost);
        echo json_encode($dataGlobalPrices);
       
    }
    function GetIdCompanyForName($nameCompany){
        $roleCompanyVo = new companyVo();
        $roleCompanyVo->name = $nameCompany;
        $roleCompanyVo->idSearchField = 1;
        $generalDao = new GeneralDao();
       
        $generalDao->GetByField($generalVo = clone $roleCompanyVo);
        if($roleCompanyVo2 = $generalDao->GetVo($generalVo = clone $roleCompanyVo)) {
           // print_r($roleCompanyVo2);
             return $roleCompanyVo2->id;
        }
    }
    function GetDataPricesCompany($idCompany){
        $dataPricesCompany = array();
        $companyVo = new PricesCompanyVo();
        $companyVo->idCompany = $idCompany;
        $companyVo->idSearchField = 4;
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $companyVo);
        while($companyVo2 = $generalDao->GetVo($generalVo = clone $companyVo)) {
           
            array_push($dataPricesCompany,$companyVo2);
        }
        return $dataPricesCompany;
    }


    function GetDataAditionalsImpostForCompany($idPricesCompany){
        $dataAditionalsImpost = array();
        if(isset($idPricesCompany)){
            $aditionalsImpostVo = new AditionalsImpostVo();
            $aditionalsImpostVo->idPricesCompany = $idPricesCompany;
            $aditionalsImpostVo->idSearchField = 3;
            $generalDao = new GeneralDao();
            $generalDao->GetByField($generalVo = clone $aditionalsImpostVo);
            while($aditionalsImpostVo2 = $generalDao->GetVo($generalVo = clone $aditionalsImpostVo)) {
                array_push($dataAditionalsImpost,$aditionalsImpostVo2);
            }
        }
        return $dataAditionalsImpost;
    }

      function LoadJsonMoneys($value){
        $ruta = "http://api.currencies.zone/v1/quotes/".$value."/COP/json?key=136|QdBH^Od*z2Q~sSLOfnKPYZYEGGECk1_1";
        //echo $ruta;
        $json = file_get_contents($ruta);
       
       return json_decode($json);
    }

}

$ajaxDataQuotation = new AjaxDataQuotation();