<?php
require_once(Config::PATH . Config::CONTROLLER . 'general/AccessManager.php');
require_once(Config::PATH . Config::CONTROLLER . 'general/UtilJquery.php');
require_once(Config::PATH . Config::VIEW_HTML . 'general/comps/GeneralCtx.php');
require_once(Config::PATH . Config::VIEW_HTML . 'general/comps/GeneralLst.php');
require_once(Config::PATH . Config::VIEW_HTML . 'general/comps/GeneralWithDataLst.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralDao.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/CompanyVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/ImpostVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/QuotationVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/InventoryVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/ProductVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/UserQuotationVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/ImpostQuotationVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/PersonVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/RoleVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/AditionalsImpostVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/ClientUserVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/HistoryTRMVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/PricesCompanyVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/HistoryModificationVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/UserVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/PersonCreateQuotationVo.php');
require_once(Config::PATH . Config::CONTROLLER . 'general/PHPMailer.php');
require_once(Config::PATH . Config::BACKEND . 'modules/MailAllowsVo.php');
require_once(Config::PATH . Config::CONTROLLER_LIB .'vendor/dompdf/dompdf/src/Autoloader.php');

use Dompdf\Dompdf;

class QuotationView
{
    private $optionInputForm;
    private $idDataInputForm;
    /*=============================================
                  Session Vars
    =============================================*/
    /*=============================================

                  Template Vars
    =============================================*/
    private $utilJQ;
    private $pageTpl, $data, $core;
    /*=============================================
                Controls Vars HTML
    =============================================*/

    private $personNamesCtx;
    private $personLastNamesCtx;
    private $personDocumentTypeLst;
    private $personDocumentCtx;
    private $personCellPhoneCtx;
    private $personTelephoneCtx;
    private $personEmailCtx;
    private $personAddressCtx;
    private $personCityCtx;
    private $personIdRoleHidden;
    private $companyLst;
    private $personIdCompanyHidden;
    private $personUserLst;
    private $personPrefijoLst;


    private $quotationCodeCtx;
    private $quotationStateCtx;
    private $quotationPercentCtx;
    private $quotationTotalCtx;
    private $quotationCompanyLst;
    private $quotationUserLst;
    private $quotationTimeLst;
    private $quotationPaymentLst;
    private $quotationTypeLst;
    private $quotationPlaceLst;
    private $productListFilterLst;

    private $companyVo;
    private $idCompanyFromGet;
    private $idRolePerson;

    private $dateUpdateCtx;

    private $arrayWithListMail = [];

    //private $quotationImpostLst;
    /*=============================================
                Data Vars
    =============================================*/
    private $generalDao;


    function __construct($pageTpl, $data, $core)
    {
        $this->data = $data;
        $this->core = $core;
        $this->pageTpl = $pageTpl;
        $this->ValidateAllowsQuotes();
            $this->CreateComponents();
            $this->UseDataFromThePostback();
      
    }

    private function UseDataFromThePostback()
    {
        if (isset($_POST['optionInputForm'])) {
            $this->optionInputForm = $_POST['optionInputForm'];
            $this->idDataInputForm = $_POST['idDataInputForm'];
            $this->generalDao = new GeneralDao();
            switch ($this->optionInputForm) {

                case "4": {
                    $this->ModifyQuotationFunction();
                    break;
                }
                case "5": {
                    $this->MakeQuotationFunction();                    
                    break;
                }
                 case "6": {
                   $this->DeleteFilePdf();
                    break;
                }
                case "7": {
                    $this->SetDataQuotationTpl();
                    break;
                }
                 case "8": {
                    $this->LoadJsonMoneys();
                    break;
                }
                case "9": {
                    $this->SendMailers();
                    break;
                }
            }
        }
        $this->AssignDataToTpl();

    }


    private function ValidateAllowsQuotes(){
        if(isset($_GET["DataReceived"])){
            header('location: ConfigData?id='.$_GET["DataReceived"]);
        }
    } 

    private function CreateComponents()
    {   

        $dataTypeQuotationLst = array("DDP","FCA","CIF");
        $dataPlaceDeliveryLst= array("En sus Instalaciones", "En fábrica", "En Aeropuerto");
        $dataPaymentLst= array("30 días fecha de facturación", "50% anticipado - 50% contra Entrega", "100% Anticipado"," 100% Contra Entrega");
        $dataTimeLst = $this->GetDataOptions();

        $this->idCompayInputForm = new GeneralCtx("idCompayInputForm","","hidden",false);
        $this->personNamesCtx = new GeneralCtx("personNamesCtx","Nombres",null,true);
        $this->personLastNamesCtx = new GeneralCtx("personLastNamesCtx","Apellidos",null,true);
        $dataPersonPrefijoTypeLst = array("Señor", "Señora", "Ingeniero", "Ingeniera" , "Doctor","Doctora");
        $this->personPrefijoLst = new GeneralWithDataLst($dataPersonPrefijoTypeLst, "personPrefijoLst", false,false,"--Tipo Prefijo--",true);
        $dataPersonDocumentTypeLst = array("CC", "CE", "TI", "Otro");
        $this->personDocumentTypeLst = new GeneralWithDataLst($dataPersonDocumentTypeLst, "personDocumentTypeLst", false,false,"--Tipo Documento--",true);
        $this->companyLst = new GeneralWithDataLst($this->GetCustomers(), "companyLst", false,true,"--Seleccione la empresa--",true);
        $this->personDocumentCtx = new GeneralCtx("personDocumentCtx","Numero Documento",null,true);
        $this->personUserLst = new GeneralWithDataLst($this->AssignDataSeller(), "personUserLst", false,true,"--Vendedor Asociado--",true);
        $this->personCellPhoneCtx = new GeneralCtx("personCellPhoneCtx","Numero Celular",null,true);
        $this->personTelephoneCtx = new GeneralCtx("personTelephoneCtx","Numero Telefono",null,true);
        $this->personEmailCtx = new GeneralCtx("personEmailCtx","Correo Electronico",null,true);
        $this->personAddressCtx = new GeneralCtx("personAddressCtx","Dirección",null,true);
        $this->personCityCtx = new GeneralCtx("personCityCtx","Ciudad",null,true);
        $this->dateUpdateCtx = new GeneralCtx("dateUpdateCtx","dateUpdate",null,true);
        

        
        $this->personIdRoleHidden = new GeneralCtx("personIdRoleHidden","","hidden",false);
        $this->personIdCompanyHidden = new GeneralCtx("personIdCompanyHidden","","hidden",false);


        $this->personIdRoleHidden = new GeneralCtx("personIdRoleHidden","","hidden",false);
        $this->quotationCodeCtx = new GeneralCtx("quotationCodeCtx", "Codigo", null, true);
        $this->quotationStateCtx = new GeneralCtx("quotationStateCtx", "Estado", null, true);
        $this->quotationPercentCtx = new GeneralCtx("quotationPercentCtx", "Porcentaje", null, true);
        $this->quotationTotalCtx = new GeneralCtx("quotationTotalCtx", "Total", null, true);
        $this->quotationUserLst = new GeneralWithDataLst($this->GetUser(), "quotationUserLst", false, true, "-- Seleccione --", true);
        $this->quotationCompanyLst = new GeneralWithDataLst($this->GetCustomers(), "quotationCompanyLst", false,true, "-- Seleccione --", true);
        $this->quotationTypeLst = new GeneralWithDataLst($dataTypeQuotationLst, "quotationTypeLst", false,false, "-- Tipo de cotizacion --", true);
        $this->quotationPlaceLst = new GeneralWithDataLst($dataPlaceDeliveryLst, "quotationPlaceLst", false,false, "-- Lugar de entrega --", true);
        $this->quotationPaymentLst = new GeneralWithDataLst($dataPaymentLst, "quotationPaymentLst", false,false, "-- Forma de pago --", true);
        $this->quotationDeliveryLst = new GeneralWithDataLst($dataTimeLst, "quotationDeliveryLst", false,false, "--Tiempo de entrega--", true);
        $this->quotationOfferLst = new GeneralWithDataLst($dataTimeLst, "quotationOfferLst", false,false, "--Validez de la oferta--", true);
        $this->productListFilterLst =  new GeneralWithDataLst($this->GetFilterProduct(), "productListFilterLst", false,true,"--Lista de Registros--",true);

        $this->utilJQ = new UtilJquery("Quotation");
    }

      private function GetFilterProduct(){
        $divCount = 0 ;
        $countProducts =   array();
        $inventoryVo = new InventoryVo();
        $inventoryVo->isList = true;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $inventoryVo);
        while ($inventoryVo2 = $generalDao->GetVo($generalVo = clone $inventoryVo)) {
            $countProducts[] = $inventoryVo2->id;
        }
        $divCount =  round(count($countProducts) / 5000);
        $arraySendData = $this->BuildFilterData($divCount);
        return $arraySendData;
    }

    private function AssignDataSeller(){

        $userArray = array();
        $PersonVo = new PersonVo();
        $PersonVo->isList = true;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $PersonVo);
        while ($PersonVo2 = $generalDao->GetVo($generalVo = clone $PersonVo)) {
            if($PersonVo2->idRole == 1 || $PersonVo2->idRole == 2 || $PersonVo2->idRole == 3 || $PersonVo2->idRole == 6 || $PersonVo2->idRole == 10 || $PersonVo2->idRole == 11 ){
                if($PersonVo2->state == 1){
                    $userArray[]  = $PersonVo2->id."_".$PersonVo2->names." ".$PersonVo2->lastNames;  
                }
            }
        }
        return $userArray;

    }

    private function BuildFilterData($cant){
        $data = array();
        $autoIncrement = 0 ;
        $autoIncremenMax = 5000 ;
        for ($i=0; $i < ($cant + 1); $i++) { 
            $data[] = ($autoIncrement)."_"."Mostrar de ".$autoIncrement." Hasta ".$autoIncremenMax." Registros";
            $autoIncrement += 5000;
            $autoIncremenMax += 5000;
        }
    return $data;
    }

    private function GetDataOptions(){
        $arrayOptions = [];
        $content = [15,30,45,60];
        for($i=0; $i < count($content); $i++){ 
            array_push($arrayOptions, $content[$i]); 
        }
        return $arrayOptions;
    }

      private function DeleteFilePdf(){
        unlink("view/docs/pdf/".$_POST["id"].".pdf");
    }

    private function SetDataToVo()
    {
        $quotationVo = new QuotationVo();
        $quotationVo->id = $this->idDataInputForm;
        $quotationVo->code = $_POST["quotationCodeCtx"];
        $quotationVo->state = $_POST["quotationStateCtx"];
        $quotationVo->percent = $_POST["quotationPercentCtx"];
        $quotationVo->percent = $_POST["quotationTotalCtx"];

        return $quotationVo;
    }

    private function SetData()
    {
        $quotationVo = $this->SetDataToVo();
        $quotationVo->id = null;
        $result = $this->generalDao->Set($generalVo = clone $quotationVo);
        if (isset($result)) {
            $message = 'MessageSwalBasic("Registrado!","Cotización Registrada Correctamente","success");';
            $this->utilJQ->AddFunctionJavaScript($message);
        }
    }

    private function SetUpdate()
    {
        $result = $this->generalDao->Update($generalVo = clone $this->SetDataToVo());
        if (isset($result)) {
            $message = 'MessageSwalBasic("Modificado!","Cotización Modificada Correctamente","success");';
            $this->utilJQ->AddFunctionJavaScript($message);
        }
    }

    private function DeleteData()
    {
        $message = "";
        $quotationVo = new QuotationyVo();
        $quotationVo->id = $this->idDataInputForm;
        if ($this->generalDao->Delete($generalVo = clone $quotationVo)) {
            $message = 'MessageSwalBasic("Eliminado!","Cotización Eliminado Correctamente","success");';
        } else {
            $message = 'MessageSwalBasic("Error!","Problema con la base de datos","error");';
        }
        $this->utilJQ->AddFunctionJavaScript($message);
    }

    private function AssignDataToTpl()
    {
        $this->data->assign('quotationUserLst', $this->quotationUserLst->paint());
        $this->data->assign('quotationCompanyLst', $this->quotationCompanyLst->paint());
        $this->data->assign('quotationTypeLst', $this->quotationTypeLst->paint());
        $this->data->assign('quotationPlaceLst', $this->quotationPlaceLst->paint());
        $this->data->assign('quotationPaymentLst', $this->quotationPaymentLst->paint());
        $this->data->assign('quotationDeliveryLst', $this->quotationDeliveryLst->paint());
        $this->data->assign('quotationOfferLst', $this->quotationOfferLst->paint());
        $this->data->assign('productListFilterLst', $this->productListFilterLst->paint());

        $this->data->assign('idCompayInputForm', $this->idCompayInputForm->paint());
        $this->data->assign('personNamesCtx', $this->personNamesCtx->paint());
        $this->data->assign('personLastNamesCtx', $this->personLastNamesCtx->paint());
        $this->data->assign('personDocumentTypeLst', $this->personDocumentTypeLst->paint());
        $this->data->assign('personDocumentCtx', $this->personDocumentCtx->paint());
        $this->data->assign('personCellPhoneCtx', $this->personCellPhoneCtx->paint());
        $this->data->assign('personTelephoneCtx', $this->personTelephoneCtx->paint());
        $this->data->assign('personEmailCtx', $this->personEmailCtx->paint());
        $this->data->assign('personAddressCtx', $this->personAddressCtx->paint());
        $this->data->assign('personCityCtx', $this->personCityCtx->paint());
        $this->data->assign('personIdRoleHidden', $this->personIdRoleHidden->paint());
        $this->data->assign('personIdCompanyHidden', $this->personIdCompanyHidden->paint());
        $this->data->assign('companyLst', $this->companyLst->paint());
        $this->data->assign('personUserLst', $this->personUserLst->paint());
        $this->data->assign('personPrefijoLst', $this->personPrefijoLst->paint());
        $this->data->assign('dateUpdateCtx', $this->dateUpdateCtx->paint());
        
         $this->data->assign('tempParameter', time());

        $this->data->assign('jquery', $this->utilJQ->PaintJQ());
        $this->SetDataToTpl();
    }

    private function GetCustomers()
    {
        $companiesArray = array();
        $companyVo = new CompanyVo();
        $companyVo->isList = true;
        $generalDao = new GeneralDao();
        $generalDao->CustomQuery("SELECT * FROM  {$companyVo->nameTable} ORDER BY name ASC");
        while ($companyVo2 = $generalDao->GetVo($generalVo = clone $companyVo)) {
            if($companyVo2->idRoleCompany == 2 || $companyVo2->idRoleCompany == 4 || $companyVo2->idRoleCompany == 5){
                if($companyVo2->state == 1){
                        $companiesArray[] = $companyVo2->id . "_" . htmlspecialchars($companyVo2->name);
                }
            }
        
        }
        return $companiesArray;
    }

    private function GetImpost()
    {
        $impostsArray = array();
        $impostVo = new ImpostVo();
        $impostVo->idSearchField = 2;
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $impostVo);
        while ($impostVo2 = $generalDao->GetVo($generalVo = clone $impostVo)) {
            $impostsArray[] = $impostVo2->id . "_" . $impostVo2->name;
        }
        return $impostsArray;
    }

    

    //========================================create quotation===================================

   

      public function SetDataCountQuotation($data)
    {
        $allCode = [];
        $result= 0;
        $quotationVo = new QuotationVo();
        $quotationVo->isList = true;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $quotationVo);
        while($quotation2 = $generalDao->GetVo($generalVo = clone $quotationVo)){
             $allCode[]  = $quotation2->code;
        }
        if ($data == 0 ) {
            $result = 1 ;
        }else{
            $result =  (count($allCode)+1);
        }

  

       return $result ;
    }

    private function GenerateQuotation($data,$arrayPerson){

        $generalDao = new GeneralDao();
        $QuotationVo = new QuotationVo();
        $QuotationVo->code = $this->SetDataCountQuotation(intval($data[1]));
        print_r("------------------////".date('Y-m-d H:i:s'));
        $QuotationVo->generationDate = date('Y-m-d H:i:s');
        $QuotationVo->state = $data[2];
        $QuotationVo->total = str_replace(".", "" ,str_replace(",",".",$data[4]));
        $QuotationVo->idUser = $arrayPerson[4];
        $QuotationVo->typeMoney = $data[6];
        $QuotationVo->idCompany = $_POST["idCompany"];
        $QuotationVo->offerValidity = $data[8];
        $QuotationVo->payment = $data[9];
        $QuotationVo->typeQuotation = $data[10];
        $QuotationVo->placeDelivery = $data[11]; // yead;
        $QuotationVo->version = $data[12];
        $QuotationVo->reminder = 0;
        $QuotationVo->IVA = $this->GetImpostIva();
        $QuotationVo->aplicateIVA = $_POST["ivaValidate"];
        $QuotationVo->isDDP = $_POST["typeQuotationBool"];
        $QuotationVo->checkeable = $_POST["check"];
        $date = new DateTime($_POST["dateUpdate"]);
        $QuotationVo->dateUpdate =  $date->format('Y-m-d');
        $result = $generalDao->Set($generalVo = clone $QuotationVo);
        print_r($QuotationVo);
        return $result;
    }

    private function UpdateQuotation($data,$arrayPerson){
        //print_r($data);
        $generalDao = new GeneralDao();
        $QuotationVo = new QuotationVo();
        $QuotationVo->id = $_POST["idc"];
        $QuotationVo->code = $data[13];
        $QuotationVo->generationDate = date("Y-m-d H:i:s");
        $QuotationVo->state = $data[2];
        $QuotationVo->total = $data[4];
        $QuotationVo->idUser = $arrayPerson[4];
        $QuotationVo->typeMoney = $data[6];
        $QuotationVo->idCompany = $_POST["idCompany"];
        $QuotationVo->offerValidity = $data[8];
        $QuotationVo->placeDelivery = $data[11];
        $QuotationVo->payment = $data[9];
        $QuotationVo->typeQuotation = $data[10];
        $QuotationVo->reminder = 1;
        $QuotationVo->version = ($data[12])+1;
        $QuotationVo->IVA = $this->GetImpostIva();
        $QuotationVo->aplicateIVA = $_POST["ivaValidate"];
        $QuotationVo->isDDP = $_POST["typeQuotationBool"];
        $date = new DateTime($_POST["dateUpdate"]);
        $QuotationVo->dateUpdate =  $date->format('Y-m-d');
        $QuotationVo->checkeable = $_POST["check"];
        $result = $generalDao->Update($generalVo = clone $QuotationVo);
        return $result;
    }


    //============================================insert product ================================

    private function InsertProductQuotation($quotation , $quantity , $price){
        $arrayList = [];
        $cont = 0;
        if(isset($_POST["id"])){
            for ($i=0; $i < count($_POST["id"]) ; $i++) { 
                $productVo = $this->ObjectProductQuotation($_POST["id"][$i] ,  $quotation);
                $product = $this->ProductQuotation($productVo ,$quantity[$i], $quotation , $_POST["porcentProducts"][$i] , $price[$i] ,  $_POST["timeDelivery"][$i] , $_POST["nodos"][$i]);
                $arrayList[$cont] = $product;
                $cont++;
            }
            return $arrayList;
        }
        
    }

    private function DeleteProducts($products , $id){
        for ($i=0; $i < count($products) ; $i++) { 
            $productVo = new ProductVo();
            $productVo->idSearchField = 16;
            $productVo->idQuotation = $id;
            $this->generalDao->DeleteByField($generalVo = clone $productVo); 
        }
    }

    private function DeleteUsersSelected($id){

         //for ($i=0; $i < count($users) ; $i++) { 
            $userQuotationVo = new UserQuotationVo();
            $userQuotationVo->idSearchField = 3;
            $userQuotationVo->idQuotation = $id;
            $this->generalDao->DeleteByField($generalVo = clone $userQuotationVo);   
         //}
    }

      private function UpdateStateQuotation()
    {

            $quotationVo = new QuotationVo();
            $quotationVo3 = new QuotationVo();
            $quotationVo4 = new QuotationVo();

            $quotationVo->id =  $_POST["idc"];
            $generalDao = new GeneralDao();
            $generalDao2 = new GeneralDao();
            $generalDao3 = new GeneralDao();
            $generalDao->GetById($generalVo = clone $quotationVo);
            while ($quotationVo2 = $generalDao->GetVo($generalVo = clone $quotationVo)) {
                $quotationVo3->id = $_POST["idc"];
                $quotationVo3->idSearchField = 0;
                $quotationVo3->version = ($quotationVo2->version + 1);
                $quotationVo3->idUpdateField = 12;
                $generalDao2->UpdateByField($generalVo = clone $quotationVo3);
                if($_POST["data"] != "DDP" ){
                 $quotationVo4->id = $_POST["idc"];
                $quotationVo4->idSearchField = 0;
                $quotationVo4->isDDP = 0;
                $quotationVo4->idUpdateField = 15;
                $generalDao3->UpdateByField($generalVo = clone $quotationVo4);
                }
            }
    }

     private function SetDataModificationQuotation($DataModification)
    {
        session_start();
        
        $personVo = unserialize($_SESSION['user']["person"]);
   
        $date = new DateTime(date("Y-m-d H:i:s"));
        $historymodificationVo = new HistorymodificationVo();
        $generalDao = new GeneralDao();
        $historymodificationVo->idUser=$personVo->id;
        $historymodificationVo->modificationDate=$date->format('Y-m-d H:i:s');
        $historymodificationVo->idQuotation= $_POST["idc"] ;
        $historymodificationVo->description="Ha cambiado el estado a ".$DataModification."";    
        $data = $generalDao->set($generalVo = clone $historymodificationVo);
        //print_r($data);

    }

       private function UpdateStateTypeQuotate(){
            $quotationVo = new QuotationVo();
            $generalDao = new GeneralDao();
            $quotationVo->id = $_POST["idc"];
            $quotationVo->idSearchField = 0;
            $quotationVo->idUpdateField = 11;
            $quotationVo->isList = false;
            //$quotationVo->total = 12;
            $quotationVo->typeQuotation = $_POST["data"];
            //print_r($quotationVo->total);
            $generalDao->UpdateByField($generalVo = clone $quotationVo);
        }



       private function UpdateQuotationTotal(){
            $quotationVo = new QuotationVo();
            $generalDao = new GeneralDao();
            $quotationVo->id = $_POST["idc"];
            $quotationVo->idSearchField = 0;
            $quotationVo->idUpdateField = 5;
            $quotationVo->isList = false;
            //$quotationVo->total = 12;
            $quotationVo->total = $_POST["quotationData"][4];
            //print_r($quotationVo->total);
            $generalDao->UpdateByField($generalVo = clone $quotationVo);
        }


    private function ModifyQuotationFunction(){
    $arrayPerson = [];
        $quantity=$_POST["quantity"];
        $quotation=$_POST["quotationData"][7];
        $price = $_POST["prices"];
        $idPerson =$_POST["person"];
        $usersSelected = $_POST["usersSelected"];
        if(isset($idPerson)){
            $arrayPerson= $this->GetDataPerson($idPerson);
            $this->UpdatePersonQuotation($arrayPerson,$idPerson,$quotation);   
        }
        $this->UpdateQuotation($_POST["quotationData"],$arrayPerson);
        $this->DeleteUsersSelected($quotation);
        $this->DeleteProducts($_POST["prices"],$quotation); 
        $this->InsertPersonsQuotation($quotation);
      
        $persons =(array) $this->GetPersonsModify($quotation);   
         $products =  $this->InsertProductQuotation($quotation , $quantity , $price);
        $quotationData = $this->GetDataQuotation($quotation);   
        $this->SetDataModificationQuotation($_POST["motivo"]);
         
        
    }

     private function MakeQuotationFunction(){
        $quantity=$_POST["quantity"];
        $total=$_POST["quotationData"][4];
        $price = $_POST["prices"];
        $idPerson =$_POST["person"];
        
        $arrayPerson = [];
        if(!empty($idPerson)){
          $arrayPerson= $this->GetDataPerson($idPerson);
        }
        $quotation = $this->GenerateQuotation($_POST["quotationData"],$arrayPerson);    
        $this->InsertPersonCreate($arrayPerson , $quotation);
         $this->InsertModification($quotation,$arrayPerson,false);

        $products =  $this->InsertProductQuotation($quotation , $quantity , $price);
        $persons = $this->InsertPersonsQuotation($quotation);  
        $quotationData = $this->GetDataQuotation($quotation);
        $quotationData[10] = $_POST["typeProduct"];    
        print_r("--split--".$quotationData[0]->code."_".$this->GetNameCompany($quotationData[0]->idCompany)."_v".$quotationData[0]->version."--split--");
    }

    private function GetListWithMails()
    {
        $MailAllowsVo = new MailAllowsVo();
        $MailAllowsVo->module = 2;
        $MailAllowsVo->idSearchField = 4;
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $MailAllowsVo);
        while ($MailAllowsVo2 = $generalDao->GetVo($generalVo = clone $MailAllowsVo)) {
            $this->arrayWithListMail[] = $MailAllowsVo2->mail;
        }
    }

    private function SendMailers(){
        $this->GetListWithMails();
        $dataMail =  array('mail' => "sysvortex@arvack.com", 'password' => "sysvortex153",'domain' => "mail.arvack.com", 'port' => 25 , "from" => "sistema de correo Vortex Company");
        $PHPMailerUtils = new PHPMailerUtils($dataMail);
        $data = $this->DataIdToEmail();
        //print_r($data);
        for($i = 0 ; $i < count($this->arrayWithListMail) ; $i++){
            $PHPMailerUtils->AddDestinatario($this->arrayWithListMail[$i]);
        }
        //$PHPMailerUtils->AddDestinatario(array("jjvela04@misena.edu.co"));
        $subject = "Confirmación de productos con descuento!";

       $contenido = '<style type="text/css">body, html,  .body {   background: #f3f3f3 !important; }</style><spacer size="16"></spacer>
                           <container>
                             <row>
                               <columns large="4" style="backgroung:black">
                                 <center>
                                   <img src="https://www.tescan.com/getattachment/beaaaee6-fa88-449d-9027-4f22f7e80758/vortex-company.aspx">
                                 </center>
                               </columns>

                               <columns large="8">
                                 <h1 style="text-align: center;">Corfirmación de cotización</h1>
                               </columns>
                             </row>
                             <spacer size="16"></spacer>

                             <row>
                               <columns>
                                 <p class="text-center">Detalles de la cotización con responsabilidad la persona '.$data["arrayPersonQuotation"][0]->completeName.' el dia '.$data["quotationQuotation"][0]->generationDate.'  ,con productos con los siguientes detalles.</p><br><ul>
                                 ';
                                for($i = 0 ; $i < count($data["productsQuotation"]);$i++){
                                    if($data["productsQuotation"][$i]["obj"]->percent < 20){
                                        $contenido .= '
                                        <li><b>'.$data["productsQuotation"][$i]["obj"]->name."</b> con un descuento del ".$data["productsQuotation"][$i]["obj"]->percent.'%</li> 
                                        ';
                                    }
                                }
                                 $contenido .= '    
                               </ul></columns>
                             </row>

                             <spacer size="16"></spacer>
                             <row>
                               <columns>
                                 <h3 class="text-center"> Puedes Verificar el siguiente link </h3>
                                 
                                 <spacer size="16"></spacer>

                                 <a style=" display: inline-block;*display: inline; zoom: 1;padding: 6px 20px; margin: 0;cursor: pointer;  border: 1px solid #bbb;overflow: visible; font: bold 13px arial, helvetica, sans-serif;text-decoration: none; white-space: nowrap; color: #555;" class="large expand" href="https://www.arvack.com/vortexcompany/Quotation?DataReceived='.$_POST["id"].'">Click para Chequear Productos</a>
                               </columns>
                             </row>

                             <spacer size="16"></spacer>

                           </container>';
                                    

        $PHPMailerUtils->SendDataMail($subject , $contenido , "");
   }

   private function DataIdToEmail(){
    $array = [];
    $array["productsQuotation"] =  $this->GetProductsQuotation($_POST["id"]);
    $array["arrayPersonQuotation"] = $this->GetPersonCreateQuotation($_POST["id"]);
    $array["quotationQuotation"] = $this->GetDataQuotation($_POST["id"]);
    $array["personsQuotation"] = $this->GetPersonsQuotation($_POST["id"]);
    $array["contentQuotation"] = array("product"=>$productsQuotation ,"person"=>$arrayPersonQuotation , "quotationData"=>$quotationQuotation ,"persons"=>$personsQuotation);
    return $array;
}



    private function SetDataQuotationTpl(){
            $productsQuotation =  $this->GetProductsQuotation($_POST["id"]);
            $arrayPersonQuotation= $this->GetPersonCreateQuotation($_POST["id"]);
            $quotationQuotation = $this->GetDataQuotation($_POST["id"]);
            $personsQuotation = $this->GetPersonsQuotation($_POST["id"]);
            $contentQuotation = array("product"=>$productsQuotation ,"person"=>$arrayPersonQuotation , "quotationData"=>$quotationQuotation ,"persons"=>$personsQuotation);
            echo "<input type='hidden' id='ConsultQuotation' value='".json_encode($contentQuotation)."'>";
    }

    private function ObjectProductQuotation($id,$quotation){
        $productVo = new InventoryVo();
        $generalDao = new GeneralDao();
        $productsVo = new ProductVo();
        $productVo->id = $id;
        $productVo->idSearchField = $id;
           
        $generalDao->GetById($generalVo = clone $productVo);
          
        while ($productVo2 = $generalDao->GetVo($generalVo = clone $productVo)){
            $productVo2->idQuotation = $quotation;
            $productsVo = $productVo2;
        }
        return $productsVo;
           
    }


    private function LoadJsonMoneys(){
      /*  $ruta = "https://api.cambio.today/v1/full/".$_POST["from"]."/json?key=136|QdBH^Od*z2Q~sSLOfnKPYZYEGGECk1_1";
        //echo $ruta;
        $json = file_get_contents($ruta);
       //$json = file_get_contents("https://api.cambio.today/v1/".$_POST["index"]."/".$_POST["from"]."/json?key=136|QdBH^Od*z2Q~sSLOfnKPYZYEGGECk1_1");
       print_r("//////////////".$json."//////////////");*/
       if($_POST["from"] == "eur" || $_POST["from"] == "EUR" ){
           print_r("//////////////".'{ "result" : {
            "from": "eur",
            "conversion": [
                {
                    "to": "COP",
                    "date": "'.date("Y-m-d").'",
                    "rate":'.$this->GetTasa("EUR").'
                  },
                  {
                    "to": "USD",
                    "date": "'.date("Y-m-d").'",
                    "rate":'.$this->GetTasa("USD").'
                  }
              ]
            } }
        '."//////////////");
       }
       
       if($_POST["from"] == "USD" || $_POST["from"] == "usd" ){
        print_r("//////////////".'{ "result": {
            "from": "usd",
            "conversion": [
                {
                    "to": "COP",
                    "date": "'.date("Y-m-d").'",
                    "rate":'.$this->GetTasa("USD").'
                  },
                  {
                    "to": "EUR",
                    "date": "'.date("Y-m-d").'",
                    "rate":'.$this->GetTasa("EUR").'
                  }
              ]
            } } 
        '."//////////////");
       }

       if($_POST["from"] == "COP" || $_POST["from"] == "cop" ){
        print_r("//////////////".'{ "result": {
            "from": "usd",
            "conversion": [
                {
                    "to": "USD",
                    "date": "'.date("Y-m-d").'",
                    "rate":'.$this->GetTasa("USD").'
                  },
                  {
                    "to": "EUR",
                    "date": "'.date("Y-m-d").'",
                    "rate":'.$this->GetTasa("EUR").'
                  }
              ]
            } } 
        '."//////////////");
       }
    }

    public function GetTasa($field){
        $arrayList = array();
        $HistoryTRMVo = new HistoryTRMVo();
        $HistoryTRMVo->isList = true;
        $generalDao = new GeneralDao();
        $generalDao->CustomQuery("SELECT * FROM {$HistoryTRMVo->nameTable} WHERE  typeMoney='{$field}'");
        while($HistoryTRMVo2 = $generalDao->GetVo($generalVo = clone $HistoryTRMVo)) {
          return $HistoryTRMVo2->money;
        }   
        return 0;
}

    private function CalculateImpostUnidad($imp,$priceUnit){
        $impost=0;
        $impost= ($imp/100);
        $total=($impost*$priceUnit);
        return $total;
    }

    private function GetRolePerson($idRole){
        $roleVo = new RoleVo();
        $roleVo->id = $idRole;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $roleVo);
        if($role2 = $generalDao->GetVo($generalVo = clone $roleVo)){
            return  $role2->name;
        }
    }

    private function GetDataPerson($id){
        if(isset($id)){
        $personsArrayList = array();
        $personVo = new PersonVo();
        $personVo->idSearchField = 0;
        $personVo->id = $id;
        //echo($id);
        //$personVo->isList = true;
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $personVo);
        while ($personVo2 = $generalDao->GetVo($generalVo = clone $personVo)) {
            $personsArrayList[] = $personVo2->names ." ". $personVo2->lastNames;
            $personsArrayList[] = $personVo2->cellPhone;
            $personsArrayList[] = $personVo2->email;
            $personsArrayList[] = $personVo2->idRole;
            $personsArrayList[] = $personVo2->id;
            //print_r($personVo2);
        }
        return $personsArrayList;
       }
    }

    public function ClearWordToSendMysql($word){
        $word = str_replace("'","\'" , $word);
        return $word;
    }


    private function GetUser()
    {   
        $personsArray = array();
        $personVo = new PersonVo();
        $personVo->idSearchField = 2;
        $personVo->isList = true;
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $personVo);
        while ($personVo2 = $generalDao->GetVo($generalVo = clone $personVo)) {
        
            if ($personVo2->idRole == 1 || $personVo2->idRole == 2 || $personVo2->idRole == 3 || $personVo2->idRole == 6 || $personVo2->idRole == 10 || $personVo2->idRole == 11 ){
                if($personVo2->state ==1){
                        $personsArray[] = $personVo2->id. "_" . htmlspecialchars($personVo2->names ." ".$personVo2->lastNames);
                    }
            }
        
    }
        return $personsArray;
    }
    
    private function ProductQuotation($objectVo , $quantity , $quotation , $percent , $price , $timeDelivery , $nodos){

        $generalDao = new GeneralDao();
        $object = new ProductVo();
        $object->id = null ;
        $object->idProduct = $objectVo->id;
        $object->typeProduct = $objectVo->typeProduct;
        $object->name = $this->ClearWordToSendMysql($objectVo->name);
        $object->brand = $this->ClearWordToSendMysql($objectVo->brand);
        $object->reference = $this->ClearWordToSendMysql($objectVo->reference);
        $object->presentation = $this->ClearWordToSendMysql($objectVo->presentation);
        $object->amount = $objectVo->amount;
        $object->minimumAmount = $objectVo->minimumAmount;
        $object->typeCurrency = $objectVo->typeCurrency;
        $object->priceUnit = $price;
        $object->priceUnitIndividual = $objectVo->priceUnit;
        $object->country = $objectVo->country;
        $object->nameCompany = $this->ClearWordToSendMysql($objectVo->nameCompany);
        $object->timeDelivery = $timeDelivery;
        error_reporting(0);
        $object->description = $this->ClearWordToSendMysql(preg_replace("[\n|\r|\n\r]", '', $objectVo->description));
        print_r($object->description);
        $object->IVA = $objectVo->IVA;
        $object->FLETE = $objectVo->FLETE;
        $object->percent = $percent;
        $object->quantity = $quantity;
        $object->namePlace = $objectVo->namePlace;
        $object->idQuotation= $quotation;
        $object->nodo= $this->ClearWordToSendMysql($nodos);
        //print_r($object);
        $result = $generalDao->Set($generalVo = clone $object);
        return $object;
    }

    private function ProductUpdate($objectVo , $quantity , $quotation , $percent , $price , $nodos ){
        print_r( $price."<br>");
        $generalDao = new GeneralDao();
        $object = new ProductVo();
        $object->id = $quotation;
        $object->idProduct = $objectVo->id;
        $object->typeProduct = $objectVo->typeProduct;
        $object->name = $this->ClearWordToSendMysql($objectVo->name);
        $object->brand = $this->ClearWordToSendMysql($objectVo->brand);
        $object->reference = $this->ClearWordToSendMysql($objectVo->reference);
        $object->presentation = $this->ClearWordToSendMysql($objectVo->presentation);
        $object->amount = $objectVo->amount;
        $object->minimumAmount = $objectVo->minimumAmount;
        $object->typeCurrency = $objectVo->typeCurrency;
        $object->priceUnit = $price;
        $object->country = $objectVo->country;
        $object->idCompany = $objectVo->idCompany;
        error_reporting(0);
        
        $object->description =  $this->ClearWordToSendMysql(preg_replace("[\n|\r|\n\r]", '', $objectVo->description));
        $object->percent = $percent;
        $object->IVA = $objectVo->IVA;
        $object->FLETE = $objectVo->FLETE;
        $object->quantity = $quantity;
        $object->nodo= $this->ClearWordToSendMysql($nodos);
        $object->idQuotation= $quotation;
        //print_r($object);
        $result = $generalDao->Update($generalVo = clone $object);
        return $object;
    }
    //============================================insert person ================================
    private function GetPersonsModify($id){

        $personList = array();
        $personVo= new UserQuotationVo();
        $personVo->idSearchField = 3;
        $personVo->idQuotation = $id;
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $personVo);
        while ($personVo2 = $generalDao->GetVo($generalVo = clone $personVo)) {
            $personList[] = $personVo2 ;
            $personList[10] = true; 
        }
        return $personList;
    }


    private function InsertPersonsQuotation($quotation){
        $arrayList = [];
        $cont = 0 ;
        $usersQuotationVo= new UserQuotationVo();
        $arrayUsersSelected=$_POST["usersSelected"];
        $generalDao = new GeneralDao();
        foreach ($_POST["usersSelected"] as $key) {
            $usersQuotationVo->id=null;
            $usersQuotationVo->idPerson = $key["idUser"];
            $usersQuotationVo->completeName = $key["name"];
            $usersQuotationVo->idCompany= $key["idCompany"];
            $usersQuotationVo->idQuotation = $quotation;
            $result = $generalDao->Set($generalVo = clone $usersQuotationVo);
            $arrayList[$cont] = $key;
            $arrayList[10] = false; 
            $cont++; 

        }
        return $arrayList;
    }

       private function InsertModification($quotation,$arrayPerson,$isCreated){
      //$personVo = unserialize($_SESSION['user']["person"]);
        //$_POST["date"] = preg_replace('~\x{00a0}~u', ' ', $_POST["date"]);
        $historymodificationVo = new HistoryModificationVo();
        $generalDao = new GeneralDao();
        $historymodificationVo->idUser=$arrayPerson[4];
        $date = new DateTime($_POST["date"]);
        $historymodificationVo->modificationDate=$date->format('Y-m-d H:i:s');
        $historymodificationVo->idQuotation=$quotation;
        if($isCreated){
            $historymodificationVo->description=$_POST["description"];    
        }else{
            $historymodificationVo->description="Se creo la cotización";
        }
        
        $generalDao->set($generalVo = clone $historymodificationVo);
    }

    private function GetDataQuotation($id){
      
        $quotationList = array();
        $quotationVo = new QuotationVo();
        $quotationVo->idSearchField = 0;
        $quotationVo->id = $id;
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $quotationVo);
        while ($quotationVo2 = $generalDao->GetVo($generalVo = clone $quotationVo)) {
            $quotationList[] = $quotationVo2 ;
        }
        return $quotationList;
    }

    








    private function GetProductsQuotation($id){
        $i=0;
        $dataQuotation = array();
        $productList = array();
        $productVo = new ProductVo();
        $productVo->idSearchField = 16;
        $productVo->idQuotation = $id;
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $productVo);
        while ($productVo2 = $generalDao->GetVo($generalVo = clone $productVo)) {
            error_reporting(0);
            $productVo2->description = null;
            $productVo2->name = preg_replace('"', "'",$productVo2->name) ;
            //print_r($productVo2->name ."<br>");
            $productList[$i]["obj"] = $productVo2 ;
     
            if($productVo2->namePlace=="Internacional"){

                $id = $this->GetIdCompany($productVo2->nameCompany);
                $dataPricesCompany = $this->GetDataPricesCompany($id);
                $dataAditionalImpostCompany = $this->GetDataAditionalImpostCompany($dataPricesCompany->id);
                
                $dataQuotation = $this->GetDataQuotation($productVo2->idQuotation);
                $totalPrice = $this->GetPriceWithImpost($productVo2->priceUnitIndividual,$dataAditionalImpostCompany,$dataQuotation[0]->typeQuotation);
                $productList[$i]["precioVenta"] = $this->GetPriceSale($totalPrice,$dataPricesCompany->ganancia);
                $productList[$i]["precioVentaCOP"] = $this->GetPriceSaleCOP($totalPrice,$dataPricesCompany->tpm,$dataPricesCompany->ganancia);
                
            }else{

                $productList[$i]["precioVenta"] = $productVo2->priceUnitIndividual;
                $productList[$i]["precioVentaCOP"] = $productVo2->priceUnitIndividual;
            }
           $i++;
           
        }
        return $productList;
    }

   
    function GetPriceSaleCOP($price,$tpm,$sale){
        $sale = $this->GetPercent($sale);
        
        $price =  $price+($price*$sale);
        return  $price*$tpm;
    }
    function GetPriceSale($price,$sale){
        $sale = $this->GetPercent($sale);
        $price =  $price+($price*$sale);
        return  $price;
    }
    function GetDataPricesCompany($idCompany){
        $pricesCompanyVo = new PricesCompanyVo();
        $pricesCompanyVo->idSearchField = 4;
        $pricesCompanyVo->idCompany = $idCompany;
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $pricesCompanyVo);
        $pricesCompanyVo2 = $generalDao->GetVo($generalVo = clone $pricesCompanyVo);
        return $pricesCompanyVo2;
    }

    function GetDataAditionalImpostCompany($idPricesCompany){
        $dataImpostAditional=array();
        $aditionalImpostVo = new AditionalsImpostVo();
        $aditionalImpostVo->idSearchField =3; 
        $aditionalImpostVo->idPricesCompany = $idPricesCompany;
         $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $aditionalImpostVo);
        while ( $aditionalImpostVo2 = $generalDao->GetVo($generalVo = clone $aditionalImpostVo)){
            array_push($dataImpostAditional,array("percent"=>$aditionalImpostVo2->percent,"DDP"=>$aditionalImpostVo2->DDP,"FCA"=>$aditionalImpostVo2->FCA,"CIF"=>$aditionalImpostVo2->CIF));
        }
        return $dataImpostAditional;
    }

    function GetPriceWithImpost($price,$porcentajes,$typeQuotation){
        $totalPercent = 0;
        for ($i=0; $i <count($porcentajes); $i++) { 
            
            if($porcentajes[$i][$typeQuotation]==1){
                $porcentaje = $this->GetPercent($porcentajes[$i]["percent"]);
                $totalPercent += floatval($price)*floatval($porcentaje);
            }
        }
        return floatval($totalPercent)+floatval($price);
    }

    function GetPercent($percent){
        $percent2 ="";
        
        if(strlen($percent)==1){
            $percent2 = ($percent/100);
        }else{
            $percent2 = ($percent/100);
        }
        return $percent2;
    } 
    function GetIdCompany($name){
        $companyVo = new CompanyVo();
        $companyVo->idSearchField =1; 
        $companyVo->name = $name;
         $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $companyVo);
        while ( $companyVo2 = $generalDao->GetVo($generalVo = clone $companyVo)){
            $id = $companyVo2->id;
        }
        return $id;
    }

    private function GetPersonCreateQuotation($id){
        $personList = array();
        $personCreateVo = new PersonCreateQuotationVo();
        $personCreateVo->idSearchField = 5;
        $personCreateVo->idQuotation = $id;
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $personCreateVo);
        while ($personCreateVo2 = $generalDao->GetVo($generalVo = clone $personCreateVo)) {
            $personList[] = $personCreateVo2 ;
        }
        return $personList;
    }

  

    private function GetPersonsQuotation($id){
        $quotationList = array();
        $quotationVo = new UserQuotationVo();
        $quotationVo->idSearchField = 3;
        $quotationVo->idQuotation = $id;
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $quotationVo);
        $i = 0;
        while ($quotationVo2 = $generalDao->GetVo($generalVo = clone $quotationVo)) {
            $quotationVo2->idCompany = $this->GetIdCompanyByIdPerson($quotationVo2->idPerson);
            $quotationList[] = $quotationVo2;
        }
        return $quotationList;
    }

       private function GetIdCompanyByIdPerson($idPerson){
        $PersonVo = new PersonVo();
        $PersonVo->id = $idPerson;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $PersonVo);
        if($PersonVo2 = $generalDao->GetVo($generalVo = clone $PersonVo)){
            return  $PersonVo2->idCompany;
        }
    }

    public function GetDataCompany($id)
    {       
        $companyVo = new CompanyVo();
        $generalDao = new GeneralDao();
        $companyVo->id = $id; 
        $generalDao->GetById($generalVo = clone $companyVo);

        if ($companyVo2 = $generalDao->GetVo($generalVo = clone $companyVo)) {
           //print_r($companyVo2); 
           return $companyVo2;
        }
    }

    public function GetNameCompany($id)
    {       
        $companyVo = new CompanyVo();
        $generalDao = new GeneralDao();
        $companyVo->id = $id; 
        $generalDao->GetById($generalVo = clone $companyVo);

        if ($companyVo2 = $generalDao->GetVo($generalVo = clone $companyVo)) {
           return $companyVo2->name;
        }
    }

    public function GetProducts($products , $i){
       
        $htmlProducts = null;
        $inventoryVo = new InventoryVo();
        $inventoryVo->id = $products[$i]->id;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $inventoryVo);
        $inventoryVo2 = $generalDao->GetVo($generalVo= clone $inventoryVo);//Configuracion de la tabla y el espacio
        $htmlProducts .= $inventoryVo2->description;
        return $htmlProducts;
    }

  

   
    private function UpdatePersonQuotation($arrayPerson,$id , $idQuotation){
        $usersQuotationVo= new PersonCreateQuotationVo();
        $personVo = unserialize($_SESSION['user']["person"]);
        $generalDao = new GeneralDao();
        if(empty($arrayPerson)){
            $usersQuotationVo->completeName =  $personVo->names."  ".$personVo->lastNames;
            $usersQuotationVo->rolName = $this->GetRolePerson($personVo->idRole);
            $usersQuotationVo->cellPhone = $personVo->cellPhone ;
            $usersQuotationVo->email =   $personVo->email;
            $usersQuotationVo->state =   $personVo->state;
            $usersQuotationVo->idPerson =   $personVo->id;
            }
            else{
            $usersQuotationVo->completeName =  $arrayPerson[0];
            $usersQuotationVo->rolName = $this->GetRolePerson($arrayPerson[3]);
            $usersQuotationVo->cellPhone = $arrayPerson[1] ;
            $usersQuotationVo->email =  $arrayPerson[2];
            $usersQuotationVo->state =  1;
            $usersQuotationVo->idPerson =   $arrayPerson[4];
                    //print_r($arrayPerson);
                    //print_r("lol".$id."");
            }
     
        $usersQuotationVo->idQuotation =  $idQuotation;
        $usersQuotationVo->id =  $idQuotation;
        $result = $generalDao->Update($generalVo = clone $usersQuotationVo);
        return $result;

    }



    private function InsertPersonCreate($arrayPerson , $id){

        $usersQuotationVo= new PersonCreateQuotationVo();
        $personVo = unserialize($_SESSION['user']["person"]);
        $generalDao = new GeneralDao();
        if($arrayPerson==null){
            $usersQuotationVo->completeName =  $personVo->names."  ".$personVo->lastNames;
            $usersQuotationVo->rolName = $this->GetRolePerson($personVo->idRole);
            $usersQuotationVo->cellPhone = $personVo->cellPhone ;
                $usersQuotationVo->email =   $personVo->email;
                 $usersQuotationVo->state =   $personVo->state;
                 $usersQuotationVo->idPerson =   $personVo->id;
        }else{
            $usersQuotationVo->completeName =  $arrayPerson[0];
            $usersQuotationVo->rolName = $this->GetRolePerson($arrayPerson[3]);
            $usersQuotationVo->cellPhone = $arrayPerson[1] ;
            $usersQuotationVo->email =  $arrayPerson[2];
             $usersQuotationVo->state = 1;
             $usersQuotationVo->idPerson =   $arrayPerson[4];
        }
        $usersQuotationVo->idQuotation =  $id;
        $result = $generalDao->Set($generalVo = clone $usersQuotationVo);
    }
    public function GetImpostIva()
    {
        $iva= 0;
        $impostVo = new ImpostVo();
        $impostVo->idSearchField = 1;
        $impostVo->name = "IVA";
        $generalDao = new GeneralDao();
         $generalDao->GetByField($generalVo = clone $impostVo);
        if($impostVo2 = $generalDao->GetVo($generalVo = clone $impostVo)){
                $iva = $impostVo2->percent;
        }else{
            $iva = 19;
        }
        return $iva;
    }

    private function SetDataToTpl()
    {
        echo $this->core->get($this->pageTpl, $this->data);
    }

   

}

