<?php
require_once(__DIR__ . '/../../../../controller/general/Config.php');
require_once(Config::PATH . Config::CONTROLLER.Config::GENERAL . 'ManageArrays.php');
require_once(Config::PATH . Config::BACKEND . 'modules/CompanyVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/ProductVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/InventoryVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/AditionalsImpostVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/PricesCompanyVo.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralDao.php');
require_once(Config::PATH . Config::CONTROLLER_LIB . 'vendor/phpoffice/phpexcel/Classes/PHPExcel/IOFactory.php');


class AjaxDataInventoryPrice
{

    private $isActionFromAjax;
    private $optionInputForm;
    private $idDataInputForm;
    private $nameCompayInputForm;

    function __construct()
    {
        if (isset($_POST['isActionFromAjax'])) {
            $this->isActionFromAjax = $_POST['isActionFromAjax'];
            $this->optionInputForm = $_POST['optionInputForm'];
            $this->idDataInputForm = $_POST['idDataInputForm'];
             $this->nameCompayInputForm = $_POST['nameCompayInputForm'];

            if ($this->isActionFromAjax == "true") {
                if ($this->optionInputForm == 3) {
                   
                }else {
                    $this->GetDataInJsonForTbl();
                }
            }
        }
    }

    

    public function GetDataInJsonForTbl()
    {
        $dataPricesCompany = $this->GetDataPricesCompany($this->idDataInputForm);
        $dataAditionalImpostCompany = $this->GetDataAditionalImpostCompany($dataPricesCompany->id);
        $existData = false;
        $inventoryVo = new InventoryVo();
        $inventoryVo->nameCompany = $this->nameCompayInputForm;
        $inventoryVo->idSearchField=11;
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $inventoryVo);
        $string = "
        {\"data\": [";
        while ($inventoryVo2 = $generalDao->GetVo($generalVo = clone $inventoryVo)) {
            if($inventoryVo2->namePlace=="Internacional"){
                $existData = true;
                $impostNum = $this->GetProcentImpost($dataAditionalImpostCompany , $_POST["typeQuotation"]);
                $totalPrice = $this->GetPriceWithImpost(str_replace(".","",$inventoryVo2->priceUnit),$dataAditionalImpostCompany,$_POST["typeQuotation"]);
               
                $json = array();
                $json[] = $inventoryVo2->id;
                $json[] = htmlspecialchars($inventoryVo2->name);
                $json[] = htmlspecialchars($inventoryVo2->reference);
                $json[] = $inventoryVo2->presentation;

            $json[] = ($dataPricesCompany->typeMoney == "") ? "USD" : $dataPricesCompany->typeMoney  ;
            if(is_numeric($inventoryVo2->priceUnit)){
                $json[] = "$".str_replace(".","", str_replace(",",".",$inventoryVo2->priceUnit))." ".$dataPricesCompany->typeMoney;
                $json[] = "$".str_replace(",","." ,($totalPrice)) ." ".$dataPricesCompany->typeMoney;
                $json[] = "$".str_replace(",","." ,number_format($this->GetPriceCOP($totalPrice,$dataPricesCompany->tpm))). " COP";
                $json[] = "$".str_replace(",","." ,number_format($this->GetPriceSaleCOP(floatval(str_replace(",","",str_replace(".","",$inventoryVo2->priceUnit))),$dataPricesCompany->tpm,$dataPricesCompany->ganancia ,$impostNum))). " COP";
                $json[] = "$".str_replace(",","." ,($this->GetPriceSale(floatval(str_replace(",","",str_replace(".","",$inventoryVo2->priceUnit))),$dataPricesCompany->ganancia , $impostNum))). " ".$dataPricesCompany->typeMoney;
            }else{
                $json[] = "$".$inventoryVo2->priceUnit." ".$dataPricesCompany->typeMoney;
                $json[] = "$".str_replace(".","," ,$totalPrice ." ".$dataPricesCompany->typeMoney);
                $json[] = "$".str_replace(",","." ,number_format($this->GetPriceCOP($totalPrice,$dataPricesCompany->tpm))). " COP";
                $json[] = "$".str_replace(",","." ,number_format($this->GetPriceSaleCOP(floatval(str_replace(".","",$inventoryVo2->priceUnit)),$dataPricesCompany->tpm,$dataPricesCompany->ganancia ,$impostNum ))). " COP";
                $json[] = "$".str_replace(".","," ,$this->GetPriceSale(floatval(str_replace(".","",$inventoryVo2->priceUnit)),$dataPricesCompany->ganancia , $impostNum )). " ".$dataPricesCompany->typeMoney;
            }
            

            $string .= json_encode($json) . ",";
            }
        }
        $string .= "-]}-";
        if($existData){
            echo  str_replace(",-]}-", "]}", $string);
        }else{
            echo "{ \"data\": [] }";
        }
    }

    function GetDataPricesCompany($idCompany){
        $pricesCompanyVo = new PricesCompanyVo();
        $pricesCompanyVo->idSearchField = 4;
        $pricesCompanyVo->idCompany = $idCompany;
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $pricesCompanyVo);
        $pricesCompanyVo2 = $generalDao->GetVo($generalVo = clone $pricesCompanyVo);
        return $pricesCompanyVo2;
    }



    function GetDataAditionalImpostCompany($idPricesCompany ){
        $dataImpostAditional=array();
        $aditionalImpostVo = new AditionalsImpostVo();
        $aditionalImpostVo->idSearchField =3; 
        $aditionalImpostVo->idPricesCompany = $idPricesCompany;
         $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $aditionalImpostVo);
        while ( $aditionalImpostVo2 = $generalDao->GetVo($generalVo = clone $aditionalImpostVo)){
           array_push($dataImpostAditional,array("percent"=>$aditionalImpostVo2->percent,"DDP"=>$aditionalImpostVo2->DDP,"FCA"=>$aditionalImpostVo2->FCA,"CIF"=>$aditionalImpostVo2->CIF));
        }
        return $dataImpostAditional;
    }

    function GetPriceWithImpost($price,$porcentajes,$typeQuotation){
        $totalPercent = 0;
        
        for ($i=0; $i <count($porcentajes); $i++) { 
            if($porcentajes[$i][$typeQuotation]==1){
                 $porcentaje = $this->GetPercent($porcentajes[$i]["percent"]);
                // echo $porcentaje. "// ";
                 $totalPercent +=  floatval($price)*$porcentaje;
            }
        }
        return $totalPercent+ floatval($price);
    }

    function GetProcentImpost($porcentajes , $typeQuotation){
        $porcentaje = 0;
        for ($i=0; $i <count($porcentajes); $i++) { 
            if($porcentajes[$i][$typeQuotation]==1){
                 $porcentaje += $this->GetPercent($porcentajes[$i]["percent"]);
                // echo $porcentaje. "// ";
            }
        }
        return $porcentaje;
    }

    function GetPriceCOP($price,$tpm){
        return $price*$tpm;
    }
    function GetPriceSaleCOP($price,$tpm,$sale ,$impost){

        $sale = $impost + ($this->GetPercent($sale));
        $price =  $price + ($price* $sale);
        return  round(($price*$tpm));
    }
    function GetPriceSale($price,$sale , $impuesto){
      
        $sale = $impuesto + ($this->GetPercent($sale));
        $price =  $price +(floatval($price)*$sale);

        return  floatval($price);
    }
    function GetPercent($percent){
        $percent2 ="";
        
       
            $percent2 = (floatval(str_replace(",",".",$percent))/100);
        
        return $percent2;
    }
    function GetCompany($idCompany)
    {
        $companyVo = new CompanyVo();
        $companyVo->id = $idCompany;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $companyVo);
        if ($companyVo2 = $generalDao->GetVo($generalVo = clone $companyVo)) {
            return "" . $companyVo2->name;
        }
        return "Problema en la consulta";
    }

    

   
    /*******************************LOAD TEMPLATE EXCEL**********************************/

    /********************************************************************/

}

$ajaxDataInventory = new AjaxDataInventoryPrice();