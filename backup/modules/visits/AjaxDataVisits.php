<?php
require_once(__DIR__ . '/../../../../controller/general/Config.php');
require_once(Config::PATH . Config::CONTROLLER.Config::GENERAL . 'ManageArrays.php');
require_once(Config::PATH . Config::BACKEND . 'modules/PersonVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/CompanyVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/UserVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/RoleVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/VisitsVo.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralDao.php');

class AjaxDataVisits
{

    private $isActionFromAjax;
    private $optionInputForm;
    private $idDataInputForm;
    public $dataList = [];

    function __construct()
    {
        if (isset($_POST['isActionFromAjax'])) {
            $this->isActionFromAjax = $_POST['isActionFromAjax'];
            $this->optionInputForm = $_POST['optionInputForm'];
            $this->idDataInputForm = $_POST['idDataInputForm'];
            if(isset($_POST["listData"])){
                $this->dataList = $_POST["listData"];  
            }
            if ($this->isActionFromAjax == "true") {
                if ($this->optionInputForm == 3) {
                    $this->SetDataInControls();
                }else if ($this->optionInputForm == 5) {
                    $this->UpdateStateUser();
                }else if ($this->optionInputForm == 6) {
                    $this->GetTypeVisitsLenght();
                }else if($this->optionInputForm == 7){
                    $this->CallUserByCompany($_POST["idCompany"]);
                }else {
                    $this->GetDataInJsonForTbl();
                }
            }
        }
    }


    public function GetLengthUsers(){
        $arrayList = array();
        $personVo = new userVo();
        $personVo->isList = true;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $personVo);
        while ($personVo2 = $generalDao->GetVo($generalVo = clone $personVo)) {
            $arrayList[] =$personVo2 ;
        }
        print_r( json_encode(count($arrayList)) );
    }

    public function GetDataInJsonForTbl()
    {
        $existData = false;
        $visitsVo = new VisitsVo();
        $visitsVo->isList = true;
        $generalDao = new GeneralDao();
        //$this->generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $visitsVo);
        $cont = 1; 
        $string = "
        {\"data\": [";
        while ($visitsVo2 = $generalDao->GetVo($generalVo = clone $visitsVo)) {
            $clientVo = $this->GetClientData($visitsVo2->idClient);
            $json = array();
            if($this->GetStateCompany($clientVo->idCompany)){
                if($clientVo->state == 1){
                    $existData = true;
                    if($clientVo->idRole == 7 || $clientVo->idRole == 9 ){
                $json[] = $cont;
                $json[] = htmlspecialchars(ucwords(strtolower($clientVo->names)));
                $json[] = htmlspecialchars(ucwords(strtolower($clientVo->lastNames)));
                $json[] = htmlspecialchars($clientVo->cellPhone);
                $json[] = htmlspecialchars($clientVo->telephone);
                $json[] = htmlspecialchars($clientVo->email);
                $json[] = htmlspecialchars($clientVo->address);
                $json[] = htmlspecialchars(ucfirst($clientVo->city));
                $json[] = $this->GetTypeRole($clientVo->idRole);
                $json[] = ucwords(strtolower($this->GetCompany($clientVo->idCompany)));
                $json[] = ucwords(strtolower($this->GetPersonName($visitsVo2->idPerson)));
                $json[] = $visitsVo2->dateCreate;
                $button = "";
                if(!in_array( "historico_visitas" , $this->dataList)){
                    $button = "<tip title='Historico Eventos' class='green-tooltip' data-toggle='tooltip' data-placement='top'><button onclick='location.href = \"VisitsDescription?idVisit=$visitsVo2->id\"' class='btn btn-success  btn-xs'><i class='fa fa-book'></i></button></tip> ";
                }
                $json[] = $button;
                $cont++;
                
            $string .= json_encode($json) . ",";
            }
         }
         }
        }

        $string .= "-]}-";
        if($existData){
            echo  str_replace(",-]}-", "]}", $string);
        }else{
            echo "{ \"data\": [] }";
        }
    }

    function GetClientData($id)
    {
        $PersonVo = new PersonVo();
        $PersonVo->id = $id;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $PersonVo);
        if ($PersonVo2 = $generalDao->GetVo($generalVo = clone $PersonVo)) {
            return $PersonVo2;
        }
        return "Problema en la consulta";
    }

    function GetPersonName($idPerson)
    {
        $personVo = new PersonVo();
        $personVo->id = $idPerson;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $personVo);
        if ($personVo2 = $generalDao->GetVo($generalVo = clone $personVo)) {
            return "" . $personVo2->names." ".$personVo2->lastNames;
        }
        return "Problema en la consulta";
    }

    function GetUserVo($idPerson)
    {
        $userVo2 = new UserVo();
        $userVo = new UserVo();
        $userVo->idPerson = $idPerson;
        $userVo->idSearchField = 5;
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $userVo);
        if ($userVo2 = $generalDao->GetVo($generalVo = clone $userVo)) {
            return $userVo2;
        }
        return $userVo2;
    }

    function UpdateStateUser()
    {
        $userVo = new UserVo();
        $userVo->id = $this->idDataInputForm;
        $userVo->idSearchField = 0;
        $userVo->state = $_POST["state"];
        $userVo->idUpdateField = 3;
        $generalDao = new GeneralDao();
        $generalDao->UpdateByField($generalVo = clone $userVo);
    }


    function GetIdUserSession()
    {
            session_start();
            $userVo = unserialize($_SESSION['user']["user"]);
            $personVo = unserialize($_SESSION['user']["person"]);
            print_r($personVo->idRole);
    }

    function GetTypeRole($idRole)
    {
        $roleVo = new RoleVo();
        $roleVo->id = $idRole;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $roleVo);
        if ($roleVo2 = $generalDao->GetVo($generalVo = clone $roleVo)) {
            return "" . $roleVo2->name;
        }
        return "Problema en la consulta";
    }

    function GetCompany($idCompany)
    {
        $companyVo = new CompanyVo();
        $companyVo->id = $idCompany;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $companyVo);
        if ($companyVo2 = $generalDao->GetVo($generalVo = clone $companyVo)) {
            return "" . $companyVo2->name;
        }
        return "Problema en la consulta";
    }


     function GetStateCompany($idCompany)
    {
        $companyVo = new CompanyVo();
        $companyVo->id = $idCompany;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $companyVo);
        if ($companyVo2 = $generalDao->GetVo($generalVo = clone $companyVo)) {
            return $companyVo2->state;
        }
        return false;
    }

 function GetTypeVisitsLenght()
    {
        $arrayList =  array();
        $VisitsVo = new VisitsVo();
        $generalDao = new GeneralDao();
        $generalDao->CustomQuery("SELECT * FROM {$VisitsVo->nameTable}");
        print_r($generalDao->GetLength());
    }



    private function CallUserByCompany($rol){
        $arraylist = null;
        $userVo = new PersonVo();
        $userVo->idSearchField = 12;
        $userVo->idCompany = $rol;
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $userVo);
        $arraylist .= "*-*";
         while($userVo2 = $generalDao->GetVo($generalVo = clone $userVo)) {
            if($userVo2->state == 1){
                 $arraylist .= $userVo2->id."#".htmlspecialchars($userVo2->names." ".$userVo2->lastNames." - ".$this->GetTypeRole($userVo2->idRole))."+";
               }
          }
          
         $arraylist .= "*-*";
        print_r($arraylist);
    }



}

$ajaxDataUser = new AjaxDataVisits();
