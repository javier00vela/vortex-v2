function DisabledClients() {
    $("#ClientsLst").attr("disabled", "true");

}


function LoadTable() {
    RemoveActionModule(14);
    GetArrayFromDataModule(14);
    setTimeout(function() {
        var parameters = {
            "isActionFromAjax": true,
            "optionInputForm": 0,
            "idDataInputForm": 0,
            "listData": arrayResponse,
        };


        $('#userTbl').DataTable({
            responsive: true,
            "drawCallback": function(settings) {
                $('[data-toggle="tooltip"]').tooltip()
            },
            language: {
                "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
            },
            "ajax": {
                url: "view/html/modules/visits/AjaxDataVisits.php",
                method: "POST",
                data: parameters
            }
        });
    }, 500);
}



function SelectDataRol() {

    $("#CompanyLst").change(function() {
        var parameters = {
            "optionInputForm": 7,
            "isActionFromAjax": true,
            "idDataInputForm": 1,
            "idCompany": $(this).val()
        };
        $.ajax({
            url: "view/html/modules/visits/AjaxDataVisits.php",
            method: "POST",
            data: parameters,
            success: function(resp) {
                if (resp != "*-**-*") {
                    var options = resp.split("*-*")[1];
                    var selections = options.split("+");
                    console.log(selections);
                    $('#ClientsLst').empty();
                    $('#ClientsLst').append("<option>--Clientes y/o prospecto--</option>");
                    for (var i = 0; i < selections.length; i++) {
                        if (selections[i] != "") {
                            $('#ClientsLst').append($('<option>', {
                                value: selections[i].split('#')[0],
                                text: selections[i].split('#')[1]
                            }));
                        }
                    }
                    if (selections[0] != "") {
                        $("#ClientsLst").removeAttr('disabled');
                    } else {

                        $("#ClientsLst").attr("disabled", "true");
                    }
                } else {
                    $("#messageModal").html("<div class='text-center alert alert-danger'>No hay usuarios registrados por parte de esta empresa</div>");
                }
            }
        });

    });

}


$(function() {
    DisabledClients();
    LoadTable();
    SelectDataRol();
});

/*
//function ReplaceImage(){
function ConvertImgToJson(img){
    console.log(img);
    var file_tmp_name = {
        tmp_name:img["file"]
    }
    var fileData = {
        file: {
            modified: img.lastModifiedDate,
            tpm_name: img.tpm_name,
            name: img.name,
            size: img.size,
            type: img.type
        }
    }
    alert(JSON.stringify(file_tmp_name));
    return JSON.stringify(fileData);
}*/