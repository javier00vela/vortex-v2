function ChangeState(id, state, i, name, notification, number) {
    var parameters = {
        "isActionFromAjax": true,
        "optionInputForm": 9,
        "idDataInputForm": 0,
        "selectedNotification": $("#selectedNotification-" + id).val(),
        "daysNotification": $("#daysNotification-" + id).val(),
        "id": id,
        "state": state
    };

    $.ajax({
        url: "view/html/modules/mailsallows/AjaxDataMailsAllows.php",
        type: 'post',
        data: parameters,
        success: function(data) {

            if ($("#" + i).text() == "Correo Desactivado") {
                $("#validateBTN2-" + i).html('<div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4"><div class="form-group"><b><span>' + name + '</span></b><button  id="' + i + '"  class="A btn btn-success btn-block" onclick="ChangeState(' + id + ',' + state + ',' + i + ',' + "'" + name + "'" + ',' + "'" + notification + "'" + ',' + number + ')">Correo Activado</button></div></div><div  class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4"><div class="form-group"><label>Tipo De Notificación</label><select onchange="ChangeState2(' + id + ',' + state + ',' + i + ',' + "'" + name + "'" + ',' + "'" + notification + "'" + ',' + number + ')" class="form-control"id="selectedNotification-' + id + '"><option value="anterior_dia">días anteriores</option><option value="post_dia">días posteriores</option><option value="mismo_dia">el mismo día</option></select></div></div> <div  class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4"><div class="form-group"><label>Días De Notificación</label><input onchange="ChangeState2(' + id + ',' + state + ',' + i + ',' + "'" + name + "'" + ',' + "'" + notification + "'" + ',' + number + ')"  type="number" ' + ((notification == "mismo_dia") ? 'disabled' : ' ') + ' id="daysNotification-' + id + '" min="1" class="form-control" value="' + ((number) ? number : 1) + '"  > </div>  </div></div>');
            } else {
                $("#validateBTN2-" + i).html('<div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4"><div class="form-group"><b><span>' + name + '</span></b><button  id="' + i + '" class="D btn btn-danger btn-block"  onclick="ChangeState(' + id + ',' + state + ',' + i + ',' + "'" + name + "'" + ',' + "'" + notification + "'" + ',' + number + ')"">Correo Desactivado</button></div></div><div  class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4"><div class="form-group"><label>Tipo De Notificación</label><select disabled class="form-control"id="selectedNotification-' + id + '"><option value="anterior_dia">días anteriores</option><option value="post_dia">días posteriores</option><option value="mismo_dia">el mismo día</option></select></div></div> <div  class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4"><div class="form-group"><label>Días De Notificación</label><input disabled type="number" id="daysNotification-' + id + '" min="1" class="form-control" value="' + ((number) ? number : 1) + '"  > </div>  </div></div>');

            }

        }
    })

}

function ChangeState2(id, state, i, name, notification, number) {
    var parameters = {
        "isActionFromAjax": true,
        "optionInputForm": 10,
        "idDataInputForm": 0,
        "selectedNotification": $("#selectedNotification-" + id).val(),
        "daysNotification": $("#daysNotification-" + id).val(),
        "id": id,
        "state": state
    };

    $.ajax({
        url: "view/html/modules/mailsallows/AjaxDataMailsAllows.php",
        type: 'post',
        data: parameters,
        success: function(data) {
            if ($("#selectedNotification-" + id).val() != "mismo_dia") {
                $("#daysNotification-" + id).removeAttr("disabled");
            } else {
                $("#daysNotification-" + id).attr("disabled", "true");
            }

        }
    })

}


function LoadRoles() {
    var parameters = {
        "isActionFromAjax": true,
        "optionInputForm": 5,
        "idDataInputForm": 0,
    };

    $.ajax({
        url: "view/html/modules/mailsallows/AjaxDataMailsAllows.php",
        type: 'post',
        data: parameters,
        success: function(data) {
            EachRoles(JSON.parse(data));
        }
    })
}

function LoadModules(id) {
    var parameters = {
        "isActionFromAjax": true,
        "optionInputForm": 7,
        "idDataInputForm": 0,
        "idRol": id
    };

    $.ajax({
        url: "view/html/modules/mailsallows/AjaxDataMailsAllows.php",
        type: 'post',
        data: parameters,
        success: function(data) {

            EachModules(JSON.parse(data), id);
        }
    })
}

function EachRoles(roles) {
    for (var i = 0; i < roles.length; i++) {
        if (roles[i].name != "Cliente" && roles[i].name != "Prospecto" && roles[i].name != "Proveedor" && roles[i].name != "SubDistribuidor") {
            $("#card-roles").append(' <div class="col-lg-3 col-xs-6"><div class="shadow-lg rounded small-box bg-' + roles[i].color + '"><div class="inner inner-1"><h4><b>' + roles[i].name + '</b></h4><h2></h2>&nbsp</div><div class="icon"><i class="fa fa-envelope"></i></div><a href="#" data-toggle="modal" data-target="#ModulePopUpAdd" onclick="LoadModules(' + roles[i].id + ')" class="small-box-footer">Ver Permisos <i class="fa fa-arrow-circle-right"></i></a></div></div>');
        }
    }

}

function EachModules(module, id) {

    $("#modulesList").html("");

    for (var i = 0; i < module.length; i++) {
        if (module[i].events.length > 0) {

            $("#modulesList").append('<div style="  box-shadow: 0 0 10px black;" id="myBox-' + i + '" class="box shadow-lg box-primary form-group col-xs-12 col-sm-12 col-md-12 col-lg-12"><div  class="box-header with-border text-center">' + module[i].nombre + '<hr><div id="validateBTN' + i + '" class="row"> </div></div></div> ');
            if (window.innerWidth >= 740) {
                $("#myBox-" + i).css({ "width": '100%', "margin": "5px" });
            } else {

                $("#myBox-" + i).css({ "width": '100%' });
            }

            for (var e = 0; e < module[i].events.length; e++) {
                console.log(module[i].events[e].event);
                if (module[i].events[e].event.state == 1) {
                    console.log(module[i].events[e]);
                    $("#validateBTN" + i).append('<div id="validateBTN2-' + module[i].events[e].event.id + '"><div  class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4"><div class="form-group"><b><span>' + module[i].events[e].nameEvent + '</span></b><button  class="A btn btn-success btn-block" id="' + module[i].events[e].event.id + '" onclick="ChangeState(' + module[i].events[e].event.id + ',' + module[i].events[e].event.state + ',' + module[i].events[e].event.id + ',' + "'" + module[i].events[e].nameEvent + "'" + ',' + "'" + module[i].events[e].event.typeNotification + "'" + ',' + module[i].events[e].event.numberNotification + ')">Correo Activado</button></div></div><div  class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4"><div class="form-group"><label>Tipo De Notificación</label><select onchange="ChangeState2(' + module[i].events[e].event.id + ',' + module[i].events[e].event.state + ',' + module[i].events[e].event.id + ',' + "'" + module[i].events[e].nameEvent + "'" + ',' + "'" + module[i].events[e].event.typeNotification + "'" + ',' + module[i].events[e].event.numberNotification + ')" class="form-control"id="selectedNotification-' + module[i].events[e].event.id + '"><option value="anterior_dia">días anteriores</option><option value="post_dia">días posteriores</option><option value="mismo_dia">el mismo día</option></select></div></div> <div  class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4"><div class="form-group"><label>Días De Notificación</label><input onchange="ChangeState2(' + module[i].events[e].event.id + ',' + module[i].events[e].event.state + ',' + module[i].events[e].event.id + ',' + "'" + module[i].events[e].nameEvent + "'" + ',' + "'" + module[i].events[e].event.typeNotification + "'" + ',' + module[i].events[e].event.numberNotification + ')"  type="number" ' + ((module[i].events[e].event.typeNotification == "mismo_dia") ? 'disabled' : ' ') + ' id="daysNotification-' + module[i].events[e].event.id + '" min="1" class="form-control" value="' + ((module[i].events[e].event.numberNotification) ? module[i].events[e].event.numberNotification : 1) + '"  > </div>  </div></div>');

                } else {
                    $("#validateBTN" + i).append('<div id="validateBTN2-' + module[i].events[e].event.id + '"><div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4"><div class="form-group"><b><span>' + module[i].events[e].nameEvent + '</span></b><button id="' + module[i].events[e].event.id + '"  class="D btn btn-danger btn-block" onclick="ChangeState(' + module[i].events[e].event.id + ',' + module[i].events[e].event.state + ',' + module[i].events[e].event.id + ',' + "'" + module[i].events[e].nameEvent.replace('"', '') + "'" + ')">Correo Desactivado</button></div></div><div  class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4"><div class="form-group"><label>Tipo De Notificación</label><select disabled class="form-control"id="selectedNotification-' + module[i].events[e].event.id + '"><option value="anterior_dia">días anteriores</option><option value="post_dia">días posteriores</option><option value="mismo_dia">el mismo día</option></select></div></div> <div  class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4"><div class="form-group"><label>Días De Notificación</label><input disabled type="number" id="daysNotification-' + module[i].events[e].event.id + '" min="1" class="form-control" value="' + ((module[i].events[e].event.numberNotification) ? module[i].events[e].event.numberNotification : 1) + '"  > </div>  </div></div>');


                }
                $("#selectedNotification-" + module[i].events[e].event.id + " option[value='" + module[i].events[e].event.typeNotification + "']").attr("selected", true);
            }
        }
    }

}




/*********************************CLICK EVENTS**************************/

$(function() {
    LoadRoles();
    $(window).resize(function() {
        $("#ModulePopUpAdd").modal('hide')
    });
});