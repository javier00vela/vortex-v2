<?php
require_once(Config::PATH . Config::CONTROLLER . 'general/AccessManager.php');
require_once(Config::PATH . Config::CONTROLLER . 'general/UtilJquery.php');
require_once(Config::PATH . Config::VIEW_HTML . 'general/comps/GeneralCtx.php');
require_once(Config::PATH . Config::VIEW_HTML . 'general/comps/GeneralLst.php');
require_once(Config::PATH . Config::VIEW_HTML . 'general/comps/GeneralWithDataLst.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralDao.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/ModuleVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/ModuleEventVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/EventsVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/ModuleRoleVo.php');

class MailsAllowsView
{
    private $optionInputForm;
    private $isUpdateUserForm;
    private $idDataInputForm;
    /*=============================================
                  Session Vars
    =============================================*/
   
    /*=============================================
                  Template Vars
    =============================================*/
    private $utilJQ;
    private $pageTpl, $data, $core;
    /*=============================================
                Controls Vars HTML
    =============================================*/
    
    /*=============================================
                Data Vars
    =============================================*/
    private $generalDao;
    private $nameEventCtx;
    private $listModule;


    function __construct($pageTpl, $data, $core)
    {
        $this->data = $data;
        $this->core = $core;
        $this->pageTpl = $pageTpl;

            $this->CreateComponents();
            $this->UseDataFromThePostback();
        
    }

    private function UseDataFromThePostback()
    {
        if (isset($_POST['optionInputForm'])) {
            $this->optionInputForm = $_POST['optionInputForm'];
            $this->idDataInputForm = $_POST['idDataInputForm'];
            $this->generalDao = new GeneralDao();
            switch ($this->optionInputForm) {
                case "1": {

                    $this->SetData();

                    break;
                }
                case "2": {
                    $this->DeleteData();
                    break;
                }
                case "4": {
                    $this->SetUpdate();
                    break;
                }
            }
        }

        $this->AssignDataToTpl();

    }

    private function CreateComponents()
    {
        $this->nameEventCtx = new GeneralCtx("nameEventCtx","Nombre Evento",null,true);
        $this->listModule = new GeneralWithDataLst($this->GetModules(), "listModules", false,true,"--Lista de modulos--",true);
       
        $this->utilJQ = new UtilJquery("Impost");
    }

    private function GetModules(){
        $companiesArray = array();
        $ModulesVo = new ModuleVo();
        $ModulesVo->isList = true;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $ModulesVo);
        while ($ModulesVo2 = $generalDao->GetVo($generalVo = clone $ModulesVo)) {
     
                $companiesArray[] = $ModulesVo2->id."_".$ModulesVo2->subName;
            
        }
        return $companiesArray;
    }
    private function SetDataToVo()
    {
        $EventsVo = new EventsVo();
        $EventsVo->name = $_POST["nameEventCtx"];
        $EventsVo->isAllow = 1;
       
        return $EventsVo;
    }
    private function SetData()
    {
        $EventsVo = $this->SetDataToVo();

        $EventsVo->id = null;
        $result = $this->generalDao->Set($generalVo = clone $EventsVo);
        if (isset($result)) {
            $this->GenerateModulesRoles($result);
            $message = 'MessageSwalBasic("Registrado!","Permisos Registrados Correctamente","success");';
            $this->utilJQ->AddFunctionJavaScript($message);
        }

    }

    private function GenerateModulesRoles($id){
        $ModuleVo = new ModuleEventVo();
        for ($i=1; $i < 13 ; $i++) {  
            $ModuleVo->idRol = $i;
            $ModuleVo->idEvent = $id;
            $ModuleVo->idModule = $_POST["listModules"];
            $ModuleVo->state = 1;
            $ModuleVo->typeNotification = "mismo_dia";
            $ModuleVo->numberNotification = 1;
            $result = $this->generalDao->Set($generalVo = clone $ModuleVo);
        }

    }


    private function SetUpdate()
    {
        $result = $this->generalDao->Update($generalVo = clone $this->SetDataToVo());
        if (isset($result)) {
            $message = 'MessageSwalBasic("Modificado!","Impuesto Modificado Correctamente","success");';
            $this->utilJQ->AddFunctionJavaScript($message);
        }
    }
    private function DeleteData()
    {
        $message = "";
        $impostVo = new ImpostVo();
        $impostVo->id = $this->idDataInputForm;
        if ($this->generalDao->Delete($generalVo = clone $impostVo)) {
            $message = 'MessageSwalBasic("Eliminado!","Impuesto Eliminado Correctamente","success");';
        } else {
            $message = 'MessageSwalBasic("Error!","Problema con la base de datos","error");';
        }
        $this->utilJQ->AddFunctionJavaScript($message);
    }

    private function AssignDataToTpl()
    {
      
        $this->data->assign('nameEventCtx', $this->nameEventCtx->paint());
        $this->data->assign('listModules', $this->listModule->paint());
         $this->data->assign('tempParameter', time());
        $this->data->assign('jquery', $this->utilJQ->PaintJQ());
        $this->SetDataToTpl();
    }   
    private function SetDataToTpl()
    {
        echo $this->core->get($this->pageTpl, $this->data);
    }
  


}   