<?php
require_once(__DIR__ . '/../../../../controller/general/Config.php');
require_once(Config::PATH . Config::CONTROLLER.Config::GENERAL . 'ManageArrays.php');
require_once(Config::PATH . Config::BACKEND . 'modules/PercentCodesVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/MailAllowsVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/PersonVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/MailAllowsVo.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralDao.php');

class AjaxDataCode{

    private $isActionFromAjax;
    private $optionInputForm;
    private $idDataInputForm;
    public $dataList = [];
    function __construct()
    {
        if (isset($_POST['isActionFromAjax'])) {
            $this->isActionFromAjax = $_POST['isActionFromAjax'];
            $this->optionInputForm = $_POST['optionInputForm'];
            $this->idDataInputForm = $_POST['idDataInputForm'];
            if(isset($_POST["listData"])){
                $this->dataList = $_POST["listData"];  
            }
            if ($this->isActionFromAjax == "true") {
                if ($this->optionInputForm == 3) {
                    $this->SetDataInControls();
                }else if($this->optionInputForm == 5){
                $this->ActionsTable($_POST["module"] , $_POST["mail"] , $_POST["idUser"]);
            }else if($this->optionInputForm == 6){
                $this->GetDataInJsonForTblOption(1);
            }else if($this->optionInputForm == 7){
                $this->GetDataInJsonForTblOption(2);
            }else if($this->optionInputForm == 8){
                $this->UpdateFieldMails();
                }else {
                    $this->GetDataInJsonForTbl();
                }
            }
        }
    }

    private function SetDataInControls(){
        $SetInputsDataFJQ = "";
        $PercentCodesVo = new PercentCodesVo();
        $PercentCodesVo->id = $this->idDataInputForm;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $PercentCodesVo);
        if($PercentCodesVo2 = $generalDao->GetVo($generalVo = clone $PercentCodesVo)){
            $SetInputsDataFJQ = "
                            <script>
                               (function($){
                                SetValueToInputText('codeCdx','{$PercentCodesVo2->code}');
                               })(jQuery);
                            </script>
                        ";
        }
        echo $SetInputsDataFJQ;
    }

    public function GetDataInJsonForTblOption($module){
        $existData = false;
        $PersonVo = new PersonVo();
        $PersonVo->isList = true;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $PersonVo);

        $string = "
        {\"data\": [";
        $cont = 1;

        while ($PersonVo2 = $generalDao->GetVo($generalVo = clone $PersonVo)) {
             if($PersonVo2->idRole == 3 ){   
                $existData = true;
                $json = array();

                $json[] = $PersonVo2->id;
                $json[] = htmlspecialchars($PersonVo2->names ." ". $PersonVo2->lastNames );
                $json[] = $PersonVo2->idRole;
              // print_r($this->ValidateExistAllowMail( $PersonVo2->id , $module)->module ."-");
                if ( $this->ValidateExistAllowMail( $PersonVo2->id , $module)->module == $module){
                    $json[] = "<input class=\"form-control\" data-id=".$this->ValidateExistAllowMail( $PersonVo2->id , $module)->id." id=\"emailInput\" value=".$this->ValidateExistAllowMail( $PersonVo2->id , $module)->mail." >";
                    $json[] = '<input id="toggle-demo" checked type="checkbox" data-toggle="toggle"  data-email="'.$PersonVo2->email.'"  data-idSend="'.$this->ValidateExistAllowMail( $PersonVo2->id , $module)->id.'" data-on="Recibiendo" data-off="No recibir" data-onstyle="success" data-email="'.$PersonVo2->email.'" data-id="'.$PersonVo2->id.'" data-offstyle="danger" data-module="'.$module.'" >';
                }else{
                    $json[] = "<input class=\"form-control\"  disabled value=\"$PersonVo2->email\" >";
                    $json[] = '<input id="toggle-demo" type="checkbox" data-toggle="toggle" data-on="Recibiendo"  data-idSend="0" data-email="'.$PersonVo2->email.'" data-id="'.$PersonVo2->id.'" data-off="No recibir" data-onstyle="success" data-offstyle="danger" >';
                }
               

                $string .= json_encode($json) . ",";
             }
        }
        $string .= "-]}-";

        if($existData){
            echo  str_replace(",-]}-", "]}", $string);
        }else{
            echo "{ \"data\": [] }";
        }
    }

    public function ValidateExistAllowMail($id , $module){

        $existData = false;
        $MailAllowsVo = new MailAllowsVo();
        $MailAllowsVo3 = new MailAllowsVo();
        $MailAllowsVo->idUser = $id;
        $MailAllowsVo->idSearchField = 1;
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $MailAllowsVo);
        while ($MailAllowsVo2 = $generalDao->GetVo($generalVo = clone $MailAllowsVo)) {
           
            if ($module == $MailAllowsVo2->module){
              //  print("*".$MailAllowsVo2->module."<br>");
                return $MailAllowsVo2; 
            }
        }

        $MailAllowsVo3->module = 0;
        return $MailAllowsVo3;
        
    }

    public function GetDataInJsonForTbl()
    {
        $existData = false;
        $PercentCodesVo = new PercentCodesVo();
        $PercentCodesVo->isList = true;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $PercentCodesVo);

        $string = "
        {\"data\": [";
        $cont = 1;
        while ($PercentCodesVo2 = $generalDao->GetVo($generalVo = clone $PercentCodesVo)) {
            $existData = true;
            $json = array();

            $json[] = $PercentCodesVo2->id;
            $json[] = htmlspecialchars($PercentCodesVo2->code);
            $json[] = $this->GetPersonQuotation($PercentCodesVo2->idPerson);
            $json[] = $PercentCodesVo2->dateCreate;
            $button = "";
            if(!in_array( "modificar_codigo" , $this->dataList)){
                $button .= "<tip title='Modificar' class='yellow-tooltip' data-toggle='tooltip' data-placement='top'><button id='updateBtn' name='updateBtn' class='btn btn-warning btn-xs' data-toggle='modal' data-target='#productPopUpAdd' title='Modificar' data-dismiss='modal' onclick='UpdateData({$PercentCodesVo2->id},\"managers\",\"managers\");'><i class='fa fa-pencil'></i></button> </tip>" ;
            }
            if(!in_array( "eliminar_codigos" , $this->dataList)){
                $button .= " <tip title='Eliminar' class='red-tooltip' data-toggle='tooltip' data-placement='top'><a id='deleteBtn' name='deleteBtn' class='btn btn-danger  btn-xs' title='Eliminar' onclick='$(\"#productPopUpAdd\").remove();DeleteData({$PercentCodesVo2->id},\"managers\")'><i class='fa fa-times'></i></a></tip> ";
            }
            $json[] = $button;
            $string .= json_encode($json) . ",";
            $cont++;
        }
        $string .= "-]}-";

        if($existData){
            echo  str_replace(",-]}-", "]}", $string);
        }else{
            echo "{ \"data\": [] }";
        }
    }


    private function SetDataToVo($module , $mail , $user)
    {
        $MailAllowsVo = new MailAllowsVo();
        $MailAllowsVo->module = $module;
        $MailAllowsVo->idUser = $user;
        $MailAllowsVo->mail = $mail;
        $MailAllowsVo->isAllow = 1;
        return $MailAllowsVo;
    }


    public function ActionsTable($module , $mail , $user){
        $generalDao = new GeneralDao();
        if ( $_POST["action"] == "SET"){
            $result = $generalDao->Set($generalVo = clone $this->SetDataToVo($module , $mail , $user));
        }else{
            $data = new MailAllowsVo();
            $data->id = $_POST["idDataInputForm"];
            $result = $generalDao->Delete($generalVo = clone $data);
        }
        
    }

    public function UpdateFieldMails(){
        $MailAllowsVo = new MailAllowsVo();
        $MailAllowsVo->id =$_POST["idDataInputForm"];
        $MailAllowsVo->idUpdateField = 2;
        $MailAllowsVo->mail = $_POST["mail"];
        $MailAllowsVo->idSearchField = 0;
        $generalDao = new GeneralDao();
        $generalDao->UpdateByField($generalVo = clone $MailAllowsVo);
    }

       private  function GetPersonQuotation($idPerson)
    {
        $nameUser="";
         $personVo= new PersonVo();
         $personVo->idSearchField = 0;
         $personVo->id = $idPerson;
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $personVo);
        while ($personVo2 = $generalDao->GetVo($generalVo = clone $personVo)) {
            $nameUser = $personVo2->names. ' '.$personVo2->lastNames ;
        }
        return $nameUser;

    }

    
}

$AjaxDataCode = new AjaxDataCode();
