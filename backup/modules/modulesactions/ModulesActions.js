function ChangeState(id, state) {
    var parameters = {
        "isActionFromAjax": true,
        "optionInputForm": 9,
        "idDataInputForm": 0,
        "id": id,
        "state": state
    };

    $.ajax({
        url: "view/html/modules/modulesactions/AjaxDataModulesActions.php",
        type: 'post',
        data: parameters,
        success: function(data) {
            if ($("#validateBTN" + id).text() != "Acción Desactivada") {
                $("#validateBTN" + id).html('<button class="D btn btn-block btn-danger" onclick="ChangeState(' + id + ',' + 0 + ')">Acción Desactivada</button>');
            } else {
                $("#validateBTN" + id).html('<button class="A btn btn-block btn-success" onclick="ChangeState(' + id + ',' + 1 + ')"> Acción Activada</button>');
            }
        }
    })

}

function LoadRoles() {
    var parameters = {
        "isActionFromAjax": true,
        "optionInputForm": 5,
        "idDataInputForm": 0,
    };

    $.ajax({
        url: "view/html/modules/modulesactions/AjaxDataModulesActions.php",
        type: 'post',
        data: parameters,
        success: function(data) {
            EachRoles(JSON.parse(data));
        }
    })
}

function LoadModules(id) {
    var parameters = {
        "isActionFromAjax": true,
        "optionInputForm": 7,
        "idDataInputForm": 0,
        "idRol": id
    };

    $.ajax({
        url: "view/html/modules/modulesactions/AjaxDataModulesActions.php",
        type: 'post',
        data: parameters,
        success: function(data) {
            EachModules(JSON.parse(data));
        }
    })
}






function EachRoles(roles) {
    for (var i = 0; i < roles.length; i++) {
        if (roles[i].name != "Cliente" && roles[i].name != "Prospecto" && roles[i].name != "Proveedor" && roles[i].name != "SubDistribuidor") {
            $("#card-roles").append(' <div class="col-lg-3 col-xs-6"><div class="shadow-lg rounded small-box bg-' + roles[i].color + '"><div class="inner inner-1"><h4><b>' + roles[i].name + '</b></h4><h2></h2>&nbsp</div><div class="icon"><i class="fa fa-' + roles[i].icon + '"></i></div><a href="#" data-toggle="modal" data-target="#ModulePopUpAdd" onclick="LoadModules(' + roles[i].id + ')" class="small-box-footer">Ver Permisos <i class="fa fa-arrow-circle-right"></i></a></div></div>');
        }
    }

}

function EachModules(modules) {
    $("#modulesList").html("");

    for (var i = 0; i < modules.length; i++) {
        $("#modulesList").append("<h1 class='text-center'>" + modules[i].nombre + "</h1><div class='container-fluid'><div class='row' id='datamodulesList-" + i + "'></div></div>");
        if (modules[i].events.length > 0) {
            for (var e = 0; e < modules[i].events.length; e++) {

                $("#datamodulesList-" + i).append('<div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-3"><b>   <span class="text-center">Acción para ' + modules[i].events[e].action + '</span></b><div id="validateBTN' + modules[i].events[e].id + '"></div></div></button>');
                if (modules[i].events[e].state == 1) {
                    $("#validateBTN" + modules[i].events[e].id).append('<button class="A btn btn-block btn-success" onclick="ChangeState(' + modules[i].events[e].id + ',' + modules[i].events[e].state + ')">Acción Activada</button>');
                } else {
                    $("#validateBTN" + modules[i].events[e].id).append('<button class="D btn btn-block btn-danger" onclick="ChangeState(' + modules[i].events[e].id + ',' + modules[i].events[e].state + ')">Acción Desactivada</button>');
                }
            }

        } else {
            $("#datamodulesList-" + i).append('<p class="text-center">Sin Acciones Asignadas </p> ');
        }
    }

}




/*********************************CLICK EVENTS**************************/

$(function() {
    LoadRoles();
});