<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Administrar Permisos por Modulos
        </h1>
        <ol class="breadcrumb">
            <li><a href="Home"><i class="fa fa-dashboard"></i> Inicio</a></li>
            <li class="active">Administrar Modulos</li>
            <li class="active">Administrar Permisos</li>
        </ol>
    </section>
    <section class="content">
        <div class="box">

            <nav class="navbar navbar-light">
                <div class="container-fluid">
                    <ul class="nav navbar-nav">
                        <li>
                            <a style="cursor: pointer;" class="" id="impostAddBtn" name="impostAddBtn" data-toggle="modal" data-target="#ModuleAdd" disabled="true">
                                <i class="fa fa-plus-circle"></i> Agregar Permiso
                            </a>
                        </li>
                        <li>
                            <a style="cursor: pointer;" id="userAddBtn" name="userAddBtn" onclick="location.href = 'Home' ">
                                <i class="fa fa-home"></i> Menu Principal
                            </a>
                        </li>
                        <li>
                            <a style="cursor: pointer;" id="userAddBtn" name="userAddBtn" onclick="location.href = 'Managers' ">
                                <i class="fa fa-arrow-left"></i> Regresar
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>

            <div id="card-roles">

            </div>

        </div>
    </section>
</div>

<!--=====================================
VENTANA MODAL AGREGAR PRODUCTO O MODIFICAR
======================================-->
<div id="ModulePopUpAdd" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg" style="width: 95%">
        <div class="modal-content">

            <div class="modal-header" style="background:#3c8dbc; color:white">
                <button type="button" class="close" data-dismiss="modal" data-target='#productPopUpAdd' onclick="ResetControls('productForm')">&times;</button>
                <h4 class="modal-title">Ver Permisos</h4>
            </div>
            <!--=====================================
              CUERPO DEL MODAL
              ======================================-->
            <div class="modal-body" id="containerinputs">
                <div class="box-body">
                    <div class="row" id="modulesList">

                    </div>
                </div>
            </div>
            <!--=====================================
              PIE DEL MODAL
              ======================================-->
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal" data-target='#productPopUpAdd' onclick="ResetControls('product2Form')">Salir</button>
            </div>

            </form>

        </div>
    </div>
</div>



<!--=====================================
VENTANA MODAL AGREGAR PRODUCTO O MODIFICAR
======================================-->
<div id="ModuleAdd" class="modal fade" role="dialog">
    <div class="modal-dialog modal-xs">
        <div class="modal-content">

            <div class="modal-header" style="background:#3c8dbc; color:white">
                <button type="button" class="close" data-dismiss="modal" data-target='#productPopUpAdd' onclick="ResetControls('productForm')">&times;</button>
                <h4 class="modal-title">Ver Permisos</h4>
            </div>
            <form id="ModulesForm" role="form" method="post" enctype="multipart/form-data">
                <!--=====================================
              Hidden
              ======================================-->
                <input id="optionInputForm" name="optionInputForm" type="hidden" value="1">
                <input id="idDataInputForm" name="idDataInputForm" type="hidden" value="0">
                <input id="isActionFromAjax" name="isActionFromAjax" type="hidden" value="false">
                <div class="modal-body" id="containerinputs">
                    <div class="box-body">
                        <div class="row" id="modulesList">
                            <div class="form-group">
                                <label>Lista de Modulo</label> {$listModules}
                            </div>
                            <!--  <div class="form-group">
                                <label>Nombre Rol</label> {$listRoles}
                            </div> -->
                            <div class="form-group">
                                <label>Accion</label>
                                <input type="text" class="form-control" name="action">
                            </div>
                        </div>
                    </div>
                </div>
                <!--=====================================
              PIE DEL MODAL
              ======================================-->
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal" data-target='#productPopUpAdd' onclick="ResetControls('product2Form')">Salir</button>
                    <button type="submit" class="btn btn-primary pull-right">Guardar</button>
                </div>

            </form>

        </div>
    </div>
</div>

<div id="containerResponseAjax"></div>
<script src="view/html/modules/modulesactions/ModulesActions.js?v= {$tempParameter}"></script>
{$jquery}