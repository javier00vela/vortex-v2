<?php
require_once(Config::PATH . Config::CONTROLLER . 'general/AccessManager.php');
require_once(Config::PATH . Config::CONTROLLER . 'general/UtilJquery.php');
require_once(Config::PATH . Config::VIEW_HTML . 'general/comps/GeneralCtx.php');
require_once(Config::PATH . Config::VIEW_HTML . 'general/comps/GeneralLst.php');
require_once(Config::PATH . Config::VIEW_HTML . 'general/comps/GeneralWithDataLst.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralDao.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/ModuleVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/ModuleActionVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/RoleVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/ModuleRoleVo.php');

class ModulesActionsView
{
    private $optionInputForm;
    private $isUpdateUserForm;
    private $idDataInputForm;
    /*=============================================
                  Session Vars
    =============================================*/
   
    /*=============================================
                  Template Vars
    =============================================*/
    private $utilJQ;
    private $pageTpl, $data, $core;
    /*=============================================
                Controls Vars HTML
    =============================================*/
    
    /*=============================================
                Data Vars
    =============================================*/
    private $generalDao;


    function __construct($pageTpl, $data, $core)
    {
        $this->data = $data;
        $this->core = $core;
        $this->pageTpl = $pageTpl;

            $this->CreateComponents();
            $this->UseDataFromThePostback();
        
    }

    private function UseDataFromThePostback()
    {
        if (isset($_POST['optionInputForm'])) {
            $this->optionInputForm = $_POST['optionInputForm'];
            $this->idDataInputForm = $_POST['idDataInputForm'];
            $this->generalDao = new GeneralDao();
            switch ($this->optionInputForm) {
                case "1": {

                    $this->SetData();

                    break;
                }
                case "2": {
                    $this->DeleteData();
                    break;
                }
                case "4": {
                    $this->SetUpdate();
                    break;
                }
            }
        }

        $this->AssignDataToTpl();

    }

    private function CreateComponents()
    {
        $this->listRoles = new GeneralWithDataLst($this->GetRoles(), "RolesLst", false,true,"--Lista de Roles--",true);
        $this->listModule = new GeneralWithDataLst($this->GetModules(), "listModules", false,true,"--Lista de modulos--",true);
       
        $this->utilJQ = new UtilJquery("Impost");
    }
    private function SetDataToVo()
    {
        $ModuleVo = new ModuleActionVo();
        $ModuleVo->id = null;
       // $ModuleVo->rol = $_POST["listRoles"];
        $ModuleVo->idModule = $_POST["listModules"];
        $ModuleVo->action = $_POST["action"];
        $ModuleVo->state = 1;
       
        return $ModuleVo;
    }
    private function SetData()
    {
        $moduleVo = $this->SetDataToVo();


            $this->GenerateModulesRoles($moduleVo);

    }

    private function GetModules(){
        $companiesArray = array();
        $ModulesVo = new ModuleVo();
        $ModulesVo->isList = true;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $ModulesVo);
        while ($ModulesVo2 = $generalDao->GetVo($generalVo = clone $ModulesVo)) {
     
                $companiesArray[] = $ModulesVo2->id."_".$ModulesVo2->subName;
            
        }
        return $companiesArray;
    }

    private function GetRoles(){
        $companiesArray = array();
        $RolVo = new RoleVo();
        $RolVo->isList = true;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $RolVo);
        while ($RolVo2 = $generalDao->GetVo($generalVo = clone $RolVo)) {
            if ($RolVo2->name != "Cliente" && $RolVo2->name != "Prospecto" && $RolVo2->name != "Proveedor" && $RolVo2->name != "SubDistribuidor") {
                $companiesArray[] = $RolVo2->id."_".$RolVo2->name;
            }
            
        }
        return $companiesArray;
    }

    private function GenerateModulesRoles($moduleActionVo){
        $ModuleVo = new ModuleActionVo();
        for ($i=1; $i < 14 ; $i++) {  
            $ModuleVo->idRol = $i;
            $ModuleVo->idModule = $moduleActionVo->idModule;
            $ModuleVo->action = $moduleActionVo->action;
            $ModuleVo->state = $moduleActionVo->state;
            $result = $this->generalDao->Set($generalVo = clone $ModuleVo);
        }
        $message = 'MessageSwalBasic("Registrado!","Acción Registrada Correctamente","success");';
        $this->utilJQ->AddFunctionJavaScript($message);

    }


    private function SetUpdate()
    {
        $result = $this->generalDao->Update($generalVo = clone $this->SetDataToVo());
        if (isset($result)) {
            $message = 'MessageSwalBasic("Modificado!","Impuesto Modificado Correctamente","success");';
            $this->utilJQ->AddFunctionJavaScript($message);
        }
    }
    private function DeleteData()
    {
        $message = "";
        $impostVo = new ImpostVo();
        $impostVo->id = $this->idDataInputForm;
        if ($this->generalDao->Delete($generalVo = clone $impostVo)) {
            $message = 'MessageSwalBasic("Eliminado!","Impuesto Eliminado Correctamente","success");';
        } else {
            $message = 'MessageSwalBasic("Error!","Problema con la base de datos","error");';
        }
        $this->utilJQ->AddFunctionJavaScript($message);
    }

    private function AssignDataToTpl()
    {
       
        $this->data->assign('listRoles', $this->listRoles->paint());
        $this->data->assign('listModules', $this->listModule->paint());
         $this->data->assign('tempParameter', time());
        $this->data->assign('jquery', $this->utilJQ->PaintJQ());
        $this->SetDataToTpl();
    }   
    private function SetDataToTpl()
    {
        echo $this->core->get($this->pageTpl, $this->data);
    }
  


}   