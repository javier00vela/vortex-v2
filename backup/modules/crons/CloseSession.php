<?php 
require_once ('../../../../controller/general/Config.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralDao.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/AccessHistoryVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/UserVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/PersonVo.php');

function GetLoginUser(){
    date_default_timezone_set("America/Bogota");
    $personVo = unserialize($_SESSION["user"]["person"]);
    $userVo = unserialize($_SESSION["user"]["user"]);
    $accessHistoryVo = unserialize($_SESSION['user']["accessHistory"]);
    session_destroy();
    /*
    $accessHistoryVo->logout = date("Y-m-d H:i:s");
    $generalDao = new GeneralDao();
    $query = "UPDATE {$accessHistoryVo->nameTable } SET {$accessHistoryVo->GetNameField(2)} = '{$accessHistoryVo->logout}',";
    $query .= " {$accessHistoryVo->GetNameField(3)} = CONCAT(EXTRACT(hour FROM TIMEDIFF('{$accessHistoryVo->logout}', '{$accessHistoryVo->login}')),'h ',";
    $query .= " EXTRACT(minute FROM TIMEDIFF('{$accessHistoryVo->logout}', '{$accessHistoryVo->login}')),'min')";
    $query .= " WHERE {$accessHistoryVo->GetNameField(7)} = {$personVo->id} AND {$accessHistoryVo->GetNameField(2)} = '0000-00-00 00:00:00'";
    $generalDao->CustomQuery($query);
    echo '<script>window.location = "Login";</script>';
    */
}
GetLoginUser();
 ?>