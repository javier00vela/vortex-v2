<?php
require_once(Config::PATH . Config::CONTROLLER . 'general/AccessManager.php');
require_once(Config::PATH . Config::CONTROLLER . 'general/UtilJquery.php');
require_once(Config::PATH . Config::VIEW_HTML . 'general/comps/GeneralCtx.php');
require_once(Config::PATH . Config::VIEW_HTML . 'general/comps/GeneralLst.php');
require_once(Config::PATH . Config::VIEW_HTML . 'general/comps/GeneralWithDataLst.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralDao.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/PersonVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/RoleCompanyVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/CompanyVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/UserVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/RoleVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/ProspectBrandVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/ClientUserVo.php');
require_once(Config::PATH . Config::CONTROLLER_LIB . 'vendor/phpoffice/phpexcel/Classes/PHPExcel/IOFactory.php');

class ContactsView
{
    private $optionInputForm;
    private $idDataInputForm;
    private $idCompayInputForm;
    /*=============================================
              Session Vars
    =============================================*/
    private $manageSession;
    /*=============================================
                  Template Vars
    =============================================*/
    private $utilJQ;
    private $pageTpl, $data, $core;
    /*=============================================
                Controls Vars HTML
    =============================================*/

    private $personNamesCtx;
    private $personLastNamesCtx;
    private $personDocumentTypeLst;
    private $personDocumentCtx;
    private $personCellPhoneCtx;
    private $personTelephoneCtx;
    private $personEmailCtx;
    private $personUserLst;
    private $personPrefijoLst;
    private $personAddressCtx;
    private $personCityCtx;
    private $personIdRoleHidden;
    private $personIdCompanyHidden;
    /*=============================================
                Data Vars
    =============================================*/
    private $generalDao;
    /**
     * @var CompanyVo
     */
    private $companyVo;
    private $idCompanyFromGet;
    private $idRolePerson;

    /***********************************************/
    /*********variable for excel ********************/
    private $idCompanyFromPost;


    function __construct($pageTpl, $data, $core)
    {
        $this->data = $data;
        $this->core = $core;
        $this->pageTpl = $pageTpl;
        $this->ValidateIfExistInventory($_GET["idCompayInputForm"]);
            $this->CreateComponents();
            $this->UseDataFromThePostback();
    }

    private function ValidateIfExistInventory($id){
        if(empty($this->GetCompanyById($id)->id)){
            header("location: DontFind");
        }
    }


     private function GetCompanyById($id){
        $CompanyVo = new CompanyVo();
        $generalDao = new GeneralDao();
        $CompanyVo->id = $id;
        $generalDao->GetById($generalVo = clone $CompanyVo);

        if($CompanyVo2 = $generalDao->GetVo($generalVo = clone $CompanyVo)){
                return  $CompanyVo2;  
         }

      return "Error al Realizar Consulta";
    }

    private function UseDataFromThePostback()
    {

        if (isset($_POST['optionInputForm'])) {
            $this->optionInputForm = $_POST['optionInputForm'];
            $this->idDataInputForm = $_POST['idDataInputForm'];
            $this->generalDao = new GeneralDao();
            switch ($this->optionInputForm) {
                case "1": {
                    $this->SetData();
                    break;
                }
                case "2": {
                    $this->DeleteData();
                    break;
                }
                case "4": {
                    $this->SetUpdate();
                    break;
                }
            }
            //echo '<script>window.location = "Users";</script>';
        }

        if (isset($_POST['optionInputTemplateForm'])) {
            $this->optionInputForm = $_POST['optionInputTemplateForm'];
            switch ($this->optionInputForm) {
                case "1": {
                    $this->GetFileToRead();
                    $this->idCompanyFromPost = $_POST["idCompayInputForm"];
                    break;
                }
            }
        }

        $this->UseDataFromGet();

        $this->AssignDataToTpl();
    }

    private function  UseDataFromGet(){
        $assign = '';
        if(isset($_GET["idCompayInputForm"])){
            $this->idCompanyFromGet = $_GET["idCompayInputForm"];
            $this->GetDataCompanyById();
            $this->ValidateRolePerson();
            $assign = 'SetValueToInputText("idCompayInputForm",'.$this->idCompanyFromGet.');';
            $assign .= 'SetValueToInputText("personIdRoleHidden",'.$this->idRolePerson.');';
            $assign .= 'SetValueToInputText("personIdCompanyHidden",'.$this->companyVo->id.');';

        }
        $this->utilJQ->AddFunctionJavaScript($assign);
    }

    private function ValidateRolePerson(){
        if($this->GetRoleCompany($this->companyVo->idRoleCompany) == "Cliente"){
            $this->idRolePerson = 7;
        }else if($this->GetRoleCompany($this->companyVo->idRoleCompany) == "Proveedor"){
            $this->idRolePerson = 8;
        }
        else if($this->GetRoleCompany($this->companyVo->idRoleCompany) == "Prospecto"){
            $this->idRolePerson = 9;
        }
    }

    private function GetRoleCompany($idRoleCompany){
        $roleCompanyVo = new RoleCompanyVo();
        $roleCompanyVo->id = $idRoleCompany;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $roleCompanyVo);
        if($roleCompanyVo2 = $generalDao->GetVo($generalVo = clone $roleCompanyVo)){
            return  $roleCompanyVo2->name;
        }
    }

    private function CreateComponents()
    {
        $this->idCompayInputForm = new GeneralCtx("idCompayInputForm","","hidden",false);
        $this->personNamesCtx = new GeneralCtx("personNamesCtx","Nombres",null,true);
        $this->personLastNamesCtx = new GeneralCtx("personLastNamesCtx","Apellidos",null,true);

        $dataPersonDocumentTypeLst = array("CC", "CE", "TI", "Otro");
        $this->personDocumentTypeLst = new GeneralWithDataLst($dataPersonDocumentTypeLst, "personDocumentTypeLst", false,false,"--Tipo Documento--",true);
        $dataPersonPrefijoTypeLst = array("Señor", "Señora", "Ingeniero", "Ingeniera" , "Doctor","Doctora");
        $this->personPrefijoLst = new GeneralWithDataLst($dataPersonPrefijoTypeLst, "personPrefijoLst", false,false,"--Tipo Prefijo--",true);
        
        $this->personUserLst = new GeneralWithDataLst($this->AssignDataSeller(), "personUserLst", false,true,"--Vendedor Asociado--",true);

        $this->personDocumentCtx = new GeneralCtx("personDocumentCtx","Numero Documento",null,false);
        $this->personCellPhoneCtx = new GeneralCtx("personCellPhoneCtx","Numero Celular",null,true);
        $this->personTelephoneCtx = new GeneralCtx("personTelephoneCtx","Numero Telefono",null,true);
        $this->personEmailCtx = new GeneralCtx("personEmailCtx","Correo Electronico",null,true);
        $this->personAddressCtx = new GeneralCtx("personAddressCtx","Dirección",null,true);
        $this->personCityCtx = new GeneralCtx("personCityCtx","Ciudad",null,true);

 $roleVo = new RoleVo();
        $roleVo->isList = true;
        $this->personIdRoleHidden = new GeneralLst($generalVo = clone $roleVo,"personIdRoleHidden", 0, 1,"--Role--", true);

        $this->personIdCompanyHidden = new GeneralCtx("personIdCompanyHidden","","hidden",false);
        $this->utilJQ = new UtilJquery("Users");
    }

    private function AssignDataSeller(){

        $userArray = array();
        $PersonVo = new PersonVo();
        $PersonVo->isList = true;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $PersonVo);
        while ($PersonVo2 = $generalDao->GetVo($generalVo = clone $PersonVo)) {
            if($PersonVo2->idRole == 1 || $PersonVo2->idRole == 2 || $PersonVo2->idRole == 3 || $PersonVo2->idRole == 6 || $PersonVo2->idRole == 10 || $PersonVo2->idRole == 11 ){
                if($PersonVo2->state == 1){
                    $userArray[]  = $PersonVo2->id."_".$PersonVo2->names." ".$PersonVo2->lastNames;  
                }
            }
        }
        return $userArray;

    }

    private function SetDataToVo()
    {
        $personVo = new PersonVo();
        $personVo->id = $this->idDataInputForm;
        $personVo->names = $_POST["personNamesCtx"];
        $personVo->lastNames = $_POST["personLastNamesCtx"];
        $personVo->documentType = " ";
        $personVo->document = " ";
        $personVo->cellPhone = $_POST["personCellPhoneCtx"];
        $personVo->telephone = $_POST["personTelephoneCtx"];
        $personVo->email = $_POST["personEmailCtx"];
        $personVo->address = $_POST["personAddressCtx"];
        $personVo->city = $_POST["personCityCtx"];
        $personVo->creationDate = date('Y-m-d H:i:s');
        $personVo->state = 1;
        $personVo->idRole = $_POST["personIdRoleHidden"];
        $personVo->idCompany = $_POST["personIdCompanyHidden"];

        return $personVo;
    }

    private function SetData()
    {
        $personVo = $this->SetDataToVo();
        $personVo->id = null;
        $result = $this->generalDao->Set($generalVo = clone $personVo);
        $this->SetDataClientUser($result);
        if (isset($result)) {
            if($personVo->idRole == "9"){
                $this->GenerateDateBrand($result);
            }
            $message = 'MessageSwalBasic("Registrado!","Usuario Registrado Correctamente","success");';
            $this->utilJQ->AddFunctionJavaScript($message);
        }

    }

    public function GenerateDateBrand($idPerson ){
        $ProspectBrandVo = new ProspectBrandVo();
        $ProspectBrandVo->idPerson = $idPerson;
        $ProspectBrandVo->nameBrand = "";
        $ProspectBrandVo->description = "";
        $result = $this->generalDao->Set($generalVo = clone $ProspectBrandVo);
    }

       private function SetDataToVoVClientUser($result)
    {
        $ClientUserVo = new ClientUserVo();
        $ClientUserVo->id = ($this->GetIdClientUser($result) != null) ? $this->GetIdClientUser($result) : null ;
        $ClientUserVo->idUser = $_POST["personUserLst"];
        $ClientUserVo->idClient = $result;
        $ClientUserVo->prefijo = $_POST["personPrefijoLst"];
        return $ClientUserVo;
    }

      private function SetDataClientUser($result)
    {
        $personVo = $this->SetDataToVoVClientUser($result);
        $personVo->id = null;
        $result = $this->generalDao->Set($generalVo = clone $personVo);

    }
    private function GetIdClientUser($idClient){

        $ClientUserVo = new ClientUserVo();
        $ClientUserVo->idSearchField = 2;
        $ClientUserVo->idClient = $idClient;
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $ClientUserVo);
        if ($ClientUserVo2 = $generalDao->GetVo($generalVo = clone $ClientUserVo)) {
            return $ClientUserVo2->id;
        }
        return false;
    }

    private function SetUser($idPerson){
        $userVo = new UserVo();
        $nickname = "UserDefaultVortex".rand ( 2000 , 5000);
        $password = $nickname;
        $userVo->nickname = $nickname;
        $userVo->password = $password;
        $userVo->state = 0;
        $userVo->photo = "view/img/users/default/anonymous.png";
        $userVo->idPerson = $idPerson;
        $generalDao = new GeneralDao();
        $result = $generalDao->Set($generalVo = clone $userVo);
        if (!isset($result)) {
            $message = 'MessageSwalBasic("Problema!","Problema al Registrar el Usuario por Defecto","error");';
            $this->utilJQ->AddFunctionJavaScript($message);
        }

    }

    private function SetUpdate()
    {
        $result = $this->generalDao->Update($generalVo = clone $this->SetDataToVo());
        if (isset($result)) {
            $this->SetUpdateClientUser($this->idDataInputForm);
            $message = 'MessageSwalBasic("Modificado!","Usuario Modificado Correctamente","success");';
            $this->utilJQ->AddFunctionJavaScript($message);
        }
    }

       private function SetUpdateClientUser($result)
    {
        $result = $this->generalDao->Update($generalVo = clone $this->SetDataToVoVClientUser($result));
    }

    private function DeleteData()
    {
        $message = "";
        $personVo = new PersonVo();
        $personVo->id = $this->idDataInputForm;
        $personVo->idUpdateField = 13;
           $personVo->isList = false;
        $personVo->state = 0;
        $personVo->idSearchField = 0;
        if ($this->generalDao->UpdateByField($generalVo = clone $personVo)) {
        }
            $this->DeleteUser($personVo->id);
            $message = 'MessageSwalBasic("Eliminado!","Registro Eliminado Correctamente","success");';
        
        $this->utilJQ->AddFunctionJavaScript($message);
    }

    private function DeleteUser($idPerson){
        $userVo = new UserVo();
        $userVo->idSearchField = 5;
        $userVo->idPerson = $idPerson;
        $generalDao = new GeneralDao();
        $generalDao->DeleteByField($generalVo = clone $userVo);
    }

    private function GetDataCompanyById(){
        $companyVo = new CompanyVo();
        $companyVo->id = $this->idCompanyFromGet;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $companyVo);
        if($companyVo2 = $generalDao->GetVo($generalVo = clone $companyVo)){
            $this->companyVo = $companyVo2;
        }
    }

    private function GetRolePerson($idRole){
        $roleVo = new RoleVo();
        $roleVo->id = $idRole;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $roleVo);
        if($role2 = $generalDao->GetVo($generalVo = clone $roleVo)){
            return  $role2->name;
        }
    }
/*
    private function GetRolePerson($idRole){
        $roleVo = new RoleVo();
        $roleVo->id = $idRole;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $roleVo);
        if($role2 = $generalDao->GetVo($generalVo = clone $roleVo)){
            return  $role2->name;
        }
    }*/

      /*******************************LOAD TEMPLATE EXCEL**********************************/

    public function GetFileToRead(){
        if(isset($_FILES["fileExcelTemplateInp"])){
            $fileExplode = explode(".", $_FILES["fileExcelTemplateInp"]["name"]);
            if($fileExplode[1] == "xlsx" ){
                $fileExcel = $_FILES["fileExcelTemplateInp"]["tmp_name"];
                $this->LoadExcelFile($fileExcel);
            }else{
                $message = '<script> MessageSwalBasic("ERROR!","Verifique que el formato que ha ingresado sea de tipo excel","error"); </script>';
                echo $message;
            }
        }
    }
    public function LoadExcelFile($fileExcel){
        if(isset($fileExcel)){
            $coreExcel = $this->LoadDataExcelFile($fileExcel);
            $numRows = $this->CountCellsExcel($coreExcel);
            $this->SetDataExcelToVo($coreExcel,$numRows);
        }
    }

    public function LoadDataExcelFile($file){
        $fileXlsx = PHPEXCEL_IOFactory::load($file);
        return $fileXlsx;
    }

    public function CountCellsExcel($file){
        $numRows = $file->setActiveSheetIndex(0)->getHighestRow();
        return $numRows;
    }


    private function SetDataExcelToVo($fileXlsx,$numRows)
    {
        $personVo = new PersonVo();
        for ($i=2; $i<$numRows+1; $i++) {
            $personVo->names = $fileXlsx->getActiveSheet()->getCell('A'.$i)->getCalculatedValue();
            if($personVo->names != ""){
                $personVo->lastNames = $fileXlsx->getActiveSheet()->getCell('B'.$i)->getCalculatedValue();
                $personVo->documentType = $fileXlsx->getActiveSheet()->getCell('C'.$i)->getCalculatedValue();
                $personVo->document = $fileXlsx->getActiveSheet()->getCell('D'.$i)->getCalculatedValue();
                $personVo->cellPhone = $fileXlsx->getActiveSheet()->getCell('E'.$i)->getCalculatedValue();
                $personVo->telephone = $fileXlsx->getActiveSheet()->getCell('F'.$i)->getCalculatedValue();
                $personVo->email = $fileXlsx->getActiveSheet()->getCell('G'.$i)->getCalculatedValue();
                $personVo->address = $fileXlsx->getActiveSheet()->getCell('H'.$i)->getCalculatedValue();
                $personVo->city = $fileXlsx->getActiveSheet()->getCell('I'.$i)->getCalculatedValue();
                $personVo->state =  1;
                $personVo->idRole = $fileXlsx->getActiveSheet()->getCell('J'.$i)->getCalculatedValue();
                $personVo->idRole = $this->ValidateRol($personVo->idRole);
                $personVo->idCompany = $_GET["idCompayInputForm"];
                $personVo->creationDate = date('Y-m-d H:i:s');

                $this->InsertDataExcel($numRows,$i,$personVo);
            }
        }

    }

      public function InsertDataExcel($numRows,$i,$personVo){
        $generalDao = new GeneralDao();
        $result = $generalDao->Set($generalVo = clone $personVo);
        if($result){
            $this->ObjectEmptyToUp($result);
            $message = '<script> MessageSwalBasic("Insertado!","Se ha agregado la plantilla Excel Correctamente","success"); </script>';
            echo $message;
        }
    }

    public function ObjectEmptyToUp($result){
       $ClientUserVo = new ClientUserVo();
       $ClientUserVo->idUser = 0;
       $ClientUserVo->idClient = $result;
       $ClientUserVo->prefijo = " ";
       $generalDao = new GeneralDao();
       $result = $generalDao->Set($generalVo = clone $ClientUserVo);
    }

    /**************************************************************************************************************************************/
    public function ValidaEmpty($valor){
        if(!isset($valor) || $valor == null){
            $valor = "";
        }else{
            $valor = $valor;
        }
        return $valor;
    }
    public function ValidateRol($rol){
        $rol  = ucfirst($rol);
        $rolDefinitive = 9;
        $rolVo = new RoleVo();
        $rolVo->isList = true;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $rolVo);
        while ($rolVo2 = $generalDao->GetVo($generalVo = clone $rolVo)) {
            if($rolVo2->name == $rol){
                $rolDefinitive = $rolVo2->id;
            }
        }
        //por defecto va hacer 9 ----> Prospecto
        if($rolDefinitive == "" || $rolDefinitive ==null || empty($rolDefinitive) || $rolDefinitive !=7 || $rolDefinitive !=9){
            $rolDefinitive = 9;
        }
        return $rolDefinitive;
    }
    
    private function AssignDataToTpl()
    {
        $this->data->assign('nameCompany', $this->companyVo->name);
        $this->data->assign('idCompayInputForm', $this->idCompayInputForm->paint());
        $this->data->assign('personNamesCtx', $this->personNamesCtx->paint());
        $this->data->assign('personLastNamesCtx', $this->personLastNamesCtx->paint());
        $this->data->assign('personDocumentTypeLst', $this->personDocumentTypeLst->paint());
        $this->data->assign('personDocumentCtx', $this->personDocumentCtx->paint());
        $this->data->assign('personCellPhoneCtx', $this->personCellPhoneCtx->paint());
        $this->data->assign('personTelephoneCtx', $this->personTelephoneCtx->paint());
        $this->data->assign('personEmailCtx', $this->personEmailCtx->paint());
        $this->data->assign('personAddressCtx', $this->personAddressCtx->paint());
        $this->data->assign('personCityCtx', $this->personCityCtx->paint());
        $this->data->assign('personIdRoleHidden', $this->personIdRoleHidden->paint());
        $this->data->assign('personIdCompanyHidden', $this->personIdCompanyHidden->paint());
         $this->data->assign('personUserLst', $this->personUserLst->paint());
         $this->data->assign('personPrefijoLst', $this->personPrefijoLst->paint());
         $this->data->assign('tempParameter', time());
         $this->data->assign('idRole', $_GET["idRole"]);

        $this->data->assign('jquery', $this->utilJQ->PaintJQ());
        $this->SetDataToTpl();
    }

    private function SetDataToTpl()
    {
        echo $this->core->get($this->pageTpl, $this->data);
    }

}