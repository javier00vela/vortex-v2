var quotes;
var trm;


function LoadTable(table , opt) {
    var parameters = {
        "isActionFromAjax" : true,
        "optionInputForm" : opt,
        "idDataInputForm" : 0
    };


   table =  $('#'+table).DataTable( {
        responsive: true,
           "drawCallback": function( settings ) {
            $('[id=toggle-demo]').bootstrapToggle();
            $('[data-toggle="tooltip"]').tooltip()
        },
        language : {
                "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
            },
        "ajax": {
            url : "view/html/modules/mails/AjaxDataMails.php",
            method : "POST",
            data : parameters
        }
    });

    return table;
}

function ChangeCheckProduct(table , clickAction , module , callbackUnselect , callbackSelect){
    $('#'+table).on('click', clickAction, function () {
        if ($(this).hasClass('btn-success')) {
         
            callbackUnselect( $(this) );
        }else{
           // console.log($(this).attr("data-id"));
            callbackSelect( $(this) )
        }
   })    ;    
}

function ChangeChangeInput(table , clickAction  , callback){
    $('#'+table).on('change', clickAction, function () {
        callback( $(this) );

   })    ;    
}

function ChangeInputWhenChangeInput(table ){
    ChangeChangeInput(table , "#emailInput" ,  function (self) {
        console.log(self.attr("data-id"));
        ActionAjax(null , 8 , self  , null);
    });
}

function CallTRM(){
    ChangeCheckProduct("TRMTbl", '.toggle' , 1 , function(self){
        ActionAjax(1 , 5 , self , "DELETE");
    } , function(self){
        ActionAjax(1 , 5 , self  , "SET");
    });
}

function CallValidateData(){
    ChangeCheckProduct("QuotesMailTbl", '.toggle' , 2 , function(self){
       
        ActionAjax(2 , 5 , self , "DELETE");
    } , function(self){
        
        ActionAjax(2 , 5 , self  , "SET");
    });
}

function ActionAjax(module , opt , table  , action){
    var parameters = {
        "isActionFromAjax" : true,
        "optionInputForm" : opt,
        "idDataInputForm" : (action != null ) ? table.children("input").attr("data-idSend") : table.attr("data-id"),
        "module":module,
        "mail": (action != null )  ? table.children("input").attr("data-email") : table.val(),
        "idUser": (action != null) ? table.children("input").attr("data-id") : table.attr("data-id")  ,
        "action":action
    };

    console.log(parameters);

    $.ajax({
        url: "view/html/modules/code/AjaxDataCode.php",
        type: 'post',
        data: parameters,
        success: function (data) {
            quotes.ajax.reload();
            trm.ajax.reload();
}
    });
}

/*********************************CLICK EVENTS**************************/

$(function() {
    LoadTable("codesTbl",0);
    quotes = LoadTable("QuotesMailTbl",7);
    trm = LoadTable("TRMTbl",6);
    console.log(trm);
    CallTRM();
    CallValidateData();
    ChangeInputWhenChangeInput("QuotesMailTbl");
    ChangeInputWhenChangeInput("TRMTbl");

});
