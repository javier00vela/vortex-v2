
function LoadTable() {
    var parameters = {
        "isActionFromAjax" : true,
        "optionInputForm" : 1,
        "idDataInputForm" : 0,
        "codeQuotation" : $("#codeQuotationInputForm").attr("value")
    };
console.log(parameters);


    $('#quotationTbl').DataTable( {
        responsive: true,
           "drawCallback": function( settings ) {
            $('[data-toggle="tooltip"]').tooltip()
        },
        language : {
                "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
            },
        "ajax": {
            url : "view/html/modules/historymodifyquotation/AjaxDataHistoryModifyQuotation.php",
            method : "post",                          
            data : parameters
        }
    });
       
}
$(function(){
    LoadTable();
})