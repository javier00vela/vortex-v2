<?php
require_once(__DIR__ . '/../../../../controller/general/Config.php');
require_once(Config::PATH . Config::CONTROLLER.Config::GENERAL . 'ManageArrays.php');
require_once(Config::PATH . Config::BACKEND . 'modules/HistoryModificationVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/CompanyVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/UserVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/PersonVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/RoleVo.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralDao.php');

class AjaxDataHistoryModifyQuotation
{

    private $isActionFromAjax;
    private $optionInputForm;
    private $idDataInputForm;
    private $codeQuotation;

    function __construct()
    { 
       
        if (isset($_POST['isActionFromAjax'])) {
            $this->isActionFromAjax = $_POST['isActionFromAjax'];
            $this->optionInputForm = $_POST['optionInputForm'];
            $this->idDataInputForm = $_POST['idDataInputForm'];
            $this->codeQuotation = $_POST['codeQuotation'];

            if ($this->isActionFromAjax == true) {
                if ($this->optionInputForm == 1) {
                    $this->GetDataInJsonForTbl();

                }
            }
        }
    }



    public function GetDataInJsonForTbl()
    {
        $existData = false;
        $historyModificationVo = new HistoryModificationVo();
        //$historyModificationVo->isList = true;
         $historyModificationVo->idSearchField = 3;
        $historyModificationVo->idQuotation = $this->codeQuotation;
       
       
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $historyModificationVo);

        $string = "
        {\"data\": [";
        while ($historyModificationVo2 = $generalDao->GetVo($generalVo = clone $historyModificationVo)) {
            $existData = true;
            $json = array();
            $json[] = $historyModificationVo2->id;
             $json[] = $historyModificationVo2->idQuotation;
            $json[] = htmlspecialchars($this->GetPersonQuotation($historyModificationVo2->idUser));
            $json[] = $historyModificationVo2->modificationDate;
            $json[] = htmlspecialchars($historyModificationVo2->description);
            $string .= json_encode($json) . ",";
        }
        $string .= "-]}-";
        if($existData){
            echo  str_replace(",-]}-", "]}", $string);
        }else{
            echo "{ \"data\": [] }";
        }
    }

    private  function GetPersonQuotation($idPerson)
    {
        $nameUser="";
         $personVo= new PersonVo();
         $personVo->idSearchField = 0;
         $personVo->id = $idPerson;
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $personVo);
        while ($personVo2 = $generalDao->GetVo($generalVo = clone $personVo)) {
            $nameUser = $personVo2->names. ' '.$personVo2->lastNames ;
        }
        return $nameUser;

    }

 
}
$ajax = new AjaxDataHistoryModifyQuotation();
