<div class="content-wrapper">
        <section class="content-header">
          <h1>
            Historial de Cotizaciones
          </h1>
          <ol class="breadcrumb">
            <li><a href="Home"><i class="fa fa-dashboard"></i> Inicio</a></li>
            <li class="active">Historial de Cotizaciones</li>
          </ol>
        </section>
        <section class="content">
          <div class="box">
          <div class="box-header with-border">
          <nav class="navbar navbar-light">
              <div class="container-fluid">
                  <ul class="nav navbar-nav">
                      <li>
                          <a style="cursor: pointer;" id="userAddBtn" name="userAddBtn" onclick="location.href = 'GenerateQuotes' ">
                              <i class="fa fa-arrow-left"></i>
                              Regresar 
                          </a>
                      </li>
                        <li>
                          <a style="cursor: pointer;" id="userAddBtn" name="userAddBtn" onclick="location.href = 'Home' ">
                              <i class="fa fa-home"></i>
                              Menu Principal
                          </a>
                      </li>
                  </ul>
              </div>
          </nav>
      </div>
            <div class="box-body">
              {$codeQuotationInputForm}
              <table id="quotationTbl" name="quotationTbl" class="table table-bordered table-striped dt-responsive tables dataTable no-footer dtr-inline" style="width: 100%;" cellspacing="0">
                  <thead>
                      <tr>
                        <th>Id</th>
                        <th>Codigo Cotización</th>
                        <th>Usuario</th>
                        <th>Fecha de Modificación</th>
                        <th>Motivo</th>
                      </tr>
                  </thead>
              </table>
            </div>
          </div>
        </section>
</div>
    
      <div id="containerResponseAjax"></div>
      <script src="view/html/modules/historymodifyquotation/HistoryModifyQuotation.js?v= {$tempParameter}"></script>
   {$jquery}