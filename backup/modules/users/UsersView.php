<?php
require_once(Config::PATH . Config::CONTROLLER . 'general/AccessManager.php');
require_once(Config::PATH . Config::CONTROLLER . 'general/UtilJquery.php');
require_once(Config::PATH . Config::VIEW_HTML . 'general/comps/GeneralCtx.php');
require_once(Config::PATH . Config::VIEW_HTML . 'general/comps/GeneralLst.php');
require_once(Config::PATH . Config::VIEW_HTML . 'general/comps/GeneralWithDataLst.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralDao.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/PersonVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/CompanyVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/UserVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/RoleVo.php');

class UsersView
{
    private $optionInputForm;
    private $isUpdateUserForm;
    private $idDataInputForm;
    /*=============================================
                  Session Vars
    =============================================*/
    private $manageSession;
    /*=============================================
                  Template Vars
    =============================================*/
    private $utilJQ;
    private $pageTpl, $data, $core;
    /*=============================================
                Controls Vars HTML
    =============================================*/

    private $personNamesCtx;
    private $personLastNamesCtx;
    private $personDocumentTypeLst;
    private $personDocumentCtx;
    private $personCellPhoneCtx;
    private $personTelephoneCtx;
    private $personEmailCtx;
    private $personAddressCtx;
    private $personCityCtx;
    private $personIdRoleLst;
    
    private $userNicknameCtx;
    private $userPasswordCtx;

    /*=============================================
                Data Vars
    =============================================*/
    private $generalDao;


    function __construct($pageTpl, $data, $core)
    {
        $this->data = $data;
        $this->core = $core;
        $this->pageTpl = $pageTpl;
            $this->CreateComponents();
            $this->UseDataFromThePostback();
    }

    private function UseDataFromThePostback()
    {

        if (isset($_POST['optionInputForm'])) {
            $this->optionInputForm = $_POST['optionInputForm'];
            $this->idDataInputForm = $_POST['idDataInputForm'];
            $this->generalDao = new GeneralDao();
            switch ($this->optionInputForm) {
                case "1": {
                    $this->SetData();
                    break;
                }
                case "2": {
                    $this->DeleteData();
                    break;
                }
                case "4": {
                    $this->SetUpdate();
                    break;
                }
            }
            //echo '<script>window.location = "Users";</script>';
        }
        if (isset($_POST['isUpdateUserForm'])) {
            $this->isUpdateUserForm = $_POST['isUpdateUserForm'];
            if($this->isUpdateUserForm == "1"){
                $this->UpdateUser();
            }
        }
        $this->AssignDataToTpl();

    }

    private function CreateComponents()
    {
        $this->personNamesCtx = new GeneralCtx("personNamesCtx","Nombres",null,true);
        $this->personLastNamesCtx = new GeneralCtx("personLastNamesCtx","Apellidos",null,true);

        $dataPersonDocumentTypeLst = array("CC", "CE", "TI", "Otro");
        $this->personDocumentTypeLst = new GeneralWithDataLst($dataPersonDocumentTypeLst, "personDocumentTypeLst", false,false,"--Tipo Documento--",true);

        $this->personDocumentCtx = new GeneralCtx("personDocumentCtx","Numero Documento",null,false);
        $this->personCellPhoneCtx = new GeneralCtx("personCellPhoneCtx","Numero Celular",null,true);
        $this->personTelephoneCtx = new GeneralCtx("personTelephoneCtx","Numero Telefono",null,true);
        $this->personEmailCtx = new GeneralCtx("personEmailCtx","Correo Electronico",null,true);
        $this->personAddressCtx = new GeneralCtx("personAddressCtx","Dirección",null,true);
        $this->personCityCtx = new GeneralCtx("personCityCtx","Ciudad",null,true);

        $roleVo = new RoleVo();
        $roleVo->isList = true;
        $this->personIdRoleLst = new GeneralLst($generalVo = clone $roleVo,"personIdRoleLst", 0, 1,"--Role--", true);

        $companyVo = new CompanyVo();
        $companyVo->isList = true;
        
        $this->userNicknameCtx = new GeneralCtx("userNicknameCtx","Usuario",null,true);
        $this->userPasswordCtx = new GeneralCtx("userPasswordCtx","Clave","password",true);

        $this->utilJQ = new UtilJquery("Users");
    }

    private function SetDataToVo()
    {
        $personVo = new PersonVo();
        $personVo->id = $this->idDataInputForm;
        $personVo->names = $_POST["personNamesCtx"];
        $personVo->lastNames = $_POST["personLastNamesCtx"];
        $personVo->documentType = $_POST["personDocumentTypeLst"];
        $personVo->document = $_POST["personDocumentCtx"];
        $personVo->cellPhone = $_POST["personCellPhoneCtx"];
        $personVo->telephone = $_POST["personTelephoneCtx"];
        $personVo->email = $_POST["personEmailCtx"];
        $personVo->address = $_POST["personAddressCtx"];
        $personVo->city = $_POST["personCityCtx"];
        $personVo->creationDate = date('Y-m-d H:i:s');
        $personVo->state = 1;
        $personVo->idRole = $_POST["personIdRoleLst"];
        $personVo->idCompany = 1;

        return $personVo;
    }

    private function SetData()
    {
        $personVo = $this->SetDataToVo();
        $personVo->id = null;
        $result = $this->generalDao->Set($generalVo = clone $personVo);
        if (isset($result)) {
            $this->SetUser($result);
            $message = 'MessageSwalBasic("Registrado!","Usuario Registrado Correctamente","success");';
            $this->utilJQ->AddFunctionJavaScript($message);
        }

    }

    private function SetUpdate()
    {
        $result = $this->generalDao->Update($generalVo = clone $this->SetDataToVo());
        if (isset($result)) {
            $message = 'MessageSwalBasic("Modificado!","Usuario Modificado Correctamente","success");';
            $this->utilJQ->AddFunctionJavaScript($message);
        }
    }

    private function DeleteData()
    {
        $message = "";
        $personVo = new PersonVo();
        $personVo->id = $this->idDataInputForm;
        $personVo->idUpdateField = 13;
           $personVo->isList = false;
        $personVo->state = 0;
        $personVo->idSearchField = 0;
        $this->DeleteUser($personVo->id);
        if ($this->generalDao->UpdateByField($generalVo = clone $personVo)) {
            
        } 
        
         $message = 'MessageSwalBasic("Eliminado!","Registro Eliminado Correctamente","success");';
        $this->utilJQ->AddFunctionJavaScript($message);
    }

    private function DeleteUser($idPerson){
        $userVo = new UserVo();
        $userVo->idSearchField = 5;
        $userVo->idPerson = $idPerson;
        $generalDao = new GeneralDao();
        $generalDao->DeleteByField($generalVo = clone $userVo);
    }

    private function SetUser($idPerson){
        $userVo = new UserVo();
        $nickname = "UserDefaultVortex".rand ( 2000 , 5000);
        $password = $nickname;
        $userVo->nickname = $nickname;
        $userVo->password = $password;
        $userVo->state = 0;
        $userVo->photo = "view/img/users/default/anonymous.png";
        $userVo->idPerson = $idPerson;
        $generalDao = new GeneralDao();
        $result = $generalDao->Set($generalVo = clone $userVo);
        if (!isset($result)) {
            $message = 'MessageSwalBasic("Problema!","Problema al Registrar el Usuario por Defecto","error");';
            $this->utilJQ->AddFunctionJavaScript($message);
        }

    }

    private function UpdateUser(){
        $userVo = new UserVo();
        $userVo->id = $_POST["userIdInputForm"];
        $userVo->nickname = $_POST["userNicknameCtx"];
        $userVo->password = $_POST["userPasswordCtx"];
        $userVo->state = $_POST["userStateInputForm"];
        $userVo->idPerson = $_POST["userIdPersonInputForm"];
        $userVo->photo = $this->SavePhoto($userVo->id,$userVo->idPerson);
        $generalDao = new GeneralDao();
        $result = $generalDao->Update($generalVo = clone $userVo);
        if (isset($result)) {
            $message = 'MessageSwalRedirect("¡Modificado!","Credenciales de Usuario Modificadas Correctamente","success","Users");';
            $this->utilJQ->AddFunctionJavaScript($message);
        }
    }

    private function SavePhoto($idUser,$idPerson){
        $document = $this->GetPersonVoForPhoto($idPerson);
        $directory = "view/img/users/default/anonymous.png";
        //print_r($_FILES["userPhotoUploadFile"]["tmp_name"]);
        if(isset($_FILES["userPhotoUploadFile"]["tmp_name"]) && !empty($_FILES["userPhotoUploadFile"]["tmp_name"])) {
            $file = $_FILES["userPhotoUploadFile"]["tmp_name"];
            list($width, $height) = getimagesize($file);
            $newWidth = 500;
            $newHeight = 500;
            if($_FILES["userPhotoUploadFile"]["type"] == "image/jpeg"){
                $numRandom = rand(1000,5000);
                $directory = $this->GenerateFolderImg($idUser,$document);
                $directory .= "/".$document.$numRandom.".jpeg";
                $this->ChangePhotoSession($idUser,$directory);
                $origin = imagecreatefromjpeg($file);
                $destination = imagecreatetruecolor($newWidth,$newHeight);
                imagecopyresized($destination,$origin,0,0,0,0,$newWidth,$newHeight,$width,$height);
                imagejpeg($destination,$directory);
            }
        }
        return $directory;
    }

    private function GenerateFolderImg($idUser,$document){
        $directory = "view/img/users/".$idUser.$document;
        $this->DeletePhotoUser($directory);
        if (!file_exists($directory)) {
            mkdir($directory,0755);
        }
        return $directory;
    }

    private function DeletePhotoUser($directory){
        $files = glob($directory.'/*'); // get all file names
        foreach($files as $file){ // iterate files
            if(is_file($file))
                unlink($file); // delete file
        }
    }

    private function GetPersonVoForPhoto($idPerson){
        $personVo = new PersonVo();
        $personVo->id = $idPerson;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $personVo);
        if($personVo2 = $generalDao->GetVo($generalVo = clone $personVo)){
            return $personVo->document;
        }
    }


    private function ChangePhotoSession($idUser,$directory){
        $userVo = unserialize($_SESSION['user']["user"]);
        if($idUser == $userVo->id){
            $userVo->photo = $directory;
            $_SESSION['user']["user"] = serialize($userVo);
        }
    }

    private function AssignDataToTpl()
    {
        $this->data->assign('personNamesCtx', $this->personNamesCtx->paint());
        $this->data->assign('personLastNamesCtx', $this->personLastNamesCtx->paint());
        $this->data->assign('personDocumentTypeLst', $this->personDocumentTypeLst->paint());
        $this->data->assign('personDocumentCtx', $this->personDocumentCtx->paint());
        $this->data->assign('personCellPhoneCtx', $this->personCellPhoneCtx->paint());
        $this->data->assign('personTelephoneCtx', $this->personTelephoneCtx->paint());
        $this->data->assign('personEmailCtx', $this->personEmailCtx->paint());
        $this->data->assign('personAddressCtx', $this->personAddressCtx->paint());
        $this->data->assign('personCityCtx', $this->personCityCtx->paint());
        $this->data->assign('personIdRoleLst', $this->personIdRoleLst->paint());
         $this->data->assign('tempParameter', time());
        $this->data->assign('userNicknameCtx', $this->userNicknameCtx->paint());
        $this->data->assign('userPasswordCtx', $this->userPasswordCtx->paint());

        $this->data->assign('jquery', $this->utilJQ->PaintJQ());
        $this->SetDataToTpl();
    }

    private function SetDataToTpl()
    {
        echo $this->core->get($this->pageTpl, $this->data);
    }

}