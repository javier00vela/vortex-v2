<?php
require_once(__DIR__ . '/../../../../controller/general/Config.php');
require_once(Config::PATH . Config::CONTROLLER.Config::GENERAL . 'ManageArrays.php');
require_once(Config::PATH . Config::BACKEND . 'modules/ImpostVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/InventoryVo.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralDao.php');
require_once(Config::PATH . Config::BACKEND . 'modules/HistoryTRMVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/CompanyVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/MailAllowsVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/PricesCompanyVo.php');
require_once(Config::PATH . Config::CONTROLLER . 'general/PHPMailer.php');

class AjaxDataImpost{

    private $isActionFromAjax;
    private $optionInputForm;
    private $idDataInputForm;
    private $arrayWithListMail = []; 
    public $dataList = [];

    function __construct()
    {
        if (isset($_POST['isActionFromAjax'])) {
            $this->isActionFromAjax = $_POST['isActionFromAjax'];
            $this->optionInputForm = $_POST['optionInputForm'];
            $this->idDataInputForm = $_POST['idDataInputForm'];
            if(isset($_POST["listData"])){
                $this->dataList = $_POST["listData"];  
            }
            if ($this->isActionFromAjax == "true") {
                if ($this->optionInputForm == 3) {
                    $this->SetDataInControls();
                }else if($this->optionInputForm == 5){
                    $this->ApplyCalendar();
                }else if($this->optionInputForm == 7){
                    $this->EachMoneyUpdate();
                }else {
                    $this->GetDataInJsonForTbl();
                }
            }
        }
    }

    private function SetDataInControls(){
        $SetInputsDataFJQ = "";
        $HistoryTRMVo = new HistoryTRMVo();
        $HistoryTRMVo->id = $this->idDataInputForm;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $HistoryTRMVo);
        if($HistoryTRMVo2 = $generalDao->GetVo($generalVo = clone $HistoryTRMVo)){
            $SetInputsDataFJQ = "
                            <script>
                               (function($){
                                SetValueToInputText('pricectx','{$HistoryTRMVo2->money}');
                                SetValueToInputText('typepriceLs','{$HistoryTRMVo2->typeMoney}');
                                SetValueToInputText('TRMDate','{$HistoryTRMVo2->date}');
                                SetValueToInputText('optionInputForm',4);
                                SetValueToInputText('idDataInputForm',$HistoryTRMVo2->id);
                               })(jQuery);
                            </script>
                        ";
        }
        echo $SetInputsDataFJQ;
    }

    public function GetDataInJsonForTbl()
    {
        $existData = false;
        $HistoryTRMVo = new HistoryTRMVo();
        $HistoryTRMVo->isList = true;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $HistoryTRMVo);

        $string = "
        {\"data\": [";
        while ($HistoryTRMVo2 = $generalDao->GetVo($generalVo = clone $HistoryTRMVo)) {
            $existData = true;
            $json = array();
           // $json[] = $impostVo2->id;
            $json[] = htmlspecialchars($HistoryTRMVo2->id);
            $json[] = htmlspecialchars($HistoryTRMVo2->typeMoney);
            $json[] = htmlspecialchars($HistoryTRMVo2->money);
            $json[] = htmlspecialchars($HistoryTRMVo2->generation);
            $json[] = htmlspecialchars($HistoryTRMVo2->date);
            $button = "";
            if(!in_array( "modificar_trm" , $this->dataList)){
                $button .= "<button class='btn btn-warning btn-xs'  data-toggle='modal' data-target='#TRMPopUpAdd' title='Modificar' onclick='UpdateData({$HistoryTRMVo2->id},\"calendartrm\",\"CalendarTRM\")'><i class='fa fa-pencil'></i></button>";
            }

            $json[] = $button;
            

            $string .= json_encode($json) . ",";

        }
        $string .= "-]}-";

        if($existData){
            echo  str_replace(",-]}-", "]}", $string);
        }else{
            echo "{ \"data\": [] }";
        }
    }

    public function ApplyCalendar(){
        $arrayList = array();
        $HistoryTRMVo = new HistoryTRMVo();
        $HistoryTRMVo->isList = true;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $HistoryTRMVo);
        while($HistoryTRMVo2 = $generalDao->GetVo($generalVo = clone $HistoryTRMVo)) {
            $arrayList[] = ["id"=>$HistoryTRMVo2->id , "name"=>"la TRM {$HistoryTRMVo2->typeMoney} Finaliza Para este día con el precio de  {$HistoryTRMVo2->money}" , "startdate"=> $HistoryTRMVo2->date, "enddate"=>$HistoryTRMVo2->date,"starttime"=>"00:00" , "endtime"=>"23:59" , "color" => "#".substr(md5($HistoryTRMVo2->date), 0, 6) , "url"=>""  ];
        }   
        print_r(json_encode($arrayList));
    }


    public function UpdateFields($field){
        $arrayList = array();
        $HistoryTRMVo = new HistoryTRMVo();
        $HistoryTRMVo->isList = true;
        $generalDao = new GeneralDao();
        $generalDao->CustomQuery("SELECT * FROM {$HistoryTRMVo->nameTable} WHERE typeMoney='{$field}'");
        while($HistoryTRMVo2 = $generalDao->GetVo($generalVo = clone $HistoryTRMVo)) {
                        
          return $HistoryTRMVo2;
        }   
    }

    private function GetListWithMails()
    {
        $MailAllowsVo = new MailAllowsVo();
        $MailAllowsVo->module = 1;
        $MailAllowsVo->idSearchField = 4;
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $MailAllowsVo);
        while ($MailAllowsVo2 = $generalDao->GetVo($generalVo = clone $MailAllowsVo)) {
            $this->arrayWithListMail[] = $MailAllowsVo2->mail;
        }
    }

    private function SendMailData($data){
     
        $dataMail =  array('mail' => "sysvortex@arvack.com", 'password' => "sysvortex153",'domain' => "mail.arvack.com", 'port' => 25 , "from" => "sysvortex@arvack.com");
        $PHPMailerUtils = new PHPMailerUtils($dataMail);
        print_r($data);
        for($i = 0 ; $i < count($this->arrayWithListMail) ; $i++){
            $PHPMailerUtils->AddDestinatario($this->arrayWithListMail[$i]);
        }
        $subject = "Atención La TRM ".$data->typeMoney." va a finalizar Hoy";
        $contenido = '<style type="text/css">body, html,  .body {   background: #f3f3f3 !important; }</style><spacer size="16"></spacer>
                           <container>
                             <row>
                               <columns large="4" style="backgroung:black">
                                 <center>
                                   <img src="https://www.tescan.com/getattachment/beaaaee6-fa88-449d-9027-4f22f7e80758/vortex-company.aspx">
                                 </center>
                               </columns>

                               <columns large="8">
                                 <h1 style="text-align: center;">Actualiza pronto la TRM!</h1>
                               </columns>
                             </row>
                             <spacer size="16"></spacer>

                             <row>
                               <columns>
                                 <h3 class="text-center">La cotización '.$data->typeMoney.' con precio actual $'.$data->money.' finaliza el dia de hoy ,  te recomendamos actualizar el precio de la TRM semanalmente! </h3><br><ul>
                                 ';
    
                                 $contenido .= '    
                               </ul></columns>
                             </row>

                             <spacer size="16"></spacer>

                             <spacer size="16"></spacer>

                           </container>';



        $PHPMailerUtils->SendDataMail($subject , $contenido , "");
   }

   

    public function UpdateAllProducts($idCompany , $money){
        $Inventory = new InventoryVo();
        $Inventory3 = new InventoryVo();
        $Inventory->idSearchField = 13;
        $Inventory->nameCompany = $this->GetCompany($idCompany);
        $generalDao = new GeneralDao();
        $generalDao2 = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $Inventory);
         while ($Inventory2 = $generalDao->GetVo($generalVo = clone $Inventory)) {
                $Inventory3->id = $Inventory2->id;
                $Inventory3->idSearchField = 0;
                $Inventory3->typeCurrency = $money->typeMoney;
                $Inventory3->idUpdateField = 8;
                $generalDao2->UpdateByField($generalVo = clone $Inventory3);
         }
    }

    
    public function UpdateCompany($money){
        $pricesVo = new PricesCompanyVo();
        $pricesVo2 = new PricesCompanyVo();
        $pricesVo->isList = true;
        $generalDao = new GeneralDao();
        $generalDao2 = new GeneralDao();
        $generalDao->GetById($generalVo = clone $pricesVo);
        $array = array(1,2);
         while ($priceVo3 = $generalDao->GetVo($generalVo = clone $pricesVo)) {

             if($priceVo3->typeMoney == $money->typeMoney ){
                 foreach($array as $ar) { 
                  $pricesVo2->id = $priceVo3->id;
                    $pricesVo2->idSearchField = 0;
                    $pricesVo2->tpm = $money->money;
                    $pricesVo2->typeMoney = $money->typeMoney;
                    $pricesVo2->idUpdateField = $ar;
                   // $this->UpdateAllProducts($priceVo3->idCompany , $money);
                    $generalDao2->UpdateByField($generalVo = clone $pricesVo2);
                }
            }
         }
    }

    private function GetCompany($idCompany)
    {
        $companyVo = new CompanyVo();
        $companyVo->id = $idCompany;
        $companyVo->idSearchField = 0;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $companyVo);
        if ($companyVo2 = $generalDao->GetVo($generalVo = clone $companyVo)) {
            return  $companyVo2->name;
        }
        return  $idCompany;
    }

    public function EachMoneyUpdate(){
        $money = array("EUR","USD");
        $this->GetListWithMails();
        for($i = 0 ; $i < count($money) ; $i++){
           $myMoney = $this->UpdateFields($money[$i]); 
           if($myMoney->mailSend == 0){
                if(date("o-m-d") == date("o-m-d", strtotime($myMoney->date))  ){
                    $this->SendMailData($myMoney);
                    $this->UpdateSendMail($myMoney->id);
                }

                if(date("o-m-d") != date("o-m-d", strtotime($myMoney->date))  ){
                    $this->UpdateNotSendMail($myMoney->id);
                }
           }else{
                if(date("o-m-d") != date("o-m-d", strtotime($myMoney->date))  ){
                    $this->UpdateNotSendMail($myMoney->id);
                }
           }
           $this->UpdateCompany($myMoney);
        }


    }

    public function UpdateNotSendMail($id){
        $HistoryTRMVo = new HistoryTRMVo();
        $generalDao = new GeneralDao();
        $HistoryTRMVo->id = $id;
        $HistoryTRMVo->idSearchField = 0;
        $HistoryTRMVo->mailSend = 0;
        $HistoryTRMVo->idUpdateField = 5;
        $generalDao->UpdateByField($generalVo = clone $HistoryTRMVo);
    }


    public function UpdateSendMail($id){
        $HistoryTRMVo = new HistoryTRMVo();
        $generalDao = new GeneralDao();
        $HistoryTRMVo->id = $id;
        $HistoryTRMVo->idSearchField = 0;
        $HistoryTRMVo->mailSend = 1;
        $HistoryTRMVo->idUpdateField = 5;
        $generalDao->UpdateByField($generalVo = clone $HistoryTRMVo);
    }

    
}

$ajaxDataImpost = new AjaxDataImpost();
