// jshint unused:true
"use strict";
var Events = { "monthly": [] };

function LoadTable() {
    RemoveActionModule(23);
    GetArrayFromDataModule(23);
    setTimeout(function() {
        var parameters = {
            "isActionFromAjax": true,
            "optionInputForm": 0,
            "idDataInputForm": 0,
            "listData": arrayResponse,
        };


        $('#impostTbl').DataTable({
            responsive: true,
            "drawCallback": function(settings) {
                $('[data-toggle="tooltip"]').tooltip()
            },
            language: {
                "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
            },
            "ajax": {
                url: "view/html/modules/calendartrm/AjaxDataCalendarTRM.php",
                method: "POST",
                data: parameters
            }
        });
    }, 500);
}

/*********************************CLICK EVENTS**************************/

function ChangeFormatPrices() {
    $("#impostPercentCtx").keypress(function(event) {
        console.log(event.charCode);
        if (event.charCode > 58 || event.charCode < 43 || event.charCode == 45 || event.charCode == 46) {
            return false;
        }
    });
}

function ValidatePriceUnit() {
    $("#impostPercentCtx").change(function(event) {
        var cont = 0;
        var numberType = $(this).val();
        for (var i = 0; i < numberType.toString().length; i++) {
            if (numberType.toString()[i] == "," || numberType.toString()[i] == ".") {
                cont++;
            }
        }
        if (cont > 1) {
            numberType = "";
        }
        $(this).val(numberType);
    });
}

function SetCalendar() {
    console.log(Events);

    $('#mycalendar').monthly({
        mode: 'event',
        dataType: 'json',
        events: Events
    });
    $(".monthly-prev").trigger("click");
    setTimeout(function() { $(".monthly-next").trigger("click") }, 500);
}

function GetDataEvents() {
    var parameters = {
        "isActionFromAjax": true,
        "optionInputForm": 5,
        "idDataInputForm": 0
    };

    $.ajax({
        url: "view/html/modules/calendartrm/AjaxDataCalendarTRM.php",
        method: "POST",
        data: parameters,
        success: function(data) {
            var myData = JSON.parse(data);
            for (var i = 0; i < myData.length; i++) {
                Events.monthly.push(myData[i]);
            }
        }
    })
}



function ControlsDatePikers() {
    $('#TRMDate').datepicker({
        autoclose: true,
        format: 'yyyy-mm-dd',
        startDate: '-0d'
    });
}



function CalendarAutoModify() {
    var parameters = {
        "isActionFromAjax": true,
        "optionInputForm": 7,
        "idDataInputForm": 0,
    };

    $.ajax({
        url: "view/html/modules/calendartrm/AjaxDataCalendarTRM.php",
        type: 'post',
        data: parameters,
        success: function(data) {
            ToastShow("success", "TRM ACTUALIZADO !!", 'Se ha actualizado la TRM ', "2000");
        }
    })
}



$(function() {
    GetDataEvents();
    ChangeFormatPrices();
    ValidatePriceUnit();
    LoadTable();
    SetCalendar();
    ControlsDatePikers();
    CalendarAutoModify();
});