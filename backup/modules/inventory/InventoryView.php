<?php
ini_set("precision", "15");
require_once(Config::PATH . Config::CONTROLLER . 'general/UtilJquery.php');
require_once(Config::PATH . Config::VIEW_HTML . 'general/comps/GeneralCtx.php');
require_once(Config::PATH . Config::VIEW_HTML . 'general/comps/GeneralLst.php');
require_once(Config::PATH . Config::VIEW_HTML . 'general/comps/GeneralWithDataLst.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralDao.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/CompanyVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/ProductVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/SerialVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/PricesCompanyVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/AditionalsImpostVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/HistoryTRMVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/InventoryVo.php');
require_once(Config::PATH . Config::CONTROLLER_LIB . 'vendor/phpoffice/phpexcel/Classes/PHPExcel/IOFactory.php');
require_once(Config::PATH . Config::CONTROLLER_LIB .'vendor/dompdf/dompdf/src/Autoloader.php');

use Dompdf\Dompdf;

class InventoryView
{
    private $optionInputForm;
    private $idDataInputForm;
    /*=============================================
                  Session Vars
    =============================================*/
    private $manageSession;
    /*=============================================
                  Template Vars
    =============================================*/
    private $utilJQ;
    private $pageTpl, $data, $core;
    /*=============================================
                Controls Vars HTML
    =============================================*/
    private $productTypeProductLst;
    private $productNameCtx;
    private $productBrandCtx;
    private $productReferenceCtx;
    private $productPresentationCtx;
    private $productAmountCtx;
    private $productMinimumAmountCtx;
    private $productTypeCurrencyLst;
    private $productPriceUnitCtx;
    private $productCompanyLst;
    private $productPlace;
    private $productDescriptionAtx;
    private $productListFilterLst;
    private $productIVALst;
    private $productFLETELst;


    /*=============================================
                Data Vars
    =============================================*/
    private $generalDao;


    function __construct($pageTpl, $data, $core)
    {
        $this->data = $data;
        $this->core = $core;
        $this->pageTpl = $pageTpl;
        $this->generalDao = new GeneralDao();
   
            $this->CreateComponents();
            $this->UseDataFromThePostback();
        
    }

    private function UseDataFromThePostback()
    {
        if (isset($_POST['optionInputForm'])) {
            $this->optionInputForm = $_POST['optionInputForm'];
            $this->idDataInputForm = $_POST['idDataInputForm'];
            
            switch ($this->optionInputForm) {
                case "1": {
                    $this->SetData();
                    break;
                }
                case "2": {
                    $this->DeleteData();
                    break;
                }
                case "4": {
                    $this->SetUpdate();
                    break;
                }
                case "9": {
                   $_SESSION["word"] = $_POST["word"];
                    break;
                }
                case "10": {
                    $this->PrintDescription();
                     break;
                 }
                
            }
        }

        if (isset($_POST['optionInputTemplateForm'])) {
            $this->optionInputForm = $_POST['optionInputTemplateForm'];
            switch ($this->optionInputForm) {
                case "6": {
                    $this->GetFileToRead();
                    break;
                }
            }
        }
        
        $this->AssignDataToTpl();

    }

    private function CreateComponents()
    {
        $dataProductTypeProductLst = array("Consumibles","Servicios","Equipos");
        $this->productTypeProductLst = new GeneralWithDataLst($dataProductTypeProductLst, "productTypeProductLst", false,false,"-- Tipo de Producto --",true);

        $this->productNameCtx = new GeneralCtx("productNameCtx","Nombre",null,true);
        $this->productBrandCtx = new GeneralCtx("productBrandCtx","Marca",null,true);
        $this->productReferenceCtx = new GeneralCtx("productReferenceCtx","Referencia",null,true);
        $this->productPresentationCtx = new GeneralCtx("productUnitMeasurementCtx","Presentación",null,true);
        $this->productAmountCtx = new GeneralCtx("productAmountCtx","Cantidad","number",true);
        $this->productMinimumAmountCtx = new GeneralCtx("productMinimumAmountCtx","Cantidad Minima de Stock","number",true);
        $this->productPriceUnitCtx = new GeneralCtx("productPriceUnitCtx","Precio Unidad",null,true);
        $this->productDescriptionAtx = new GeneralCtx("productDescriptionAtx",null,"hidden",true);
        $dataProductCountryLst = array();
        $this->productCountryLst = new GeneralWithDataLst($dataProductCountryLst, "productCountryLst", false,false,"--País de Origen--",true);
        $dataProductPlaceLst = array("Nacional","Internacional");
        $this->productPlaceLst = new GeneralWithDataLst($dataProductPlaceLst, "productPlaceLst", false,false,"--Lugar--",true);
        $dataIVASelection =  array("1_Aplicar el IVA " , "0_No Aplicar El IVA");
        $this->productIVALst = new GeneralWithDataLst($dataIVASelection, "productIVALst", false,true,"--Aplicar El IVA--",true);
        $dataFLETESelection =  array("1_Aplicar el Flete " , "0_No Aplicar El Flete");  
        $this->productFLETELst = new GeneralCtx("productFLETELst","Precio Flete",null,true);


        $this->productTypeCurrencyLst = new GeneralCtx("productTypeCurrencyLst","Seleccione un proveedor para determinar el tipo de moneda",null,true);
        $this->productCompanyLst = new GeneralWithDataLst($this->GetProviders(), "productCompanyLst", false,true,"--Proveedor--",true);
        $this->productListFilterLst =  new GeneralWithDataLst($this->GetFilterProduct(), "productListFilterLst", false,true,"--Lista de Registros--",true);

        $this->utilJQ = new UtilJquery("Users");
    }

    private function GetFilterProduct(){
        $divCount = 0 ;
        $countProducts =   array();
        $inventoryVo = new InventoryVo();
        $inventoryVo->isList = true;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $inventoryVo);
        while ($inventoryVo2 = $generalDao->GetVo($generalVo = clone $inventoryVo)) {
            $countProducts[] = $inventoryVo2->id;
        }
        $divCount =  round(count($countProducts) / 5000);
        $arraySendData = $this->BuildFilterData($divCount);
        return $arraySendData;
    }

    private function BuildFilterData($cant){
        $data = array();
        $autoIncrement = 0 ;
        $autoIncremenMax = 5000 ;
        for ($i=0; $i < ($cant + 1); $i++) { 
            $data[] = ($autoIncrement)."_"."Mostrar de ".$autoIncrement." Hasta ".$autoIncremenMax." Registros";
            $autoIncrement += 5000;
            $autoIncremenMax += 5000;
        }
    return $data;
    }

    private function SetDataToVo()
    {
        $inventoryVo = new InventoryVo();
        $inventoryVo->id = $this->idDataInputForm;
        $inventoryVo->typeProduct = $_POST["productTypeProductLst"];
        $inventoryVo->name = $_POST["productNameCtx"];
        $inventoryVo->brand = $_POST["productBrandCtx"];
        $inventoryVo->reference = $_POST["productReferenceCtx"];
        $inventoryVo->presentation = $_POST["productUnitMeasurementCtx"];
        $inventoryVo->typeCurrency = $_POST["productTypeCurrencyLst"];
        $inventoryVo->amount = $_POST["productAmountCtx"];
        $inventoryVo->minimumAmount = $_POST["productMinimumAmountCtx"];
        $inventoryVo->FLETE = '0';
        if(isset( $_POST["productFLETELst"])){
            $inventoryVo->FLETE = $_POST["productFLETELst"];
        }else{
            $inventoryVo->FLETE = '0'; 
        }
        if(empty($_POST["productFLETELst"])){
            $inventoryVo->priceUnit = str_replace(' ',"",$_POST["productPriceUnitCtx"]);
        }else{
        $inventoryVo->priceUnit = $_POST["productPriceUnitCtx"] +  $_POST["productFLETELst"];
        }
        $inventoryVo->country = $_POST["productCountryLst"];
        $inventoryVo->nameCompany = $_POST["productCompanyLst"];
        $inventoryVo->description = str_replace("'", '"', $_POST["productDescriptionAtx"]);
        $inventoryVo->namePlace = $_POST["productPlaceLst"];
        $inventoryVo->IVA = $_POST["productIVALst"];
        
        return $inventoryVo;
    }

    private function SetData()
    {
        $inventoryVo = $this->SetDataToVo();
        $inventoryVo->id = null;
        $result = $this->generalDao->Set($generalVo = clone $inventoryVo);
        if (isset($result)) {
            $message = 'MessageSwalBasic("Registrado!","Producto Registrado Correctamente","success");';
            $this->utilJQ->AddFunctionJavaScript($message);
        }
    }

    private function SetUpdate()
    {
        $result = $this->generalDao->Update($generalVo = clone $this->SetDataToVo());
        if (isset($result)) {
            $message = 'MessageSwalBasic("Modificado!","Producto Modificado Correctamente","success");';
            $this->utilJQ->AddFunctionJavaScript($message);
        }
    }

    private function DeleteData()
    {
        $message = "";
        $inventoryVo = new InventoryVo();
        $inventoryVo->id = $this->idDataInputForm;
        $this->DeleteSeriales($inventoryVo->id);
        if ($this->generalDao->Delete($generalVo = clone $inventoryVo)) {
            $message = 'MessageSwalBasic("Eliminado!","Registro Eliminado Correctamente","success");';
        } else {
            $message = 'MessageSwalBasic("Error!","Problema con la base de datos","error");';
        }
        $this->utilJQ->AddFunctionJavaScript($message);
    }


    private function DeleteSeriales($id){
         $SerialVo = new SerialVo();
            $SerialVo->idSearchField = 3; 
        $SerialVo->idInventary = $id;
        $this->generalDao->DeleteByField($generalVo = clone $SerialVo);

    }

    public function ClearWordToSendMysql($word){
        $word = str_replace("'","\'" , $word);
        return $word;
    }

    public function SetDataContextHTML($description){
        
        $producs = ' '; 
        $producs .= $this->HeadStylesPDF();
        $producs .= $this->ColumnDataCompany();
        $producs .= $this->DataClientCompany();
        $producs .= '<body><div style="width:80%;margin-left:2px;margin:20px;">';
        $producs .= "<b>Item # XX</b>";
        $producs .= '<br><br>';
        //error_reporting(0);
        $producs .= "<p>".$this->ClearWordToSendMysql(preg_replace("[\n|\r|\n\r]", '',$description))."</p>";
        $producs .= '</br><div></body></html>';
        
        return $producs;
    }

    public function  PrintDescription (){
            $dompdf = new DOMPDF();  
            $content = $this->SetDataContextHTML($_POST["description"]);
            $dompdf->loadHtml($content);
            $dompdf->setPaper('A3', 'letter');
            $dompdf->set_option('isRemoteEnabled', TRUE);
            $dompdf->render();
            $pdf = $dompdf->output();  
            $number = random_int(1,300000);
            print_r("--split--". $number ."--split--");
           file_put_contents("view/docs/pdf/TEST-".$number.".pdf", $pdf);
    }

    private function AssignDataToTpl()
    {
        $this->data->assign('productTypeProductLst', $this->productTypeProductLst->paint());
        $this->data->assign('productNameCtx', $this->productNameCtx->paint());
        $this->data->assign('productBrandCtx', $this->productBrandCtx->paint());
        $this->data->assign('productReferenceCtx', $this->productReferenceCtx->paint());
        $this->data->assign('productPresentationCtx', $this->productPresentationCtx->paint());
        $this->data->assign('productAmountCtx', $this->productAmountCtx->paint());
        $this->data->assign('productMinimumAmountCtx', $this->productMinimumAmountCtx->paint());
        $this->data->assign('productAmountCtx', $this->productAmountCtx->paint());
        $this->data->assign('productTypeCurrencyLst', $this->productTypeCurrencyLst->paint());
        $this->data->assign('productPriceUnitCtx', $this->productPriceUnitCtx->paint());
        $this->data->assign('productCountryLst', $this->productCountryLst->paint());
        $this->data->assign('productCompanyLst', $this->productCompanyLst->paint());
        $this->data->assign('productPlaceLst', $this->productPlaceLst->paint());
        $this->data->assign('productDescriptionAtx', $this->productDescriptionAtx->paint());
        $this->data->assign('productListFilterLst', $this->productListFilterLst->paint());
        $this->data->assign('productIVALst', $this->productIVALst->paint());
        $this->data->assign('productFLETELst', $this->productFLETELst->paint());
        $this->data->assign('cookieResult', (isset($_SESSION["word"])) ? $_SESSION["word"] : ""  );


        
        $this->data->assign('tempParameter', time());
        $this->data->assign('jquery', $this->utilJQ->PaintJQ());
        $this->SetDataToTpl();
    }

    private function SetDataToTpl()
    {
        echo $this->core->get($this->pageTpl, $this->data);
    }

    /********************************************************/
    private function GetProviders(){
        $companiesArray = array();
        $companyVo = new CompanyVo();
        $companyVo->idSearchField = 10;
        $companyVo->idRoleCompany = 3;
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $companyVo);
        while ($companyVo2 = $generalDao->GetVo($generalVo = clone $companyVo)) {
            if($companyVo2->state == 1){
                $companiesArray[] = $companyVo2->name."_".$companyVo2->name;
            }
        }
        return $companiesArray;
    }

    function LoadJsonMoneys($value){
        $ruta = "http://api.currencies.zone/v1/quotes/".$value."/COP/json?key=136|QdBH^Od*z2Q~sSLOfnKPYZYEGGECk1_1";
        $json = file_get_contents($ruta);
       return json_decode($json);
    }
 

    /*******************************LOAD TEMPLATE EXCEL**********************************/

    public function GetFileToRead(){
        if(isset($_FILES["fileExcelTemplateInp"])){
            $fileExplode = explode(".", $_FILES["fileExcelTemplateInp"]["name"]);
            if($fileExplode[1] == "xlsx" ){
                $fileExcel = $_FILES["fileExcelTemplateInp"]["tmp_name"];
                $this->LoadExcelFile($fileExcel);
            }else{
                $message = '<script> MessageSwalBasic("ERROR!","Verifique que el formato que ha ingresado sea de tipo excel","error"); </script>';
                echo $message;
            }
        }
    }
    public function LoadExcelFile($fileExcel){
        if(isset($fileExcel)){
            $coreExcel = $this->LoadDataExcelFile($fileExcel);
            $numRows = $this->CountCellsExcel($coreExcel);
            $this->SetDataExcelToVo($coreExcel,$numRows);

        }
    }

    public function LoadDataExcelFile($file){
        $fileXlsx = null;
        try{
            $fileXlsx = PHPEXCEL_IOFactory::load($file);
	
        }catch(Exception $e){
        throw new Exception();
        }
        return $fileXlsx;
    }

    public function CountCellsExcel($file){
        $file->setActiveSheetIndex(0);
        $numRows = $file->setActiveSheetIndex(0)->getHighestRow();
        return $numRows;
    }


    private function SetDataExcelToVo($fileXlsx,$numRows)
    {
        $inventoryVo = new InventoryVo();
        for ($i=2; $i<$numRows+1; $i++) {
            $inventoryVo->typeProduct = $fileXlsx->getActiveSheet()->getCell('A'.$i)->getCalculatedValue();
            $inventoryVo->typeProduct = $this->ValidateTypeProduct( $inventoryVo->typeProduct);
            $inventoryVo->name = htmlspecialchars($fileXlsx->getActiveSheet()->getCell('B'.$i)->getCalculatedValue());
            $inventoryVo->name = $this->ValidateQuotesInString($inventoryVo->name);
            $inventoryVo->brand = $fileXlsx->getActiveSheet()->getCell('C'.$i)->getCalculatedValue();
            $inventoryVo->reference = $fileXlsx->getActiveSheet()->getCell('D'.$i)->getCalculatedValue();
            $inventoryVo->amount = $fileXlsx->getActiveSheet()->getCell('E'.$i)->getCalculatedValue();
            $inventoryVo->amount = $this->ValidateIsInt($inventoryVo->amount);
            $inventoryVo->minimumAmount = $fileXlsx->getActiveSheet()->getCell('F'.$i)->getCalculatedValue();
            $inventoryVo->minimumAmount = $this->ValidateIsInt($inventoryVo->minimumAmount);
            $inventoryVo->typeCurrency = $fileXlsx->getActiveSheet()->getCell('G'.$i)->getCalculatedValue();
            $inventoryVo->typeCurrency = $this->ValidateTypeCurrency($inventoryVo->typeCurrency);
            $inventoryVo->FLETE = $fileXlsx->getActiveSheet()->getCell('O'.$i)->getCalculatedValue();	
            $inventoryVo->priceUnit = $fileXlsx->getActiveSheet()->getCell('H'.$i)->getCalculatedValue();	
            $inventoryVo->priceUnit = $this->ValidatePrices( $inventoryVo->priceUnit);
           
           
                if(empty($inventoryVo->FLETE)){
                    $inventoryVo->FLETE = 0;
                    $inventoryVo->priceUnit = $inventoryVo->priceUnit;
                }else{
                    $inventoryVo->FLETE = $this->ValidatePrices( $inventoryVo->FLETE);
                    $inventoryVo->priceUnit = $inventoryVo->priceUnit + $inventoryVo->FLETE;
                }
            if (strlen(strstr($inventoryVo->priceUnit,'.'))>0) {
                $arrayPrice = explode("." , $inventoryVo->priceUnit);
                $inventoryVo->priceUnit = $arrayPrice[0] .".". substr($arrayPrice[1] , 0 , 2);
            }
            $inventoryVo->presentation = $fileXlsx->getActiveSheet()->getCell('I'.$i)->getCalculatedValue();
            $inventoryVo->country = ucwords(strtolower($fileXlsx->getActiveSheet()->getCell('J'.$i)->getCalculatedValue()));
            $inventoryVo->nameCompany = $fileXlsx->getActiveSheet()->getCell('K'.$i)->getCalculatedValue();
            $inventoryVo->description = htmlspecialchars($fileXlsx->getActiveSheet()->getCell('L'.$i)->getCalculatedValue());
            if($inventoryVo->typeCurrency == "COP"){
                $inventoryVo->namePlace = "Nacional";
            }else{
                $inventoryVo->namePlace = "Internacional";
            }
            $inventoryVo->IVA = $this->ValidateIVASelection($fileXlsx->getActiveSheet()->getCell('N'.$i)->getCalculatedValue());
            if( $inventoryVo->typeCurrency == "USD" || $inventoryVo->typeCurrency == "EUR" ){
                str_replace(".",",",$inventoryVo->priceUnit);
            }
            $this->ValidateCompany($inventoryVo->nameCompany , $inventoryVo->typeCurrency);
            $this->InsertDataExcel($numRows,$i,$inventoryVo);
        }
    }
    private function ValidateIVASelection($option){
        $option = strtoupper($option);
        switch ($option) {
            case 'SI':
                return "1";
                break;
            
            default:
                return "0";
                break;
        }

    }

      private function ValidateFLETESelection($option){
        $option = strtoupper($option);
        switch ($option) {
            case 'SI':
                return "1";
                break;
            
            default:
                return "0";
                break;
        }

    }

    private function ValidateCompany($company , $money)
    {
        if(isset($company)){
            if($this->ExistCompany($company)){
                    $this->CreateCompanyByExcel($company , $money);
            }else{
              
                $this->UpdateStateCompany($this->GetIdByName($company));
            }
        }
    }

    private function UpdateStateCompany($company)
    {
        $CompanyVo = new CompanyVo();
        $CompanyVo->id = $company;
        $CompanyVo->idSearchField = 0;
        $CompanyVo->state = 1;
        $CompanyVo->idUpdateField = 11;
        $generalDao = new GeneralDao();
        $generalDao->UpdateByField($generalVo = clone $CompanyVo);
    }

    
    private  function GetIdByName($company)
    {
        $companyVo = new CompanyVo();
        $companyVo->idSearchField = 1;
        $companyVo->name = $company;
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $companyVo);
       // print_r("-".$company."-");
        if ($companyVo2 = $generalDao->GetVo($generalVo = clone $companyVo)) {
         //   print_r($companyVo2);
            return $companyVo2->id;
        }
        return 0;
    }

      private function SetDataToVoExcel($company)
    {
        $companyVo = new CompanyVo();
        $companyVo->name = $company;
        $companyVo->nit = "Sin Diligenciar";
        $companyVo->phone =  "Sin Diligenciar";
        $companyVo->email =  "Sin Diligenciar";
        $companyVo->webSite = "Sin Diligenciar";
        $companyVo->address =  "Sin Diligenciar";
        $companyVo->country =  "Sin Diligenciar";
        $companyVo->city =  "Sin Diligenciar";
        $companyVo->creationDate =  date("Y-m-d H:i:s");
        $companyVo->state = 1;
        $companyVo->idRoleCompany = 3;

        return $companyVo;
    }

    private function CreateCompanyByExcel($company , $money)
    {
        $companyVo = $this->SetDataToVoExcel($company);
        $companyVo->id = null;
        $result = $this->generalDao->Set($generalVo = clone $companyVo);
        if (isset($result)) {
            $this->AddDefaultDataToPrices($companyVo , $result , $money);
        }

    }


    private function AddDefaultDataToPrices($companyVo , $id , $money){
        if($companyVo->idRoleCompany == 3){
            $pricesCompanyVo = $this->SetDataPricesCompany($id , $money);
            $result = $this->generalDao->Set($generalVo = clone $pricesCompanyVo);
            $this->CreateImpostCompany($result);
        }
    }

    private  function CreateImpostCompany($idPrice){
        $pricesCompanyVo = $this->SetDataPricesImpost($idPrice);
        $result = $this->generalDao->Set($generalVo = clone $pricesCompanyVo);
    }

    private function SetDataPricesImpost($idCompany){
        $AditionalsImpostVo = new AditionalsImpostVo();
        $AditionalsImpostVo->name = '';
        $AditionalsImpostVo->percent = null;
        $AditionalsImpostVo->idPricesCompany = $idCompany;
        $AditionalsImpostVo->CIF = 0;
        $AditionalsImpostVo->FCA = 0;
        $AditionalsImpostVo->DDP = 0;
        return $AditionalsImpostVo;

    }


    private function SetDataPricesCompany($idCompany , $money){
        $pricesCompanyVo = new pricesCompanyVo();
        $pricesCompanyVo->id = null;
        $pricesCompanyVo->typeMoney = $money;
        $pricesCompanyVo->tpm = $this->GetTasa($money);
        $pricesCompanyVo->ganancia = 0;
        $pricesCompanyVo->idCompany = $idCompany;
        return $pricesCompanyVo;
    }

    
    public function GetTasa($field){
        $arrayList = array();
        $HistoryTRMVo = new HistoryTRMVo();
        $HistoryTRMVo->isList = true;
        $generalDao = new GeneralDao();
        $generalDao->CustomQuery("SELECT * FROM {$HistoryTRMVo->nameTable} WHERE DATE(DATE) >= CURDATE() AND typeMoney='{$field}' ORDER BY DATE DESC , id desc LIMIT 1");
        while($HistoryTRMVo2 = $generalDao->GetVo($generalVo = clone $HistoryTRMVo)) {
          return $HistoryTRMVo2->money;
        }   
        return 0;
}

    private  function ExistCompany($company)
    {

        $companyVo = new CompanyVo();
        $companyVo->idSearchField = 1;
        $companyVo->name = $company;
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $companyVo);
        if ($companyVo2 = $generalDao->GetVo($generalVo = clone $companyVo)) {
            return false;
        }
        return true;
    }

    private function ValidateQuotesInString($value){
        $value = preg_replace('/"/',"\"",$value);
        $value = preg_replace("/'/","\'",$value);
        return $value;
    }

    private function ValidateTypeCurrency($value){
        $value = strtolower($value);
        if($value == "pesos" || $value == "cop" || $value == "pesos colombianos" || $value == "colombianos" ){
         $value = "COP";
        }else if($value == "dollar" || $value == "usd" || $value == "dolares" || $value == "dolar" ){
            $value = "USD";
        }else if($value == "euro" || $value == "eur" || $value == "euros"){
            $value = "USD";
        }else{
            $value = "COP";
        }

        return $value;
    }
    public function HeadStylesPDF(){
        $head = '<html>
                   <head>
                  <style type="text/css" media="screen">
                  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
                  @font-face {
                   font-family: "Arial Narrow";
                   font-style: normal;
                   font-weight: normal;
                   src: url(dompdf/font/arial_narrow_7.ttf);
               }
                       body{
                           background-image: url("view/img/Fondo1.png");
                           background-repeat: no-repeat;
                           font-family: "font";
                       }
                       #i
                           margin-left:-40px;
                           width:200px;
                           height:180px;
                       }
                       #imgv{

                           margin-left:-40px;
                           width:200px;
                           height:180px;
                       }
                       header {
                           position: fixed;
                           bottom: 10px;
                           top: -60px;
                           height: 300px;
                           padding-left: 875px;
                        
                       }
                       footer {
                           position: fixed; 
                           bottom: -60px; 
                           left: 0px; 
                           right: 0px;
                           height: 50px;         
                           text-align: center;
                           line-height: 35px;
                           opacity: 0.5; 
                       }

                        .alignright {
                            opacity: 0.5;
                              width: 85%;
                           height:auto;
                           text-align: left;
                       }
                       #container{
                           width: 80%;
                       }
                       p{
                           font-family: "Arial Narrow", sans-serif;
                           font-size:18px;
                           
                           
                       }
                         .table, h2 {
                           page-break-inside: avoid !important;
                           border-collapse: separate;
                           border-spacing: 2px 3px;
                       }

                        table th, th {
                           page-break-inside: avoid !important;
                           border: black 1px solid ;
                           text-align: center;
                           font-weight:normal;
                           color:black;
                           padding: 8px;
                       }
                        .react th, th {
                           page-break-inside: avoid !important;
                           border: black 1px solid;
                            font-weight:normal;
                           color:black;
                           padding: 8px;
                       }
                  </style>
               </head>';
               return $head;
   }

   public function ColumnDataCompany(){
    $header =  '<body  style="margin-left:20px;font-family:Narrow">
                    <header style="margin-bottom:35px">
                        <div class="alignright" style="margin-bottom:35px">
                            <div id="logoContainer" style="height:100px; margin-bottom:35px">
                                <img id="imgv" style="margin-top:3%;display:relative;padding-right:80%" src="view/img/main-logo.png"/>
                            </div> <br> <br>

                            <div style="line-height:0.4;margin-top:7%;text-align:right">
                                 <p style="font-size:85%">Vortex Company SAS</p>                                <!--  Dirección   -->
                                 <p style="font-size:85%" > Teléfono: XXXXXXX</p>                        <!--  Celular   -->
                                 <p style="font-size:85%" >XXXXXXXXXX</p> <hr>                         <!--  sitio Web   -->
                                <p style="font-size:85%">XXXXXX</p> <hr>                              <!--  Correo   -->
                                <p style="font-size:85%">XXXXXXXXXX</p>         <!--  ciudad   -->
                                <p style="font-size:85%">Sur America</p>                                            <!--  Ubicación   -->
                            </div>        
                        </div>
                    </header>';
    return $header;
}


      private function ValidateTypeProduct($value){
        $value = strtolower($value);
        if($value == "equipos"){
         $value = "Equipos";
        }else if($value == "consumibles" ){
            $value = "Consumibles";
        }else if($value == "servicios"){
            $value = "Servicios";
        }else{
            $value = "Equipos";
        }

        return $value;
    }
   
    public function DataClientCompany(){
       
        $regards ='
        <div class="serif" style="margin-top:0%;font-family:Narrow;;margin-left:27px">
            <div style="width:76%;line-height:0.5">
                <p style="text-align: right;margin-left:300px;"><label style="font-weight:bold">Cotización No. XXX</label></p>
                <p style="text-align: right;margin-left:300px;"> V.x </p>
            </div>
        <div style="margin-bottom:25px;">
            xx/xx/xxxx
        </div> <br>

        <div style="line-height:0.4;margin:2px;">
            <p>Señores </p>
            <p><label style="font-weight: 600; !important"> PRUEBA DE TESTING</label></p><p>';
        $regards .= '</p> </div><br><br><p>Estimado   XXXXX señor(a)';
        $regards .= '</p>
            <p>Para VORTEX es un gusto dejar para su consideración la propuesta económica solicitada</p>
        </div>
        ';   

        return $regards;
    }

    private function ValidateIsInt($value){
        return $value;
    }

    private function ValidatePrices($value){
      $value =  str_replace(".",",",$value);
 	  $value =  str_replace(",",".",$value);
      $value =  str_replace("$","",$value);
      $value =  str_replace("€","",$value);
      
      return $value;
    }


    public function InsertDataExcel($numRows,$i,$inventoryVo){
        $generalDao = new GeneralDao();
        $result = $generalDao->Set($generalVo = clone $inventoryVo);

        if($i >= $numRows){
            $message = '<script> MessageSwalBasic("Insertado!","Se ha agregado la plantilla Excel Correctamente, Numero de registros: "+'.($numRows-1).',"success"); </script>';
            echo $message;
        }
    }

}
