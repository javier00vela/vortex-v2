<?php
require_once(__DIR__ . '/../../../../controller/general/Config.php');
require_once(Config::PATH . Config::CONTROLLER.Config::GENERAL . 'ManageArrays.php');
require_once(Config::PATH . Config::BACKEND . 'modules/CompanyVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/ProductVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/InventoryVo.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralDao.php');
require_once(Config::PATH . Config::BACKEND . 'modules/PricesCompanyVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/AditionalsImpostVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/HistoryTRMVo.php');
require_once(Config::PATH . Config::CONTROLLER_LIB . 'vendor/phpoffice/phpexcel/Classes/PHPExcel/IOFactory.php');


class AjaxDataInventory
{

    private $isActionFromAjax;
    private $optionInputForm;
    private $idDataInputForm;
    private $tasaEUR = 0;
    private $tasaUSD = 0;
    public $dataList = [];

    function __construct()
    {
        if (isset($_POST['isActionFromAjax'])) {

            session_start();
            $this->isActionFromAjax = $_POST['isActionFromAjax'];
            $this->optionInputForm = $_POST['optionInputForm'];
            $this->idDataInputForm = $_POST['idDataInputForm'];
                if(isset($_POST["listData"])){
                    $this->dataList = $_POST["listData"];  
                }
            if ($this->isActionFromAjax == "true") {
                if ($this->optionInputForm == 3) {
                    $this->SetDataInControls();
                }else if($this->optionInputForm == 2){
                    $this->GetMoneyProvider();
                }else if($this->optionInputForm == 4){
                    $this->DuplicateProduct();
                }else if ($this->optionInputForm == 5) {
                    $this->UpdateStateUser();
                }else if ($this->optionInputForm == 6) {
                    //$this->RecreateTableAjax();
                    $this->RecreateTableAjaxWord();  
                }else if ($this->optionInputForm == 7) {
                    $this->GetLengthProducts();
                }else if ($this->optionInputForm == 8) {
                    $this->RecreateTableAjaxWord();  
                }else {

                    $this->GetDataInJsonForTbl();
                }
            }
        }
    }

    public function SetDataInControls()
    {
        $SetInputsDataFJQ = "";
        $inventoryVo = new InventoryVo();
        $inventoryVo->id = $this->idDataInputForm;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $inventoryVo);
        if ($inventoryVo2 = $generalDao->GetVo($generalVo = clone $inventoryVo)) {
            //$descriptionHtml = trim($productVo2->description," ");
            $unit = $inventoryVo2->priceUnit - $inventoryVo2->FLETE;
            $descriptionHtml = preg_replace("[\n|\r|\n\r]", '', $inventoryVo2->description);
            $SetInputsDataFJQ = "
                  <script>
                    if({$inventoryVo2->FLETE} > 0){
                        $('#isTrueFlete').val('SI');
                        $('#productFLETELst').removeAttr('disabled');
                        $('#fleteHiddenW').removeClass('hidden');
                    }

                    (function($){
                                SetValueToSelect('productTypeProductLst','{$inventoryVo2->typeProduct}',true);
                                SetValueToInputText('productNameCtx','$inventoryVo2->name');
                                SetValueToInputText('productBrandCtx','{$inventoryVo2->brand}');
                                SetValueToInputText('productReferenceCtx','{$inventoryVo2->reference}');
                                SetValueToInputText('productUnitMeasurementCtx','{$inventoryVo2->presentation}');
                                SetValueToSelect('productTypeCurrencyLst','{$inventoryVo2->typeCurrency}',true);
                                SetValueToInputText('productAmountCtx','{$inventoryVo2->amount}');
                                 SetValueToInputText('productTypeCurrencyLst','{$inventoryVo2->typeCurrency}');
                                SetValueToInputText('productMinimumAmountCtx','{$inventoryVo2->minimumAmount}');
                                if({$inventoryVo2->FLETE} == 0){
                                    $('#fleteHiddenW').attr('class','form-group col-xs-12 col-sm-3 col-md-3 col-lg-3 hidden');
                                    $('#isTrueFlete').val('NO')
                                     SetValueToInputText('productPriceUnitCtx','{$unit}');
                                }else{
                                    $('#fleteHiddenW').attr('class','form-group col-xs-12 col-sm-3 col-md-3 col-lg-3 ');
                                    $('#isTrueFlete').val('SI')
                                    SetValueToInputText('productPriceUnitCtx','{$unit}');
                                }
                                

                                SetValueToInputText('productIVALst','{$inventoryVo2->IVA}');
                                SetValueToInputText('productFLETELst','{$inventoryVo2->FLETE}');
                                
                                
                                $('#productCountryLst').select2('destroy'); 
                                SetValueToSelect('productCountryLst','{$inventoryVo2->country}',true);
                                SetSelect2Countries();
                                //GetCompany
                                SetValueToSelect('productCompanyLst','$inventoryVo2->nameCompany',true);
                                SetValueToSelect('productPlaceLst','$inventoryVo2->namePlace',true);
                                //SetValueToInputText('productDescriptionAtx','$descriptionHtml');
                                ApplyEditor('productDescriptionAtx','$descriptionHtml');
                                SetValueToInputText('optionInputForm',4);
                                SetValueToInputText('idDataInputForm',$inventoryVo2->id);
                      })(jQuery);
                  </script>
                            ";
        }
        echo $SetInputsDataFJQ;
    }

    public function GetDataInJsonForTbl()
    {
        $tasaEUR = $this->GetTasa("EUR");
        $tasaUSD = $this->GetTasa("USD");

        if(!isset($_SESSION["word"])){
        $existData = false;
        $inventoryVo = new InventoryVo();
        $inventoryVo->isList = true;
        $generalDao = new GeneralDao();
        $generalDao->CustomQuery("select * from  (select * from inventory Limit 0 , 1000 ) inventory ORDER BY id desc");
        $string = "
        {\"data\": [";
        while ($inventoryVo2 = $generalDao->GetVo($generalVo = clone $inventoryVo)) {
            $existData = true;
            $json = array();
            $json[] = $inventoryVo2->id;
            $json[] = htmlspecialchars($inventoryVo2->typeProduct);
            $json[] = htmlspecialchars(ucwords(strtolower($inventoryVo2->name)));
            $json[] = htmlspecialchars($inventoryVo2->brand);
            $json[] = htmlspecialchars($inventoryVo2->reference);
            if($inventoryVo2->amount <= $inventoryVo2->minimumAmount){
                $json[] = "<h4 class='text-center' style='margin: 0;padding:0;color: #ff5825;'><b>$inventoryVo2->amount</b></h4>";
            }else{
                $json[] = "<h4 class='text-center' style='margin: 0;padding:0;color: #51ba35;'><b>$inventoryVo2->amount</b></h4>";
            }
            $json[] = $inventoryVo2->typeCurrency;
            if(is_numeric($inventoryVo2->priceUnit)){
            $json[] = "$".$inventoryVo2->priceUnit;
            }else{
                $exp = explode(",",$inventoryVo2->priceUnit);
            $json[] = "$".$exp[0].",".substr(ceil($exp[1]) , 0 , 2);
            }
            $json[] = $inventoryVo2->presentation;
            $json[] = ucfirst(strtolower($inventoryVo2->country));
            $json[] = $inventoryVo2->nameCompany;
            $json[] = $inventoryVo2->namePlace;
                if($inventoryVo2->typeCurrency == "EUR"){
                    $json[] =  str_replace(",",".",number_format($tasaEUR * str_replace(".","",$inventoryVo2->priceUnit)))." COP";
                }else if($inventoryVo2->typeCurrency == "USD"){
                    $json[] =  str_replace(",",".",number_format($tasaUSD * str_replace(",",".",$inventoryVo2->priceUnit))." COP");  
                }else{
                   $json[] =   "$".str_replace(",",".",number_format($inventoryVo2->priceUnit))." COP";  
                }
             $json[] = ( $inventoryVo2->IVA == 0 )?  "<p style='color:red'>No Aplica El IVA</p>" : " <p style='color:green'>Aplica El IVA</p>";
             $json[] = "$ ".str_replace(",",".",number_format($this->GetPriceToSell($inventoryVo2  , $tasaUSD , $tasaEUR)))." COP";
             if(is_numeric($inventoryVo2->FLETE)){
                  $json[] = "$".$inventoryVo2->FLETE;
            }else{

                $expF = explode(",",$inventoryVo2->FLETE);
  
            $json[] = "$".$expF[0].",".substr($expF[1] , 0 , 2);
            }
            $button = "";
            if(!in_array( "ver_historial_productos" , $this->dataList)){
                $button .= "<a class='btn btn-success btn-xs green-tooltip' name='allows' data-allow='1,11' data-toggle='tooltip' data-placement='top' title='Historial Productos' id='btnHistory' href='HistoryProducts?idProductInputForm=".$inventoryVo2->id."'><i class=' fa fa-book'></i></a> ";
            }

            if(!in_array( "ver_seriales" , $this->dataList)){
                $button .= " <a id='serialBtn' name='serialBtn' data-toggle='tooltip' data-placement='top' class='btn btn-primary btn-xs blue-tooltip'  title='Administrar Seriales' onclick='SerialData({$inventoryVo2->id});'><i class='fa fa-barcode'></i></a> ";
            }
            if(!in_array( "copiar_producto" , $this->dataList)){
                $button .=" <a id='duplicateBtn' data-toggle='tooltip' data-placement='top' title='Copiar Registro' onclick='CloneRow($inventoryVo2->id)' name='duplciateBtn' class='btn btn-default btn-xs white-tooltip' ><i class='fa fa-clone'></i></a> ";
            }
            if(!in_array( "editar_producto" , $this->dataList)){
                 $button .=" <space data-toggle='tooltip' data-placement='top' class='yellow-tooltip' title='Modificar'> <a id='updateBtn' name='updateBtn'  class='btn btn-warning btn-xs ' data-toggle='modal' data-target='#productPopUpAdd'  onclick='UpdateData({$inventoryVo2->id},\"inventory\",\"inventory\");'><i class='fa fa-pencil'></i></a></space> ";
            }
            if(!in_array( "eliminar_producto" , $this->dataList)){
                $button .=" <a id='deleteBtn' name='deleteBtn' data-toggle='tooltip' data-placement='top' class='btn btn-danger  btn-xs red-tooltip' title='Eliminar' onclick='DeleteData({$inventoryVo2->id},\"inventory\")'><i class='fa fa-times'></i></a></a> ";
            }
            $json[] = $button;

            $string .= json_encode($json) . ",";
        }
        $string .= "-]}-";
        if($existData){
            echo  str_replace(",-]}-", "]}", $string);
        }else{
            echo "{ \"data\": [] }";
        }

        }else{
           $_POST["word"] = str_replace("'","\'",$_SESSION["word"]);
            $this->RecreateTableAjaxWord();

        }
    }

    function RecreateTableAjax(){
        $tasaEUR = $this->GetTasa("EUR");
        $tasaUSD = $this->GetTasa("USD");
        
         $existData = false;
        $inventoryVo = new InventoryVo();
        $inventoryVo->isList = true;
        $generalDao = new GeneralDao();
        $generalDao->CustomQuery("select * from inventory Limit ".($_POST["range"])." , 1000");
        $string = "
        {\"data\": [";
        while ($inventoryVo2 = $generalDao->GetVo($generalVo = clone $inventoryVo)) {
            $existData = true;
            $json = array();
            $json[] = $inventoryVo2->id; 
            $json[] = htmlspecialchars($inventoryVo2->typeProduct);
            $json[] = htmlspecialchars(ucwords(strtolower($inventoryVo2->name)));
            $json[] = htmlspecialchars($inventoryVo2->brand);
            $json[] = htmlspecialchars($inventoryVo2->reference);
            if($inventoryVo2->amount <= $inventoryVo2->minimumAmount){
                $json[] = "<h4 class='text-center' style='margin: 0;padding:0;color: #ff5825;'><b>$inventoryVo2->amount</b></h4>";
            }else{
                $json[] = "<h4 class='text-center' style='margin: 0;padding:0;color: #51ba35;'><b>$inventoryVo2->amount</b></h4>";
            }
            $json[] = $inventoryVo2->typeCurrency;
                    if(is_numeric($inventoryVo2->priceUnit)){
                 $exp = explode(",",$inventoryVo2->priceUnit);
                     $exp = explode(",",$inventoryVo2->priceUnit);

            $json[] = "$".$exp[0].",".substr(ceil($exp[1]) , 0 , 2);
            }else{

                $exp = explode(",",$inventoryVo2->priceUnit);
  
            $json[] = "$".$exp[0].",".substr(ceil($exp[1]) , 0 , 2);
            }
            $json[] = $inventoryVo2->presentation;
            $json[] = ucfirst(strtolower($inventoryVo2->country));
            $json[] = ucwords(strtolower($inventoryVo2->nameCompany));
            $json[] = $inventoryVo2->namePlace;
               if($inventoryVo2->typeCurrency == "EUR"){
                    $json[] =  str_replace(",",".",number_format($tasaEUR * floatval(str_replace(".","",$inventoryVo2->priceUnit))))." COP";
                }else if($inventoryVo2->typeCurrency == "USD"){
                    //print_r($tasaEUR);
                    $json[] =  str_replace(",",".",number_format($tasaUSD * floatval($inventoryVo2->priceUnit)))." COP";  
                }else{
                   $json[] =   "$".str_replace(",",".",number_format($inventoryVo2->priceUnit))." COP";  
                }
           $json[] = ( $inventoryVo2->IVA == 0 )?  "<p style='color:red'>No Aplica El IVA</p>" : " <p style='color:green'>Aplica El IVA</p>";
           $button = "";
            if(!in_array( "ver_historial_productos" , $this->dataList)){
                $button .= "<a class='btn btn-success btn-xs green-tooltip' name='allows' data-allow='1,11' data-toggle='tooltip' data-placement='top' title='Historial Productos' id='btnHistory' href='HistoryProducts?idProductInputForm=".$inventoryVo2->id."'><i class=' fa fa-book'></i></a> ";
            }

            if(!in_array( "ver_seriales" , $this->dataList)){
                $button .= " <a id='serialBtn' name='serialBtn' data-toggle='tooltip' data-placement='top' class='btn btn-primary btn-xs blue-tooltip'  title='Administrar Seriales' onclick='SerialData({$inventoryVo2->id});'><i class='fa fa-barcode'></i></a> ";
            }
            if(!in_array( "copiar_producto" , $this->dataList)){
                $button .=" <a id='duplicateBtn' data-toggle='tooltip' data-placement='top' title='Copiar Registro' onclick='CloneRow($inventoryVo2->id)' name='duplciateBtn' class='btn btn-default btn-xs white-tooltip' ><i class='fa fa-clone'></i></a> ";
            }
            if(!in_array( "editar_producto" , $this->dataList)){
                 $button .=" <space data-toggle='tooltip' data-placement='top' class='yellow-tooltip' title='Modificar'> <a id='updateBtn' name='updateBtn'  class='btn btn-warning btn-xs ' data-toggle='modal' data-target='#productPopUpAdd'  onclick='UpdateData({$inventoryVo2->id},\"inventory\",\"inventory\");'><i class='fa fa-pencil'></i></a></space> ";
            }
            if(!in_array( "eliminar_producto" , $this->dataList)){
                $button .=" <a id='deleteBtn' name='deleteBtn' data-toggle='tooltip' data-placement='top' class='btn btn-danger  btn-xs red-tooltip' title='Eliminar' onclick='DeleteData({$inventoryVo2->id},\"inventory\")'><i class='fa fa-times'></i></a></a> ";
            }
            $json[] = $button;

            $string .= json_encode($json) . ",";
        }
        $string .= "-]}-";
        if($existData){
            echo  str_replace(",-]}-", "]}", $string);
        }else{
            echo "{ \"data\": [] }";
        }
    }

     function RecreateTableAjaxWord(){

         $existData = false;
        $tasaEUR = $this->GetTasa("EUR");
         $tasaUSD =$this->GetTasa("USD");

        $inventoryVo = new InventoryVo();
        $inventoryVo->isList = true;
        $generalDao = new GeneralDao();
        $_POST["word"] = str_replace("'","\'",$_POST["word"]);
        $_SESSION["word"] = $_POST["word"];
        $generalDao->CustomQuery("select * from inventory where id > 0 and id < 1000000 AND nameCompany LIKE '%".$_POST["word"]."%' or name LIKE '%".$_POST["word"]."%' OR reference LIKE '%".$_POST["word"]."%' OR brand LIKE '%".$_POST["word"]."%'  OR presentation LIKE '%".$_POST["word"]."%' LIMIT 2000 ");
        $string = "
        {\"data\": [";
        while ($inventoryVo2 = $generalDao->GetVo($generalVo = clone $inventoryVo)) {
            $existData = true;
            $json = array();
            $json[] = $inventoryVo2->id;
            $json[] = htmlspecialchars($inventoryVo2->typeProduct);
            $json[] = htmlspecialchars(ucwords(strtolower($inventoryVo2->name)));
            $json[] = htmlspecialchars($inventoryVo2->brand);
            $json[] = htmlspecialchars($inventoryVo2->reference);
            if($inventoryVo2->amount <= $inventoryVo2->minimumAmount){
                $json[] = "<h4 class='text-center' style='margin: 0;padding:0;color: #ff5825;'><b>$inventoryVo2->amount</b></h4>";
            }else{
                $json[] = "<h4 class='text-center' style='margin: 0;padding:0;color: #51ba35;'><b>$inventoryVo2->amount</b></h4>";
            }
            $json[] = $inventoryVo2->typeCurrency;
            if(is_numeric($inventoryVo2->priceUnit)){
                  $json[] = "$".$inventoryVo2->priceUnit;
            }else{

                $exp = explode(",",$inventoryVo2->priceUnit);
  
            $json[] = "$".$exp[0].",".substr(ceil($exp[1]) , 0 , 2);
            }
            $json[] = $inventoryVo2->presentation;
            $json[] = ucfirst(strtolower($inventoryVo2->country));
            $json[] =  $inventoryVo2->nameCompany;
            $json[] = $inventoryVo2->namePlace;
               if($inventoryVo2->typeCurrency == "EUR"){
                    $json[] =  str_replace(",",".",number_format($tasaEUR * (str_replace(",",".",$inventoryVo2->priceUnit))))." COP";
                }else if($inventoryVo2->typeCurrency == "USD"){
                    $json[] =  str_replace(",",".",number_format($tasaUSD * str_replace(",",".",$inventoryVo2->priceUnit)))." COP";  
                }else{
                   $json[] =   "$".str_replace(",",".",number_format($inventoryVo2->priceUnit))." COP";  
                }

            $json[] = ( $inventoryVo2->IVA == 0 )?  "<p style='color:red'>No Aplica El IVA</p>" : " <p style='color:green'>Aplica El IVA</p>";
            $json[] = "$ ".str_replace(",",".",number_format($this->GetPriceToSell($inventoryVo2 , $tasaUSD , $tasaEUR)))." COP";
            if(is_numeric($inventoryVo2->FLETE)){
                  $json[] = "$".$inventoryVo2->FLETE;
            }else{

                $expF = explode(",",$inventoryVo2->FLETE);
  
            $json[] = "$".$expF[0].",".substr(ceil($expF[1]) , 0 , 2);
            }
            $button = "";
            if(!in_array( "ver_historial_productos" , $this->dataList)){
                $button .= "<a class='btn btn-success btn-xs green-tooltip' name='allows' data-allow='1,11' data-toggle='tooltip' data-placement='top' title='Historial Productos' id='btnHistory' href='HistoryProducts?idProductInputForm=".$inventoryVo2->id."'><i class=' fa fa-book'></i></a> ";
            }

            if(!in_array( "ver_seriales" , $this->dataList)){
                $button .= " <a id='serialBtn' name='serialBtn' data-toggle='tooltip' data-placement='top' class='btn btn-primary btn-xs blue-tooltip'  title='Administrar Seriales' onclick='SerialData({$inventoryVo2->id});'><i class='fa fa-barcode'></i></a> ";
            }
            if(!in_array( "copiar_producto" , $this->dataList)){
                $button .=" <a id='duplicateBtn' data-toggle='tooltip' data-placement='top' title='Copiar Registro' onclick='CloneRow($inventoryVo2->id)' name='duplciateBtn' class='btn btn-default btn-xs white-tooltip' ><i class='fa fa-clone'></i></a> ";
            }
            if(!in_array( "editar_producto" , $this->dataList)){
                 $button .=" <space data-toggle='tooltip' data-placement='top' class='yellow-tooltip' title='Modificar'> <a id='updateBtn' name='updateBtn'  class='btn btn-warning btn-xs ' data-toggle='modal' data-target='#productPopUpAdd'  onclick='UpdateData({$inventoryVo2->id},\"inventory\",\"inventory\");'><i class='fa fa-pencil'></i></a></space> ";
            }
            if(!in_array( "eliminar_producto" , $this->dataList)){
                $button .=" <a id='deleteBtn' name='deleteBtn' data-toggle='tooltip' data-placement='top' class='btn btn-danger  btn-xs red-tooltip' title='Eliminar' onclick='DeleteData({$inventoryVo2->id},\"inventory\")'><i class='fa fa-times'></i></a></a> ";
            }
            $json[] = $button;

            $string .= json_encode($json) . ",";
        }
        $string .= "-]}-";

        if($existData){
           if(in_array(",,-]}-" , $json)){
            echo str_replace(",,-]}-", "]}", $string);
           }else{
            echo  str_replace(",]}", "]}", str_replace(",-]}-", "]}", $string));
           }
            
        }else{
            echo "{ \"data\": [] }";
        }
    }

    function GetMoneyProvider(){
        //print_r($this->idDataInputForm);
        $idCompany = $this->GetIdCompanyForName($this->idDataInputForm);
        $moneyCompany = $this->GetDataPricesCompany($idCompany);
        print_r($moneyCompany->typeMoney);
       
    }
    function GetIdCompanyForName($nameCompany){
        $roleCompanyVo = new companyVo();
        $generalDao = new GeneralDao();
        $generalDao->CustomQuery("Select * from company where name='{$nameCompany}' and idRoleCompany='3' ");
        if($roleCompanyVo2 = $generalDao->GetVo($generalVo = clone $roleCompanyVo)) {
                return $roleCompanyVo2->id;
        }
        return 0;
    }
    function GetDataPricesCompany($idCompany){
        $companyVo = new PricesCompanyVo();
        $companyVo->idCompany = $idCompany;
        $companyVo->idSearchField = 4;
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $companyVo);
        if($companyVo2 = $generalDao->GetVo($generalVo = clone $companyVo)) {
           return $companyVo2;
        }
        return "error en la consulta";
    }
    function GetCompany($idCompany)
    {
        $companyVo = new CompanyVo();
        $companyVo->id = $idCompany;
        $companyVo->idSearchField = 0;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $companyVo);
        if ($companyVo2 = $generalDao->GetVo($generalVo = clone $companyVo)) {
            return  $companyVo2->name;
        }
        return  $idCompany;
    }

      public function GetLengthProducts(){
        $arrayList = array();
        $inventoryVo = new inventoryVo();
        $inventoryVo->isList = true;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $inventoryVo);
        print_r($generalDao->GetLength());
    }

    private function GetPlaceProduct($placeProduct){
        switch($placeProduct){
            case 1:{
                return "Nacional";
            }
            case 2:{
                return "Internacional";
            }
            default:{
                return false;
            }
        }
    }
    private function DuplicateProduct(){
        
         $inventoryVo2 = $this->GetVoProductForId($_POST["idRow"]);
       
        $generalDao = new GeneralDao();
        $inventoryVo = new InventoryVo();
        $inventoryVo->typeProduct =  $inventoryVo2->typeProduct;
        $inventoryVo->name = $inventoryVo2->name;
        $inventoryVo->brand = $inventoryVo2->brand; 
        $inventoryVo->reference = $inventoryVo2->reference;
        $inventoryVo->presentation = $inventoryVo2->presentation;
        $inventoryVo->typeCurrency =   $inventoryVo2->typeCurrency;
        $inventoryVo->amount = $inventoryVo2->amount; 
        $inventoryVo->minimumAmount =  $inventoryVo2->minimumAmount;
        if(isset($inventoryVo2->FLETE)){
            $inventoryVo->FLETE = $inventoryVo2->FLETE;
        }else{
            $inventoryVo->FLETE = '0'; 
        }
        if(empty($inventoryVo2->FLETE)){
            $inventoryVo->priceUnit = floatval($inventoryVo2->priceUnit) +  0;
        }else{
        $inventoryVo->priceUnit = floatval($inventoryVo2->priceUnit) +  floatval($inventoryVo2->FLETE);
        }
        $inventoryVo->country = $inventoryVo2->country;
        $inventoryVo->nameCompany =$inventoryVo2->nameCompany; 
        $inventoryVo->namePlace = $inventoryVo2->namePlace; 
        $inventoryVo->description = str_replace("'", '"',$inventoryVo2->description); 
        $inventoryVo->IVA = $inventoryVo2->IVA; 
        

         $result = $generalDao->Set($generalVo = clone $inventoryVo);
    }
    private function GetVoProductForId($id){
        $inventoryVo = new InventoryVo();
        $productVoCopy = new InventoryVo();
         $generalDao = new GeneralDao();
        $inventoryVo->id = $id;
        $inventoryVo->idSearchField = 0;
           
        $generalDao->GetById($generalVo = clone $inventoryVo);
          
        while ($inventoryVo2 = $generalDao->GetVo($generalVo = clone $inventoryVo)){
            $productVoCopy = $inventoryVo2;
        }
        
        return $productVoCopy;
    }

        public function GetTasa($field){
            $arrayList = array();
            $HistoryTRMVo = new HistoryTRMVo();
            $HistoryTRMVo->isList = true;
            $generalDao = new GeneralDao();
            $generalDao->CustomQuery("SELECT * FROM {$HistoryTRMVo->nameTable} WHERE DATE(DATE) >= CURDATE() AND typeMoney='{$field}' ORDER BY DATE DESC , id desc LIMIT 1");
            while($HistoryTRMVo2 = $generalDao->GetVo($generalVo = clone $HistoryTRMVo)) {
              return $HistoryTRMVo2->money;
            }   
            return 0;
    }

    public function GetPriceToSell($inventoryVo2 , $usd , $eur){
        if($inventoryVo2->typeCurrency == "EUR"){
            $priceCOP =  $eur * str_replace(",",".",str_replace(".","",$inventoryVo2->priceUnit));
        }else if($inventoryVo2->typeCurrency == "USD"){
            $priceCOP =  $usd * str_replace(",",".",str_replace(".","",$inventoryVo2->priceUnit));
        }else{
            $priceCOP = $inventoryVo2->priceUnit;  
        }
        return $this->CalculateInternationalPrice($priceCOP , $inventoryVo2);
    }

    private function CalculateInternationalPrice($priceCOP , $inventoryVo){
        if ($inventoryVo->namePlace == "Internacional") {
            $dataGlobalPrices = $this->GetDataCompanyPrices($inventoryVo->nameCompany);
            return $this->GetCalculePrice($priceCOP, $dataGlobalPrices[1],$dataGlobalPrices[0], "DDP");
        }else{
            return $priceCOP;
        }
    }

    private function GetCalculePrice($priceNormal, $arrayAditionalsImpost, $arrayPricesCompany, $typeQuotation) {
        $impost = $this->GetPriceWithImpost($priceNormal, $arrayAditionalsImpost, $typeQuotation);
        // hasta aqui se suman bien el precio
        $priceTotalVenta = $this->GetPriceSale($priceNormal, $arrayPricesCompany->ganancia, $impost); //se le suma la ganancia propuesta por el proveedor 
        //console.log("precio total con ganancia --> "+priceTotalVenta);
        return $priceTotalVenta;
    }

    private function GetPriceWithImpost($price, $porcentajes, $typeQuotation) {

        $totalPercent = 0;
        $porcentaje = 0;
        for ($i = 0; $i < count($porcentajes); $i++) {
            if ($porcentajes[$i]->DDP == 1) {

                $porcentaje += $this->GetPercent(floatval(str_replace(",",".",$porcentajes[$i]->percent)));
            }

        }

        return $porcentaje;
    }

    private function GetPercent ($percent) {
        $percent2 = "";
        //console.log("porcentaje ingresado : "+percent);
        $percent2 = ($percent / 100);

        return $percent2;
    }

    private function GetPriceSale($price, $sale, $impost) {
        //console.log("================================= se va a calcular el impuesto y el porcentaje ===================================");
        $sale = $this->GetPercent($sale);
        $sale = $impost + $sale;
        //console.log("la suma del impuesto y el procentaje es ====== "+sale);
        $price = $price + ($price * $sale);
        //console.log("se obtiene el precio total con ganancia apartir de esta formulala  "+price+"+"+"("+price+"*"+sale+")");
        //console.log("el resultado obtenido es "+price);
        return $price;
    }

    

    function GetDataCompanyPrices($nameCompany){
        $dataPricesCompany = array();
        $idCompany = $this->GetIdCompanyForName($nameCompany);
        $dataPricesCompany = $this->GetDataPricesCompany($idCompany);
        $dataAditionalsImpost = $this->GetDataAditionalsImpostForCompany($dataPricesCompany->id);
        
        $dataGlobalPrices = array($dataPricesCompany, $dataAditionalsImpost);

        return  $dataGlobalPrices;
       
    }
 


    function GetDataAditionalsImpostForCompany($idPricesCompany){
        $dataAditionalsImpost = array();
        if(isset($idPricesCompany)){
            $aditionalsImpostVo = new AditionalsImpostVo();
            $aditionalsImpostVo->idPricesCompany = $idPricesCompany;
            $aditionalsImpostVo->idSearchField = 3;
            $generalDao = new GeneralDao();
            $generalDao->GetByField($generalVo = clone $aditionalsImpostVo);
            while($aditionalsImpostVo2 = $generalDao->GetVo($generalVo = clone $aditionalsImpostVo)) {
                array_push($dataAditionalsImpost,$aditionalsImpostVo2);
            }
        }
        return $dataAditionalsImpost;
    }


}
$ajaxDataInventory = new AjaxDataInventory();
