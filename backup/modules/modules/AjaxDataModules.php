<?php
require_once(__DIR__ . '/../../../../controller/general/Config.php');
require_once(Config::PATH . Config::CONTROLLER.Config::GENERAL . 'ManageArrays.php');
require_once(Config::PATH . Config::BACKEND . 'modules/RoleVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/ModuleVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/ModuleRoleVo.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralDao.php');


class AjaxDataModules{

    private $isActionFromAjax;
    private $optionInputForm;
    private $idDataInputForm;

    private $idTemp;

    function __construct()
    {
        if (isset($_POST['isActionFromAjax'])) {
            $this->isActionFromAjax = $_POST['isActionFromAjax'];
            $this->optionInputForm = $_POST['optionInputForm'];
            $this->idDataInputForm = $_POST['idDataInputForm'];

            if ($this->isActionFromAjax == "true") {
                if ($this->optionInputForm == 3) {
                    $this->SetDataInControls();
            }else if($this->optionInputForm == 5){
                $this->GetRoles();
            }else if($this->optionInputForm == 7){
                $this->idTemp = $_POST["idRol"];
                $this->GetRolesById();
            }else if($this->optionInputForm == 9){
                 $this->idTemp = $_POST["id"];
                  $this->stateTemp = $_POST["state"];
                $this->SetStateModules();
            }else {
                    $this->GetDataInJsonForTbl();
                }
            }
        }
    }

    private function SetDataInControls(){
        $SetInputsDataFJQ = "";
        $impostVo = new ImpostVo();
        $impostVo->id = $this->idDataInputForm;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $impostVo);
        if($impostVo2 = $generalDao->GetVo($generalVo = clone $impostVo)){
            $SetInputsDataFJQ = "
                            <script>
                               (function($){
                                SetValueToInputText('impostNameCtx','{$impostVo2->name}');
                                SetValueToInputText('impostPercentCtx','{$impostVo2->percent}');
                                SetValueToInputText('optionInputForm',4);
                                SetValueToInputText('idDataInputForm',$impostVo2->id);
                               })(jQuery);
                            </script>
                        ";
        }
        echo $SetInputsDataFJQ;
    }

    public function GetRoles()
    {
        $rolesVo = new RoleVo();
        $rolesVo->isList = true;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $rolesVo);
        while ($rolesVo2 = $generalDao->GetVo($generalVo = clone $rolesVo)) {
            $arrayList[] =$rolesVo2 ;
        }
        print_r( json_encode($arrayList) );
    }

    public function GetRolesById(){
        $arrayList = [];
        $rolesVo = new ModuleRoleVo();
        $rolesVo->idSearchField = 1;
        $rolesVo->idRol = $this->idTemp;
        $generalDao = new GeneralDao();


        $generalDao->GetByField($generalVo = clone $rolesVo);
        while ($rolesVo2 = $generalDao->GetVo($generalVo = clone $rolesVo)) {
                 $arrayList[] = ["id"=>$rolesVo2->id , "alias"=>$this->GetNameModule($rolesVo2->idModule)->subName , "state"=>$rolesVo2->state];
        }


        print_r( json_encode($arrayList) );
    }


    public  function SetStateModules()
    {
        $roleVo = new ModuleRoleVo();
        $roleVo->id = $this->idTemp;
        $roleVo->idSearchField = 0;
        $roleVo->state = $this->DeterminateState($this->stateTemp);
        $roleVo->idUpdateField = 3;
        $generalDao = new GeneralDao();
        $generalDao->UpdateByField($generalVo = clone $roleVo);
    }

    public function DeterminateState($state){
        if($state == 0){
            return 1;
        }
        return 0;
    }




    public function GetNameModule($id){
        $moduleVo = new ModuleVo();
        $moduleVo->isList = false;
        $moduleVo->idSearchField = 0;
        $moduleVo->id = $id;
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $moduleVo);
        while($moduleVo2 = $generalDao->GetVo($generalVo = clone $moduleVo)) {
            return $moduleVo2;
        }
        return "error al realizar consulta";
    }

    
}

$ajaxDataImpost = new AjaxDataModules();
