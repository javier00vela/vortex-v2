<?php
require_once(__DIR__ . '/../../../../controller/general/Config.php');
require_once(Config::PATH . Config::CONTROLLER.Config::GENERAL . 'ManageArrays.php');
require_once(Config::PATH . Config::BACKEND . 'modules/DiagnosisVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/PhotosDiagnosisVo.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralDao.php');

class AjaxDataImpost{

    private $isActionFromAjax;
    private $optionInputForm;
    private $idDataInputForm;

    function __construct()
    {
        if (isset($_POST['isActionFromAjax'])) {
            $this->isActionFromAjax = $_POST['isActionFromAjax'];
            $this->optionInputForm = $_POST['optionInputForm'];
            $this->idDataInputForm = $_POST['idDataInputForm'];

            if ($this->isActionFromAjax == "true") {
                if ($this->optionInputForm == 3) {
                    $this->SetDataInControls();
                }else if($this->optionInputForm == 5){
                $this->GetLengthImpost();
                }else {
                    $this->GetDataInJsonForTbl();
                }
            }
        }
    }

    private function SetDataInControls(){
        $SetInputsDataFJQ = "";
        $DiagnosisVo = new DiagnosisVo();
        $DiagnosisVo->id = $_POST["id"];
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $DiagnosisVo);
        $photos = $this->GetPhotosVo($DiagnosisVo->id);
        if($DiagnosisVo2 = $generalDao->GetVo($generalVo = clone $DiagnosisVo)){
            $SetInputsDataFJQ = "
                            <script>
                            $('#send').text('actualizar');
                            $('#idDataInputForm').val('{$DiagnosisVo2->id}');
                            $('#optionInputForm').val('4');
                            $('#checkStatusMachine').val('{$DiagnosisVo2->idStatusMachine}');
                            if('{$DiagnosisVo2->firmClient}' != ''){
                                $('#firmClienteDiv').removeAttr('class');
                                $('#firma1').val('{$DiagnosisVo2->firmClient}');
                                $('#firmClienteImg').attr('src', '{$DiagnosisVo2->firmClient}');
                            }
                            if('{$DiagnosisVo2->firmMaintenance}' != ''){
                                $('#firmMainDiv').removeAttr('class');
                                $('#firma2').val('{$DiagnosisVo2->firmClient}');
                                $('#firmMainImg').attr('src', '{$DiagnosisVo2->firmMaintenance}');
                            }

                            if('{$photos->photo1}' <  3000){
                                $('#photo1').attr('src'  , '{$photos->photo1}');
                            }else{
                                $('[name=photo1temp]').val('{$photos->photo1}')
                                $('#photo1').attr('src'  , '{$photos->photo1}');
                            }
                            if('{$photos->photo2}' <  3000){
                                $('#photo2').attr('src'  , '{$photos->photo2}');
                            }else{
                                $('[name=photo2temp]').val('{$photos->photo2}')
                                $('#photo2').attr('src'  , '{$photos->photo2}');
                            } 
                            if('{$photos->photo3}' <  3000){
                                $('#photo3').attr('src'  , '{$photos->photo3}');
                            }else{
                                $('[name=photo3temp]').val('{$photos->photo3}')
                                $('#photo3').attr('src'  , '{$photos->photo3}');
                            }
                            $('#FirmClientCtx').remove();
                            $('#FirmUserCtx').remove();
                            $('#firmaG1').remove();
                            $('#firmaB1').remove();
                            $('#firmaG2').remove();
                            $('#firmaB2').remove();
                            
                               (function($){
                                $('[name=CommentClient]').val('{$DiagnosisVo2->commentClient}');
                                $('[name=MachineState]').val('{$DiagnosisVo2->machineCommentState}');
                                $('[name=observationMachine]').val('{$DiagnosisVo2->machineObservation}');
                                $('#FirmClientCtx').val('{$DiagnosisVo2->firmClient}');
                                $('#FirmUserCtx').val('{$DiagnosisVo2->firmMaintenance}');

                                                                   
                               })(jQuery);
                            </script>
                        ";
        }
        echo $SetInputsDataFJQ;
    }

    public function GetDataInJsonForTbl()
    {
        $existData = false;
        $impostVo = new ImpostVo();
        $impostVo->isList = true;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $impostVo);

        $string = "
        {\"data\": [";
        while ($impostVo2 = $generalDao->GetVo($generalVo = clone $impostVo)) {
            $existData = true;
            $json = array();
           // $json[] = $impostVo2->id;
            $json[] = htmlspecialchars(ucfirst(strtolower($impostVo2->name)));
            $json[] = $impostVo2->percent;
            if(ucfirst(strtolower($impostVo2->name)) != "Iva"){
                $json[] = "<tip title='Modificar' class='yellow-tooltip' data-toggle='tooltip' data-placement='top'><button id='updateBtn' name='updateBtn' class='btn btn-warning btn-xs' data-toggle='modal' data-target='#impostPopUpAdd' title='Modificar' onclick='UpdateData({$impostVo2->id},\"impost\",\"impost\");'><i class='fa fa-pencil'></i></button> </tip>" .
                " <tip title='Eliminar' class='red-tooltip' data-toggle='tooltip' data-placement='top'><a id='deleteBtn' name='deleteBtn' class='btn btn-danger  btn-xs' title='Eliminar' onclick='DeleteData({$impostVo2->id},\"impost\")'><i class='fa fa-times'></i></a></tip> ";
            }else{
                 $json[] = "<tip title='Modificar' class='yellow-tooltip' data-toggle='tooltip' data-placement='top'><button id='updateBtn' name='updateBtn' class='btn btn-warning btn-xs' data-toggle='modal' data-target='#impostPopUpAdd' title='Modificar' onclick='UpdateData({$impostVo2->id},\"impost\",\"impost\");'><i class='fa fa-pencil'></i></button> </tip>";
            }

            $string .= json_encode($json) . ",";

        }
        $string .= "-]}-";

        if($existData){
            echo  str_replace(",-]}-", "]}", $string);
        }else{
            echo "{ \"data\": [] }";
        }
    }

    public function GetPhotosVo($id){
        $arrayList = array();
        $PhotosDiagnosisVo = new PhotosDiagnosisVo();
        $PhotosDiagnosisVo->idSearchField = 4;
        $PhotosDiagnosisVo->idDiagnosis = $id;
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $PhotosDiagnosisVo);
        if($PhotosDiagnosisVo2 = $generalDao->GetVo($generalVo = clone $PhotosDiagnosisVo)){
            return $PhotosDiagnosisVo2;
        }
        
    }

    
}

$ajaxDataImpost = new AjaxDataImpost();
