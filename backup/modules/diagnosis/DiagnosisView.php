<?php
require_once(Config::PATH . Config::CONTROLLER . 'general/AccessManager.php');
require_once(Config::PATH . Config::CONTROLLER . 'general/UtilJquery.php');
require_once(Config::PATH . Config::VIEW_HTML . 'general/comps/GeneralCtx.php');
require_once(Config::PATH . Config::VIEW_HTML . 'general/comps/GeneralLst.php');
require_once(Config::PATH . Config::VIEW_HTML . 'general/comps/GeneralWithDataLst.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralDao.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/ProductVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/QuotationVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/CompanyVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/DiagnosisVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/PhotosDiagnosisVo.php');


class DiagnosisView
{
    private $optionInputForm;
    private $isUpdateUserForm;
    private $idDataInputForm;
    /*=============================================
                  Session Vars
    =============================================*/
  
    /*=============================================
                  Template Vars
    =============================================*/
    private $utilJQ;
    private $pageTpl, $data, $core;
    /*=============================================
                Controls Vars HTML
    =============================================*/
    private $FirmClientCtx;
    private $FirmUserCtx;
    private $DiagnosisSetData = false;

    /*=============================================
                Data Vars
    =============================================*/
    private $generalDao;


    function __construct($pageTpl, $data, $core)
    {
        //phpInfo();
        $this->data = $data;
        $this->core = $core;
        $this->pageTpl = $pageTpl;
            $this->SetDataDiagnosis();
            $this->CreateComponents();
            $this->UseDataFromThePostback();
    }

    private function UseDataFromThePostback()
    {
        if (isset($_POST['optionInputForm'])) {
            $this->optionInputForm = $_POST['optionInputForm'];
            $this->idDataInputForm = $_POST['idDataInputForm'];
            $this->generalDao = new GeneralDao();
            switch ($this->optionInputForm) {
                case "1": {
                $this->SetData();
                    break;
                }
                case "2": {
                    $this->DeleteData();
                    break;
                }
                case "4": {
                    $this->SetUpdate();
                    break;
                }
            }
        }

        $this->AssignDataToTpl();

    }

    private function SetDataDiagnosis(){
        if(isset($_GET["diagnosis"])){
            $DiagnosisSetData = true;
        }
    }

    private function CreateComponents()
    {
        $this->FirmClientCtx = new GeneralCtx("FirmClientCtx","Firma Cliente",null,false);
        $this->FirmUserCtx = new GeneralCtx("FirmUserCtx","Firma Usuario",null,false);
       
        $this->utilJQ = new UtilJquery("Impost");
    }
    private function SetDataToVo()
    {
        $DiagnosisVo = new DiagnosisVo();
        $DiagnosisVo->id = $this->idDataInputForm;
        $DiagnosisVo->commentClient = $_POST["CommentClient"];
        $DiagnosisVo->machineCommentState = $_POST["MachineState"];
        $DiagnosisVo->machineObservation = $_POST["observationMachine"];
        $DiagnosisVo->firmClient = $_POST["firma1"];
        $DiagnosisVo->firmMaintenance = $_POST["firma2"];
        $DiagnosisVo->idStatusMachine = $_POST["checkStatusMachine"];
        $DiagnosisVo->idProduct = $_GET["idProduct"];
        $DiagnosisVo->dateCreated = date("Y-m-d H:i:s");
       
        return $DiagnosisVo;
    }


    private function SetDataPhotosToVo($result , $id){
        $PhotosDiagnosisVo = new PhotosDiagnosisVo();
        $PhotosDiagnosisVo->id = $id;
        if($_POST["photo1temp"] == "null"){
            //print_r("inserta");
            $PhotosDiagnosisVo->photo1 = $this->SavePhoto(1 ,$result);
        }else{
           // print_r($_POST);
            $PhotosDiagnosisVo->photo1 = $_POST["photo1temp"];
        }
        if($_POST["photo2temp"] == "null" || $_POST["photo2temp"] == "view/img/upload.jpg" && isset($_FILES["photo2"]) ){
            $PhotosDiagnosisVo->photo2 = $this->SavePhoto(2 ,$result);
        }else{
            $PhotosDiagnosisVo->photo2 = $_POST["photo2temp"];
        }
        if($_POST["photo3temp"] == "null" || $_POST["photo3temp"] == "view/img/upload.jpg" && isset($_FILES["photo3"]) ){
            $PhotosDiagnosisVo->photo3 = $this->SavePhoto(3 ,$result);
        }else{
            $PhotosDiagnosisVo->photo3 = $_POST["photo3temp"];
        }
        $PhotosDiagnosisVo->idDiagnosis = $result;
       
        return $PhotosDiagnosisVo;
    }

    private function SavePhoto($number,$id){
        $directory = "view/img/upload.jpg";
        if(isset($_FILES["photo".$number]["tmp_name"]) && !empty($_FILES["photo".$number]["tmp_name"])) {
            $file = $_FILES["photo".$number]["tmp_name"];
            list($width, $height) = getimagesize($file);
            $newWidth = 500;
            $newHeight = 500;
            if($_FILES["photo".$number]["type"] == "image/jpeg"){
                $numRandom = rand(1000,5000);
                $directory = $this->GenerateFolderImg($number,$id);
                $directory .= "/photo".$number."_".$id.".jpg";
                $origin = imagecreatefromjpeg($file);
                $destination = imagecreatetruecolor($newWidth,$newHeight);
                imagecopyresized($destination,$origin,0,0,0,0,$newWidth,$newHeight,$width,$height);
                imagejpeg($destination,$directory);
            }
        }
        return $directory;
    }

    private function SaveFirma($number,$id){
        $directory = "view/img/upload.jpg";
        if(isset($_FILES["firma".$number]["tmp_name"]) && !empty($_FILES["firma".$number]["tmp_name"])) {
            $file = $_FILES["firma".$number]["tmp_name"];
            list($width, $height) = getimagesize($file);
            $newWidth = 500;
            $newHeight = 500;
            if($_FILES["photo".$number]["type"] == "image/jpeg"){
                $numRandom = rand(1000,5000);
                $directory = $this->GenerateFolderImg($number,$id);
                $directory .= "/photo".$number."_".$id.".jpg";
                $origin = imagecreatefromjpeg($file);
                $destination = imagecreatetruecolor($newWidth,$newHeight);
                imagecopyresized($destination,$origin,0,0,0,0,$newWidth,$newHeight,$width,$height);
                imagejpeg($destination,$directory);
            }
        }
        return $directory;
    }

    private function GenerateFolderImg($number,$id){
        $directory = "view/img/diagnosis/photos_".$number."_".$id;
        $this->DeletePhotoUser($directory);
        if (!file_exists($directory)) {
            mkdir($directory,0755);
        }
        return $directory;
    }

    private function DeletePhotoUser($directory){
        $files = glob($directory.'/*'); // get all file names
        foreach($files as $file){ // iterate files
            if(is_file($file))
                unlink($file); // delete file
        }
    }

    private function SetData()
    {
        $DiagnosisVo = $this->SetDataToVo();
        $DiagnosisVo->id = null;
        $result = $this->generalDao->Set($generalVo = clone $DiagnosisVo);
        if (isset($result)) {
           $this->SetPhotosDiagnosis($result);
           $message = 'MessageSwalRedirect("Diagnostico Generado!","Se ha  Generado el diagnostico Correctamente","success","ListDiagnosis");';
           $this->utilJQ->AddFunctionJavaScript($message);
        }

    }

    public function SetPhotosDiagnosis($id){
        $EvaluationDiagnosisVo = $this->SetDataPhotosToVo($id , null);
        $EvaluationDiagnosisVo->id = null;
        $result = $this->generalDao->Set($generalVo = clone $EvaluationDiagnosisVo);
    }


    private function SetUpdate()
    {
        $DiagnosisVo = $this->SetDataToVo();
        $result = $this->generalDao->Update($generalVo = clone $DiagnosisVo);
        if (isset($result)) {
           $this->UpdatePhotosDiagnosis($result);
           $message = 'MessageSwalRedirect("Diagnostico Actualizado!","Se ha  actualizado el diagnostico Correctamente","success","ListDiagnosis");';
           $this->utilJQ->AddFunctionJavaScript($message);
        }
    }


    private function UpdatePhotosDiagnosis(){
        $EvaluationDiagnosisVo = $this->SetDataPhotosToVo($this->idDataInputForm , $this->idDataInputForm);
        $result = $this->generalDao->Update($generalVo = clone $EvaluationDiagnosisVo);
    }


    public function GetProductData(){
        $ProductVo = new ProductVo();
        $ProductVo->id = $_GET["idProduct"];
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $ProductVo);
        if ($ProductVo2 = $generalDao->GetVo($generalVo = clone $ProductVo)) {
            return $ProductVo2;
        }
    }

    public function GetCompanyData(){
        $CompanyVo = new CompanyVo();
        $CompanyVo->id = $this->GetQuotation($this->GetProductData()->idQuotation)->idCompany;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $CompanyVo);
        if ($CompanyVo2 = $generalDao->GetVo($generalVo = clone $CompanyVo)) {
            return $CompanyVo2;
        }
    }

    public function GetQuotation($id){
        $QuotationVo = new QuotationVo();
        $QuotationVo->id = $id;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $QuotationVo);
        if ($QuotationVo2 = $generalDao->GetVo($generalVo = clone $QuotationVo)) {
            return $QuotationVo2;
        }
        
    }

    private function AssignDataToTpl()
    {
        $this->data->assign('FirmClientCtx', $this->FirmClientCtx->paint());
        $this->data->assign('FirmUserCtx', $this->FirmUserCtx->paint());
        $this->data->assign('productData', $this->GetProductData());
        $this->data->assign('companyData', $this->GetCompanyData());
       
        $this->data->assign('diagnosisId', (isset($_GET["diagnosis"])) ? $_GET["diagnosis"] : 0);
        $this->data->assign('tempParameter', time());
        $this->data->assign('jquery', $this->utilJQ->PaintJQ());
        $this->SetDataToTpl();
    }   
    private function SetDataToTpl()
    {
        echo $this->core->get($this->pageTpl, $this->data);
    }
  


}   