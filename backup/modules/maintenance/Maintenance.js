// jshint unused:true
"use strict";

function ContactsData(idQuote) {
    var info;

    var parameters = {
        "isActionFromAjax": true,
        "optionInputForm": 7,
        "idDataInputForm": 0,
        "idQuotation": idQuote
    };
    var elementHtml = "<div class='table-responsive' ><table class=\"table text-center table-striped \" style=\"width:100%\"><strong><thead class=\" text-center font-weight-bold \"><tr><td>Nombres</td><td>Apellidos</td><td>Correo</td><td>Telelfono</td><td>Celular</td></tr></thead></strong>";
    $.ajax({
        url: "view/html/modules/maintenance/AjaxDataMaintenance.php",
        type: 'post',
        data: parameters,
        success: function(data) {
            info = $.parseJSON(data);
            for (var i = 0; i < info.length; i++) {
                //console.log(info[i]);
                elementHtml += "<td>" + info[i].names + "</td>";
                elementHtml += "<td>" + info[i].lastNames + "</td>";
                elementHtml += "<td>" + info[i].email + "</td>";
                elementHtml += "<td>" + info[i].telephone + "</td>";
                elementHtml += "<td>" + info[i].cellPhone + "</td>";
            }
            elementHtml += "</table></div>";
            $("#ContactTab ").html(elementHtml);
        }
    })
}

function LoadTable() {
    RemoveActionModule(27);
    GetArrayFromDataModule(27);
    setTimeout(function() {
        var parameters = {
            "isActionFromAjax": true,
            "optionInputForm": 0,
            "idDataInputForm": 0,
            "listData": arrayResponse,
        };


        $('#impostTbl').DataTable({
            responsive: true,
            "drawCallback": function(settings) {
                $('[data-toggle="tooltip"]').tooltip()
            },
            language: {
                "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
            },
            "ajax": {
                url: "view/html/modules/maintenance/AjaxDataMaintenance.php",
                method: "POST",
                data: parameters
            }
        });
    }, 500);
}

/*********************************CLICK EVENTS**************************/

$(function() {
    LoadTable();

});