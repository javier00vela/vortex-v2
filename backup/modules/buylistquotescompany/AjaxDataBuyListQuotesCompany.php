<?php
require_once(__DIR__ . '/../../../../controller/general/Config.php');
require_once(Config::PATH . Config::CONTROLLER.Config::GENERAL . 'ManageArrays.php');
require_once(Config::PATH . Config::BACKEND . 'modules/QuotationVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/ProductVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/CompanyVo.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralDao.php');
require_once(Config::PATH . Config::BACKEND . 'modules/OrderBuyProviderVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/OrderBuyClientVo.php');


class AjaxDataImpost{

    private $isActionFromAjax;
    private $optionInputForm;
    private $idDataInputForm;
    public $dataList = [];

    function __construct()
    {
        if (isset($_POST['isActionFromAjax'])) {
            $this->isActionFromAjax = $_POST['isActionFromAjax'];
            $this->optionInputForm = $_POST['optionInputForm'];
            $this->idDataInputForm = $_POST['idDataInputForm'];
            if(isset($_POST["listData"])){
                $this->dataList = $_POST["listData"];  
            }
            if ($this->isActionFromAjax == "true") {
                if ($this->optionInputForm == 3) {
                    $this->SetDataInControls();
                }else if($this->optionInputForm == 5){
                $this->GetLengthImpost();
                }else if($this->optionInputForm == 6){
                $this->UpdateOrdenCharacters();
                }else {
                    $this->GetDataInJsonForTbl();
                }
            }
        }
    }

    private function SetDataInControls(){
        $SetInputsDataFJQ = "";
        $OrderBuyClientVo = new OrderBuyProviderVo();
        $OrderBuyClientVo->id = $this->idDataInputForm;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $OrderBuyClientVo);
        if($OrderBuyClientVo2 = $generalDao->GetVo($generalVo = clone $OrderBuyClientVo)){
            $SetInputsDataFJQ = "
                            <script>
                               (function($){
                                SetValueToInputText('description','{$OrderBuyClientVo2->description}');
                                SetValueToInputText('optionInputForm',9);
                                SetValueToInputText('idDataInputForm',$OrderBuyClientVo2->id);
                               })(jQuery);
                            </script>
                        ";
        }
        echo $SetInputsDataFJQ;
    }

    public function GetDataInJsonForTbl()
    {
        $existData = false;
        $arrayProvider = [];
        $ProductVo = new ProductVo();
        $ProductVo->idQuotation = $_POST["idQuote"];
        $ProductVo->idSearchField = 16;
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $ProductVo);
        $string = "
        {\"data\": [";
        while ($ProductVo2 = $generalDao->GetVo($generalVo = clone $ProductVo)) {
            $provider = $this->GetCompanyToTable($ProductVo2);
            $providerOrder = $this->ListProviderBuy($provider->id);
            if(!in_array($provider->id , $arrayProvider)){
                $existData = true;
                $description = "description";
                $json = array();
                $json[] = $provider->id;
                $json[] = "#".htmlspecialchars(ucfirst(strtolower($provider->name)));
                $json[] = $provider->phone;
                $json[] = $provider->email;
                $json[] = "#".$_POST["idQuote"];
                $json[] = "<input id=\"percent-{$providerOrder->id}\" type=\"number\" placeholder=\"Agregar porcentaje\" class=\"form-control\" onchange=\"ChangePercent({$providerOrder->id} , 'percent')\" value=\"{$providerOrder->percent}\">";
                $json[] = "<select class=\"form-control\" id=\"pay-{$providerOrder->id}\"  onchange=\"ChangePercent({$providerOrder->id} , 'pay')\">
                    <option value=\"Credito\" ".(($providerOrder->pay == 'Credito') ? "selected='true'" : "'false'"   ).">Credito</option>
                    <option value=\"Debito\" ".(($providerOrder->pay == 'Debito') ? "selected='true'" : "'false'" ).">Debito</option>
                    <option value=\"Anticipado\" ".(($providerOrder->pay == 'Anticipado') ? "selected='true'" : "'false'"  ).">Anticipado</option>
                </select>";

                $json[] = "<button class=\"btn btn-block btn-primary\" data-toggle='modal' data-target='#descriptionPopUpAdd' onclick='UpdateData({$providerOrder->id},\"buylistquotescompany\",\"BuyListQuotesCompany\");SetValueToInputText(\"optionInputForm\",9);' >Descripcion</button>";

                $button = "";
                if(!in_array( "imprimir_orden_compra" , $this->dataList)){
                    $button .= "<tip title='Generar orden compra' class='blue-tooltip' data-toggle='tooltip' data-placement='top'><button id='Update' onClick='PrintData({$_POST["idQuote"]},\"{$provider->name}\" , \"{$providerOrder->id}\")' name='OrdenPrintBtn' class='btn btn-primary btn-xs'><i class='fa fa-copy'></i></button> </tip>";
                 }
                    $json[] = $button;
    
                $string .= json_encode($json) . ",";
                $arrayProvider[] = $provider->id ;
            }
           
        }
        $string .= "-]}-";

        if($existData){
            echo  str_replace(",-]}-", "]}", $string);
        }else{
            echo "{ \"data\": [] }";
        }
    }

    public function ListProviderBuy($idProvider){
        $OrderBuyProviderVo = new OrderBuyProviderVo();
        $OrderBuyProviderVo->idProvider = $idProvider;
        $OrderBuyProviderVo->idSearchField = 4;
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $OrderBuyProviderVo);
        if($OrderBuyProviderVo2 = $generalDao->GetVo($generalVo = clone $OrderBuyProviderVo)) {            
            return $OrderBuyProviderVo2;
        } 
    }

    public function GetCompanyToTable($product){
        $arrayList = array();
        $CompanyVo = new CompanyVo();
        $CompanyVo->name = $product->nameCompany;
        $CompanyVo->idSearchField = 1;
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $CompanyVo);
        if($CompanyVo2 = $generalDao->GetVo($generalVo = clone $CompanyVo)) {            
            return $CompanyVo2;
        }  
    }

    public function UpdateOrdenCharacters(){
        $OrderBuyProviderVo = new OrderBuyProviderVo();
        $generalDao = new GeneralDao();
        $OrderBuyProviderVo->id = $_POST["id"];
        $OrderBuyProviderVo->idSearchField = 0;
        if($_POST["type"] ==  "percent"){
            $OrderBuyProviderVo->idUpdateField = 2;
            $OrderBuyProviderVo->percent = $_POST["percent"];
        }else if($_POST["type"] ==  "pay"){
            $OrderBuyProviderVo->idUpdateField = 3;
            $OrderBuyProviderVo->pay = $_POST["pay"];
        }else{
            $OrderBuyProviderVo->idUpdateField = 1;
            $OrderBuyProviderVo->percent = $_POST["description"];
        }
        $generalDao->UpdateByField($generalVo = clone $OrderBuyProviderVo);
    }


    private function GetSizeProductsByQuote($id){
        $arrayList = array();
        $ProductVo = new ProductVo();
        $ProductVo->idQuotation = $id;
        $ProductVo->idSearchField = 16;
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $ProductVo);
        while($ProductVo2 = $generalDao->GetVo($generalVo = clone $ProductVo)) {            
            $arrayList[] =  $ProductVo2->id;
        }  
        return count($arrayList); 
    }

    
}

$ajaxDataImpost = new AjaxDataImpost();
