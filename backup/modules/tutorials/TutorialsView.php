<?php

require_once(Config::PATH . Config::CONTROLLER . 'general/ManageSession.php');
require_once(Config::PATH . Config::CONTROLLER . 'general/AccessManager.php');
require_once(Config::PATH . Config::CONTROLLER . 'general/UtilJquery.php');
class TutorialsView
{
    private $optionInputForm;
    private $isUpdateUserForm;
    private $idDataInputForm;


    /*=============================================
                Data Vars
    =============================================*/
    private $generalDao;


    function __construct($pageTpl, $data, $core)
    {
        $this->data = $data;
        $this->core = $core;
        $this->pageTpl = $pageTpl;
        $this->manageSession = new ManageSession();
        if(!$this->manageSession->IsSessionTimeEnd()){
            $this->CreateComponents();
            $this->UseDataFromThePostback();
        }
    }

    private function UseDataFromThePostback()
    {
        if (isset($_POST['optionInputForm'])) {
            $this->optionInputForm = $_POST['optionInputForm'];
            $this->idDataInputForm = $_POST['idDataInputForm'];
            $this->generalDao = new GeneralDao();
            switch ($this->optionInputForm) {

            }
        }

        $this->AssignDataToTpl();

    }

    private function CreateComponents()
    {
        $this->utilJQ = new UtilJquery("Impost");
    }
 
    private function AssignDataToTpl()
    {
        $this->SetDataToTpl();
    }   

        private function SetDataToTpl()
    {
        echo $this->core->get($this->pageTpl, $this->data);
    }

 
  


}   