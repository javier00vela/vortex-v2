<?php
require_once(__DIR__ . '/../../../../controller/general/Config.php');
require_once(Config::PATH . Config::CONTROLLER.Config::GENERAL . 'ManageArrays.php');
require_once(Config::PATH . Config::BACKEND . 'modules/QuotationVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/PersonVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/UserVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/CompanyVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/ProductVo.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralDao.php');
require_once(Config::PATH . Config::BACKEND . 'modules/HistoryModificationVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/UserQuotationVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/OrderBuyClientVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/OrderBuyProviderVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/PersonCreateQuotationVo.php');
require_once(Config::PATH . Config::CONTROLLER . 'general/ManageSession.php');


class AjaxDataGenerateQuotes{

    private $isActionFromAjax;
    private $optionInputForm;
    private $idDataInputForm;
    public $dataList = [];

    function __construct()
    {
        if (isset($_POST['isActionFromAjax'])) {
            $this->isActionFromAjax = $_POST['isActionFromAjax'];
            $this->optionInputForm = $_POST['optionInputForm'];
            $this->idDataInputForm = $_POST['idDataInputForm'];
            if(isset($_POST["listData"])){
                $this->dataList = $_POST["listData"];  
            }
            if ($this->isActionFromAjax == "true") {
                if ($this->optionInputForm == 3) {
                    $this->SetDataInControls();
                }elseif($this->optionInputForm == 4){
                    $DataModification = $_POST["DataModification"];
                    $this->UpdateState($DataModification);
                    $this->SetDataModification($DataModification);
                    
                }else if($this->optionInputForm == 5){
                    $this->GetQuotatesLength();
                }else if($this->optionInputForm == 6){
                    $this->GetProductsQuotation($this->idDataInputForm);
                }else if($this->optionInputForm == 7){
                    $DataModification = $_POST["DataModification"];
                    if(empty($_POST["dataProductsQuotationNoChecked"])){
                        $_POST["dataProductsQuotationNoChecked"] = [];
                    }
                    $this->UpdateProductsQuotation($_POST["dataProductsQuotationNoChecked"]);
                    $this->UpdateQuotation($_POST["idQuotation"]);
                    $this->UpdateState($DataModification);
                    $this->SetDataModification($DataModification);
                    $this->CreatedBuyQuotesClient($_POST["idQuotation"]);
                    $this->CreatedBuyQuotesProviders($_POST["idQuotation"]);
                }else if($this->optionInputForm == 8){
                    $this->UpdateStateQuotation();
                    $this->SetDataModificationQuotation("Modificado atravez de cambio de fecha");
                    $this->UpdateByDate();
                 }else if($this->optionInputForm == 10){
                     $this->UpdateStateQuotation();
                    $this->SetDataModificationQuotation("Modificado atravez de cambio de Personas");
                     $this->UpdateByPerson();
                }else if($this->optionInputForm == 12){
                     $this->UpdateStateQuotation();
                    $this->SetDataModificationQuotation("Modificado atravez de cambio de estado de cotización");
                    $this->UpdateStateTypeQuotate();
                }else if($this->optionInputForm == 13){
                    $this->GetPersonsQuotation();
                }else {
                    $this->GetDataInJsonForTbl();
                }
            }
        }
    }

    private function CreatedBuyQuotesProviders($id){
        $arrayProvider = [];
        $ProductVo = new ProductVo();
        $ProductVo->idQuotation = $id;
        $ProductVo->idSearchField = 16;
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $ProductVo);
        while ($ProductVo2 = $generalDao->GetVo($generalVo = clone $ProductVo)) {
            $provider = $this->GetCompanyToTable($ProductVo2);
            if(!in_array($provider->id , $arrayProvider)){
                $OrderBuyClientVo = $this->SetDataToVoBuyProvider($id , $provider->id );
                $generalDao2 = new GeneralDao();
                $result = $generalDao2->Set($generalVo = clone $OrderBuyClientVo);
            }
        }
    }

    public function GetCompanyToTable($product){
        $arrayList = array();
        $CompanyVo = new CompanyVo();
        $CompanyVo->name = $product->nameCompany;
        $CompanyVo->idSearchField = 1;
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $CompanyVo);
        if($CompanyVo2 = $generalDao->GetVo($generalVo = clone $CompanyVo)) {            
            return $CompanyVo2;
        }  
    }

    private function CreatedBuyQuotesClient($idQuote){
        $array = ["Orden","Factura"];
        foreach($array as $op ){
            $OrderBuyClientVo = $this->SetDataToVoBuyClient($idQuote , $op);
            $generalDao = new GeneralDao();
            $result = $generalDao->Set($generalVo = clone $OrderBuyClientVo);
        }
    }

    private function SetDataToVoBuyProvider($id , $empresa)
    {
        $OrderBuyProviderVo = new OrderBuyProviderVo();
        $OrderBuyProviderVo->description = "# {$id}";
        $OrderBuyProviderVo->percent = 0;
        $OrderBuyProviderVo->pay = "Credito";
        $OrderBuyProviderVo->idProvider = $empresa;
        $OrderBuyProviderVo->idQuotation = $id;
        $OrderBuyProviderVo->checked = 0;
       
        return $OrderBuyProviderVo;
    }

    private function SetDataToVoBuyClient($id , $op)
    {
        $OrderBuyClientVo = new OrderBuyClientVo();
        $OrderBuyClientVo->description = "Orden de Compra";
        $OrderBuyClientVo->percent = 0;
        $OrderBuyClientVo->pay = "Credito";
        $OrderBuyClientVo->idClient = $this->GetQuotation($id)->idCompany;
        $OrderBuyClientVo->idQuotation = $id;
        $OrderBuyClientVo->type = $op;
       
        return $OrderBuyClientVo;
    }

    public function GetQuotation($id){
        $quotationVo = new QuotationVo();
        $quotationVo->id = $id;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $quotationVo);
        if($quotationVo2 = $generalDao->GetVo($generalVo = clone $quotationVo)) {            
            return $quotationVo2;
        }  
    }


        private function UpdateStateTypeQuotate(){
            $quotationVo = new QuotationVo();
            $generalDao = new GeneralDao();
            $quotationVo->id = $idQuotation;
            $quotationVo->idSearchField = 0;
            $quotationVo->idUpdateField = 11;
            $quotationVo->isList = false;
            //$quotationVo->total = 12;
            $quotationVo->typeQuotation = $_POST["data"];
            //print_r($quotationVo->total);
            $generalDao->UpdateByField($generalVo = clone $quotationVo);
        }

        private function UpdateByPerson(){
         $UserQuotationVo = new UserQuotationVo();
         $UserQuotationVo->idSearchField = 3; 
         $UserQuotationVo->idQuotation = $_POST["id"];
         $generalDao = new GeneralDao();
        $generalDao->DeleteByField($generalVo = clone $UserQuotationVo);

        $UserQuotationVo2 = new UserQuotationVo();
        for ($i=0; $i < count($_POST["data"]) ; $i++) { 
            $UserQuotationVo2->idPerson = $_POST["data"][$i]["idPerson"];
            $UserQuotationVo2->completeName = $_POST["data"][$i]["person"];
             $UserQuotationVo2->idQuotation = $_POST["id"];
             $data = $generalDao->set($generalVo = clone $UserQuotationVo2);
        }
        

        }

       private function GetPersonsQuotation(){
        $quotationList = array();
        $quotationVo = new UserQuotationVo();
        $quotationVo->idSearchField = 3;
        $quotationVo->idQuotation = $_POST["idDataInputForm"];
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $quotationVo);
        while ($quotationVo2 = $generalDao->GetVo($generalVo = clone $quotationVo)) {
            $quotationList[] = $quotationVo2 ;
        }
        print_r( json_encode($quotationList) );
    }

    private function SetDataInControls(){
        $SetInputsDataFJQ = "";
        $quotationVo = new QuptationVo();
        $quotationVo->id = $this->idDataInputForm;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $impostVo);
        if($quotationVo2 = $generalDao->GetVo($generalVo = clone $quotationVo)){
            $SetInputsDataFJQ = "
                            <script>
                               (function($){
                                SetValueToInputText('quotationCodeCtx','{$quotationVo2->code}');
                                SetValueToInputText('quotationGenerationDateCtx','{$quotationVo2->generationDate}');
                                SetValieToInputText('quotationStateCtx',''{$quotationVo2->state});
                                SetValieToInputText('quotationTotalCtx',''{$quotationVo2->Total});
                                SetValueToInputText('optionInputForm',4);
                                SetValueToInputText('idDataInputForm',$quotationVo2->id);
                               })(jQuery);
                            </script>
                        ";
        }
        echo $SetInputsDataFJQ;
    }


    private function GetQuotatesLength(){
               $arrayList = [];
        $quotationVo = new QuotationVo();
        $quotationVo->isList = true;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $quotationVo);
        print_r($generalDao->GetLength());
    } 

    public function GetDataInJsonForTbl()
    {
        $existData = false;
        $quotationVo = new QuotationVo();
        $quotationVo->isList = true;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $quotationVo);

        $string = "
        {\"data\": [";
        while ($quotationVo2 = $generalDao->GetVo($generalVo = clone $quotationVo)) {
            $existData = true;
            $json = array();
            $json[] = $quotationVo2->id;
            $json[] = htmlspecialchars($quotationVo2->code);
            $json[] = $quotationVo2->version;
            $json[] = ucwords(strtolower($this->GetUserQuotation($quotationVo2->id)));
            $json[] = $quotationVo2->generationDate;
            $options = $this->ValidateOptions($quotationVo2->state);
            if(  $quotationVo2->checkeable == 0 ){
                    if($quotationVo2->state == "Compra"){
                        $json[] = "<select name='state' id='state' disabled class='form-control'>".$options."</select>";     
                    }else{
                        $json[] = "<select name='state' id='state' class='form-control'>".$options."</select>";
                    }
                  }else{  $json[] = "<select  disabled class='form-control'> Sin Permisos</select>";  }     
            $json[] = $quotationVo2->typeQuotation;
            if($quotationVo2->typeMoney == "COP"){
                $json[] = "$". str_replace(",","." ,number_format($quotationVo2->total ));  
            }else{
                $json[] = "$". str_replace(".","," ,(substr($quotationVo2->total , 0 , -2 )).",". (substr($quotationVo2->total , -2  )) ); 
            }
            $json[] = $quotationVo2->typeMoney; 
            $json[] = $quotationVo2->offerValidity. " dias"; 
            $code = "";
            $update = "";
            $print = "";
            $history = "";
            $tracing= "";
        
                $code = "<tip title='Codigo' class='red-tooltip' data-toggle='tooltip' data-placement='top'><a id='UpdateBtn' name='CodigoBtn' class='btn btn-danger btn-xs' title='Agregar Codigo' onclick='ModalCode(".$quotationVo2->id.")'><i class='fa fa-code'></i></a></tip> ";
            
            if(!in_array( "editar_cotizacion" , $this->dataList)){
                $update = "<tip title='Actualizar' class='yellow-tooltip' data-toggle='tooltip' data-placement='top'><a id='UpdateBtn' name='printBtn' class='btn btn-warning btn-xs' title='actualizar' onclick='ModifyQuotation({$quotationVo2->id});$(\"#UpdateBtn\").attr(\"onclick\",\" \");'><i class='fa fa-edit'></i></a></tip> ";
            }
            if(!in_array( "ver_cotizacion" , $this->dataList)){
                $print = "<tip title='Imprimir' class='blue-tooltip' data-toggle='tooltip' data-placement='top'><a id='printBtn' name='printBtn' class='btn btn-primary btn-xs' title='' onclick='PrintData({$quotationVo2->id});'><i class='fa fa-file'></i></a></tip> ";
            }
            if(!in_array( "historico_cotizacion" , $this->dataList)){
                $history = "<tip title='Historial Cotizaciones' class='green-tooltip' data-toggle='tooltip' data-placement='top'><a id='idQuotation' name='historyModificationBtn' href='HistoryModifyQuotation?codeQuotationInputForm=".$quotationVo2->id."' class='btn btn-success btn-xs' ><i class='fa fa-history'></i></a></tip> ";
            }
            if(!in_array( "seguimiento_cotizacion" , $this->dataList)){
                $tracing = "<tip title='Seguimiento' class='purple-tooltip' data-toggle='tooltip' data-placement='top'><a id='idQuotation' name='tracingQuotationBtn' href='TracingQuotation?codeQuotationInputForm=".$quotationVo2->id."' class='btn bg-purple btn-xs' ><i class='fa fa-book'></i></a></tip> ";
            }
            if($quotationVo2->state == "Compra"){
                $json[] = $print.$history.$tracing;
            }else{
            $json[] = (  $quotationVo2->checkeable == 1 ) ? $code : $update.$print.$history.$tracing ;
            }
            $string .= json_encode($json) . ",";

        }
        $string .= "-]}-";

        if($existData){
            echo  str_replace(",-]}-", "]}", $string);
        }else{
            echo "{ \"data\": [] }";
        }
    }
      private function ValidateOptions($state){
        $options = "<option value=".$state.">".$state."</option>";
        $arrayStates = [];
        $statesDefinitive = [];
        array_push($arrayStates, "Cancelada","Enviada","Pendiente","Orden de Compra");
    
        for ($i=0; $i <count($arrayStates) ; $i++) { 
            if ($arrayStates[$i]!=$state) {
                array_push($statesDefinitive, $arrayStates[$i]);
            }
        }
        for ($q=0; $q <count($statesDefinitive) ; $q++) {
            $options .= "<option value=".$statesDefinitive[$q].">".$statesDefinitive[$q]."</option>";
        }
        return $options;
         

    }


    public function UpdateByDate()
    {
            $quotationVo = new QuotationVo();
            $quotationVo->id = $_POST["id"];
            $quotationVo->idSearchField = 0;
            $quotationVo->generationDate = $_POST["data"];
            $quotationVo->idUpdateField = 3;
            $generalDao = new GeneralDao();
            $generalDao->UpdateByField($generalVo = clone $quotationVo);



    }


    private function GetProductsQuotation($id){
        $productList = array();
        $productVo = new ProductVo();
        $productVo->idSearchField = 16;
        $productVo->idQuotation = $id;
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $productVo);
        while ($productVo2 = $generalDao->GetVo($generalVo = clone $productVo)) {
            $productVo2->description = null;
            $productList[] = $productVo2 ;
        }
        echo json_encode($productList);
    }
    private  function GetUserQuotation($idQuotation)
    {
        $nameUser="";
         $usersQuotationVo= new PersonCreateQuotationVo();
         $usersQuotationVo->idSearchField = 5;
         $usersQuotationVo->idQuotation = $idQuotation;
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $usersQuotationVo);
        while ($usersQuotationVo2 = $generalDao->GetVo($generalVo = clone $usersQuotationVo)) {
            $nameUser = $usersQuotationVo2->completeName;
        }
        return $nameUser;

    }

    private function UpdateStateQuotation()
    {

            $quotationVo = new QuotationVo();
            $quotationVo3 = new QuotationVo();
            $quotationVo->id =  $_POST["id"];
            $generalDao = new GeneralDao();
            $generalDao2 = new GeneralDao();
            $generalDao->GetById($generalVo = clone $quotationVo);
            while ($quotationVo2 = $generalDao->GetVo($generalVo = clone $quotationVo)) {
                $quotationVo3->id = $_POST["id"];
                $quotationVo3->idSearchField = 0;
                $quotationVo3->version = ($quotationVo2->version + 1);
                $quotationVo3->idUpdateField = 12;
                $generalDao2->UpdateByField($generalVo = clone $quotationVo3);
            }
    }

     private function UpdateState($dataModification)
    {

        $quotationVo = new QuotationVo();
        $generalDao = new GeneralDao();
        $quotationVo2 = $this->GetDataQuotationModification($dataModification[0]["idQuotation"]);
        $quotationVo->id = $quotationVo2->id;
        $quotationVo->idUser = $quotationVo2->idUser;
        $quotationVo->code = $quotationVo2->code;
        $quotationVo->state = $dataModification[0]["state"];
        $quotationVo->typeMoney = $quotationVo2->typeMoney;
        $quotationVo->generationDate = $quotationVo2->generationDate;
        $quotationVo->total = $quotationVo2->total;
        $quotationVo->idCompany = $quotationVo2->idCompany;
        $quotationVo->offerValidity = $quotationVo2->offerValidity;
        
        $quotationVo->placeDelivery = $quotationVo2->placeDelivery;
        $quotationVo->payment = $quotationVo2->payment;
        $quotationVo->version = ($quotationVo2->version+1);
        $quotationVo->typeQuotation = $quotationVo2->typeQuotation;
        $quotationVo->IVA = $quotationVo2->IVA;
        $quotationVo->reminder = $quotationVo2->reminder;
        $quotationVo->isDDP = $quotationVo2->isDDP;
        $quotationVo->checkeable = $quotationVo2->checkeable;
        $quotationVo->dateUpdate = $quotationVo2->dateUpdate;

        $result = $generalDao->Update($generalVo = clone $quotationVo);

    }

   private function GetDataCompany($idCompany){
        $companyVo = new CompanyVo();
        $generalDao = new GeneralDao();
        $companyVo->idSearchField = 0;
        $companyVo->id = $idCompany;
        $generalDao->GetById($generalVo = clone $companyVo);
        $companyVo2 = $generalDao->GetVo($generalVo = clone $companyVo);
        if($companyVo2->idRoleCompany == 4){//actualice la empresa si es prospecto
            $this->UpdateCompany($companyVo2);
            $this->UpdatePersonsCompany($companyVo2->id);
        }
   }

   private function UpdateCompany($companyVo2){
         $companyVo = new CompanyVo();
        $generalDao = new GeneralDao(); 
        $companyVo->id = $companyVo2->id;
        $companyVo->name = $companyVo2->name;
        $companyVo->nit = $companyVo2->nit;
        $companyVo->phone = $companyVo2->phone;
        $companyVo->email = $companyVo2->email;
        $companyVo->webSite = $companyVo2->webSite;
        $companyVo->address =  $companyVo2->address;
        $companyVo->country =  $companyVo2->country;
        $companyVo->city = $companyVo2->city;
        $companyVo->creationDate = $companyVo2->creationDate;
        $companyVo->idRoleCompany = 2;
        $companyVo->state = $companyVo2->state;
        $generalDao->Update($generalVo = clone $companyVo);
   }
   private function UpdatePersonsCompany($idCompany){
        $arrayPersons = $this->GetDataPersonsCompany($idCompany);
        $this->UpdatePerson($arrayPersons);
        
   }
   private function UpdatePerson($personVo2){
    for ($i=0; $i <count($personVo2); $i++) {
        $personVo = new PersonVo();
        $generalDao = new GeneralDao();
        $personVo->id = $personVo2[$i]->id;
        $personVo->names = $personVo2[$i]->names;
        $personVo->lastNames = $personVo2[$i]->lastNames;
        $personVo->documentType = $personVo2[$i]->documentType;
        $personVo->document = $personVo2[$i]->document;
        $personVo->cellPhone = $personVo2[$i]->cellPhone;
        $personVo->telephone = $personVo2[$i]->telephone;
        $personVo->email = $personVo2[$i]->email;
        $personVo->address = $personVo2[$i]->address;
        $personVo->city = $personVo2[$i]->city;
        $personVo->birthday = $personVo2[$i]->birthday;
        $personVo->creationDate = $personVo2[$i]->creationDate;
        $personVo->idRole = 7;
        $personVo->idCompany = $personVo2[$i]->idCompany;
        $personVo->state = $personVo2[$i]->state;
        $generalDao->Update($generalVo = clone $personVo);
   }
}
   private function GetDataPersonsCompany($idCompany){
        $arrayPersons = [];
        $personVo = new PersonVo();
        $generalDao = new GeneralDao();
        $personVo->idSearchField = 12;
        $personVo->idCompany = $idCompany;
        $generalDao->GetByField($generalVo = clone $personVo);
       while( $personVo2 = $generalDao->GetVo($generalVo = clone $personVo)){
            $arrayPersons[] = $personVo2;
        }
        return $arrayPersons;
   }
    private function GetDataQuotationModification($idQuotation)
    {
        $quotationVo = new QuotationVo();
        $generalDao = new GeneralDao();
        $quotationVo->id=$idQuotation;
        $generalDao->GetById($generalVo = clone $quotationVo);
        $quotationVo2 = $generalDao->GetVo($generalVo = clone $quotationVo);
        return $quotationVo2;
    }

    private function SetDataModification($DataModification)
    {
         session_start();
         $userVo = unserialize($_SESSION['user']["user"]);
        $personVo = unserialize($_SESSION['user']["person"]);
        $historymodificationVo = new HistorymodificationVo();
        $generalDao = new GeneralDao();
        $historymodificationVo->idUser=$personVo->id;
        $date = new DateTime($DataModification[0]["Date"]);
        $historymodificationVo->modificationDate=$date->format('Y-m-d H:i:s');
        $historymodificationVo->idQuotation=$DataModification[0]["idQuotation"];
        $historymodificationVo->description="Ha cambiado el estado a ".$DataModification[0]["state"]."";    
        $generalDao->set($generalVo = clone $historymodificationVo);

    }

     private function SetDataModificationQuotation($DataModification)
    {
    session_start();

        $personVo = unserialize($_SESSION['user']["person"]);
   
        $date = new DateTime();

        $historymodificationVo = new HistorymodificationVo();
        $generalDao = new GeneralDao();
        $historymodificationVo->idUser=$personVo->id;
        $historymodificationVo->modificationDate=$date->format('Y-m-d H:i:s');
        $historymodificationVo->idQuotation= $_POST["id"] ;
        $historymodificationVo->description="Ha cambiado el estado a ".$DataModification."";    
        $data = $generalDao->set($generalVo = clone $historymodificationVo);
        print_r($data);

    }
    public function UpdateQuotation($idQuotation){
        $quotationVo = new QuotationVo();
        $generalDao = new GeneralDao();
        $quotationVo->id = $idQuotation;
        $quotationVo->idSearchField = 0;
        $quotationVo->idUpdateField = 5;
        $quotationVo->isList = false;
        //$quotationVo->total = 12;
        $quotationVo->total = $this->GetTotalQuotation($idQuotation);
        //print_r($quotationVo->total);
        $generalDao->UpdateByField($generalVo = clone $quotationVo);
    }
   
    public function GetTotalQuotation($idQuotation){
        $productVo = new ProductVo();
        $generalDao = new GeneralDao();
        $productVo->idSearchField=16;
        $productVo->idQuotation=$idQuotation;
        $total=0;
        $generalDao->GetByField($generalVo = clone $productVo);
        while( $productVo2 = $generalDao->GetVo($generalVo = clone $productVo)){
            $productVo2->priceUnit = (($productVo2->priceUnit*$productVo2->percent/100)+$productVo2->priceUnit);
            $totalProducto = ($productVo2->quantity * $productVo2->priceUnit);
            $total += $totalProducto;       
        }
        $total = ($total*0.19)+$total;
        return $total;
    }
    public function UpdateProductsQuotation($productsNoChecked){
        if(count($productsNoChecked)>0){
            for($i=0; $i<count($productsNoChecked); $i++){
                $productVo = new ProductVo();
                $generalDao = new GeneralDao();
                $productVo->id = $productsNoChecked[$i]["id-Product"];
                $generalDao->Delete($generalVo = clone $productVo);
               // $quotationVo->total = $this->GetTotalQuotation($idQuotation);
             //   $generalDao->UpdateByField($generalVo = clone $productVo);
                      
            }

         
     
        }
       
       
    }

    
}

$ajaxDataGenerateQuotes = new AjaxDataGenerateQuotes();