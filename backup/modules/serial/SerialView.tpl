<div class="content-wrapper">
    <section class="content-header">

        <h1>
            Administrar Serial por Producto
        </h1>
        <ol class="breadcrumb">
            <li><a href="Home"><i class="fa fa-dashboard"></i> Inicio</a></li>
            <li class="active">Inventario</li>
            <li class="active">Serial</li>
        </ol>
    </section>

    <section class="content">
        <div class="box">
            <div class="box-header with-border">
                <nav class="navbar navbar-light">
                    <div class="container-fluid">
                        <ul class="nav navbar-nav">
                            <li>
                                <a style="cursor: pointer;" node="node-agregar_serial_excel" id="serialesAddFromExcelBtn" name="productsAddFromExcelBtn" data-toggle="modal" data-target="#modalAddTemplateExcel">
                                    <i class="fa fa-upload"></i> Agregar Seriales desde plantilla de excel
                                </a>
                            </li>
                            <li>
                                <a style="cursor: pointer;" id="userAddBtn" name="userAddBtn" onclick="location.href = 'Inventory' ">
                                    <i class="fa fa-arrow-left"></i> Regresar
                                </a>
                            </li>
                            <li>
                                <a style="cursor: pointer;" id="userAddBtn" name="userAddBtn" onclick="location.href = 'Home' ">
                                    <i class="fa fa-home"></i> Menu Principal
                                </a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
            <div class="box-body">
                <input type="hidden" id="idProductHidden" value="{$idProducts}">
                <table id="serialTbl" name="serialTbl" class="table table-bordered table-striped dt-responsive tables dataTable no-footer dtr-inline" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>id</th>
                            <th>Tipo de Producto</th>
                            <th>Nombre</th>
                            <th>Serial</th>
                            <th>Marca</th>
                            <th>Referencia</th>
                            <th>Presentación</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </section>
</div>

<div id="modalAddTemplateExcel" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">

            <form id="excelTemplateForm" name="ExcelTemplateForm" role="form" method="post" enctype="multipart/form-data">
                <input id="optionInputTemplateForm" name="optionInputTemplateForm" type="hidden" value="0">
                <div class="modal-header" style="background: #3c8dbc; color: white">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Agregar Seriales</h4>
                </div>
                <div class="modal-body">
                    <div class="alert bg-orange">Recuerda agregar un archivo excel(xlsx) con una cantidad entre 1 a 15000 registros con el fin de garantizar un uso optimo y rapido del funcionamiento de la carga de seriales en el sistema.</div>
                    <input type="hidden" name="MAX_FILE_SIZE" value="2000000" />
                    <div class="form-group">
                        <label>Inserte una plantilla excel para cargar los datos :</label>
                        <input type="file" name="fileExcelTemplateInp" accept=".xlsx" class="file">
                        <div class="input-group col-12">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-folder-open"></i></span>
                            <input type="text" class="form-control input-lg" disabled placeholder="Sin Plantilla Excel">
                            <span class="input-group-btn">
                              <button class="browse btn btn-primary input-lg" type="button"><i class="fa fa-plus"></i></button>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button id="exitExcelTemplateBtn" type="button" class="btn btn-default pull-left" data-dismiss="modal" onclick="ResetControls('ExcelTemplateForm')">Exit</button>
                    <button id="sendExcelTemplateBtn" name="sendExcelTemplateBtn" type="button" class="importExcel btn btn-primary">Importar</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div id="containerResponseAjax"></div>

<script src="view/html/modules/serial/serial.js?v= {$tempParameter}"></script>
{$jquery}