<?php
require_once(Config::PATH . Config::CONTROLLER . 'general/AccessManager.php');
require_once(Config::PATH . Config::CONTROLLER . 'general/UtilJquery.php');
require_once(Config::PATH . Config::CONTROLLER . 'general/ManageSession.php');
require_once(Config::PATH . Config::VIEW_HTML . 'general/comps/GeneralCtx.php');
require_once(Config::PATH . Config::VIEW_HTML . 'general/comps/GeneralLst.php');
require_once(Config::PATH . Config::VIEW_HTML . 'general/comps/GeneralWithDataLst.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralDao.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/CompanyVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/SerialVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/ProductVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/InventoryVo.php');
require_once(Config::PATH . Config::CONTROLLER_LIB . 'vendor/phpoffice/phpexcel/Classes/PHPExcel/IOFactory.php');

class InventoryView
{
    private $optionInputForm;
    private $idDataInputForm;
    /*=============================================
                  Session Vars
    =============================================*/
    private $manageSession;
    /*=============================================
                  Template Vars
    =============================================*/
    private $utilJQ;
    private $pageTpl, $data, $core;
    /*=============================================
                Controls Vars HTML
    =============================================*/
    private $idProduct;
    private $numberRow = 1 ;


    /*=============================================
                Data Vars
    =============================================*/
    private $generalDao;


    function __construct($pageTpl, $data, $core)
    {
        $this->data = $data;
        $this->core = $core;
        $this->pageTpl = $pageTpl;
         $this->ValidateIfExistInventory($_GET["id"]);
            $this->CreateComponents();
            $this->UseDataFromThePostback();
        
    }

    private function UseDataFromThePostback()
    {
        if (isset($_POST['optionInputForm'])) {
            $this->optionInputForm = $_POST['optionInputForm'];
            $this->idDataInputForm = $_POST['idDataInputForm'];
            $this->generalDao = new GeneralDao();
            switch ($this->optionInputForm) {
                case "1": {
                    $this->SetData();
                    break;
                }
                case "2": {
                    $this->DeleteData();
                    break;
                }
                case "4": {
                    $this->SetUpdate();
                    break;
                }
            }
        }

        if (isset($_POST['optionInputTemplateForm'])) {
            $this->optionInputForm = $_POST['optionInputTemplateForm'];
            switch ($this->optionInputForm) {
                case "6": {
                    $this->GetFileToRead();
                    break;
                }
            }
        }
        
        $this->AssignDataToTpl();

    }

    private function CreateComponents()
    {

        $this->productNameCtx = new GeneralCtx("productNameCtx","Nombre",null,true);
        $this->idProduct = new GeneralCtx("idProductS","idProduct",null,true);
        $dataProductCountryLst = array();
        $this->productCountryLst = new GeneralWithDataLst($dataProductCountryLst, "productCountryLst", false,false,"--País de Origen--",true);

        $this->utilJQ = new UtilJquery("Users");
    }

    private function SetDataToVo()
    {
        $inventoryVo = new InventoryVo();
        $inventoryVo->id = $this->idDataInputForm;
        $inventoryVo->typeProduct = $_POST["productTypeProductLst"];
        $inventoryVo->name = $_POST["productNameCtx"];
        $inventoryVo->brand = $_POST["productBrandCtx"];
        $inventoryVo->reference = $_POST["productReferenceCtx"];
        $inventoryVo->unitMeasurement = $_POST["productUnitMeasurementCtx"];
        $inventoryVo->typeCurrency = $_POST["productTypeCurrencyLst"];
        $inventoryVo->amount = $_POST["productAmountCtx"];
        $inventoryVo->minimumAmount = $_POST["productMinimumAmountCtx"];
        $inventoryVo->priceUnit = $_POST["productPriceUnitCtx"];
        $inventoryVo->country = $_POST["productCountryLst"];
        $inventoryVo->idCompany = $_POST["productCompanyLst"];
        $inventoryVo->description = $_POST["productDescriptionAtx"];
        return $inventoryVo;
    }

    private function SetData()
    {
        $inventoryVo = $this->SetDataToVo();
        $inventoryVo->id = null;
        $result = $this->generalDao->Set($generalVo = clone $inventoryVo);
        if (isset($result)) {
            $message = 'MessageSwalBasic("Registrado!","Producto Registrado Correctamente","success");';
            $this->utilJQ->AddFunctionJavaScript($message);
        }
    }

    private function SetUpdate()
    {
        $result = $this->generalDao->Update($generalVo = clone $this->SetDataToVo());
        if (isset($result)) {
            $message = 'MessageSwalBasic("Modificado!","Producto Modificado Correctamente","success");';
            $this->utilJQ->AddFunctionJavaScript($message);
        }
    }

      private function ValidateIfExistInventory($id){
        if(empty($this->GetInventoryById($id)->id)){
            header("location: DontFind");
        }
    }

    private function DeleteData()
    {
        $message = "";
        $inventoryVo = new InventoryVo();
        $inventoryVo->id = $this->idDataInputForm;
        if ($this->generalDao->Delete($generalVo = clone $inventoryVo)) {
            $message = 'MessageSwalBasic("Eliminado!","Registro Eliminado Correctamente","success");';
        } else {
            $message = 'MessageSwalBasic("Error!","Problema con la base de datos","error");';
        }
        $this->utilJQ->AddFunctionJavaScript($message);
    }


    private function AssignDataToTpl()
    {
          $this->data->assign('idProducts', $_GET["id"]);
           $this->data->assign('tempParameter', time());
        $this->data->assign('jquery', $this->utilJQ->PaintJQ());
        $this->SetDataToTpl();
    }

    private function SetDataToTpl()
    {
        echo $this->core->get($this->pageTpl, $this->data);
    }

    /********************************************************/
    private function GetProviders(){
        $companiesArray = array();
        $companyVo = new CompanyVo();
        $companyVo->idSearchField = 10;
        $companyVo->idRoleCompany = 3;
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $companyVo);
        while ($companyVo2 = $generalDao->GetVo($generalVo = clone $companyVo)) {
            $companiesArray[] = $companyVo2->id."_".$companyVo2->name;
        }
        return $companiesArray;
    }

    /*******************************LOAD TEMPLATE EXCEL**********************************/

    public function GetFileToRead(){
        if(isset($_FILES["fileExcelTemplateInp"])){
            $fileExplode = explode(".", $_FILES["fileExcelTemplateInp"]["name"]);
            if($fileExplode[1] == "xlsx" ){
                $fileExcel = $_FILES["fileExcelTemplateInp"]["tmp_name"];
                $this->LoadExcelFile($fileExcel);
            }else{
                $message = '<script> MessageSwalBasic("ERROR!","Verifique que el formato que ha ingresado sea de tipo excel","error"); </script>';
                echo $message;
            }
        }
    }
    public function LoadExcelFile($fileExcel){
        if(isset($fileExcel)){
            $coreExcel = $this->LoadDataExcelFile($fileExcel);
            $numRows = $this->CountCellsExcel($coreExcel);
            $this->SetDataExcelToVo($coreExcel,$numRows);
        }
    }

    public function LoadDataExcelFile($file){
        $fileXlsx = PHPEXCEL_IOFactory::load($file);
        return $fileXlsx;
    }

    public function CountCellsExcel($file){
        $file->setActiveSheetIndex(0);
        $numRows = $file->setActiveSheetIndex(0)->getHighestRow();
        return $numRows;
    }


    private function SetDataExcelToVo($fileXlsx,$numRows)
    {
        for ($i=2; $i<$numRows+1; $i++) {
            $SerialVo = new SerialVo();
            $SerialVo->numberRow = $fileXlsx->getActiveSheet()->getCell('A'.$i)->getCalculatedValue();
            $SerialVo->numberRow = $this->ValidateIsInt($SerialVo->numberRow);
            $SerialVo->numberSerial = $fileXlsx->getActiveSheet()->getCell('B'.$i)->getCalculatedValue();
            $this->numberRow++;
            $this->InsertDataExcel($numRows,$i,$SerialVo);
   
        }
    }

    private function ValidateQuotesInString($value){
        $value = preg_replace('/"/',"\"",$value);
        $value = preg_replace("/'/","\'",$value);
        return $value;
    }



    private function ValidateIsInt($value){
        if(!is_numeric($value)){
            $value = $this->numberRow++;
        }
        return $value;
    }

    private function GetInventoryById($id){
        $inventoryVo = new inventoryVo();
        $generalDao = new GeneralDao();
        $inventoryVo->id = $id;
        $generalDao->GetById($generalVo = clone $inventoryVo);

        if($inventoryVo2 = $generalDao->GetVo($generalVo = clone $inventoryVo)){
                return  $inventoryVo2;
          
      }
      return "Error al Realizar Consulta";

    }


    public function InsertDataExcel($numRows,$i,$inventoryVo){
        $generalDao = new GeneralDao();
        $inventoryVo->idInventary = $_GET["id"];
        $result = $generalDao->Set($generalVo = clone $inventoryVo);
       // $this->DeleteSeriales($inventoryVo->idInventary , $inventoryVo->numberRow );
            if($i >= $numRows){
                $message = '<script> MessageSwalBasic("Insertado!","Se ha agregado la plantilla Excel Correctamente, Numero de registros: "+'.($numRows-1).',"success"); </script>';
                echo $message;
            }
        }
    
    private function DeleteSeriales($idProduct , $fila){
        $SerialVo = new SerialVo();
        $generalDao = new GeneralDao();
        $SerialVo->idSearchField = 3; 
        $SerialVo->isList = true;
        $SerialVo->idInventary = $idProduct;
        $generalDao->GetByField($generalVo = clone $SerialVo);
        while($SerialVo2 = $generalDao->GetVo($generalVo = clone $SerialVo)){
            if($SerialVo2->numberRow == $fila){
                $SerialVo3 = new SerialVo();
                $SerialVo3->id = $SerialVo2->id;
                $generalDao = new GeneralDao();
                $generalDao->Delete($generalVo = clone $SerialVo3);
            }
        }
    }

}
