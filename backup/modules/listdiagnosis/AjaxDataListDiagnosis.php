<?php
require_once(__DIR__ . '/../../../../controller/general/Config.php');
require_once(Config::PATH . Config::CONTROLLER.Config::GENERAL . 'ManageArrays.php');
require_once(Config::PATH . Config::BACKEND . 'modules/ProductVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/QuotationVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/CompanyVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/HistoryModificationVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/UserQuotationVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/PersonVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/DiagnosisVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/EvaluationDiagnosisVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/PhotosDiagnosisVo.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralDao.php');

class AjaxDataMaintenance{

    private $isActionFromAjax;
    private $optionInputForm;
    private $idDataInputForm;

    function __construct()
    {
        if (isset($_POST['isActionFromAjax'])) {
            $this->isActionFromAjax = $_POST['isActionFromAjax'];
            $this->optionInputForm = $_POST['optionInputForm'];
            $this->idDataInputForm = $_POST['idDataInputForm'];

            if ($this->isActionFromAjax == "true") {
                if ($this->optionInputForm == 3) {
                    $this->SetDataInControls();
                }else if($this->optionInputForm == 5){
                $this->GetLengthImpost();
                }else if($this->optionInputForm == 7){
                $this->GetContacts();
                }else {
                    $this->GetDataInJsonForTbl();
                }
            }
        }
    }

    private function SetDataInControls(){
        $SetInputsDataFJQ = "";
        $impostVo = new ImpostVo();
        $impostVo->id = $this->idDataInputForm;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $impostVo);
        if($impostVo2 = $generalDao->GetVo($generalVo = clone $impostVo)){
            $SetInputsDataFJQ = "
                            <script>
                               (function($){
                                SetValueToInputText('impostNameCtx','{$impostVo2->name}');
                                SetValueToInputText('impostPercentCtx','{$impostVo2->percent}');
                                SetValueToInputText('optionInputForm',4);
                                SetValueToInputText('idDataInputForm',$impostVo2->id);
                               })(jQuery);
                            </script>
                        ";
        }
        echo $SetInputsDataFJQ;
    }

    public function GetDataInJsonForTbl()
    {
        $existData = false;
        $DiagnosisVo = new DiagnosisVo();
        $generalDao = new GeneralDao();
        $generalDao->CustomQuery("SELECT diagnosis.* FROM diagnosis INNER JOIN product ON product.id = diagnosis.idProduct");
        $string = "
        {\"data\": [";
        while ($DiagnosisVo2 = $generalDao->GetVo($generalVo = clone $DiagnosisVo)) {
            $existData = true;
            //print_r($DiagnosisVo2);
            $json = array();
            $json[] = htmlspecialchars($this->GetProductByIdDiagnosis($DiagnosisVo2->idProduct)->reference);
            $json[] = htmlspecialchars($this->GetProductByIdDiagnosis($DiagnosisVo2->idProduct)->name);
            $json[] = htmlspecialchars($this->GetProductByIdDiagnosis($DiagnosisVo2->idProduct)->brand);
            $json[] = htmlspecialchars($this->GetProductByIdDiagnosis($DiagnosisVo2->idProduct)->typeProduct);
            $json[] = htmlspecialchars($this->GetProductByIdDiagnosis($DiagnosisVo2->idProduct)->namePlace);
            $json[] = htmlspecialchars($this->GetProductByIdDiagnosis($DiagnosisVo2->idProduct)->nameCompany);
            $json[] = htmlspecialchars("#".$this->GetProductByIdDiagnosis($DiagnosisVo2->idProduct)->idQuotation);
            $json[] = $this->GetCompany($this->GetQuotation($this->GetProductByIdDiagnosis($DiagnosisVo2->idProduct)->idQuotation)->idCompany)->name;
            $json[] = $this->GetDataDiagnosis($this->GetProductByIdDiagnosis($DiagnosisVo2->idProduct)->id)->dateCreated;
            $percent =  $this->GetPercentUseDiagnosis($DiagnosisVo2);
            if($percent < 45){
                $json[] = "<p style='color:red;text-align:center;font-size:15px'>".$percent."%</p>";
            }else if($percent < 70 ){
                $json[] = "<p style='color:orange;text-align:center;font-size:15px'>".$percent."%</p>";
            }else if($percent > 70 ){
                $json[] = "<p style='color:green;text-align:center;font-size:15px'>".$percent."%</p>";
            }
            $json[] = "<tip title='DIagnosticar' class='yellow-tooltip' data-toggle='tooltip' data-placement='top'><a id='DiagnosticarBtn' name='DiagnosticarBtn' class='btn btn-warning  btn-xs' title='ver/editar Diagnostico' onclick='location.href=\"Diagnosis?idProduct={$this->GetProductByIdDiagnosis($DiagnosisVo2->idProduct)->id}&diagnosis={$DiagnosisVo2->id}\"'><i class='fa fa-edit'></i></a></tip> ";
           



            $string .= json_encode($json) . ",";

        }
        $string .= "-]}-";

        if($existData){
            echo  str_replace(",-]}-", "]}", $string);
        }else{
            echo "{ \"data\": [] }";
        }
    }

    public function GetProductByIdDiagnosis($idProduct){
        $ProductVo = new ProductVo();
        $ProductVo->id = $idProduct;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $ProductVo);
        if ($ProductVo2 = $generalDao->GetVo($generalVo = clone $ProductVo)) {
            return $ProductVo2;
        }
    }

    public function GetDataDiagnosis($idProduct){
        $DiagnosisVo = new DiagnosisVo();
        $DiagnosisVo->idProduct = $idProduct;
        $DiagnosisVo->idSearchField = 5;
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $DiagnosisVo);
        if ($DiagnosisVo2 = $generalDao->GetVo($generalVo = clone $DiagnosisVo)) {
            return $DiagnosisVo2;
        }
    }

    public function GetPercentUseDiagnosis($diagnosis){
        $suma = 10;
        $suma += $this->GetPercentFromDiagnosis($diagnosis);
        $suma += $this->GetPercentFromEvaluation($this->GetEvaluation($diagnosis->id));
        $suma += $this->GetPercentFromPhotos($this->GetPhotos($diagnosis->id));
        return $suma;
    }

    public function GetPercentFromDiagnosis($diagnosisVo){
        $suma = 0;
        if($diagnosisVo->machineCommentState){ //campo de fotos es el 20% +  
            $suma += 20;
        }else{
            $suma += 0;
        }

        if($diagnosisVo->commentClient){ //campo de comentarios del cliente es el 10% +  
            $suma += 10;
        }else{
            $suma += 0;
        }
        //print_r($diagnosisVo);
        if($diagnosisVo->machineObservation){ //campo de comentarios es el 12.5% +  
            $suma += 10;
        }else{
            $suma += 0;
        }

        if($diagnosisVo->firmClient){ //campo de firmas es el 2.5% +  
            $suma += 2.5;
        }else{
            $suma += 0;
        }

        if($diagnosisVo->firmMaintenance){ //campo de firmas es el 2.5% +  
            $suma += 2.5;
        }else{
            $suma += 0;
        }

        return $suma;
    }

    public function GetPercentFromEvaluation($evaluationVo){
        $suma = 0;
        if(isset($evaluationVo->satisfation) || isset($evaluationVo->hurt) || isset($evaluationVo->machine) ){
            $suma += 15; 
        }else{
            $suma += 0; 
        }

        return $suma;
    }

    public function GetPhotos($id){
        $PhotosDiagnosisVo = new PhotosDiagnosisVo();
        $PhotosDiagnosisVo->idDiagnosis = $id;
        $PhotosDiagnosisVo->idSearchField = 4;
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $PhotosDiagnosisVo);
        if ($PhotosDiagnosisVo2 = $generalDao->GetVo($generalVo = clone $PhotosDiagnosisVo)) {
            return $PhotosDiagnosisVo2;
        }
    }

    public function GetEvaluation($id){
        $EvaluationDiagnosisVo = new EvaluationDiagnosisVo();
        $EvaluationDiagnosisVo->idDiagnosis = $id;
        $EvaluationDiagnosisVo->idSearchField = 4;
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $EvaluationDiagnosisVo);
        if ($EvaluationDiagnosisVo2 = $generalDao->GetVo($generalVo = clone $EvaluationDiagnosisVo)) {
            return $EvaluationDiagnosisVo2;
        }
    }

    public function GetPercentFromPhotos($photosVo){
        $suma = 0;
        if($photosVo->photo1 != "view/img/upload.jpg"){
            $suma += 10; 
        }else{
            $suma += 0; 
        }

            if($photosVo->photo2  != "view/img/upload.jpg"){
                $suma += 10; 
            }else{
                $suma += 0; 
            }

        if($photosVo->photo3  != "view/img/upload.jpg"){
            $suma += 10; 
        }else{
            $suma += 0; 
        }

        return $suma;
        
    }

    public function GetContacts(){
        $dataArray = [];
        $UserQuotationVo = new UserQuotationVo();
        $UserQuotationVo->idSearchField = 3;
        $UserQuotationVo->idQuotation = $_POST["idQuotation"];
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $UserQuotationVo);
        while ($UserQuotationVo2 = $generalDao->GetVo($generalVo = clone $UserQuotationVo)) {
            $dataArray[] = $this->GetPerson($UserQuotationVo2->idPerson);
        }

        print_r(json_encode($dataArray));
    }

    public function GetPerson($id){
        $PersonVo = new PersonVo();
        $PersonVo->id = $id;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $PersonVo);
        if ($PersonVo2 = $generalDao->GetVo($generalVo = clone $PersonVo)) {
            return $PersonVo2;
        }
    }
    

    public function GetCompany($id){
        $CompanyVo = new CompanyVo();
        $CompanyVo->id = $id;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $CompanyVo);
        if ($CompanyVo2 = $generalDao->GetVo($generalVo = clone $CompanyVo)) {
            return $CompanyVo2;
        }
    }

       public function GetQuotation($id){
        $QuotationVo = new QuotationVo();
        $QuotationVo->id = $id;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $QuotationVo);
        if ($QuotationVo2 = $generalDao->GetVo($generalVo = clone $QuotationVo)) {
            return $QuotationVo2;
        }
    }

    public function GetDateBuy(){
        $dataArray = [];
        $HistorymodificationVo = new HistorymodificationVo();
        $HistorymodificationVo->idSearchField = $id;
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $HistorymodificationVo);
        while ($HistorymodificationVo2 = $generalDao->GetVo($generalVo = clone $HistorymodificationVo)) {
            $dataArray[] = $HistorymodificationVo2;
        }

        return end($dataArray)->modificationDate;
    }

    
}

$ajaxDataImpost = new AjaxDataMaintenance();
