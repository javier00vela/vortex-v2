// jshint unused:true
"use strict";

function LoadTable() {
    var parameters = {
        "isActionFromAjax" : true,
        "optionInputForm" : 0,
        "idDataInputForm" : 0
    };


    $('#impostTbl').DataTable( {
        responsive: true,
           "drawCallback": function( settings ) {
            $('[data-toggle="tooltip"]').tooltip()
        },
        language : {
                "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
            },
        "ajax": {
            url : "view/html/modules/listdiagnosis/AjaxDataListDiagnosis.php",
            method : "POST",
            data : parameters
        }
    });
}

/*********************************CLICK EVENTS**************************/

$(function() {
    LoadTable();

});
