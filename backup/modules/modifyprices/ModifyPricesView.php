<?php
require_once(Config::PATH . Config::CONTROLLER . 'general/AccessManager.php');
require_once(Config::PATH . Config::CONTROLLER . 'general/UtilJquery.php');
require_once(Config::PATH . Config::VIEW_HTML . 'general/comps/GeneralCtx.php');
require_once(Config::PATH . Config::VIEW_HTML . 'general/comps/GeneralLst.php');
require_once(Config::PATH . Config::VIEW_HTML . 'general/comps/GeneralWithDataLst.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralDao.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/AditionalsImpostVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/PricesCompanyVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/CompanyVo.php');

class ModifyPricesView
{
    private $optionInputForm;
    private $isUpdateUserForm;
    private $idDataInputForm;
    /*=============================================
                  Session Vars
    =============================================*/
    private $manageSession;
    /*=============================================
                  Template Vars
    =============================================*/
    private $utilJQ;
    private $pageTpl, $data, $core;
    /*=============================================
                Controls Vars HTML
    =============================================*/
    private $TMPnameCtx;
    private $typeMoneyLst;
    private $PorcentToSailCtx;
    private $impostNameCtx;
    private $impostPercentCtx;
    private $idCompanyPriceCtx;
    private $percents = ["DDP"=>"" , "FCA"=>"" , "CIF"=>"" ];
   

   private $ddp = 0;
   private $cif= 0;
   private $fca= 0;


    /*=============================================
                Data Vars
    =============================================*/
    private $generalDao;


    function __construct($pageTpl, $data, $core)
    {
         $this->PassImpost();
        $this->data = $data;
        $this->core = $core;
        $this->pageTpl = $pageTpl;
   
        $this->ValidateIfExistCompany($_GET["idCompany"]);
           
            $this->CreateComponents();
            $this->UseDataFromThePostback();
         
    }

    private function UseDataFromThePostback()
    {
        if (isset($_POST['optionInputForm'])) {
            $this->optionInputForm = $_POST['optionInputForm'];
            $this->idDataInputForm = $_POST['idDataInputForm'];
            $this->generalDao = new GeneralDao();
            switch ($this->optionInputForm) {
                case "1": {
                    $this->SetData();
                    break;
                }
                case "2": {
                    $this->DeleteData();
                    break;
                }
                case "4": {
                    $this->SetUpdate();
                    break;
                }
                case "8": {
                    $this->LoadJsonMoneys();
                    break;
                }
            }
        }

        $this->AssignDataToTpl();

    }

    private function CreateComponents()
    {
        $this->TMPnameCtx = new GeneralCtx("TMPnameCtx","Nombre",null,true);
        $array = array("EUR","USD","COP");
      
        $this->typeMoneyLst = new  GeneralWithDataLst($array, "typeMoneyLst", false,false,"-- seleccione un tipo de moneda --",true);
        $this->impostNameCtx = new GeneralCtx("impostNameCtx","Nombre",null,true);
        $this->impostPercentCtx = new GeneralCtx("impostPercentCtx","Porcentaje",null,true);
       
        $this->utilJQ = new UtilJquery("Impost");
    }
    private function SetDataToVo()
    {
        $impostVo = new AditionalsImpostVo();
        $impostVo->id = $this->idDataInputForm;
        $impostVo->name = $_POST["impostNameCtx"];
        $impostVo->percent = $_POST["impostPercentCtx"];
        $impostVo->DDP = $_POST["DDP"];
        $impostVo->FCA = $_POST["FCA"];
        $impostVo->CIF = $_POST["CIF"];
        $impostVo->idPricesCompany = $_POST["idCompanyCtx"];
       
        return $impostVo;
    }
    private function SetData()
    {
        $impostVo = $this->SetDataToVo();
        $impostVo->id = null;
        $result = $this->generalDao->Set($generalVo = clone $impostVo);
        if (isset($result)) {
            $message = 'MessageSwalBasic("Registrado!","Costo Registrado Correctamente","success");';
            $this->utilJQ->AddFunctionJavaScript($message);
            header("Refresh:0");
        }

    }

    private function LoadJsonMoneys(){
       $json = file_get_contents("http://api.currencies.zone/v1/quotes/".$_POST["from"]."/COP/json?key=136|QdBH^Od*z2Q~sSLOfnKPYZYEGGECk1_1");
       print_r("---**".$json."---**");
    }

    private function SetUpdate()
    {
        $result = $this->generalDao->Update($generalVo = clone $this->SetDataToVo());
        if (isset($result)) {
            $message = 'MessageSwalBasic("Modificado!","Costo Modificado Correctamente","success");';
            $this->utilJQ->AddFunctionJavaScript($message);
        }
    }

    private function ValidateIfExistCompany($id){
        if(empty($this->GetVoIdCompany($id)->id)){
            header("location: DontFind");
        }
    }


    private function DeleteData()
    {
        $message = "";
        $impostVo = new AditionalsImpostVo();
        $impostVo->id = $this->idDataInputForm;
        if ($this->generalDao->Delete($generalVo = clone $impostVo)) {
            $message = 'MessageSwalBasic("Eliminado!","Impuesto Eliminado Correctamente","success");';
        } else {
            $message = 'MessageSwalBasic("Error!","Problema con la base de datos","error");';
        }
        $this->utilJQ->AddFunctionJavaScript($message);
    }

    private function AssignDataToTpl()
    {
      
        $this->data->assign('TMPnameCtx', $this->GetVoIdCompany($_GET["idCompany"])->tpm);
        $this->data->assign('typeMoneyLst', $this->typeMoneyLst->paint());
        $this->data->assign('PorcentToSailCtx', $this->GetVoIdCompany($_GET["idCompany"])->ganancia);
        $this->data->assign('SelectedTypeMoney', $this->GetVoIdCompany($_GET["idCompany"])->typeMoney);
        $this->data->assign('nameCompany', $this->GetCompany($_GET["idCompany"]));
        $this->data->assign('idPricesCompany', $this->GetVoIdCompany($_GET["idCompany"])->id);
      
        $this->data->assign('impostNameCtx', $this->impostNameCtx->paint());
        $this->data->assign('impostPercentCtx', $this->impostPercentCtx->paint());
        $this->data->assign('idURL', $_GET["idCompany"]);

          $this->data->assign('DDP', $this->ddp);
        $this->data->assign('CFI', $this->cif);
        $this->data->assign('FCA', $this->fca);
        $this->data->assign('percents', $this->percents);

         $this->data->assign('tempParameter', time());
        $this->data->assign('jquery', $this->utilJQ->PaintJQ());
        $this->SetDataToTpl();
    }   
    private function SetDataToTpl()
    {
        echo $this->core->get($this->pageTpl, $this->data);
    }

    private function GetVoIdCompany($idCompany){
        $pricesCompanyVo = new PricesCompanyVo();
        $generalDao = new GeneralDao();
        $pricesCompanyVo->idCompany = $idCompany;
        $pricesCompanyVo->idSearchField = 4;
        $generalDao->GetByField($generalVo = clone $pricesCompanyVo);

        if($pricesCompanyVo2 = $generalDao->GetVo($generalVo = clone $pricesCompanyVo)){
                return  $pricesCompanyVo2;
          
      }
      return "Error al Realizar Consulta";
    }  


    private function CheckedValidate($validate){
        if($validate == 1){
            return "checked";
        }
        return false;
    } 

        private function GetCompany($idCompany)
    {
        $companyVo = new CompanyVo();
        $companyVo->id = $idCompany;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $companyVo);
        if ($companyVo2 = $generalDao->GetVo($generalVo = clone $companyVo)) {
            return "" . $companyVo2->name;
        }
        return "Problema en la consulta";
    } 
  

  private function PassImpost(){
         $impostVo = new AditionalsImpostVo();
            $generalDao = new GeneralDao();
        $impostVo->idPricesCompany = $this->GetVoIdCompany($_GET["idCompany"])->id;
        $impostVo->idSearchField = 3;
        $generalDao->GetByField($generalVo = clone $impostVo);

        $total = 0;
        while($impostVo2 = $generalDao->GetVo($generalVo = clone $impostVo)){
            if($impostVo2->name != ""){
                if($impostVo2->DDP == "1"){
                    $this->ddp +=  floatval(str_replace(",", ".", $impostVo2->percent));
                }
                if($impostVo2->CIF == "1"){
                    $this->cif +=  floatval(str_replace(",", ".", $impostVo2->percent));
                }
                if($impostVo2->FCA == "1"){
                    $this->fca += floatval(str_replace(",", ".", $impostVo2->percent));
                    
                }
                $total +=  floatval(str_replace(",", ".", $impostVo2->percent)); 
            }
        }

        $this->percents["DDP"] = $this->ddp ;
         $this->percents["CIF"] = $this->cif ;
          $this->percents["FCA"] = $this->fca ;




  }


}   