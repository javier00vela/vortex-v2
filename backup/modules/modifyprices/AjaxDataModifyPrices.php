<?php
require_once(__DIR__ . '/../../../../controller/general/Config.php');
require_once(Config::PATH . Config::CONTROLLER.Config::GENERAL . 'ManageArrays.php');
require_once(Config::PATH . Config::BACKEND . 'modules/AditionalsImpostVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/PricesCompanyVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/InventoryVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/CompanyVo.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralDao.php');

class AjaxDataModifyPrices{

    private $isActionFromAjax;
    private $optionInputForm;
    private $idDataInputForm;
    private $dataList = [];

    function __construct()
    {
        if (isset($_POST['isActionFromAjax'])) {
            $this->isActionFromAjax = $_POST['isActionFromAjax'];
            $this->optionInputForm = $_POST['optionInputForm'];
            $this->idDataInputForm = $_POST['idDataInputForm'];
            $this->generalDao = new GeneralDao();
            if(isset($_POST["listData"])){
                $this->dataList = $_POST["listData"];  
            }
            if ($this->isActionFromAjax == "true") {
                if ($this->optionInputForm == 3) {
                    $this->SetDataInControls();
                }else if($this->optionInputForm == 5){
                $this->GetLengthImpost();
                }else if($this->optionInputForm == 6){
                $this->UpdateByInput($_POST["input"]);
                }else {
                    $this->GetDataInJsonForTbl();
                }
            }
        }
    }

    private function SetDataInControls(){
        $SetInputsDataFJQ = "";
        $impostVo = new AditionalsImpostVo();
        $impostVo->id = $this->idDataInputForm;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $impostVo);
        if($impostVo2 = $generalDao->GetVo($generalVo = clone $impostVo)){
            $SetInputsDataFJQ = "
                            <script>
                               (function($){
                                $('.modal-title').html('Modificar costo');
                                SetValueToInputText('impostNameCtx','{$impostVo2->name}');
                                SetValueToInputText('impostPercentCtx','{$impostVo2->percent}');
                                SetValueToInputText('optionInputForm',4);
                                SetValueToInputText('idDataInputForm',$impostVo2->id);
                                SetCheckedToInputToggle('DDP',$impostVo2->DDP);
                                SetCheckedToInputToggle('FCA',$impostVo2->FCA);
                                SetCheckedToInputToggle('CIF',$impostVo2->CIF);
                               })(jQuery);
                            </script>
                        ";
        }
        echo $SetInputsDataFJQ;
    }

    public function GetDataInJsonForTbl()
    {
        $existData = false;
        $AditionalsImpostVo = new AditionalsImpostVo();
        $AditionalsImpostVo->idSearchField = 3;
        $AditionalsImpostVo->idPricesCompany = $_POST["idCompany"];
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $AditionalsImpostVo);

        $string = "
        {\"data\": [";
        while ($AditionalsImpostVo2 = $generalDao->GetVo($generalVo = clone $AditionalsImpostVo)) {
            if($AditionalsImpostVo2->name != ""){
                $existData = true;
                $json = array();
                $json[] = htmlspecialchars($AditionalsImpostVo2->name);
                $json[] = $AditionalsImpostVo2->percent;
                if($AditionalsImpostVo2->DDP==1){
                $json[] = "<i style='color:green' class='fa fa-check'></i>"; 
                }else{
                    $json[] = "<i style='color:red' class='fa fa-close'></i>"; 
                }
                if($AditionalsImpostVo2->FCA==1){
                    $json[] = "<i style='color:green' class='fa fa-check'></i>"; 
                }else{
                    $json[] = "<i style='color:red' class='fa fa-close'></i>"; 
                }
                if($AditionalsImpostVo2->CIF==1){
                    $json[] = "<i style='color:green' class='fa fa-check'></i>"; 
                }else{
                    $json[] = "<i style='color:red' class='fa fa-close'></i>"; 
                }

                $button = "";
                if(!in_array( "modificar_costo" , $this->dataList)){
                    $button .= "<button id='updateBtn' name='updateBtn' class='btn btn-warning btn-xs' data-toggle='modal' data-target='#impostPopUpAdd' title='Modificar' onclick='UpdateData({$AditionalsImpostVo2->id},\"modifyprices\",\"modifyPrices\");'><i class='fa fa-pencil'></i></button> ";
                }
                if(!in_array( "eliminar_costo" , $this->dataList)){
                    $button .= "<button id='deleteBtn' name='deleteBtn' class='btn btn-danger  btn-xs' title='Eliminar' onclick='DeleteData({$AditionalsImpostVo2->id},\"modifyPrices\")'><i class='fa fa-times'></i></button> ";
                }
                $json[] = $button;

                $string .= json_encode($json) . ",";
            }
        }
        $string .= "-]}-";

        if($existData){
            echo  str_replace(",-]}-", "]}", $string);
        }else{
            echo "{ \"data\": [] }";
        }
    }

    public function GetLengthImpost(){
        $arrayList = array();
        $personVo = new impostVo();
        $personVo->isList = true;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $personVo);
        while ($personVo2 = $generalDao->GetVo($generalVo = clone $personVo)) {
            $arrayList[] =$personVo2 ;
        }
        print_r( json_encode(count($arrayList)) );
    }

      private function UpdateByInput($input){
        switch ($input) {
            case 'TMPnameCtx':
                $this->UpdateByTypeInput( $_POST["inputValue"] , "tpm" , 2);
                break;
            case 'typeMoneyLst':
                 $this->UpdateByTypeInput( $_POST["inputValue"] , "typeMoney" , 1);
                 $this->ChangePricesProduct();
                break;
            
            case 'procentSailCtx':
                  $this->UpdateByTypeInput( $_POST["inputValue"] , "ganancia" , 3);
                break;
             }

    }

    public function GetTasa(){
        $pricesCompanyVo = new PricesCompanyVo();
        $pricesCompanyVo->isList = true;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $pricesCompanyVo);
        while ($pricesCompanyVo2 = $generalDao->GetVo($generalVo = clone $pricesCompanyVo)) {
                return $pricesCompanyVo2->tmp;
        }

    }




    private function ChangePricesProduct()
    {
        if(!$_POST["inputValue"]){ $_POST["inputValue"] = "EUR";  }
        $InventoryVo = new InventoryVo();
        $InventoryVo->idSearchField = 11;
        $InventoryVo->nameCompany = $this->GetNameCompany( $_POST["idCompany"]);
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $InventoryVo);
        while($InventoryVo2 = $generalDao->GetVo($generalVo = clone $InventoryVo)) {
            if($_POST["inputValue"] == "COP" && $InventoryVo2->namePlace == "Internacional"   ){
                # no se actualiza
            }else{
                $this->SetUpdateProducts($InventoryVo2);
            }
        }
    }
  

    private function SetUpdateProducts($InventoryVo2){
        $InventoryVo = new InventoryVo();
        $InventoryVo->id = $InventoryVo2->id;
        $InventoryVo->idSearchField = 0;
        $InventoryVo->typeCurrency = $_POST["inputValue"];
        $InventoryVo->idUpdateField = 8;
        $generalDao = new GeneralDao();
        $generalDao->UpdateByField($generalVo = clone $InventoryVo);

    }


  /*
      while($InventoryVo2 = $generalDao->GetVo($generalVo = clone $InventoryVo)) {
      print_r($InventoryVo2->id);
            $InventoryVo3->id = $InventoryVo2->id;
            $InventoryVo3->idSearchField = 0;
            $InventoryVo3->typeCurrency = $_POST["inputValue"];
            $InventoryVo3->idUpdateField = 8;
            $generalDao = new GeneralDao();
         //   $generalDao->UpdateByField($generalVo = clone $InventoryVo3);
        }
    */    
    

 private  function GetNameCompany($company)
    {

        $companyVo = new CompanyVo();
        $companyVo->idSearchField = 0;
        $companyVo->id = $company;
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $companyVo);
        if ($companyVo2 = $generalDao->GetVo($generalVo = clone $companyVo)) {
            return $companyVo2->name;
        }
        return false;
    }

    private function UpdateByTypeInput($valueInput , $nameInput ,  $number){
        $pricesCompanyVo = new PricesCompanyVo();
        $pricesCompanyVo->id = $_POST["idPriceCompany"];
        $pricesCompanyVo->idUpdateField = $number ;
        $pricesCompanyVo->$nameInput = $valueInput;
        $pricesCompanyVo->idSearchField = 0;
        if ($this->generalDao->UpdateByField($generalVo = clone $pricesCompanyVo)) {}
    }

    
}

$ajaxDataImpost = new AjaxDataModifyPrices();
