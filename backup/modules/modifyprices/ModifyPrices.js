// jshint unused:true
"use strict";


function LoadTable() {
    RemoveActionModule(19);
    GetArrayFromDataModule(19);
    setTimeout(function() {
        var parameters = {
            "isActionFromAjax": true,
            "optionInputForm": 0,
            "idDataInputForm": 0,
            "idCompany": GetValueToInputText("idCompany"),
            "listData": arrayResponse,

        };

        $('#impostTbl').DataTable({
            responsive: true,
            "drawCallback": function(settings) {
                $('[data-toggle="tooltip"]').tooltip()
            },
            language: {
                "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
            },
            "ajax": {
                url: "view/html/modules/modifyprices/AjaxDataModifyPrices.php",
                method: "POST",
                data: parameters
            }
        });
    }, 500);
}


function UpdateInputs() {
    $("#1 input,select").change(function() {
        var parameters = {
            "isActionFromAjax": true,
            "optionInputForm": 6,
            "input": $(this).attr("name"),
            "idPriceCompany": GetValueToInputText("idCompany"),
            "inputValue": $(this).val(),
            "idCompany": GetValueToInputText("idUrl")
        };
        console.log(parameters);
        $.ajax({
            url: "view/html/modules/modifyprices/AjaxDataModifyPrices.php",
            type: "POST",
            data: parameters,
            success: function(data) {
                ToastShow("success", "Actualizado!!", "se ha actualizado efectivamente", 3);
            }
        });
    });
}

function SetTypeMoney() {
    var typeMoney = GetValueToInputText("SeletedTypeMoney");
    $("#typeMoneyLst option").each(function() {
        if ($(this).val() == typeMoney) {
            $(this).attr("selected", "true");
        }
    });
}

function SetListMoneys() {
    var data = 0;
    var arrayTemp = ["JPY", "USD", "EUR", "GBP"];
    for (var i = 0; i < arrayTemp.length; i++) {
        data = GetTypeMoney("full", arrayTemp[i]);
    }

}

function GetTypeMoney(index, from) {
    var top = null;
    var parameters = {
        "isActionFromAjax": true,
        "optionInputForm": 8,
        "index": index,
        "from": from
    };
    console.log(parameters);
    $.ajax({
        url: "ModifyPrices?idCompany=" + GetValueToInputText("idUrl"),
        type: "POST",
        data: parameters,
        success: function(data) {

            data = data.split("---**")[1];
            data = JSON.parse(data);
            console.log(data);
            if (from == "USD") {

            }
            $("#money-" + from).html("$" + data.result.value.toString().replace(",", "."));
        }
    });
}

function Events() {
    $("#btnGuardarCosto").on("click", OnSubmitSaveCost);
}

function OnSubmitSaveCost() {
    $("#modifyPricesForm input[type=checkbox]").each(function() {
        if (!$(this).parent().hasClass("off")) { //comprobamos si esta seleccionado 
            $("#" + $(this).attr("id") + "hidden").attr("value", "1"); //1 significa que fue seleccionado ese tipo de cotizacion
        } else {
            $("#" + $(this).attr("id") + "hidden").attr("value", "0"); //0 no seleccioando
        }
    });

    $("#modifyPricesForm").submit();
}

/*********************************CLICK EVENTS**************************/

function ChangeFormatPrices() {
    $("#impostPercentCtx").keypress(function(event) {

        if (event.charCode > 58 || event.charCode < 43 || event.charCode == 45 || event.charCode == 46) {
            return false;
        }
    });
}

function ValidatePriceUnit() {
    $("#impostPercentCtx").change(function(event) {
        var cont = 0;
        var numberType = $(this).val();
        for (var i = 0; i < numberType.toString().length; i++) {
            if (numberType.toString()[i] == "," || numberType.toString()[i] == ".") {
                cont++;
            }
        }
        if (cont > 1) {
            numberType = "";
        }
        $(this).val(numberType);
    });
}

function ActiveEditPrice() {
    $("#modifyPriceBtn").click(function() {
        $("#TMPnameCtx").removeAttr('readonly');
    });
}



$(function() {
    $("#TMPnameCtx").attr("readonly", "true");
    // $("#typeMoneyLst").attr("disabled","true");
    LoadTable();
    ChangeFormatPrices();
    ValidatePriceUnit();
    SetListMoneys();
    SetTypeMoney();
    UpdateInputs();
    Events();
    ActiveEditPrice();


    $("ul#ticker01").liScroll();
    $("ul#ticker02").liScroll({ travelocity: 0.07 });



    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-36251023-1']);
    _gaq.push(['_setDomainName', 'jqueryscript.net']);
    _gaq.push(['_trackPageview']);


    var ga = document.createElement('script');
    ga.type = 'text/javascript';
    ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(ga, s);


});