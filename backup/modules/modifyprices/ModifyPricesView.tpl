<div class="content-wrapper">
    <section class="content-header m-2">
        <h1>
            Modificar Precios Al Proveedor (<strong>{$nameCompany}</strong>)
        </h1>
        <ol class="breadcrumb">
            <li><a href="Home"><i class="fa fa-dashboard"></i> Inicio</a></li>
            <li class="active">Modificar Precios</li>
        </ol>
    </section>
    <input type="hidden" id="idUrl" value="{$idURL}">

    <section class="content">
        <div class="box m-5">
            <div class="box ">
                <nav class="navbar navbar-light">
                    <div class="container-fluid">
                        <ul class="nav navbar-nav">
                            <li>
                                <a style="cursor: pointer;" id="userAddBtn" name="userAddBtn" onclick="history.back();">
                                    <i class="fa fa-arrow-left"></i> Regresar
                                </a>
                            </li>
                            <li>
                                <a style="cursor: pointer;" id="userAddBtn" name="userAddBtn" onclick="location.href = 'Home' ">
                                    <i class="fa fa-home"></i> Menu Principal
                                </a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>

            <nav class="navbar navbar-light">
                <div class="container-fluid">
                    <br>
                    <ul id="ticker02" style="width: 100%">
                        <li>
                            <a style="cursor: pointer;">
                                <label> El precio del Dolar el dia de hoy es :</label>
                                <label id="money-USD">cargando...</label>
                            </a>
                        </li>
                        <li>
                            <a style="cursor: pointer;">
                                <label> El precio del  Euro  el dia de hoy es :</label>
                                <label id="money-EUR">cargando...</label>
                            </a>
                        </li>
                        <li>
                            <a style="cursor: pointer;">
                                <label> El precio del Yen  el dia de hoy es :</label>
                                <label id="money-JPY">cargando...</label>
                            </a>
                        </li>
                        <li>
                            <a style="cursor: pointer;">
                                <label> El precio de la libra Esterlina  el dia de hoy es :</label>
                                <label id="money-GBP">cargando...</label>
                            </a>
                        </li>

                    </ul>
                </div>
            </nav>
            <div class="box-body container-fluid" id="1">
                <div class="row">
                    <div class="col-sm-4">

                        <div class="form-group" disabled>
                            <label>Precio Moneda (TRM)</label>
                            <input type="number" name="TMPnameCtx" class="form-control" id="TMPnameCtx" value="{$TMPnameCtx}">

                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Tipo Moneda </label>
                            <input type="hidden" id="SeletedTypeMoney" value="{$SelectedTypeMoney}"> {$typeMoneyLst}
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>% Porcentaje Venta </label>
                            <input type="number" name="procentSailCtx" class="form-control" min="0" id="PorcentToSailCtx" value="{$PorcentToSailCtx}">
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="box">
            <nav class="navbar navbar-light">
                <div class="container-fluid">
                    <ul class="nav navbar-nav">
                        <li>
                            <a node="node-agregar_costo" style="cursor: pointer;" id="impostAddBtn" name="impostAddBtn" data-toggle="modal" data-target="#impostPopUpAdd" onclick="ResetControls('containerinputs');SetData()">
                                <i class="fa fa-plus-circle"></i> Agregar costo
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>

            <div class="box-body">
                <div class="col-sm-7 col-xs-12">
                    <table id="impostTbl" name="impostTbl" class="table table-bordered table-striped dt-responsive tables dataTable no-footer dtr-inline" style="width: 100%;" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Porcentaje</th>
                                <th>DDP</th>
                                <th>FCA</th>
                                <th>CIF</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                    </table>
                </div>
                <div class="col-sm-5 col-xs-12">

                    <div class="info-box bg-red">
                        <span class="info-box-icon">  DDP </span>
                        <div class="info-box-content">
                            <span class="info-box-text">Suma Total DDP:</span>
                            <span class="info-box-number" id="DDP">{$DDP}%</span>
                            <div class="progress">
                                <div class="progress-bar" style="width:  {$percents['DDP']}%"></div>
                            </div>
                            <span class="progress-description">
                     {$percents['DDP']}% 
                  </span>
                        </div>
                    </div>

                    <div class="info-box bg-blue">
                        <span class="info-box-icon">  FCA </span>
                        <div class="info-box-content">
                            <span class="info-box-text">Suma Total FCA:</span>
                            <span class="info-box-number" id="FCA">{$FCA}%</span>
                            <div class="progress">
                                <div class="progress-bar" style="width:  {$percents['FCA']}%"></div>
                            </div>
                            <span class="progress-description">
                    {$percents['FCA']}%  
                  </span>
                        </div>
                    </div>

                    <div class="info-box bg-green">
                        <span class="info-box-icon">  CIF </span>
                        <div class="info-box-content">
                            <span class="info-box-text">Suma Total CIF:</span>
                            <span class="info-box-number" id="CIF">{$CFI}%</span>
                            <div class="progress">
                                <div class="progress-bar" style="width:  {$percents['CIF']}%"></div>
                            </div>
                            <span class="progress-description">
                    {$percents['CIF']}%  
                  </span>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
</div>
<!--=====================================
VENTANA MODAL AGREGAR IMPUESTO O MODIFICAR
======================================-->
<div id="impostPopUpAdd" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form id="modifyPricesForm" role="form" method="post" enctype="multipart/form-data">
                <!--=====================================
              Hidden
              ======================================-->
                <input id="optionInputForm" name="optionInputForm" type="hidden" value="0">
                <input id="idDataInputForm" name="idDataInputForm" type="hidden" value="0">
                <input id="isActionFromAjax" name="isActionFromAjax" type="hidden" value="false">

                <input id="DDPhidden" name="DDP" type="hidden" value="0">
                <input id="FCAhidden" name="FCA" type="hidden" value="0">
                <input id="CIFhidden" name="CIF" type="hidden" value="0">
                <!--=====================================
              CABEZA DEL MODAL
              ======================================-->
                <div class="modal-header" style="background:#3c8dbc; color:white">
                    <button type="button" class="close" data-dismiss="modal" data-target='#impostPopUpAdd' onclick="ResetControls('impostForm')">&times;</button>
                    <h4 class="modal-title">Agregar costo</h4>
                </div>
                <!--=====================================
              CUERPO DEL MODAL
              ======================================-->
                <div class="modal-body">
                    <div class="alert bg-orange">
                        <p><i class="fa fa-warning">  </i> Recuerda seleccionar en cuales de los tipos de cotización(DDP,FCA,CIF) se aplicará tal costo al producto. </p>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-industry"></i></span> {$impostNameCtx}
                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-percent"></i></span> {$impostPercentCtx}
                                </div>
                            </div>
                            <div class="form-group col-xs-4 col-sm-2 col-md-2 col-lg-2">
                                <div class="input-group">

                                    <label>
                                  <input type="checkbox" id="DDP" data-toggle="toggle">
                                  DDP
                                </label>

                                </div>
                            </div>
                            <div class="form-group col-xs-4 col-sm-2 col-md-2 col-lg-2">
                                <div class="input-group">

                                    <label>
                                  <input type="checkbox" id="FCA" data-toggle="toggle">
                                  FCA
                                </label>

                                </div>
                            </div>
                            <div class="form-group col-xs-4 col-sm-2 col-md-2 col-lg-2">
                                <div class="input-group">

                                    <label>
                                  <input type="checkbox" id="CIF" data-toggle="toggle">
                                  CIF
                                </label>

                                </div>
                            </div>
                            <input type="hidden" name="idCompanyCtx" id="idCompany" value="{$idPricesCompany}">
                        </div>
                    </div>
                </div>
                <!--=====================================
              PIE DEL MODAL
              ======================================-->
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal" data-target='#impostPopUpAdd' onclick="ResetControls('modifyPricesForm')">Salir</button>
                    <button type="button" class="btn btn-primary" id="btnGuardarCosto">Guardar </button>
                </div>
            </form>

        </div>
    </div>
</div>
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<div id="containerResponseAjax"></div>
<script src="view/html/modules/modifyprices/ModifyPrices.js?v= {$tempParameter}"></script>
{$jquery}