<?php
require_once(Config::PATH . Config::CONTROLLER_LIB . 'mobileDetect/Mobile_Detect.php');
require_once(Config::PATH . Config::CONTROLLER . 'general/UtilJquery.php');
require_once(Config::PATH . Config::CONTROLLER . 'general/ManageSession.php');
require_once(Config::PATH . Config::VIEW_HTML . 'general/comps/GeneralCtx.php');
require_once(Config::PATH . Config::VIEW_HTML . 'general/comps/GeneralLst.php');
require_once(Config::PATH . Config::VIEW_HTML . 'general/comps/GeneralWithDataLst.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralDao.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/AccessHistoryVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/UserVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/PersonVo.php');

class  LoginView
{

    private $pageTpl, $data, $core;
    private $mobile_Detect;
    private $manageSession;

    function __construct($pageTpl, $data, $core)
    {
        $this->data = $data;
        $this->core = $core;
        $this->pageTpl = $pageTpl;
        $this->manageSession = new ManageSession();
        $this->mobile_Detect;
        $this->UseDataFromThePostback();
    }

    private function UseDataFromThePostback()
    {
        $this->Login();
        $this->AssignDataToTpl();
    }

    private function Login()
    {
        if ($_POST) {
            if (preg_match('/^[a-zA-Z0-9]+$/', $_POST["userNicknameCtx"]) &&
                preg_match('/^[a-zA-Z0-9]+$/', $_POST["userPasswordCtx"])
            ) {
                $userNickname = strtolower(trim($_POST["userNicknameCtx"]));
                $encryptPassword = strtolower($_POST["userPasswordCtx"]);//crypt($_POST["userPasswordCtx"], '$2a$07$asxx54ahjppf45sd87a5a4dDDGsystemdev$');
                $userVo = new UserVo();
                $userVo->idSearchField = 1;
                $userVo->nickname = $userNickname;
                $generalDao = new GeneralDao();
                $generalDao->GetByField($generalVo = clone $userVo);
                if ($userVo2 = $generalDao->GetVo($generalVo = clone $userVo)) {
                    if($userVo2->state == 1 && strtolower(trim($userVo2->nickname)) == strtolower($userNickname) && strtolower($userVo2->password) == strtolower($encryptPassword)){
                        $this->manageSession->CreateSessionUser($userVo2);
                        $this->CreateSessionUser($userVo2);
                        $this->SetAccessHistory($userVo2);
                        if(isset($_GET["DataReceived"])){
                            echo "<script>window.location = 'ConfigData?id={$_GET["DataReceived"]}';</script>";
                        }else{
                           echo '<script>window.location = "Home";</script>';
                        }
                    }else{
                        $message = '<script> MessageSwalBasic("Alerta de Usuario!","Usuario Inactivo, Consulte con el Administrador del Sistema para dar Permisos de Ingreeso","warning"); </script>';
                        echo $message;
                    }
                }else{
                    $message = '<script> MessageSwalBasic("Error de Usuario y Contraseña!","El Usuario Ingresado no Existe, Consulte con el Administrador del Sistema Para mas Información","error"); </script>';
                    echo $message;
                }
            }
        }
    }

    private function CreateSessionUser($userVo){
        $personVo = new PersonVo();
        $personVo->id = $userVo->idPerson;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $personVo);
        if ($personVo2 = $generalDao->GetVo($generalVo = clone $personVo)) {
            $this->manageSession->CreateSessionPerson($personVo2);
        }
    }

    private function SetAccessHistory($userVo){
        date_default_timezone_set("America/Bogota");
        $this->mobile_Detect = new Mobile_Detect();
        $accessHistoryVo = new AccessHistoryVo();
        $accessHistoryVo->login = date("Y-m-d H:i:s");
        $accessHistoryVo->logout = "00-00-00 00:00:00";
        $accessHistoryVo->numHours = "0";
        $accessHistoryVo->ipAddress = $this->GetIpAdress();
        $accessHistoryVo->device = $this->GetDevice();
        $accessHistoryVo->browser = $this->GetBrowser();
        $accessHistoryVo->idUser = $this->GetIdPersonByIdUser($userVo->id);
        $_SESSION['user']["accessHistory"] = serialize($accessHistoryVo);
        $generalDao = new GeneralDao();
        $generalDao->Set($generalVo = clone $accessHistoryVo);
    }

      private function GetIdPersonByIdUser($idUser){
        $userVo = new UserVo();
        $personVo = new PersonVo();

        $userVo->id = $idUser;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $userVo);
        if($userVo2 = $generalDao->GetVo($generalVo = clone $userVo)){
            $personVo->id = $userVo2->idPerson;
            $generalDao->GetById($generalVo = clone $personVo);
            if($personVo2 = $generalDao->GetVo($generalVo = clone $personVo)){
              if($personVo2->state ==1){
                return  $personVo2->id;
              }
            }    
        }
    }

    public function GetIpAdress() {
        if (getenv('HTTP_CLIENT_IP')) {
            $ip = getenv('HTTP_CLIENT_IP');
        } elseif (getenv('HTTP_X_FORWARDED_FOR')) {
            $ip = getenv('HTTP_X_FORWARDED_FOR');
        } elseif (getenv('HTTP_X_FORWARDED')) {
            $ip = getenv('HTTP_X_FORWARDED');
        } elseif (getenv('HTTP_FORWARDED_FOR')) {
            $ip = getenv('HTTP_FORWARDED_FOR');
        } elseif (getenv('HTTP_FORWARDED')) {
            $ip = getenv('HTTP_FORWARDED');
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }


    public function GetBrowser() {
        $detect = new Mobile_Detect;
        $browsers = array();
        foreach ($detect->getProperties() as $name => $match) {
            $check = $detect->version($name);
            if ($check != false) {
                array_push($browsers, $name);
            }
        }
        return $browsers[0];
    }

    public function GetDevice() {
        $detect = new Mobile_Detect;
        $deviceType = ($detect->isMobile() ? ($detect->isTablet() ? 'tablet' : 'celular') : 'Computador');
        return $deviceType;
    }


    private function AssignDataToTpl()
    {
        //$this->data->assign('userNicknameCtx',$this->userNicknameCtx->Paint());
        //$this->data->assign('userPasswordCtx',$this->userPasswordCtx->Paint());
        //$this->data->assign('jquery',$this->utilJQ->PaintJQ());
        $this->SetDataToTpl();
    }

    private function SetDataToTpl()
    {
        echo $this->core->get($this->pageTpl, $this->data);
    }
}
