<?php
require_once(__DIR__ . '/../../../../controller/general/Config.php');
require_once(Config::PATH . Config::CONTROLLER.Config::GENERAL . 'ManageArrays.php');
require_once(Config::PATH . Config::BACKEND . 'modules/PersonVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/StaticEventsVo.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralDao.php');

class AjaxDataImpost{

    private $isActionFromAjax;
    private $optionInputForm;
    private $idDataInputForm;
    public $dataList = [];

    function __construct()
    {
        if (isset($_POST['isActionFromAjax'])) {
            $this->isActionFromAjax = $_POST['isActionFromAjax'];
            $this->optionInputForm = $_POST['optionInputForm'];
            $this->idDataInputForm = $_POST['idDataInputForm'];
            if(isset($_POST["listData"])){
                $this->dataList = $_POST["listData"];  
            }
            if ($this->isActionFromAjax == "true") {
                if ($this->optionInputForm == 3) {
                    $this->SetDataInControls();
                }else if($this->optionInputForm == 5){
                $this->GetLengthImpost();
                }else {
                    $this->GetDataInJsonForTbl();
                }
            }
        }
    }

    private function SetDataInControls(){
        $SetInputsDataFJQ = "";
        $StaticEventsVo = new StaticEventsVo();
        $StaticEventsVo->id = $this->idDataInputForm;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $StaticEventsVo);
        if($StaticEventsVo2 = $generalDao->GetVo($generalVo = clone $StaticEventsVo)){
            $SetInputsDataFJQ = "
                            <script>
                               (function($){
                                SetValueToInputText('eventDescription','{$StaticEventsVo2->nameEvent}');
                                SetValueToInputText('fechaCtx','{$StaticEventsVo2->date}');
                                SetValueToInputText('fechaFinishCtx','{$StaticEventsVo2->dateFinish}');
                                SetValueToInputText('personUserLst','{$StaticEventsVo2->idUser}');
                                SetValueToInputText('horaInitCtx','{$StaticEventsVo2->hourInit}');
                                SetValueToInputText('horaFinishCtx','{$StaticEventsVo2->hourFinish}');
                                SetValueToInputText('optionInputForm',4);
                                SetValueToInputText('idDataInputForm',$StaticEventsVo2->id);
                               })(jQuery);
                            </script>
                        ";
        }
        echo $SetInputsDataFJQ;
    }

    public function GetDataInJsonForTbl()
    {
        $existData = false;
        $StaticEventsVo = new StaticEventsVo();
        $StaticEventsVo->isList = true;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $StaticEventsVo);

        $string = "
        {\"data\": [";
        while ($StaticEventsVo2 = $generalDao->GetVo($generalVo = clone $StaticEventsVo)) {
            $existData = true;
            $json = array();
           // $json[] = $impostVo2->id;
            $json[] = htmlspecialchars(ucfirst(strtolower($StaticEventsVo2->nameEvent)));
            $json[] = $StaticEventsVo2->date;
            $json[] = $StaticEventsVo2->dateFinish;
            $json[] = $this->GetPersonName($StaticEventsVo2->idUser);
            $json[] = $StaticEventsVo2->hourInit ." - ". $StaticEventsVo2->hourFinish;
            $button = "";
                if(!in_array( "modificar_evento" , $this->dataList)){
                $button .= "<tip title='Modificar' class='yellow-tooltip' data-toggle='tooltip' data-placement='top'><button id='updateBtn' name='updateBtn' class='btn btn-warning btn-xs' data-toggle='modal' data-target='#calendarPopUpAdd' title='Modificar' onclick='UpdateData({$StaticEventsVo2->id},\"modifyeventsstatics\",\"ModifyEventsStatics\");'><i class='fa fa-pencil'></i></button> </tip>";
                }
                if(!in_array( "eliminar_evento" , $this->dataList)){
                $button .= " <tip title='Eliminar' class='red-tooltip' data-toggle='tooltip' data-placement='top'><a id='deleteBtn' name='deleteBtn' class='btn btn-danger  btn-xs' title='Eliminar' onclick='DeleteData({$StaticEventsVo2->id},\"calendar\")'><i class='fa fa-times'></i></a></tip> ";
                }
                $json[] = $button;

            $string .= json_encode($json) . ",";

        }
        $string .= "-]}-";

        if($existData){
            echo  str_replace(",-]}-", "]}", $string);
        }else{
            echo "{ \"data\": [] }";
        }
    }

    function GetPersonName($idPerson)
    {
        $personVo = new PersonVo();
        $personVo->id = $idPerson;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $personVo);
        if ($personVo2 = $generalDao->GetVo($generalVo = clone $personVo)) {
            return "" . $personVo2->names." ".$personVo2->lastNames;
        }
        return "Problema en la consulta";
    }

       public function GetLengthImpost(){
        $arrayList = array();
        $personVo = new impostVo();
        $personVo->isList = true;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $personVo);
        print_r($generalDao->GetLength());
    }

    
}

$ajaxDataImpost = new AjaxDataImpost();
