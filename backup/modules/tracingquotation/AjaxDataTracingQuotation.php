<?php
require_once(__DIR__ . '/../../../../controller/general/Config.php');
require_once(Config::PATH . Config::CONTROLLER.Config::GENERAL . 'ManageArrays.php');
require_once(Config::PATH . Config::BACKEND . 'modules/HistoryModificationVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/CompanyVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/UserVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/PersonVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/RoleVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/TracingQuotationVo.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralDao.php');

class AjaxDataTracingQuotation
{

    private $isActionFromAjax;
    private $optionInputForm;
    private $idDataInputForm;
    private $codeQuotation;
    public $dataList = [];

    function __construct()
    { 
       
        if (isset($_POST['isActionFromAjax'])) {
            $this->isActionFromAjax = $_POST['isActionFromAjax'];
            $this->optionInputForm = $_POST['optionInputForm'];
            $this->idDataInputForm = $_POST['idDataInputForm'];
            if(isset($_POST["listData"])){
                $this->dataList = $_POST["listData"];  
            }

            if ($this->isActionFromAjax == true) {
                if ($this->optionInputForm == 1) {
                     $this->codeQuotation = $_POST['codeQuotation'];
                    $this->GetDataInJsonForTbl();
                }else if ($this->optionInputForm == 3){
                    $this->SetDataInControls();
                }
            }
        }
    }

     public function SetDataInControls()
    {
        $SetInputsDataFJQ = "";
        $tracingQuotationVo = new TracingQuotationVo();
        $tracingQuotationVo->id = $this->idDataInputForm;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $tracingQuotationVo);
        if ($tracingQuotationVo2 = $generalDao->GetVo($generalVo = clone $tracingQuotationVo)) {
            //$descriptionHtml = trim($productVo2->description," ");
            $descriptionHtml = preg_replace("[\n|\r|\n\r]", '', $tracingQuotationVo2->description);
            $SetInputsDataFJQ = "
                  <script>
                    (function($){

                                SetValueToInputText('subjectCtx','{$tracingQuotationVo2->subject}',true);
                                SetValueToInputText('tracingUserLst','{$tracingQuotationVo2->idResponsable}');
                                SetValueToInputText('dateCtx','{$tracingQuotationVo2->dateRegistry}',true);
                                SetValueToInputText('predictionList','{$tracingQuotationVo2->prediction}',true);
                                ApplyEditor('descriptionAtx','$descriptionHtml');
                                SetValueToInputText('optionInputForm',2);
                                SetValueToInputText('idDataInputForm', $tracingQuotationVo2->id);
                      })(jQuery);
                  </script>
                            ";
        }
        echo $SetInputsDataFJQ;
    }


    public function GetDataInJsonForTbl()
    {
        $existData = false;
        $tracingQuotationVo = new TracingQuotationVo();
        $tracingQuotationVo->idSearchField = 1;
        $tracingQuotationVo->idQuotation = $this->codeQuotation;
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $tracingQuotationVo);
        $json = array();
        $string = "
        {\"data\": [";
        while ($tracingQuotationVo2 = $generalDao->GetVo($generalVo = clone $tracingQuotationVo)) {
            $existData = true;
            $json[] = $tracingQuotationVo2->id;
            $json[] =   $this->GetPersonQuotation( $tracingQuotationVo2->idResponsable);
            $json[] = $tracingQuotationVo2->subject;
            $json[] = $tracingQuotationVo2->dateRegistry;
            $json[] = $tracingQuotationVo2->prediction;
            $button = "";
            if(!in_array( "modificar_seguimiento" , $this->dataList)){
                $button .= "<tip title='Ver seguimiento' class='blue-tooltip' data-toggle='tooltip' data-placement='top'><a class='btn btn-primary btn-xs' data-toggle='modal' data-target='#ModalCreateSeguimiento' onclick='UpdateData({$tracingQuotationVo2->id},\"tracingquotation\",\"tracingQuotation\")'; title='Ver descripcion'><i class='fa fa-eye'></i></a>";
            }
            $json[] = $button;
            $string .= json_encode($json) . ",";
        }
        $string .= "-]}-";
        if($existData){
            echo  str_replace(",-]}-", "]}", $string);
        }else{
            echo "{ \"data\": [] }";
        }
    }


     private  function GetPersonQuotation($idPerson)
    {
        $nameUser="";
         $personVo= new PersonVo();
         $personVo->idSearchField = 0;
         $personVo->id = $idPerson;
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $personVo);
        while ($personVo2 = $generalDao->GetVo($generalVo = clone $personVo)) {
            $nameUser = htmlspecialchars($personVo2->names. ' '.$personVo2->lastNames) ;
        }
        return $nameUser;

    }


   

 
}
$ajax = new AjaxDataTracingQuotation();
