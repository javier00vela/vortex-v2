<?php
require_once(__DIR__ . '/../../../../controller/general/Config.php');
require_once(Config::PATH . Config::CONTROLLER.Config::GENERAL . 'ManageArrays.php');
require_once(Config::PATH . Config::BACKEND . 'modules/PersonVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/CompanyVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/UserVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/RoleVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/VisitsVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/VisitsDescriptionVo.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralDao.php');

class AjaxDataVisitsDescription
{

    private $isActionFromAjax;
    private $optionInputForm;
    private $idDataInputForm;
    public $dataList = [];
    function __construct()
    {
        if (isset($_POST['isActionFromAjax'])) {
            $this->isActionFromAjax = $_POST['isActionFromAjax'];
            $this->optionInputForm = $_POST['optionInputForm'];
            $this->idDataInputForm = $_POST['idDataInputForm'];
            if(isset($_POST["listData"])){
                $this->dataList = $_POST["listData"];  
            }
            if ($this->isActionFromAjax == "true") {
                if ($this->optionInputForm == 3) {
                    $this->SetDataInControls();
                }else if ($this->optionInputForm == 5) {
                    $this->UpdateStateUser();
                }else if($this->optionInputForm == 7){
                    $this->CallUserByCompany($_POST["idCompany"]);
                }else {
                    $this->GetDataInJsonForTbl();
                }
            }
        }
    }

       public function SetDataInControls()
    {
        $SetInputsDataFJQ = "";
        $visitVo = new VisitDescriptionVo();
        $visitVo->id = $this->idDataInputForm;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $visitVo);
        if ($visitVo2 = $generalDao->GetVo($generalVo = clone $visitVo)) {

            //$descriptionHtml = trim($productVo2->description," ");

            $descriptionHtml = preg_replace("[\n|\r|\n\r]", '', $visitVo2->description);
            $SetInputsDataFJQ = "
                  <script>
                    (function($){
                                SetValueToSelect('idDataInputForm','$visitVo2->id');
                                SetValueToSelect('productDescriptionAtx','$descriptionHtml');
                                ApplyEditor('productDescriptionAtx','$descriptionHtml');
                      })(jQuery);
                  </script>
                            ";
        }
        echo $SetInputsDataFJQ;
    }


    public function GetLengthUsers(){
        $arrayList = array();
        $personVo = new userVo();
        $personVo->isList = true;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $personVo);
        while ($personVo2 = $generalDao->GetVo($generalVo = clone $personVo)) {
            $arrayList[] =$personVo2 ;
        }
        print_r( json_encode(count($arrayList)) );
    }

    public function GetDataInJsonForTbl()
    {
        $existData = false;
        $cont = 1; 
        $visitsDescriptionsVo = new VisitDescriptionVo();
        $visitsDescriptionsVo->idVisits = $_POST["idVisit"];
         $visitsDescriptionsVo->idSearchField = 2;
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $visitsDescriptionsVo);
      
        $string = "
        {\"data\": [";
        while ($visitsDescriptionsVo2 = $generalDao->GetVo($generalVo = clone $visitsDescriptionsVo)) {
            $json = array();
             $clientVo = $this->GetClientData($this->GetVisitClient($_POST["idVisit"]));
                    $existData = true;
                $html = htmlspecialchars(preg_replace("[\n|\r|\n\r]", '', $visitsDescriptionsVo2->description));
                $json[] = $cont;
                $json[] = htmlspecialchars(ucwords(strtolower($clientVo->names)));
                $json[] = htmlspecialchars(ucwords(strtolower($clientVo->lastNames)));
                 $json[] = htmlspecialchars(ucwords(strtolower($this->GetNameByIdUser($visitsDescriptionsVo2->responsable))));
                $json[] = $visitsDescriptionsVo2->dateCreate;
                $json[] = $visitsDescriptionsVo2->dateCita;
                $button = "";
                if(!in_array( "modificar_descripcion" , $this->dataList)){
                    $button .= "<button class='btn btn-block btn-primary' data-toggle='modal' data-target='#SeeDescriptionPopUpAdd' onclick=\"loadDescription('$html','$visitsDescriptionsVo2->id') , loadDescription('$html','$visitsDescriptionsVo2->id')\"> ver Descripcion</button>";
                }
                $json[] =  $button;
                $cont++;
                
            $string .= json_encode($json) . ",";

         
        }

        $string .= "-]}-";
        if($existData){
            echo  str_replace(",-]}-", "]}", $string);
        }else{
            echo "{ \"data\": [] }";
        }
    }

    function GetClientData($id)
    {
        $PersonVo = new PersonVo();
        $PersonVo->id = $id;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $PersonVo);
        if ($PersonVo2 = $generalDao->GetVo($generalVo = clone $PersonVo)) {
            return $PersonVo2;
        }
        return "Problema en la consulta";
    }

    function GetNameByIdUser($id){
           $PersonVo = new PersonVo();
        $PersonVo->id = $id;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $PersonVo);
        if ($PersonVo2 = $generalDao->GetVo($generalVo = clone $PersonVo)) {
            return $PersonVo2->names." ".$PersonVo2->lastNames;
        }
        return "Problema en la consulta";
    }

    function GetVisitClient($id)
    {
        $VisitVo = new VisitsVo();
        $VisitVo->id = $id;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $VisitVo);
        if ($VisitVo2 = $generalDao->GetVo($generalVo = clone $VisitVo)) {
            return $VisitVo2->idClient;
        }
        return "Problema en la consulta";
    }

    function GetPersonName($idPerson)
    {
        $personVo = new PersonVo();
        $personVo->id = $idPerson;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $personVo);
        if ($personVo2 = $generalDao->GetVo($generalVo = clone $personVo)) {
            return "" . $personVo2->names." ".$personVo2->lastNames;
        }
        return "Problema en la consulta";
    }

    function GetUserVo($idPerson)
    {
        $userVo2 = new UserVo();
        $userVo = new UserVo();
        $userVo->idPerson = $idPerson;
        $userVo->idSearchField = 5;
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $userVo);
        if ($userVo2 = $generalDao->GetVo($generalVo = clone $userVo)) {
            return $userVo2;
        }
        return $userVo2;
    }

    function UpdateStateUser()
    {
        $userVo = new UserVo();
        $userVo->id = $this->idDataInputForm;
        $userVo->idSearchField = 0;
        $userVo->state = $_POST["state"];
        $userVo->idUpdateField = 3;
        $generalDao = new GeneralDao();
        $generalDao->UpdateByField($generalVo = clone $userVo);
    }


    function GetIdUserSession()
    {
            session_start();
            $userVo = unserialize($_SESSION['user']["user"]);
            $personVo = unserialize($_SESSION['user']["person"]);
            print_r($personVo->idRole);
    }

    function GetTypeRole($idRole)
    {
        $roleVo = new RoleVo();
        $roleVo->id = $idRole;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $roleVo);
        if ($roleVo2 = $generalDao->GetVo($generalVo = clone $roleVo)) {
            return "" . $roleVo2->name;
        }
        return "Problema en la consulta";
    }

    function GetCompany($idCompany)
    {
        $companyVo = new CompanyVo();
        $companyVo->id = $idCompany;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $companyVo);
        if ($companyVo2 = $generalDao->GetVo($generalVo = clone $companyVo)) {
            return "" . $companyVo2->name;
        }
        return "Problema en la consulta";
    }



    private function CallUserByCompany($rol){
        $arraylist = null;
        $userVo = new PersonVo();
        $userVo->idSearchField = 13;
        $userVo->idCompany = $rol;
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $userVo);
        $arraylist .= "*-*";
         while($userVo2 = $generalDao->GetVo($generalVo = clone $userVo)) {
            if($userVo2->state == 1){
                 $arraylist .= $userVo2->id."#".htmlspecialchars($userVo2->names." ".$userVo2->lastNames)."+";
               }
          }
          
         $arraylist .= "*-*";
        print_r($arraylist);
    }



}

$ajaxDataUser = new AjaxDataVisitsDescription();
