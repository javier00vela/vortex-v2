<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Histórico de Visita al cliente <strong>({$nameClient})</strong>
        </h1>
        <ol class="breadcrumb">
            <li><a href="Home"><i class="fa fa-dashboard"></i> Inicio</a></li>
            <li class="fa fa-dashboard">Administrar Visitas</li>
            <li class="active">Visita a {$nameClient}</li>
        </ol>
    </section>
    <section class="content">
        <div class="box">
            <div class="box-header with-border">
                <input type="hidden" id="idVisit" value="{$idVisit}">
                <nav class="navbar navbar-light">
                    <div class="container-fluid">
                        <ul class="nav navbar-nav">
                            <li>
                                <a node="node-agregar_descripcion" style="cursor: pointer;" id="userAddBtn" name="userAddBtn" data-toggle="modal" data-target="#personPopUpAdd" onclick="ResetControls('containerinputs');SetData()">
                                    <i class="fa fa-plus-circle"></i> Agregar Descripción
                                </a>
                            </li>
                            <li>
                                <a style="cursor: pointer;" id="userAddBtn" name="userAddBtn" onclick="location.href = 'Visits' ">
                                    <i class="fa fa-arrow-left"></i> Regresar
                                </a>
                            </li>
                            <li>
                                <a style="cursor: pointer;" id="userAddBtn" name="userAddBtn" onclick="location.href = 'Home' ">
                                    <i class="fa fa-home"></i> Menu Principal
                                </a>
                            </li>
                        </ul>
                    </div>
                </nav>

            </div>
            <div class="box-body">
                <table id="userTbl" name="userTbl" class="table table-bordered table-striped dt-responsive tables dataTable no-footer dtr-inline" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Nombres</th>
                            <th>Apellidos</th>
                            <th>Autor Descripción</th>
                            <th>Descripción</th>
                            <th>Fecha Registro</th>
                            <th>Fecha Cita</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </section>
</div>

<!--=====================================
VENTANA MODAL AGREGAR PERSONA O MODIFICAR
======================================-->
<div id="personPopUpAdd" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form id="VisitsForm" role="form" method="post" enctype="multipart/form-data">
                <!--=====================================
              Hidden
              ======================================-->
                <input id="optionInputForm" name="optionInputForm" type="hidden" value="0">
                <input id="idDataInputForm" name="idDataInputForm" type="hidden" value="0">
                <input id="isActionFromAjax" name="isActionFromAjax" type="hidden" value="false">
                <!--=====================================
              CABEZA DEL MODAL
              ======================================-->
                <div class="modal-header" style="background:#3c8dbc; color:white">
                    <button type="button" class="close" data-dismiss="modal" data-target='#personPopUpAdd' onclick="ResetControls('personForm')">&times;</button>
                    <h4 class="modal-title">Agregar Visita</h4>
                </div>
                <!--=====================================
              CUERPO DEL MODAL
              ======================================-->
                <div class="modal-body" id="containerinputs">
                    <div class="box-body">
                        <div class="row">
                            <div class="form-group">
                                <div class="input-group  col-xs-12">
                                    <textarea class="ckeditor" id="productDescriptionAtxs" name="productDescriptionAtx" required></textarea>
                                </div>
                            </div>

                            <div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                <span class="text-center"><strong>¿Se acordó una fecha de cita ?</strong></span>
                                <button id="ActivateBtn" class="active Btn btn btn-block  btn-success" onclick="event.preventDefault();">Hubo Acuerdo </button>
                            </div>
                            <div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                <label><strong>Fecha Cita</strong></label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-user"></i></span> {$dateCita}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <!--=====================================
              PIE DEL MODAL
              ======================================-->
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal" data-target='#personPopUpAdd' onclick="ResetControls('personForm')">Salir</button>
                <button id="submit" form="VisitsForms" class="btn btn-primary">Guardar Descripción</button>
            </div>


        </div>
    </div>
</div>

<!--=====================================
VENTANA MODAL VER DESCRIPCIÓN
======================================-->
<div id="SeeDescriptionPopUpAdd" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form method="post">
                <!--=====================================
              Hidden
              ======================================-->
                <input id="optionInputForm" name="optionInputForm" type="hidden" value="4">
                <input id="idDataInputForm" name="idDataInputForm" type="hidden" value="1">
                <input id="isActionFromAjax" name="isActionFromAjax" type="hidden" value="false">
                <!--=====================================
              CABEZA DEL MODAL
              ======================================-->
                <div class="modal-header" style="background:#3c8dbc; color:white">
                    <button type="button" class="close" data-dismiss="modal" data-target='#personPopUpAdd' onclick="ResetControls('personForm')">&times;</button>
                    <h4 class="modal-title">Ver Eventos</h4>
                </div>
                <!--=====================================
              CUERPO DEL MODAL
              ======================================-->
                <div class="modal-body" id="containerinputs">
                    <div class="box-body">
                        <div class="row">
                            <div class="form-group">
                                <div class="input-group  col-xs-12">
                                    <textarea class="ckeditor" id="productDescriptionAtx" name="productDescriptionAtx" required></textarea>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <div class="pull-right">
                        <button type="submit" class="btn btn-primary">
                      actualizar
                  </button>
                    </div>
                </div>

                <input id="idInfo" name="idInfo" type="hidden">
            </form>
        </div>
    </div>
</div>

<div id="containerResponseAjax"></div>
<script src="view/html/modules/visitsdescription/VisitsDescription.js?v= {$tempParameter}"></script>
{$jquery}