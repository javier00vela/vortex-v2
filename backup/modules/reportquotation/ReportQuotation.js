$(function() {
    dataPerson = [];
    Events();
    GetDataPerson();
    LoadTableQuotation();
    PaintQuotationCuantity();
    PaintQuotationPrice();
    RemoveActionModule(7);
    //GetImageGrafic();
})

function Events() {
    $("#personLst").change(GetDataTableQuotation);
    $("#filterDate").click(ValidateForDate);
    $("#descargar").click(function() { downloadCanvas("quotationCuantity") });
    $("#descargar2").click(function() { downloadCanvas("quotationMonto") });
    //ValidateDate();
}

//trae los usuarios del roladmin
function GetDataPerson() {

    var parameters = {
        "isActionFromAjax": true,
        "optionInputForm": 5,
        "idDataInputForm": 0
    };
    $.ajax ({
        url: "view/html/modules/reportquotation/AjaxDataReportQuotation.php",
        type: 'post',
        data: parameters,

        success: function(data) {
            console.log(data);
            data = JSON.parse(data);

            SetDataPerson(data);
        }
    });

}
//array que guarda todos los usuarios
function SetDataPerson(data) {
    dataPerson = data;

}

function downloadCanvas(name) {
    $("#" + name).get(0).toBlob(function(blob) {
        saveAs(blob, "cotizaciones.png");
    });
}

function ValidateForDate() {
    if ($("#min").val() != "" && $("#max").val() != "" && $("#min").val() <= $("#max").val()) {
        dateMin = $("#min").val();
        dateMax = $("#max").val();
        LoadTableQuotationForDate(dateMin, dateMax);
    }
}

function LoadTableQuotationForDate(min, max) {
    tbl.destroy();
    var parameters = {
        "isActionFromAjax": true,
        "optionInputForm": 6,
        "idDataInputForm": 0,
        "min": min,
        "max": max
    };

    tbl = $('#quotationTbl').DataTable({
        responsive: true,
        language: {
            "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
        },
        "ajax": {
            url: "view/html/modules/reportquotation/AjaxDataReportQuotation.php",
            method: "POST",
            data: parameters
        }
    });
}

function GetDataTableQuotation() {
    tbl.destroy();
    if ($("#personLst option:selected").val() != "") {
        $("#personLst option:selected").each(function() {
            personId = $("#personLst").val();
        });

        var parameters = {
            "isActionFromAjax": true,
            "optionInputForm": 2,
            "idDataInputForm": 0,
            "idUser": personId
        };

        console.log(parameters);

        tbl = $('#quotationTbl').DataTable({
            responsive: true,
            language: {
                "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
            },
            "ajax": {
                url: "view/html/modules/reportquotation/AjaxDataReportQuotation.php",
                method: "POST",
                data: parameters
            }
        });
    } else {
        LoadTableQuotation();
    }
}

function GetImageGrafic() {
    var url_base64 = document.getElementById('quotationCuantity').toDataURL('image/png');
    console.log(url_base64);

}

function PaintQuotationCuantity() {

    var parameters = {
        "isActionFromAjax": true,
        "optionInputForm": 4,
        "idDataInputForm": 0
    };
    $.ajax ({
        url: "view/html/modules/reportquotation/AjaxDataReportQuotation.php",
        type: 'post',
        data: parameters,

        success: function(data) {
            data = JSON.parse(data);
            dataX = [];

            for (var d = 0; d < dataPerson.length; d++) {
                cont = 0;
                for (var i = 0; i < data.length; i++) {
                    if (dataPerson[d]["id"] == data[i].idUser) {
                        dataX[d] = cont += 1;
                    }
                }
            }

            dataPerson2 = transformPerson(dataPerson);
            if (dataX.length < dataPerson2.length) {
                dataX.length = dataPerson2.length;
            }
            for (var i = 0; i < dataPerson2.length; i++) {
                if (dataX[i] == null || dataX[i] == "") {
                    dataX[i] = 0;
                }
                dataPerson2[i] = dataPerson2[i] + "-" + dataX[i] + " Cotizaciones";
            }
            dataX = ConvertPorcent(dataX, data.length);

            paintGrafic("quotationCuantity", dataPerson2, dataX);
        }
    });

}

function transformPerson(data) {
    miArray = [];
    for (var i = 0; i < data.length; i++) {
        miArray.push(data[i]["name"]);
    }
    return miArray;
}

function ConvertPorcent(dataX, cienporciento) {
    for (var r = 0; r < dataX.length; r++) {
        if (dataX[r] == "" || dataX[r] == null) {
            dataX[r] = 0;
        }

        porcentaje = ((dataX[r] * 100) / cienporciento);
        porcentaje = parseInt(porcentaje);
        console.log(porcentaje);
        dataX[r] = porcentaje;
    }
    return dataX;
}

function GetTypePrice(price, type, convert) {

    if (type == "USD") {
        if (convert == "COP") {
            price = Math.round(parseFloat(price) * 3000);

            price = Math.round(price);
        } else if (convert == "EUR") {
            price = Math.round(parseFloat(price) * 0.88);
            price = Math.round(price);
        }

    } else if (type == "COP") {
        if (convert == "USD") {
            price = Math.round(parseFloat(price) / 3000);
            price = Math.round(price);

        } else if (convert == "EUR") {
            price = Math.round(parseFloat(price) / 3300);
            price = Math.round(price);
        }

    } else {
        if (convert == "USD") {
            price = Math.round(parseFloat(price) / 0.88);
            price = Math.round(price);
        } else if (convert == "COP") {
            price = Math.round(parseFloat(price) * 3300);
            price = Math.round(price);

        }
    }
    return price;
}

function PaintQuotationPrice() {

    var parameters = {
        "isActionFromAjax": true,
        "optionInputForm": 4,
        "idDataInputForm": 0
    };
    $.ajax ({
        url: "view/html/modules/reportquotation/AjaxDataReportQuotation.php",
        type: 'post',
        data: parameters,

        success: function(data) {
            dataMonto = [];
            data = JSON.parse(data);

            for (var d = 0; d < dataPerson.length; d++) {
                total = 0;
                for (var i = 0; i < data.length; i++) {
                    if (dataPerson[d]["id"] == data[i]["idUser"]) {
                        dataMonto[d] = total += parseFloat(GetTypePrice(data[i]["total"], data[i]["typeMoney"], "COP"));
                    }
                }
            }

            dataPerson2 = transformPerson(dataPerson);
            paintLine("quotationMonto", dataPerson2, dataMonto);
        }
    });

}

function paintLine(name, x, y) {
    new Chart(document.getElementById(name), {
        type: 'bar',
        data: {
            labels: x,
            datasets: [{
                label: "Total (millions)",
                backgroundColor: ["#3e95cd", "#c45850", "#9C44FF", "#E2FC0B", "#26D8D6", "#E348C0", "#29B420", "#32EDBF", "#5944F6", "#F42FE4", "#E5F52D", "#9F3F15", "#28904A", "#0685FB"],
                data: y
            }]
        },
        options: {
            legend: { display: false },
            title: {
                display: true,
                text: 'Cotizaciones por monto (Pesos Colombianos)'
            }
        }
    });
}

function paintGrafic(name, x, y) {
    new Chart(document.getElementById(name), {
        type: 'pie',
        data: {
            labels: x,
            datasets: [{
                label: "pocentaje",
                backgroundColor: ["#3e95cd", "#c45850", "#9C44FF", "#E2FC0B", "#26D8D6", "#E348C0", "#29B420", "#32EDBF", "#5944F6", "#F42FE4", "#E5F52D", "#9F3F15", "#28904A", "#0685FB"],
                data: y
            }]
        },
        options: {
            legend: {
                display: true,

            },
            title: {
                display: true,
                text: ''
            }
        }
    });

}

function LoadTableQuotation() {

    var parameters = {
        "isActionFromAjax": true,
        "optionInputForm": 1,
        "idDataInputForm": 0
    };


    tbl = $('#quotationTbl').DataTable({
        responsive: true,
        "drawCallback": function(settings) {
            $('[data-toggle="tooltip"]').tooltip()
        },
        language: {
            "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
        },
        "ajax": {
            url: "view/html/modules/reportquotation/AjaxDataReportQuotation.php",
            method: "POST",
            data: parameters
        }
    });
}

//__________________________________EXCEL_________________________________




function CreateExcelDocs() {
    GetAllDataTableToExcel("ReportQuotation", "view/docs/excel/ReportsQuotation.xlsx", "quotationTbl");
}