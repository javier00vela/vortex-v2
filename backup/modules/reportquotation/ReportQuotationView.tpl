<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Reporte de cotizaciones</b>
        </h1>
        <ol class="breadcrumb">
            <li><a href="Home"><i class="fa fa-dashboard"></i> Inicio</a></li>
            <li class="active">Reportes</li>
            <li class="active">Cotizaciones</li>
        </ol>
    </section>
    <div class="container">

    </div>
    <section class="content">
        <div class="row">
            <div class="col-md-6">
                <!-- AREA CHART -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Cotizaciones por cantidad</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>

                        </div>
                        <div class="box-body">

                            <div class="chart">
                                <canvas id="quotationCuantity" style="height:250px"></canvas>
                                <a id="descargar">Exportar Imagen</a>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <!-- LINE CHART -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">cotizaciones por monto</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>

                        </div>
                        <div class="box-body">
                            <div class="chart">
                                <canvas id="quotationMonto" style="height:250px"></canvas>
                                <a id="descargar2">Exportar Imagen</a>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
            </div>

    </section>
    <section class="content">

        <div class="row">
            <section class="col-md-12">
                <div class="box">
                    <div class="container-fluid">
                        <div class="row text-center">
                            <div class="col-md-4">
                                <div class="form-group ">
                                    <label>Personas</label>
                                    <div name="quotationTimeContainer" class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-user-circle-o"></i></span> {$personLst}
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group ">
                                    <label for="">Fecha Inicio:</label>
                                    <div name="quotationTimeContainer" class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-history"></i></span>
                                        <input type="date" id="min" class="form-control" name="">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group ">
                                    <label for="">Fecha Final:</label>
                                    <div name="quotationTimeContainer" class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-history"></i></span>
                                        <input type="date" id="max" class="form-control" name="">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group ">
                                    <label for="">Opciones:</label><br>
                                    <a id="filterDate" title="buscar" class="btn btn-warning"><i class="fa fa-search"></i> </a>
                                    <button class="btn btn-success" title="Generar Excel" node="node-generar_excel" onclick=" CreateExcelDocs()"><i class="fa fa-file-excel-o"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-body">
                        <table id="quotationTbl" name="quotationTbl" class="table table-bordered table-striped dt-responsive tables dataTable no-footer dtr-inline" width="100%" cellspacing="0">
                            <thead>
                                <th>Id</th>
                                <th>Código</th>
                                <th>Versión</th>
                                <th>Responsable</th>
                                <th>Fecha de Creación</th>
                                <th>Estado</th>
                                <th>Tipo de cotización</th>
                                <th>Tipo de Moneda</th>
                                <th>Total</th>

                            </thead>
                        </table>
                    </div>
                </div>
            </section>
        </div>
    </section>
    </div>
    <div id="containerResponseAjax"></div>
    <script src="view/html/modules/reportquotation/ReportQuotation.js?v= {$tempParameter}"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.1.4/Chart.bundle.min.js"></script>
    <script type="text/javascript" src="https://fastcdn.org/FileSaver.js/1.1.20151003/FileSaver.js"></script>
    <script type="text/javascript" src="https://cdn.rawgit.com/eligrey/canvas-toBlob.js/master/canvas-toBlob.js"></script>
    {$jquery}