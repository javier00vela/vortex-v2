<?php
require_once(__DIR__ . '/../../../../controller/general/Config.php');
require_once(Config::PATH . Config::CONTROLLER.Config::GENERAL . 'ManageArrays.php');
require_once(Config::PATH . Config::BACKEND . 'modules/PercentCodesVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/PersonVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/ProductVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/QuotationVo.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralDao.php');
require_once(Config::PATH . Config::CONTROLLER . 'general/PHPMailer.php');

class AjaxDataManagers{

    private $isActionFromAjax;
    private $optionInputForm;
    private $idDataInputForm;

    function __construct()
    {
        if (isset($_POST['isActionFromAjax'])) {
            $this->isActionFromAjax = $_POST['isActionFromAjax'];
            $this->optionInputForm = $_POST['optionInputForm'];
            $this->idDataInputForm = $_POST['idDataInputForm'];

            if ($this->isActionFromAjax == "true") {
                if ($this->optionInputForm == 3) {
                    $this->SetDataInControls();
                }else if($this->optionInputForm == 5){
                $this->GetLengthImpost();
                }else if($this->optionInputForm == 7){
                $this->UpdateDataPercent();
            }else if($this->optionInputForm == 9){
                $this->AbleQuotation();
                }else {
                    $this->GetDataInJsonForTbl();
                }
            }
        }
    }

    private function SetDataInControls(){
        $SetInputsDataFJQ = "";
        $PercentCodesVo = new PercentCodesVo();
        $PercentCodesVo->id = $this->idDataInputForm;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $PercentCodesVo);
        if($PercentCodesVo2 = $generalDao->GetVo($generalVo = clone $PercentCodesVo)){
            $SetInputsDataFJQ = "
                            <script>
                               (function($){
                                SetValueToInputText('codeCdx','{$PercentCodesVo2->code}');
                               })(jQuery);
                            </script>
                        ";
        }
        echo $SetInputsDataFJQ;
    }

    public function GetDataInJsonForTbl()
    {
        $existData = false;
        $ProductVo = new ProductVo();
        $ProductVo->idSearchField = 16;
        $ProductVo->idQuotation = $_POST["id"];
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $ProductVo);

        $string = "
        {\"data\": [";
        $cont = 1;
        while ($ProductVo2 = $generalDao->GetVo($generalVo = clone $ProductVo)) {
            if($ProductVo2->percent < 20){
            $existData = true;
            $json = array();
           
            $json[] = $ProductVo2->idProduct;
            $json[] = $ProductVo2->typeProduct;
            $json[] = $ProductVo2->name;
            $json[] = $ProductVo2->brand;
            $json[] = $ProductVo2->reference;
            $json[] = $ProductVo2->presentation;
            $json[] = "$".str_replace(",",".",number_format($ProductVo2->priceUnit));
            $json[] = $ProductVo2->quantity;
            $json[] = "$".str_replace(",",".",number_format( (($ProductVo2->priceUnit * ($ProductVo2->percent / 100) ) + $ProductVo2->priceUnit)  ));
            $json[] = $ProductVo2->nameCompany;
            $json[] = $ProductVo2->timeDelivery;
            $json[] = $ProductVo2->percent;
            $json[] = '<input id="toggle-demo" data-id="'.$ProductVo2->id.'" type="checkbox" checked data-toggle="toggle" data-on="Descuento" data-off="Sin Descuento" data-onstyle="success" data-offstyle="danger" data="'.$ProductVo2->percent.','.$ProductVo2->id.','.$ProductVo2->priceUnit.'" >';
           // $json[] = '<div class="toggle btn btn-success" data-toggle="toggle" style="width: 150.514px; height: 46.8264px;"><input id="toggle-demo" type="checkbox" checked="" data-toggle="toggle" data-on="Con Descuento" data-off="Sin Descuento" data-onstyle="success" data-offstyle="danger"  data="'.$ProductVo2->percent.','.$ProductVo2->id.','.$ProductVo2->priceUnit.'"><div class="toggle-group"><label class="btn btn-success toggle-on">Habilitado</label><label class="btn btn-danger active toggle-off">InHabilitado</label><span class="toggle-handle btn btn-default"></span></div></div>';

            $string .= json_encode($json) . ",";
            }
        }
        $string .= "-]}-";

        if($existData){
            echo  str_replace(",-]}-", "]}", $string);
        }else{
            echo "{ \"data\": [] }";
        }
    }

    public function UpdateDataPercent(){
        $productVo = new ProductVo();
        $productVo->id = $_POST["id"];
        $productVo->idSearchField = 0;
        $productVo->percent = $_POST["percent"];
        $productVo->idUpdateField = 14;
        $generalDao = new GeneralDao();
        print($productVo->percent."---");
        $generalDao->UpdateByField($generalVo = clone $productVo);
        //$this->UpdatePricePercent($_POST["prepercent"]);
    }

    public function UpdatePricePercent($percentClasic){
        $priceTotal = $_POST["price"] + ($_POST["price"] * ($_POST["percent"] / 100 )) ;
        print_r($priceTotal);
        $productVo = new ProductVo();
        $productVo->id = $_POST["id"];
        $productVo->idSearchField = 0;
        $productVo->priceUnit = $priceTotal;
        $productVo->idUpdateField = 10;
        $generalDao = new GeneralDao();
        $generalDao->UpdateByField($generalVo = clone $productVo);
    }

    public function AbleQuotation(){
        $QuotationVo = new QuotationVo();
        $QuotationVo->id = $_POST["id"];
        $QuotationVo->idSearchField = 0;
        $QuotationVo->checkeable = 0;
        $QuotationVo->idUpdateField = 16;
        $generalDao = new GeneralDao();
        $generalDao->UpdateByField($generalVo = clone $QuotationVo);
        $this->SendMailData();
        if(empty($_POST["onlyAction"])){
            $this->ChangeTotalQuotes();
        }
    }

    private function SendMailData(){
        $dataMail =  array('mail' => "sysvortex@arvack.com", 'password' => "sysvortex153",'domain' => "mail.arvack.com", 'port' => 25 , "from" => "sysvortex@arvack.com");
        $PHPMailerUtils = new PHPMailerUtils($dataMail);
        print($_POST["mail"]);
         $PHPMailerUtils->AddDestinatario($_POST["mail"]);

        $subject = "Cotización #".$_POST["id"]." Confirmación!";
        $contenido = '<style type="text/css">body, html,  .body {   background: #f3f3f3 !important; }</style><spacer size="16"></spacer>
                           <container>
                             <row>
                               <columns large="4" style="backgroung:black">
                                 <center>
                                   <img src="https://www.tescan.com/getattachment/beaaaee6-fa88-449d-9027-4f22f7e80758/vortex-company.aspx">
                                 </center>
                               </columns>

                               <columns large="8">
                                 <h1 style="text-align: center;">Cotización Confirmada!</h1>
                               </columns>
                             </row>
                             <spacer size="16"></spacer>

                             <row>
                               <columns>
                                 <h3 class="text-center">Se ha Confirmado la cotización #'.$_POST["id"].' en la fecha '.date("Y-m-d H:i:s").' y esta lista para ser visualizada y/o actualizada desde este momento! </h3><br><ul>
                                 ';
    
                                 $contenido .= '    
                               </ul></columns>
                             </row>

                             <spacer size="16"></spacer>

                             <spacer size="16"></spacer>

                           </container>';


        $PHPMailerUtils->SendDataMail($subject , $contenido , "");
   }

    public function ChangeTotalQuotes(){
        $QuotationVo = new QuotationVo();
        $QuotationVo->id = $_POST["id"];
        $QuotationVo->idSearchField = 0;
        $QuotationVo->total = $this->CountProductsTotal();;
        $QuotationVo->idUpdateField = 5;
        $generalDao = new GeneralDao();
        $generalDao->UpdateByField($generalVo = clone $QuotationVo);
    }

    public function CountProductsTotal(){
        $productVo = new ProductVo();
        $generalDao = new GeneralDao();
        $productVo->idSearchField=16;
        $productVo->idQuotation= $_POST["id"];
        $total=0;
        $generalDao->GetByField($generalVo = clone $productVo);
        while( $productVo2 = $generalDao->GetVo($generalVo = clone $productVo)){
            
            $productVo2->priceUnit = (($productVo2->priceUnit*$productVo2->percent/100)+$productVo2->priceUnit);
            $total += $productVo2->priceUnit;       
            print_r($total);
        }
        return $total;
    }

       public function GetLengthImpost(){
        $arrayList = array();
        $personVo = new impostVo();
        $personVo->isList = true;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $personVo);
        while ($personVo2 = $generalDao->GetVo($generalVo = clone $personVo)) {
            $arrayList[] =$personVo2 ;
        }
        print_r( json_encode(count($arrayList)) );
    }


    
}

$ajaxDataImpost = new AjaxDataManagers();
