<?php
require_once(Config::PATH . Config::CONTROLLER . 'general/AccessManager.php');
require_once(Config::PATH . Config::CONTROLLER . 'general/UtilJquery.php');
require_once(Config::PATH . Config::VIEW_HTML . 'general/comps/GeneralCtx.php');
require_once(Config::PATH . Config::VIEW_HTML . 'general/comps/GeneralLst.php');
require_once(Config::PATH . Config::VIEW_HTML . 'general/comps/GeneralWithDataLst.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralDao.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/CompanyVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/PricesCompanyVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/RoleCompanyVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/InventoryVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/HistoryTRMVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/AditionalsImpostVo.php');
require_once(Config::PATH . Config::CONTROLLER . 'general/ExcelManager.php');
require_once(Config::PATH . Config::CONTROLLER_LIB . 'vendor/phpoffice/phpexcel/Classes/PHPExcel/IOFactory.php');

class CompanyView
{
    private $optionInputForm;
    private $isUpdateUserForm;
    private $idDataInputForm;
    /*=============================================
                  Session Vars
    =============================================*/
    private $manageSession;
    /*=============================================
                  Template Vars
    =============================================*/
    private $utilJQ;
    private $pageTpl, $data, $core;
    /*=============================================
                Controls Vars HTML
    =============================================*/
    private $companyNameCtx;
    private $companyNameHiddenCtx;
    private $companyNitCtx;
    private $companyPhoneCtx;
    private $companyEmailCtx;
    private $companyWebSiteCtx;
    private $companyAddressCtx;
    private $companyCountryLst;
    private $companyCityLst;
    private $companyIdRoleLst;
    /*=============================================
                Data Vars
    =============================================*/
    private $generalDao;
     private $nameColumns =  array("id","Nombre","Nit","Telefono","Email creación","Sitio","Dirección","Pais","Ciudad","Fecha","Rol");


    function __construct($pageTpl, $data, $core)
    {
        $this->data = $data;
        $this->core = $core;
        $this->pageTpl = $pageTpl;
        $this->CreateComponents();
        $this->UseDataFromThePostback();
         $this->CreateReportExcel();
        
    }

    private function UseDataFromThePostback()
    {
        if (isset($_POST['optionInputForm'])) {
            $this->optionInputForm = $_POST['optionInputForm'];
            $this->idDataInputForm = $_POST['idDataInputForm'];
            $this->generalDao = new GeneralDao();
            switch ($this->optionInputForm) {
                case "1": {
                    $this->SetData();
                    break;
                }
                case "2": {
                    $this->DeleteData();
                    break;
                }
                case "4": {
                    $this->SetUpdate();
                    break;
                }
            }
        }
        if (isset($_POST['isUpdateUserForm'])) {
            $this->isUpdateUserForm = $_POST['isUpdateUserForm'];
            if($this->isUpdateUserForm == "1"){
                $this->UpdateUser();
            }
        }
        if (isset($_POST['optionInputTemplateForm'])) {
             $this->generalDao = new GeneralDao();
            $this->optionInputForm = $_POST['optionInputTemplateForm'];
           
            switch ($this->optionInputForm) {
                case 6: {
                    $this->GetFileToRead();
                    break;
                }
            }
        }
        $this->AssignDataToTpl();

    }

    private function CreateReportExcel(){
    if(isset($_POST["data"])){
      ExcelManager::CreateReportExcel($_POST["data"] , $this->nameColumns , "ReportCompany" );
  }
}


    private function CreateComponents()
    {
        $this->companyNameCtx = new GeneralCtx("companyNameCtx","Nombre",null,true);
         $this->companyNameHiddenCtx = new GeneralCtx("companyNameHiddenCtx","Nombre","hidden",true);
        $this->companyNitCtx = new GeneralCtx("companyNitCtx","Nit",null,true);
        $this->companyPhoneCtx = new GeneralCtx("companyPhoneCtx","Telefono",null,true);
        $this->companyEmailCtx = new GeneralCtx("companyEmailCtx","Email",null,true);
        $this->companyWebSiteCtx = new GeneralCtx("companyWebSiteCtx","Sitio Web",null,true);
        $this->companyAddressCtx = new GeneralCtx("companyAddressCtx","Direccion",null,true);
        $dataCompanyCountryLst = array();
        $this->companyCountryLst = new GeneralWithDataLst($dataCompanyCountryLst, "companyCountryLst", false,false,"-- País --",true);
        $this->companyCountryLst->lst->adiPropiedad("style","width: 100%;");
        $dataCompanyCityCLst = array();
        $this->companyCityLst = new GeneralWithDataLst($dataCompanyCityCLst, "companyCityLst", false,false,"-- Ciudad --",true);
        $this->companyCityLst->lst->adiPropiedad("style","width: 100%;");

        $roleCompanyVo = new RoleCompanyVo();
        $roleCompanyVo->isList = true;
        $this->companyIdRoleLst = new GeneralLst($generalVo = clone $roleCompanyVo,"companyIdRoleLst", 0, 1, "--Compañia--",true);

        $this->utilJQ = new UtilJquery("Users");
    }

    private function SetDataToVo()
    {
        $companyVo = new CompanyVo();
        $companyVo->id = $this->idDataInputForm;
        $companyVo->name = $_POST["companyNameCtx"];
        $companyVo->nit = $_POST["companyNitCtx"];
        $companyVo->phone = $_POST["companyPhoneCtx"];
        $companyVo->email = $_POST["companyEmailCtx"];
        $companyVo->webSite = $_POST["companyWebSiteCtx"];
        $companyVo->address = $_POST["companyAddressCtx"];
        $companyVo->country = $_POST["companyCountryLst"];
        $companyVo->city = $_POST["companyCityLst"];
        $companyVo->creationDate = date('Y-m-d H:i:s');
        $companyVo->state = 1;
        $companyVo->idRoleCompany = $_POST["companyIdRoleLst"];

        return $companyVo;
    }

    private function SetData()
    {
        $companyVo = $this->SetDataToVo();
        $companyVo->id = null;
        $result = $this->generalDao->Set($generalVo = clone $companyVo);
        if (isset($result)) {
            $this->AddDefaultDataToPrices($companyVo , $result);
            $message = 'MessageSwalBasic("Registrado!","Empresa Registrada Correctamente","success");';
            $this->utilJQ->AddFunctionJavaScript($message);
        }

    }

    private function AddDefaultDataToPrices($companyVo , $id){
        if($companyVo->idRoleCompany == 3){
            $pricesCompanyVo = $this->SetDataPricesCompany($id);
            $result = $this->generalDao->Set($generalVo = clone $pricesCompanyVo);
            $this->CreateImpostCompany($result);
        }
    }

    private  function CreateImpostCompany($idPrice){
        $pricesCompanyVo = $this->SetDataPricesImpost($idPrice);
        $result = $this->generalDao->Set($generalVo = clone $pricesCompanyVo);
    }

    private function SetDataPricesImpost($idCompany){
        $AditionalsImpostVo = new AditionalsImpostVo();
        $AditionalsImpostVo->name = '';
        $AditionalsImpostVo->percent = null;
        $AditionalsImpostVo->idPricesCompany = $idCompany;
        $AditionalsImpostVo->CIF = 0;
        $AditionalsImpostVo->FCA = 0;
        $AditionalsImpostVo->DDP = 0;
        return $AditionalsImpostVo;

    }


    private function SetDataPricesCompany($idCompany){
        $pricesCompanyVo = new pricesCompanyVo();
        $pricesCompanyVo->id = null;
        $pricesCompanyVo->typeMoney = "USD";
        $pricesCompanyVo->iva = 0;
        $pricesCompanyVo->tpm = $this->GetTasa("USD");
        $pricesCompanyVo->ganancia = 0;

        $pricesCompanyVo->idCompany = $idCompany;
        return $pricesCompanyVo;
    }

    public function GetTasa($field){
        $arrayList = array();
        $HistoryTRMVo = new HistoryTRMVo();
        $HistoryTRMVo->isList = true;
        $generalDao = new GeneralDao();
        $generalDao->CustomQuery("SELECT * FROM {$HistoryTRMVo->nameTable} WHERE DATE(DATE) >= CURDATE() AND typeMoney='{$field}' ORDER BY DATE DESC , id desc LIMIT 1");
        while($HistoryTRMVo2 = $generalDao->GetVo($generalVo = clone $HistoryTRMVo)) {
          return $HistoryTRMVo2->money;
        }   
        return 0;
}

    private function SetUpdate()
    {
        $companyVo = $this->SetDataToVo();
        $result = $this->generalDao->Update($generalVo = clone $companyVo);
        if (isset($result)) {
            $this->UpdateNameProduct();
            $this->AddDefaultDataToPrices($companyVo , $this->idDataInputForm);
            $message = 'MessageSwalBasic("Modificado!","Empresa Modificado Correctamente","success");';
            $this->utilJQ->AddFunctionJavaScript($message);
        }
    }

    

    private function UpdateNameProduct()
    {
        $InventoryVo = new InventoryVo();
        $InventoryVo3 = new InventoryVo();
        $InventoryVo->idSearchField = 11;
        $InventoryVo->nameCompany = $_POST["companyNameHiddenCtx"];
        $generalDao = new GeneralDao();
        $generalDao2 = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $InventoryVo);
        while($InventoryVo2 = $generalDao->GetVo($generalVo = clone $InventoryVo)) {
            $InventoryVo3->id = $InventoryVo2->id;
            $InventoryVo3->idSearchField = 0;
            $InventoryVo3->nameCompany = $_POST["companyNameCtx"];
            $InventoryVo3->idUpdateField = 11;
            $generalDao2->UpdateByField($generalVo = clone $InventoryVo3);
        }   
    }




    private function DeleteData()
    {
        $message = "";
        $companyVo = new CompanyVo();
        $companyVo->id = $this->idDataInputForm;
        $companyVo->idUpdateField = 11;
        $companyVo->isList = false;
        $companyVo->state = 0;
        $companyVo->idSearchField = 0;
        $this->DeleteByNameProduct($this->GetNameCompany($companyVo->id));
         $this->DeletePersonById($companyVo->id);
        if ($this->generalDao->UpdateByField($generalVo = clone $companyVo)) {
        }
            $message = 'MessageSwalBasic("Eliminado!","Registro Eliminado Correctamente","success");';
       
        $this->utilJQ->AddFunctionJavaScript($message);
    }


    private  function GetNameCompany($company)
    {

        $companyVo = new CompanyVo();
        $companyVo->idSearchField = 0;
        $companyVo->id = $company;
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $companyVo);
        if ($companyVo2 = $generalDao->GetVo($generalVo = clone $companyVo)) {
            return $companyVo2->name;
        }
        return false;
    }

    private  function DeleteByNameProduct($company)
    {

        $InventoryVo = new InventoryVo();
        $InventoryVo->idSearchField = 11;
        $InventoryVo->nameCompany = $company;
        $generalDao = new GeneralDao();
        $generalDao->DeleteByField($generalVo = clone $InventoryVo);
    }

     private  function DeletePersonById($company)
    {

        $PersonVo = new PersonVo();
        $PersonVo3 = new PersonVo();
        $generalDao = new GeneralDao();
        $generalDao2 = new GeneralDao();
        $PersonVo->idSearchField = 12;
        $PersonVo->idCompany = $company;
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $PersonVo);
        while ($PersonVo2 = $generalDao->GetVo($generalVo = clone $PersonVo)) {
            $PersonVo3->id = $PersonVo2->id;
            $PersonVo3->idSearchField = 0;
            $PersonVo3->idUpdateField = 13;
            $PersonVo3->state = 0;
        $generalDao2->UpdateByField($generalVo = clone $PersonVo3);
        }
    }


     public function GetFileToRead(){
        if(isset($_FILES["fileExcelTemplateInp"])){
            $fileExplode = explode(".", $_FILES["fileExcelTemplateInp"]["name"]);
            if($fileExplode[1] == "xlsx" ){
                $fileExcel = $_FILES["fileExcelTemplateInp"]["tmp_name"];
                $this->LoadExcelFile($fileExcel);
            }else{
                $message = '<script> MessageSwalBasic("ERROR!","Verifique que el formato que ha ingresado sea de tipo excel","error"); </script>';
                echo $message;
            }
        }
    }
    public function LoadExcelFile($fileExcel){
        if(isset($fileExcel)){
            $coreExcel = $this->LoadDataExcelFile($fileExcel);
            $numRows = $this->CountCellsExcel($coreExcel);
            $this->SetDataExcelToVo($coreExcel,$numRows);
        }
    }

    public function LoadDataExcelFile($file){
        $fileXlsx = PHPEXCEL_IOFactory::load($file);
        return $fileXlsx;
    }

    public function CountCellsExcel($file){
        $file->setActiveSheetIndex(0);
        $numRows = $file->setActiveSheetIndex(0)->getHighestRow();
        return $numRows;
    }


    private function SetDataExcelToVo($fileXlsx,$numRows)
    {
        $companyVo = new CompanyVo();
        for ($i=2; $i<$numRows+1; $i++) {
            $companyVo->name = htmlspecialchars($fileXlsx->getActiveSheet()->getCell('A'.$i)->getCalculatedValue());
            $companyVo->nit = htmlspecialchars($fileXlsx->getActiveSheet()->getCell('B'.$i)->getCalculatedValue());
             $companyVo->phone = htmlspecialchars($fileXlsx->getActiveSheet()->getCell('C'.$i)->getCalculatedValue());
            $companyVo->email = htmlspecialchars($fileXlsx->getActiveSheet()->getCell('D'.$i)->getCalculatedValue());
             $companyVo->webSite = htmlspecialchars($fileXlsx->getActiveSheet()->getCell('E'.$i)->getCalculatedValue());
            $companyVo->address = htmlspecialchars($fileXlsx->getActiveSheet()->getCell('F'.$i)->getCalculatedValue());
             $companyVo->country = htmlspecialchars($fileXlsx->getActiveSheet()->getCell('G'.$i)->getCalculatedValue());
            $companyVo->city = htmlspecialchars($fileXlsx->getActiveSheet()->getCell('H'.$i)->getCalculatedValue());
             $companyVo->creationDate = date("Y-m-d H:i:s");
             $companyVo->state = 1;
            $companyVo->idRoleCompany = $this->ValidateCompanyState($fileXlsx->getActiveSheet()->getCell('I'.$i)->getCalculatedValue());
        
            $this->InsertDataExcel($numRows,$i,$companyVo);
        }
    }

    public function InsertDataExcel($numRows,$i,$companyVo){
        $generalDao = new GeneralDao();
        $result = $generalDao->Set($generalVo = clone $companyVo);
        $this->ValidateIfIsProveedor($companyVo , $result);
        if($i >= $numRows){

            $message = '<script> MessageSwalBasic("Insertado!","Se ha agregado la plantilla Excel Correctamente, Numero de registros: "+'.($numRows-1).',"success"); </script>';
            echo $message;
        }
    }

      private function ValidateCompanyState($value){
        $value = strtolower($value);
        if($value == "prospecto"){
         $value = "Prospecto";
        }else if($value == "proveedor" ){
            $value = "Proveedor";
        }else{
            $value = "Cliente";
        }
        $value = $this->GetCompanyRol($value);

        return  $value;
    }


    private function ValidateIfIsProveedor($companyVo , $id){
        if($companyVo->idRoleCompany == 3){
            $pricesCompanyVo = $this->SetDataPricesCompanyExcel($id);
            $result = $this->generalDao->Set($generalVo = clone $pricesCompanyVo);
        }
    }


    private function SetDataPricesCompanyExcel($idCompany){
        $pricesCompanyVo = new pricesCompanyVo();
        $pricesCompanyVo->id = null;
        $pricesCompanyVo->typeMoney = "USD";
        $pricesCompanyVo->iva = 0;
        $pricesCompanyVo->tpm = $this->GetTasa("USD");
        $pricesCompanyVo->ganancia = 0;
        $pricesCompanyVo->idCompany = $idCompany;
        return $pricesCompanyVo;
    }

    private function GetCompanyRol($rol){
        $companyVo = new RoleCompanyVo();
        $companyVo->idSearchField = 1;
        $companyVo->name = $rol;
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $companyVo);
        if ($companyVo2 = $generalDao->GetVo($generalVo = clone $companyVo)) {

            return $companyVo2->id;
        }
        return 2;


    }

    private function AssignDataToTpl()
    {
        $this->data->assign('companyNameCtx', $this->companyNameCtx->paint());
        $this->data->assign('companyNameHiddenCtx', $this->companyNameHiddenCtx->paint());
        $this->data->assign('companyNitCtx', $this->companyNitCtx->paint());
        $this->data->assign('companyPhoneCtx', $this->companyPhoneCtx->paint());
        $this->data->assign('companyEmailCtx', $this->companyEmailCtx->paint());
        $this->data->assign('companyWebSiteCtx', $this->companyWebSiteCtx->paint());
        $this->data->assign('companyAddressCtx', $this->companyAddressCtx->paint());
        $this->data->assign('companyCountryLst', $this->companyCountryLst->paint());
        $this->data->assign('companyCityLst', $this->companyCityLst->paint());
        $this->data->assign('companyIdRoleLst', $this->companyIdRoleLst->paint());
        $this->data->assign('tempParameter', time());

        $this->data->assign('jquery', $this->utilJQ->PaintJQ());
        $this->SetDataToTpl();
    }

    private function SetDataToTpl()
    {
        echo $this->core->get($this->pageTpl, $this->data);
    }

}   