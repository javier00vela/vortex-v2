<?php
require_once(__DIR__ . '/../../../../controller/general/Config.php');
require_once(Config::PATH . Config::CONTROLLER.Config::GENERAL . 'ManageArrays.php');
require_once(Config::PATH . Config::BACKEND . 'modules/PersonCreateQuotationVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/QuotationVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/VisitsVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/VisitsDescriptionVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/PersonVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/ClientUserVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/CompanyVo.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralDao.php');
require_once(Config::PATH . Config::BACKEND . 'modules/StaticEventsVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/CheckedEventsVo.php');


class AjaxDataImpost{

    private $isActionFromAjax;
    private $optionInputForm;
    private $idDataInputForm;
    public $dataList = [];

    function __construct()
    {
        if (isset($_POST['isActionFromAjax'])) {
            $this->isActionFromAjax = $_POST['isActionFromAjax'];
            $this->optionInputForm = $_POST['optionInputForm'];
            $this->idDataInputForm = $_POST['idDataInputForm'];
            if(isset($_POST["listData"])){
                $this->dataList = $_POST["listData"];  
            }
            if ($this->isActionFromAjax == "true") {
                if ($this->optionInputForm == 3) {
                    $this->SetDataInControls();
                }else if($this->optionInputForm == 5){
                    $this->GetArrayToCalendar();
                }else if($this->optionInputForm == 6){
                    $this->GetArrayToCalendarByParameter();
                }else if($this->optionInputForm == 9){
                    $this->UpdateStateWithCalendar();
                }else {
                    $this->GetDataInJsonForTbl();
                }
            }
        }
    }

    private function SetDataInControls(){
        $SetInputsDataFJQ = "";
        $impostVo = new ImpostVo();
        $impostVo->id = $this->idDataInputForm;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $impostVo);
        if($impostVo2 = $generalDao->GetVo($generalVo = clone $impostVo)){
            $SetInputsDataFJQ = "
                            <script>
                               (function($){
                                SetValueToInputText('impostNameCtx','{$impostVo2->name}');
                                SetValueToInputText('impostPercentCtx','{$impostVo2->percent}');
                                SetValueToInputText('optionInputForm',4);
                                SetValueToInputText('idDataInputForm',$impostVo2->id);
                               })(jQuery);
                            </script>
                        ";
        }
        echo $SetInputsDataFJQ;
    }

    public function GetDataInJsonForTbl()
    {
        $existData = false;
        $impostVo = new ImpostVo();
        $impostVo->isList = true;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $impostVo);

        $string = "
        {\"data\": [";
        while ($impostVo2 = $generalDao->GetVo($generalVo = clone $impostVo)) {
            $existData = true;
            $json = array();
           // $json[] = $impostVo2->id;
            $json[] = htmlspecialchars(ucfirst(strtolower($impostVo2->name)));
            $json[] = $impostVo2->percent;
            $button = "";
            if(ucfirst(strtolower($impostVo2->name)) != "Iva"){
                if(!in_array( "modificar_impuesto" , $this->dataList)){
                $button .= "<tip title='Modificar' class='yellow-tooltip' data-toggle='tooltip' data-placement='top'><button id='updateBtn' name='updateBtn' class='btn btn-warning btn-xs' data-toggle='modal' data-target='#impostPopUpAdd' title='Modificar' onclick='UpdateData({$impostVo2->id},\"impost\",\"impost\");'><i class='fa fa-pencil'></i></button> </tip>";
                }
                if(!in_array( "eliminar_costo" , $this->dataList)){
                $button .= " <tip title='Eliminar' class='red-tooltip' data-toggle='tooltip' data-placement='top'><a id='deleteBtn' name='deleteBtn' class='btn btn-danger  btn-xs' title='Eliminar' onclick='DeleteData({$impostVo2->id},\"impost\")'><i class='fa fa-times'></i></a></tip> ";
                }
            }else{
                if(!in_array( "modificar_impuesto" , $this->dataList)){
                     $button .= "<tip title='Modificar' class='yellow-tooltip' data-toggle='tooltip' data-placement='top'><button id='updateBtn' name='updateBtn' class='btn btn-warning btn-xs' data-toggle='modal' data-target='#impostPopUpAdd' title='Modificar' onclick='UpdateData({$impostVo2->id},\"impost\",\"impost\");'><i class='fa fa-pencil'></i></button> </tip>";
                }
            }
                $json[] = $button;

            $string .= json_encode($json) . ",";

        }
        $string .= "-]}-";

        if($existData){
            echo  str_replace(",-]}-", "]}", $string);
        }else{
            echo "{ \"data\": [] }";
        }
    }

    public function GetQuoationSend($idUser){
     
    $DataQuotes = ["Quotation" => [] , "Visits" => [] , "Prospects" => [] , "Estaticos" => []];
    $DataQuotes["Quotation"] = $this->GetDataQuote($_POST["idPerson"]); 
    $DataQuotes["Visits"] = $this->GetVisitByPerson($_POST["idPerson"]); 
    $DataQuotes["Prospects"] = $this->GetProspectsByPerson($_POST["idPerson"]); 
    $DataQuotes["Estaticos"] = $this->GetStaticByPerson($_POST["idPerson"]); 
       return $DataQuotes;
    }

    public function GetQuoationSendByParameter($idUser){
       
        $DataQuotes = ["Quotation" => [] , "Visits" => [] , "Prospects" => [], "Estaticos" => []];
        if($_POST["parameter"] == "Quotes"){
            $DataQuotes["Quotation"] = $this->GetDataQuote($_POST["idPerson"]); 
        }
        if($_POST["parameter"] == "Visits"){
            $DataQuotes["Visits"] = $this->GetVisitByPerson($_POST["idPerson"]); 
        }
        if($_POST["parameter"] == "Prospects"){
            $DataQuotes["Prospects"] = $this->GetProspectsByPerson($_POST["idPerson"]);
        } 

        if($_POST["parameter"] == "Estaticos"){
            $DataQuotes["Estaticos"] = $this->GetStaticByPerson($_POST["idPerson"]);
        } 
           return $DataQuotes;
        }

        public function GetStaticByPerson($id){
            $arrayList = array();
            $ClientUserVo = new StaticEventsVo();
            $ClientUserVo->idUser = $id;
            $ClientUserVo->idSearchField = 3;
            $generalDao = new GeneralDao();
            $generalDao->GetByField($generalVo = clone $ClientUserVo);
            while($ClientUserVo2 = $generalDao->GetVo($generalVo = clone $ClientUserVo)) {
                
                $person = $this->GetPerson($ClientUserVo2->idUser);
            
                if(isset($person)){
                    
                    $date = new DateTime($ClientUserVo2->date);
                    $dateFinish = new DateTime($ClientUserVo2->dateFinish);
                
                    if( $person->state == "1" ){
                        for ($i=0; $i < 1 ; $i++) { 
                         $arrayList[] = ["id"=>"004".$person->id ,"checked"=> $this->ValidateEventChecked($ClientUserVo2->nameEvent,$date , $i), "name"=> $ClientUserVo2->nameEvent , "startdate"=> $date->format('Y-m-d'), "enddate"=> $dateFinish->format('Y-m-d'),"starttime"=> $ClientUserVo2->hourInit , "endtime"=>$ClientUserVo2->hourFinish , "color" => "#".substr(md5(date ( rand(5,90000)) ), 0, 6) , "url"=>"#"  ];
                         $date = new DateTime(date("Y-m-d",strtotime ( '+1 day' , strtotime ($date->format('Y-m-d')))));
                        }
                    }
                }
            }
            return  $arrayList;
        }

    public function GetProspectsByPerson($id){
        $arrayList = array();
        $ClientUserVo = new ClientUserVo();
        $ClientUserVo->idUser = $id;
        $ClientUserVo->idSearchField = 1;
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $ClientUserVo);
        while($ClientUserVo2 = $generalDao->GetVo($generalVo = clone $ClientUserVo)) {
            
            $person = $this->GetPerson($ClientUserVo2->idClient);
            if(isset($person)){
                $date = new DateTime($person->creationDate);
            
                if($person->idRole == "9" && $person->state == "1" ){
                    for ($i=0; $i < 3 ; $i++) { 
                    $arrayList[] = ["id"=>"003".$person->id ,"checked"=> $this->ValidateEventChecked("Se debe realizar Seguimiento al Prospecto {$person->names} {$person->lastNames}.",$date , $i), "name"=>"Se debe realizar Seguimiento al Prospecto {$person->names} {$person->lastNames}." , "startdate"=> $date->format('Y-m-d'), "enddate"=>date ( 'Y-m-d' ,strtotime ( '+0 day' , strtotime ($date->format('Y-m-d')))),"starttime"=>"00:00" , "endtime"=>"23:59" , "color" => "#".substr(md5(date ( rand(5,90000)) ), 0, 6) , "url"=>"#"  ];
                    $date = new DateTime(date("Y-m-d",strtotime ( '+1 day' , strtotime ($date->format('Y-m-d')))));
                    }
                }
            }
        }
        return  $arrayList;
    }

    public function ValidateEventChecked($evento , $date , $range){
        $CheckedEventsVo = new CheckedEventsVo();
        $day = $date->format('d');
        $CheckedEventsVo->titleEvent = $evento."-".$day;
        $CheckedEventsVo->idSearchField = 1;
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $CheckedEventsVo);
       
        //print_r($day."-");
        if($CheckedEventsVo2 = $generalDao->GetVo($generalVo = clone $CheckedEventsVo)) {   
          return true;  
        }
        return false;
    }


    public function GetVisitByPerson($id){
        $arrayList = array();
        $VisitDescriptionVo = new VisitDescriptionVo();
        $VisitDescriptionVo->responsable = $id;
        $VisitDescriptionVo->idSearchField = 5;
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $VisitDescriptionVo);
        while($VisitDescriptionVo2 = $generalDao->GetVo($generalVo = clone $VisitDescriptionVo)) {
            
            if($VisitDescriptionVo2->dateCita != "sin novedades de cita"){
              
                $date = new DateTime($VisitDescriptionVo2->dateCita);
                for ($i=0; $i < 1 ; $i++) { 
                    $arrayList[] = ["id"=>"002".$VisitDescriptionVo2->id ,"checked"=> $this->ValidateEventChecked("Tiene una visita programada para el día {$VisitDescriptionVo2->dateCita} con el cliente o prospecto <b>{$this->GetPerson($this->GetVisit($VisitDescriptionVo2->idVisits)->idClient)->names} {$this->GetPerson($this->GetVisit($VisitDescriptionVo2->idVisits)->idClient)->lastNames}</b> de la empresa <b>{$this->GetCompany($this->GetPerson($this->GetVisit($VisitDescriptionVo2->idVisits)->idClient)->idCompany)->name} </b>.",$date , $i), "name"=>"Tiene una visita programada para el día {$VisitDescriptionVo2->dateCita} con el cliente o prospecto <b>{$this->GetPerson($this->GetVisit($VisitDescriptionVo2->idVisits)->idClient)->names} {$this->GetPerson($this->GetVisit($VisitDescriptionVo2->idVisits)->idClient)->lastNames}</b> de la empresa <b>{$this->GetCompany($this->GetPerson($this->GetVisit($VisitDescriptionVo2->idVisits)->idClient)->idCompany)->name} </b>." , "startdate"=> $date->format('Y-m-d') , "enddate"=>$date->format('Y-m-d') ,"starttime"=>"00:00" , "endtime"=>"23:59" , "color" => "#".substr(md5(date ( rand(5,90000)) ), 0, 6) , "url"=>"#"  ];
                    $date = new DateTime(date("Y-m-d",strtotime ( '+1 day' , strtotime ($date->format('Y-m-d')))));
                }
            }
        }
        return  $arrayList;
    }

    public function GetVisit($id){
        $VisitsVo = new VisitsVo();
        $VisitsVo->id = $id;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $VisitsVo);
        if($VisitsVo2 = $generalDao->GetVo($generalVo = clone $VisitsVo)) {
            return $VisitsVo2;
        }
    }

    public function GetPerson($id){
        $PersonVo = new PersonVo();
        $PersonVo->id = $id;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $PersonVo);
        if($PersonVo2 = $generalDao->GetVo($generalVo = clone $PersonVo)) {
            return $PersonVo2;
        }
    }

    public function GetCompany($id){
        $CompanyVo = new CompanyVo();
        $CompanyVo->id = $id;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $CompanyVo);
        if($CompanyVo2 = $generalDao->GetVo($generalVo = clone $CompanyVo)) {
            return $CompanyVo2;
        }
    }

    public function GetDataQuote($id){
        $arrayList = array();
        $QuotationVo = new QuotationVo();
        $QuotationVo->idUser = $id;
        $QuotationVo->idSearchField = 6;
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $QuotationVo);
        while($QuotationVo2 = $generalDao->GetVo($generalVo = clone $QuotationVo)) {
            
            if($QuotationVo2->state == "Enviada"){
              
                $date = new DateTime($QuotationVo2->generationDate);
                for ($i=0; $i < 2 ; $i++) { 
                    $arrayList[] = ["id"=>"001".$QuotationVo2->id ,"checked" => $this->ValidateEventChecked("Se Debe Atender la cotización #{$QuotationVo2->id}  enviada hace dos días antes.",$date , $i), "name"=>"Se Debe Atender la cotización #{$QuotationVo2->id}  enviada hace dos días antes." , "startdate"=> date ( 'Y-m-d' ,strtotime ( '+2 day' , strtotime ($date->format('Y-m-d')))), "enddate"=>date ( 'Y-m-d' ,strtotime ( '+0 day' , strtotime ($date->format('Y-m-d')))),"starttime"=>"00:00" , "endtime"=>"23:59" , "color" => "#".substr(md5(date ( rand(5,90000)) ), 0, 6) , "url"=>"#"  ];
                    $date = new DateTime(date("Y-m-d",strtotime ( '+1 day' , strtotime ($date->format('Y-m-d')))));
                }
            }
        }
        return  $arrayList;
    }

    public function GetIdQuotation($idPerson){
        $arrayList = array();
        $PersonCreateQuotationVo = new PersonCreateQuotationVo();
        $PersonCreateQuotationVo->idPerson = $idPerson;
        $PersonCreateQuotationVo->idSearchField = 7;
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $PersonCreateQuotationVo);
        while($PersonCreateQuotationVo2 = $generalDao->GetVo($generalVo = clone $PersonCreateQuotationVo)) {
          if(!in_array($PersonCreateQuotationVo2->idQuotation , $arrayList )){
            $arrayList[] =  $PersonCreateQuotationVo2->idQuotation;
          }
        }   
        return $arrayList;
    }

    public function GetArrayToCalendar(){
        $arrayList = $this->GetQuoationSend($_POST["idPerson"]);
       // print_r($_POST["idPerson"]);
        print_r(json_encode($arrayList));
    }

    public function GetArrayToCalendarByParameter(){
        $arrayList = $this->GetQuoationSendByParameter($_POST["idPerson"]);
        print_r(json_encode($arrayList));
    }

    public function UpdateStateWithCalendar(){
       // if($_POST["state"] == "0"){
            $this->InsertCheckData($_POST["event"] , $_POST["day"]);
       // }else{
         //   $this->UpdateCheckData($_POST["event"]);
       // }
    }

    private function SetDataToVo($id , $event , $state , $day)
    {
        $CheckedEventsVo = new CheckedEventsVo();
        $CheckedEventsVo->id = $id;
        $CheckedEventsVo->day = $day;
        $CheckedEventsVo->titleEvent = $event."-".( (strlen($CheckedEventsVo->day) == 1) ? "0".$CheckedEventsVo->day : $CheckedEventsVo->day  );
        $CheckedEventsVo->checked = $state;
        
       
        return $CheckedEventsVo;
    }

    private function InsertCheckData($event , $day){
        $CheckedEventsVo = $this->SetDataToVo(null ,$event,1 , $day);
        $CheckedEventsVo->id = null;
        $generalDao = new GeneralDao();
        $result = $generalDao->Set($generalVo = clone $CheckedEventsVo);
    }
    
}

$ajaxDataImpost = new AjaxDataImpost();
