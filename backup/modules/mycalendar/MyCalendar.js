// jshint unused:true
"use strict";
var Events = { "monthly": [] };
var e = 1;
/*********************************CLICK EVENTS**************************/

function ChangeFormatPrices() {
    $("#impostPercentCtx").keypress(function(event) {
        console.log(event.charCode);
        if (event.charCode > 58 || event.charCode < 43 || event.charCode == 45 || event.charCode == 46) {
            return false;
        }
    });
}

function ValidatePriceUnit() {
    $("#impostPercentCtx").change(function(event) {
        var cont = 0;
        var numberType = $(this).val();
        for (var i = 0; i < numberType.toString().length; i++) {
            if (numberType.toString()[i] == "," || numberType.toString()[i] == ".") {
                cont++;
            }
        }
        if (cont > 1) {
            numberType = "";
        }
        $(this).val(numberType);
    });
}

function DataLoadAjax(callback, user) {
    var parameters = {
        "isActionFromAjax": true,
        "optionInputForm": 5,
        "idDataInputForm": 0,
        "idPerson": (($("#personUserLst").val()) ? $("#personUserLst").val() : $("#idPerson").val()),
    };

    $.ajax({
        url: "view/html/modules/mycalendar/AjaxDataMyCalendar.php",
        method: "POST",
        data: parameters,
        success: function(data) {
            callback(data);
        }
    })
}

function DataLoadAjaxParameters(callback, user) {
    var parameters = {
        "isActionFromAjax": true,
        "optionInputForm": 6,
        "idDataInputForm": 0,

        "idPerson": (($("#personUserLst").val()) ? $("#personUserLst").val() : $("#idPerson").val()),
        "parameter": $("#eventChange").val()
    };

    $.ajax({
        url: "view/html/modules/mycalendar/AjaxDataMyCalendar.php",
        method: "POST",
        data: parameters,
        success: function(data) {
            callback(data);
        }
    })
}

function LoadDataToCalendar(user = '') {
    DataLoadAjax(function(data) {
        var myData = JSON.parse(data);
        for (var i = 0; i < myData.Quotation.length; i++) {
            Events.monthly.push(myData.Quotation[i]);
        }

        for (var i = 0; i < myData.Visits.length; i++) {
            Events.monthly.push(myData.Visits[i]);
        }

        for (var i = 0; i < myData.Prospects.length; i++) {
            Events.monthly.push(myData.Prospects[i]);
        }

        for (var i = 0; i < myData.Estaticos.length; i++) {
            Events.monthly.push(myData.Estaticos[i]);
        }

    }, user);
}

function CalendarData() {
    $('#mycalendar' + e).monthly({
        mode: 'event',
        dataType: 'json',
        events: Events,
    });
    $(".monthly-prev").trigger("click");
    setTimeout(function() { $(".monthly-next").trigger("click") }, 500);
}

function ChangeEventCalendar(user = '') {
    Events = { "monthly": [] };
    $("#mycalendar" + e).remove();
    e++;
    $("#calendarDiv").html('<div class="monthly" id="mycalendar' + e + '" style="width:100% ; height: 30%; max-height: 30%"></div>');
    DataLoadAjaxParameters(function(data) {
        var myData = JSON.parse(data);
        console.log(myData, user);
        //if ($("#eventChange").val() == "Quotes") {
        for (var i = 0; i < myData.Quotation.length; i++) {
            Events.monthly.push(myData.Quotation[i]);
        }
        //}
        //if ($("#eventChange").val() == "Visits") {
        for (var i = 0; i < myData.Visits.length; i++) {
            Events.monthly.push(myData.Visits[i]);
        }
        //}

        //if ($("#eventChange").val() == "Prospects") {
        for (var i = 0; i < myData.Prospects.length; i++) {
            Events.monthly.push(myData.Prospects[i]);
        }

        for (var i = 0; i < myData.Estaticos.length; i++) {
            Events.monthly.push(myData.Estaticos[i]);
        }
        //}

        if ($("#eventChange").val() == "Todos") {
            LoadDataToCalendar();
        }

    }, user)
    CalendarData();
}
function CalendarCheck( classes , day){
    swal({
        title: "¿DESEA CHECKEAR EL EVENTO?",
        text: "¿Esta seguro que desea completar el evento o los eventos seleccionados?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si!',
        cancelButtonText: 'No!',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: false,
        reverseButtons: true
    }).then(function(result) {
        if (result.value) {
        ActionCalendarCheck($(classes).attr("title-data") , 1 , classes);
        }
    });
        

}

function ActionCalendarCheck(event, state , obj) {
    var parameters = {
        "isActionFromAjax": true,
        "optionInputForm": 9,
        "idDataInputForm": 0,
        "event": event,
        "state": state,
        "day":$(obj).parent().parent().parent().parent().attr("data-number")
    };
    $.ajax({
        url: "view/html/modules/mycalendar/AjaxDataMyCalendar.php",
        method: "POST",
        data: parameters,
        success: function(data) {
            $(obj).attr("class","btn btn-btn btn-primary btn-xs-block pull-right");
            $(obj).html("<i class='fa fa-thumbs-up'></i>");
            ChangeEventCalendar();
            MessageSwalBasic("Evento Actualizado!","Se ha actualizado el evento efectivamente.","success");
        }
    })
    
}

function CalendarChangeUser() {
    $("#personUserLst").change(function() {
        ChangeEventCalendar($(this).val());
    });
}

$(function() {
    ChangeFormatPrices();
    ValidatePriceUnit();
    LoadDataToCalendar();
    CalendarData();
    CalendarChangeUser();
    RemoveActionModule(32);
    $("#fechaCtx").datepicker({
        format: 'yyyy-mm-dd',

    });
    $("#fechaFinishCtx").datepicker({
        format: 'yyyy-mm-dd',

    });

    $('#datetimepicker3').datetimepicker({
        format: 'HH:mm'
    });
    $('#datetimepicker4').datetimepicker({
        format: 'HH:mm'
    });
});