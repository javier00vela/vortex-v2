<?php
require_once(Config::PATH . Config::CONTROLLER . 'general/AccessManager.php');
require_once(Config::PATH . Config::CONTROLLER . 'general/UtilJquery.php');
require_once(Config::PATH . Config::VIEW_HTML . 'general/comps/GeneralCtx.php');
require_once(Config::PATH . Config::VIEW_HTML . 'general/comps/GeneralLst.php');
require_once(Config::PATH . Config::VIEW_HTML . 'general/comps/GeneralWithDataLst.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralDao.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/UserVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/StaticEventsVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/ImpostVo.php');

class MyCalendarView
{
    private $optionInputForm;
    private $isUpdateUserForm;
    private $idDataInputForm;
    /*=============================================
                  Session Vars
    =============================================*/
  
    /*=============================================
                  Template Vars
    =============================================*/
    private $utilJQ;
    private $pageTpl, $data, $core;
    /*=============================================
                Controls Vars HTML
    =============================================*/
    private $fechaCtx;
    private $fechaFinishCtx;
    private $hourInit;
    private $hourFinish;
    private $listPerson;
    /*=============================================
                Data Vars
    =============================================*/
    private $generalDao;


    function __construct($pageTpl, $data, $core)
    {
        $this->data = $data;
        $this->core = $core;
        $this->pageTpl = $pageTpl;
            $this->CreateComponents();
            $this->UseDataFromThePostback();
    }

    private function UseDataFromThePostback()
    {
        if (isset($_POST['optionInputForm'])) {
            $this->optionInputForm = $_POST['optionInputForm'];
            $this->idDataInputForm = $_POST['idDataInputForm'];
            $this->generalDao = new GeneralDao();
            switch ($this->optionInputForm) {
                case "1": {
                    $this->SetData();
                    break;
                }
                case "2": {
                    $this->DeleteData();
                    break;
                }
                case "4": {
                    $this->SetUpdate();
                    break;
                }
            }
        }

        $this->AssignDataToTpl();

    }

    private function CreateComponents()
    {
        $this->fechaCtx = new GeneralCtx("fechaCtx","fecha Inicio del evento",null,true);
        $this->fechaFinishCtx = new GeneralCtx("fechaFinishCtx","fecha Final del evento",null,true);
        $this->hourInit = new GeneralCtx("horaInitCtx","hora Inicio del evento",null,true);
        $this->hourFinish = new GeneralCtx("horaFinishCtx","hora final del evento",null,true);
        $this->listPerson = new GeneralWithDataLst($this->AssignDataSeller(), "personUserLst", false,true,"--Vendedor Asociado--",true);
       
        $this->utilJQ = new UtilJquery("Impost");
    }

    private function AssignDataSeller(){

        $userArray = array();
        $PersonVo = new PersonVo();
        $PersonVo->isList = true;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $PersonVo);
        while ($PersonVo2 = $generalDao->GetVo($generalVo = clone $PersonVo)) {
                if($PersonVo2->state == 1){
                    if($PersonVo2->idRole == 1 || $PersonVo2->idRole == 2 || $PersonVo2->idRole == 3 || $PersonVo2->idRole == 4  ||  $PersonVo2->idRole == 5 || $PersonVo2->idRole == 6 || $PersonVo2->idRole == 10 || $PersonVo2->idRole == 11 || $PersonVo2->idRole == 12 ||  $PersonVo2->idRole == 13){
                        $userArray[]  = $PersonVo2->id."_".$PersonVo2->names." ".$PersonVo2->lastNames;  
                    }
            }
        }
        return $userArray;

    }
    private function SetDataToVo()
    {
        $StaticEventsVo = new StaticEventsVo();
        $StaticEventsVo->id = $this->idDataInputForm;
        $StaticEventsVo->nameEvent = $_POST["eventDescription"];
        $StaticEventsVo->date = $_POST["fechaCtx"];
        $StaticEventsVo->dateFinish = $_POST["fechaFinishCtx"];
        $StaticEventsVo->idUser = $_POST["personUserLst"];
        $StaticEventsVo->hourInit = $_POST["horaInitCtx"];
        $StaticEventsVo->hourFinish = $_POST["horaFinishCtx"];

       
        return $StaticEventsVo;
    }
    private function SetData()
    {
        $StaticEventsVo = $this->SetDataToVo();
        $StaticEventsVo->id = null;
        $result = $this->generalDao->Set($generalVo = clone $StaticEventsVo);
        if (isset($result)) {
            $message = 'MessageSwalBasic("Registrado!","Evento Registrado Correctamente","success");';
            $this->utilJQ->AddFunctionJavaScript($message);
        }

    }
    private function SetUpdate()
    {
        $result = $this->generalDao->Update($generalVo = clone $this->SetDataToVo());
        if (isset($result)) {
            $message = 'MessageSwalBasic("Modificado!","Impuesto Modificado Correctamente","success");';
            $this->utilJQ->AddFunctionJavaScript($message);
        }
    }
    private function DeleteData()
    {
        $message = "";
        $impostVo = new ImpostVo();
        $impostVo->id = $this->idDataInputForm;
        if ($this->generalDao->Delete($generalVo = clone $impostVo)) {
            $message = 'MessageSwalBasic("Eliminado!","Impuesto Eliminado Correctamente","success");';
        } else {
            $message = 'MessageSwalBasic("Error!","Problema con la base de datos","error");';
        }
        $this->utilJQ->AddFunctionJavaScript($message);
    }

    public function GetDataPersonId(){
        $userVo = unserialize($_SESSION['user']["user"]);
        $personVo = unserialize($_SESSION['user']["person"]);
        return  $personVo->id;
    }

    private function AssignDataToTpl()
    {
        $this->data->assign('fechaCtx', $this->fechaCtx->paint());
        $this->data->assign('fechaFinishCtx', $this->fechaFinishCtx->paint());
        $this->data->assign('PersonId', $this->GetDataPersonId());
        $this->data->assign('listPerson', $this->listPerson->paint());
        $this->data->assign('hourInit', $this->hourInit->paint());
        $this->data->assign('hourFinish', $this->hourFinish->paint());
        $this->data->assign('tempParameter', time());
        $this->data->assign('jquery', $this->utilJQ->PaintJQ());
        $this->SetDataToTpl();
    }   
    private function SetDataToTpl()
    {
        echo $this->core->get($this->pageTpl, $this->data);
    }
  


}   