<?php
require_once(Config::PATH . Config::CONTROLLER . 'general/AccessManager.php');
require_once(Config::PATH . Config::CONTROLLER . 'general/UtilJquery.php');
require_once(Config::PATH . Config::VIEW_HTML . 'general/comps/GeneralCtx.php');
require_once(Config::PATH . Config::VIEW_HTML . 'general/comps/GeneralLst.php');
require_once(Config::PATH . Config::VIEW_HTML . 'general/comps/GeneralWithDataLst.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralDao.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/CompanyVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/PricesCompanyVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/RoleCompanyVo.php');
require_once(Config::PATH . Config::CONTROLLER . 'general/ExcelManager.php');


class ProviderView
{
    private $optionInputForm;
    private $isUpdateUserForm;
    private $idDataInputForm;
    /*=============================================
                  Session Vars
    =============================================*/
    private $manageSession;
    /*=============================================
                  Template Vars
    =============================================*/
    private $utilJQ;
    private $pageTpl, $data, $core;
    /*=============================================
                Controls Vars HTML
    =============================================*/
    private $companyNameCtx;
    private $companyNitCtx;
    private $companyPhoneCtx;
    private $companyEmailCtx;
    private $companyWebSiteCtx;
    private $companyAddressCtx;
    private $companyCountryLst;
    private $companyCityLst;
    private $companyIdRoleLst;

    /*=============================================
                Data Vars
    =============================================*/
    private $generalDao;
    private $nameColumns =  array("id","Nombre","Nit","Telefono","Email creación","Sitio","Dirección","Pais","Ciudad","Fecha","Rol");


    function __construct($pageTpl, $data, $core)
    {
        $this->data = $data;
        $this->core = $core;
        $this->pageTpl = $pageTpl;

            $this->CreateComponents();
            $this->UseDataFromThePostback();
            $this->CreateReportExcel();
        
    }

        private function CreateReportExcel(){
            if(isset($_POST["data"])){
                ExcelManager::CreateReportExcel($_POST["data"] , $this->nameColumns , "ReportProvider" );
            }
        }

    private function UseDataFromThePostback()
    {
        if (isset($_POST['optionInputForm'])) {
            $this->optionInputForm = $_POST['optionInputForm'];
            $this->idDataInputForm = $_POST['idDataInputForm'];
            $this->generalDao = new GeneralDao();
            switch ($this->optionInputForm) {
                case "1": {
                    $this->SetData();
                    break;
                }
                case "2": {
                    $this->DeleteData();
                    break;
                }
                case "4": {
                    $this->SetUpdate();
                    break;
                }
            }
        }
        if (isset($_POST['isUpdateUserForm'])) {
            $this->isUpdateUserForm = $_POST['isUpdateUserForm'];
            if($this->isUpdateUserForm == "1"){
                $this->UpdateUser();
            }
        }
        $this->AssignDataToTpl();

    }

    private function CreateComponents()
    {
        $this->companyNameCtx = new GeneralCtx("companyNameCtx","Nombre",null,true);
        $this->companyNitCtx = new GeneralCtx("companyNitCtx","Nit",null,true);
        $this->companyPhoneCtx = new GeneralCtx("companyPhoneCtx","Telefono",null,true);
        $this->companyEmailCtx = new GeneralCtx("companyEmailCtx","Email",null,true);
        $this->companyWebSiteCtx = new GeneralCtx("companyWebSiteCtx","Sitio Web",null,true);
        $this->companyAddressCtx = new GeneralCtx("companyAddressCtx","Direccion",null,true);
        $dataCompanyCountryLst = array();
        $this->companyCountryLst = new GeneralWithDataLst($dataCompanyCountryLst, "companyCountryLst", false,false,"-- País --",true);
        $this->companyCountryLst->lst->adiPropiedad("style","width: 100%;");
        $dataCompanyCityCLst = array();
        $this->companyCityLst = new GeneralWithDataLst($dataCompanyCityCLst, "companyCityLst", false,false,"-- Ciudad --",true);
        $this->companyCityLst->lst->adiPropiedad("style","width: 100%;");

        $roleCompanyVo = new RoleCompanyVo();
        $roleCompanyVo->isList = true;
        $this->companyIdRoleLst = new GeneralLst($generalVo = clone $roleCompanyVo,"companyIdRoleLst", 0, 1, "--Compañia--",true);

        $this->utilJQ = new UtilJquery("Users");
    }

    private function SetDataToVo()
    {
        $companyVo = new CompanyVo();
        $companyVo->id = $this->idDataInputForm;
        $companyVo->name = $_POST["companyNameCtx"];
        $companyVo->nit = $_POST["companyNitCtx"];
        $companyVo->phone = $_POST["companyPhoneCtx"];
        $companyVo->email = $_POST["companyEmailCtx"];
        $companyVo->webSite = $_POST["companyWebSiteCtx"];
        $companyVo->address = $_POST["companyAddressCtx"];
        $companyVo->country = $_POST["companyCountryLst"];
        $companyVo->city = $_POST["companyCityLst"];
        $companyVo->creationDate = date('Y-m-d H:i:s');
        $companyVo->state = 1;
        $companyVo->idRoleCompany = $_POST["companyIdRoleLst"];

        return $companyVo;
    }

    private function SetData()
    {
        $companyVo = $this->SetDataToVo();
        $companyVo->id = null;
        $result = $this->generalDao->Set($generalVo = clone $companyVo);
        if (isset($result)) {
            $this->AddDefaultDataToPrices($companyVo , $result);
            $message = 'MessageSwalBasic("Registrado!","empresa Registrada Correctamente","success");';
            $this->utilJQ->AddFunctionJavaScript($message);
        }

    }

    private function AddDefaultDataToPrices($companyVo , $id){
        if($companyVo->idRoleCompany == 3){
            $pricesCompanyVo = $this->SetDataPricesCompany($id);
            $result = $this->generalDao->Set($generalVo = clone $pricesCompanyVo);
        }
    }


    private function SetDataPricesCompany($idCompany){
        $pricesCompanyVo = new pricesCompanyVo();
        $pricesCompanyVo->id = null;
        $pricesCompanyVo->typeMoney = "USD";
        $pricesCompanyVo->iva = 0;
        $pricesCompanyVo->tpm = 3000;
        $pricesCompanyVo->ganancia = 0;
        $pricesCompanyVo->idCompany = $idCompany;
        return $pricesCompanyVo;
    }

    private function SetUpdate()
    {
        $result = $this->generalDao->Update($generalVo = clone $this->SetDataToVo());
        if (isset($result)) {
            $message = 'MessageSwalBasic("Modificado!","Usuario Modificado Correctamente","success");';
            $this->utilJQ->AddFunctionJavaScript($message);
        }
    }

    private function DeleteData()
    {
        $message = "";
        $companyVo = new CompanyVo();
        $companyVo->id = $this->idDataInputForm;
        $companyVo->idUpdateField = 11;
        $companyVo->isList = false;
        $companyVo->state = 0;
        $companyVo->idSearchField = 0;
        if ($this->generalDao->UpdateByField($generalVo = clone $companyVo)) {
        }
            $message = 'MessageSwalBasic("Eliminado!","Registro Eliminado Correctamente","success");';
       
        $this->utilJQ->AddFunctionJavaScript($message);
    }

    private function AssignDataToTpl()
    {
        $this->data->assign('companyNameCtx', $this->companyNameCtx->paint());
        $this->data->assign('companyNitCtx', $this->companyNitCtx->paint());
        $this->data->assign('companyPhoneCtx', $this->companyPhoneCtx->paint());
        $this->data->assign('companyEmailCtx', $this->companyEmailCtx->paint());
        $this->data->assign('companyWebSiteCtx', $this->companyWebSiteCtx->paint());
        $this->data->assign('companyAddressCtx', $this->companyAddressCtx->paint());
        $this->data->assign('companyCountryLst', $this->companyCountryLst->paint());
        $this->data->assign('companyCityLst', $this->companyCityLst->paint());
        $this->data->assign('companyIdRoleLst', $this->companyIdRoleLst->paint());

         $this->data->assign('tempParameter', time());

        $this->data->assign('jquery', $this->utilJQ->PaintJQ());
        $this->SetDataToTpl();
    }

    private function SetDataToTpl()
    {
        echo $this->core->get($this->pageTpl, $this->data);
    }

}   