<?php
require_once(__DIR__ . '/../../../../controller/general/Config.php');
require_once(Config::PATH . Config::CONTROLLER.Config::GENERAL . 'ManageArrays.php');
require_once(Config::PATH . Config::BACKEND . 'modules/CompanyVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/RoleCompanyVo.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralDao.php');

class AjaxDataProviders{

    private $isActionFromAjax;
    private $optionInputForm;
    private $idDataInputForm;
    public $dataList = [];

    function __construct()
    {
        if (isset($_POST['isActionFromAjax'])) {
            $this->isActionFromAjax = $_POST['isActionFromAjax'];
            $this->optionInputForm = $_POST['optionInputForm'];
            $this->idDataInputForm = $_POST['idDataInputForm'];
            if(isset($_POST["listData"])){
                $this->dataList = $_POST["listData"];  
            }
            if ($this->isActionFromAjax == "true") {
                if ($this->optionInputForm == 3) {
                    $this->SetDataInControls();
                }else if($this->optionInputForm == 5){
                    $this->GetTypeCompanyLenght($_POST["typeRol"]);
                }else {
                    $this->GetDataInJsonForTbl();
                }
            }
        }
    }

    private function SetDataInControls(){
        $SetInputsDataFJQ = "";
        $companyVo = new CompanyVo();
        $companyVo->id = $this->idDataInputForm;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $companyVo);
        if($companyVo2 = $generalDao->GetVo($generalVo = clone $companyVo)){
            $SetInputsDataFJQ = "
                            <script>
                               (function($){
                        SetValueToInputText('companyNameCtx','{$companyVo2->name}');
                        SetValueToInputText('companyNitCtx','{$companyVo2->nit}');
                        SetValueToInputText('companyPhoneCtx','{$companyVo2->phone}');
                        SetValueToInputText('companyEmailCtx','{$companyVo2->email}');
                        SetValueToInputText('companyWebSiteCtx','{$companyVo2->webSite}');
                        SetValueToInputText('companyAddressCtx','{$companyVo2->address}');
                        
                        $('#companyCountryLst').select2('destroy'); 
                        SetValueToSelect('companyCountryLst','{$companyVo2->country}',true);
                        SetSelect2Countries();
                        
                        $('#companyCityLst').select2('destroy');
                        SetValueToInputText('companyCityLst','{$companyVo2->city}');
                        SetSelect2Cities();
                        
                        SetValueToInputText('companyCreationDateCtx','{$companyVo2->creationDate}');
                        SetValueToSelect('companyIdRoleLst','{$companyVo2->idRoleCompany}',true);
                        $('#'+'$companyVo->nameTable'+'PopUpAdd').modal('show');
                        SetValueToInputText('optionInputForm',4);
                        SetValueToInputText('idDataInputForm',$companyVo2->id);
                               })(jQuery);
                            </script>
                        ";
        }
        echo $SetInputsDataFJQ;
    }

    public function GetDataInJsonForTbl()
    {
        $existData = false;
        $companyVo = new CompanyVo();
        $companyVo->isList = true;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $companyVo);
        $buttoms = "";
        $string = "
        {\"data\": [";
        while ($companyVo2 = $generalDao->GetVo($generalVo = clone $companyVo)) {
            if($companyVo2->state == 1 && $companyVo2->idRoleCompany == 3){
            $existData = true;
            $json = array();
            $json[] = $companyVo2->id;
            $json[] = htmlspecialchars($companyVo2->name);
            $json[] = htmlspecialchars($companyVo2->nit);
            $json[] = htmlspecialchars($companyVo2->phone);
            $json[] = htmlspecialchars($companyVo2->email);
            $json[] = htmlspecialchars($companyVo2->webSite);
            $json[] = htmlspecialchars($companyVo2->address);
            $json[] = htmlspecialchars(ucfirst($companyVo2->country));
            $json[] = htmlspecialchars(ucfirst($companyVo2->city));
            $json[] = $companyVo2->creationDate;
            $json[] = htmlspecialchars($this->GetRoleCompany($companyVo2->idRoleCompany));
            if(!in_array( "ver_lista_precios" , $this->dataList)){    
                $buttoms .= "<tip title='Listar-Precios-Productos' class='green-tooltip' data-toggle='tooltip' data-placement='top'><a id='productsPrices' name='productsPriceBtn' href='InventoryPrice?nameCompayInputForm=".$companyVo2->name."&&idCompayInputForm=".$companyVo2->id."' class='btn btn-success btn-xs'   ><i class='fa fa-list'></i></a></tip> ";
            }
            if(!in_array( "modificar_precios" , $this->dataList)){
                $buttoms .="<tip title='Modificar Precios' class='purple-tooltip' data-toggle='tooltip' data-placement='top'>
            <a id='modyfyPricesBtn' name='modifyPricesBtn' class='btn bg-purple btn-xs'   onclick='location.href=\"ModifyPrices?idCompany=$companyVo2->id\"' > <i class='fa fa-dollar'></i></a><tip> ";
            }
            
            $json[] = $buttoms;
            $string .= json_encode($json) . ",";
            $buttoms = "";
            }
        }
        $string .= "-]}-";

        if($existData){
            echo  str_replace(",-]}-", "]}", $string);
        }else{
            echo "{ \"data\": [] }";
        }
    }

    function GetTypeCompanyLenght($idRole)
    {
        $arrayList =  array();
        $roleCompanyVo = new companyVo();
        $roleCompanyVo->idRoleCompany = $idRole;
        $roleCompanyVo->idSearchField = 10;
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $roleCompanyVo);
        print_r($generalDao->GetLength());
    }

    function GetRoleCompany($idRole)
    {
        $roleCompanyVo = new RoleCompanyVo();
        $roleCompanyVo->id = $idRole;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $roleCompanyVo);
        if ($roleCompanyVo2 = $generalDao->GetVo($generalVo = clone $roleCompanyVo)) {
            return "" . $roleCompanyVo2->name;
        }
        return "Problema en la consulta";
    }

}

$ajaxDataCompany = new AjaxDataProviders();
