<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Administrar Empresa/Proveedores
        </h1>
        <ol class="breadcrumb">
            <li><a href="Home"><i class="fa fa-dashboard"></i> Inicio</a></li>
            <li class="active">Administrar Empresas/Proveedores</li>
        </ol>
    </section>
    <section class="content">
        <div class="box">

            <nav class="navbar navbar-light">
                <div class="container-fluid">
                    <ul class="nav navbar-nav">

                        <li>
                            <a style="cursor: pointer;" id="userAddBtn" name="userAddBtn" onclick="location.href = 'Home' ">
                                <i class="fa fa-home"></i> Menu Principal
                            </a>
                        </li>
                        <li>
                            <a node="node-generar_excel" style="cursor: pointer;" id="userAddBtn" name="userAddBtn" onclick="CreateExcelDocs() ">
                                <i class="fa fa-file"></i> Generar Excel
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>

            <div class="box-body">
                <table id="companyTbl" name="companyTbl" class="table table-bordered table-striped dt-responsive tables dataTable no-footer dtr-inline" style="width: 100%;" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Nombre</th>
                            <th>Nit</th>
                            <th>Telefono</th>
                            <th>Email</th>
                            <th>Sitio Web</th>
                            <th>Direccion</th>
                            <th>Pais</th>
                            <th>Ciudad</th>
                            <th>Fecha de Creacion</th>
                            <th>Role</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </section>
</div>
<div id="containerResponseAjax"></div>
<script src="view/html/modules/providers/Providers.js?v= {$tempParameter}"></script>
{$jquery}