
   <?php
require_once(__DIR__ . '/../../../../controller/general/Config.php');
require_once(Config::PATH . Config::CONTROLLER.Config::GENERAL . 'ManageArrays.php');

require_once(Config::PATH . Config::BACKEND . 'modules/ProductVo.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralDao.php');
require_once(Config::PATH . Config::BACKEND . 'modules/QuotationVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/InventoryVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/UserQuotationVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/CompanyVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/PersonCreateQuotationVo.php');


class AjaxDataHistoryProducts
{

    private $isActionFromAjax;
    private $optionInputForm;
    private $idDataInputForm;
    public $dataList = [];
     function __construct()
    {
        if (isset($_POST['isActionFromAjax'])) {
            $this->isActionFromAjax = $_POST['isActionFromAjax'];
            $this->optionInputForm = $_POST['optionInputForm'];
            $this->idDataInputForm = $_POST['idDataInputForm'];
            if(isset($_POST["listData"])){
                $this->dataList = $_POST["listData"];  
            }
            if ($this->isActionFromAjax == "true") {
                if ($this->optionInputForm == 1) {
                   $getDataQuotationFromProduct = $this->GetDataQuotation($this->SetDataProductQuotation(),$this->GetDataNameCompanyProduct());
                   $this->LoadDataTbl($getDataQuotationFromProduct);
                }elseif ($this->optionInputForm ==2) {
                    $this->GetNumTimesForProduct($this->SetDataProductQuotation());
                }
            }
        }
    }

    public function LoadDataTbl($data){
        $existData = false;
        $cont = 1;
         $string = "
        {\"data\": [";
      
        for ($x=0; $x <count($data) ; $x++) { 
            $existData = true;
            $json = array();
            $json[] = $cont++;
            $json[] = $data[$x]["id"];
            $json[] = $this->GetNameUserQuotation($data[$x]["id"]);
            $json[] = $this->GetNameCompany($data[$x]["idCompany"]);
            $json[] = $data[$x]["userQuotation"];
            $json[] = $data[$x]["generationDate"];
            $json[] = $data[$x]["nameCompany"];
            $button = "";
            if(!in_array( "ver_cotizacion" , $this->dataList)){
                $button .= "<tip title='Ver Cotización' class='blue-tooltip' data-toggle='tooltip' data-placement='top'><a class='btn btn-primary btn-xs'  onclick='PrintData(".$data[$x]["id"].")'><i class='fa fa-file'></i></a></tip>";
            }
            $json[] = $button;

            $string .= json_encode($json) . ",";
        }

         $string .= "-]}-";

        if ($existData) {
            echo str_replace(",-]}-", "]}", $string);
        } else {
            echo "{ \"data\": [] }";
        }
    }

    public function GetDataQuotation($idQuotationLst, $nameCompany){
        $productQuotationList = array();
        for ($i=0; $i <count($idQuotationLst) ; $i++) { 
            $quotationDataObj = $this->ObjectQuotation($idQuotationLst[$i]);
            $usersQuotationArray = $this->GetDataUsersQuotation($idQuotationLst[$i]);
            $productQuotationList[$i]["userQuotation"] = "";
            for ($u=0; $u <count($usersQuotationArray) ; $u++) { 
                if(($u+1)>=count($usersQuotationArray)){
                    $productQuotationList[$i]["userQuotation"] .= $usersQuotationArray[$u];    
                }else{
                    $productQuotationList[$i]["userQuotation"] .= $usersQuotationArray[$u].",";
                }
            }
          
            $productQuotationList[$i]["id"] = $quotationDataObj->id;
            $productQuotationList[$i]["generationDate"] = $quotationDataObj->generationDate;
            $productQuotationList[$i]["userCreate"] = $quotationDataObj->idUser;
            $productQuotationList[$i]["idCompany"] = $quotationDataObj->idCompany;
             $productQuotationList[$i]["nameCompany"] = $nameCompany;
        }
       
        return $productQuotationList;
    }

    public function ObjectQuotation($idQuotation){
        $quotationVo = new QuotationVo();
        $generalDao = new GeneralDao();
        $quotationVo->idSearchField = 0;
        $quotationVo->id =$idQuotation;
        $generalDao->GetByField($generalVo = clone $quotationVo);
        while ($quotationVo2 = $generalDao->GetVo($generalVo = clone $quotationVo)) {
           return $quotationVo2;
        }  
    }

    public function GetDataUsersQuotation($idQuotation){
         $usersQuotationList = array();
            $usersQuotationVo = new UserQuotationVo();
            $generalDao = new GeneralDao();
            $usersQuotationVo->idSearchField = 3;
            $usersQuotationVo->idQuotation = $idQuotation;
            $generalDao->GetByField($generalVo = clone $usersQuotationVo);
            while ($usersQuotationVo2 = $generalDao->GetVo($generalVo = clone $usersQuotationVo)) {
                $usersQuotationList[] = $usersQuotationVo2->completeName;
            }
            return $usersQuotationList;
    }

    public function SetDataProductQuotation(){
        $productList = array();
        $productVo = new ProductVo();
        $generalDao = new GeneralDao();
        $productVo->idSearchField = 1;
        $productVo->idProduct = $_POST["idProduct"];
        $generalDao->GetByField($generalVo = clone $productVo);
        while ($productVo2 = $generalDao->GetVo($generalVo = clone $productVo)) {
            $productList[] = $productVo2->idQuotation;
        }
        return $productList;
       // echo json_encode($productList); 
    }

        public function GetDataNameCompanyProduct(){
        
        $productVo = new InventoryVo();
        $generalDao = new GeneralDao();
        $productVo->idSearchField = 0;
        $productVo->id = $_POST["idProduct"];
        $generalDao->GetByField($generalVo = clone $productVo);
        while ($productVo2 = $generalDao->GetVo($generalVo = clone $productVo)) {
            $nameCompany= $productVo2->nameCompany;
        }
        return $nameCompany;
       // echo json_encode($productList); 
    }

   public function GetNameUserQuotation($idQuotation){
       $personCreateQuotationVo = new PersonCreateQuotationVo();
        $generalDao = new GeneralDao();
        $personCreateQuotationVo->idSearchField = 5;
        $personCreateQuotationVo->idQuotation = $idQuotation;
        $generalDao->GetByField($generalVo = clone $personCreateQuotationVo);
        while ($personCreateQuotationVo2 = $generalDao->GetVo($generalVo = clone $personCreateQuotationVo)) {
            $completeName = $personCreateQuotationVo2->completeName;
        }
        return $completeName;
   }

   public function GetNameCompany($idCompany){
        $name = null;
        $companyVo = new CompanyVo();
        $generalDao = new GeneralDao();
        $companyVo->idSearchField = 0;
        $companyVo->id = $idCompany;
        $generalDao->GetByField($generalVo = clone $companyVo);
        while ($companyVo2 = $generalDao->GetVo($generalVo = clone $companyVo)) {
            $name = $companyVo2->name;
        }
        return $name;
   }
   private function GetNumTimesForProduct($numTimesForProduct){
        echo json_encode(count($numTimesForProduct));
    }
}

$ajaxDataProduct = new AjaxDataHistoryProducts();
 ?>

 