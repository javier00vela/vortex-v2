<?php 
require_once(Config::PATH . Config::CONTROLLER . 'general/AccessManager.php');
require_once(Config::PATH . Config::CONTROLLER . 'general/UtilJquery.php');
require_once(Config::PATH . Config::VIEW_HTML . 'general/comps/GeneralCtx.php');
require_once(Config::PATH . Config::VIEW_HTML . 'general/comps/GeneralLst.php');
require_once(Config::PATH . Config::VIEW_HTML . 'general/comps/GeneralWithDataLst.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralDao.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/InventoryVo.php');
require_once(Config::PATH . Config::CONTROLLER_LIB . 'vendor/phpoffice/phpexcel/Classes/PHPExcel/IOFactory.php');
require_once (Config::PATH . Config::CONTROLLER_LIB .'vendor/dompdf/dompdf/src/Autoloader.php');
use Dompdf\Dompdf;



class HistoryProductsView{
    private $idProductInputForm;
    private $isUpdateUserForm;
    private $idDataInputForm;
    private $idProductFromGet;
    /*=============================================
                  Session Vars
    =============================================*/
  
    /*=============================================
                  Names Vars
    =============================================*/
    private $utilJQ;
    private $pageTpl, $data, $core;


/*=============================================
                  Components Vars
    =============================================*/

    private $productNameCtx;
 
    


     function __construct($pageTpl, $data, $core)
    {
        $this->data = $data;
        $this->core = $core;
        $this->pageTpl = $pageTpl;
          $this->ValidateIfExistInventory($_GET["idProductInputForm"]);
 
            $this->CreateComponents();
            $this->UseDataFromThePostback();
        
        $this->UseDataFromGet();
        $this->AssignDataToTpl();
    }

  

    private function ValidateIfExistInventory($id){
        if(empty($this->GetInventoryById($id)->id)){
            header("location: DontFind");
        }
    }


     private function GetInventoryById($id){
        $inventoryVo = new inventoryVo();
        $generalDao = new GeneralDao();
        $inventoryVo->id = $id;
        $generalDao->GetById($generalVo = clone $inventoryVo);

        if($inventoryVo2 = $generalDao->GetVo($generalVo = clone $inventoryVo)){
                return  $inventoryVo2;  
         }

      return "Error al Realizar Consulta";
    }


 

    private function UseDataFromThePostback()
    {
        if (isset($_POST['optionInputForm'])) {
            $this->optionInputForm = $_POST['optionInputForm'];
            $this->idDataInputForm = $_POST['idDataInputForm'];
            $this->generalDao = new GeneralDao();
            switch ($this->optionInputForm) {
                case "1": {
                  
                    
                    break;
                }
               
            }
        }
         

    }

     private function  UseDataFromGet(){
        $assign = '';
        if(isset($_GET["idProductInputForm"])){
            
            $this->idProductFromGet = $_GET["idProductInputForm"];
                $assign = 'SetValueToInputText("idProductInputForm",'.$this->idProductFromGet.');';
          
        }
       
        $this->utilJQ->AddFunctionJavaScript($assign);
          
    }

   

    private function CreateComponents(){  
        $this->idProductInputForm = new GeneralCtx("idProductInputForm","","hidden",false);
        $this->utilJQ = new UtilJquery("HistoryProducts");
    }

     private function AssignDataToTpl(){
        $this->data->assign('idProductInputForm', $this->idProductInputForm->paint());
        $this->data->assign('jquery', $this->utilJQ->PaintJQ());
         $this->data->assign('tempParameter', time());
        $this->SetDataToTpl();
    }


    private function SetDataToTpl() {
        echo $this->core->get($this->pageTpl, $this->data);
    }
}

 ?>