var table = null;


function BuildIPData(json) {
    var data = '';
    var tabla = '';
    var mapa = '';


    tabla += '<table class="table" ><tr><th><b>Nombre de Datos</b> </th><th><b>Resultados de datos</b></th></tr><tr><th>IP Usuario  </th><th>' + json.query + '</th></tr><tr><th>Pais  </th><th>' + json.country + '</th></tr><tr><th>Ciudad  </th><th>' + json.city + '</th></tr><tr><th>Operador  </th><th>' + json.org + '</th></tr><tr><th>Zona Horaria  </th><th>' + json.timezone + '</th></tr><tr><th>Latitud  </th><th>' + json.lat + '</th></tr><tr><th>Longitud  </th><th>' + json.lon + '</th></tr><tr><th>Codigo Del País  </th><th>' + json.countryCode + '</th></tr></table>';
    mapa += '<div id="charthiv" style="width: 100%;height: 500px;"></div>'
    data += '<div class="container-fluid">   <div class="row">       <div class="col-sm-6"><div class="box text-center"> ' + tabla + '  </div> </div>  <div class="col-sm-6"><div class="box"> <div class="map img-fluid" id="mapa" style="width: 100%;height: 330px;" ></div> </div>  </div>  </div>  </div>    <div class="container-fluid">    <div class="row "> <div class="col-sm-12"> <div class="box"> ' + mapa + '</div>       </div>   </div>  </div> </div>';
    return data;
}

function ModifyListData() {
    var options = $('#personIdRoleLst option');
    $.map(options, function(option) {
        if (option.innerHTML == "Cliente" || option.innerHTML == "Proveedor" || option.innerHTML == "Prospecto") {
            option.remove();
        }
    });
}


function SelectDataRol() {

    $("#personIdRoleLst").change(function() {
        var parameters = {
            "optionInputForm": 1,
            "idUser": $("#idUserCtx").attr("value"),
            "idRol": $(this).val()
        };
        $.ajax({
            url: "ReportHistorialPerson",
            method: "POST",
            data: parameters,
            success: function(resp) {

                var options = resp.split("*-*")[1];
                var selections = options.split("+");
                $('#quotationDeliveryLst').empty();
                $('#quotationDeliveryLst').append("<option>--Seleccione Usuario--</option>");
                for (var i = 0; i < selections.length; i++) {
                    if (selections[i] != "") {
                        $('#quotationDeliveryLst').append($('<option>', {
                            value: selections[i].split('#')[0],
                            text: selections[i].split('#')[1]
                        }));
                    }
                }
            }
        });

    });

}


function CreateExcelDocs() {
    table.destroy();
    GetAllDataTableToExcel("ReportHistorialPerson?idUser=1", "view/docs/excel/historialAccess.xlsx", "accessHistoryTbl");
}


function SetTableJson() {
    RemoveActionModule(6);
    GetArrayFromDataModule(6);
    setTimeout(function() {
        var parameters = {
            "optionInputForm": 1,
            "isActionFromAjax": true,
            "listData": arrayResponse,
            "idUser": $("#idUserCtx").attr("value")
        };
        table = $('#accessHistoryTbl').DataTable({
            language: {
                "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
            },
            "drawCallback": function(settings) {
                $('[data-toggle="tooltip"]').tooltip()
            },
            "ajax": {
                url: "view/html/modules/reporthistorialperson/AjaxDataReportHistorialPerson.php",
                method: "POST",
                data: parameters
            }
        });
    }, 500);
}

function ChangeUser() {
    RemoveActionModule(6);
    GetArrayFromDataModule(6);
    setTimeout(function() {
        $('#quotationDeliveryLst').change(function() {

            var parameters = {
                "optionInputForm": 2,
                "isActionFromAjax": true,
                "listData": arrayResponse,
                "idUser": $("#quotationDeliveryLst option:selected").val(),
            };

            table.destroy();
            table = $('#accessHistoryTbl').DataTable({
                responsive: true,
                "ajax": {
                    url: "view/html/modules/reporthistorialperson/AjaxDataReportHistorialPerson.php",
                    method: "POST",
                    data: parameters
                }

            })
        })
    }, 500);
}


function FilterByTable() {

    RemoveActionModule(6);
    GetArrayFromDataModule(6);
    setTimeout(function() {
        var parameters = {
            "optionInputForm": 3,
            "isActionFromAjax": true,
            "min": GetValueToInputText("min"),
            "max": GetValueToInputText("max"),
            "idUser": $("#quotationDeliveryLst option:selected").val(),
            "listData": arrayResponse,
        };

        table.destroy();
        table = $('#accessHistoryTbl').DataTable({
            responsive: true,
            language: {
                "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
            },
            "ajax": {
                url: "view/html/modules/reporthistorialperson/AjaxDataReportHistorialPerson.php",
                method: "POST",
                data: parameters
            }

        })

    }, 500);
}

function FilterDates() {
    $("#filter").click(function() {
        if (GetValueToInputText("min") && GetValueToInputText("min")) {
            FilterByTable();
        }
    });
}

function ValidateSelections() {
    $("#personIdRoleLst").change(function() {
        if (GetValueSelect("personIdRoleLst") == "") {
            $("#quotationDeliveryLst").attr("disabled", "true");
            $("#excelButton").attr("disabled", "true");
            $("#filter").attr("disabled", "true");
            $("#min").attr("disabled", "true");
            $("#max").attr("disabled", "true");
        } else {
            $("#quotationDeliveryLst").removeAttr("disabled");
            $("#min").removeAttr("disabled");
            $("#max").removeAttr("disabled");
            $("#filter").removeAttr("disabled");
            $("#excelButton").removeAttr("disabled");
        }
    });
}



function ViewDetailsIp(ip) {
    if (ip != "::1") {
        var parameters = {
            "isActionFromAjax": true,
            "optionInputForm": 2,
            "ip": ip,
        };
        $.ajax({
            url: "ReportHistorialPerson",
            type: "POST",
            data: parameters,
            success: function(data) {
                $(".content").empty();
                var JsonApi = JSON.parse(data.split("//////////////")[1]);
                $(".content").html(BuildIPData(JsonApi));
                var targetSVG = "M9,0C4.029,0,0,4.029,0,9s4.029,9,9,9s9-4.029,9-9S13.971,0,9,0z M9,15.93 c-3.83,0-6.93-3.1-6.93-6.93S5.17,2.07,9,2.07s6.93,3.1,6.93,6.93S12.83,15.93,9,15.93 M12.5,9c0,1.933-1.567,3.5-3.5,3.5S5.5,10.933,5.5,9S7.067,5.5,9,5.5 S12.5,7.067,12.5,9z";
                var map = AmCharts.makeChart("charthiv", {

                    "type": "map",
                    "theme": "light",
                    "projection": "miller",

                    "dataProvider": {
                        "map": "worldLow",
                        "getAreasFromMap": true,

                        "images": [{
                            "svgPath": targetSVG,
                            "zoomLevel": 5,
                            "scale": 0.5,
                            "title": JsonApi.city,
                            "latitude": JsonApi.lat,
                            "longitude": JsonApi.lon
                        }]
                    },
                    "areasSettings": {
                        "autoZoom": true,
                        "selectedColor": "#CC0000"
                    },
                    "smallMap": {},
                    "export": {
                        "enabled": true,
                        "position": "bottom-right"
                    }
                });

                var latlng = new google.maps.LatLng(JsonApi.lat, JsonApi.lon);
                map = new google.maps.Map(document.getElementById('mapa'), {
                    center: latlng,
                    zoom: 12
                });
            }
        });
    } else {
        MessageSwalBasic("SERVIDOR LOCAL", "IP designado por servidor local", "warning");
    }
}

$(function() {
    ValidateSelections();
    SetTableJson();
    ModifyListData();
    SelectDataRol();
    ChangeUser();
    FilterDates();
})