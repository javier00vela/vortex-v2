<?php
require_once(__DIR__ . '/../../../../controller/general/Config.php');
require_once(Config::PATH . Config::CONTROLLER.Config::GENERAL . 'ManageArrays.php');
require_once(Config::PATH . Config::BACKEND . 'modules/AccessHistoryVo.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralDao.php');
require_once(Config::PATH . Config::BACKEND . 'modules/UserVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/PersonVo.php');


class AjaxDataAccessHistory
{

     private $isActionFromAjax;
    private $optionInputForm;
    private $idDataInputForm;
    public $dataList = [];
    

    function __construct(){
       
           $this->isActionFromAjax = $_POST['isActionFromAjax'];
            $this->optionInputForm = $_POST['optionInputForm'];
            if(isset($_POST["listData"])){
                $this->dataList = $_POST["listData"];  
            }
            if ($this->isActionFromAjax == "true") {
                if ($this->optionInputForm == 1) {
                     $this->GetDataInJsonForTbl();
                }else if($this->optionInputForm == 2){
                    $this->GetDataInJsonByPerson();
                }else if($this->optionInputForm == 3){
                    $this->GetDataInJsonByDates();
                }
            }
    }

     private function GetNamePerson($idUser){
    
        $personVo = new PersonVo();
        $personVo->id = $idUser;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $personVo);
            if($personVo2 = $generalDao->GetVo($generalVo = clone $personVo)){
                if($personVo2->state == 1){
                return  htmlspecialchars($personVo2->names." ".$personVo2->lastNames);
                }
            }    
     }


    private function GetNameUser($idUser){
        $userVo = new UserVo();
        $personVo = new PersonVo();
        $userVo->id = $idUser;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $userVo);
        if($userVo2 = $generalDao->GetVo($generalVo = clone $userVo)){
            $personVo->id = $userVo2->idPerson;
            $generalDao->GetById($generalVo = clone $personVo);
            if($personVo2 = $generalDao->GetVo($generalVo = clone $personVo)){
                if($personVo2->state == 1){
                return  htmlspecialchars($personVo2->names." ".$personVo2->lastNames);
                }
            }    
        }
     }
    

    public function GetDataInJsonForTbl()
    {
        $existData = false;
        $accessHistoryVo = new AccessHistoryVo();
        $accessHistoryVo->isList = true;
        $accessHistoryVo->idSearchField = 7;
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $accessHistoryVo);

        $string = "
    {\"data\": [";
        while ($accessHistoryVo2 = $generalDao->GetVo($generalVo = clone $accessHistoryVo)) {
            $json = array();
           // if($this->GetNameUser($accessHistoryVo2->idUser)){
            $existData = true;
            $json[] = $accessHistoryVo2->id;
            $json[] = $accessHistoryVo2->login;
            $json[] = $accessHistoryVo2->logout;
            $json[] = $accessHistoryVo2->numHours;
            $json[] = $accessHistoryVo2->ipAddress;
            $json[] = $accessHistoryVo2->device;
            $json[] = $accessHistoryVo2->browser;
            $json[] = $accessHistoryVo2->idUser;
            $json[] = htmlspecialchars($this->GetNamePerson($accessHistoryVo2->idUser));
            $button = "";
            if(!in_array( "ver_ip_usuario" , $this->dataList)){
                $button .= "<button id='{$accessHistoryVo2->id}viewIpBtn' ". "onclick='ViewDetailsIp(\"$accessHistoryVo2->ipAddress\" )' data-toggle='tooltip'  data-placement='top' name='{$accessHistoryVo2->id}viewIpBtn' class='btn btn-primary btn-xs none blue-tooltip'  title='ver IP'><i class='fa fa-map-marker' ></i></button>";
            }
            $json[] =  $button;
            $string .= json_encode($json) . ",";
          //}
        }
        $string .= "-]}-";

        if($existData){
            echo  str_replace(",-]}-", "]}", $string);
        }else{
            echo "{ \"data\": [] }";
        }

    }


    public function GetDataInJsonByDates()
    {
        $existData = false;
        $accessHistoryVo = new AccessHistoryVo();
        $accessHistoryVo->isList = true;
        $accessHistoryVo->idSearchField = 7;
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $accessHistoryVo);

        $string = "
    {\"data\": [";
        while ($accessHistoryVo2 = $generalDao->GetVo($generalVo = clone $accessHistoryVo)) {
        if(isset($_POST["idUser"])){
            // if(date("o-m-d", strtotime($quotationVo2->generationDate)) <= date("o-m-d", strtotime($_POST["max"])) && date("o-m-d", strtotime($quotationVo2->generationDate)) >= date("o-m-d", strtotime($_POST["min"])) ){
             if(date("o-m-d", strtotime($accessHistoryVo2->login)) <= date("o-m-d", strtotime($_POST["max"])) && date("o-m-d", strtotime($accessHistoryVo2->logout)) >= date("o-m-d", strtotime($_POST["min"]))){
                if($_POST["idUser"] == $accessHistoryVo2->idUser){
            $existData = true;
            $json = array();
            $json[] = $accessHistoryVo2->id;
            $json[] = $accessHistoryVo2->login;
            $json[] = $accessHistoryVo2->logout;
            $json[] = $accessHistoryVo2->numHours;
            $json[] = $accessHistoryVo2->ipAddress;
            $json[] = $accessHistoryVo2->device;
            $json[] = $accessHistoryVo2->browser;
            $json[] = $accessHistoryVo2->idUser;
            $json[] = htmlspecialchars($this->GetNamePerson($accessHistoryVo2->idUser));
            $json[] = "<button id='{$accessHistoryVo2->id}viewIpBtn' ". "onclick='ViewDetailsIp(\"$accessHistoryVo2->ipAddress\" )' data-toggle='tooltip'  name='{$accessHistoryVo2->id}viewIpBtn' class='btn btn-primary btn-xs none blue-tooltip'  title='ver IP'><i class='fa fa-map-marker' ></i></button>";
            $string .= json_encode($json) . ",";
            }
          }
        }else{
          if($accessHistoryVo2->login >= $_POST["min"] && $accessHistoryVo2->logout <= $_POST["max"]){
            $existData = true;
            $json = array();
            $json[] = $accessHistoryVo2->id;
            $json[] = $accessHistoryVo2->login;
            $json[] = $accessHistoryVo2->logout;
            $json[] = $accessHistoryVo2->numHours;
            $json[] = $accessHistoryVo2->ipAddress;
            $json[] = $accessHistoryVo2->device;
            $json[] = $accessHistoryVo2->browser;
            $json[] = $accessHistoryVo2->idUser;
            $json[] = htmlspecialchars($this->GetNamePerson($accessHistoryVo2->idUser));
            $button = '';
            if(!in_array( "ver_ip_usuario" , $this->dataList)){
            $button .= "<button id='{$accessHistoryVo2->id}viewIpBtn' ". "onclick='ViewDetailsIp(\"$accessHistoryVo2->ipAddress\" )' data-toggle='tooltip' name='{$accessHistoryVo2->id}viewIpBtn' class='btn btn-primary btn-xs none blue-tooltip'  title='ver IP'><i class='fa fa-map-marker' ></i></button>";
            }
            $string .= json_encode($json) . ",";
            }
        }
    }
        $string .= "-]}-";

        if($existData){
            echo  str_replace(",-]}-", "]}", $string);
        }else{
            echo "{ \"data\": [] }";
        }

    }



 public function GetDataInJsonByPerson()
    {
        $existData = false;
        $accessHistoryVo = new AccessHistoryVo();
        $accessHistoryVo->isList = true;
        $accessHistoryVo->idSearchField = 7;
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $accessHistoryVo);

        $string = "
    {\"data\": [";
        while ($accessHistoryVo2 = $generalDao->GetVo($generalVo = clone $accessHistoryVo)) {
            if($_POST["idUser"] == $accessHistoryVo2->idUser){
            $existData = true;
            $json = array();
            $json[] = $accessHistoryVo2->id;
            $json[] = $accessHistoryVo2->login;
            $json[] = $accessHistoryVo2->logout;
            $json[] = $accessHistoryVo2->numHours;
            $json[] = $accessHistoryVo2->ipAddress;
            $json[] = $accessHistoryVo2->device;
            $json[] = $accessHistoryVo2->browser;
            $json[] = $accessHistoryVo2->idUser;
            $json[] = htmlspecialchars($this->GetNamePerson($accessHistoryVo2->idUser));
            $button = "";
            if(!in_array( "ver_ip_usuario" , $this->dataList)){
                $button .= "<button id='{$accessHistoryVo2->id}viewIpBtn' ". "onclick='ViewDetailsIp(\"$accessHistoryVo2->ipAddress\" )' data-toggle='tooltip' name='{$accessHistoryVo2->id}viewIpBtn' class='btn btn-primary btn-xs none blue-tooltip'  title='ver IP'><i class='fa fa-map-marker' ></i></button>";
            }
            $json[] = $button;
            $string .= json_encode($json) . ",";
            }
        }
        $string .= "-]}-";

        if($existData){
            echo  str_replace(",-]}-", "]}", $string);
        }else{
            echo "{ \"data\": [] }";
        }

    }


}

$ajaxDataReportQuotation = new AjaxDataAccessHistory();
