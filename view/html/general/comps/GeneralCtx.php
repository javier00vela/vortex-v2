<?php
require_once(Config::PATH . Config::CONTROLLER_LIB . 'PCampoTexto.php');

class GeneralCtx {

    public $ctx;
    private $placeHolder;
    private $isRequired;
    private $typeInput;

    public function __construct($idOrNameCtx,$placeHolder,$typeInput,$isRequired) {
        $this->isRequired = $isRequired;
        $this->placeHolder = $placeHolder;
        $this->typeInput = $typeInput;
        $this->ctx = new PCampoTexto($idOrNameCtx);
    }

    public function Paint() {
        if(isset($this->typeInput)){
            $this->ctx->adiPropiedad("type", $this->typeInput);
        }

        $this->ctx->adiPropiedad("class", "form-control");
        $this->ctx->adiPropiedad("maxlength", "50");
        if($this->isRequired){
          $this->ctx->adiPropiedad("required", "");
        }
        $this->ctx->adiPropiedad("placeholder", $this->placeHolder);
        return $this->ctx->conHtm();
    }
}
