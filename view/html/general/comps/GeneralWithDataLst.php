<?php
require_once(Config::PATH . Config::CONTROLLER_LIB . 'PLista.php');

class GeneralWithDataLst {

    public $lst;
    private $isIndexOfDataTheValue;
    private $isIndexFromDataBase;
    private $data;
    private $placeHolder;
    private $isRequired;

    public function __construct($data,$idLst,$isIndexOfDataTheValue,$isIndexFromDataBase,$placeHolder,$isRequired) {
        $this->data = $data;
        $this->isIndexOfDataTheValue = $isIndexOfDataTheValue;
        $this->isIndexFromDataBase = $isIndexFromDataBase;
        $this->placeHolder = $placeHolder;
        $this->isRequired = $isRequired;
        $this->lst = new PLista($idLst);
    }

    public function Paint(){
        $this->lst->adiPropiedad("class", "form-control");
        $this->lst->modControlador($this, "GetItem");
        $this->lst->titulo = $this->placeHolder;
        $this->lst->IsRequired($this->isRequired);
        return $this->lst->conHtm();
    }

    public function GetItem($itemNum) {
      for ($i=0; $i < count($this->data); $i++) {
        $elements = "";
        if($itemNum == ($i+1)){
          $item = new PListaItem();
          if($this->isIndexOfDataTheValue){
            $item->value = $i+1;
            $item->texto = $this->data[$i];
          }else {
              if($this->isIndexFromDataBase){
                  $elements = explode("_", $this->data[$i]); // separates the id from the data
                  $item->value = $elements[0];
                  $item->texto = $elements[1];
              }else{
                  $item->value = $this->data[$i];
                  $item->texto = $this->data[$i];
              }
          }
          return $item;
        }
      }
      return false;
    }
}
