<?php
require_once(Config::PATH . Config::CONTROLLER_LIB . 'PLista.php');
require_once (Config::PATH . Config::BACKEND . 'general/GeneralDao.php');
require_once (Config::PATH . Config::BACKEND . 'general/GeneralVo.php');

class GeneralLst {

    public $lst;
    private $fieldValue;
    private $fieldOption;
    private $placeHolder;
    private $isRequired;
    private $generalVo;
    private $generalDao;

    public function __construct($generalVo,$idLst,$fieldValue,$fieldOption,$placeHolder,$isRequired) {
        $this->generalVo = $generalVo;
        $this->fieldValue = $fieldValue;
        $this->fieldOption = $fieldOption;
        $this->placeHolder = $placeHolder;
        $this->isRequired = $isRequired;
        $this->lst = new PLista($idLst);
    }

    public function Paint(){
        $this->generalDao = new GeneralDao();
        $this->generalDao->GetById($this->generalVo);
        $this->lst->titulo = $this->placeHolder;
        $this->lst->adiPropiedad("class", "form-control");
        $this->lst->IsRequired($this->isRequired);
        $this->lst->modControlador($this, "GetItem");
        return $this->lst->conHtm();
    }

    public function GetItem() {
        while ($generalVo2 = $this->generalDao->GetVo($this->generalVo)) {
            $item = new PListaItem();
            $item->value = $generalVo2->{$generalVo2->namesFieldsArray[$this->fieldValue]};
            $item->texto = $generalVo2->{$generalVo2->namesFieldsArray[$this->fieldOption]};
            return $item;
        }
        return false;
    }
}
