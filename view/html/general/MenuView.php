<?php

$htmlMenu = '
    <aside class="main-sidebar">
    <section class="sidebar">
            <ul class="sidebar-menu tree" data-widget="tree">
            <li >
                    <!--<a href="tEST">-->
                    <a href="Home">
                        <i class="fa fa-home"></i>
                        <span>Inicio</span>
                    </a>
                </li>
                <li node="main-2">
                    <a class="field-nav" href="Users" >
                        <i class="fa fa-user " ></i>
                        <span>Usuarios</span>
                        <span class="pull-right-container">
                            <span class="userLenght label label-primary pull-right">0</span>
                        </span>
                    </a>
                </li>
                <li node="main-17">
                <a class="field-nav" href="Prospect" >
                    <i class="fa fa-user-circle"></i>
                    <span>Prospectos</span>
                    <span class="pull-right-container">
                        <span class="ProspectosLength label label-primary pull-right">0</span>
                    </span>

                </a>
               </li>
               <li node="main-12">
                <a class="field-nav" href="Clients" >
                    <i class="fa fa-user-circle"></i>
                    <span>Contactos</span>
                    <span class="pull-right-container">
                        <span class="ClientsLength label label-primary pull-right">0</span>
                    </span>

                </a>
               </li>
                <li node="main-1">
                    <!--<a class="field-nav" href="Products" >-->
                    <a href="Inventory">
                        <i class="fa fa-product-hunt"></i>
                        <span>Inventario</span>
                        <span class="pull-right-container">
                            <span class="inventoryLenght label label-primary pull-right">0</span>
                        </span>
                    </a>
                </li><li node="main-3">
                        <!--<a href="Company">-->
                        <a class="field-nav" href="Company" >
                            <i class="fa fa-industry"></i>
                            <span>Empresa</span>
                            <span class="pull-right-container">
                                 <span class="CompanysLength label label-primary pull-right">0</span>
                             </span>

                        </a>
                    </li>
                    <li class="field-nav" class="treeview" >

                        <li node="main-20">
                            <a href="Providers">
                                <i class="fa fa-institution"></i>
                                <span>Proveedores</span>
                                <span class="pull-right-container">
                            <span class="proveedoresLength label label-primary pull-right">0</span>
                        </span>

                            </a>
                        </li>

                    <li node="main-9">
                        <a class="field-nav" href="Impost" >
                            <i class="fa fa-percent"></i>
                            <span>Costos</span>
                             <span class="pull-right-container">
                            <span class="impostLength label label-primary pull-right">0</span>
                        </span>
                        </a>
                    </li>
                    <li node="main-32">
                        <a class="field-nav" href="MyCalendar" >
                            <i class="fa fa-calendar"></i>
                            <span>Mi Calendario</span>
                             <span class="pull-right-container">
                        </span>
                        </a>
                    </li>
                    <li node="main-14">
                    <a class="field-nav" href="Visits" >
                        <i class="fa fa-users"></i>
                        <span>Visitas</span>
                          <span class="pull-right-container">
                            <span class="visitsLength label label-primary pull-right">0</span>
                        </span>
                    </a>
                   </li>

                   <li class="treeview " >
                   <a class="field-nav" href="#">
                       <i class="fa fa-cube"></i>
                       <span>Mantenimiento</span>
                       <span class="pull-right-container">
                           <i class="fa fa-angle-left pull-right"></i>
                       </span>
                   </a>
                   <ul class="treeview-menu" >
                           <li node="main-27">
                           <!--<a href="">-->
                           <a class="field-nav" href="Maintenance" >
                               <i class="fa fa-circle-o"></i>
                               <span>Lista Equipos</span>
                           </a>
                       </li>
                       <li node="main-28">
                           <!--<a href="">-->
                           <a class="field-nav" href="ListDiagnosis" >
                               <i class="fa fa-circle-o"></i>
                               <span>Lista Diagnosticos</span>
                           </a>
                       </li>
                       <li node="main-33">
                            <!--<a href="">-->
                            <a class="field-nav" href="PricesMaintence" >
                                <i class="fa fa-circle-o"></i>
                                <span>Precios Proceso</span>
                            </a>
                        </li>
                        <li node="main-33">
                        <!--<a href="">-->
                        <a class="field-nav" href="Viatics" >
                        <i class="fa fa-circle-o"></i>
                            <span>Costos de Viaticos</span>
                        </a>
                    </li>
                        <li node="main-35">
                        <!--<a href="">-->
                        <a class="field-nav" href="ListQuotesMainteince" >
                            <i class="fa fa-circle-o"></i>
                            <span>Lista de Cotizaciones</span>
                        </a>
                    </li>
                    <li node="main-38">
                        <!--<a href="">-->
                        <a class="field-nav" href="QuoteMainteince?idMainteince=null&nameCompany=null" >
                        <i class="fa fa-circle-o"></i>
                            <span>Cotizar Mantenimiento</span>
                        </a>
                    </li>
                   </ul>
               </li>
                    <li class="treeview " >
                    <a class="field-nav" href="#">
                        <i class="fa fa-list-ul"></i>
                        <span>Cotizaciones</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu" >
                            <li node="main-5">
                            <!--<a href="">-->
                            <a class="field-nav" href="Quotation" >
                                <i class="fa fa-circle-o"></i>
                                <span>Nueva Cotización</span>
                            </a>
                        </li>
                        <li node="main-4">
                            <!--<a href="">-->
                            <a class="field-nav" href="GenerateQuotes" >
                                <i class="fa fa-circle-o"></i>
                                <span>Consultar Cotización</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li node="main-35">
                <a class="field-nav" href="BuyListQuotes" >
                    <i class="fa fa-barcode"></i>
                    <span>Ordenes de Compra</span>
                </span>
                </a>
            </li>
                <li class="treeview" >
                    <!--<a href="#">-->
                        <a class="field-nav" href="#" >
                            <i class="fa fa-bar-chart"></i>
                            <span>Reportes</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                         </a>
                    <ul class="treeview-menu">
                    <li class="treeview menu-open">
                      <a href="#"><i class="fa fa-angle-right"></i> Reportes De Personas
                        <span class="pull-right-container">
                          <i class="fa fa-angle-left pull-right"></i>
                        </span>
                      </a>
                      <ul class="treeview-menu" style="display: block;">
                        <li node="main-6"><a class="field-nav" href="ReportHistorialPerson" ><i class="fa fa-circle-o"></i> Historial Acceso</a></li>
                        <li class="treeview">
                      </li>
                  </ul>
                 </li>
                  <li class="treeview menu-open">
                      <a href="#" ><i class="fa fa-angle-right"></i> Reportes Cotizaciones
                        <span class="pull-right-container">
                          <i class="fa fa-angle-left pull-right"></i>
                        </span>
                      </a>
                        <ul class="treeview-menu" style="display: block;">
                            <li node="main-7">
                                <a href="ReportQuotation" class="field-nav" ><i class="fa fa-circle-o"></i>reportes graficos</a>
                            </li>
                        <ul>
                      </li>
                    </ul>
                    </li>
                </ul>
            </li>
        </ul>

            <li class="treeview" >
                        <a class="field-nav" href="#" >
                            <i class="fa fa-cog"></i>
                            <span>Configuración</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                         </a>
                    <ul class="treeview-menu">
                    <li class="treeview menu-open">
                      <a href="#"><i class="fa fa-angle-right"></i> Permisos De Correos
                        <span class="pull-right-container">
                        </span>
                      </a>
                      <ul class="treeview-menu menu-open" style="display: block;" >
                        <li node="main-26"><a class="field-nav" href="Mails" >
                        <i class="fa fa-circle-o"></i> Correos Generales</a></li>
                        <li node="main-6"><a class="field-nav" href="MailsAllows" >
                        <i class="fa fa-circle-o"></i> Alertas </a></li>
                        <li class="treeview">
                      </li>
                  </ul>
                <li node="main-25">
                  <!--<a href="">-->
                  <a class="field-nav" href="Code" >
                      <i class="fa fa-circle-o"></i>
                      <span>Codigos Descuentos</span>
                  </a>
              </li>
                  <li node="main-23">

                  <a class="field-nav" href="CalendarTRM" >
                      <i class="fa fa-circle-o"></i>
                      <span>Calendario TRM</span>
                  </a>
              </li>
              <li node="main-18">

              <a class="field-nav" href="Modules" >
                  <i class="fa fa-circle-o"></i>
                  <span>Modulos</span>
              </a>
          </li>
                 </li>
                    </ul>
                        </li>
                    </ul>
                </li>
            </ul>
         </section>
       </aside>
    ';

echo $htmlMenu;
