<?php
$pageTpl;
$data = new Dwoo\Data();
$core = new Dwoo\Core();
require_once(Config::PATH . Config::CONTROLLER . 'general/AccessManager.php');

if(!isset($_SESSION["startSession"])){
    if(!isset($_GET["RestURL"])){
    $url = "Login";
    $pageTpl = new Dwoo\Template\File(Config::PATH . Config::MODULES_VIEW . '' . strtolower($url) . '/' . $url . 'View.tpl');
  
    require_once(Config::PATH . Config::MODULES_VIEW . '' . strtolower($url) . '/' . $url . 'View.php');
    $view = new LoginView($pageTpl, $data, $core);
    }else{
        require_once Config::PATH . Config::VIEW_HTML_MODULES . "rest/inventoryRestService.php";
         $service = new InventoryRestService();
         $service->SendJSONproducts();
    }
}else if(isset($_SESSION["startSession"])){
    $existPage = false;
    if(isset($_GET["url"])){
        $isAllow = false;
          $AccessManager = new AccessManager($_GET["url"]);
        $url = $_GET["url"];
        $pageTpl = new Dwoo\Template\File(Config::PATH . Config::MODULES_VIEW . '' . strtolower($url) . '/' . $url . 'View.tpl');
         
  
        switch ($url) {
            case "Home": {
                $existPage = true;
                echo '<div class="wrapper">';
                include Config::PATH . Config::VIEW_HTML . "general/HeaderView.php";
                include Config::PATH . Config::VIEW_HTML . "general/MenuView.php";
                include Config::PATH . Config::VIEW_HTML_MODULES . "home/HomeView.php";
                include Config::PATH . Config::VIEW_HTML . "general/FooterView.php";
                echo '</div>';
                break;
            }
            case "Users": {
                $existPage = true;
                echo '<div class="wrapper">';
                include Config::PATH . Config::VIEW_HTML . "general/HeaderView.php";
                include Config::PATH . Config::VIEW_HTML . "general/MenuView.php";
                 $isAllow = $AccessManager->validateURL();
                 if($isAllow){
                    $view = new UsersView($pageTpl, $data, $core);
                }else{
                     include Config::PATH . Config::VIEW_HTML . "general/DenegateAccess.php";
                }
                include Config::PATH . Config::VIEW_HTML . "general/FooterView.php";
                echo '</div>';
                break;
            }
            case "ConfigData": {
                $existPage = true;
                echo '<div class="wrapper">';
                include Config::PATH . Config::VIEW_HTML . "general/HeaderView.php";
                include Config::PATH . Config::VIEW_HTML . "general/MenuView.php";
                 $isAllow = $AccessManager->validateURL();
                 if($isAllow){
                    $view = new ConfigDataView($pageTpl, $data, $core);
                }else{
                     include Config::PATH . Config::VIEW_HTML . "general/DenegateAccess.php";
                }
                include Config::PATH . Config::VIEW_HTML . "general/FooterView.php";
                echo '</div>';
                break;
            }


            case "ExpensesViatics": {
                $existPage = true;
                echo '<div class="wrapper">';
                include Config::PATH . Config::VIEW_HTML . "general/HeaderView.php";
                include Config::PATH . Config::VIEW_HTML . "general/MenuView.php";
                 $isAllow = $AccessManager->validateURL();
                 if($isAllow){
                    $view = new ExpensesViaticsView($pageTpl, $data, $core);
                }else{
                     include Config::PATH . Config::VIEW_HTML . "general/DenegateAccess.php";
                }
                include Config::PATH . Config::VIEW_HTML . "general/FooterView.php";
                echo '</div>';
                break;
            }



            case "BuyListQuotes": {
                $existPage = true;
                echo '<div class="wrapper">';
                include Config::PATH . Config::VIEW_HTML . "general/HeaderView.php";
                include Config::PATH . Config::VIEW_HTML . "general/MenuView.php";
                 $isAllow = $AccessManager->validateURL();
                 if($isAllow){
                    $view = new BuyListQuotesView($pageTpl, $data, $core);
                }else{
                     include Config::PATH . Config::VIEW_HTML . "general/DenegateAccess.php";
                }
                include Config::PATH . Config::VIEW_HTML . "general/FooterView.php";
                echo '</div>';
                break;
            }
            case "BuyListQuotesCompany": {
                $existPage = true;
                echo '<div class="wrapper">';
                include Config::PATH . Config::VIEW_HTML . "general/HeaderView.php";
                include Config::PATH . Config::VIEW_HTML . "general/MenuView.php";
                 $isAllow = $AccessManager->validateURL();
                 if($isAllow){
                    $view = new BuyListQuotesCompanyView($pageTpl, $data, $core);
                }else{
                     include Config::PATH . Config::VIEW_HTML . "general/DenegateAccess.php";
                }
                include Config::PATH . Config::VIEW_HTML . "general/FooterView.php";
                echo '</div>';
                break;
            }
              case "Visits": {
                $existPage = true;
                echo '<div class="wrapper">';
                include Config::PATH . Config::VIEW_HTML . "general/HeaderView.php";
                include Config::PATH . Config::VIEW_HTML . "general/MenuView.php";
                 $isAllow = $AccessManager->validateURL();
                 if($isAllow){
                    $view = new VisitsView($pageTpl, $data, $core);
                }else{
                     include Config::PATH . Config::VIEW_HTML . "general/DenegateAccess.php";
                }
                include Config::PATH . Config::VIEW_HTML . "general/FooterView.php";
                echo '</div>';
                break;
            }
            case "VisitsDescription": {
                $existPage = true;
                echo '<div class="wrapper">';
                include Config::PATH . Config::VIEW_HTML . "general/HeaderView.php";
                include Config::PATH . Config::VIEW_HTML . "general/MenuView.php";
                 $isAllow = $AccessManager->validateURL();
                 if($isAllow){
                    $view = new VisitsDescriptionView($pageTpl, $data, $core);
                }else{
                     include Config::PATH . Config::VIEW_HTML . "general/DenegateAccess.php";
                }
                include Config::PATH . Config::VIEW_HTML . "general/FooterView.php";
                echo '</div>';
                break;
            }
            case "MailsAllows": {
                $existPage = true;
                echo '<div class="wrapper">';
                include Config::PATH . Config::VIEW_HTML . "general/HeaderView.php";
                include Config::PATH . Config::VIEW_HTML . "general/MenuView.php";
                 $isAllow = $AccessManager->validateURL();
                 if($isAllow){
                    $view = new MailsAllowsView($pageTpl, $data, $core);
                }else{
                     include Config::PATH . Config::VIEW_HTML . "general/DenegateAccess.php";
                }
                include Config::PATH . Config::VIEW_HTML . "general/FooterView.php";
                echo '</div>';
                break;
            }
            case "Inventory":{
                $existPage = true;
                echo '<div class="wrapper">';
                include Config::PATH . Config::VIEW_HTML . "general/HeaderView.php";
                include Config::PATH . Config::VIEW_HTML . "general/MenuView.php";
                $isAllow = $AccessManager->validateURL();
                 if($isAllow){
                    $view = new InventoryView($pageTpl, $data, $core);
                }else{
                     include Config::PATH . Config::VIEW_HTML . "general/DenegateAccess.php";
                }
                include Config::PATH . Config::VIEW_HTML . "general/FooterView.php";
                echo '</div>';
                break;
            }
             case "InventoryPrice":{
                $existPage = true;
                echo '<div class="wrapper">';
                include Config::PATH . Config::VIEW_HTML . "general/HeaderView.php";
                include Config::PATH . Config::VIEW_HTML . "general/MenuView.php";
                $isAllow = $AccessManager->validateURL();
                   require(Config::PATH . Config::MODULES_VIEW . '' . strtolower('inventoryPrice') . '/' . 'InventoryPrice' . 'View.php');
                    $view = new InventoryPriceView($pageTpl, $data, $core);
                 /*if($isAllow){
                   
                }else{
                     include Config::PATH . Config::VIEW_HTML . "general/DenegateAccess.php";
                }*/
                include Config::PATH . Config::VIEW_HTML . "general/FooterView.php";
                echo '</div>';
                break;
            }
              case "HistoryProducts":{
                $existPage = true;
                echo '<div class="wrapper">';
                include Config::PATH . Config::VIEW_HTML . "general/HeaderView.php";
                include Config::PATH . Config::VIEW_HTML . "general/MenuView.php";
                $isAllow = $AccessManager->validateURL();
                 if($isAllow){
                    $view = new HistoryProductsView($pageTpl, $data, $core);
                }else{
                     include Config::PATH . Config::VIEW_HTML . "general/DenegateAccess.php";
                }
                include Config::PATH . Config::VIEW_HTML . "general/FooterView.php";
                echo '</div>';
                break;
            }
            
            case "ModifyEventsStatics":{
                $existPage = true;
                echo '<div class="wrapper">';
                include Config::PATH . Config::VIEW_HTML . "general/HeaderView.php";
                include Config::PATH . Config::VIEW_HTML . "general/MenuView.php";
                 $isAllow = $AccessManager->validateURL();
                 if($isAllow){
                $view = new ModifyEventsStaticsView($pageTpl, $data, $core);
                 }else{
                     include Config::PATH . Config::VIEW_HTML . "general/DenegateAccess.php";
                }
                include Config::PATH . Config::VIEW_HTML . "general/FooterView.php";
                echo '</div>';
                break;
            }

            case "BuyListQuotesClient":{
                $existPage = true;
                echo '<div class="wrapper">';
                include Config::PATH . Config::VIEW_HTML . "general/HeaderView.php";
                include Config::PATH . Config::VIEW_HTML . "general/MenuView.php";
                 $isAllow = $AccessManager->validateURL();
                 if($isAllow){
                $view = new BuyListQuotesClientView($pageTpl, $data, $core);
                 }else{
                     include Config::PATH . Config::VIEW_HTML . "general/DenegateAccess.php";
                }
                include Config::PATH . Config::VIEW_HTML . "general/FooterView.php";
                echo '</div>';
                break;
            }

            case "Serial":{
                $existPage = true;
                echo '<div class="wrapper">';
                include Config::PATH . Config::VIEW_HTML . "general/HeaderView.php";
                include Config::PATH . Config::VIEW_HTML . "general/MenuView.php";
                 $isAllow = $AccessManager->validateURL();
                 if($isAllow){
                $view = new InventoryView($pageTpl, $data, $core);
                 }else{
                     include Config::PATH . Config::VIEW_HTML . "general/DenegateAccess.php";
                }
                include Config::PATH . Config::VIEW_HTML . "general/FooterView.php";
                echo '</div>';
                break;
            }
            case "Products":{
                $existPage = true;
                echo '<div class="wrapper">';
                include Config::PATH . Config::VIEW_HTML . "general/HeaderView.php";
                include Config::PATH . Config::VIEW_HTML . "general/MenuView.php";
                 $isAllow = $AccessManager->validateURL();
                 if($isAllow){
                $view = new ProductsView($pageTpl, $data, $core);
                 }else{
                     include Config::PATH . Config::VIEW_HTML . "general/DenegateAccess.php";
                }
                include Config::PATH . Config::VIEW_HTML . "general/FooterView.php";
                echo '</div>';
                break;
            }
            case "Products2":{
                $existPage = true;
                echo '<div class="wrapper">';
                include Config::PATH . Config::VIEW_HTML . "general/HeaderView.php";
                include Config::PATH . Config::VIEW_HTML . "general/MenuView.php";
                 $isAllow = $AccessManager->validateURL();
                 if($isAllow){
                $view = new Products2View($pageTpl, $data, $core);
                 }else{
                     include Config::PATH . Config::VIEW_HTML . "general/DenegateAccess.php";
                }
                include Config::PATH . Config::VIEW_HTML . "general/FooterView.php";
                echo '</div>';
                break;
            }
            case "Company":{
                $existPage = true;
                echo '<div class="wrapper">';
                include Config::PATH . Config::VIEW_HTML . "general/HeaderView.php";
                include Config::PATH . Config::VIEW_HTML . "general/MenuView.php";
                 $isAllow = $AccessManager->validateURL();
                 if($isAllow){
                $view = new CompanyView($pageTpl, $data, $core);
                 }else{
                     include Config::PATH . Config::VIEW_HTML . "general/DenegateAccess.php";
                }
                include Config::PATH . Config::VIEW_HTML . "general/FooterView.php";
                echo '</div>';
                break;
            }
            case "Contacts":{
                $existPage = true;
                echo '<div class="wrapper">';
                include Config::PATH . Config::VIEW_HTML . "general/HeaderView.php";
                include Config::PATH . Config::VIEW_HTML . "general/MenuView.php";
                 $isAllow = $AccessManager->validateURL();
                 if($isAllow){
                $view = new ContactsView($pageTpl, $data, $core);
                 }else{
                     include Config::PATH . Config::VIEW_HTML . "general/DenegateAccess.php";
                }
                include Config::PATH . Config::VIEW_HTML . "general/FooterView.php";
                echo '</div>';
                break;
            }
            case "Impost":{
                $existPage = true;
                echo '<div class="wrapper">';
                include Config::PATH . Config::VIEW_HTML . "general/HeaderView.php";
                include Config::PATH . Config::VIEW_HTML . "general/MenuView.php";
                 $isAllow = $AccessManager->validateURL();
                 if($isAllow){
                $view = new ImpostView($pageTpl, $data, $core);
                 }else{
                     include Config::PATH . Config::VIEW_HTML . "general/DenegateAccess.php";
                }
                include Config::PATH . Config::VIEW_HTML . "general/FooterView.php";
                echo '</div>';
                break;
            }
            case "Viatics":{
                $existPage = true;
                echo '<div class="wrapper">';
                include Config::PATH . Config::VIEW_HTML . "general/HeaderView.php";
                include Config::PATH . Config::VIEW_HTML . "general/MenuView.php";
                 $isAllow = $AccessManager->validateURL();
                 if($isAllow){
                $view = new ViaticsView($pageTpl, $data, $core);
                 }else{
                     include Config::PATH . Config::VIEW_HTML . "general/DenegateAccess.php";
                }
                include Config::PATH . Config::VIEW_HTML . "general/FooterView.php";
                echo '</div>';
                break;
            } 
            case "ListViatics":{
                $existPage = true;
                echo '<div class="wrapper">';
                include Config::PATH . Config::VIEW_HTML . "general/HeaderView.php";
                include Config::PATH . Config::VIEW_HTML . "general/MenuView.php";
                 $isAllow = $AccessManager->validateURL();
                 if($isAllow){
                $view = new ListViaticsView($pageTpl, $data, $core);
                 }else{
                     include Config::PATH . Config::VIEW_HTML . "general/DenegateAccess.php";
                }
                include Config::PATH . Config::VIEW_HTML . "general/FooterView.php";
                echo '</div>';
                break;
            } 

            case "ListQuotesMainteince":{
                $existPage = true;
                echo '<div class="wrapper">';
                include Config::PATH . Config::VIEW_HTML . "general/HeaderView.php";
                include Config::PATH . Config::VIEW_HTML . "general/MenuView.php";
                 $isAllow = $AccessManager->validateURL();
                 if($isAllow){
                $view = new ListQuotesMainteinceView($pageTpl, $data, $core);
                 }else{
                     include Config::PATH . Config::VIEW_HTML . "general/DenegateAccess.php";
                }
                include Config::PATH . Config::VIEW_HTML . "general/FooterView.php";
                echo '</div>';
                break;
            } 

            

            
            case "MyCalendar":{
                $existPage = true;
                echo '<div class="wrapper">';
                include Config::PATH . Config::VIEW_HTML . "general/HeaderView.php";
                include Config::PATH . Config::VIEW_HTML . "general/MenuView.php";
                 $isAllow = $AccessManager->validateURL();
                 if($isAllow){
                $view = new MyCalendarView($pageTpl, $data, $core);
                 }else{
                     include Config::PATH . Config::VIEW_HTML . "general/DenegateAccess.php";
                }
                include Config::PATH . Config::VIEW_HTML . "general/FooterView.php";
                echo '</div>';
                break;
            }            
            case "AccessHistory": {
                $existPage = true;
                echo '<div class="wrapper">';
                include Config::PATH . Config::VIEW_HTML . "general/HeaderView.php";
                include Config::PATH . Config::VIEW_HTML . "general/MenuView.php";
                 $isAllow = $AccessManager->validateURL();
                 if($isAllow){
                $view = new AccessHistoryView($pageTpl, $data, $core);
                 }else{
                     include Config::PATH . Config::VIEW_HTML . "general/DenegateAccess.php";
                }
                include Config::PATH . Config::VIEW_HTML . "general/FooterView.php";
                echo '</div>';
                break;
            }
            case "Quotation":{
                $existPage = true;
                echo '<div class="wrapper">';
                include Config::PATH . Config::VIEW_HTML . "general/HeaderView.php";
                include Config::PATH . Config::VIEW_HTML . "general/MenuView.php";
                 $isAllow = $AccessManager->validateURL();
                 if($isAllow){
                $view = new QuotationView($pageTpl, $data, $core);
                 }else{
                     include Config::PATH . Config::VIEW_HTML . "general/DenegateAccess.php";
                }
                include Config::PATH . Config::VIEW_HTML . "general/FooterView.php";
                echo '</div>';
                break;
            }
            case "GenerateQuotes":{
                $existPage = true;
                echo '<div class="wrapper">';
                include Config::PATH . Config::VIEW_HTML . "general/HeaderView.php";
                include Config::PATH . Config::VIEW_HTML . "general/MenuView.php";
                 $isAllow = $AccessManager->validateURL();
                 if($isAllow){
                $view = new GenerateQuotesView($pageTpl, $data, $core);
                 }else{
                     include Config::PATH . Config::VIEW_HTML . "general/DenegateAccess.php";
                }
                include Config::PATH . Config::VIEW_HTML . "general/FooterView.php";
                echo '</div>';
                break;
            }
            case "HistoryModifyQuotation":{
                $existPage = true;
                echo '<div class="wrapper">';
                include Config::PATH . Config::VIEW_HTML . "general/HeaderView.php";
                include Config::PATH . Config::VIEW_HTML . "general/MenuView.php";
                 $isAllow = $AccessManager->validateURL();
                 if($isAllow){
                $view = new HistoryModifyQuotationView($pageTpl, $data, $core);
                 }else{
                     include Config::PATH . Config::VIEW_HTML . "general/DenegateAccess.php";
                }
                include Config::PATH . Config::VIEW_HTML . "general/FooterView.php";
                echo '</div>';
                break;
            }

            case "HistoryModifyQuotationMainteince":{
                $existPage = true;
                echo '<div class="wrapper">';
                include Config::PATH . Config::VIEW_HTML . "general/HeaderView.php";
                include Config::PATH . Config::VIEW_HTML . "general/MenuView.php";
                 $isAllow = $AccessManager->validateURL();
                 if($isAllow){
                $view = new HistoryModifyQuotationMainteinceView($pageTpl, $data, $core);
                 }else{
                     include Config::PATH . Config::VIEW_HTML . "general/DenegateAccess.php";
                }
                include Config::PATH . Config::VIEW_HTML . "general/FooterView.php";
                echo '</div>';
                break;
            }
            case "ModifyQuotation":{
               $existPage = true;
                echo '<div class="wrapper">';
                include Config::PATH . Config::VIEW_HTML . "general/HeaderView.php";
                include Config::PATH . Config::VIEW_HTML . "general/MenuView.php";
                require(Config::PATH . Config::MODULES_VIEW . '' . strtolower('quotation') . '/' . 'Quotation' . 'View.php');
                $view = new QuotationView($pageTpl, $data, $core);
                echo '</div>';
                break;
            }

            case "ModifyQuotationMainteince":{
                $existPage = true;
                 echo '<div class="wrapper">';
                 include Config::PATH . Config::VIEW_HTML . "general/HeaderView.php";
                 include Config::PATH . Config::VIEW_HTML . "general/MenuView.php";
                 require(Config::PATH . Config::MODULES_VIEW . '' . strtolower('QuoteMainteince') . '/' . 'QuoteMainteince' . 'View.php');
                 $view = new QuoteMainteinceView($pageTpl, $data, $core);
                 echo '</div>';
                 break;
             }

            case "QuoteMainteince":{
                $existPage = true;
                 echo '<div class="wrapper">';
                 include Config::PATH . Config::VIEW_HTML . "general/HeaderView.php";
                 include Config::PATH . Config::VIEW_HTML . "general/MenuView.php";
                 require(Config::PATH . Config::MODULES_VIEW . '' . strtolower('QuoteMainteince') . '/' . 'QuoteMainteince' . 'View.php');
                 $view = new QuoteMainteinceView($pageTpl, $data, $core);
                 echo '</div>';
                 break;
             }

             case "Crud":{
                $existPage = true;
                 echo '<div class="wrapper">';
                 include Config::PATH . Config::VIEW_HTML . "general/HeaderView.php";
                 include Config::PATH . Config::VIEW_HTML . "general/MenuView.php";
                 $isAllow = $AccessManager->validateURL();
                  if($isAllow){
                 $view = new CrudView($pageTpl, $data, $core);
                 }else{
                      include Config::PATH . Config::VIEW_HTML . "general/DenegateAccess.php";
                 }
                 echo '</div>';
                 include Config::PATH . Config::VIEW_HTML . "general/FooterView.php";
                 break;
             }
            
            case "PricesMaintence":{
                $existPage = true;
                 echo '<div class="wrapper">';
                 include Config::PATH . Config::VIEW_HTML . "general/HeaderView.php";
                 include Config::PATH . Config::VIEW_HTML . "general/MenuView.php";
                 $isAllow = $AccessManager->validateURL();
                  if($isAllow){
                 $view = new PricesMaintenceView($pageTpl, $data, $core);
                 }else{
                      include Config::PATH . Config::VIEW_HTML . "general/DenegateAccess.php";
                 }
                 echo '</div>';
                 include Config::PATH . Config::VIEW_HTML . "general/FooterView.php";
                 break;
             }
               case "Providers":{
               $existPage = true;
                echo '<div class="wrapper">';
                include Config::PATH . Config::VIEW_HTML . "general/HeaderView.php";
                include Config::PATH . Config::VIEW_HTML . "general/MenuView.php";
                $isAllow = $AccessManager->validateURL();
                 if($isAllow){
                $view = new ProviderView($pageTpl, $data, $core);
                }else{
                     include Config::PATH . Config::VIEW_HTML . "general/DenegateAccess.php";
                }
                echo '</div>';
                include Config::PATH . Config::VIEW_HTML . "general/FooterView.php";
                break;
            }
        
               case "ReportHistorialPerson": {
                $existPage = true;
                echo '<div class="wrapper">';
                include Config::PATH . Config::VIEW_HTML . "general/HeaderView.php";
                include Config::PATH . Config::VIEW_HTML . "general/MenuView.php";
                 $isAllow = $AccessManager->validateURL();
                 if($isAllow){
                $view = new ReportHistorialPersonView($pageTpl, $data, $core);
                 }else{
                     include Config::PATH . Config::VIEW_HTML . "general/DenegateAccess.php";
                }
                include Config::PATH . Config::VIEW_HTML . "general/FooterView.php";
                echo '</div>';
                break;
            }
                 case "ReportQuotation": {
                $existPage = true;
                echo '<div class="wrapper">';
                include Config::PATH . Config::VIEW_HTML . "general/HeaderView.php";
                include Config::PATH . Config::VIEW_HTML . "general/MenuView.php";
                 $isAllow = $AccessManager->validateURL();
                 if($isAllow){
                $view = new ReportQuotaionView($pageTpl, $data, $core);
                 }else{
                     include Config::PATH . Config::VIEW_HTML . "general/DenegateAccess.php";
                }
                include Config::PATH . Config::VIEW_HTML . "general/FooterView.php";
                echo '</div>';
                break;
            }
             case "TracingQuotation":{
                $existPage = true;
                echo '<div class="wrapper">';
                include Config::PATH . Config::VIEW_HTML . "general/HeaderView.php";
                include Config::PATH . Config::VIEW_HTML . "general/MenuView.php";
                 $isAllow = $AccessManager->validateURL();
                 if($isAllow){
                $view = new TracingQuotationView($pageTpl, $data, $core);
                 }else{
                     include Config::PATH . Config::VIEW_HTML . "general/DenegateAccess.php";
                }
                include Config::PATH . Config::VIEW_HTML . "general/FooterView.php";
                echo '</div>';
                break;
            }
            case "TracingQuotationMainteince":{
                $existPage = true;
                echo '<div class="wrapper">';
                include Config::PATH . Config::VIEW_HTML . "general/HeaderView.php";
                include Config::PATH . Config::VIEW_HTML . "general/MenuView.php";
                 $isAllow = $AccessManager->validateURL();
                 if($isAllow){
                $view = new TracingQuotationMainteinceView($pageTpl, $data, $core);
                 }else{
                     include Config::PATH . Config::VIEW_HTML . "general/DenegateAccess.php";
                }
                include Config::PATH . Config::VIEW_HTML . "general/FooterView.php";
                echo '</div>';
                break;
            }
               case "ModifyPrices": {
                $existPage = true;
                echo '<div class="wrapper">';
                include Config::PATH . Config::VIEW_HTML . "general/HeaderView.php";
                include Config::PATH . Config::VIEW_HTML . "general/MenuView.php";
                 $isAllow = $AccessManager->validateURL();
                 if($isAllow){
                $view = new ModifyPricesView($pageTpl, $data, $core);
                 }else{
                     include Config::PATH . Config::VIEW_HTML . "general/DenegateAccess.php";
                }
                include Config::PATH . Config::VIEW_HTML . "general/FooterView.php";
                echo '</div>';
                break;
            }

              case "Prospect": {
                $existPage = true;
                echo '<div class="wrapper">';
                include Config::PATH . Config::VIEW_HTML . "general/HeaderView.php";
                include Config::PATH . Config::VIEW_HTML . "general/MenuView.php";
                 $isAllow = $AccessManager->validateURL();
                 if($isAllow){
                $view = new ProspectView($pageTpl, $data, $core);
                 }else{
                     include Config::PATH . Config::VIEW_HTML . "general/DenegateAccess.php";
                }
                include Config::PATH . Config::VIEW_HTML . "general/FooterView.php";
                echo '</div>';
                break;
            }
            case "HistoryProducts":{
                $existPage = true;
                echo '<div class="wrapper">';
                include Config::PATH . Config::VIEW_HTML . "general/HeaderView.php";
                include Config::PATH . Config::VIEW_HTML . "general/MenuView.php";
                $isAllow = $AccessManager->validateURL();
                 if($isAllow){
                    $view = new HistoryProductsView($pageTpl, $data, $core);
                }else{
                     include Config::PATH . Config::VIEW_HTML . "general/DenegateAccess.php";
                }
                include Config::PATH . Config::VIEW_HTML . "general/FooterView.php";
                echo '</div>';
                break;
            }
            case "Code":{
                $existPage = true;
                echo '<div class="wrapper">';
                include Config::PATH . Config::VIEW_HTML . "general/HeaderView.php";
                include Config::PATH . Config::VIEW_HTML . "general/MenuView.php";
                $isAllow = $AccessManager->validateURL();
                 if($isAllow){
                    $view = new CodeView($pageTpl, $data, $core);
                }else{
                     include Config::PATH . Config::VIEW_HTML . "general/DenegateAccess.php";
                }
                include Config::PATH . Config::VIEW_HTML . "general/FooterView.php";
                echo '</div>';
                break;
            }
            case "Mails":{
                $existPage = true;
                echo '<div class="wrapper">';
                include Config::PATH . Config::VIEW_HTML . "general/HeaderView.php";
                include Config::PATH . Config::VIEW_HTML . "general/MenuView.php";
                $isAllow = $AccessManager->validateURL();
                 if($isAllow){
                    $view = new MailsView($pageTpl, $data, $core);
                }else{
                     include Config::PATH . Config::VIEW_HTML . "general/DenegateAccess.php";
                }
                include Config::PATH . Config::VIEW_HTML . "general/FooterView.php";
                echo '</div>';
                break;
            }
            case "Modules":{
                $existPage = true;
                echo '<div class="wrapper">';
                include Config::PATH . Config::VIEW_HTML . "general/HeaderView.php";
                include Config::PATH . Config::VIEW_HTML . "general/MenuView.php";
                $isAllow = $AccessManager->validateURL();
                 if($isAllow){
                    $view = new ModulesView($pageTpl, $data, $core);
                }else{
                     include Config::PATH . Config::VIEW_HTML . "general/DenegateAccess.php";
                }
                include Config::PATH . Config::VIEW_HTML . "general/FooterView.php";
                echo '</div>';
                break;
            }
            case "ModulesActions":{
                $existPage = true;
                echo '<div class="wrapper">';
                include Config::PATH . Config::VIEW_HTML . "general/HeaderView.php";
                include Config::PATH . Config::VIEW_HTML . "general/MenuView.php";
                $isAllow = $AccessManager->validateURL();
                 if($isAllow){
                    $view = new ModulesActionsView($pageTpl, $data, $core);
                }else{
                     include Config::PATH . Config::VIEW_HTML . "general/DenegateAccess.php";
                }
                include Config::PATH . Config::VIEW_HTML . "general/FooterView.php";
                echo '</div>';
                break;
            }
            case "Clients":{
                $existPage = true;
                echo '<div class="wrapper">';
                include Config::PATH . Config::VIEW_HTML . "general/HeaderView.php";
                include Config::PATH . Config::VIEW_HTML . "general/MenuView.php";
                $isAllow = $AccessManager->validateURL();
                 if($isAllow){
                    $view = new ClientsView($pageTpl, $data, $core);
                }else{
                     include Config::PATH . Config::VIEW_HTML . "general/DenegateAccess.php";
                }
                include Config::PATH . Config::VIEW_HTML . "general/FooterView.php";
                echo '</div>';
                break;
            }
            case "CalendarTRM":{
                $existPage = true;
                echo '<div class="wrapper">';
                include Config::PATH . Config::VIEW_HTML . "general/HeaderView.php";
                include Config::PATH . Config::VIEW_HTML . "general/MenuView.php";
                $isAllow = $AccessManager->validateURL();
                 if($isAllow){
                    $view = new CalendarTRMView($pageTpl, $data, $core);
                }else{
                     include Config::PATH . Config::VIEW_HTML . "general/DenegateAccess.php";
                }
                include Config::PATH . Config::VIEW_HTML . "general/FooterView.php";
                echo '</div>';
                break;
            }
              case "Maintenance":{
                $existPage = true;
                echo '<div class="wrapper">';
                include Config::PATH . Config::VIEW_HTML . "general/HeaderView.php";
                include Config::PATH . Config::VIEW_HTML . "general/MenuView.php";
                $isAllow = $AccessManager->validateURL();
                 if($isAllow){
                    $view = new MaintenanceView($pageTpl, $data, $core);
                }else{
                     include Config::PATH . Config::VIEW_HTML . "general/DenegateAccess.php";
                }
                include Config::PATH . Config::VIEW_HTML . "general/FooterView.php";
                echo '</div>';
                break;
            }

            case "Diagnosis":{
                $existPage = true;
                echo '<div class="wrapper">';
                include Config::PATH . Config::VIEW_HTML . "general/HeaderView.php";
                include Config::PATH . Config::VIEW_HTML . "general/MenuView.php";
                $isAllow = $AccessManager->validateURL();
                 if($isAllow){
                    $view = new DiagnosisView($pageTpl, $data, $core);
                }else{
                     include Config::PATH . Config::VIEW_HTML . "general/DenegateAccess.php";
                }
                include Config::PATH . Config::VIEW_HTML . "general/FooterView.php";
                echo '</div>';
                break;
            }

            case "ListDiagnosis":{
                $existPage = true;
                echo '<div class="wrapper">';
                include Config::PATH . Config::VIEW_HTML . "general/HeaderView.php";
                include Config::PATH . Config::VIEW_HTML . "general/MenuView.php";
                $isAllow = $AccessManager->validateURL();
                 if($isAllow){
                    $view = new ListDiagnosisView($pageTpl, $data, $core);
                }else{
                     include Config::PATH . Config::VIEW_HTML . "general/DenegateAccess.php";
                }
                include Config::PATH . Config::VIEW_HTML . "general/FooterView.php";
                echo '</div>';
                break;
            }


              case "Managers":{
                $existPage = true;
                echo '<div class="wrapper">';
                include Config::PATH . Config::VIEW_HTML . "general/HeaderView.php";
                include Config::PATH . Config::VIEW_HTML . "general/MenuView.php";
                $isAllow = $AccessManager->validateURL();
                 if($isAllow){
                    $view = new ManagersView($pageTpl, $data, $core);
                }else{
                     include Config::PATH . Config::VIEW_HTML . "general/DenegateAccess.php";
                }
                include Config::PATH . Config::VIEW_HTML . "general/FooterView.php";
                echo '</div>';
                break;
            }

              case "Tutorials":{
                $existPage = true;
                echo '<div class="wrapper">';
                include Config::PATH . Config::VIEW_HTML . "general/HeaderView.php";
                include Config::PATH . Config::VIEW_HTML . "general/MenuView.php";
                $isAllow = $AccessManager->validateURL();
                 if($isAllow){
                    $view = new TutorialsView($pageTpl, $data, $core);
                }else{
                     include Config::PATH . Config::VIEW_HTML . "general/DenegateAccess.php";
                }
                include Config::PATH . Config::VIEW_HTML . "general/FooterView.php";
                echo '</div>';
                break;
            }

            
            case "Exit": {
                include (Config::PATH . Config::MODULES_VIEW . '' . strtolower($url) . '/' . $url . 'View.php');
            }
        }
    }


    if (!$existPage) {
        echo '<div class="wrapper">';
        include Config::PATH . Config::VIEW_HTML . "general/HeaderView.php";
        include Config::PATH . Config::VIEW_HTML . "general/MenuView.php";
        include Config::PATH . Config::VIEW_HTML_MODULES . "home/HomeView.php";
        include Config::PATH . Config::VIEW_HTML . "general/FooterView.php";
        echo '</div>';
    }

}

