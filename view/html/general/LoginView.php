<div id="back"></div>
<div class="login-box">
  <div class="login-logo">
    <img src="view/img/template/logo-blanco-bloque.png" class="img-responsive" style="padding:30px 100px 0px 100px">
  </div>
  <div class="login-box-body">
    <p class="login-box-msg">Ingresar al sistema</p>
    <form method="post">
      <div class="form-group has-feedback">
        <input id="userLoginCtx" name="userLoginCtx" type="text" class="form-control" placeholder="Usuario" required>
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input id="userLoginPassCtx" name="userLoginPassCtx" type="password" class="form-control" placeholder="Contraseña"  required>
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-xs-block btn-block btn-flat">Ingresar</button>
        </div>
      </div>
      <?php
        $loginCtrl = new LoginCtrl();
      ?>
    </form>
  </div>
</div>
