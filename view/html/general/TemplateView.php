<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Vortex Company</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta http-equiv="Expires" content="0">
  <meta http-equiv="Last-Modified" content="0">
  <meta http-equiv="Cache-Control" content="no-cache, mustrevalidate">
  <meta http-equiv="Pragma" content="no-cache">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <meta name="theme-color" content="#000000">
  <link rel="icon" href="view/img/template/icono-negro.png">
  <meta name="apple-mobile-web-app-status-bar-style" content="black">
   <!--=====================================
  PLUGINS DE CSS
  ======================================-->
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" rel="preload" href="view/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" rel="prefetch" href="view/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" rel="preload" href="view/dist/css/AdminLTE.css">
  <!-- Google Font -->
  <link rel="stylesheet" rel="prefetch"  href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  <!-- AdminLTE Skins -->
  <link rel="stylesheet" rel="preload" href="view/dist/css/skins/_all-skins.min.css">
  <!-- pace style-->
   <link rel="stylesheet" rel="prefetch" href="view/dist/css/stylePace.css">
   <!-- DataTables -->
  <link rel="stylesheet" rel="preload" href="view/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <link rel="stylesheet" rel="preload" href="view/bower_components/datatables.net-bs/css/responsive.bootstrap.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" rel="prefetch" href="view/plugins/iCheck/all.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" rel="prefetch" href="view/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- DatePicker -->
  <link rel="stylesheet" rel="prefetch" href="view/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" rel="prefetch" href="view/bower_components/select2/dist/css/select2.css" rel="stylesheet" />
  <!-- Toast -->
  <link rel="stylesheet" rel="prefetch" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
  <!-- easy jquery -->
  <link rel="stylesheet" rel="prefetch" type="text/css" href="view/bower_components/Easy-jQuery/li-scroller.css">
  <!--=====================================
  PLUGINS DE JAVASCRIPT
  ======================================-->
  <!-- jQuery 3 -->
  <script rel="preload"  src="view/bower_components/jquery/dist/jquery.min.js"></script>
  <script rel="preload" src="view/bower_components/jquery-resizable-master/dist/jquery-resizable.min.js"></script>
  <!-- Bootstrap 3.3.7 -->
  <script src="view/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
  <!-- FastClick -->
  <script src="view/bower_components/fastclick/lib/fastclick.js"></script>
   <!-- jvector -->
  <script src="view/bower_components/jvectormap/jquery-jvectormap.js"></script>
  <!-- AdminLTE App -->
  <script src="view/dist/js/adminlte.min.js"></script>
  <!-- DataTables -->
  <script src="view/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
  <script src="view/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
  <script src="view/bower_components/datatables.net-bs/js/dataTables.responsive.min.js"></script>
  <script src="view/bower_components/datatables.net-bs/js/responsive.bootstrap.min.js"></script>
  <!-- SweetAlert 2 -->
  <script src="view/plugins/sweetalert2/sweetalert2.all.js"></script>
  <!-- InputMask -->
  <script src="view/plugins/input-mask/jquery.inputmask.js"></script>
  <script src="view/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
  <script src="view/plugins/input-mask/jquery.inputmask.extensions.js"></script>
  <!-- jQuery Number -->
  <script src="view/plugins/jqueryNumber/jquerynumber.min.js"></script>
  <!-- daterangepicker -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
  <script src="view/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
  <!-- datepicker -->
  <script src="view/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
  <!-- UtilesJquery -->
  <script src="view/js/UtilesJquery.js"></script>
  <!-- CK Editor -->
  <script src="view/bower_components/ckeditor/ckeditor.js?v=13243434543543"></script>
  <!-- Idle Timer For Validate Activity On Page For Sessions -->
  <script src="view/bower_components/jquery-idletimer-master/dist/idle-timer.js"></script>
  <!-- Plugin AutoComplete Inputs -->
  <script src="view/bower_components/EasyAutocomplete-1.3.5/jquery.easy-autocomplete.js"></script>
  <!-- Select2 -->
  <script src="view/bower_components/select2/dist/js/select2.js"></script>
  <!-- Pace -->
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/pace.min.js"></script>
  <!-- toast-->
  <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
  <!-- easy jquery -->
  <script src="view/bower_components/Easy-jQuery/jquery.li-scroller.1.0.js "></script>

  <!--=====================================
  ADICIONALES JAVASCRIPT
  ======================================-->

</head>
<!--=====================================
CUERPO DOCUMENTO
======================================-->
<body class="hold-transition skin-blue sidebar-collapse sidebar-mini login-page " style="background: #222d32">
<!--    <div id="area" style="background-color: #8abeb7">-->
<!--    nada-->
<!--    </div>-->
<?php
    /*=============================================
    LOAD CONTENT
    =============================================*/
    include "LoaderView.php";

    //include "FooterView.php";


?>
<script src="view/html/modules/home/home.js?v=234"></script>
<script type="text/javascript"> 
    $(".sidebar-menu li a").each( function(e , et)  {
        if( $(this).attr("href") == "<?php echo $_GET['url'] ?>"){
          $(this).parent("li").attr("class","active");
          $(this).parent("li").parent("ul").parent("li").attr("class"," treeview menu-open active");
          $(this).parent("li").parent("ul").parent("li").parent("ul").parent("li").attr("class","treeview menu-open active");
        }
    });

    if ($("html").width() < 767){
      $("body").attr("class" , "skin-blue sidebar-mini sidebar-open");
    }

    $(".content").attr("style","min-height: 720px;"); 
    $(".content-wrapper").attr("style","min-height: 1400px !important;")
     {
}
</script>
</body>
</html>