<?php
require_once(Config::PATH . Config::BACKEND . 'modules/PersonVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/UserVo.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralDao.php');
$userVo = unserialize($_SESSION['user']["user"]);
$personVo = unserialize($_SESSION['user']["person"]);

 function GetTypeRole($idRole)
    {
        $roleVo = new RoleVo();
        $roleVo->id = $idRole;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $roleVo);
        if ($roleVo2 = $generalDao->GetVo($generalVo = clone $roleVo)) {
            return "" . $roleVo2->name;
        }
        return "Problema en la consulta";
    }
?>



<header class="main-header">
	<!--=====================================
	LOGOTIPO
	======================================-->
	<a href="Home" class="logo">
		<!-- logo mini -->
		<span class="logo-mini">
			<img src="view/img/template/icono-blanco.png" class="img-responsive" style="padding:10px">
		</span>
		<!-- logo normal -->
		<span class="logo-lg">
			<b>Vortex Company</b>
			<!--<img src="view/img/template/logo-negro-lineal.png" class="img-responsive" style="padding:10px 0px">-->
		</span>
	</a>
	<!--=====================================
	BARRA DE NAVEGACIÓN
	======================================-->
	<nav class="navbar navbar-static-top" role="navigation">
		<!-- Botón de navegación -->
	 	<a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        	<span class="sr-only">Toggle navigation</span>
      	</a>
		<!-- perfil de usuario -->
		<div class="navbar-custom-menu">
			<ul class="nav navbar-nav">
				  <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?php echo $userVo->photo ?>" class="user-image" alt="User Image">
              <span class="hidden-xs"><?php echo $personVo->names; ?><?php /* echo $_SESSION["nombre"];*/ ?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header bg-silver">
                <img src="<?php echo $userVo->photo ?>" class="img-circle" alt="User Image">

                <p>
                  <?php echo $personVo->names." ".$personVo->lastNames; ?>
                  <small class="text-uppercase"><?php echo GetTypeRole($personVo->idRole) ; ?></small>
                </p>
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                 <a href="Tutorials" class="btn btn-primary btn-flat" style="color:white !important;  background: #149BF1 !important"">Tutoriales</a>
                </div>
                <div class="pull-right">
                  <a href="Exit" class="btn btn-danger btn-flat" style="color:white !important ;    background: #dd4b39 !important"><i class="fa fa-arrow-right"></i> Salir</a>
                </div>
              </li>
            </ul>
          </li>
			</ul>
		</div>
	</nav>
 </header>
