<div class="content-wrapper">
  <section class="content-header">
    <h1>
     Permisos de Productos Cotización #{$idContent}
    </h1>
    <ol class="breadcrumb">
      <li><a href="Home"><i class="fa fa-dashboard"></i> Inicio</a></li>
      <li class="active"> Permisos de Productos Cotización</li>
    </ol>
  </section>
  <section class="content">
    <div class="box">
        <nav class="navbar navbar-light">
            <div class="container-fluid">
                <ul class="nav navbar-nav">
                      <li>
                          <a style="cursor: pointer;" id="userAddBtn" name="userAddBtn" onclick="location.href = 'Home' ">
                              <i class="fa fa-home"></i>
                              Menu Principal
                          </a>
                      </li>
                </ul>
            </div>
        </nav>
        <input type="hidden" id="idUserCreate" value="{$idUserCreate}">
      <input type="hidden" id="idContent" value="{$idContent}">
      <div class="box-body">
          <div class="alert alert-warning alert-dismissible m-3" role="alert">
            <strong>Información cotización</strong><br>
           la cotización con codigo #{$quotation[0]->id} generada {$quotation[0]->generationDate} con el valor general de ${number_format($quotation[0]->total)} {$quotation[0]->typeMoney} con su versión numero # {$quotation[0]->version} para la empresa '{$company}'.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span style="color:black" aria-hidden="true">&times;</span>
            </button>
          </div>
        <table id="impostTbl" name="impostTbl" class="table table-bordered table-striped dt-responsive tables dataTable no-footer dtr-inline" style="width: 100%;" cellspacing="0">
            <thead>
                <tr>
                  <th>Codigo </th>
                  <th>tipo </th>
                  <th>Nombre</th>
                  <th>Marca</th>
                  <th>Refererencia </th>
                  <th>Presentación </th>
                  <th>Producto sin porciento Producto</th>
                  <th>Cantidad</th>  
                  <th>Producto con porciento Producto</th>
                  <th>Empresa</th>
                  <th>Tiempo Entrega</th>  
                  <th>Porcentaje</th>
                  <th>Habilitar</th>  
                </tr>
            </thead>
        </table>
  
        <div class="col-sm-12">
          <button class="btn btn-primary btn-block" onclick="AbleQuote({$idContent})">Habilitar Cotización</button>
        </div>
      </div>
    </div>
  </section>
</div>


<div class="modal" id="price" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header bg-primary">
        <h5 class="modal-title">Asignar Ganancia</h5>
      </div>
      <div class="modal-body">
          <input type="hidden" class="form-control" id="idData" >
          <input type="hidden" class="form-control" id="percentClassic" >
          <input type="hidden" class="form-control" id="priceDataSend" >
        <div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <label>Precio con porcentaje Actual</label>
          <div class="input-group" id="setDataPercent">
             
              <span id="iconPercent" class="input-group-addon"><i class="fa fa-money"></i></span>
              <input type="text" class="form-control" disabled id="priceData" >
          </div>
      </div>
      <div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
          <label>porcentaje Actual</label>
          <div  class="input-group">
              <span  class="input-group-addon"><i class="fa fa-codepen"></i></span>
              <input type="text" class="form-control" id="percentData" >
          </div>
      </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" onClick="UpdatePercent();">Actualizar</button>
      </div>
    </div>
  </div>
</div>




<div id="containerResponseAjax"></div>

<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<script src="view/html/modules/configdata/ConfigData.js?v= {$tempParameter}"></script>

{$jquery}









