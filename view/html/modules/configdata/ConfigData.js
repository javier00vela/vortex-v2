// jshint unused:true
var table ;
var data_id;

function LoadTable() {
    var parameters = {
        "isActionFromAjax" : true,
        "optionInputForm" : 0,
        "idDataInputForm" : 0,
        "id":$("#idContent").val()
    };


    table = $('#impostTbl').DataTable( {
        responsive: true,
           "drawCallback": function( settings ) {
            $('[id=toggle-demo]').bootstrapToggle();
            $('[data-toggle="tooltip"]').tooltip();
        },
        language : {
                "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
            },
        "ajax": {
            url : "view/html/modules/configdata/AjaxDataConfigData.php",
            method : "POST",
            data : parameters
        }
    });
}


function ChangeCheckProduct(){
    $('#impostTbl ').on('click', '.toggle', function () {
         percent = $(this).children("input").attr("data").split(",")[0];
         id = $(this).children("input").attr("data").split(",")[1];
         price = $(this).children("input").attr("data").split(",")[2];
         data_id = $(this).children("input").attr("data-id");
        if ($(this).hasClass('btn-success')) {
            $("#idData").attr("value",id);
            $("#percentClassic").attr("value",percent);
            $("#priceDataSend").attr("value", Math.round(price));
            $("#priceData").attr("value", "$"+ GetPercent(Math.round(price) , percent ) );
            $("#percentData").attr("value",percent);
            $('#price').modal({backdrop: 'static', keyboard: false})
           $("#price").modal("show");
           
        }
   })        
}

function GetPercent(price , percent){
    console.log(price +"    " +percent);
    return Math.round(((price * (percent / 100)) + price)); 
}

function ChangePrice(val){
    if(val== "NaN" ||  !parseFloat(val) ){
        val = 20;
    }
    $("#percentData").removeAttr("value");
    $("#percentData").attr("value",val);
    $("#priceData").attr("value", "$"+GetPercent(Math.round($("#priceDataSend").attr("value") ), parseFloat(val) ));
}

function AbleQuote(id){
    var parameters = {
        "isActionFromAjax" : true,
        "optionInputForm" : 9,
        "idDataInputForm" : 0,
        "id":id,
        "mail": $("#idUserCreate").val()
    };

    $.ajax({
        url: "view/html/modules/configdata/AjaxDataConfigData.php",
        type: 'post',
        data: parameters,
        success: function (data) {
            PrintData(id);
            setTimeout( function() {
                window.location = "GenerateQuotes"; 
            } , 7000);
         }});
}

function ChangeFormatPrices(){
    $("#percentData").keypress(function(event) {
            console.log(event.charCode);
            if (event.charCode > 58 || event.charCode < 43  || event.charCode == 45 ||  event.charCode == 46 ){
                return false;
            }                
    });
}

function UpdatePercent(){
    var id = $("#idData").attr("value");
    var price = $("#priceDataSend").attr("value");
    var percent = $("#percentData").attr("value");
    UpdateChange( percent , id , percent , price);
}

function ValidatePriceUnit(){
$("#percentData").change(function(event) {
var cont = 0;
var numberType = $(this).val();
ChangePrice(numberType);
for (var i = 0; i < numberType.toString().length; i++) {
   if (numberType.toString()[i] == "," || numberType.toString()[i] == "." ){
        cont++;
    }
}
if( cont > 1){
   numberType = 20;
}
 $(this).val(numberType);
});
}

function PrintData(id){
    var parameters = {
        "isActionFromAjax": true,
        "optionInputForm": 3,
        "idDataInputForm": 0,
        "id":id
    }

        $.ajax ({
            url: "GenerateQuotes",
            type: 'post',
            data: parameters,

             beforeSend:function(){
                
               setTimeout(MessageSwalWaiter('cargando...','Abriendo cotización!'), 10000);
            },
            success: function (data) {
                data = data.split("--split--")[1];
                var windowOpen =  window.open("view/docs/pdf/"+data+".pdf?v="+ Math.random(), '_blank');
              if(windowOpen){
               windowOpen.focus();
                DeletePDFQuotation(data);
               MessageSwalRedirect("GENERADO !!","Se ha abierto el documento de cotización ","success","Quotation");
              
              }else{
                MessageSwalBasic("VERIFICAR !!","Debes habilitar permisos para abrir ventanas emergentes","warning");
              }
            }

        });
}

function UpdateChange( percent , id , prepercent , price){
    var parameters = {
        "isActionFromAjax" : true,
        "optionInputForm" : 7,
        "idDataInputForm" : 0,
        "percent":percent,
        "id":id,
        "prepercent":prepercent,
        "price":price
    };

    $.ajax({
        url: "view/html/modules/configdata/AjaxDataConfigData.php",
        type: 'post',
        data: parameters,
        success: function (data) {
            $("#price").modal("hide");
            table.ajax.reload();

         }});
}
/*********************************CLICK EVENTS**************************/

$(function() {

    ChangeCheckProduct();
    LoadTable();
    ChangeFormatPrices();
    ValidatePriceUnit();
});
