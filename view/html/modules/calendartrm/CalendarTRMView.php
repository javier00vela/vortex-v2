<?php
require_once(Config::PATH . Config::CONTROLLER . 'general/AccessManager.php');
require_once(Config::PATH . Config::CONTROLLER . 'general/UtilJquery.php');
require_once(Config::PATH . Config::VIEW_HTML . 'general/comps/GeneralCtx.php');
require_once(Config::PATH . Config::VIEW_HTML . 'general/comps/GeneralLst.php');
require_once(Config::PATH . Config::VIEW_HTML . 'general/comps/GeneralWithDataLst.php');
require_once(Config::PATH . Config::BACKEND . 'modules/PricesCompanyVo.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralDao.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/ImpostVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/CompanyVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/InventoryVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/HistoryTRMVo.php');

class CalendarTRMView
{
    private $optionInputForm;
    private $isUpdateUserForm;
    private $idDataInputForm;
    /*=============================================
                  Session Vars
    =============================================*/
  
    /*=============================================
                  Template Vars
    =============================================*/
    private $utilJQ;
    private $pageTpl, $data, $core;
    /*=============================================
                Controls Vars HTML
    =============================================*/
    private $pricectx;
    private $typepriceLs;
    private $date;

    /*=============================================
                Data Vars
    =============================================*/
    private $generalDao;


    function __construct($pageTpl, $data, $core)
    {
        $this->data = $data;
        $this->core = $core;
        $this->pageTpl = $pageTpl;
            $this->CreateComponents();
            $this->UseDataFromThePostback();
    }

    private function UseDataFromThePostback()
    {
        if (isset($_POST['optionInputForm'])) {
            $this->optionInputForm = $_POST['optionInputForm'];
            $this->idDataInputForm = $_POST['idDataInputForm'];
            $this->generalDao = new GeneralDao();
            switch ($this->optionInputForm) {
                case "1": {
                    $this->SetData();
                    break;
                }
                case "2": {
                    $this->DeleteData();
                    break;
                }
                case "4": {
                    $this->SetUpdate();
                    break;
                }
            }
        }

        $this->AssignDataToTpl();

    }

    private function CreateComponents()
    {
        $this->pricectx  = new  GeneralCtx("pricectx","Precio para los proveedores",null,true);
        $dataFLETESelection =  array("USD" , "EUR");  
         $this->typepriceLs = new GeneralWithDataLst($dataFLETESelection, "typepriceLs", false,false,"--Tipo de Moneda--",true); 
         $this->date = new GeneralCtx("TRMDate","Fecha limite de la TRM",null,true);
       
        $this->utilJQ = new UtilJquery("Impost");
    }
    private function SetDataToVo()
    {
        $HistoryTRMVo = new HistoryTRMVo();
        $HistoryTRMVo->id = $this->idDataInputForm;
        $HistoryTRMVo->typeMoney = $_POST["typepriceLs"];
        $HistoryTRMVo->money = $_POST["pricectx"];
        $HistoryTRMVo->generation = date("Y-m-d");
        $HistoryTRMVo->date =  $_POST["TRMDate"];
        $HistoryTRMVo->mailSend =  0;
       
        return $HistoryTRMVo;
    }
    private function SetData()
    {
        $impostVo = $this->SetDataToVo();
        $impostVo->id = null;
        $result = $this->generalDao->Set($generalVo = clone $impostVo);
        if (isset($result)) {
            $message = 'MessageSwalBasic("Registrado!","TRM Registrado Correctamente","success");';
            $this->utilJQ->AddFunctionJavaScript($message);
        }

    }

    private function SetUpdate()
    {
        $result = $this->generalDao->Update($generalVo = clone $this->SetDataToVo());
        if (isset($result)) {
            $message = 'MessageSwalBasic("Modificado!","TRM Modificado Correctamente","success");';
            $this->utilJQ->AddFunctionJavaScript($message);
        }
    }
    private function DeleteData()
    {
        $message = "";
        $impostVo = new ImpostVo();
        $impostVo->id = $this->idDataInputForm;
        if ($this->generalDao->Delete($generalVo = clone $impostVo)) {
            $message = 'MessageSwalBasic("Eliminado!","Impuesto Eliminado Correctamente","success");';
        } else {
            $message = 'MessageSwalBasic("Error!","Problema con la base de datos","error");';
        }
        $this->utilJQ->AddFunctionJavaScript($message);
    }

    private function AssignDataToTpl()
    {
        $this->data->assign('pricectx', $this->pricectx->paint());
        $this->data->assign('typepriceLs', $this->typepriceLs->paint());
        $this->data->assign('date', $this->date->paint());

        $this->data->assign('tempParameter', time());
        $this->data->assign('jquery', $this->utilJQ->PaintJQ());
        $this->SetDataToTpl();
    }   
    private function SetDataToTpl()
    {
        echo $this->core->get($this->pageTpl, $this->data);
    }
  


}   