// jshint unused:true
"use strict";


function LoadTable() {
    RemoveActionModule(33);
    GetArrayFromDataModule(33);
    setTimeout(function() {
        var parameters = {
            "isActionFromAjax": true,
            "optionInputForm": 0,
            "idDataInputForm": 0,
            "listData": arrayResponse,
        };


        $('#QuotesTbl').DataTable({
            responsive: true,
            "drawCallback": function(settings) {
                $('[data-toggle="tooltip"]').tooltip()
            },
            language: {
                "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
            },
            "ajax": {
                url: "view/html/modules/buylistquotes/AjaxDataBuyListQuotes.php",
                method: "POST",
                data: parameters
            }
        });
    }, 500);
}

/*********************************CLICK EVENTS**************************/


function PrintData(id) {
    var parameters = {
        "isActionFromAjax": true,
        "optionInputForm": 3,
        "idDataInputForm": 0,
        "id": id
    }

    $.ajax ({
        url: "BuyListQuotes",
        type: 'post',
        data: parameters,

        beforeSend: function() {

            setTimeout(MessageSwalWaiter('cargando...', 'Abriendo Orden de Compra!'), 10000);
        },
        success: function(data) {
            var windowOpen = window.open("view/docs/pdf/Orden_" + id + ".pdf?v=" + Math.random(), '_blank');
            if (windowOpen) {
                windowOpen.focus();
                MessageSwalRedirect("GENERADO !!", "Se ha abierto el documento de Orden De Compra ", "success", "GenerateQuotes");

            } else {
                MessageSwalBasic("VERIFICAR !!", "Debes habilitar permisos para abrir ventanas emergentes", "warning");
            }
        }

    });
}

$(function() {
    LoadTable();

});