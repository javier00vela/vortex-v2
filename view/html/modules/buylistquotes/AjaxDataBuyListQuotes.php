<?php
require_once(__DIR__ . '/../../../../controller/general/Config.php');
require_once(Config::PATH . Config::CONTROLLER.Config::GENERAL . 'ManageArrays.php');
require_once(Config::PATH . Config::BACKEND . 'modules/QuotationVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/ProductVo.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralDao.php');


class AjaxDataImpost{

    private $isActionFromAjax;
    private $optionInputForm;
    private $idDataInputForm;
    public $dataList = [];

    function __construct()
    {
        if (isset($_POST['isActionFromAjax'])) {
            $this->isActionFromAjax = $_POST['isActionFromAjax'];
            $this->optionInputForm = $_POST['optionInputForm'];
            $this->idDataInputForm = $_POST['idDataInputForm'];
            if(isset($_POST["listData"])){
                $this->dataList = $_POST["listData"];  
            }
            if ($this->isActionFromAjax == "true") {
                if ($this->optionInputForm == 3) {
                    $this->SetDataInControls();
                }else if($this->optionInputForm == 5){
                $this->GetLengthImpost();
                }else {
                    $this->GetDataInJsonForTbl();
                }
            }
        }
    }

    public function GetDataInJsonForTbl()
    {
        $existData = false;
        $QuotationVo = new QuotationVo();
        $QuotationVo->state = "Compra";
        $QuotationVo->idSearchField = 4;
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $QuotationVo);

        $string = "
        {\"data\": [";
        while ($QuotationVo2 = $generalDao->GetVo($generalVo = clone $QuotationVo)) {
            $existData = true;
            $json = array();
           // $json[] = $impostVo2->id;
            $json[] = "#".htmlspecialchars(ucfirst(strtolower($QuotationVo2->code)));
            $json[] = $QuotationVo2->generationDate;
            $json[] = $QuotationVo2->typeQuotation;
            $json[] = $QuotationVo2->version;
            $json[] = $this->GetSizeProductsByQuote($QuotationVo2->code);
        
            $button = "";
            if(!in_array( "ver_lista_proveedor" , $this->dataList)){
                $button .= "<tip title='Lista Proveedores de la Orden' class='blue-tooltip' data-toggle='tooltip' data-placement='top'><a href=\"BuyListQuotesCompany?idQuotes={$QuotationVo2->code}\" id='Update' name='ListBtn' class='btn btn-primary btn-xs'><i class='fa fa-list'></i></a> </tip>";
                $button .= "<tip title='Lista de ordenes para el cliente' class='yellow-tooltip' data-toggle='tooltip' data-placement='top'><a href='BuyListQuotesClient?idQuotes={$QuotationVo2->code}' class='btn btn-warning btn-xs'><i class='fa fa-list'></i></a> </tip>";
             }
                $json[] = $button;

            $string .= json_encode($json) . ",";

        }
        $string .= "-]}-";

        if($existData){
            echo  str_replace(",-]}-", "]}", $string);
        }else{
            echo "{ \"data\": [] }";
        }
    }


    private function GetSizeProductsByQuote($id){
        $arrayList = array();
        $ProductVo = new ProductVo();
        $ProductVo->idQuotation = $id;
        $ProductVo->idSearchField = 16;
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $ProductVo);
        while($ProductVo2 = $generalDao->GetVo($generalVo = clone $ProductVo)) {            
            $arrayList[] =  $ProductVo2->id;
        }  
        return count($arrayList); 
    }

    
}

$ajaxDataImpost = new AjaxDataImpost();
