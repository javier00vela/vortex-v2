<?php
require_once(Config::PATH . Config::CONTROLLER . 'general/AccessManager.php');
require_once(Config::PATH . Config::CONTROLLER . 'general/UtilJquery.php');
require_once(Config::PATH . Config::VIEW_HTML . 'general/comps/GeneralCtx.php');
require_once(Config::PATH . Config::VIEW_HTML . 'general/comps/GeneralLst.php');
require_once(Config::PATH . Config::VIEW_HTML . 'general/comps/GeneralWithDataLst.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralDao.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/ProductVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/PersonVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/QuotationVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/CompanyVo.php');
require_once(Config::PATH . Config::CONTROLLER_LIB .'vendor/dompdf/dompdf/src/Autoloader.php');
require_once(Config::PATH . Config::CONTROLLER . 'general/MailsUtils.php');
require_once(Config::PATH . Config::CONTROLLER_LIB . 'vendor/luecano/numero-a-letras/src/NumeroALetras.php');
require_once(Config::PATH . Config::BACKEND . 'modules/HistoryTRMVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/OrderBuyProviderVo.php');


use Dompdf\Dompdf;
use NumeroALetras\NumeroALetras;

class BuyListQuotesCompanyView
{
    private $optionInputForm;
    private $isUpdateUserForm;
    private $idDataInputForm;
    /*=============================================
                  Session Vars
    =============================================*/
  
    /*=============================================
                  Template Vars
    =============================================*/
    private $utilJQ;
    private $pageTpl, $data, $core;
    /*=============================================
                Controls Vars HTML
    =============================================*/
    private $impostNameCtx;
    private $impostPercentCtx;

    /*=============================================
                Data Vars
    =============================================*/
    private $generalDao;


    function __construct($pageTpl, $data, $core)
    {
        $this->data = $data;
        $this->core = $core;
        $this->pageTpl = $pageTpl;
            $this->CreateComponents();
            $this->UseDataFromThePostback();
           // $this->Testing();
    }

    private function UseDataFromThePostback()
    {
        if (isset($_POST['optionInputForm'])) {
            $this->optionInputForm = $_POST['optionInputForm'];
            $this->idDataInputForm = $_POST['idDataInputForm'];
            $this->generalDao = new GeneralDao();
            switch ($this->optionInputForm) {
                case "1": {
                    $this->SetData();
                    break;
                }
                case "2": {
                    $this->DeleteData();
                    break;
                }
                case "3": {
                    $this->PrintDataOrdenCompra($_POST["id"]);
                    break;
                }
                case "4": {
                    $this->SetUpdate();
                    break;
                }
                case "9": {
                    $this->SetDescription();
                    break;
                }
            }
        }

        $this->AssignDataToTpl();

    }

    public function PrintDataOrdenCompra($idQuote){
        $dompdf = new DOMPDF();  
        $content = $this->SetDataContextHTML($idQuote);
        $dompdf->loadHtml($content);
        $dompdf->setPaper('A3', 'letter');
        $dompdf->set_option('isRemoteEnabled', TRUE);
        $dompdf->render();
        $pdf = $dompdf->output();  
        print_r("--split--Orden_".$idQuote."--split--");
       file_put_contents("view/docs/pdf/Orden_".$idQuote.".pdf", $pdf);
    }

    public function SetDataContextHTML($idQuote){
        $html = '';
        $html = $this->GetTemplate("Orden");
        $html = $this->ReplaceActionsCompany(1 , $html);
        $html = $this->ReplaceActionsProvider( $_POST["nameProvider"] , $idQuote , $html);
        $html = $this->ReplacetTableDataDescription($idQuote , "Orden de Compra" ,$html);
        $html = $this->ReplaceStoreProducts($idQuote , $html);
        return $html;
    }

    public function ReplacetTableDataDescription($idQuote , $typeDescription , $text){
       $arrayToReplace = [$idQuote ,$this->GetDescriptionById($_POST["idProvider"])  ,  date("d-m-y")  ,  date("d-m-y")];
        $arrayReplace = ["#ODC#" , "#observaciones#" , "#Fdocumento#","#Fvencimiento#"];
        for ($i=0; $i <  count($arrayReplace); $i++) { 
           $text = str_replace($arrayReplace[$i] , $arrayToReplace[$i]  , $text);
        }
        return $text;
    }

    public function GetDescriptionById($id){
        $OrderBuyProviderVo = new OrderBuyProviderVo();
        $OrderBuyProviderVo->id = $id;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $OrderBuyProviderVo);
        if ($OrderBuyProviderVo2 = $generalDao->GetVo($generalVo = clone $OrderBuyProviderVo)) {
            return $OrderBuyProviderVo2->description;
        }
    }

    public function GetNameCompanyById($name , $person){
        $CompanyVo = new CompanyVo();
        $CompanyVo->name = $name;
        $CompanyVo->idSearchField = 1;
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $CompanyVo);
        if ($CompanyVo2 = $generalDao->GetVo($generalVo = clone $CompanyVo)) {
            return [$CompanyVo2->name , $CompanyVo2->nit , $CompanyVo2->address , $CompanyVo2->city , $CompanyVo2->phone ,  $person->names ." ".$person->lastNames  , $_POST["pay"]  ];
        }
        return ["Sin Determinar","Sin Determinar","Sin Determinar","Sin Determinar","Sin Determinar","Sin Determinar","Sin Determinar"];
    }

    private function ReplaceActionsProvider($nameProvider ,  $idQuote , $text  ){
        $arrayToReplace = [];
        $arrayToReplace = $this->GetNameCompanyById($nameProvider , $this->GetPerson($this->GetQuote($idQuote)->idUser));
        $arrayReplace = ["#nombreProveedor#" , "#nitProveedor#" , "#direccionProveedor#","#ciudadProveedor#","#telefonoProveedor#","#elaborado#","#metodo#"];
        for ($i=0; $i <  count($arrayReplace); $i++) { 
           $text = str_replace($arrayReplace[$i] , $arrayToReplace[$i]  , $text);
            
        }
        return $text;
    }

    private function GetTemplate($tpl = "Orden"){
        switch($tpl){
            case "Orden":
                return file_get_contents("view/html/modules/buylistquotescompany/templates/orden.html");
                break;
        }
    }

    private function ValidatePercent($percent){
        return str_replace(",",".",$percent);
    }

    private function ReplaceStoreProducts($id , $text){
        $products = [];
        $subtotal = 0;
        $IVASUMA = 0;
        $productData = '';
        $products = $this->GetProductsByQuote($id);
            for ($i=0; $i < count($products) ; $i++) { 
               $productData .=  $this->GetFormatProduct($products[$i] , $i+1 , $this->GetQuote($id));
               $subtotal +=  $this->GetSubTotalItem($products[$i] , $this->GetQuote($id));
                    if($products[$i]->IVA == "0" ||  $this->GetQuote($id)->isDDP == "0"){
                        $IVASUMA += 0;
                    }else{
                        $IVASUMA += $this->GetIvaProduct($products , $this->GetQuote($id) , $i);
                    }
            }
        $IvaTotal =  $this->GetIVATotal($IVASUMA , $this->GetQuote($id));
        $percent = $this->ValidatePercent($_POST["percent"]);
        $total = $this->GetTotal( $this->GetQuote($id) , $subtotal , $IVASUMA , $percent);
        $text = str_replace("#Subtotal#" , $this->GetSubtotal($subtotal , $this->GetQuote($id) ) , $text);
        $text = str_replace("#Descuento#" , (floatval($_POST["percent"]) ? $_POST["percent"] : 0 ) , $text);
        $text = str_replace("#IvaTotal#" , $IvaTotal , $text);
        $text = str_replace("#Tdocumento#" , $total , $text);
        $text = str_replace("#letrasValor#" , str_replace("CON 00/100 PESOS","PESOS " , NumeroALetras::convertir( str_replace(",",".",str_replace(".","",$total)), $this->GetMoney($this->GetQuote($id)->typeMoney) ) ), $text);
        
        return str_replace("#contentDataTable#" , $productData , $text);
    }  

    public function GetMoney($money){
        switch($money){
            case 'COP':
                return 'pesos';
                break;
            case 'USD':
                return 'dolares';
                break;
            case 'EUR':
                return 'euros';
                break;
        }
    }

    
    public function GetTotal( $quotation , $subTotal , $ivaTotal , $percent){
        $total = 0;
        $total = $subTotal + $ivaTotal;
        $total =  $total - ($total * ($percent / 100)) ;
        if($quotation->typeMoney == "USD" || $quotation->typeMoney == "EUR" ){
            $total = $this->SetPriceAsInternational(str_replace(".","," , $total ));
            return $total;
        }else{
            return str_replace(",","." ,number_format($total)) ;
        }
    }

    public function GetIvaProduct($products , $quotation , $i){
        return (( ($products[$i]->priceUnit * $this->GetPercent($products[$i]->percent,100)) + $products[$i]->priceUnit) * $products[$i]->quantity) * $this->GetPercent($quotation->IVA,100);
    }

    public function GetIVATotal($iva , $quotation){
        if($quotation->typeMoney == "USD" || $quotation->typeMoney == "EUR" ){
            return $this->SetPriceAsInternational(str_replace(".","," ,($iva))) ;
        }else{
            return str_replace(",","." ,number_format($iva)) ;
        }
    }

    public function GetPriceToCurrency($type , $money){
        return ( $money / $this->GetTasa($type)  );
    }

    public function GetTasa($field){
        $arrayList = array();
        $HistoryTRMVo = new HistoryTRMVo();
        $HistoryTRMVo->isList = true;
        $generalDao = new GeneralDao();
        $generalDao->CustomQuery("SELECT * FROM {$HistoryTRMVo->nameTable} WHERE  typeMoney='{$field}'");
        while($HistoryTRMVo2 = $generalDao->GetVo($generalVo = clone $HistoryTRMVo)) {
          return $HistoryTRMVo2->money;
        }   
        return 0;
}

    public function GetPriceData($products , $quotation ){
        if($quotation->typeMoney == "EUR"){
            $products->priceUnit =  $this->GetPriceToCurrency("EUR" , $products->priceUnit );
        }

        if($quotation->typeMoney == "USD"){
            $products->priceUnit =  $this->GetPriceToCurrency("USD" , $products->priceUnit );
        }
        return $products->priceUnit;
    }
    
    
    private function GetFormatProduct($product , $cod , $quotationVo ){
        $data = '';
        #$descriptionProduct = (empty($product->description)) ? $this->DescriptionEmpty($product) : $product->description ;
        $product->priceUnit = $this->GetPriceData($product , $quotationVo);
        $descriptionProduct = $this->DescriptionEmpty($product);
        $priceUnit = $this->GetPriceUnit($product , $quotationVo);
        $pricetTotal = $this->GetTotalItem($product , $quotationVo);
        if($product->IVA == "0" ||  $quotationVo->isDDP == "0"){
            $iva = 0;
        }else{
            $iva = $quotationVo->IVA;
        }
        $data = " <tr style='height:800px;min-height:800px;'>
                    <th style='width:5%;border-bottom: 1px solid white;border-top: 1px solid white;'><b>{$cod}</b></th>
                    <th style='width:40%;border-bottom: 1px solid white;border-top: 1px solid white;'>{$descriptionProduct}</th>
                    <th style='width:5%;border-bottom: 1px solid white;border-top: 1px solid white;'><b>{$product->quantity}</b></th>
                    <th style='width:20%;border-bottom: 1px solid white;border-top: 1px solid white;'><b>$ {$priceUnit}</b></th>
                    <th style='height:300px;width:10%;border-bottom: 1px solid white;border-top: 1px solid white;'><b> {$iva} %</b></th>
                    <th style='width:20%;border-bottom: 1px solid white;border-top: 1px solid white;'><b>$ {$pricetTotal}</b></th>
                 </tr>";
        return $data;
    }

    public function GetTotalIva($products , $quotation ,$ivaState){
        if($quotation->typeMoney == "USD" || $quotation->typeMoney == "EUR" ){
            return $this->SetPriceAsInternational(str_replace(".","," ,(( (( ($products->priceUnit * $this->GetPercent($products->percent , 100) ) + $products->priceUnit))* $ivaState))));
          }else{
              return str_replace(",","." ,number_format(( (( ($products->priceUnit * $this->GetPercent($products->percent , 100) ) + $products->priceUnit))* $ivaState)));
          }
    }

    

    public function GetTotalItem($products , $quotation){
        if($products->IVA == "0" ||  $quotation->isDDP == "0"){
            $ivaState = 0;
        }else{
            $ivaState = $this->GetPercent($quotation->IVA,100);
        }
        if($quotation->typeMoney == "USD" || $quotation->typeMoney == "EUR" ){
            return $this->SetPriceAsInternational(str_replace(".","," ,(( ($products->priceUnit * $this->GetPercent($products->percent , 100) ) + $products->priceUnit) +  (( ($products->priceUnit * $this->GetPercent($products->percent , 100) ) + $products->priceUnit))* $ivaState ) *$products->quantity ));
        }else{
            return str_replace(",","." , number_format(str_replace(",","." , ((( ( ($products->priceUnit * $this->GetPercent($products->percent , 100) ) + $products->priceUnit))* $ivaState) + ( ($products->priceUnit * $this->GetPercent($products->percent , 100) ) + $products->priceUnit) )  * $products->quantity   )));
        }
    }

    public function FromInternacionalToCalculate($price){
        return str_replace(",","." , str_replace(".","" , $price));
    }

    public function GetSubTotalItem($products , $quotation){
        $subTotal = 0;
        if($quotation->typeMoney == "USD" || $quotation->typeMoney == "EUR" ){
            $subTotal = $this->FromInternacionalToCalculate($this->SetPriceAsInternational(str_replace(".","," ,((( ($products->priceUnit * ($products->percent / 100) ) + $products->priceUnit)) )))) ;
        }else{
            $subTotal = (( ($products->priceUnit * ($products->percent / 100) ) + $products->priceUnit) * $products->quantity);
        }
        return $subTotal;
    }

    public function GetSubtotal($subtotal , $quotation){
        if($quotation->typeMoney == "USD" || $quotation->typeMoney == "EUR" ){
            return $this->SetPriceAsInternational(str_replace(".","," ,($subtotal))) ;
        }else{
            return str_replace(",","." ,number_format($subtotal));
        }
    }


    
    public function GetPriceUnit($products,$quotation ){
        if($quotation->typeMoney == "USD" || $quotation->typeMoney == "EUR" ){
            return  $this->SetPriceAsInternational(str_replace(".","," ,(( ($products->priceUnit * $this->GetPercent($products->percent , 100) ) + $products->priceUnit) )));
        }else{
            return str_replace(",","." ,number_format((( ($products->priceUnit * $this->GetPercent($products->percent , 100) ) + $products->priceUnit)) ));
        }
    }

    private function SetPriceAsInternational($price){
        $priceData = explode(",", $price);
        $centavos = substr($priceData[1] , 0 , 2);
        if(empty($centavos)){
            $centavos = "00";
        }
        return  str_replace(",",".",number_format($priceData[0]))  .",".$centavos;
    }


    public function GetPercent($price , $percent){
        return ($price/$percent);
     }


    public function DescriptionEmpty($products){
        return "
        Referencia : {$products->reference}<br>
        Nombre : {$products->name}<br>
        Presentación  {$products->presentation} <br>
        Marca : {$products->brand} <br>";
    }


    private function GetProductsByQuote($id){
        $arrayList = array();
        $ProductVo = new ProductVo();
        $ProductVo->idQuotation = $id;
        $ProductVo->idSearchField = 16;
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $ProductVo);
        while($ProductVo2 = $generalDao->GetVo($generalVo = clone $ProductVo)) {  
            if($ProductVo2->nameCompany == $_POST["nameProvider"]){          
                $arrayList[] =  $ProductVo2;
            }
        }  
        return $arrayList; 
    }

    private function GetQuote($id){
        $arrayList = array();
        $QuotationVo = new QuotationVo();
        $QuotationVo->id = $id;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $QuotationVo);
        if($QuotationVo2 = $generalDao->GetVo($generalVo = clone $QuotationVo)) {            
            return $QuotationVo2;
        }  
    }

    private function GetPerson($id){
        $arrayList = array();
        $PersonVo = new PersonVo();
        $PersonVo->id = $id;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $PersonVo);
        if($PersonVo2 = $generalDao->GetVo($generalVo = clone $PersonVo)) {            
            return $PersonVo2;
        }  
    }

    private function ReplaceActionsCompany($companyId , $text){
        $arrayToReplace = [];
        $arrayToReplace = $this->GetCompanyById($companyId);
        $arrayReplace = ["#NameCompany#" , "#nit#" , "#telefono#","#direccion#","#pais#","#ciudad#"];
        for ($i=0; $i <  count($arrayReplace); $i++) { 
           $text = str_replace($arrayReplace[$i] , $arrayToReplace[$i]  , $text);
            
        }
        return $text;
    }

    public function GetCompanyById($id){
        $CompanyVo = new CompanyVo();
        $CompanyVo->id = $id;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $CompanyVo);
        if ($CompanyVo2 = $generalDao->GetVo($generalVo = clone $CompanyVo)) {
            return [$CompanyVo2->name , $CompanyVo2->nit , $CompanyVo2->phone ,  $CompanyVo2->address , $CompanyVo2->country , $CompanyVo2->city ];
        }
        return ["Vortex Company SAS"];
    }

    private function CreateComponents()
    {
        $this->impostNameCtx = new GeneralCtx("impostNameCtx","Nombre",null,true);
        $this->impostPercentCtx = new GeneralCtx("impostPercentCtx","Porcentaje",null,true);
       
        $this->utilJQ = new UtilJquery("Impost");
    }
    private function SetDataToVo()
    {
        $impostVo = new ImpostVo();
        $impostVo->id = $this->idDataInputForm;
        $impostVo->name = $_POST["impostNameCtx"];
        $impostVo->percent = $_POST["impostPercentCtx"];
       
        return $impostVo;
    }
    private function SetData()
    {
        $impostVo = $this->SetDataToVo();
        $impostVo->id = null;
        $result = $this->generalDao->Set($generalVo = clone $impostVo);
        if (isset($result)) {
            $message = 'MessageSwalBasic("Registrado!","Impuesto Registrado Correctamente","success");';
            $this->utilJQ->AddFunctionJavaScript($message);
        }

    }
    private function SetUpdate()
    {
        $result = $this->generalDao->Update($generalVo = clone $this->SetDataToVo());
        if (isset($result)) {
            $message = 'MessageSwalBasic("Modificado!","Impuesto Modificado Correctamente","success");';
            $this->utilJQ->AddFunctionJavaScript($message);
        }
    }
    private function DeleteData()
    {
        $message = "";
        $impostVo = new ImpostVo();
        $impostVo->id = $this->idDataInputForm;
        if ($this->generalDao->Delete($generalVo = clone $impostVo)) {
            $message = 'MessageSwalBasic("Eliminado!","Impuesto Eliminado Correctamente","success");';
        } else {
            $message = 'MessageSwalBasic("Error!","Problema con la base de datos","error");';
        }
        $this->utilJQ->AddFunctionJavaScript($message);
    }

    public function SetDescription(){
        $OrderBuyProviderVo = new OrderBuyProviderVo();
        $generalDao = new GeneralDao();
        $OrderBuyProviderVo->id = $_POST["idDataInputForm"];
        $OrderBuyProviderVo->idSearchField = 0;
        $OrderBuyProviderVo->idUpdateField = 1;
        $OrderBuyProviderVo->description = $_POST["description"];
        $generalDao->UpdateByField($generalVo = clone $OrderBuyProviderVo);
        $message = 'MessageSwalBasic("Actualizado!","Descripción Actualizada Correctamente","success");';
        $this->utilJQ->AddFunctionJavaScript($message);
    }

    private function AssignDataToTpl()
    {
        $this->data->assign('impostNameCtx', $this->impostNameCtx->paint());
        $this->data->assign('impostPercentCtx', $this->impostPercentCtx->paint());
         $this->data->assign('tempParameter', time());
         $this->data->assign('ordenNumber', $_GET["idQuotes"]);
         
        $this->data->assign('jquery', $this->utilJQ->PaintJQ());
        $this->SetDataToTpl();
    }   
    private function SetDataToTpl()
    {
        echo $this->core->get($this->pageTpl, $this->data);
    }
  


}   