<?php
require_once(Config::PATH . Config::CONTROLLER . 'general/UtilJquery.php');
require_once(Config::PATH . Config::VIEW_HTML . 'general/comps/GeneralCtx.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralDao.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/UserVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/PersonVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/AccessHistoryVo.php');

class AccessHistoryView
{
    private $idUserCtx;
    /*=============================================
                  Template Vars
    =============================================*/
    private $utilJQ;
    private $pageTpl, $data, $core;
    /*=============================================
                Data Vars
    =============================================*/
    private $generalDao;


    function __construct($pageTpl, $data, $core)
    {
        $this->data = $data;
        $this->core = $core;
        $this->pageTpl = $pageTpl;
        $this->CreateComponents();
        $this->AssignDataToTpl();
    }
    private function CreateComponents(){
        $this->idUserCtx = new GeneralCtx("idUserCtx","user","hidden",false);
        $this->utilJQ = new UtilJquery("AccessHistory");
    }

    private function AssignDataToTpl(){
        $assign = '';
        if(isset($_GET["idUser"])){
            $assign = 'SetValueToInputText("idUserCtx",'.$_GET["idUser"].');';
        }
        $this->data->assign('idUserCtx', $this->idUserCtx->paint());
        $this->data->assign('nameUser', $this->GetNameUser($_GET["idUser"]));
        $this->utilJQ->AddFunctionJavaScript($assign);
         $this->data->assign('tempParameter', time());
        $this->data->assign('jquery', $this->utilJQ->PaintJQ());
        $this->SetDataToTpl();
    }

    private function GetNameUser($idUser){
        $personVo = new PersonVo();
         $generalDao = new GeneralDao();
        $personVo->id = $idUser;
            $generalDao->GetById($generalVo = clone $personVo);
            if($personVo2 = $generalDao->GetVo($generalVo = clone $personVo)){
              if($personVo2->state ==1){
                return  htmlspecialchars($personVo2->names." ".$personVo2->lastNames);
              }
            }    
        }

    private function SetDataToTpl(){
        echo $this->core->get($this->pageTpl, $this->data);
    }

}