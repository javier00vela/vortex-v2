<?php
require_once(__DIR__ . '/../../../../controller/general/Config.php');
require_once(Config::PATH . Config::CONTROLLER.Config::GENERAL . 'ManageArrays.php');
require_once(Config::PATH . Config::BACKEND . 'modules/AccessHistoryVo.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralDao.php');


class AjaxDataAccessHistory
{

    private $isActionFromAjax;
    private $idUser;

    function __construct(){
        if (isset($_POST['isActionFromAjax']) && isset($_POST['idUser'])) {
            $this->isActionFromAjax = $_POST['isActionFromAjax'];
            $this->idUser = $_POST['idUser'];
            if ($this->isActionFromAjax == "true") {
               $this->GetDataInJsonForTbl();
            }
        }
    }

    

    public function GetDataInJsonForTbl()
    {
        $existData = false;
        $accessHistoryVo = new AccessHistoryVo();
        $accessHistoryVo->idUser = $this->idUser;
        $accessHistoryVo->isList = false;
        $accessHistoryVo->idSearchField = 7;
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $accessHistoryVo);

        $string = "
    {\"data\": [";
        while ($accessHistoryVo2 = $generalDao->GetVo($generalVo = clone $accessHistoryVo)) {
            $existData = true;
            $json = array();
            $json[] = $accessHistoryVo2->id;
            $json[] = $accessHistoryVo2->login;
            $json[] = $accessHistoryVo2->logout;
            $json[] = $accessHistoryVo2->numHours;
            $json[] = $accessHistoryVo2->ipAddress;
            $json[] = $accessHistoryVo2->device;
            $json[] = $accessHistoryVo2->browser;
            $json[] = $accessHistoryVo2->idUser;
            
            $string .= json_encode($json) . ",";
        }
        $string .= "-]}-";

        if($existData){
            echo  str_replace(",-]}-", "]}", $string);
        }else{
            echo "{ \"data\": [] }";
        }

    }
}

$ajaxDataAccessHistory = new AjaxDataAccessHistory();
