<?php
require_once(__DIR__ . '/../../../../controller/general/Config.php');
require_once(Config::PATH . Config::CONTROLLER.Config::GENERAL . 'ManageArrays.php');
require_once(Config::PATH . Config::BACKEND . 'modules/DiagnosisVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/PhotosDiagnosisVo.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralDao.php');

class AjaxDataImpost{

    private $isActionFromAjax;
    private $optionInputForm;
    private $idDataInputForm;

    function __construct()
    {
        if (isset($_POST['isActionFromAjax'])) {
            $this->isActionFromAjax = $_POST['isActionFromAjax'];
            $this->optionInputForm = $_POST['optionInputForm'];
            $this->idDataInputForm = $_POST['idDataInputForm'];
            if(isset($_POST["listData"])){
                $this->dataList = $_POST["listData"];  
            }
            
            if ($this->isActionFromAjax == "true") {
                if ($this->optionInputForm == 3) {
                    $this->SetDataInControls();
                }else if($this->optionInputForm == 5){
                $this->GetLengthImpost();
                }else {
                    $this->GetDataInJsonForTbl();
                }
            }
        }
    }        

    private function ValidateIsVisible(){
            return "<script> 
            CKEDITOR.replace('CommentClient');
            CKEDITOR.replace('observationMachine');
            CKEDITOR.replace('MachineState');
            </script>";
    }

    private function ValidateVisibleToolbox(){
        return "<script> 
        CKEDITOR.inline('CommentClient' ,{
            removePlugins: 'toolbar'});
        CKEDITOR.inline('observationMachine',{
            removePlugins: 'toolbar'});
        CKEDITOR.inline('MachineState',{
            removePlugins: 'toolbar'});
        </script>";
}

    private function SetDataInControls(){
        $SetInputsDataFJQ = "";
        $DiagnosisVo = new DiagnosisVo();
        $DiagnosisVo->id = $_POST["id"];
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $DiagnosisVo);
        $photos = $this->GetPhotosVo($DiagnosisVo->id);
        if($DiagnosisVo2 = $generalDao->GetVo($generalVo = clone $DiagnosisVo)){
            if($_POST["edit_data"] == "0"){
               $SetInputsDataFJQ .= $this->ValidateIsVisible(); 
               $SetInputsDataFJQ = $this->GetDataToEdit($DiagnosisVo2 ,$photos );
            }else{
                $SetInputsDataFJQ .=  $this->GetDataToEdit($DiagnosisVo2 ,$photos ); 
                $SetInputsDataFJQ .=  $this->ValidateVisibleToolbox();
                $SetInputsDataFJQ .=  $this->BlockFieldToSee();
            }
            
        }
        echo $SetInputsDataFJQ;
    }

    public function BlockFieldToSee(){
       return " <script> 
       $('#send').text('actualizar');
       $('[name=CommentClient]').attr('readonly','true');
       $('[name=MachineState]').attr('readonly','true');
       $('[id*=photo_field]').each(function(){
           $(this).remove();
       });
       $('#updateClientBtn').remove();
       $('#updateUserBtn').remove();
       $('[name=checkStatusMachine]').attr('disabled','true');
       $('[name=observationMachine]').attr('disabled','true');
       $('#send').remove();
       </script>";
    }

    public function GetDataToEdit($DiagnosisVo2 , $photos){
        $mcliente =preg_replace("[\n|\r|\n\r]", '', $DiagnosisVo2->commentClient );
        $machineState = preg_replace("[\n|\r|\n\r]", '', $DiagnosisVo2->machineCommentState );
        $machineObservation = preg_replace("[\n|\r|\n\r]", '', $DiagnosisVo2->machineObservation );

        return "
                            <script>
                            $('#send').text('Actualizar');
                            $('#idDataInputForm').val('{$DiagnosisVo2->id}');
                            $('#optionInputForm').val('4');
                            $('#checkStatusMachine').val('{$DiagnosisVo2->idStatusMachine}');
                            if('{$DiagnosisVo2->firmClient}' != ''){
                                $('#firmClienteDiv').removeAttr('class');
                                $('#firma1').val('{$DiagnosisVo2->firmClient}');
                                $('#firmClienteImg').attr('src', '{$DiagnosisVo2->firmClient}');
                            }
                            if('{$DiagnosisVo2->firmMaintenance}' != ''){
                                $('#firmMainDiv').removeAttr('class');
                                $('#firma2').val('{$DiagnosisVo2->firmMaintenance}');
                                $('#firmMainImg').attr('src', '{$DiagnosisVo2->firmMaintenance}');
                            }

                            if('{$photos->photo1}' == 'null'){
                                
                                $('#photo1').attr('src'  , 'view/img/upload.jpg');
                            }else{
                                $('[name=photo1temp]').val('{$photos->photo1}')
                                $('#photo1').attr('src'  , '{$photos->photo1}');
                            }
                            if('{$photos->photo2}' == 'null'){
                                $('#photo2').attr('src'  , 'view/img/upload.jpg');
                            }else{
                                $('[name=photo2temp]').val('{$photos->photo2}')
                                $('#photo2').attr('src'  , '{$photos->photo2}')
                            } 
                            if('{$photos->photo3}'  == 'null'){
                                $('#photo3').attr('src'  , 'view/img/upload.jpg');
                            }else{
                                $('[name=photo3temp]').val('{$photos->photo3}')
                                $('#photo3').attr('src'  , '{$photos->photo3}');
                            }
                            $('#FirmClientCtx').css('display','none');
                            $('#FirmUserCtx').css('display','none');
                            $('#firmaG1').css('display','none');
                            $('#firmaB1').css('display','none');
                            $('#firmaG2').css('display','none');
                            $('#firmaB2').css('display','none');
                            $('#updateUserBtn').html('<button form=\"null\" class=\"btn btn-warning btn-block\" onClick=\"ModifyFirm(1)\">Editar Firma Usuario</button>');
                            $('#updateClientBtn').html('<button form=\"null\" class=\"btn btn-warning btn-block\" onClick=\"ModifyFirm(2)\">Editar Firma Cliente</button>');
                            
                            
                               (function($){
                                $('[name=CommentClient]').val('{$mcliente}');
                                $('[name=noteGeneral]').val('{$DiagnosisVo2->noteGeneral}');
                                $('[name=MachineState]').val('{$machineState}');
                                $('[name=observationMachine]').val('{$machineObservation}');
                                $('#FirmClientCtx').val('{$DiagnosisVo2->firmClient}');
                                $('#FirmUserCtx').val('{$DiagnosisVo2->firmMaintenance}');

                                                                   
                               })(jQuery);
                            </script>
                        ";
    }

    public function GetDataInJsonForTbl()
    {
        $existData = false;
        $impostVo = new ImpostVo();
        $impostVo->isList = true;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $impostVo);

        $string = "
        {\"data\": [";
        while ($impostVo2 = $generalDao->GetVo($generalVo = clone $impostVo)) {
            $existData = true;
            $json = array();
           // $json[] = $impostVo2->id;
            $json[] = htmlspecialchars(ucfirst(strtolower($impostVo2->name)));
            $json[] = $impostVo2->percent;
            if(ucfirst(strtolower($impostVo2->name)) != "Iva"){
                $json[] = "<tip title='Modificar' class='yellow-tooltip' data-toggle='tooltip' data-placement='top'><button id='updateBtn' name='updateBtn' class='btn btn-warning btn-xs' data-toggle='modal' data-target='#impostPopUpAdd' title='Modificar' onclick='UpdateData({$impostVo2->id},\"impost\",\"impost\");'><i class='fa fa-pencil'></i></button> </tip>" .
                " <tip title='Eliminar' class='red-tooltip' data-toggle='tooltip' data-placement='top'><a id='deleteBtn' name='deleteBtn' class='btn btn-danger  btn-xs' title='Eliminar' onclick='DeleteData({$impostVo2->id},\"impost\")'><i class='fa fa-times'></i></a></tip> ";
            }else{
                 $json[] = "<tip title='Modificar' class='yellow-tooltip' data-toggle='tooltip' data-placement='top'><button id='updateBtn' name='updateBtn' class='btn btn-warning btn-xs' data-toggle='modal' data-target='#impostPopUpAdd' title='Modificar' onclick='UpdateData({$impostVo2->id},\"impost\",\"impost\");'><i class='fa fa-pencil'></i></button> </tip>";
            }

            $string .= json_encode($json) . ",";

        }
        $string .= "-]}-";

        if($existData){
            echo  str_replace(",-]}-", "]}", $string);
        }else{
            echo "{ \"data\": [] }";
        }
    }

    public function GetPhotosVo($id){
        $arrayList = array();
        $PhotosDiagnosisVo = new PhotosDiagnosisVo();
        $PhotosDiagnosisVo->idSearchField = 4;
        $PhotosDiagnosisVo->idDiagnosis = $id;
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $PhotosDiagnosisVo);
        if($PhotosDiagnosisVo2 = $generalDao->GetVo($generalVo = clone $PhotosDiagnosisVo)){
            return $PhotosDiagnosisVo2;
        }
        
    }

    
}

$ajaxDataImpost = new AjaxDataImpost();
