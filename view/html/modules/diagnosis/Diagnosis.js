// jshint unused:true
"use strict";
var photoId;

$("#sendExcelTemplateBtn").click(function () {
    if ($(".file").val() == "") {
        MessageSwalBasic("Error", "Por favor seleccione un archivo excel a cargar", "error");
    } else {
        SetValueToInputText("optionInputTemplateForm", "6");
        $("[name=sendExcelTemplateBtn]").attr("disabled", "true");
        $("[name=sendExcelTemplateBtn]").html("<i class='fa fa-spin fa-refresh'></i> Cargando ");
        $("#excelTemplateForm").submit();
    }
})

function LoadTable() {
    RemoveActionModule(29);
    GetArrayFromDataModule(29);
    setTimeout(function () {
        var parameters = {
            "isActionFromAjax": true,
            "optionInputForm": 0,
            "idDataInputForm": 0,
            "listData": arrayResponse,
        };


        $('#impostTbl').DataTable({
            responsive: true,
            "drawCallback": function (settings) {
                $('[data-toggle="tooltip"]').tooltip()
            },
            language: {
                "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
            },
            "ajax": {
                url: "view/html/modules/impost/AjaxDataImpost.php",
                method: "POST",
                data: parameters
            }
        });
    }, 500);
}

/*********************************CLICK EVENTS**************************/

function ChangeFormatPrices() {
    $("#impostPercentCtx").keypress(function (event) {
        console.log(event.charCode);
        if (event.charCode > 58 || event.charCode < 43 || event.charCode == 45 || event.charCode == 46) {
            return false;
        }
    });
}

function ValidatePriceUnit() {
    $("#impostPercentCtx").change(function (event) {
        var cont = 0;
        var numberType = $(this).val();
        for (var i = 0; i < numberType.toString().length; i++) {
            if (numberType.toString()[i] == "," || numberType.toString()[i] == ".") {
                cont++;
            }
        }
        if (cont > 1) {
            numberType = "";
        }
        $(this).val(numberType);
    });
}

$(".file").change(function () {
    var img = this.files[0];
    if (img["type"] !== "image/jpeg" /*&& img["type"] !== "image/png"*/) {
        $(this).val("");
        MessageSwalBasic("Error al Subir Imagen", "Los Tipos de Imagen Permitidos son: JPEG", "error");
    } else if (img["type"] > 27000000) {
        $(this).val("");
        MessageSwalBasic("Error al Subir Imagen", "¡La Imagen no Debe Pesar mas de 50MB", "error");
    }
})

function LoadFile() {
    $(".file").css({ "visibility": "hidden", "position": "absolute" });
    $(document).on('click', '.browse', function (evt) {
        var file = $(this).parent().parent().parent().find('.file');
        file.trigger('click');

    });
    $(document).on('change', '.file', function (evt) {
        var nodeSpace = $(this).attr("photo-node");
        var positionPhoto = ClearTextAreaPhoto(nodeSpace);
        photoId = $(".file").parent().parent().children()[nodeSpace].id;
        var tgt = evt.target || window.event.srcElement,
            files = tgt.files;

        // FileReader support

        var fr = new FileReader();
        fr.onload = function () {
            console.log(fr.result);
            imageToDataUri(positionPhoto, photoId, fr.result, 200, 200);

        }
        fr.readAsDataURL(files[0]);

    });
}

function imageToDataUri(position, photoid, imgen, width, height) {
    var img = new Image;
    img.src = imgen;
    img.onload = function () {
        // create an off-screen canvas
        var canvas = document.createElement('canvas'),
            ctx = canvas.getContext('2d');

        // set its dimension to target size
        canvas.width = width;
        canvas.height = height;

        // draw source image into the off-screen canvas:
        ctx.drawImage(img, 0, 0, width, height);

        // encode image to data-uri with base64 version of compressed image
        $("#" + photoid).attr("src", canvas.toDataURL('image/jpeg', [0.0, 1.0]));
        $("[name=photo" + position + "temp]").val(canvas.toDataURL('image/jpeg', [0.0, 1.0]));
        console.log(canvas.toDataURL('image/jpeg', [0.0, 1.0]));
    };
}

function ClearTextAreaPhoto(id) {

    switch (id) {
        case '0':
            $("[name=photo1temp]").val('null');
            return 1;
            break;
        case '3':
            $("[name=photo2temp]").val('null');
            return 2;
            break;
        case '6':
            $("[name=photo3temp]").val('null');
            return 3;
            break;
    }
}

function SetDataDiagnosis() {
    if ($("#diagnosisBool").attr("data-diagnosis") != "0") {
        GetDataToSetForm($("#diagnosisBool").attr("data-diagnosis"));
    }
}

function GetDataToSetForm(id) {
    var parameters = {
        "isActionFromAjax": true,
        "optionInputForm": 3,
        "idDataInputForm": 0,
        "id": id,
        "edit_data": $("#edit_data").val()
    }

    $.ajax({
        url: "view/html/modules/diagnosis/AjaxDataDiagnosis.php",
        type: 'post',
        data: parameters,
        success: function (data) {
            $("body").append(data);
        }

    });
}


function SavePhotoByCamera() {
    $("#photo" + $("#tipoImagen").val()).attr("src", $("#img-camera").attr('src'));
    console.log($("#img-camera").attr('src'));
    $("[name=photo" + $("#tipoImagen").val() + "temp]").val($("#img-camera").attr('src'));
}


const video = document.getElementById('video');
const canvas = document.getElementById('canvas');
const snap = document.getElementById("snap");
const errorMsgElement = document.querySelector('span#errorMsg');

const constraints = {
    audio: false,
    video: {
        width: 1280, height: 720
    }
};

// Access webcam
async function init() {
    try {
        const stream = await navigator.mediaDevices.getUserMedia(constraints);
        handleSuccess(stream);
    } catch (e) {
        $("#impostPopUpAdd").modal("hide");
        MessageSwalBasic("VERIFICAR !!", "Debes verificar que tengas los permisos y/o tengas conectado la camara y/o el audio del navegador", "warning");
    }
}

// Success
function handleSuccess(stream) {
    window.stream = stream;
    video.srcObject = stream;

}
var photobase64;
var context = canvas.getContext('2d');

snap.addEventListener("click", function () {
    context.drawImage(video, 0, 0, 480, 300);
    photobase64 = canvas.toDataURL();
    $("#imagenDiv").html("<img id='img-camera' src='" + photobase64 + "' width='100%'>")
});


function SaveFirma(type) {
    if (type == 1) {
        $("#firma1").val(document.getElementById('FirmClientCtx').toDataURL());
        ToastShow("success", "FIRMA AGREGADA !!", 'Se ha Agregado la firma correctamente', "2000");
    } else {
        $("#firma2").val(document.getElementById('FirmUserCtx').toDataURL());
        ToastShow("success", "FIRMA AGREGADA !!", 'Se ha Agregado la firma correctamente', "2000");
    }
}

function ValidateForm() {
    if ($("#firma2").val()) {
        if ($("#firma1").val()) {
            $("#form").submit();
        } else {
            MessageSwalBasic("VERIFICAR !!", "Debes Agregar la firma de cliente", "warning");
        }
    } else {
        MessageSwalBasic("VERIFICAR !!", "Debes Agregar la firma de usuario", "warning");
    }
}


function ModifyFirm(typeFirm) {

    if (typeFirm == 2) {
        $('#FirmClientCtx').css('display', 'block');
        $('#firmaG1').css('display', 'block');
        $('#firmaB1').css('display', 'block');
        $('#updateClientBtn').remove();
        $("#firmClienteDiv").remove();
    } else {
        $('#FirmUserCtx').css('display', 'block');
        $('#firmaG2').css('display', 'block');
        $('#firmaB2').css('display', 'block');
        $('#updateUserBtn').remove();
        $("#firmMainDiv").remove();
    }
}


$(function () {

    ChangeFormatPrices();
    ValidatePriceUnit();
    LoadTable();
    LoadFile();
    SetDataDiagnosis();
    $("#FirmClientCtx").jqScribble();
    $("#FirmUserCtx").jqScribble();
    if (document.createEvent("TouchEvent")) {
        $("[name=cameraWindows]").remove();
    }

});