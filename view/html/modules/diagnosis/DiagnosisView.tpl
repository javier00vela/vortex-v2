<form method="post" id="form">
    <input id="optionInputForm" name="optionInputForm" type="hidden" value="1">
    <input id="idDataInputForm" name="idDataInputForm" type="hidden" value="0">
    <input id="isActionFromAjax" name="isActionFromAjax" type="hidden" value="false">
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                Diagnostico <strong>('{$productData->name}')</strong>
            </h1>
            <ol class="breadcrumb">
                <li><a href="Home"><i class="fa fa-dashboard"></i> Inicio</a></li>
                <li class="active">Diagnostico {$productData->name}</li>
            </ol>
        </section>
        <section class="content">
            <div class="box">
                <nav class="navbar navbar-light">
                    <div class="container-fluid">
                        <ul class="nav navbar-nav">
                            <li>
                                <a style="cursor: pointer;" id="userAddBtn" name="userAddBtn"
                                    onclick="location.href = 'Home' ">
                                    <i class="fa fa-home"></i> Menu Principal
                                </a>
                            </li>
                        </ul>
                    </div>
                </nav>
                <input type="hidden" id="diagnosisBool" data-diagnosis="{$diagnosisId}" value="{$diagnosisBool}">
                <div class="box-body ">
                    <div class="row text-center">
                        <div class="col-sm-6">
                            <div class="box box-primary" style="box-shadow: 0 0 4px black;">
                                <div class="box-tools pull-right" data-toggle="tooltip" title=""
                                    data-original-title="Status">
                                    <small class="label pull-right bg-green">5%</small>
                                </div>
                                <div class="box-header with-border">
                                    Datos Cliente
                                </div>
                                <div class="box-body">
                                    <ul class="list-group">
                                        <li class="list-group-item"><b>nombre</b> : {if $companyData->name eq ""} Equipo
                                            Externo {else} {$companyData->name} {/if}</li>
                                        <li class="list-group-item"><b>dirección</b> : {if $companyData->address eq ""}
                                            Equipo Externo {else} {$companyData->address} {/if}</li>
                                        <li class="list-group-item"><b>telefóno</b> : {if $companyData->phone eq ""}
                                            Equipo Externo {else} {$companyData->phone} {/if} </li>
                                        <li class="list-group-item"><b>correo</b> : {if $companyData->email eq ""}
                                            Equipo Externo {else} {$companyData->email} {/if}</li>
                                        <li class="list-group-item"><b>Web</b> : {if $companyData->webSite eq ""} Equipo
                                            Externo {else} {$companyData->webSite} {/if}</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="box box-primary" style="box-shadow: 0 0 4px black;">
                                <div class="box-tools pull-right" data-toggle="tooltip" title=""
                                    data-original-title="Status">
                                    <small class="label pull-right bg-green">5%</small>
                                </div>
                                <div class="box-header with-border">
                                    Datos Producto
                                </div>
                                <div class="box-body">
                                    <ul class="list-group">
                                        <li class="list-group-item"><b>nombre</b> : {$productData->name}</li>
                                        <li class="list-group-item"><b>referencia</b> : {$productData->reference}</li>
                                        <li class="list-group-item"><b>marca</b> : {$productData->brand}</li>
                                        <li class="list-group-item"><b>presentación</b> : {$productData->presentation}
                                        </li>
                                        <li class="list-group-item"><b>Tipo Producto</b> : {$productData->typeProduct}
                                        </li>
                                        <li class="list-group-item"><b>Serial</b> : {$productData->serial}
                                        </li>
                                    </ul>
                                </div>
                            </div>

                        </div>

                    </div>
                    <input type="hidden" id="edit_data" value="{$edit_data}">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="box box-primary" style="box-shadow: 0 0 4px black;">
                                <div class="box-tools pull-right" data-toggle="tooltip" title=""
                                    data-original-title="Status">
                                    <small class="label pull-right bg-green">10%</small>
                                </div>
                                <div class="box-header with-border">
                                    Comentario Cliente
                                </div>
                                <div class="box-body">
                                    <textarea class="form-control" id="CommentClient" name="CommentClient" cols="30"
                                        rows="10" style="resize: none;"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="box box-primary" style="box-shadow: 0 0 4px black;">
                                <div class="box-tools pull-right" data-toggle="tooltip" title=""
                                    data-original-title="Status">
                                    <small class="label pull-right bg-green">50%</small>
                                </div>
                                <div class="box-header with-border">
                                    Estado Inicial
                                </div>
                                <div class="box-body">

                                    <div class="box" style="box-shadow: 0 0 1px black;">
                                        <div class="box-header with-border">
                                            Estado Encontrado de la maquina
                                        </div>
                                        <div class="box-body">
                                            <textarea class="form-control" id="MachineState" name="MachineState"
                                                cols="30" rows="10" style="resize: none;"></textarea>
                                        </div>
                                    </div>

                                    <div class="box" style="box-shadow: 0 0 1px black;">
                                        <div class="box-header with-border">
                                            Fotos y/o Evidencias
                                        </div>
                                        <input type="hidden" name="photo1temp" value="null">
                                        <input type="hidden" name="photo2temp" value="null">
                                        <input type="hidden" name="photo3temp" value="null">
                                        <div class="box-body">
                                            <div class="row text-center">
                                                <div class="col-sm-4">

                                                    <img src="view/img/upload.jpg" class="img-responsive img-thumbnail"
                                                        id="photo1" width="270" height="250">

                                                    <input type="hidden" name="MAX_FILE_SIZE" value="500000000000" />
                                                    <div class="form-group" id="photo_field">
                                                        <label>Inserte una imagen para ser cargada :</label>
                                                        <input type="file" photo-node="0" name="photo1" accept="image/*"
                                                            class="file">
                                                        <div class="input-group col-6">
                                                            <span class="input-group-addon"><i
                                                                    class="glyphicon glyphicon-folder-open"></i></span>
                                                            <input type="text" class="form-control input-lg" disabled
                                                                placeholder="Sin Imagen">
                                                            <span class="input-group-btn">
                                                                <button class="browse btn btn-primary input-lg"
                                                                    type="button"><i class="fa fa-plus"></i></button>
                                                            </span>
                                                        </div>
                                                        <div class="input-group col-sm-12 col-xs-12"
                                                            name="cameraWindows">
                                                            <button class="btn btn-block btn-primary btn-xs-block"
                                                                style="margin: 5px;" data-toggle="modal"
                                                                data-target="#impostPopUpAdd" form="null"
                                                                onclick="$('#tipoImagen').val(1); init();">Subir desde
                                                                camara web</button>
                                                        </div>
                                                    </div>


                                                </div>
                                                <div class="col-sm-4 text-center">

                                                    <img src="view/img/upload.jpg" class="img-responsive img-thumbnail"
                                                        id="photo2" width="270" height="250">
                                                    <input type="hidden" name="MAX_FILE_SIZE" value="500000000000" />
                                                    <div class="form-group" id="photo_field">
                                                        <label>Inserte una imagen para ser cargada :</label>
                                                        <input type="file" name="photo2" photo-node="3" accept="image/*"
                                                            class="file">
                                                        <div class="input-group col-12">
                                                            <span class="input-group-addon"><i
                                                                    class="glyphicon glyphicon-folder-open"></i></span>
                                                            <input type="text" class="form-control input-lg" disabled
                                                                placeholder="Sin Imagen">
                                                            <span class="input-group-btn">
                                                                <button class="browse btn btn-primary input-lg"
                                                                    type="button"><i class="fa fa-plus"></i></button>
                                                            </span>
                                                        </div>
                                                        <div class="input-group col-sm-12 col-xs-12"
                                                            name="cameraWindows">
                                                            <button class="btn btn-block btn-primary btn-xs-block"
                                                                style="margin: 5px;" data-toggle="modal"
                                                                data-target="#impostPopUpAdd" form="null"
                                                                onclick="$('#tipoImagen').val(2); init();">Subir desde
                                                                camara web</button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4 text-center">

                                                    <img src="view/img/upload.jpg" class="img-responsive img-thumbnail"
                                                        id="photo3" width="270" height="250">
                                                    <input type="hidden" name="MAX_FILE_SIZE" value="500000000000"
                                                        photo-node="7" />
                                                    <div class="form-group" id="photo_field">
                                                        <label>Inserte una imagen para ser cargada :</label>
                                                        <input type="file" name="photo3" accept="image/*" class="file"
                                                            photo-node="6">
                                                        <div class="input-group col-12">
                                                            <span class="input-group-addon"><i
                                                                    class="glyphicon glyphicon-folder-open"></i></span>
                                                            <input type="text" class="form-control input-lg" disabled
                                                                placeholder="Sin Imagen">
                                                            <span class="input-group-btn">
                                                                <button
                                                                    class="browse btn btn-primary input-lg btn-xs-block"
                                                                    type="button"><i class="fa fa-plus"></i></button>
                                                            </span>
                                                        </div>
                                                        <div class="input-group col-sm-12 col-xs-12"
                                                            name="cameraWindows">
                                                            <button class="btn btn-block btn-primary btn-xs-block"
                                                                style="margin: 5px;" data-toggle="modal"
                                                                data-target="#impostPopUpAdd" form="null"
                                                                onclick="$('#tipoImagen').val(3); init();">Subir desde
                                                                camara web</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container-fluid">
                    <div class="row">
                        <div class="box box-primary" style="box-shadow: 0 0 4px black;">
                            <div class="box-tools pull-right" data-toggle="tooltip" title=""
                                data-original-title="Status">
                                <small class="label pull-right bg-green">25%</small>
                            </div>
                            <div class="box-header with-border">
                                Resultados de Funcionamiento
                            </div>
                            <div class="box-body">

                                <div class="box" style="box-shadow: 0 0 1px black;">
                                    <div class="box-header with-border">
                                        Resultados de Evaluación de Funcionamiento
                                    </div>
                                    <div class="box-body">
                                        <div class="col-sm-12 col-xs-12">
                                            <label for="">Estado de la maquina </label>
                                            <select name="checkStatusMachine" id="checkStatusMachine"
                                                class="form-control m-3">
                                                <option value="1"> funcionamiento Correcto y satisfactorio
                                                </option>
                                                <option value="2">Funcional con Daño Menor</option>
                                                <option value="3">Equipo para Mantenimiento Correctivo</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="box" style="box-shadow: 0 0 1px black;">

                                        <div class="box-header with-border">
                                            Observaciones Equipo
                                        </div>
                                        <div class="box-body">
                                            <textarea class="form-control" id="observationMachine"
                                                name="observationMachine" cols="30" rows="10"
                                                style="resize: none;"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="container-fluid">
                        <div class="row">
                            <div class="box box-primary" style="box-shadow: 0 0 4px black;">
                                <div class="box-tools pull-right" data-toggle="tooltip" title=""
                                    data-original-title="Status">
                                    <small class="label pull-right bg-green">0%</small>
                                </div>
                                <div class="box-header with-border">
                                    Notas Generales (Opcional)
                                </div>
                                <div class="box-body">
                                    <div class="col-sm-12">
                                        <textarea class="form-control" id="noteGeneral"
                                        name="noteGeneral" cols="30" rows="10"
                                        style="resize: none;"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>



                    <div class="container-fluid">
                        <div class="row">
                            <div class="box box-primary" style="box-shadow: 0 0 4px black;">
                                <div class="box-tools pull-right" data-toggle="tooltip" title=""
                                    data-original-title="Status">
                                    <small class="label pull-right bg-green">5%</small>
                                </div>
                                <div class="box-header with-border">
                                    Firmados
                                </div>
                                <div class="box-body">
                                    <div class="col-sm-6">
                                        <div class="hidden" id="firmClienteDiv">
                                            <label for="">firma cliente actual</label>
                                            <img style="border: 1px solid black;width: 100%" id="firmClienteImg" src="">
                                        </div>
                                        <label>Firma Cliente</label>
                                        <canvas class="img-thumbnail" id="FirmClientCtx"
                                            style="border: 1px solid black;width: 100%"></canvas>
                                        <input type="hidden" name="firma1" id="firma1" value=''>
                                        <button class="btn btn-block btn-danger" id="firmaB1" form="null"
                                            onclick="$('#FirmClientCtx').data('jqScribble').clear()">Borrar Firma
                                            Cliente</button>
                                        <button class="btn btn-block btn-primary" id="firmaG1" form="null"
                                            onclick="SaveFirma(1)">Guardar Firma</button>
                                            <div id="updateClientBtn"></div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="hidden" id="firmMainDiv">
                                            <label for="">firma Usuario actual</label>
                                            <img style="border: 1px solid black;width: 100%" id="firmMainImg" src="">
                                        </div>
                                        <label>Firma Usuario</label>
                                        <canvas class="img-thumbnail" id="FirmUserCtx"
                                            style="border: 1px solid black;width: 100%"></canvas>
                                        <input type="hidden" name="firma2" id="firma2" value=''>
                                        <button class="btn btn-block btn-danger" id="firmaB2" form="null"
                                            onclick="$('#FirmUserCtx').data('jqScribble').clear()">Borrar Firma
                                            Usuario</button>
                                        <button class="btn btn-block btn-primary" id="firmaG2" form="null"
                                            onclick="SaveFirma(2)">Guardar Firma</button>
                                            <div id="updateUserBtn"></div>
                                    </div>

                                </div>
                                <div class="box-footer">
                                    <button form="null" onclick="ValidateForm();" id="send"
                                        class="btn btn-primary btn-block">Agregar</button>
                                </div>

                            </div>
                        </div>
                    </div>
        </section>
    </div>
    </div>
</form>

<div id="impostPopUpAdd" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header" style="background:#3c8dbc; color:white">
                <button type="button" class="close" data-dismiss="modal" data-target='#impostPopUpAdd'
                    onclick="ResetControls('impostForm')">&times;</button>
                <h4 class="modal-title">Agregar imagen</h4>
            </div>
            <!--=====================================
              CUERPO DEL MODAL
              ======================================-->
            <div class="modal-body">
                <div class="box-body">
                    <div class="row">

                        <input type="hidden" id="tipoImagen" value="0">
                        <div class="col-sm-6 col-xs-12">
                            <label for="">Grabación de la camara</label>
                            <video class="img-thumbnail" id="video" width="100%" playsinline autoplay></video>
                        </div>
                        <div class="form-group col-sm-6 col-xs-12">
                            <label for="">Imagen Tomada</label>
                            <div id="imagenDiv">
                                <canvas class="img-thumbnail" id="canvas" width="400px" height="310px;"></canvas>
                            </div>
                        </div>
                        <div class="controller">
                            <button class="btn btn-primary btn-block" form="null" id="snap">Capturar</button>
                        </div>

                    </div>
                </div>
            </div>
            <!--=====================================
              PIE DEL MODAL
              ======================================-->
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal"
                    data-target='#impostPopUpAdd' onclick="ResetControls('impostForm')">Salir</button>
                <button data-dismiss="modal" data-target='#impostPopUpAdd' class="btn btn-primary"
                    onclick="SavePhotoByCamera()">Guardar </button>
            </div>

        </div>
    </div>
</div>



<div id="containerResponseAjax"></div>
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<script src="view/html/modules/diagnosis/Diagnosis.js?v= {$tempParameter}"></script>
<script src="view/html/modules/diagnosis/jquery.jqscribble.js"></script>
{$jquery}