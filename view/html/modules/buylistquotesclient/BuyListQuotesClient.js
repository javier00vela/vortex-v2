// jshint unused:true
"use strict";


function LoadTable() {
    RemoveActionModule(37);
    GetArrayFromDataModule(37);
    setTimeout(function() {
        var parameters = {
            "isActionFromAjax": true,
            "optionInputForm": 0,
            "idDataInputForm": 0,
            "idQuote": $("#orderNumber").val(),
            "listData": arrayResponse,
        };


        $('#QuotesTbl').DataTable({
            responsive: true,
            "drawCallback": function(settings) {
                $('[data-toggle="tooltip"]').tooltip()
            },
            language: {
                "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
            },
            "ajax": {
                url: "view/html/modules/buylistquotesclient/AjaxDataBuyQuotesListClient.php?idQuote=" + $("#orderNumber").val(),
                method: "POST",
                data: parameters
            }
        });
    }, 500);
}

/*********************************CLICK EVENTS**************************/



function ChangePercent(id , type){
    var parameters = {
        "isActionFromAjax": true,
        "optionInputForm": 6,
        "idDataInputForm": 0,
        "pay" : $("#pay-"+id).val(),
        "percent" : $("#percent-"+id).val(),
        "description" : $("#description").val(),
        "type": type,
        "id": id
    }

    $.ajax ({
        url: "view/html/modules/buylistquotesclient/AjaxDataBuyQuotesListClient.php?idQuote=" + $("#orderNumber").val(),
        type: 'post',
        data: parameters,
        success: function(data) {
        }
    });
}

function PrintDataOrden(id, Provider , idOrden , idProvider) {
    var parameters = {
        "isActionFromAjax": true,
        "optionInputForm": 3,
        "idDataInputForm": 0,
        "idFactura" : idOrden,
        "idProvider":idProvider,
        "nameProvider": Provider,
        "pay" : $("#pay-"+idOrden).val(),
        "percent" : $("#percent-"+idOrden).val(),
        "id": id,
        "type":"orden"
    }
    console.log(parameters);

    $.ajax ({
        url: "BuyListQuotesClient?idQuotes=" + id,
        type: 'post',
        data: parameters,

        beforeSend: function() {

            setTimeout(MessageSwalWaiter('cargando...', 'Abriendo Orden de Compra!'), 10000);
        },
        success: function(data) {
            var windowOpen = window.open("view/docs/pdf/Orden_" + id + ".pdf?v=" + Math.random(), '_blank');
            if (windowOpen) {
                windowOpen.focus();
                MessageSwalRedirect("GENERADO !!", "Se ha abierto el documento de Orden De Compra ", "success", "BuyListQuotes");

            } else {
                MessageSwalBasic("VERIFICAR !!", "Debes habilitar permisos para abrir ventanas emergentes", "warning");
            }
        }

    });
}

function PrintDataFactura(id, Provider , idFactura , idProvider) {
    var parameters = {
        "isActionFromAjax": true,
        "optionInputForm": 3,
        "idDataInputForm": 0,
        "idFactura" : idFactura,
        "idProvider":idProvider,
        "nameProvider": Provider,
        "pay" : $("#pay-"+idFactura).val(),
        "percent" : $("#percent-"+idFactura).val(),
        "id": id,
        "type":"factura"
    }
    console.log(parameters);

    $.ajax ({
        url: "BuyListQuotesClient?idQuotes=" + id,
        type: 'post',
        data: parameters,

        beforeSend: function() {

            setTimeout(MessageSwalWaiter('cargando...', 'Abriendo Orden de Compra!'), 10000);
        },
        success: function(data) {
            var windowOpen = window.open("view/docs/pdf/Orden_" + id + ".pdf?v=" + Math.random(), '_blank');
            if (windowOpen) {
                windowOpen.focus();
                MessageSwalRedirect("GENERADO !!", "Se ha abierto el documento de Orden De Compra ", "success", "GenerateQuotes");

            } else {
                MessageSwalBasic("VERIFICAR !!", "Debes habilitar permisos para abrir ventanas emergentes", "warning");
            }
        }

    });
}

$(function() {
    LoadTable();

});