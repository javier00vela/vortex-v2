<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Administrar procesos/mantenimientos
        </h1>
        <ol class="breadcrumb">
            <li><a href="Home"><i class="fa fa-dashboard"></i> Inicio</a></li>
            <li class="active">Administrar procesos/mantenimientos</li>
        </ol>
    </section>
    <section class="content">
        <div class="box">
            <nav class="navbar navbar-light">
                <div class="container-fluid">
                    <ul class="nav navbar-nav">
                        <li>
                            <a node="node-agregar_costo" style="cursor: pointer;" id="impostAddBtn" name="impostAddBtn" data-toggle="modal" data-target="#impostPopUpAdd" onclick="ResetControls('containerinputs');SetData()">
                                <i class="fa fa-plus-circle"></i> Agregar proceso/mantenimiento
                            </a>
                        </li>
                        <li>
                            <a style="cursor: pointer;" id="userAddBtn" name="userAddBtn" onclick="location.href = 'Home' ">
                                <i class="fa fa-home"></i> Menu Principal
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>

            <div class="box-body">
                <div class="alert bg-orange">
                    Los procesos de mantenimiento se calculan apartir de la siguiente formula : <br>
                    (<b>precio hora del proceso</b> * <b>ganancia</b> + <b>precio hora del proceso</b>) + (<b>tiempo mantenimiento</b> / <b>60</b> -> 'minutos')
                </div>
                <table id="impostTbl" name="impostTbl" class="table table-bordered table-striped dt-responsive tables dataTable no-footer dtr-inline" style="width: 100%;" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Ganancia</th>
                            <th>Tiempo ( Minutos)</th>
                            <th>Precio por Hora</th>
                            <th>Total precio Proceso</th>
                            <th>Opciones</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </section>
</div>
<!--=====================================
VENTANA MODAL AGREGAR IMPUESTO O MODIFICAR
======================================-->
<div id="impostPopUpAdd" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form id="priceMaintenceForm" role="form" method="post" enctype="multipart/form-data">
                <!--=====================================
              Hidden
              ======================================-->
                <input id="optionInputForm" name="optionInputForm" type="hidden" value="0">
                <input id="idDataInputForm" name="idDataInputForm" type="hidden" value="0">
                <input id="isActionFromAjax" name="isActionFromAjax" type="hidden" value="false">
                <!--=====================================
              CABEZA DEL MODAL
              ======================================-->
                <div class="modal-header" style="background:#3c8dbc; color:white">
                    <button type="button" class="close" data-dismiss="modal" data-target='#impostPopUpAdd' onclick="ResetControls('impostForm')">&times;</button>
                    <h4 class="modal-title">Agregar Mantenimiento</h4>
                </div>
                <!--=====================================
              CUERPO DEL MODAL
              ======================================-->
                <div class="modal-body">
                    <div class="box-body">
                        <div class="row">
                            <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-industry"></i></span> {$mainteninNameCtx}
                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-percent"></i></span> {$mainteninGananceCtx}
                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-clock-o"></i></span> {$mainteninTimeCtx}
                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-dollar"></i></span> {$mainteninPriceCtx}
                                </div>
                            </div>
                            <div class="input-group  col-xs-12">
                                <textarea class="ckeditor" id="productDescriptionAtx" name="productDescriptionAtx" required></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <!--=====================================
              PIE DEL MODAL
              ======================================-->
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal" data-target='#impostPopUpAdd' onclick="ResetControls('impostForm')">Salir</button>
                    <button type="submit" class="btn btn-primary">Guardar </button>
                </div>
            </form>

        </div>
    </div>
</div>
<div id="containerResponseAjax"></div>
<script src="view/html/modules/pricesmaintence/PricesMaintence.js?v= {$tempParameter}"></script>
{$jquery}