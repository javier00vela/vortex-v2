<?php
require_once(__DIR__ . '/../../../../controller/general/Config.php');
require_once(Config::PATH . Config::CONTROLLER.Config::GENERAL . 'ManageArrays.php');
require_once(Config::PATH . Config::BACKEND . 'modules/ImpostVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/PricesMainteinceVo.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralDao.php');

class AjaxDataImpost{

    private $isActionFromAjax;
    private $optionInputForm;
    private $idDataInputForm;
    public $dataList = [];

    function __construct()
    {
        if (isset($_POST['isActionFromAjax'])) {
            $this->isActionFromAjax = $_POST['isActionFromAjax'];
            $this->optionInputForm = $_POST['optionInputForm'];
            $this->idDataInputForm = $_POST['idDataInputForm'];
            if(isset($_POST["listData"])){
                $this->dataList = $_POST["listData"];  
            }
            if ($this->isActionFromAjax == "true") {
                if ($this->optionInputForm == 3) {
                    $this->SetDataInControls();
                }else if($this->optionInputForm == 5){
                $this->GetLengthImpost();
                }else {
                    $this->GetDataInJsonForTbl();
                }
            }
        }
    }

    private function SetDataInControls(){
        $SetInputsDataFJQ = "";
        $PricesMainteinceVo = new PricesMainteinceVo();
        $PricesMainteinceVo->id = $this->idDataInputForm;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $PricesMainteinceVo);
       
        if($PricesMainteinceVo2 = $generalDao->GetVo($generalVo = clone $PricesMainteinceVo)){
            $descriptionHtml = preg_replace("[\n|\r|\n\r]", '', $PricesMainteinceVo2->description);
            $SetInputsDataFJQ = "
                            <script>
                               (function($){
                                SetValueToInputText('mainteninNameCtx','{$PricesMainteinceVo2->name}');
                                SetValueToInputText('mainteninGananceCtx','{$PricesMainteinceVo2->ganance}');
                                SetValueToInputText('mainteninTimeCtx','{$PricesMainteinceVo2->time}');
                                SetValueToInputText('mainteninPriceCtx','{$PricesMainteinceVo2->price}');
                                //CKEDITOR.instances[productDescriptionAtx].setData('{descriptionHtml}');
                                ApplyEditor('productDescriptionAtx','$descriptionHtml');
                                SetValueToInputText('optionInputForm',4);
                                SetValueToInputText('idDataInputForm',$PricesMainteinceVo2->id);
                               })(jQuery);
                            </script>
                        ";
        }
        echo $SetInputsDataFJQ;
    }

    public function GetDataInJsonForTbl()
    {
        $existData = false;
        $PricesMaintenceView = new PricesMainteinceVo();
        $PricesMaintenceView->isList = true;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $PricesMaintenceView);

        $string = "
        {\"data\": [";
        while ($PricesMaintenceView2 = $generalDao->GetVo($generalVo = clone $PricesMaintenceView)) {
            $existData = true;
            $json = array();
           // $json[] = $impostVo2->id;
            $json[] = htmlspecialchars(ucfirst(strtolower($PricesMaintenceView2->name)));
            $json[] = floatval(str_replace(",",".",$PricesMaintenceView2->ganance))."%";
            $json[] = $PricesMaintenceView2->time ." Minutos";
            $json[] = "$ ". str_replace(",",".",number_format($PricesMaintenceView2->price)) ." COP ";
            // floatval(str_replace(",",".",$PricesMaintenceView2->time))
            $json[] = "$ ". str_replace(",",".",number_format(round( (( (str_replace(",",".",$PricesMaintenceView2->price))  * ( floatval(str_replace(",",".",$PricesMaintenceView2->ganance)) / 100)   ) + (str_replace(",",".",$PricesMaintenceView2->price))) * floatval(str_replace(",",".",$PricesMaintenceView2->time) / 60) )))  . " COP ";

            $button = "";
        
                if(!in_array( "modificar_impuesto" , $this->dataList)){
                $button .= "<tip title='Modificar' class='yellow-tooltip' data-toggle='tooltip' data-placement='top'><button id='updateBtn' name='updateBtn' class='btn btn-warning btn-xs' data-toggle='modal' data-target='#impostPopUpAdd' title='Modificar' onclick='UpdateData({$PricesMaintenceView2->id},\"pricesmaintence\",\"PricesMaintence\");'><i class='fa fa-pencil'></i></button> </tip>";
                }
                if(!in_array( "eliminar_costo" , $this->dataList)){
                $button .= " <tip title='Eliminar' class='red-tooltip' data-toggle='tooltip' data-placement='top'><a id='deleteBtn' name='deleteBtn' class='btn btn-danger  btn-xs' title='Eliminar' onclick='DeleteData({$PricesMaintenceView2->id},\"priceMaintence\")'><i class='fa fa-times'></i></a></tip> ";
                }
                $json[] = $button;

            $string .= json_encode($json) . ",";

        }
        $string .= "-]}-";

        if($existData){
            echo  str_replace(",-]}-", "]}", $string);
        }else{
            echo "{ \"data\": [] }";
        }
    }

       public function GetLengthImpost(){
        $arrayList = array();
        $personVo = new impostVo();
        $personVo->isList = true;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $personVo);
        print_r($generalDao->GetLength());
    }

    
}

$ajaxDataImpost = new AjaxDataImpost();
