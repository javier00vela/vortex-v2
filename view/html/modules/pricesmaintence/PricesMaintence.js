// jshint unused:true
"use strict";


function LoadTable() {
    RemoveActionModule(38);
    GetArrayFromDataModule(38);
    setTimeout(function() {
        var parameters = {
            "isActionFromAjax": true,
            "optionInputForm": 0,
            "idDataInputForm": 0,
            "listData": arrayResponse,
        };


        $('#impostTbl').DataTable({
            responsive: true,
            "drawCallback": function(settings) {
                $('[data-toggle="tooltip"]').tooltip()
            },
            language: {
                "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
            },
            "ajax": {
                url: "view/html/modules/pricesmaintence/AjaxDataPricesMaintence.php",
                method: "POST",
                data: parameters
            }
        });
    }, 500);
}

/*********************************CLICK EVENTS**************************/
function ChangeFormatPrices() {
    $("#mainteninGananceCtx").keypress(function(event) {
        console.log(event.charCode);
        if (event.charCode > 58 || event.charCode < 43 || event.charCode == 45 || event.charCode == 46) {
            return false;
        }
    
    });

    $("#mainteninTimeCtx").keypress(function(event) {
        console.log(event.charCode);
        if (event.charCode > 58 || event.charCode < 43 || event.charCode == 45 || event.charCode == 46) {
            return false;
        }
    
    });

    $("#mainteninPriceCtx").keypress(function(event) {
        console.log(event.charCode);
        if (event.charCode > 58 || event.charCode < 43 || event.charCode == 45 || event.charCode == 46) {
            return false;
        }
    
    });
}

function ValidatePriceUnit() {
    $("#mainteninGananceCtx #mainteninTimeCtx #mainteninPriceCtx").change(function(event) {
        var cont = 0;
        var numberType = $(this).val();
        for (var i = 0; i < numberType.toString().length; i++) {
            if (numberType.toString()[i] == "," || numberType.toString()[i] == ".") {
                cont++;
            }
        }
        if (cont > 1) {
            numberType = "";
        }
        $(this).val(numberType);
    });
}

$(function() {
    ChangeFormatPrices();
    ValidatePriceUnit();
    LoadTable();

});