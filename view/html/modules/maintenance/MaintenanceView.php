<?php
require_once(Config::PATH . Config::CONTROLLER . 'general/AccessManager.php');
require_once(Config::PATH . Config::CONTROLLER . 'general/UtilJquery.php');
require_once(Config::PATH . Config::CONTROLLER . 'general/ManageSession.php');
require_once(Config::PATH . Config::VIEW_HTML . 'general/comps/GeneralCtx.php');
require_once(Config::PATH . Config::VIEW_HTML . 'general/comps/GeneralLst.php');
require_once(Config::PATH . Config::VIEW_HTML . 'general/comps/GeneralWithDataLst.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralDao.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralVo.php');

require_once(Config::PATH . Config::BACKEND . 'modules/RoleCompanyVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/CompanyVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/ProductVo.php');

class MaintenanceView
{
    private $optionInputForm;
    private $isUpdateUserForm;
    private $idDataInputForm;
    /*=============================================
                  Session Vars
    =============================================*/
    private $manageSession;
    /*=============================================
                  Template Vars
    =============================================*/
    private $utilJQ;
    private $pageTpl, $data, $core;
    /*=============================================
                Controls Vars HTML
    =============================================*/
    private $productNameCtx;
    private $productBrandCtx;
    private $productReferenceCtx;
    private $productPresentationCtx;
    private $productCountryCtx;
    private $productNameCompanyCtx;
    private $productPlaceLst;
    private $quotationCompanyLst;
    private $productNameProviderCtx;
    private $productSerialLst;
    private $quotationNumberInventoryLst;


    private $companyNameCtx;
    private $companyNameHiddenCtx;
    private $companyNitCtx;
    private $companyPhoneCtx;
    private $companyEmailCtx;
    private $companyWebSiteCtx;
    private $companyAddressCtx;
    private $companyCountryLst;
    private $companyCityLst;
    private $companyIdRoleLst;
    /*===========================


    /*=============================================
                Data Vars
    =============================================*/
    private $generalDao;


    function __construct($pageTpl, $data, $core)
    {
        $this->data = $data;
        $this->core = $core;
        $this->pageTpl = $pageTpl;

            $this->CreateComponents();
            $this->UseDataFromThePostback();
        
    }

    private function UseDataFromThePostback()
    {
        if (isset($_POST['optionInputForm'])) {
            $this->optionInputForm = $_POST['optionInputForm'];
            $this->idDataInputForm = $_POST['idDataInputForm'];
            $this->generalDao = new GeneralDao();
            switch ($this->optionInputForm) {
                case "1": {
                    $this->SetData();
                    break;
                }
                case "2": {
                    $this->DeleteData();
                    break;
                }
                case "4": {
                    $this->SetUpdate();
                    break;
                }
            }
        }

        $this->AssignDataToTpl();

    }

    private function CreateComponents()
    {
        $this->productNameCtx = new GeneralCtx("productNameCtx","Nombre",null,true);
        $this->productBrandCtx = new GeneralCtx("productBrandCtx","Marca",null,true);
        $this->productReferenceCtx = new GeneralCtx("productReferenceCtx","Referencia",null,true);
        $this->productPresentationCtx = new GeneralCtx("productPresentationCtx","Presentación",null,true);     
        $dataProductCountryLst = array();
        $this->quotationCompanyLst = new GeneralWithDataLst($this->GetCustomers(), "quotationCompanyLst", false,true, "-- Seleccione un Cliente --", true);
        $this->productCountryCtx = new GeneralWithDataLst($dataProductCountryLst, "productCountryCtx", false,false,"--País de Origen--",true);
        $this->productNameCompanyCtx = new GeneralCtx("productNameCompanyCtx","Nombre Empresa",null,true);
        $dataProductPlaceLst = array("Nacional","Internacional");
        $this->productNameProviderCtx = new GeneralCtx("productNameProviderCtx","Nombre Proveedor",null,true);
       
        $this->productSerialLst = new GeneralCtx("productSerialLst","Numero de serial del equipo",null,true);
    
        $this->quotationNumberInventoryLst = new GeneralCtx("quotationNumberInventoryLst","numero de Inventario del producto",null,true);

        $this->productPlaceLst = new GeneralWithDataLst($dataProductPlaceLst, "productPlaceLst", false,false,"--Lugar--",true);



        $this->companyNameCtx = new GeneralCtx("companyNameCtx","Nombre",null,true);
        $this->companyNameHiddenCtx = new GeneralCtx("companyNameHiddenCtx","Nombre","hidden",true);
       $this->companyNitCtx = new GeneralCtx("companyNitCtx","Nit",null,true);
       $this->companyPhoneCtx = new GeneralCtx("companyPhoneCtx","Telefono",null,true);
       $this->companyEmailCtx = new GeneralCtx("companyEmailCtx","Email",null,true);
       $this->companyWebSiteCtx = new GeneralCtx("companyWebSiteCtx","Sitio Web",null,true);
       $this->companyAddressCtx = new GeneralCtx("companyAddressCtx","Direccion",null,true);
       $dataCompanyCountryLst = array();
       $this->companyCountryLst = new GeneralWithDataLst($dataCompanyCountryLst, "companyCountryLst", false,false,"-- País --",true);
       $this->companyCountryLst->lst->adiPropiedad("style","width: 100%;");
       $dataCompanyCityCLst = array();
       $this->companyCityLst = new GeneralWithDataLst($dataCompanyCityCLst, "companyCityLst", false,false,"-- Ciudad --",true);
       $this->companyCityLst->lst->adiPropiedad("style","width: 100%;");
       
       $roleCompanyVo = new RoleCompanyVo();
       $roleCompanyVo->isList = true;
       $this->companyIdRoleLst = new GeneralLst($generalVo = clone $roleCompanyVo,"companyIdRoleLst", 0, 1, "--Compañia--",true);
        $this->utilJQ = new UtilJquery("Machine");
    }
    private function SetDataToVo($id , $idProduct)
    {
        $ProductVo = new ProductVo();
        $ProductVo->id = $id;
        $ProductVo->idProduct = $idProduct ;
        $ProductVo->typeProduct = "Equipos";
        $ProductVo->name = $_POST["productNameCtx"];
        $ProductVo->brand =  $_POST["productBrandCtx"];
        $ProductVo->reference =  $_POST["productReferenceCtx"];
        $ProductVo->presentation =  $_POST["productPresentationCtx"];
        $ProductVo->amount = 1 ;
        $ProductVo->minimumAmount = 0;
        $ProductVo->typeCurrency = 0;
        $ProductVo->priceUnit = 0 ;
        $ProductVo->country = $_POST["productPlaceLst"] ;
        $ProductVo->nameCompany = $_POST["quotationCompanyLst"] ;
        $ProductVo->description = $_POST["descripcion"] ;
        $ProductVo->percent = 0;
        $ProductVo->quantity = 1 ;
        $ProductVo->idQuotation = 0;
        $ProductVo->timeDelivery = "Sin establece";
        $ProductVo->namePlace = $_POST["productPlaceLst"] ;
        $ProductVo->IVA = 0;
        $ProductVo->nodo = ' ';
        $ProductVo->FLETE = 0;
        $ProductVo->isFromCompany = 0;
        $ProductVo->nameProvider = $_POST["productNameProviderCtx"];
        $ProductVo->serial = $_POST["productSerialLst"];
        $ProductVo->inventaryNumber = $_POST["quotationNumberInventoryLst"];
       
        return $ProductVo;
    }
    private function SetData()
    {
        $productVo = $this->SetDataToVo(null , 0 );
        $productVo->id = null;
        $result = $this->generalDao->Set($generalVo = clone $productVo);
        if (isset($result)) {
            $message = 'MessageSwalBasic("Registrado!","Equipo Externo Registrado Correctamente","success");';
            $productVo->id = $result;
            $productVo->idProduct = $result;
            $this->generalDao->Update($generalVo = clone $productVo);
            $this->utilJQ->AddFunctionJavaScript($message);
        }

    }
    private function SetUpdate()
    {
        $result = $this->generalDao->Update($generalVo = clone $this->SetDataToVo($this->idDataInputForm , $this->idDataInputForm ));
        if (isset($result)) {
            $message = 'MessageSwalBasic("Modificado!","Equipo Modificado Correctamente","success");';
            $this->utilJQ->AddFunctionJavaScript($message);
        }
    }
    private function DeleteData()
    {
        $message = "";
        $impostVo = new ImpostVo();
        $impostVo->id = $this->idDataInputForm;
        if ($this->generalDao->Delete($generalVo = clone $impostVo)) {
            $message = 'MessageSwalBasic("Eliminado!","Impuesto Eliminado Correctamente","success");';
        } else {
            $message = 'MessageSwalBasic("Error!","Problema con la base de datos","error");';
        }
        $this->utilJQ->AddFunctionJavaScript($message);
    }

    private function GetCustomers()
    {
        $companiesArray = array();
        $companyVo = new CompanyVo();
        $companyVo->isList = true;
        $generalDao = new GeneralDao();
        $generalDao->CustomQuery("SELECT * FROM  {$companyVo->nameTable} ORDER BY name ASC");
        while ($companyVo2 = $generalDao->GetVo($generalVo = clone $companyVo)) {
            if($companyVo2->idRoleCompany == 2 || $companyVo2->idRoleCompany == 4 || $companyVo2->idRoleCompany == 5){
                if($companyVo2->state == 1){
                        $companiesArray[] = $companyVo2->name . "_" . htmlspecialchars($companyVo2->name);
                }
            }
        
        }
        return $companiesArray;
    }

    private function AssignDataToTpl()
    {
        $this->data->assign('productNameCtx', $this->productNameCtx->paint());
        $this->data->assign('productBrandCtx', $this->productBrandCtx->paint());
        $this->data->assign('productNameCompanyCtx', $this->productNameCompanyCtx->paint());
        $this->data->assign('productPresentationCtx', $this->productPresentationCtx->paint());
        $this->data->assign('productReferenceCtx', $this->productReferenceCtx->paint());
        $this->data->assign('productCountryCtx', $this->productCountryCtx->paint());
        $this->data->assign('quotationCompanyLst', $this->quotationCompanyLst->paint());
        $this->data->assign('companyNameCtx', $this->companyNameCtx->paint());
        $this->data->assign('companyNameHiddenCtx', $this->companyNameHiddenCtx->paint());
        $this->data->assign('companyNitCtx', $this->companyNitCtx->paint());
        $this->data->assign('companyPhoneCtx', $this->companyPhoneCtx->paint());
        $this->data->assign('companyEmailCtx', $this->companyEmailCtx->paint());
        $this->data->assign('companyWebSiteCtx', $this->companyWebSiteCtx->paint());
        $this->data->assign('companyAddressCtx', $this->companyAddressCtx->paint());
        $this->data->assign('companyCountryLst', $this->companyCountryLst->paint());
        $this->data->assign('companyCityLst', $this->companyCityLst->paint());

        $this->data->assign('productNameProviderCtx', $this->productNameProviderCtx->paint());
        $this->data->assign('productSerialLst', $this->productSerialLst->paint());
        $this->data->assign('quotationNumberInventoryLst', $this->quotationNumberInventoryLst->paint());
        $this->data->assign('companyIdRoleLst', $this->companyIdRoleLst->paint());
        $this->data->assign('productPlaceLst', $this->productPlaceLst->paint());
        $this->data->assign('jquery', $this->utilJQ->PaintJQ());
         $this->data->assign('tempParameter', time());
        $this->SetDataToTpl();
    }   
    private function SetDataToTpl()
    {
        echo $this->core->get($this->pageTpl, $this->data);
    }
  


}   