<?php
require_once(__DIR__ . '/../../../../controller/general/Config.php');
require_once(Config::PATH . Config::CONTROLLER.Config::GENERAL . 'ManageArrays.php');
require_once(Config::PATH . Config::BACKEND . 'modules/ProductVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/QuotationVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/CompanyVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/HistoryModificationVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/UserQuotationVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/PersonVo.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralDao.php');
require_once Config::PATH . Config::BACKEND . 'modules/FactureVo.php';
require_once Config::PATH . Config::BACKEND . 'modules/OrderBuyVo.php';

class AjaxDataMaintenance{

    private $isActionFromAjax;
    private $optionInputForm;
    private $idDataInputForm;
    public $dataList = [];
    function __construct()
    {
        if (isset($_POST['isActionFromAjax'])) {
            $this->isActionFromAjax = $_POST['isActionFromAjax'];
            $this->optionInputForm = $_POST['optionInputForm'];
            $this->idDataInputForm = $_POST['idDataInputForm'];
            if(isset($_POST["listData"])){
                $this->dataList = $_POST["listData"];  
            }
            if ($this->isActionFromAjax == "true") {
                if ($this->optionInputForm == 3) {
                    $this->SetDataInControls();
                }else if($this->optionInputForm == 5){
                $this->GetLengthImpost();
                }else if($this->optionInputForm == 7){
                $this->GetContacts();
                }else {
                    $this->GetDataInJsonForTbl();
                }
            }
        }
    }

    private function SetDataInControls(){
        $SetInputsDataFJQ = "";
        $ProductVo = new ProductVo();
        $ProductVo->id = $this->idDataInputForm;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $ProductVo);
        if($ProductVo2 = $generalDao->GetVo($generalVo = clone $ProductVo)){
            $SetInputsDataFJQ = "
                            <script>
                               (function($){
                                SetValueToInputText('productNameCtx','{$ProductVo2->name}');
                                SetValueToInputText('productBrandCtx','{$ProductVo2->brand}');
                                SetValueToInputText('quotationCompanyLst','{$ProductVo2->nameCompany}');
                                SetValueToInputText('productReferenceCtx','{$ProductVo2->reference}');
                                SetValueToInputText('productCountryCtx','{$ProductVo2->country}');
                                SetValueToInputText('productPlaceLst','{$ProductVo2->namePlace}');
                                SetValueToInputText('productPresentationCtx','{$ProductVo2->presentation}');
                                SetValueToInputText('productNameProviderCtx','{$ProductVo2->nameProvider}');
                                SetValueToInputText('productSerialLst','{$ProductVo2->serial}');
                                SetValueToInputText('quotationNumberInventoryLst','{$ProductVo2->inventaryNumber}');
                                SetValueToInputText('descripcion','{$ProductVo2->description}');
                                
                                SetValueToInputText('optionInputForm',4);
                                SetValueToInputText('idDataInputForm',$ProductVo2->id);
                               })(jQuery);
                            </script>
                        ";
        }
        echo $SetInputsDataFJQ;
    }

    public function GetDataInJsonForTbl()
    {
        $existData = false;
        $ProductVo = new ProductVo();
        $ProductVo->isList = true;
        $generalDao = new GeneralDao();
        $generalDao->CustomQuery("SELECT product.* FROM product INNER JOIN quotation ON product.idQuotation = quotation.id WHERE quotation.state = 'Compra'  AND product.typeProduct='Equipos' OR  product.typeProduct='Equipos' OR product.isFromCompany=0   ");
        $string = "
        {\"data\": [";
        while ($ProductVo2 = $generalDao->GetVo($generalVo = clone $ProductVo)) {
            $existData = true;
            $json = array();
            $json[] = htmlspecialchars($ProductVo2->id);
            $json[] = htmlspecialchars($ProductVo2->name);
            $json[] = htmlspecialchars($ProductVo2->brand);
            $json[] = htmlspecialchars($ProductVo2->typeProduct);
            $json[] = htmlspecialchars($ProductVo2->namePlace);
            
            
            if($ProductVo2->isFromCompany == 1){
                $json[] = htmlspecialchars("#".$this->GetNumberFacture($ProductVo2->idQuotation));
                $json[] = htmlspecialchars($ProductVo2->nameCompany);
                $json[] = htmlspecialchars($ProductVo2->serial);
                $json[] = $this->GetCompany($this->GetQuotation($ProductVo2->idQuotation)->idCompany)->name;
                
                //$json[] = $this->GetDateBuy($ProductVo2->idQuotation);
                $json[] = "Es de la Empresa";
            }else{
                $json[] =  " -- Equipo Externo --";
                $json[] = htmlspecialchars($ProductVo2->nameProvider);
                $json[] = htmlspecialchars($ProductVo2->serial);
                $json[] = htmlspecialchars($ProductVo2->nameCompany);
                //$json[] =  " -- Equipo Externo --";
                $json[] = "No es de la Empresa";
            }
            $button = "";
            if(!in_array( "ver_contactos" , $this->dataList)){
                if($ProductVo2->isFromCompany == "1"){
                    $button .= "<tip title='Contactos' class='blue-tooltip' data-toggle='tooltip' data-placement='top'><button id='ContactsBtn' name='ContactsBtn' class='btn btn-primary btn-xs' data-toggle='modal' data-target='#ContactPopUpAdd' title='Modificar' onclick='ContactsData({$ProductVo2->idQuotation});'><i class='fa fa-users'></i></button> </tip>";
                }
            }
            if(!in_array( "editar_Equipos" , $this->dataList)){
                if($ProductVo2->isFromCompany == "0"){
                    $button .= " <tip title='Editar' class='yellow-tooltip' data-toggle='tooltip' data-placement='top'><a id='DiagnosticarBtn' name='DiagnosticarBtn' class='btn btn-warning  btn-xs' title='Editar'  data-toggle='modal' data-target='#machinePopUpAdd' title='Modificar' onclick='UpdateData({$ProductVo2->id},\"maintenance\",\"Maintenance\");'  ><i class='fa fa-edit'></i></a></tip>";
                }
            }

            if(!in_array( "generar_diagnostico" , $this->dataList)){

                $button .= " <tip title='Diagnosticar' class='yellow-tooltip' data-toggle='tooltip' data-placement='top'><a id='DiagnosticarBtn' name='DiagnosticarBtn' class='btn btn-warning  btn-xs' title='Diagnosticar' onclick='location.href=\"Diagnosis?idProduct={$ProductVo2->id}\"'><i class='fa fa-gear'></i></a></tip> ";
            }
            $json[] = $button;

            $string .= json_encode($json) . ",";

        }
        $string .= "-]}-";

        if($existData){
            echo  str_replace(",-]}-", "]}", $string);
        }else{
            echo "{ \"data\": [] }";
        }
    }

    public function GetNumberFacture($idFacture){
        $FactureVo = new FactureVo();
        $FactureVo->idQuotation= $idFacture;
        $FactureVo->idSearchField = 1;
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $FactureVo);
        if ($FactureVo2 = $generalDao->GetVo($generalVo = clone $FactureVo)) {
            return $FactureVo2->id;
        }
     }

    public function GetContacts(){
        $dataArray = [];
        $UserQuotationVo = new UserQuotationVo();
        $UserQuotationVo->idSearchField = 3;
        //print_r($_POST["idQuotation"]);
        $UserQuotationVo->idQuotation = $_POST["idQuotation"];
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $UserQuotationVo);
        while ($UserQuotationVo2 = $generalDao->GetVo($generalVo = clone $UserQuotationVo)) {
            $dataArray[] = $this->GetPerson($UserQuotationVo2->idPerson);
        }

        print_r(json_encode($dataArray));
    }

    public function GetPerson($id){
        $PersonVo = new PersonVo();
        $PersonVo->id = $id;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $PersonVo);
        if ($PersonVo2 = $generalDao->GetVo($generalVo = clone $PersonVo)) {
            return $PersonVo2;
        }
    }
    

    public function GetCompany($id){
        $CompanyVo = new CompanyVo();
        $CompanyVo->id = $id;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $CompanyVo);
        if ($CompanyVo2 = $generalDao->GetVo($generalVo = clone $CompanyVo)) {
            return $CompanyVo2;
        }
    }

       public function GetQuotation($id){
        $QuotationVo = new QuotationVo();
        $QuotationVo->id = $id;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $QuotationVo);
        if ($QuotationVo2 = $generalDao->GetVo($generalVo = clone $QuotationVo)) {
            return $QuotationVo2;
        }
    }

    public function GetDateBuy($id){
        $HistorymodificationVo = new HistoryModificationVo();
        $generalDao = new GeneralDao();
        $generalDao->CustomQuery("SELECT * FROM `historymodification` WHERE idQuotation={$id} ORDER BY id desc LIMIT 1");
        if ($HistorymodificationVo2 = $generalDao->GetVo($generalVo = clone $HistorymodificationVo)) {
       
            return $HistorymodificationVo2->modificationDate;
        }
      
      
       
    }

    
}

$ajaxDataImpost = new AjaxDataMaintenance();
