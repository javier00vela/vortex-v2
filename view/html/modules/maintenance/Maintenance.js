// jshint unused:true
"use strict";

function ContactsData(idQuote) {
    var info;

    var parameters = {
        "isActionFromAjax": true,
        "optionInputForm": 7,
        "idDataInputForm": 0,
        "idQuotation": idQuote
    };
    var elementHtml = "<div class='table-responsive' ><table class=\"table text-center table-striped \" style=\"width:100%\"><strong><thead class=\" text-center font-weight-bold \"><tr><td>Nombres</td><td>Apellidos</td><td>Correo</td><td>Telelfono</td><td>Celular</td></tr></thead></strong>";
    $.ajax({
        url: "view/html/modules/maintenance/AjaxDataMaintenance.php",
        type: 'post',
        data: parameters,
        success: function(data) {
            info = $.parseJSON(data);
            for (var i = 0; i < info.length; i++) {
                //console.log(info[i]);
                elementHtml += "<td>" + info[i].names + "</td>";
                elementHtml += "<td>" + info[i].lastNames + "</td>";
                elementHtml += "<td>" + info[i].email + "</td>";
                elementHtml += "<td>" + info[i].telephone + "</td>";
                elementHtml += "<td>" + info[i].cellPhone + "</td>";
            }
            elementHtml += "</table></div>";
            $("#ContactTab ").html(elementHtml);
        }
    })
}

function GetElementsCountries() {
    var $select = $('#productCountryCtx');
    $.getJSON('backend/DataJson/countries.json', function(data) {
        $.each(data.results, function(key, val) {
            $select.append("<option id='" + val.name + "'  value='" + val.name + "'>" + val.name + "</option>");
        })
    });
}

function SetSelect2Countries() {
    $("#productCountryLst").select2({
        placeholder: "--País de Origen--"
    });
}

function LoadTable() {
    RemoveActionModule(27);
    GetArrayFromDataModule(27);
    setTimeout(function() {
        var parameters = {
            "isActionFromAjax": true,
            "optionInputForm": 0,
            "idDataInputForm": 0,
            "listData": arrayResponse,
        };


        $('#impostTbl').DataTable({
            responsive: true,
            "drawCallback": function(settings) {
                $('[data-toggle="tooltip"]').tooltip()
            },
            language: {
                "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
            },
            "ajax": {
                url: "view/html/modules/maintenance/AjaxDataMaintenance.php",
                method: "POST",
                data: parameters
            }
        });
    }, 500);
}

function SetSelect2Countries() {
    $("#companyCountryLst").select2({
        placeholder: "--País de Origen--"
    });
}

function GetElementsCountries() {
    var $select = $('#companyCountryLst , productCountryCtx');
    $.getJSON('backend/DataJson/countries.json', function(data) {
        $.each(data.results, function(key, val) {
            $select.append("<option id='" + val.name + "'    value='" + val.name + "'>" + val.name + "</option>");
        })
    });
    var $selects = $('#productCountryCtx');
    $.getJSON('backend/DataJson/countries.json', function(data) {
        $.each(data.results, function(key, val) {
            $selects.append("<option id='" + val.name + "'    value='" + val.name + "'>" + val.name + "</option>");
        })
    });
}

function SetSelect2Cities() {
    $("#companyCityLst").select2({
        placeholder: "-- Ciudad --"
    });
}

function GetElementsCities() {
    var $select = $('#companyCityLst');
    $.getJSON('backend/DataJson/cities.json', function(data) {
        $.each(data.results, function(key, val) {
            $select.append("<option id='" + val.name + "'  value='" + val.name + "'>" + val.name + "</option>");
        })
    });
}

/*********************************CLICK EVENTS**************************/

$(function() {
    LoadTable();
    GetElementsCountries();
    SetSelect2Countries();
    SetSelect2Countries();
    GetElementsCountries();
    SetSelect2Cities();
    GetElementsCities();
    //$("#companyIdRoleLst").val(2);
    //$("#companyIdRoleLst").attr("disabled","true");
});