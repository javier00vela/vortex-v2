<!--=====================================
PÁGINA DE INICIO
======================================-->
<!-- content-wrapper -->
<div class="content-wrapper" style="height:800px;">
  <!-- content-header -->
  <section class="content-header">
    <h1>
    Tablero
    <small>Panel de Control</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="Home"><i class="fa fa-dashboard"></i> Inicio</a></li>
      <li class="active">Tablero</li>
    </ol>
  </section>
  <!-- content-header -->
  <!-- content -->
  <section class="content" >
    <!-- row -->
    <div class="row">
      <div class="col-lg-3 col-xs-6" node="main-2"  >
        <!-- small box -->
        <div class="small-box bg-aqua">
          <div class="inner inner-1">
            <h2>Actual <b class="userLenght">0</b></h2>
            <h4>Usuarios</h4>
          </div>
          <div class="icon">
            <i class="fa fa-user"></i>
          </div>
          <a href="Users" class="small-box-footer">Ir al Modulo <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <div class="col-lg-3 col-xs-6" node="main-3" >
        <!-- small box -->
        <div class="small-box bg-green">
          <div class="inner">
            <h2>Actual <b class="clientLength">0</b></h2>
            <h4>Empresas(Clientes)</h4>
          </div>
          <div class="icon">
            <i class="fa fa-industry"></i>
          </div>
          <a href="Company" class="small-box-footer">Ir al Modulo <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div>

      <!-- col -->
      <div class="col-lg-3 col-xs-6" node="main-4">
        <!-- small box -->
        <div class="small-box bg-blue">
          <div class="inner">
            <h2>Actual <b class="cotizacionesLength">0</b></h2>
            <h4>Cotizaciones</h4>
          </div>
          <div class="icon">
            <i class="fa fa-list-ul"></i>
          </div>
          <a href="GenerateQuotes" class="small-box-footer">Ir al Modulo <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div>

         <!-- col -->
      <div class="col-lg-3 col-xs-6" node="main-9" >
        <!-- small box -->
        <div class="small-box bg-purple">
          <div class="inner">
            <h2>Actual <b class="impostLength">0</b></h2>
            <h4>Impuestos</h4>
          </div>
          <div class="icon">
            <i class="fa fa-percent"></i>
          </div>
          <a href="Impost" class="small-box-footer">Ir al Modulo <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div>

         <!-- col -->
         <div class="col-lg-3 col-xs-6" node="main-20" >
        <!-- small box -->
        <div class="small-box bg-yellow">
          <div class="inner">
            <h2>Actual <b class="proveedoresLength">0</b></h2>
            <h4>Proveedores</h4>
          </div>
          <div class="icon">
            <i class="fa fa-building"></i>
          </div>
          <a href="Providers" class="small-box-footer">Ir al Modulo <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div>

         <!-- col -->
      <div class="col-lg-3 col-xs-6" node="main-17" >
        <!-- small box -->
        <div class="small-box bg-orange">
          <div class="inner">
            <h2>Actual <b class="ProspectosLength">0</b></h2>
            <h4>Prospectos</h4>
          </div>
          <div class="icon">
            <i class="fa fa-users"></i>
          </div>
          <a href="Prospect" class="small-box-footer">Ir al Modulo <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div>

      <!-- col -->
      <div class="col-lg-3 col-xs-6" node="main-1">
        <div class="small-box bg-red">
          <div class="inner">
            <h2>Actual <b class="inventoryLenght">0</b></h2>
            <p>Productos</p>
          </div>
          <div class="icon">
            <i class="fa fa-product-hunt"></i>
          </div>
          <a href="Inventory" class="small-box-footer">Ir al Modulo <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div>

      <div class="col-lg-3 col-xs-6" node="main-6" >
        <div class="small-box bg-fuchsia">
          <div class="inner">
            <h2>Modulo<b class="">&nbsp;</b></h2>
            <p>Reportes Usuarios</p>
          </div>
          <div class="icon">
            <i class="fa fa-user-circle"></i>
          </div>
          <a href="ReportHistorialPerson" class="small-box-footer">Ir al Modulo <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div>

      <div class="col-lg-3 col-xs-6" node="main-7" >
        <div class="small-box bg-olive">
          <div class="inner">
            <h2>Modulo<b class="">&nbsp;</b></h2>
            <p>Reportes Cotizaciones</p>
          </div>
          <div class="icon">
            <i class="fa fa-circle"></i>
          </div>
          <a href="ReportQuotation" class="small-box-footer">Ir al Modulo <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div>

      <div class="col-lg-3 col-xs-6"  node="main-21">
        <div class="small-box bg-blue">
          <div class="inner">
            <h2>Modulo<b class="">&nbsp;</b></h2>
            <p>Configuración</p>
          </div>
          <div class="icon">
            <i class="fa fa-cog"></i>
          </div>
          <a href="Managers" class="small-box-footer">Ir al Modulo <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div>

      <div class="col-lg-3 col-xs-6" node="main-22" >
        <div class="small-box bg-black">
          <div class="inner">
            <h2>Actual <b class="ClientsLength">0</b></h2>
            <p>Clientes</p>
          </div>
          <div class="icon">
            <i class="fa fa-users"></i>
          </div>
          <a href="Clients" class="small-box-footer">Ir al Modulo <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div>

       <div class="col-lg-3 col-xs-6" node="main-14" >
        <div class="small-box bg-navy">
          <div class="inner">
            <h2>Actual <b class="visitsLength">0</b></h2>
            <p>Visitas</p>
          </div>
          <div class="icon">
            <i class="fa fa-users"></i>
          </div>
          <a href="Visits" class="small-box-footer">Ir al Modulo <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div>
    </div>
    <!-- row -->
   
 </section>
  <!-- content -->
</div>
<!-- content-wrapper -->