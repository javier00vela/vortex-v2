function animationNumber(selector) {
    $(selector).each(function() {
        $(this).prop('Counter', 0).animate({
            Counter: $(this).text()
        }, {
            duration: 500,
            easing: 'swing',
            step: function(now) {
                $(this).text(Math.ceil(now));
            }
        });
    });
}

function LoadQuotatesLength() {
    var parameters = {
        "isActionFromAjax": true,
        "optionInputForm": 5,
        "idDataInputForm": 0,
    };

    $.ajax({
        url: "view/html/modules/generatequotes/AjaxDataGenerateQuotes.php",
        type: 'post',
        data: parameters,
        success: function(data) {
            $(".cotizacionesLength").html(data);
            animationNumber(".cotizacionesLength");
        }
    })
}

function LoadClientLength(rol) {
    var parameters = {
        "isActionFromAjax": true,
        "optionInputForm": 5,
        "idDataInputForm": 0,
        "typeRol": rol
    };

    $.ajax({
        url: "view/html/modules/company/AjaxDataCompany.php",
        type: 'post',
        data: parameters,
        success: function(data) {
            $(".clientLength").html(data);
            animationNumber(".clientLength");
        }
    })
}

function LoadProspectosLength(rol) {
    var parameters = {
        "isActionFromAjax": true,
        "optionInputForm": 6,
        "idDataInputForm": 0,
        "typeRol": rol
    };

    $.ajax({
        url: "view/html/modules/prospect/AjaxDataProspect.php",
        type: 'post',
        data: parameters,
        success: function(data) {
            $(".ProspectosLength").html(data);
            animationNumber(".ProspectosLength");
        }
    })
}

function LoadClientsLength(rol) {
    var parameters = {
        "isActionFromAjax": true,
        "optionInputForm": 6,
        "idDataInputForm": 0,
        "typeRol": rol
    };

    $.ajax({
        url: "view/html/modules/clients/AjaxDataClients.php",
        type: 'post',
        data: parameters,
        success: function(data) {
            $(".ClientsLength").html(data);
            animationNumber(".ClientsLength");
        }
    })
}

function LoadVisitsLength() {
    var parameters = {
        "isActionFromAjax": true,
        "optionInputForm": 6,
        "idDataInputForm": 0
    };

    $.ajax({
        url: "view/html/modules/visits/AjaxDataVisits.php",
        type: 'post',
        data: parameters,
        success: function(data) {
            $(".visitsLength").html(data);
            animationNumber(".visitsLength");
        }
    })
}

function LoadProveedorLength(rol) {
    var parameters = {
        "isActionFromAjax": true,
        "optionInputForm": 5,
        "idDataInputForm": 0,
        "typeRol": rol
    };

    $.ajax({
        url: "view/html/modules/company/AjaxDataCompany.php",
        type: 'post',
        data: parameters,
        success: function(data) {
            console.log(data);
            $(".proveedoresLength").html(data);
            animationNumber(".proveedoresLength");
        }
    })
}

function LoadImpostLength() {
    var parameters = {
        "isActionFromAjax": true,
        "optionInputForm": 5,
        "idDataInputForm": 0,
    };

    $.ajax({
        url: "view/html/modules/impost/AjaxDataImpost.php",
        type: 'post',
        data: parameters,
        success: function(data) {
            $(".impostLength").html(data);
            animationNumber(".impostLength");
        }
    })
}


function LoadUsersLength() {
    var parameters = {
        "isActionFromAjax": true,
        "optionInputForm": 7,
        "idDataInputForm": 0
    };


    $.ajax({
        url: "view/html/modules/users/AjaxDataUser.php",
        method: "POST",
        data: parameters,
        success: function(data) {
            $(".userLenght").html(data);
            animationNumber(".userLenght");
        }
    })
}


function LoadProductsLength() {
    var parameters = {
        "isActionFromAjax": true,
        "optionInputForm": 7,
        "idDataInputForm": 0
    };


    $.ajax({
        url: "view/html/modules/inventory/AjaxDataInventory.php",
        method: "POST",
        data: parameters,
        success: function(data) {
            $(".inventoryLenght").html(data);
            animationNumber(".inventoryLenght");
        }
    })
}




function LoadALLCompanyLength() {
    var parameters = {
        "isActionFromAjax": true,
        "optionInputForm": 7,
        "idDataInputForm": 0,
    };

    $.ajax({
        url: "view/html/modules/company/AjaxDataCompany.php",
        type: 'post',
        data: parameters,
        success: function(data) {
            $(".CompanysLength").html(data);
            animationNumber(".proveedorLength");
        }
    })
}

function CalendarAutoModify() {
    var parameters = {
        "isActionFromAjax": true,
        "optionInputForm": 7,
        "idDataInputForm": 0,
    };

    $.ajax({
        url: "view/html/modules/calendartrm/AjaxDataCalendarTRM.php",
        type: 'post',
        data: parameters,
        success: function(data) {
            // ToastShow("success" , "TRM ACTUALIZADO !!" ,'Se ha actualizado la TRM ', "2000");
        }
    })


}



function LoadAllowsMain() {

    var parameters = {
        "isActionFromAjax": true,
        "optionInputForm": 9,
        "idDataInputForm": 0
    };


    $.ajax({
        url: "view/html/modules/users/AjaxDataUser.php",
        method: "POST",
        data: parameters,
        success: function(response) {
            response = JSON.parse(response);
            for (var i = 0; i < response.length; i++) {
                $("[node=main-" + response[i] + "]").each(function() {
                    $(this).remove();
                });
            }

        }
    })
}




$(function() {
    LoadALLCompanyLength();
    LoadImpostLength();
    LoadUsersLength();
    LoadProductsLength();
    LoadQuotatesLength();
    LoadClientLength(2);
    LoadClientsLength(7);
    LoadProveedorLength(3);
    LoadProspectosLength(4);
    LoadVisitsLength();
    CalendarAutoModify();

    LoadAllowsMain();
    //LoadALLCompanyLength();


});