$(function () {
    var tbl;
    var idUpdate = 0;
    var arrayProductsNoChequed = [];
    var arrayProductsChequed = [];
    var idQuotation;
    var optionUpdate = 0;
    var functions = {
        init: function () {
            functions.LoadTable();

            functions.ChangeUpdate();
            functions.btnSaveDate()
            functions.btnSaveContact();

            functions.OnChequedInput();
            functions.Events();
            $('#updatedate').datepicker({
                format: 'yyyy-mm-dd 00:00'
            });
            functions.ChangeState();
        },

        Events: function () {
            $("#btnSaveProducts").on("click", functions.OnClickBtnSaveProducts);

        },



        ChangeUpdate: function () {
            $("#quotationTbl tbody").on("change", "#UpdateModal", function () {
                idUpdate = $(this).val().split("-")[1];
                optionUpdate = $(this).val().split("-")[0];
                switch ($(this).val().split("-")[0]) {
                    case "8":
                        $("#fechaUpdatePopUpProducts").modal("show");
                        break;
                    case "9":
                        ModifyQuotation(idUpdate);
                        break;
                    case "10":
                        $("#PersonsPopUpProducts").modal("show");
                        functions.GetUser();
                        functions.GetChangeUser();
                        break;


                }
            });
        },

        btnSaveDate: function () {
            $("#btnSaveDate").click(function () {
                if ($("#updatedate").val() != "") {
                    functions.UpdateAjaxQuotation($("#updatedate").val());
                } else {
                    MessageSwalBasic("Verifica!!", "Debes establecer una fecha", "warning");
                }
            });
        },

        btnSaveContact: function () {
            $("#btnSaveContact").click(function () {
                var array = []
                $("[name=checkbox]:checked").each(function () {
                    array.push({ id: $(this).attr("id"), "idPerson": $(this).attr("person"), "person": $(this).val() });
                });
                if (array.length > 0) {
                    functions.UpdateAjaxQuotation(array);
                } else {
                    MessageSwalBasic("Verifica!!", "Debes Agregar al menos un contacto", "warning");
                }


            })
        },

        UpdateAjaxQuotation: function (data) {
            var parameters = {
                "isActionFromAjax": true,
                "optionInputForm": optionUpdate,
                "idDataInputForm": 0,
                "id": idUpdate,
                "data": data
            }

            $.ajax({
                url: "view/html/modules/generatequotes/AjaxDataGenerateQuotes.php?v=23454345",
                type: 'post',
                data: parameters,
                success: function (data) {
                    PrintData(idUpdate);
                }

            });
        },

        GetChangeUser: function (data) {
            $("#quotationCompanyLst").change(function () {
                $("#quotationCompanyLst option:selected").each(function () {
                    //if (GetValueToInputText('isContactsContainerShow') == 0) {
                    if (GetValueSelect("quotationCompanyLst") != '') {

                        SetValueToInputText('isContactsContainerShow', 1);

                        $('#contactsContainer').show();
                        var parameters = {
                            "isActionFromAjax": true,
                            "optionInputForm": 5,
                            "idDataInputForm": GetValueSelect("quotationCompanyLst")
                        };
                        elementHtml = "";
                        $.ajax({
                            url: "view/html/modules/quotation/AjaxDataQuotation.php",
                            type: 'post',
                            data: parameters,
                            success: function (data) {
                                info = $.parseJSON(data);

                                if (info.length == null || info.length == 0) {
                                    $("#contactsContainer ").html(" <h4><b style='alignment:center'>No hay usuarios registrados en esta empresa.</b></h4>");
                                } else {
                                    for (var i = 0; i < info.length; i++) {
                                        //console.log(info[i]);
                                        elementHtml += "<div class='checkbox NoSelected' style='margin-left: 6px;'><label><input  type='checkbox' person='" + info[i]["id"] + "'  name='checkbox' value='" + info[i]["name"] + "'>" + info[i]["name"] + " <input type='hidden' name='idCompany' value='" + info[i]["idCompany"] + "'><input type='hidden' name='idUser' value='" + info[i]["id"] + "'></label></div>";
                                    }
                                    $("#contactsContainer ").html(elementHtml);

                                    elementHtml = "";
                                }
                            }
                        })
                    } else {
                        $("#contactsContainer ").html("");
                    }
                    //}
                })

            });
        },

        GetUser: function (data) {
            //if (GetValueToInputText('isContactsContainerShow') == 0) {

            $('#contactsContainer').show();
            var parameters = {
                "isActionFromAjax": true,
                "optionInputForm": 13,
                "idDataInputForm": idUpdate
            };
            elementHtml = "";
            $.ajax({
                url: "view/html/modules/generatequotes/AjaxDataGenerateQuotes.php",
                type: 'post',
                data: parameters,
                success: function (data) {
                    info = $.parseJSON(data);
                    for (var i = 0; i < info.length; i++) {
                        //console.log(info[i]);
                        elementHtml += "<div class='checkbox NoSelected' style='margin-left: 6px;'><label><input checked type='checkbox' person='" + info[i]["idPerson"] + "' quotation='" + info[i]["idQuotation"] + "' id='" + info[i]["id"] + "' name='checkbox' value='" + info[i]["completeName"] + "'>" + info[i]["completeName"] + " <input type='hidden' name='idCompany' value='" + info[i]["idCompany"] + "'><input type='hidden' name='idUser' value='" + info[i]["idPerson"] + "'></label></div>";
                    }
                    $("#contactsContainer ").html(elementHtml);
                    $("#quotationCompanyLst")[0].selectedIndex = info[0]["idCompany"];
                    elementHtml = "";
                }
            })
            //}
        },

        LoadTable: function () {
            RemoveActionModule(4);
            GetArrayFromDataModule(4);
            setTimeout(function () {
                var parameters = {
                    "isActionFromAjax": true,
                    "optionInputForm": 0,
                    "idDataInputForm": 0,
                    "listData": arrayResponse,
                };
                tbl = $('#quotationTbl').DataTable({
                    responsive: true,
                    "drawCallback": function (settings) {
                        $('[data-toggle="tooltip"]').tooltip()
                    },
                    language: {
                        "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
                    },
                    "ajax": {
                        url: "view/html/modules/generatequotes/AjaxDataGenerateQuotes.php",
                        method: "POST",
                        data: parameters
                    }
                });
            }, 500);
        },
        ChangeState: function () {

            $("#quotationTbl").on("change", "select", function () {

                var state = $(this).val();
                var data = tbl.row($(this).parents('tr')).data();
                var DataModification = [];
                // console.log(data);
                if (state == "Orden") {
                    $("#btnOpenModalProducts").trigger("click");
                    idQuotation = data[0];
                    functions.CallAjaxForProductsQuotation(idQuotation, data[7], data[8]);
                    return;
                }
                DataModification.push({ "idQuotation": data[0], "state": state, "attribute": "state", "Date": new Date().toLocaleString().replace("/", "-").replace("/", "-") });

                var parameters = {
                    "isActionFromAjax": true,
                    "optionInputForm": 4,

                    "idDataInputForm": 0,
                    "DataModification": DataModification
                };
                console.log(parameters);

                $.ajax({
                    url: "view/html/modules/generatequotes/AjaxDataGenerateQuotes.php",
                    type: 'post',
                    data: parameters,
                    success: function (data) {
                        $("#containerResponseAjax").html(data);
                        ToastShow("success", "ESTADO ACTUALIZADO !!", 'Se ha actualizado el estado ', "2000");
                        tbl.ajax.reload();
                    }
                })
            });
        },


        OnClickBtnSaveProducts: function () {
            checkedVacios = functions.ValidateCheckedVacios();
            //alert($("#notSendByReference").val() != 'undefined');
            if ($("#notSendByReference").val() != 'undefined') {
                console.log(checkedVacios);
                if (checkedVacios) {
                    functions.ValidateProductsNoChecked();
                    dataModification = [];
                    dataModification.push({ "idQuotation": idQuotation, "state": "Compra", "Date": new Date().toLocaleString().replace("/", "-").replace("/", "-") })
                    var parameters = { "isActionFromAjax": true, "optionInputForm": 7, "idDataInputForm": 0, "dataProductsQuotationNoChecked": (arrayProductsNoChequed) ? arrayProductsNoChequed : null,  "idQuotation": idQuotation,"arrayProductsChequed": (arrayProductsChequed) ? arrayProductsChequed : null , "DataModification": dataModification };
                    console.log(parameters);
                    $.ajax({
                        url: "view/html/modules/generatequotes/AjaxDataGenerateQuotes.php",
                        type: 'post',
                        data: parameters,
                        success: function (data) {
                            ToastShow("success", "ESTADO ACTUALIZADO !!", 'Se ha actualizado el estado ', "2000");
                            setTimeout(location.reload(), 2000);
                        }
                    })
                    
                } else {
                    MessageSwalBasic("UPPS !!", "Debes Seleccionar los productos cotizados ", "warning");
                }
            } else {
                MessageSwalBasic("UPPS !!", "Debes Agregar al menos un serial por equipo , desde el modulo de inventario > serial por producto y completar el numero de serial ", "warning");
            }
        },
        ValidateCheckedVacios: function () {
            var chulos = 0;
            $("#tblProductsQuotation tbody tr").each(function () {
                var columna = $(this).find("th").eq(12).html();

                if ($(columna).hasClass("yes")) {
                    chulos++;
                }
            });
            if (chulos == 0) {
                return false;
            }
            return true;
        },
        OnChequedInput: function () {
            $("#tblProductsQuotation tbody").on("change", "#cotizo", function () {
                if ($(this).is(":checked")) {
                    $(this).addClass("yes");
                } else {
                    $(this).removeClass("yes");
                }
            });
        },
        CallAjaxForProductsQuotation: function (idQuotation, totalQuotation, typeMoney) {
            var parameters = {
                "isActionFromAjax": true,
                "optionInputForm": 6,
                "idDataInputForm": idQuotation,

            };
            //console.log(parameters);

            $.ajax({
                url: "view/html/modules/generatequotes/AjaxDataGenerateQuotes.php",
                type: 'post',
                data: parameters,
                success: function (data) {
                    functions.BuildModalWithProducts(data, idQuotation, totalQuotation, typeMoney);
                }
            })
        },
        GetSerialProduct: function (serialArray, typeProduct, id) {
            var wordResponse = "";

            if (typeProduct == "Equipos") {
                //alert(typeProduct);
                if (serialArray.length > 0) {
                    wordResponse += "<select class='form-control' id='serialProduct-" + id + "' >";
                    for (var e = 0; e < serialArray.length; e++) {
                        wordResponse += "<option value='" + serialArray[e].numberSerial + "'>" + serialArray[e].numberSerial + "</option>";
                    }
                    wordResponse += "</select>";
                } else {
                    wordResponse += "<input type='hidden' id='notSendByReference' value='true' ><p style='color:red'>Debes Registrar el Serial en Inventario</p>"
                }
            } else {
                wordResponse += "No requiere Serial.";
            }
            return wordResponse;
        },
        BuildModalWithProducts: function (products, idQuotation, totalQuotation, typeMoney) {
            products = JSON.parse(products);
            var contenido = "";
            total = 0;
            for (var i = 0; i < products.length; i++) {
                serial = functions.GetSerialProduct(products[i]["serial"], products[i]["typeProduct"], products[i]["id"]);
                precioUnit = parseInt(products[i]["priceUnit"]);
                total = parseInt(parseInt(products[i]["priceUnit"]) * parseFloat(products[i]["percent"] / 100) + parseInt(products[i]["priceUnit"]));
                contenido += "<tr><th class='hidden'>" + products[i]["id"] + "</th><th>" + products[i]["name"] + "</th><th>" + products[i]["typeProduct"] + "</th><th>" + products[i]["nameCompany"] + "</th><th>" + products[i]["brand"] + "</th><th>" + products[i]["presentation"] + "</th><th>" + "% " + products[i]["percent"] + "</th><th>" + serial + "</th><th>" + products[i]["typeCurrency"] + "/" + typeMoney + "</th><th>" + "$ " + ConvertNumber(precioUnit) + "</th><th>" + products[i]["quantity"] + "</th><th>" + "$ " + ConvertNumber(total) + "</th><th><input type='checkbox' name='product_" + products[i]["id"] + "' id='cotizo' data='" + (products[i]["quantity"] * products[i]["priceUnit"]) + "' > Cotizado</th></tr>";
            }
            $("#tblProductsQuotation tbody").html(contenido);
            $("#spanIdQuotation").html(idQuotation);
            $("#spanTotalQuotation").html(totalQuotation + " " + typeMoney);
        },
        ValidateProductsNoChecked: function () {
            $("#tblProductsQuotation tbody tr").each(function () {
                var columna = $(this).find("th").eq(12).html();
                if (!$(columna).hasClass("yes")) {
                   
                    arrayProductsNoChequed.push({ "id-Product": $(this).find("th").eq(0).html() , "serial" :  $(this).find("th").eq(7).html().split(">")[2].split("<")[0] });
                }else{
                    arrayProductsChequed.push({ "id-Product": $(this).find("th").eq(0).html() , "serial" :  $(this).find("th").eq(7).html().split(">")[2].split("<")[0] });
                    
                }
            });
            console.log(arrayProductsNoChequed)

        },
    }



    functions.init();

});


function PrintData(id) {
    var parameters = {
        "isActionFromAjax": true,
        "optionInputForm": 3,
        "idDataInputForm": 0,
        "id": id
    }

    $.ajax({
        url: "GenerateQuotes",
        type: 'post',
        data: parameters,

        beforeSend: function () {

            setTimeout(MessageSwalWaiter('cargando...', 'Abriendo cotización!'), 10000);
        },
        success: function (data) {
            data = data.split("--split--")[1];
            var windowOpen = window.open("view/docs/pdf/" + data + ".pdf?v=" + Math.random(), '_blank');
            if (windowOpen) {
                windowOpen.focus();
                //  DeletePDFQuotation(data);
                MessageSwalRedirect("GENERADO !!", "Se ha abierto el documento de cotización ", "success", "GenerateQuotes");

            } else {
                MessageSwalBasic("VERIFICAR !!", "Debes habilitar permisos para abrir ventanas emergentes", "warning");
            }
        }

    });
}

function ModifyQuotation(id) {
    var parameters = {
        "isActionFromAjax": "true",
        "optionInputForm": 7,
        "idDataInputForm": 0,
        "id": id
    }
    $.ajax({
        url: "ModifyQuotation",
        type: 'post',
        data: parameters,
        beforeSend: function () {
            setTimeout(MessageSwalWaiter('cargando...', 'se esta cargando los datos de la cotización!'), 10000);
        },
        success: function (data) {
            $("body").html(data);
        }
    });
}

function DeletePDFQuotation(id) {
    var parameters = {
        "isActionFromAjax": "true",
        "optionInputForm": 5,
        "idDataInputForm": 0,
        "id": id
    }
    $.ajax({
        url: "GenerateQuotes",
        type: 'post',
        data: parameters,

        success: function (data) {
            // console.log(data);
        }
    });
}


function ModalCode(id) {
    swal({
        title: 'Ingrese el codigo de verificación',
        input: 'text',
        allowOutsideClick: false,
        inputAttributes: {
            autocapitalize: 'off'
        },
        showCancelButton: true,
        confirmButtonText: 'Verificar',
        showLoaderOnConfirm: true,
        preConfirm: (login) => {
            return fetch(`//api.github.com/users/${login}`)
                .then(response => {
                    if (!response.ok) {
                        throw new Error(response.statusText)
                    }
                    return response.json()
                })
                .catch(error => {
                    swal.showValidationMessage(
                        `Request failed: ${error}`
                    )
                })
        },
        preConfirm: (login) => {

        },
        allowOutsideClick: () => !swal.isLoading()
    }).then((result) => {
        VerifyCode(result, id);
    })
}

function VerifyCode(code, id) {
    var parameters = {
        "isActionFromAjax": true,
        "optionInputForm": 9,
        "idDataInputForm": 0,
        "code": code.value,
        "id": id
    };

    $.ajax({
        url: "view/html/modules/quotation/AjaxDataQuotation.php",
        type: 'post',
        data: parameters,
        success: function (data) {
            if (data == 0) {
                MessageSwalBasic("Codigo Incorrecto!!", "verifique el codigo", "error");
            }

            if (data == 1) {
                MessageSwalBasic("Codigo Correcto!!", "se ha verificado el codigo", "success");
                setTimeout(function () {
                    AbleQuote(id);
                    PrintData(id);
                    location.reload();
                }, 1000);
            }

        }
    });

}


function AbleQuote(id) {
    var parameters = {
        "isActionFromAjax": true,
        "optionInputForm": 9,
        "idDataInputForm": 0,
        "id": id,
        "onlyAction": true
    };

    $.ajax({
        url: "view/html/modules/configdata/AjaxDataConfigData.php",
        type: 'post',
        data: parameters,
        success: function (data) { }
    });
}