function LoadTable() {
    RemoveActionModule(13);
    GetArrayFromDataModule(13);
    setTimeout(function() {
        var parameters = {
            "isActionFromAjax": true,
            "optionInputForm": 1,
            "idDataInputForm": 0,
            "idProduct": GetValueToInputText("idProductInputForm"),
            "listData": arrayResponse,
        };


        tbl = $('#productTbl').DataTable({
            responsive: true,
            "drawCallback": function(settings) {
                $('[data-toggle="tooltip"]').tooltip()
            },
            language: {
                "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
            },
            "ajax": {
                url: "view/html/modules/historyproducts/AjaxDataHistoryProducts.php",
                method: "POST",
                data: parameters
            }
        });
        LoadNumTimesForProduct();
    }, 500);
}

function LoadNumTimesForProduct() {


    var parameters = {

        "isActionFromAjax": true,
        "optionInputForm": 2,
        "idDataInputForm": 0,
        "idProduct": GetValueToInputText("idProductInputForm")
    };
    $.ajax({
        data: parameters,
        url: 'view/html/modules/historyproducts/AjaxDataHistoryProducts.php',
        type: 'POST',
        success: function(response) {
            response = JSON.parse(response);
            console.log(response);
            AssignTittle(response);
        }
    });
}


function AssignTittle(size) {


    if (size > 1) {
        $(".box-header").append("<h3>El producto se ha cotizado " + size + " veces</h3>");
    } else if (size == null || size == 0) {
        $(".box-header").append("<h3>El producto no se ha cotizado</h3>");
    } else {
        $(".box-header").append("<h3>El producto se ha cotizado " + size + " vez</h3>");
    }

}


function PrintData(id) {
    var parameters = {
        "isActionFromAjax": true,
        "optionInputForm": 3,
        "idDataInputForm": 0,
        "id": id
    }

    $.ajax ({
        url: "GenerateQuotes",
        type: 'post',
        data: parameters,
        beforeSend: function() {
            setTimeout(MessageSwalWaiter('cargando...', 'Abriendo cotización!'), 10000);
        },
        success: function(data) {
            console.log(data);
            data = data.split("--split--")[1];
            var windowOpen = window.open("view/docs/pdf/" + data + ".pdf?v=" + Math.random(), '_blank');
            if (windowOpen) {
                windowOpen.focus();
                DeletePDFQuotation(data);
                MessageSwalBasic("GENERADO !!", "Se ha abierto el documento de cotización ", "success");

            } else {
                MessageSwalBasic("VERIFICAR !!", "Debes habilitar permisos para abrir ventanas emergentes", "warning");
            }


            //   $("body").html(data);


        }

    });
}

function DeletePDFQuotation(id) {
    var parameters = {
        "isActionFromAjax": "true",
        "optionInputForm": 5,
        "idDataInputForm": 0,
        "id": id
    }
    $.ajax ({
        url: "GenerateQuotes",
        type: 'post',
        data: parameters,

        success: function(data) {
            console.log(data);
        }
    });
}

$(function() {
    LoadTable();
});