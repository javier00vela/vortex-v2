<?php
require_once(Config::PATH . Config::CONTROLLER . 'general/AccessManager.php');
require_once(Config::PATH . Config::CONTROLLER . 'general/UtilJquery.php');
require_once(Config::PATH . Config::CONTROLLER . 'general/ManageSession.php');
require_once(Config::PATH . Config::VIEW_HTML . 'general/comps/GeneralCtx.php');
require_once(Config::PATH . Config::VIEW_HTML . 'general/comps/GeneralLst.php');
require_once(Config::PATH . Config::VIEW_HTML . 'general/comps/GeneralWithDataLst.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralDao.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/QuotationVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/PersonVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/ProductVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/TracingQuotationMainteinceVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/CompanyVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/RoleVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/QuotationVo.php');

class TracingQuotationMainteinceView
{
    private $optionInputForm;
    private $isUpdateUserForm;
    private $codeQuotationInputForm;
    private $codeQuotationFromGet;
    private $prediction;
    /*=============================================
                  Session Vars
    =============================================*/
    private $manageSession;
    /*=============================================
                  Template Vars
    =============================================*/
    private $utilJQ;
    private $pageTpl, $data, $core;
    /*=============================================
                Controls Vars HTML
    =============================================*/
    private $subjectCtx;
    private $personResponsableLst;
    private $dateCtx;

    /*=============================================
                Data Vars
    =============================================*/
    private $generalDao;


    function __construct($pageTpl, $data, $core)
    {

        $this->data = $data;
        $this->core = $core;
        $this->pageTpl = $pageTpl;

            $this->CreateComponents();
            $this->UseDataFromThePostback();
    
        $this->UseDataFromGet();
        $this->AssignDataToTpl();
    }

     private function UseDataFromThePostback()
    {

        if (isset($_POST['optionInputForm'])) {
            $this->optionInputForm = $_POST['optionInputForm'];
            $this->idDataInputForm = $_POST['idDataInputForm'];
            $this->generalDao = new GeneralDao();
            switch ($this->optionInputForm) {
                case "1": {
                    $this->SetData();
                    break;
                }
                case "2": {
                    $this->SetUpdate();
                    break;
                }

            }
           
        }
      

    }

     private function  UseDataFromGet(){
        $assign = '';
        if(isset($_GET["codeQuotationInputForm"])){
            
            $this->codeQuotationFromGet = $_GET["codeQuotationInputForm"];
                $assign = 'SetValueToInputText("codeQuotationInputForm",'.$this->codeQuotationFromGet.');';
          
        }
       
        $this->utilJQ->AddFunctionJavaScript($assign);
          
    }


    private function CreateComponents()
    {
        $this->subjectCtx = new GeneralCtx("subjectCtx", "asunto", null, true);
        $this->dateCtx  = new GeneralCtx("dateCtx","fecha",null,true);
        $predictionList = array("AA | El Cliente tiene dinero y quiere comprarle a vortex | 95%","AB | El CLiente tiene el dinero , y esta inseguro de cual vendedor escoger   | 75% ","AC | El Cliente tiene dinero, pero quiere comprarle a nuestra competencia | 15%","BA | El Cliente no esta seguro del dinero , pero quiere comprarle a Vortex | 75%","BB | El Cliente no esta seguro del dinero , y no esta seguro de cual vendedor escoger | 50%","BC | El Cliente no esta seguro del dinero , pero quiere comprarle a la competencia | 10% ","CA | No tienen dinero , pero quiere comprarle a Vortex | 15%","CB | No tienen dinero , y no estan seguros de cual vendedor escoger | 10%","CC | No tienen dinero , pero quiere comprarle a la competencia | 5%");
        $this->prediction = new GeneralWithDataLst($predictionList, "predictionList", false,false,"--predicción Actual--",true);
        $this->personResponsableLst = new GeneralWithDataLst($this->GetUser(), "tracingUserLst", false, true, "-- Seleccione --", true);
        $this->codeQuotationInputForm = new GeneralCtx("codeQuotationInputForm","","hidden",false);
        $this->utilJQ = new UtilJquery("tracingQuotation");
       
    }

    private function AssignDataToTpl()
    {     
        $this->data->assign('subjectCtx', $this->subjectCtx->paint());
        $this->data->assign('dateCtx', $this->dateCtx->paint());
        $this->data->assign('personResponsableLst', $this->personResponsableLst->paint());
         $this->data->assign('tempParameter', time());
        $this->data->assign('codeQuotationInputForm', $this->codeQuotationInputForm->paint());
        $this->data->assign('prediction', $this->prediction->paint());
        $this->data->assign('idQuotationInputForm', $this->codeQuotationFromGet);
        $this->data->assign('jquery', $this->utilJQ->PaintJQ());    
        $this->SetDataToTpl();
    }   

    private function SetDataToTpl()
    {

        echo $this->core->get($this->pageTpl, $this->data);
        
    }



    private function GetUser()
    {   
        $personsArray = array();
        $personVo = new PersonVo();
        $personVo->idSearchField = 2;
        $personVo->isList = true;
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $personVo);
        while ($personVo2 = $generalDao->GetVo($generalVo = clone $personVo)) {
        
            if ($personVo2->idRole==1||$personVo2->idRole==2||$personVo2->idRole==3||$personVo2->idRole==4||$personVo2->idRole==5 || $personVo2->idRole==6 || $personVo2->idRole==10 || $personVo2->idRole==11 || $personVo2->idRole==12){
                if($personVo2->state ==1){
                        $personsArray[] = $personVo2->id. "_" . htmlspecialchars($personVo2->names ." ".$personVo2->lastNames);
                    }
                    
            }
        
    }
        return $personsArray;
    }


     private function SetDataToVo()
    {
        $tracingQuotationVo = new TracingQuotationMainteinceVo();
        $tracingQuotationVo->id = $this->idDataInputForm;
        $tracingQuotationVo->idQuotation = $_GET["codeQuotationInputForm"];
        $tracingQuotationVo->idResponsable = $_POST["tracingUserLst"];
        $tracingQuotationVo->subject = $_POST["subjectCtx"];
        $tracingQuotationVo->prediction = $_POST["predictionList"];
        $tracingQuotationVo->description = $_POST["descriptionAtx"];
        $tracingQuotationVo->dateRegistry =   date('Y-m-d H:i:m', strtotime($_POST["dateCtx"]));
    

        return $tracingQuotationVo;
    }

    private function SetData()
    {
        $tracingQuotationVo = $this->SetDataToVo();
         $tracingQuotationVo->id = null;
        $result = $this->generalDao->Set($generalVo = clone $tracingQuotationVo);
        if (isset($result)) {
            $message = 'MessageSwalBasic("Registrado!","Seguimiento Registrado Correctamente","success");';
            $this->utilJQ->AddFunctionJavaScript($message);
        }

    }

/*********************************************************************************
*********************************UPDATE*********************************************/
    private function SetUpdate()
    {
        $result = $this->generalDao->Update($generalVo = clone $this->SetDataToVo());
        if (isset($result)) {
            $message = 'MessageSwalBasic("Modificado!","Seguimiento Modificado Correctamente","success");';
            $this->utilJQ->AddFunctionJavaScript($message);
        }
    }

}   