$(function() {
    var gestionTracingQuotationVista = {

        init: function() {
            vista.LoadTable();
            vista.Controls();
            vista.ControlsDatePicker();

        },
        Controls: function() {
            $("#formBtn").click(vista.SubmitForm);
        },
        ControlsDatePicker: function() {
            $('#dateCtx').datepicker({
                autoclose: true
            });
        },
        SubmitForm: function() {

            SetValueToInputText("idQuotation", GetValueToInputText("codeQuotationInputForm"));
            $("#tracingForm").submit();
        },
        LoadTable: function() {
            RemoveActionModule(43);
            GetArrayFromDataModule(43);
            setTimeout(function() {
                var parameters = {
                    "isActionFromAjax": true,
                    "optionInputForm": 1,
                    "idDataInputForm": 0,
                    "codeQuotation": $("#codeQuotationInputForm").attr("value"),
                    "listData": arrayResponse,
                };


                $('#quotationTbl').DataTable({
                    responsive: true,
                    "drawCallback": function(settings) {
                        $('[data-toggle="tooltip"]').tooltip()
                    },
                    language: {
                        "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
                    },
                    "ajax": {
                        url: "view/html/modules/tracingquotationmainteince/AjaxDataTracingQuotationMainteince.php",
                        method: "post",
                        data: parameters
                    }
                });
            }, 500);
        }


    };

    vista = gestionTracingQuotationVista;
    vista.init();

})