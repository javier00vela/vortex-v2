<?php
require_once(Config::PATH . Config::CONTROLLER . 'general/AccessManager.php');
require_once(Config::PATH . Config::CONTROLLER . 'general/UtilJquery.php');
require_once(Config::PATH . Config::VIEW_HTML . 'general/comps/GeneralCtx.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralDao.php');
require_once(Config::PATH . Config::VIEW_HTML . 'general/comps/GeneralLst.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/UserVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/PersonVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/AccessHistoryVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/RoleVo.php');
require_once(Config::PATH . Config::CONTROLLER . 'general/ExcelManager.php');

class ReportHistorialPersonView
{
    private $idUserCtx;
    /*=============================================
                  Template Vars
    =============================================*/
    private $utilJQ;
    private $pageTpl, $data, $core;
    private $personIdRoleLst;
    private $optionInputForm;
    private $numDataRows;
    /*=============================================
                Data Vars
    =============================================*/
    private $generalDao;
       private $nameColumns =  array("id","fechaIngreso","fechaSalida","horas","direccionIp","dispositivo","navegador","idUsuario","nombre");


    function __construct($pageTpl, $data, $core)
    {
        $this->data = $data;
        $this->core = $core;
        $this->pageTpl = $pageTpl;
        $this->CreateComponents();
        $this->AssignDataToTpl();
        $this->CreateReportExcel();
        $this->UseDataFromThePostback();
    }


    private function UseDataFromThePostback()
    {
        if (isset($_POST['optionInputForm'])) {
            $this->optionInputForm = $_POST['optionInputForm'];
            $this->generalDao = new GeneralDao();
            switch ($this->optionInputForm) {

                case "1": {
                    $this->CallUserByRol($_POST["idRol"]);
                    break;
                }
                 case "2": {
                    $this->CallApiIp($_POST["ip"]);
                    break;
                }
            }
        }
      

    }

    private function CreateComponents(){
        $this->idUserCtx = new GeneralCtx("idUserCtx","user","hidden",false);
        $this->utilJQ = new UtilJquery("AccessHistory");
        $roleVo = new RoleVo();
        $roleVo->isList = true;
        $this->personIdRoleLst = new GeneralLst($generalVo = clone $roleVo,"personIdRoleLst", 0, 1,"--Seleccione el Rol--", true);
    }

    private function AssignDataToTpl(){
        $assign = '';
        if(isset($_GET["idUser"])){
            $assign = 'SetValueToInputText("idUserCtx",'.$_GET["idUser"].');';
        }
    
         $this->data->assign('listData', $this->personIdRoleLst->paint());
        $this->data->assign('idUserCtx', $this->idUserCtx->paint());
         $this->data->assign('tempParameter', time());
        
        $this->utilJQ->AddFunctionJavaScript($assign);
        $this->data->assign('jquery', $this->utilJQ->PaintJQ());
        $this->SetDataToTpl();
    }

    private function CallUserByRol($rol){
    $arraylist = null;
    $usrVo = new UserVo();
    $userVo = new PersonVo();
        $userVo->idSearchField = 12;
        $userVo->idRole = $rol;
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $userVo);
        $arraylist .= "*-*";
         while($userVo2 = $generalDao->GetVo($generalVo = clone $userVo)) {
            if($userVo2->state == 1){
                 $arraylist .= $userVo2->id."#".htmlspecialchars($userVo2->names." ".$userVo2->lastNames)."+";
               }
          }
          
         $arraylist .= "*-*";
        print_r($arraylist);
    }


       private function CallApiIp($ip){

       $json = file_get_contents("http://ip-api.com/json/".$ip);
       print_r("//////////////".$json."//////////////");
    }


    private function GetNameUser($idUser){
        $userVo = new UserVo();
        $personVo = new PersonVo();

        $userVo->id = $idUser;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $userVo);
        if($userVo2 = $generalDao->GetVo($generalVo = clone $userVo)){
            $personVo->id = $userVo2->idPerson;
            $generalDao->GetById($generalVo = clone $personVo);
            if($personVo2 = $generalDao->GetVo($generalVo = clone $personVo)){
              if($personVo2->state ==1){
                return  htmlspecialchars($personVo2->names." ".$personVo2->lastNames);
              }
            }    
        }
    }

/*=============================================
                  Excel Insert Functions 
    =============================================*/

public function LoadExcelFile(){
    if(isset($this->fileExcel)){
        $coreExcel = $this->InsertExcelFile($this->fileExcel);
        $this->numRows = $this->CountCellsExcel($coreExcel);
        $this->SetDataExcelToVo($coreExcel);
    }
}

public function setDataExcel(){

  if(isset($_FILES["selectTemplate"])){
      $fileExplode = explode(".", $_FILES["selectTemplate"]["name"]);
    if($fileExplode[1] == "xlsx" ){
        $this->fileExcel = $_FILES["selectTemplate"]["tmp_name"];
    }else{
        $message = '<script> MessageSwalBasic("ERROR!","Verifique que el formato que ha ingresado sea tipo excel","error"); </script>';
        echo $message;
    }
  }
}

public function InsertExcelFile($file){
$fileXlsx = PHPEXCEL_IOFactory::load($file);
return $fileXlsx;
}

public function CountCellsExcel($file){
$file->setActiveSheetIndex(0);
$numRows = $file->setActiveSheetIndex(0)->getHighestRow();
return $numRows;
}



private function SetDataExcelToVo($fileXlsx)
    {
/*
      $productVo = new ProductVo();
      $productVo->isList = true;
      $this->generalDao->GetById($generalVo = clone $productVo);
      while ($productVo2 = $this->generalDao->GetVo($generalVo = clone $productVo)) {
           $this->names[]=$productVo2->name;
      }
      if(empty($this->names)){
          $this->names= array() ;
      }*/

      for ($i=2; $i<$this->numRows+1; $i++) {
            $productVo = new ProductVo();
            $productVo->name = $fileXlsx->getActiveSheet()->getCell('B'.$i)->getCalculatedValue();
            $productVo->brand = $fileXlsx->getActiveSheet()->getCell('C'.$i)->getCalculatedValue();
            $productVo->reference = $fileXlsx->getActiveSheet()->getCell('D'.$i)->getCalculatedValue();
            $productVo->unitMeasurement = $fileXlsx->getActiveSheet()->getCell('E'.$i)->getCalculatedValue();
            $productVo->amount = $fileXlsx->getActiveSheet()->getCell('F'.$i)->getCalculatedValue();
            $productVo->minimumAmount = $fileXlsx->getActiveSheet()->getCell('G'.$i)->getCalculatedValue();
            $productVo->typeCurrency = $fileXlsx->getActiveSheet()->getCell('H'.$i)->getCalculatedValue();
            $productVo->priceUnit = $fileXlsx->getActiveSheet()->getCell('I'.$i)->getCalculatedValue();
            $productVo->country = $fileXlsx->getActiveSheet()->getCell('J'.$i)->getCalculatedValue();
            $productVo->idCompany = $fileXlsx->getActiveSheet()->getCell('K'.$i)->getCalculatedValue();
            $productVo->description = $fileXlsx->getActiveSheet()->getCell('L'.$i)->getCalculatedValue();


          $this->InsertDataExcel($productVo);
            /*if (count($this->names) > 0  ) {
                $save = $this->names[$i];
              }else{
                $save = "";
            }

            if ($productVo->name != $save) {

              $this->productVo = $productVo;
              $this->InsertDataExcel();
            }*/
      }
  }

public function InsertDataExcel($productVo){
  $result = $this->generalDao->Set($generalVo = clone $productVo);
  if($result){
     $message = '<script> MessageSwalBasic("Insertado!","Se ha agregado la plantilla Excel Correctamente","success"); </script>';
      echo $message;
  }
}




private function CreateReportExcel(){
    if(isset($_POST["data"])){
     ExcelManager::CreateReportExcel($_POST["data"] , $this->nameColumns , "historialAccess" );
  }
}


    private function SetDataToTpl(){
        echo $this->core->get($this->pageTpl, $this->data);
    }

}