<?php

require_once('/var/www/html/vortex/controller/general/Config.php');
require_once(Config::PATH . Config::BACKEND . 'modules/QuotationVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/PersonVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/CompanyVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/ClientUserVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/ModuleEventVo.php');
require_once(Config::PATH . Config::CONTROLLER . 'general/MailsUtils.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralDao.php');

 function GetProspectsByPerson(){
    $arrayList = array();
    $CLientUserVo = new ClientUserVo();
    $CLientUserVo->isList = true;
    $generalDao = new GeneralDao();
    $generalDao->GetById($generalVo = clone $CLientUserVo);
    while($CLientUserVo2 = $generalDao->GetVo($generalVo = clone $CLientUserVo)) {
        $arrayList[] = ["id" => $CLientUserVo2->id , "date" => GetPerson($CLientUserVo2->idClient)->creationDate , "Person" => GetPerson($CLientUserVo2->idUser) , "client" => GetPerson($CLientUserVo2->idClient) , "GetEventModules" => GetEventModules(GetPerson($CLientUserVo2->idUser) , 1 , 17) ]; 
    }
    return  $arrayList;
}

function GetEventModules($personVo , $event , $module){
    $ModuleEventVo = new  ModuleEventVo();
    $ModuleEventVo->idRol = $personVo->idRole;
    $ModuleEventVo->idSearchField = 4;
    $generalDao = new GeneralDao();
    $generalDao->GetByField($generalVo = clone $ModuleEventVo);
    while($ModuleEventVo2 = $generalDao->GetVo($generalVo = clone $ModuleEventVo)) {
        if($ModuleEventVo2->idEvent == $event){
          if($ModuleEventVo2->idModule == $module){
            return $ModuleEventVo2;
          }
        }
    }
    return false;
}

function SendMail($arrays){
    for ($i=0; $i < count($arrays) ; $i++) { 
     if(isset($arrays[$i]["GetEventModules"])){
      if($arrays[$i]["GetEventModules"]->state == 1){
       if( $arrays[$i]["Person"]->state == 1 &&   $arrays[$i]["client"]->state == 1 &&  $arrays[$i]["client"]->idRole == 9){ 
            $MailsUtils = new MailsUtils();
            $word = [ "init" => "" , "finish" => ""];
            $MailsUtils->AddDestinatario($arrays[$i]["Person"]->email);
            $array = $MailsUtils->GetArrayModel();
            $array["Event"] = 1;
            $array["IdRol"] = $arrays[$i]["Person"]->idRole;
            $array["WordReplace"] = ["#prospecto" , "#empresa"];
            $array["ToReplace"] = [ $arrays[$i]["client"]->names ." ". $arrays[$i]["client"]->lastNames, GetCompany($arrays[$i]["client"]->idCompany)->name];
            $array["Alias"] = "vortex - recordatorios";
            $nuevafecha = strtotime ( ValidateNotificationsTime($arrays[$i]["GetEventModules"]->typeNotification ,$arrays[$i]["GetEventModules"]->numberNotification ) , strtotime ( $arrays[$i]["date"] ) ) ;
            $nuevafecha = date ( 'Y-m-d' , $nuevafecha );
            $word["init"] = $nuevafecha;
            $word["finish"] = date("Y-m-d");
            //print_r(ValidateNotificationsTime($arrays[$i]["GetEventModules"]->typeNotification ,$arrays[$i]["GetEventModules"]->numberNotification ));
            $MailsUtils->SendWithDate($array , $word );
       }
     }
    }else{
        echo "falso";
    }
  }
}

function ValidateNotificationsTime($notification, $days){
    if($notification == "mismo_dia"){
        return "+0 day";
    }else if($notification == "post_dia"){
        return "+{$days} day";
    }else{
        return "-{$days} day";
    }
}

 function GetPerson($id){
    $PersonVo = new PersonVo();
    $PersonVo->id = $id;
    $generalDao = new GeneralDao();
    $generalDao->GetById($generalVo = clone $PersonVo);
    if($PersonVo2 = $generalDao->GetVo($generalVo = clone $PersonVo)) {
        return $PersonVo2;
    }
}


 function GetCompany($id){
    $CompanyVo = new CompanyVo();
    $CompanyVo->id = $id;
    $generalDao = new GeneralDao();
    $generalDao->GetById($generalVo = clone $CompanyVo);
    if($CompanyVo2 = $generalDao->GetVo($generalVo = clone $CompanyVo)) {
        return $CompanyVo2;
    }
}


$array = GetProspectsByPerson();
SendMail($array);


?>