<?php

require_once('/var/www/html/vortex/controller/general/Config.php');
require_once(Config::PATH . Config::BACKEND . 'modules/VisitsVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/VisitsDescriptionVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/PersonVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/ModuleEventVo.php');
require_once(Config::PATH . Config::CONTROLLER . 'general/MailsUtils.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralDao.php');


 function GetVisitByPerson(){
    $arrayList = array();
    $VisitDescriptionVo = new VisitDescriptionVo();
    $VisitDescriptionVo->isList = true;
    $generalDao = new GeneralDao();
    $generalDao->GetById($generalVo = clone $VisitDescriptionVo);
    while($VisitDescriptionVo2 = $generalDao->GetVo($generalVo = clone $VisitDescriptionVo)) {
        $arrayList[] = ["id" => $VisitDescriptionVo2->id , "date" => $VisitDescriptionVo2->dateCita , "Person" => GetPerson(GetVisit($VisitDescriptionVo2->idVisits)->idPerson) , "Client" => GetPerson(GetVisit($VisitDescriptionVo2->idVisits)->idClient) , "GetEventModules" => GetEventModules(GetPerson($CLientUserVo2->idUser) , 2 , 14) ]; 
    }
    return  $arrayList;
}

function SendMail($arrays){
    for ($i=0; $i < count($arrays) ; $i++) { 
      if(isset($arrays[$i]["GetEventModules"])){
        if($arrays[$i]["GetEventModules"]->state == 1){
         if($arrays[$i]["Client"]->state == 1 && $arrays[$i]["Person"]->state == 1 ){ 
            $MailsUtils = new MailsUtils();
            $word = [ "init" => "" , "finish" => ""];
            $MailsUtils->AddDestinatario($arrays[$i]["Person"]->email);
            $array = $MailsUtils->GetArrayModel();
            $array["Event"] = 2;
            $array["IdRol"] = $arrays[$i]["Person"]->idRole;
            $array["WordReplace"] = ["#cliente" , "#fecha"];
            $array["ToReplace"] = [ $arrays[$i]["Client"]->names." ".$arrays[$i]["Client"]->lastNames , date("y-m-d")];
            $array["Alias"] = "vortex - recordatorios";
            $nuevafecha = strtotime ( ValidateNotificationsTime($arrays[$i]["GetEventModules"]->typeNotification ,$arrays[$i]["GetEventModules"]->numberNotification ) , strtotime ( $arrays[$i]["date"] ) ) ;
            $nuevafecha = date ( 'Y-m-d' , $nuevafecha );
            $word["init"] = $nuevafecha;
            $word["finish"] = date("Y-m-d");
            $MailsUtils->SendWithDate($array , $word );
             }
            }
        }
    }
}

 function GetVisit($id){
    $VisitsVo = new VisitsVo();
    $VisitsVo->id = $id;
    $generalDao = new GeneralDao();
    $generalDao->GetById($generalVo = clone $VisitsVo);
    if($VisitsVo2 = $generalDao->GetVo($generalVo = clone $VisitsVo)) {
        return $VisitsVo2;
    }
}

function ValidateNotificationsTime($notification, $days){
    if($notification == "mismo_dia"){
        return "+0 day";
    }else if($notification == "post_dia"){
        return "+{$days} day";
    }else{
        return "-{$days} day";
    }
}

function GetEventModules($personVo , $event , $module){
    $ModuleEventVo = new  ModuleEventVo();
    $ModuleEventVo->idRol = $personVo->idRole;
    $ModuleEventVo->idSearchField = 4;
    $generalDao = new GeneralDao();
    $generalDao->GetByField($generalVo = clone $ModuleEventVo);
    while($ModuleEventVo2 = $generalDao->GetVo($generalVo = clone $ModuleEventVo)) {
        if($ModuleEventVo2->idEvent == $event){
          if($ModuleEventVo2->idModule == $module){
            return $ModuleEventVo2;
          }
        }
    }
    return false;
}

 function GetPerson($id){
    $PersonVo = new PersonVo();
    $PersonVo->id = $id;
    $generalDao = new GeneralDao();
    $generalDao->GetById($generalVo = clone $PersonVo);
    if($PersonVo2 = $generalDao->GetVo($generalVo = clone $PersonVo)) {
        return $PersonVo2;
    }
}


$array = GetVisitByPerson();
SendMail($array);


?>