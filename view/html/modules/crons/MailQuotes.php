<?php

require_once('/var/www/html/vortex/controller/general/Config.php');
require_once(Config::PATH . Config::BACKEND . 'modules/QuotationVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/PersonVo.php');
require_once(Config::PATH . Config::CONTROLLER . 'general/MailsUtils.php');
require_once(Config::PATH . Config::BACKEND . 'modules/ModuleEventVo.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralDao.php');


 function GetQuotesByPerson(){
    $arrayList = array();
    $QuotationVo = new QuotationVo();
    $QuotationVo->isList = true;
    $generalDao = new GeneralDao();
    $generalDao->GetById($generalVo = clone $QuotationVo);
    while($QuotationVo2 = $generalDao->GetVo($generalVo = clone $QuotationVo)) {
        $arrayList[] = ["id" => $QuotationVo2->id , "date" => $QuotationVo2->generationDate , "Person" => GetPerson($QuotationVo2->idUser) , "state" => $QuotationVo2->state , "GetEventModules" => GetEventModules(GetPerson($CLientUserVo2->idUser) , 3 , 4) ]; 
    }
    return  $arrayList;
}

function SendMail($arrays){
    for ($i=0; $i < count($arrays) ; $i++) { 
        if(isset($arrays[$i]["GetEventModules"])){
            if($arrays[$i]["GetEventModules"]->state == 1){
            if( $arrays[$i]["Person"]->state == 1 ){ 
            $MailsUtils = new MailsUtils();
            $word = [ "init" => "" , "finish" => ""];
            $MailsUtils->AddDestinatario($arrays[$i]["Person"]->email);
            $array = $MailsUtils->GetArrayModel();
            $array["Event"] = 3;
            $array["IdRol"] = $arrays[$i]["Person"]->idRole;
            $array["WordReplace"] = ["#diaGenerada" , "#codigo"];
            $array["ToReplace"] = [ $arrays[$i]["date"] , $arrays[$i]["id"]];
            $array["Alias"] = "vortex - recordatorios";
            $nuevafecha = strtotime ( ValidateNotificationsTime($arrays[$i]["GetEventModules"]->typeNotification ,$arrays[$i]["GetEventModules"]->numberNotification ) , strtotime ( $arrays[$i]["date"] ) ) ;
            $nuevafecha = date ( 'Y-m-d' , $nuevafecha );
            $word["init"] = $nuevafecha;
            $word["finish"] = date("Y-m-d");
            $MailsUtils->SendWithDate($array , $word );
            }
            }
        }
    }
}

function GetEventModules($personVo , $event , $module){
    $ModuleEventVo = new  ModuleEventVo();
    $ModuleEventVo->idRol = $personVo->idRole;
    $ModuleEventVo->idSearchField = 4;
    $generalDao = new GeneralDao();
    $generalDao->GetByField($generalVo = clone $ModuleEventVo);
    while($ModuleEventVo2 = $generalDao->GetVo($generalVo = clone $ModuleEventVo)) {
        if($ModuleEventVo2->idEvent == $event){
          if($ModuleEventVo2->idModule == $module){
            return $ModuleEventVo2;
          }
        }
    }
    return false;
}

 function GetPerson($id){
    $PersonVo = new PersonVo();
    $PersonVo->id = $id;
    $generalDao = new GeneralDao();
    $generalDao->GetById($generalVo = clone $PersonVo);
    if($PersonVo2 = $generalDao->GetVo($generalVo = clone $PersonVo)) {
        return $PersonVo2;
    }
}

function ValidateNotificationsTime($notification, $days){
    if($notification == "mismo_dia"){
        return "+0 day";
    }else if($notification == "post_dia"){
        return "+{$days} day";
    }else{
        return "-{$days} day";
    }
}


$array = GetQuotesByPerson();
SendMail($array);


?>