function ChangeState(id, state, i) {
    var parameters = {
        "isActionFromAjax": true,
        "optionInputForm": 9,
        "idDataInputForm": 0,
        "id": id,
        "state": state
    };

    $.ajax({
        url: "view/html/modules/modules/AjaxDataModules.php",
        type: 'post',
        data: parameters,
        success: function(data) {
            if ($("#validateBTN" + id).text() != "Modulo Desactivado") {
                $("#validateBTN" + id).html('<button   class="D btn btn-block  btn-danger" onclick="ChangeState(' + id + ',' + state + ',' + i + ')">Modulo Desactivado</button><button title="subpermisos"  disabled class="btn btn-primary btn-xs-block btn-block"><i class="fa fa-list"></i></button>');
            } else {

                $("#validateBTN" + id).html('<button   class="A btn btn-block  btn-success" onclick="ChangeState(' + id + ',' + state + ',' + i + ')"> Modulo Activado</button><button onclick="LoadAction(' + i + ')" title="subpermisos"  class="btn btn-primary btn-xs-block btn-block"><i class="fa fa-list"></i></button>');
            }
        }
    })

}

function ChangeState2(id, state) {
    var parameters = {
        "isActionFromAjax": true,
        "optionInputForm": 9,
        "idDataInputForm": 0,
        "id": id,
        "state": state
    };

    $.ajax({
        url: "view/html/modules/modulesactions/AjaxDataModulesActions.php",
        type: 'post',
        data: parameters,
        success: function(data) {

            if ($("#validateBTN2-" + id).text() != "Acción Desactivada") {
                $("#validateBTN2-" + id).html('<button style=""  class="D btn btn-block btn-danger" onclick="ChangeState2(' + id + ',' + 0 + ')">Acción Desactivada</button>');
            } else {
                $("#validateBTN2-" + id).html('<button style="" class="A btn-block btn btn-success" onclick="ChangeState2(' + id + ',' + 1 + ')">Acción Activada</button>');
            }
        }
    })

}

function LoadRoles() {
    var parameters = {
        "isActionFromAjax": true,
        "optionInputForm": 5,
        "idDataInputForm": 0,
    };

    $.ajax({
        url: "view/html/modules/modules/AjaxDataModules.php",
        type: 'post',
        data: parameters,
        success: function(data) {
            EachRoles(JSON.parse(data));
        }
    })
}

function LoadModules(id) {
    var parameters = {
        "isActionFromAjax": true,
        "optionInputForm": 7,
        "idDataInputForm": 0,
        "idRol": id
    };

    $.ajax({
        url: "view/html/modules/modules/AjaxDataModules.php",
        type: 'post',
        data: parameters,
        success: function(data) {
            EachModules(JSON.parse(data), id);
        }
    })
}

function EachRoles(roles) {
    for (var i = 0; i < roles.length; i++) {
        if (roles[i].name != "Cliente" && roles[i].name != "Prospecto" && roles[i].name != "Proveedor" && roles[i].name != "SubDistribuidor") {
            $("#card-roles").append(' <div class="col-lg-3 col-xs-6"><div class="shadow-lg rounded small-box bg-' + roles[i].color + '"><div class="inner inner-1"><h4><b>' + roles[i].name + '</b></h4><h2></h2>&nbsp</div><div class="icon"><i class="fa fa-' + roles[i].icon + '"></i></div><a href="#" data-toggle="modal" data-target="#ModulePopUpAdd" onclick="LoadModules(' + roles[i].id + ')" class="small-box-footer">Ver Permisos <i class="fa fa-arrow-circle-right"></i></a></div></div>');
        }
    }

}

function EachModules(module, id) {
    $("#modulesList").html("");

    for (var i = 0; i < module.length; i++) {
        if(module[i].alias != null || module[i].alias != undefined ){
            console.log(module[i]);
        $("#modulesList").append('<div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6"><b>   <span>' + module[i].alias + '</span></b><div id="validateBTN' + module[i].id + '"></div>          <div class="box hidden"  id="200-' + module[i].id + '" style="  box-shadow: 0 0 10px black;"> <div class="box-header with-border text-center">Acciones de ' + module[i].alias + ' </div><div class=""box-body" id="contentBox-' + module[i].id + '">    <div class="container-fluid"><div class="row" id="datamodulesList2-' + i + '"></div></div>   <div class="container-fluid"><div class="row" ></div></div>      </div></div>                  <div class="box hidden " id="400-' + module[i].id + '" style="  box-shadow: 0 0 10px black;"> <div class="box-header with-border bg-danger text-center">Acciones Desactivadas</div><div class=""box-body" id="">  <div class="container-fluid"><div class="row" id=""><h2 class="text-center">Activa el modulo para ver los permisos</h2></div></div>   </div></div>               </div> ');
        if (module[i].state == 1) {
            $("#validateBTN" + module[i].id).append('<button  class="A btn btn-success btn-block" onclick="ChangeState(' + module[i].id + ',' + module[i].state + ',' + i + ')">Modulo Activado</button><button onclick="LoadAction(' + i + ')" title="subpermisos"  class="btn btn-primary btn-block btn-xs-block"><i class="fa fa-list"></i></button>');
            $("#400-" + module[i].id).attr("class", "hidden");
        } else {
            $("#validateBTN" + module[i].id).append('<button  class="D btn btn-danger btn-block" onclick="ChangeState(' + module[i].id + ',' + module[i].state + ',' + i + ')">Modulo Desactivado</button><button title="subpermisos"  disabled class="btn btn-primary btn-xs-block btn-block"><i class="fa fa-list"></i></button>');
            $("#200-" + module[i].id).attr("class", "hidden ");

            }
        }
        LoadModules2(id, i);
    }

}

function LoadAction(id) {
    $("#ModuleAllows").modal('show');
    //$("#ModulePopUpAdd").modal('close');
    $("#contentActionsModule").html($("#datamodulesList2-" + id));
}

function LoadModules2(id, i) {
    var parameters = {
        "isActionFromAjax": true,
        "optionInputForm": 7,
        "idDataInputForm": 0,
        "idRol": id
    };

    $.ajax({
        url: "view/html/modules/modulesactions/AjaxDataModulesActions.php",
        type: 'post',
        data: parameters,
        beforeSend: function() {
            $("#datamodulesList2-" + i).html("°");
        },
        success: function(data) {
            EachModules2(JSON.parse(data), i);
        }
    })
}

function EachModules2(modules, i) {

    if (modules[i].events.length > 0) {
        for (var e = 0; e < modules[i].events.length; e++) {
            $("#datamodulesList2-" + i).append('<div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-3"><b>   <span class="text-center">Acción para ' + modules[i].events[e].action + '</span></b><div id="validateBTN2-' + modules[i].events[e].id + '"></div></div></button>');
            if (modules[i].events[e].state == 1) {
                $("#validateBTN2-" + modules[i].events[e].id).append('<button class="A btn btn-block btn-success" style="margin:5px;" onclick="ChangeState2(' + modules[i].events[e].id + ',' + modules[i].events[e].state + ')">Acción Activada</button>');
            } else {
                $("#validateBTN2-" + modules[i].events[e].id).append('<button class="D btn btn-block btn-danger" style="margin:5px;" onclick="ChangeState2(' + modules[i].events[e].id + ',' + modules[i].events[e].state + ')">Acción Desactivada</button>');
            }

        }

    } else {
        $("#datamodulesList2-" + i).append('<h2 class="text-center" style="margin:15px;">Sin Acciones Asignadas </h2> ');
    }
}






/*********************************CLICK EVENTS**************************/

$(function() {
    LoadRoles();

});