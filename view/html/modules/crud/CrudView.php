<?php
require_once(Config::PATH . Config::CONTROLLER . 'general/AccessManager.php');
require_once(Config::PATH . Config::CONTROLLER . 'general/UtilJquery.php');
require_once(Config::PATH . Config::VIEW_HTML . 'general/comps/GeneralCtx.php');
require_once(Config::PATH . Config::VIEW_HTML . 'general/comps/GeneralLst.php');
require_once(Config::PATH . Config::VIEW_HTML . 'general/comps/GeneralWithDataLst.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralDao.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/CrudVo.php');
// require_once(Config::PATH . Config::BACKEND . 'modules/PersonVo.php');
// require_once(Config::PATH . Config::BACKEND . 'modules/CompanyVo.php');
// require_once(Config::PATH . Config::BACKEND . 'modules/UserVo.php');
// require_once(Config::PATH . Config::BACKEND . 'modules/RoleVo.php');

class CrudView
{
    private $optionInputForm;
    private $isUpdateUserForm;
    private $idDataInputForm;
    /*=============================================
                  Session Vars
    =============================================*/
    private $manageSession;
    /*=============================================
                  Template Vars
    =============================================*/
    private $utilJQ;
    private $pageTpl, $data, $core;
    /*=============================================
                Controls Vars HTML
    =============================================*/

    private $productNamesCtx;
    private $productCostCtx;
    private $productProviderCtx;
    private $productCompanyCtx;
    private $productDateCtx;
    
    // private $userNicknameCtx;
    // private $userPasswordCtx;

    /*=============================================
                Data Vars
    =============================================*/
    private $generalDao;


    function __construct($pageTpl, $data, $core)
    {
        $this->data = $data;
        $this->core = $core;
        $this->pageTpl = $pageTpl;
            $this->CreateComponents();
            $this->UseDataFromThePostback();
    }

    private function UseDataFromThePostback()
    {

        if (isset($_POST['optionInputForm'])) {
            $this->optionInputForm = $_POST['optionInputForm'];
            $this->idDataInputForm = $_POST['idDataInputForm'];
            $this->generalDao = new GeneralDao();
            switch ($this->optionInputForm) {
                case "1": {
                    $this->SetData();
                    break;
                }
                case "2": {
                    $this->DeleteData();
                    break;
                }
                case "4": {
                    $this->SetUpdate();
                    break;
                }
            }
            //echo '<script>window.location = "Users";</script>';
        }
        if (isset($_POST['isUpdateUserForm'])) {
            $this->isUpdateUserForm = $_POST['isUpdateUserForm'];
            if($this->isUpdateUserForm == "1"){
                $this->UpdateUser();
            }
        }
        $this->AssignDataToTpl();

    }

    private function CreateComponents()
    {
        $this->productNamesCtx = new GeneralCtx("productNamesCtx","Nombre Producto",null,true);
        $this->productCostCtx = new GeneralCtx("productCostCtx","Costo",null,true);

        $this->productProviderCtx = new GeneralCtx("productProviderCtx","Proveedor",null,false);
        $this->productCompanyCtx = new GeneralCtx("productCompanyCtx","Empresa",null,true);
        $this->productDateCtx = new GeneralCtx("productDateCtx","Fecha",null,true);

        // $roleVo = new RoleVo();
        // $roleVo->isList = true;
        // $this->personIdRoleLst = new GeneralLst($generalVo = clone $roleVo,"personIdRoleLst", 0, 1,"--Role--", true);

        // $companyVo = new CompanyVo();
        // $companyVo->isList = true;
        
        // $this->userNicknameCtx = new GeneralCtx("userNicknameCtx","Usuario",null,true);
        // $this->userPasswordCtx = new GeneralCtx("userPasswordCtx","Clave","password",true);

        $this->utilJQ = new UtilJquery("Crud");
    }

    private function SetDataToVo()
    {
        $crudVo = new CrudVo();
        $crudVo->id = $this->idDataInputForm;
        $crudVo->nameProduct = $_POST["productNamesCtx"];
        $crudVo->costProduct = $_POST["productCostCtx"];
        $crudVo->providerProduct = $_POST["productProviderCtx"];
        $crudVo->companyProduct = $_POST["productCompanyCtx"];
        $crudVo->dateProduct = $_POST["productDateCtx"];
        return $crudVo;
    }

    private function SetData()
    {
        $crudVo = $this->SetDataToVo();
        $crudVo->id = null;
        $result = $this->generalDao->Set($generalVo = clone $crudVo);
        if (isset($result)) {
            // $this->SetUser($result);
            $message = 'MessageSwalBasic("Registrado!","Producto Registrado Correctamente","success");';
            $this->utilJQ->AddFunctionJavaScript($message);
        }

    }

    private function SetUpdate()
    {
        $result = $this->generalDao->Update($generalVo = clone $this->SetDataToVo());
        if (isset($result)) {
            $message = 'MessageSwalBasic("Modificado!","Producto Modificado Correctamente","success");';
            $this->utilJQ->AddFunctionJavaScript($message);
        }
    }

    private function DeleteData()
    {
        $message = "";
        $crudVo = new CrudVo();
        $crudVo->id = $this->idDataInputForm;
        $crudVo->idUpdateField = 13;
           $crudVo->isList = false;
        $crudVo->state = 0;
        $crudVo->idSearchField = 0;
        $this->DeleteUser($crudVo->id);
        if ($this->generalDao->UpdateByField($generalVo = clone $crudVo)) {
            
        } 
         $message = 'MessageSwalBasic("Eliminado!","Producto Eliminado Correctamente","success");';
        $this->utilJQ->AddFunctionJavaScript($message);
    }

    // private function DeleteUser($idProduct){
    //     $crudVo = new CrudVo();
    //     $crudVo->idSearchField = 5;
    //     $crudVo->id = $idProduct;
    //     $generalDao = new GeneralDao();
    //     $generalDao->DeleteByField($generalVo = clone $crudVo);
    // }

    // private function SetUser($idPerson){
    //     $crudVo = new CrudVo();
    //     $crudVo->nameProduct;
    //     $crudVo->costProduct;
    //     $crudVo->providerProduct;
    //     $crudVo->companyProduct;
    //     $crudVo->dateProduct;
    //     $generalDao = new GeneralDao();
    //     $result = $generalDao->Set($generalVo = clone $crudVo);
    //     if (!isset($result)) {
    //         $message = 'MessageSwalBasic("Problema!","Problema al Registrar el producto por Defecto","error");';
    //         $this->utilJQ->AddFunctionJavaScript($message);
    //     }

    // }

    // private function UpdateUser(){
    //     $userVo = new UserVo();
    //     $userVo->id = $_POST["userIdInputForm"];
    //     $userVo->nickname = $_POST["nameProduct"];
    //     $userVo->password = $_POST["userPasswordCtx"];
    //     $userVo->state = $_POST["userStateInputForm"];
    //     $generalDao = new GeneralDao();
    //     $result = $generalDao->Update($generalVo = clone $userVo);
    //     if (isset($result)) {
    //         $message = 'MessageSwalRedirect("¡Modificado!","Credenciales de Usuario Modificadas Correctamente","success","Users");';
    //         $this->utilJQ->AddFunctionJavaScript($message);
    //     }
    // }

    // private function SavePhoto($idUser,$idPerson){
    //     $document = $this->GetPersonVoForPhoto($idPerson);
    //     $directory = "view/img/users/default/anonymous.png";
    //     //print_r($_FILES["userPhotoUploadFile"]["tmp_name"]);
    //     if(isset($_FILES["userPhotoUploadFile"]["tmp_name"]) && !empty($_FILES["userPhotoUploadFile"]["tmp_name"])) {
    //         $file = $_FILES["userPhotoUploadFile"]["tmp_name"];
    //         list($width, $height) = getimagesize($file);
    //         $newWidth = 500;
    //         $newHeight = 500;
    //         if($_FILES["userPhotoUploadFile"]["type"] == "image/jpeg"){
    //             $numRandom = rand(1000,5000);
    //             $directory = $this->GenerateFolderImg($idUser,$document);
    //             $directory .= "/".$document.$numRandom.".jpeg";
    //             $this->ChangePhotoSession($idUser,$directory);
    //             $origin = imagecreatefromjpeg($file);
    //             $destination = imagecreatetruecolor($newWidth,$newHeight);
    //             imagecopyresized($destination,$origin,0,0,0,0,$newWidth,$newHeight,$width,$height);
    //             imagejpeg($destination,$directory);
    //         }
    //     }
    //     return $directory;
    // }

    // private function GenerateFolderImg($idUser,$document){
    //     $directory = "view/img/users/".$idUser.$document;
    //     $this->DeletePhotoUser($directory);
    //     if (!file_exists($directory)) {
    //         mkdir($directory,0755);
    //     }
    //     return $directory;
    // }

    // private function DeletePhotoUser($directory){
    //     $files = glob($directory.'/*'); // get all file names
    //     foreach($files as $file){ // iterate files
    //         if(is_file($file))
    //             unlink($file); // delete file
    //     }
    // }

    // private function GetPersonVoForPhoto($idPerson){
    //     $personVo = new PersonVo();
    //     $personVo->id = $idPerson;
    //     $generalDao = new GeneralDao();
    //     $generalDao->GetById($generalVo = clone $personVo);
    //     if($personVo2 = $generalDao->GetVo($generalVo = clone $personVo)){
    //         return $personVo->document;
    //     }
    // }


    // private function ChangePhotoSession($idUser,$directory){
    //     $userVo = unserialize($_SESSION['user']["user"]);
    //     if($idUser == $userVo->id){
    //         $userVo->photo = $directory;
    //         $_SESSION['user']["user"] = serialize($userVo);
    //     }
    // }

    private function AssignDataToTpl()
    {
        $this->data->assign('productNamesCtx', $this->productNamesCtx->paint());
        $this->data->assign('productCostCtx', $this->productCostCtx->paint());
        $this->data->assign('productProviderCtx', $this->productProviderCtx->paint());
        $this->data->assign('productCompanyCtx', $this->productCompanyCtx->paint());
        $this->data->assign('productDateCtx', $this->productDateCtx->paint());

        $this->data->assign('jquery', $this->utilJQ->PaintJQ());
        $this->SetDataToTpl();
    }

    private function SetDataToTpl()
    {
        echo $this->core->get($this->pageTpl, $this->data);
    }

}