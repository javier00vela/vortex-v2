<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Administrar usuarios
        </h1>
        <ol class="breadcrumb">
            <li><a href="Home"><i class="fa fa-dashboard"></i> Inicio</a></li>
            <li class="active">Administrar usuarios</li>
        </ol>
    </section>
    <section class="content">
        <div class="box">
            <div class="box-header with-border">

                <nav class="navbar navbar-light">
                    <div class="container-fluid">
                        <ul class="nav navbar-nav">
                            <li>
                                <a node="node-insertar_usuario" style="cursor: pointer;" id="userAddBtn" name="userAddBtn" data-toggle="modal" data-target="#productPopUpAdd" onclick="ResetControls('containerinputs');SetData()">
                                    <i class="fa fa-plus-circle"></i> Agregar usuario
                                </a>
                            </li>
                            <li>
                                <a style="cursor: pointer;" id="userAddBtn" name="userAddBtn" onclick="location.href = 'Home' ">
                                    <i class="fa fa-home"></i> Menu Principal
                                </a>
                            </li>
                        </ul>
                    </div>
                </nav>


            </div>

            <div class="box-body">
                <table id="userTbl" name="userTbl" class="table table-bordered table-striped dt-responsive tables dataTable no-footer dtr-inline" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Nombre Producto</th>
                            <th>Costo</th>
                            <th>Proveedor</th>
                            <th>Empresa</th>
                            <th>Fecha</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </section>
</div>
<!--=====================================
VENTANA MODAL AGREGAR PERSONA O MODIFICAR
======================================-->
<div id="productPopUpAdd" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form id="productForm" role="form" method="post" enctype="multipart/form-data">
                <!--=====================================
              Hidden
              ======================================-->
                <input id="optionInputForm" name="optionInputForm" type="hidden" value="0">
                <input id="idDataInputForm" name="idDataInputForm" type="hidden" value="0">
                <input id="isActionFromAjax" name="isActionFromAjax" type="hidden" value="false">
                <!--=====================================
              CABEZA DEL MODAL
              ======================================-->
                <div class="modal-header" style="background:#3c8dbc; color:white">
                    <button type="button" class="close" data-dismiss="modal" data-target='#productPopUpAdd' onclick="ResetControls('productForm')">&times;</button>
                    <h4 class="modal-title">Agregar usuario</h4>
                </div>
                <!--=====================================
              CUERPO DEL MODAL
              ======================================-->
                <div class="modal-body" id="containerinputs">
                    <div class="box-body">
                        <div class="row">
                            <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-user"></i></span> {$productNamesCtx}
                                    <!-- <input id="userNamesCtx" name="userNamesCtx" type="text" class="form-control input-lg" placeholder="Ingresar nombres" required> -->
                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-user-o"></i></span> {$productCostCtx}
                                    <!-- <input id="userLastNamesCtx" name="userLastNamesCtx" type="text" class="form-control input-lg"  placeholder="Ingresar apellidos" required> -->
                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-id-card"></i></span> {$productProviderCtx}
                                    <!-- <select id="userDocumentTypeLst" name="userDocumentTypeLst" class="form-control input-lg" required> -->

                                    <!-- </select> -->
                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-id-card-o"></i></span> {$productCompanyCtx}
                                    <!-- <input id="userDocumentNumberCtx" name="userDocumentNumberCtx" type="text" class="form-control input-lg" placeholder="Ingresar Numero de Documento" required> -->
                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-mobile"></i></span> {$productDateCtx}
                                    <!-- <input id="userCellNumberCtx" name="userCellNumberCtx" type="text" class="form-control input-lg" placeholder="Ingresar numero celular" required> -->
                                </div>
                            </div>
                           
                        </div>
                    </div>
                </div>
                <!--=====================================
              PIE DEL MODAL
              ======================================-->
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal" data-target='#productPopUpAdd' onclick="ResetControls('productForm')">Salir</button>
                    <button type="submit" class="btn btn-primary">Guardar Producto</button>
                </div>
            </form>

        </div>
    </div>
</div>
// <!--=====================================
// VENTANA MODAL AGREGAR USUARIO O MODIFICAR
// ======================================-->
// <div id="userCredencialsPopUp" class="modal fade" role="dialog">
//     <div class="modal-dialog modal-sm">
//         <div class="modal-content">
//             <form id="userForm" role="form" method="post" enctype="multipart/form-data">
//                 <!--=====================================
//                 Hidden
//                 ======================================-->
//                 <input id="isUpdateUserForm" name="isUpdateUserForm" type="hidden" value="0">
//                 <!--==================================-->
//                 <input id="userIdInputForm" name="userIdInputForm" type="hidden" value="0">
//                 <input id="userStateInputForm" name="userStateInputForm" type="hidden" value="0">
//                 <input id="userIdPersonInputForm" name="userIdPersonInputForm" type="hidden" value="0">
//                 <!--=====================================
//                 CABEZA DEL MODAL
//                 ======================================-->
//                 <div class="modal-header" style="background:#3c8dbc; color:white">
//                     <button type="button" class="close" data-dismiss="modal" onclick="ResetControls('userForm')">&times;</button>
//                     <h4 class="modal-title">Credenciales del Usuario</h4>
//                 </div>
//                 <!--=====================================
//                 CUERPO DEL MODAL
//                 ======================================-->
//                 <div class="modal-body">
//                     <div class="box-body">
//                         <div class="row">

//                             <div class="form-group">
//                                 <img id="userPhotoImg" name="userPhotoImg" class="profile-user-img img-responsive img-circle" src="view/img/users/default/anonymous.png" alt="User profile picture">
//                                 <h3 class="profile-username text-center">Nombre Usuario</h3>

//                                 <label class="btn btn-block btn-primary">
//                                     Subir Foto<input id="userPhotoUploadFile" name="userPhotoUploadFile" type="file" style="display: none;">
//                                 </label>
//                                 <p class="help-block">Peso máximo de la foto 25MB</p>
//                             </div>
//                             <div class="form-group">
//                                 <div class="input-group">
//                                     <span class="input-group-addon"><i class="fa fa-user"></i></span> {$userNicknameCtx}
//                                 </div>
//                             </div>
//                             <div class="form-group">
//                                 <div class="input-group">
//                                     <span class="input-group-addon"><i class="fa fa-lock"></i></span> {$userPasswordCtx}
//                                     <span class="input-group-addon" id="password"><i class="fa fa-eye"></i></span>
//                                 </div>
//                             </div>
//                         </div>
//                     </div>
//                 </div>
//                 <!--=====================================
//                 PIE DEL MODAL
//                 ======================================-->
//                 <div class="modal-footer">
//                     <button type="button" class="btn btn-default pull-left" data-dismiss="modal" onclick="ResetControls('userForm')">Salir</button>
//                     <button id="userFormBtn" name="userFormBtn" type="submit" class="btn btn-primary">Guardar Credenciales</button>
//                 </div>
//             </form>

//         </div>
//     </div>
</div>


<div id="containerResponseAjax"></div>
<script src="view/html/modules/crud/Crud.js"></script>
{$jquery}