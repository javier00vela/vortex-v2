// jshint unused:true
"use strict";
var tbl;

function LoadTable() {
    RemoveActionModule(39);
    GetArrayFromDataModule(39);
    setTimeout(function () {
        var parameters = {
            "isActionFromAjax": true,
            "optionInputForm": 0,
            "idDataInputForm": 0,
            "listData": arrayResponse,
        };


        tbl = $('#impostTbl').DataTable({
            responsive: true,
            "drawCallback": function (settings) {
                $('[data-toggle="tooltip"]').tooltip()
            },
            language: {
                "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
            },
            "ajax": {
                url: "view/html/modules/viatics/AjaxDataViatics.php",
                method: "POST",
                data: parameters
            }
        });
    }, 500);
}

/*********************************CLICK EVENTS**************************/

function updateStateAllowViatic(id, state) {
    MessageSwalConfirm("¿Deseas Aceptar los costos establecidos de viaticos?", "una vez aceptado no podras modificar cada costo de viatico", "warning", function () {
        var parameters = {
            "isActionFromAjax": true,
            "optionInputForm": 7,
            "idDataInputForm": id,
            "isAccept": state,
        };
        $.ajax({
            url: "view/html/modules/viatics/AjaxDataViatics.php",
            type: 'post',
            data: parameters,
            success: function (data) {
                tbl.ajax.reload();
            }
        });
    });
}


function ChangeFormatPrices() {
    $("#impostPercentCtx").keypress(function (event) {
        console.log(event.charCode);
        if (event.charCode > 58 || event.charCode < 43 || event.charCode == 45 || event.charCode == 46) {
            return false;
        }
    });
}

function ValidatePriceUnit() {
    $("#impostPercentCtx").change(function (event) {
        var cont = 0;
        var numberType = $(this).val();
        for (var i = 0; i < numberType.toString().length; i++) {
            if (numberType.toString()[i] == "," || numberType.toString()[i] == ".") {
                cont++;
            }
        }
        if (cont > 1) {
            numberType = "";
        }
        $(this).val(numberType);
    });
}

$(function () {
    ChangeFormatPrices();
    ValidatePriceUnit();
    LoadTable();
   
    $("#viaticsPersonsCtx").attr("min","1");
    $("#viaticsPersonsCtx").val(1);
});