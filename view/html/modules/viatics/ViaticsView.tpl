<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Administrar Viaticos Y/O costos de viajes
        </h1>
        <ol class="breadcrumb">
            <li><a href="Home"><i class="fa fa-dashboard"></i> Inicio</a></li>
            <li class="active">Administrar Viaticos</li>
        </ol>
    </section>
    <section class="content">
        <div class="box">
            <nav class="navbar navbar-light">
                <div class="container-fluid">
                    <ul class="nav navbar-nav">
                        <li>
                            <a node="node-agregar_costo" style="cursor: pointer;" id="impostAddBtn" name="impostAddBtn"
                                data-toggle="modal" data-target="#impostPopUpAdd"
                                onclick="ResetControls('containerinputs');SetData()">
                                <i class="fa fa-plus-circle"></i> Agregar Viaticos
                            </a>
                        </li>
                        <li>
                            <a style="cursor: pointer;" id="userAddBtn" name="userAddBtn"
                                onclick="location.href = 'Home' ">
                                <i class="fa fa-home"></i> Menu Principal
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>

            <div class="box-body">
                <div class="alert alert-warning alert-dismissible m-3" role="alert">
                    <strong>Recuerda!</strong> recuerda que el precio total por viaje va derivado a los costos de los
                    viaticos.
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span style="color:black" aria-hidden="true">&times;</span>
                    </button>
                </div>
                <table id="impostTbl" name="impostTbl"
                    class="table table-bordered table-striped dt-responsive tables dataTable no-footer dtr-inline"
                    style="width: 100%;" cellspacing="0">
                    <thead>
                        <tr>
                            <th>id</th>
                            <th>Lugar Viatico</th>
                            <th>Días Viatico</th>
                            <th>Cantidad Personas</th>
                            <th>Precio Transporte</th>
                            <th>Precio Alimentos</th>
                            <th>Precio Hospedaje</th>
                            <th>Precio Improvistos</th>
                            <th>Total de Items</th>
                            <th>Acciones</th>
                            <th>Permitir Viatico</th>
                        </tr>
                    </thead>
                </table>

            </div>
        </div>
    </section>
</div>
<!--=====================================
VENTANA MODAL AGREGAR IMPUESTO O MODIFICAR
======================================-->
<div id="impostPopUpAdd" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form id="ViaticsForm" role="form" method="post" enctype="multipart/form-data">
                <!--=====================================
              Hidden
              ======================================-->
                <input id="optionInputForm" name="optionInputForm" type="hidden" value="0">
                <input id="idDataInputForm" name="idDataInputForm" type="hidden" value="0">
                <input id="isActionFromAjax" name="isActionFromAjax" type="hidden" value="false">
                <!--=====================================
              CABEZA DEL MODAL
              ======================================-->
                <div class="modal-header" style="background:#3c8dbc; color:white">
                    <button type="button" class="close" data-dismiss="modal" data-target='#impostPopUpAdd'
                        onclick="ResetControls('impostForm')">&times;</button>
                    <h4 class="modal-title">Agregar Viaje</h4>
                </div>
                <!--=====================================
              CUERPO DEL MODAL
              ======================================-->
                <div class="modal-body">
                    <div class="box-body">
                        <div class="row">
                            <div class="alert bg-primary">
                                <p> Nota : Se debe diligenciar el nombre y los dias del viaje para diligenciar los
                                    costos de viaticos correspondido. </p>
                            </div>
                            <div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-cube"></i></span>
                                    {$viaticsNameCtx}
                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-industry"></i></span>
                                    {$viaticsDaysCtx}
                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-users"></i></span>
                                    {$viaticsPersonsCtx}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--=====================================
              PIE DEL MODAL
              ======================================-->
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal"
                        data-target='#impostPopUpAdd' onclick="ResetControls('impostForm')">Salir</button>
                    <button type="submit" id="optionsBtn" class="btn btn-primary">Agregar Viaje</button>
                </div>
            </form>

        </div>
    </div>
</div>
<div id="containerResponseAjax"></div>
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<script src="view/html/modules/viatics/Viatics.js?v= {$tempParameter}"></script>
{$jquery}