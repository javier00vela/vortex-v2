<?php
require_once __DIR__ . '/../../../../controller/general/Config.php';
require_once Config::PATH . Config::CONTROLLER . Config::GENERAL . 'ManageArrays.php';
require_once Config::PATH . Config::BACKEND . 'modules/ViaticsVo.php';
require_once Config::PATH . Config::BACKEND . 'modules/ViaticsPropetyVo.php';
require_once Config::PATH . Config::BACKEND . 'general/GeneralDao.php';

class AjaxDataViatics
{

    private $isActionFromAjax;
    private $optionInputForm;
    private $idDataInputForm;
    public $dataList = [];
    public $arrayViatics = [
        "transport" => [
            "price" => 0,
            "quantity" => 0,
        ],
        "food" => [
            "price" => 0,
            "quantity" => 0,
        ],
        "improvistos" => [
            "price" => 0,
            "quantity" => 0,
        ],
        "hospedaje" => [
            "price" => 0,
            "quantity" => 0,
        ],
        "total" => 0,

    ];

    public function __construct()
    {
        if (isset($_POST['isActionFromAjax'])) {
            $this->isActionFromAjax = $_POST['isActionFromAjax'];
            $this->optionInputForm = $_POST['optionInputForm'];
            $this->idDataInputForm = $_POST['idDataInputForm'];
            if (isset($_POST["listData"])) {
                $this->dataList = $_POST["listData"];
            }
            if ($this->isActionFromAjax == "true") {
                if ($this->optionInputForm == 3) {
                    $this->SetDataInControls();
                } else if ($this->optionInputForm == 5) {
                    $this->GetLengthImpost();
                } else if ($this->optionInputForm == 7) {
                    $this->UpdateStateViatic();
                } else {
                    $this->GetDataInJsonForTbl();
                }
            }
        }
    }

    private function SetDataInControls()
    {
        $SetInputsDataFJQ = "";
        $ViaticsVo = new ViaticsVo();
        $ViaticsVo->id = $this->idDataInputForm;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $ViaticsVo);
        if ($ViaticsVo2 = $generalDao->GetVo($generalVo = clone $ViaticsVo)) {
            $SetInputsDataFJQ = "
                            <script>
                               (function($){
                                SetValueToInputText('viaticsNameCtx','{$ViaticsVo2->place}');
                                SetValueToInputText('viaticsDaysCtx','{$ViaticsVo2->days}');
                                SetValueToInputText('optionInputForm',4);
                                SetValueToInputText('idDataInputForm',$ViaticsVo2->id);
                                $('#optionsBtn').text('Editar Viaje');
                               })(jQuery);
                            </script>
                        ";
        }
        echo $SetInputsDataFJQ;
    }

    public function GetDataInJsonForTbl()
    {
        $existData = false;
        $ViaticsVo = new ViaticsVo();
        $ViaticsVo->isList = true;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $ViaticsVo);
        $viaticsCopy = $this->arrayViatics;
        $string = "
        {\"data\": [";
        while ($ViaticsVo2 = $generalDao->GetVo($generalVo = clone $ViaticsVo)) {
            $dataViatic = $this->GetDataViatics($ViaticsVo2->id);
            $existData = true;
            $json = array();
            $json[] = htmlspecialchars(ucfirst(strtolower($ViaticsVo2->id)));
            $json[] = htmlspecialchars(ucfirst(strtolower($ViaticsVo2->place)));
            $json[] = htmlspecialchars(ucfirst(strtolower($ViaticsVo2->days))) . " Día/s";
            $json[] = htmlspecialchars(ucfirst(strtolower($ViaticsVo2->numberPersons))) . " Persona/s";
            $json[] = "$" . $dataViatic["transport"]["price"] . " COP";
            $json[] = "$" . $dataViatic["food"]["price"] . " COP";
            $json[] = "$" . $dataViatic["hospedaje"]["price"] . " COP";
            $json[] = "$" . $dataViatic["improvistos"]["price"] . " COP";
            $json[] = "$" . $dataViatic["total"] . " COP";

            $button = "";
            if (!$ViaticsVo2->isAccept) {
                if (!in_array("modificar_viatico", $this->dataList)) {

                    $button .= "<tip title='Modificar' class='yellow-tooltip' data-toggle='tooltip' data-placement='top'><button id='updateBtn' name='updateBtn' class='btn btn-warning btn-xs' data-toggle='modal' data-target='#impostPopUpAdd' title='Modificar' onclick='UpdateData({$ViaticsVo2->id},\"viatics\",\"Viatics\");'><i class='fa fa-pencil'></i></button> </tip>";
                }
            }
            if (!$ViaticsVo2->isAccept) {
                if (!in_array("eliminar_viatico", $this->dataList)) {
                    $button .= " <tip title='Eliminar' class='red-tooltip' data-toggle='tooltip' data-placement='top'><a id='deleteBtn' name='deleteBtn' class='btn btn-danger  btn-xs' title='Eliminar' onclick='DeleteData({$ViaticsVo2->id},\"Viatics\")'><i class='fa fa-times'></i></a></tip> ";
                }
            }

            if (!in_array("agregar_viatico_costo", $this->dataList)) {
                $button .= " <tip title='editar costos viatico' class='purple-tooltip' data-toggle='tooltip' data-placement='top'><a href=\"ListViatics?idViatic={$ViaticsVo2->id}\" id='deleteBtn' name='deleteBtn' class='btn bg-purple  btn-xs' title='editar costos viatico' ><i class='fa fa-money'></i></a></tip> ";
            }

            $json[] = $button;
            if (!in_array("validar_estado_viatico", $this->dataList)) {
                if ($ViaticsVo2->isAccept) {
                    $json[] = "<button class='btn btn-success btn-block' disabled>Costos Aprobados</button>";
                } else {
                    $json[] = "<button name='viaticBtn-" . $ViaticsVo2->id . "' class='btn btn-danger btn-block' onClick='updateStateAllowViatic(" . $ViaticsVo2->id . " , " . $ViaticsVo2->isAccept . ")' >Costos No Aprobados</button>";
                }
            }

            $string .= json_encode($json) . ",";
            $this->arrayViatics = $viaticsCopy;
        }
        $string .= "-]}-";

        if ($existData) {
            echo str_replace(",-]}-", "]}", $string);
        } else {
            echo "{ \"data\": [] }";
        }
    }

    public function UpdateStateViatic()
    {
        $ViaticsVo = new ViaticsVo();
        $ViaticsVo->id = $this->idDataInputForm;
        $ViaticsVo->idSearchField = 0;
        $ViaticsVo->isAccept = ($_POST["isAccept"]) ? 0 : 1;
        $ViaticsVo->idUpdateField = 3;
        $generalDao = new GeneralDao();
        $generalDao->UpdateByField($generalVo = clone $ViaticsVo);
    }

    private function GetDataViatics(int $idViatic)
    {
        $ViaticsPropetyVo = new ViaticsPropetyVo();
        $ViaticsPropetyVo->idViatic = $idViatic;
        $ViaticsPropetyVo->idSearchField = 5;
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $ViaticsPropetyVo);
        while ($ViaticsPropetyVo2 = $generalDao->GetVo($generalVo = clone $ViaticsPropetyVo)) {
            if ($ViaticsPropetyVo2->typeViatic == "transport") {
                $this->arrayViatics["transport"]["price"] += (str_replace(",", ".", ($ViaticsPropetyVo2->price * ($ViaticsPropetyVo2->percent / 100) + $ViaticsPropetyVo2->price) * $ViaticsPropetyVo2->quantity));
                $this->arrayViatics["transport"]["quantity"] += 1;
            } else if ($ViaticsPropetyVo2->typeViatic == "food") {
                $this->arrayViatics["food"]["price"] += (str_replace(",", ".", ($ViaticsPropetyVo2->price * ($ViaticsPropetyVo2->percent / 100) + $ViaticsPropetyVo2->price) * $ViaticsPropetyVo2->quantity));
                $this->arrayViatics["food"]["quantity"] += 1;
            } else if ($ViaticsPropetyVo2->typeViatic == "hospedaje") {
                $this->arrayViatics["hospedaje"]["price"] += (str_replace(",", ".", ($ViaticsPropetyVo2->price * ($ViaticsPropetyVo2->percent / 100) + $ViaticsPropetyVo2->price) * $ViaticsPropetyVo2->quantity));
                $this->arrayViatics["hospedaje"]["quantity"] += 1;
            } else if ($ViaticsPropetyVo2->typeViatic == "improvistos") {
                $this->arrayViatics["improvistos"]["price"] += (str_replace(",", ".", ($ViaticsPropetyVo2->price * ($ViaticsPropetyVo2->percent / 100) + $ViaticsPropetyVo2->price) * $ViaticsPropetyVo2->quantity));
                $this->arrayViatics["improvistos"]["quantity"] += 1;
            }
            $this->arrayViatics["total"] += (str_replace(",", ".", ($ViaticsPropetyVo2->price * ($ViaticsPropetyVo2->percent / 100) + $ViaticsPropetyVo2->price) * $ViaticsPropetyVo2->quantity));
        }
        $this->arrayViatics["transport"]["price"] = str_replace(",", ".", number_format($this->arrayViatics["transport"]["price"]));
        $this->arrayViatics["food"]["price"] = str_replace(",", ".", number_format($this->arrayViatics["food"]["price"]));
        $this->arrayViatics["hospedaje"]["price"] = str_replace(",", ".", number_format($this->arrayViatics["hospedaje"]["price"]));
        $this->arrayViatics["improvistos"]["price"] = str_replace(",", ".", number_format($this->arrayViatics["improvistos"]["price"]));
        $this->arrayViatics["total"] = str_replace(",", ".", number_format($this->arrayViatics["total"]));

        return $this->arrayViatics;
    }

    public function GetLengthImpost()
    {
        $arrayList = array();
        $personVo = new impostVo();
        $personVo->isList = true;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $personVo);
        print_r($generalDao->GetLength());
    }

}

$ajaxDataImpost = new AjaxDataViatics();
