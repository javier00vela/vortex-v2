<?php
require_once(Config::PATH . Config::CONTROLLER . 'general/AccessManager.php');
require_once(Config::PATH . Config::CONTROLLER . 'general/UtilJquery.php');
require_once(Config::PATH . Config::VIEW_HTML . 'general/comps/GeneralCtx.php');
require_once(Config::PATH . Config::VIEW_HTML . 'general/comps/GeneralLst.php');
require_once(Config::PATH . Config::VIEW_HTML . 'general/comps/GeneralWithDataLst.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralDao.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/PersonVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/CompanyVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/UserVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/RoleVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/VisitsVo.php');

class VisitsView
{
    private $optionInputForm;
    private $isUpdateUserForm;
    private $idDataInputForm;
    /*=============================================
                  Session Vars
    =============================================*/
    private $manageSession;
    /*=============================================
                  Template Vars
    =============================================*/
    private $utilJQ;
    private $pageTpl, $data, $core;
    /*=============================================
                Controls Vars HTML
    =============================================*/

    private $CompanyLst;
    private $ClientsLst;
    private $personIdRoleLst;
    

    /*=============================================
                Data Vars
    =============================================*/
    private $generalDao;


    function __construct($pageTpl, $data, $core)
    {
        $this->data = $data;
        $this->core = $core;
        $this->pageTpl = $pageTpl;

            $this->CreateComponents();
            $this->UseDataFromThePostback();
        
    }

    private function UseDataFromThePostback()
    {

        if (isset($_POST['optionInputForm'])) {
            $this->optionInputForm = $_POST['optionInputForm'];
            $this->idDataInputForm = $_POST['idDataInputForm'];
            $this->generalDao = new GeneralDao();
            switch ($this->optionInputForm) {
                case "1": {
                    $this->SetData();
                    break;
                }
                case "2": {
                    $this->DeleteData();
                    break;
                }
                case "4": {
                    $this->SetUpdate();
                    break;
                }
            }
            //echo '<script>window.location = "Users";</script>';
        }
        if (isset($_POST['isUpdateUserForm'])) {
            $this->isUpdateUserForm = $_POST['isUpdateUserForm'];
            if($this->isUpdateUserForm == "1"){
                $this->UpdateUser();
            }
        }
        $this->AssignDataToTpl();

    }

    private function CreateComponents()
    {

        $this->personIdRoleLst = new GeneralWithDataLst($this->GetUserPerson(), "personIdRoleLst", false,true,"--Usuarios--", true);
        $this->CompanyLst = new GeneralWithDataLst($this->GetCompanys(), "CompanyLst", false,true,"--Empresas--", true);
        $this->ClientsLst = new GeneralWithDataLst(array(), "ClientsLst", false,true,"--Clientes y/o prospecto--", true);


 


        $this->utilJQ = new UtilJquery("Visits");
    }


 function GetUserPerson()
    {
        $array = [];
        $userVo = new PersonVo();
        $userVo->isList = true;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $userVo);
        while ($userVo2 = $generalDao->GetVo($generalVo = clone $userVo)) {
            if($userVo2->idRole != 7 && $userVo2->idRole != 8 && $userVo2->idRole != 9){
                if($userVo2->state == 1 && $userVo2->idRole != 7 && $userVo2->idRole != 8 && $userVo2->idRole != 9 ){
                $array[] = htmlspecialchars($userVo2->id."_".$userVo2->names." ".$userVo2->lastNames);
                }
            }
           
        }
            return $array;
    }

     function GetCompanys()
    {
        $array = [];
        $companyVo = new CompanyVo();
        $companyVo->isList = true;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $companyVo);
        while ($companyVo2 = $generalDao->GetVo($generalVo = clone $companyVo)) {
            if($companyVo2->idRoleCompany != 1 && $companyVo2->idRoleCompany != 3 ){
                if($companyVo2->state){
                    $array[] = htmlspecialchars($companyVo2->id."_".$companyVo2->name);
                }
            }
           
        }
            return $array;
    }

    private function SetDataToVo()
    {
        $visitsVo = new VisitsVo();
        $visitsVo->id = $this->idDataInputForm;
        $visitsVo->idClient = $_POST["ClientsLst"];
        $visitsVo->idPerson = $_POST["personIdRoleLst"];
        $visitsVo->dateCreate = date('Y-m-d H:i:s');

        return $visitsVo;
    }

    private function SetData()
    {
        $visitsVo = $this->SetDataToVo();
        $visitsVo->id = null;
        $result = $this->generalDao->Set($generalVo = clone $visitsVo);
        if (isset($result)) {
            $message = 'MessageSwalBasic("Registrado!","Visita Registrada Correctamente","success");';
            $this->utilJQ->AddFunctionJavaScript($message);
        }

    }

    private function SetUpdate()
    {
        $result = $this->generalDao->Update($generalVo = clone $this->SetDataToVo());
        if (isset($result)) {
            $message = 'MessageSwalBasic("Modificado!","Usuario Modificado Correctamente","success");';
            $this->utilJQ->AddFunctionJavaScript($message);
        }
    }

    private function DeleteData()
    {
        $message = "";
        $personVo = new PersonVo();
        $personVo->id = $this->idDataInputForm;
        $personVo->idUpdateField = 14;
           $personVo->isList = false;
        $personVo->state = 0;
        $personVo->idSearchField = 0;
        $this->DeleteUser($personVo->id);
        if ($this->generalDao->UpdateByField($generalVo = clone $personVo)) {
            
        } 
        
         $message = 'MessageSwalBasic("Eliminado!","Registro Eliminado Correctamente","success");';
        $this->utilJQ->AddFunctionJavaScript($message);
    }


  

    

  

    private function AssignDataToTpl()
    {
       
        $this->data->assign('personIdRoleLst', $this->personIdRoleLst->paint());
        $this->data->assign('CompanyLst', $this->CompanyLst->paint());
        $this->data->assign('ClientsLst', $this->ClientsLst->paint());
         $this->data->assign('tempParameter', time());

        $this->data->assign('jquery', $this->utilJQ->PaintJQ());
        $this->SetDataToTpl();
    }

    private function SetDataToTpl()
    {
        echo $this->core->get($this->pageTpl, $this->data);
    }

}