"use strict";
var tbl;
var userToTbl;

function LoadTable() {
    RemoveActionModule(1);
    GetArrayFromDataModule(1);
    setTimeout(function() {
        var parameters = {
            "isActionFromAjax": true,
            "optionInputForm": 0,
            "idDataInputForm": 0,
            "listData": arrayResponse
        };
        tbl = $('#productTbl').DataTable({
            responsive: true,
            "drawCallback": function(settings) {
                $('[data-toggle="tooltip"]').tooltip()
            },
            language: {
                "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
            },
            "ajax": {
                url: "view/html/modules/inventory/AjaxDataInventory.php?v=2345",
                method: "POST",
                data: parameters
            }
        });
    }, 500);
}

function SendDataSessionAjax(word) {
    var parameters = {
        "isActionFromAjax": true,
        "optionInputForm": 9,
        "idDataInputForm": 0,
        "word": word
    }

    $.ajax ({
        url: "Inventory",
        type: 'post',
        data: parameters,
        success: function(data) {}

    });
}

/********************************LOAD COUNTRIES***********************/
function SetSelect2Countries() {
    $("#productCountryLst").select2({
        placeholder: "--País de Origen--"
    });
}

function GetElementsCountries() {
    var $select = $('#productCountryLst');
    $.getJSON('backend/DataJson/countries.json', function(data) {
        $.each(data.results, function(key, val) {
            $select.append("<option id='" + val.name + "'  value='" + val.name + "'>" + val.name + "</option>");
        })
    });
}



/********************************SERIAL EVENTS******************************/



function SerialData(id) {
    window.location = "Serial?id=" + id;
}

function CloneRow(id) {
    var parameters = {
        "isActionFromAjax": true,
        "optionInputForm": 4,
        "idDataInputForm": 0,
        "idRow": id
    };

    $.ajax({
        url: "view/html/modules/inventory/AjaxDataInventory.php",
        method: "POST",
        data: parameters,
        success: function(data) {
            MessageSwalRedirect("Duplicado!", "Producto Duplicado Correctamente", "success", "Inventory")
                // ToastShow("success" , "SERIAL ACTUALIZADO !!" ,'Se ha Actualizado el serial #'+row+' a "'+$("#camp"+row).val()+'.', "2000");
        }
    });
}


/*********************************CLICK EVENTS**************************/

$("#sendExcelTemplateBtn").click(function() {
    if ($(".file").val() == "") {
        MessageSwalBasic("Error", "Por favor seleccione un archivo excel a cargar", "error");
    } else {
        SetValueToInputText("optionInputTemplateForm", "6");
        $("[name=sendExcelTemplateBtn]").attr("disabled", "true");
        $("[name=sendExcelTemplateBtn]").html("<i class='fa fa-spin fa-refresh'></i> Cargando ");
        $("#excelTemplateForm").submit();
    }
})

function Events() {
    $("#productTypeCurrencyLst").attr("readonly", true);
    $("#productCompanyLst").on("change", OnChangeProvider);
}

function OnChangeProvider() {
    $("#productCompanyLst option:selected").each(function() {
        if (GetValueSelect("productCompanyLst") != '') {
            GetMoneyProvider($(this).val());
        } else {
            $('#productTypeCurrencyLst option[value="COP"]').attr("selected", true);
        }

    });
}

function GetMoneyProvider(provider) {

    var parameters = { "isActionFromAjax": true, "optionInputForm": 2, "idDataInputForm": provider };
    $.ajax({
        url: "view/html/modules/inventory/AjaxDataInventory.php",
        type: 'post',
        data: parameters,
        success: function(response) {
            SetOptionSelected(response);
        }
    })
}

function SetOptionSelected(response) {
    $('#productTypeCurrencyLst').val(response);
    setTimeout(() => {
        if (response == "COP") {
            $('#productPlaceLst option[value="Internacional"]').css({ "display": "none" });

        } else {
            $('#productPlaceLst option[value="Internacional"]').css({ "display": "block" });
        }
        // alert(response);
    }, 500);
}


function ChangeFormatPrices() {
    $("#productPriceUnitCtx  , #productFLETELst").keypress(function(event) {
        if ($("#productTypeCurrencyLst").val() == "EUR" || $("#productTypeCurrencyLst").val() == "USD") {
            if (event.charCode > 58 || event.charCode < 46 || event.charCode == 45) {
                return false;
            }
        } else {
            if (event.charCode > 58 || event.charCode < 46 || event.charCode == 45) {
                return false;
            }
        }
    });
}

function ValidatePriceUnit() {
    $("#productPriceUnitCtx ").change(function(event) {
        if ($("#productTypeCurrencyLst").val()) {
            var numberType = (new Intl.NumberFormat("de-DE", { style: "currency", currency: $("#productTypeCurrencyLst").val() }).format($(this).val()));
            numberType = numberType.replace("$", "");
            numberType = numberType.replace("€", "");
            numberType = numberType.replace("COP", "");
            console.log(numberType);
            if (!parseInt(numberType)) {
                numberType = "";
            }

            if ($("#productTypeCurrencyLst").val() == "EUR" || $("#productTypeCurrencyLst").val() == "USD") {
                numberType = numberType.replace(".", "");
            }


            if ($("#productTypeCurrencyLst").val() == "COP") {
                numberType = numberType.split(",")[0].toString().replace(".", "");
            }
            $(this).val(numberType);
        } else {
            $("#productFLETELst").val('');
            $("#productPriceUnitCtx").val('');
        }
    });
}

function ValidateFleteUnit() {
    $("#productFLETELst ").change(function(event) {
        if ($("#productTypeCurrencyLst").val()) {
            var numberType = (new Intl.NumberFormat("de-DE", { style: "currency", currency: $("#productTypeCurrencyLst").val() }).format($(this).val()));
            numberType = numberType.replace("$", "");
            numberType = numberType.replace("€", "");
            numberType = numberType.replace("COP", "");
            console.log(numberType);
            if (!parseInt(numberType)) {
                numberType = "";
            }

            if ($("#productTypeCurrencyLst").val() == "EUR" || $("#productTypeCurrencyLst").val() == "USD") {
                numberType = numberType.replace(".", "");
            }

            if ($("#productTypeCurrencyLst").val() == "COP") {
                numberType = numberType.split(",")[0].toString().replace(".", "");
            }
            $(this).val(numberType);
        } else {
            $("#productFLETELst").val('');
        }
    });
}

function AlterTableAjaxInventory(ind) {
    var parameters = {
        "isActionFromAjax": true,
        "optionInputForm": 6,
        "idDataInputForm": 0,
        "range": ind
    };
    tbl.destroy();
    tbl = $('#productTbl').DataTable({
        responsive: true,
        "drawCallback": function(settings) {
            MessageSwalWaiter("cargando... ", "Filtrando Busqueda");
        },
        "drawCallback": function(settings) {
            $(".swal2-container").remove();
            $('[data-toggle="tooltip"]').tooltip()
        },
        language: {
            "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
        },
        "ajax": {
            url: "view/html/modules/inventory/AjaxDataInventory.php",
            method: "POST",
            data: parameters
        }
    });
}

function AlterTableAjaxInventoryWord(ind) {
    RemoveActionModule(1);
    GetArrayFromDataModule(1);
    setTimeout(function() {
        var parameters = {
            "isActionFromAjax": true,
            "optionInputForm": 8,
            "idDataInputForm": 0,
            "listData": arrayResponse,
            "word": ind
        };
        tbl.destroy();
        tbl = $('#productTbl').DataTable({
            responsive: true,
            "drawCallback": function(settings) {
                MessageSwalWaiter("cargando... ", "Filtrando Busqueda");
            },
            "drawCallback": function(settings) {
                $(".swal2-container").remove();
                $('[data-toggle="tooltip"]').tooltip()
            },
            language: {
                "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
            },
            "ajax": {
                url: "view/html/modules/inventory/AjaxDataInventory.php",
                method: "POST",
                data: parameters
            }
        });
        SendDataSessionAjax(ind);
        $("html").css({ 'overflow': 'scroll' });
    }, 500);
}

function ChangeValeuFIlter() {
    $("#productListFilterLst ").change(function() {
        AlterTableAjaxInventory($(this).val());
        MessageSwalWaiter("cargando... ", "Filtrando Busqueda");
        setTimeout(function() { $(".swal2-container").remove(); }, 10000);
    });
}

function ChangeValeuFIlterClick() {
    $("#wordKeyBtn ").click(function() {
        AlterTableAjaxInventoryWord($("#wordKey").val());
    });
}

function ChangeValeuFIlterWord() {
    /*$("#wordKey").change(function(){
        AlterTableAjaxInventoryWord($(this).val()); 
    });*/
}


function PHPcookies() {
    $("#wordKey").val(" ");
    $("#wordKey").trigger("change");

}


function isTrueFlete() {
    $("#isTrueFlete").change(function() {
        if ($(this).val() == "SI") {
            $("#fleteHiddenW").removeClass("hidden");
            $("#productFLETELst").removeAttr("disabled");
            $("#productFLETELst").attr("value", "0");
            SetValueToInputText('productFLETELst', '0');
        } else {
            $("#fleteHiddenW").addClass("hidden");
            $("#productFLETELst").attr("disabled", "true");
            SetValueToInputText('productFLETELst', '0');
        }
    });
}


function LoadRolUserMenu() {
    var parameters = {
        "isActionFromAjax": true,
        "optionInputForm": 9,
        "idDataInputForm": 0
    };

    $.ajax({
        url: "view/html/modules/users/AjaxDataUser.php",
        method: "POST",
        data: parameters,
        success: function(data) {
            SetDataUser(data);
        }
    })
}

function SetDataUser(userAjax) {
    userToTbl = userAjax;
}


function ExecutePDF(){
    $("#PDF").click(function(e){
        var parameters = {
            "isActionFromAjax": true,
            "optionInputForm": 10,
            "idDataInputForm": 0,
            "description":CKEDITOR.instances['productDescriptionAtx'].getData()
        };
    
        $.ajax({
            url: "Inventory",
            method: "POST",
            data: parameters,
            beforeSend : function(){
                
            },
            success: function(data) {
                data = data.split("--split--")[1];
                var windowOpen = window.open("view/docs/pdf/TEST-" + data + ".pdf?v=" + Math.random(), '_blank');
                if (windowOpen) {
                    windowOpen.focus();
    
                } else {
                    MessageSwalBasic("VERIFICAR !!", "Debes habilitar permisos para abrir ventanas emergentes", "warning");
                }
            }
        })

        e.preventDefault();
    });
}

/******************************/

$(function() {
    // var ckeditor = document.getElementById("productDescriptionAtx");
    //ckeditor.value = "hola a todos";

    LoadRolUserMenu();
    $("#productFLETELst").attr("disabled", "false");
    $("#productFLETELst").attr("value", "0");
    $("#productFLETELst").val(" ");
    ChangeValeuFIlterWord();
    ChangeValeuFIlter();
    ExecutePDF();
    Events();
    isTrueFlete();
    ValidateFleteUnit();
    LoadExcelFile();
    setTimeout(function() { LoadTable() }, 500);
    GetElementsCountries();
    SetSelect2Countries();
    ChangeFormatPrices();
    ValidatePriceUnit();
    ChangeValeuFIlterClick();
    window.getCookie = function(name) {
        var match = document.cookie.match(new RegExp('(^| )' + name + '=([^;]+)'));
        if (match) $("#wordKey").val(match[2]);
    }

});