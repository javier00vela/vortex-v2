<div class="content-wrapper">
    <section class="content-header">

        <h1>
            Administrar Productos del Inventario
        </h1>
        <ol class="breadcrumb">
            <li><a href="Home"><i class="fa fa-dashboard"></i> Inicio</a></li>
            <li class="active">Inventario</li>
        </ol>
    </section>

    <section class="content">
        <div class="box">
            <div class="box-header with-border">
                <nav class="navbar navbar-light">
                    <div class="container-fluid">
                        <ul class="nav navbar-nav">
                            <li>
                                <a node="node-insertar" style="cursor: pointer;" id="productAddBtn" name="productAddBtn" data-toggle="modal" data-target="#productPopUpAdd" onclick="ResetControls('containerinputs');SetData();ApplyEditor('productDescriptionAtx','');">
                                    <i class="fa fa-plus-circle"></i> Agregar Producto al Inventario
                                </a>
                            </li>
                            <li>
                                <a node="node-insertar_excel" style="cursor: pointer;" id="productsAddFromExcelBtn" name="productsAddFromExcelBtn" data-toggle="modal" data-target="#modalAddTemplateExcel">
                                    <i class="fa fa-upload"></i> Agregar Productos desde plantilla de excel
                                </a>
                            </li>
                            <li>
                                <a style="cursor: pointer;" id="userAddBtn" name="userAddBtn" onclick="location.href = 'Home' ">
                                    <i class="fa fa-home"></i> Menu Principal
                                </a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-10 col-xs-12 hidden">
                        <div class="form-group">
                            <label>Seleccione Rango De Registros a Mostrar(<i class="fa fa-info red-tooltip"  data-toggle='tooltip' data-placement='top' title="Recuerde que el filtro esta sincronizado a la cantidad de filas que existen "></i>)</label>                            {$productListFilterLst}
                        </div>
                    </div>
                    <div class="col-sm-11 col-xs-12">

                        <label for="input">Palabra Clave a Buscar (<kdb class="red-tooltip"  data-toggle='tooltip' data-placement='top' title="Recuerde que el filtro esta sincronizado con el 'nombre' - ' marca ' - 'referencia' "><i class="fa fa-info"></i></kdb>)  </label>
                        <div class="input-group">
                            <input class="form-control" type="text" id="wordKey" placeholder="Agregar palabra clave de Busqueda" value="{$cookieResult}">
                            <span class="btn input-group-addon bg-blue" id="wordKeyBtn">Buscar</span>
                        </div>
                    </div>
                    <div class="col-sm-1  col-xs-12">
                        <div class="">
                            <label>&nbsp &nbsp &nbsp</label>
                            <button class="btn btn-danger btn-block btn-xs-block" id="btnDeleteBuffer" onclick="PHPcookies()"><i class="fa fa-times"></i> Borrar</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-body">
                <table id="productTbl" name="productTbl" class="table table-bordered table-striped dt-responsive tables dataTable no-footer dtr-inline" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Tipo de Producto</th>
                            <th>Nombre</th>
                            <th>Marca</th>
                            <th>Referencia</th>
                            <th>Cantidad Actual</th>
                            <th>Tipo de Moneda</th>
                            <th>Precio Unitario</th>
                            <th>Presentación</th>
                            <th>País</th>
                            <th>Proveedor</th>
                            <th>Lugar de Producto</th>
                            <th>precio Colombiano Aproximado</th>
                            <th>Aplica IVA</th>
                            <th>Precio Venta (DDP)</th>
                            <th>Aplica FLETE</th>
                            

                            <th>Acciones</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </section>
</div>
<!--=====================================
VENTANA MODAL AGREGAR PRODUCTO O MODIFICAR
======================================-->
<div node="node-insertar" id="productPopUpAdd" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form id="inventoryForm" role="form" method="post" enctype="multipart/form-data">
                <!--=====================================
              Hidden
              ======================================-->
                <input id="optionInputForm" name="optionInputForm" type="hidden" value="0">
                <input id="idDataInputForm" name="idDataInputForm" type="hidden" value="0">
                <input id="isActionFromAjax" name="isActionFromAjax" type="hidden" value="false">
                <!--=====================================
              CABEZA DEL MODAL
              ======================================-->
                <div class="modal-header" style="background:#3c8dbc; color:white">
                    <button type="button" class="close" data-dismiss="modal" data-target='#productPopUpAdd' onclick="ResetControls('productForm')">&times;</button>
                    <h4 class="modal-title">Agregar Producto</h4>
                </div>
                <!--=====================================
              CUERPO DEL MODAL
              ======================================-->
                <div class="modal-body" id="containerinputs">
                    <div class="box-body">
                        <div class="row">
                            <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-codepen"></i></span> {$productTypeProductLst}
                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-codepen"></i></span> {$productNameCtx}
                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-barcode"></i></span> {$productBrandCtx}
                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-barcode"></i></span> {$productReferenceCtx}
                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-cubes"></i></span> {$productPresentationCtx}
                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-cubes"></i></span> {$productAmountCtx}
                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-cubes"></i></span> {$productMinimumAmountCtx}
                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-industry"></i></span> {$productCompanyLst}
                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-dollar"></i></span> {$productTypeCurrencyLst}
                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-3 ">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-cube"></i></span>
                                    <select id="isTrueFlete" class="form-control">
                                 <option selected value="NO">No Aplicar Flete</option>
                                 <option value="SI">Aplicar Flete</option>
                             </select>
                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-3 hidden" id="fleteHiddenW">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-dollar"></i></span> {$productFLETELst}
                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-dollar"></i></span> {$productPriceUnitCtx}
                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-industry"></i></span> {$productPlaceLst}
                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-globe"></i></span>
                                    <select id="productCountryLst" name="productCountryLst" class="form-control select2 select2-hidden-accessible" style="width: 100%;" tabindex="-1" aria-hidden="true" required>
                                  <option value="" selected="">--País de Origen--</option>
                              </select> {*{$productCountryLst}*}
                                </div>
                            </div>



                            <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-percent"></i></span> {$productIVALst}
                                </div>
                            </div>




                        </div>
                        <div class="form-group">
                            <div class="input-group  col-xs-12">
                                <textarea class="ckeditor" id="productDescriptionAtx" name="productDescriptionAtx"></textarea>
                            </div>
                            <button id="PDF" class="bnt bnt-primary btn-block">Mostrar Descripción en PDF</button>
                        </div>

                    </div>
                </div>
                <!--=====================================
              PIE DEL MODAL
              ======================================-->
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal" data-target='#productPopUpAdd' onclick="ResetControls('product2Form')">Salir</button>
                    <button type="submit" class="btn btn-primary" name="sendData">Guardar Producto</button>
                </div>

            </form>

        </div>
    </div>
</div>
<!--==================================================================
=            MODAL AGREGAR PRODUCTO DESDE PLANTILLA DE EXCEL           =
======================================================================-->
<!-- Modal -->
<div node="node-insertar_excel" id="modalAddTemplateExcel" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">

            <form id="excelTemplateForm" name="ExcelTemplateForm" role="form" method="post" enctype="multipart/form-data">
                <input id="optionInputTemplateForm" name="optionInputTemplateForm" type="hidden" value="0">
                <div class="modal-header" style="background: #3c8dbc; color: white">
                    <button type="button" class="close txt-white" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Agregar Productos</h4>
                </div>
                <div class="modal-body">
                    <div class="alert bg-orange">Recuerda agregar un archivo excel(xlsx) con una cantidad entre 1 a 15000 registros con el fin de garantizar un uso optimo y rapido del funcionamiento de la carga de productos en el sistema,ademas puedes descargar la plantilla <a href="/var/www/html/vortex/view/docs/plantillas/productstemple.xlsx">Aquí</a>
                    </div>
                    <div class="alert bg-orange">Recuerda que los precios no deben tener ningun caracter ni puntos ni comas a <b>a excepción que el los precios tengas centecimas entonces se deben separar por puntos </b> , por ejemplo :'354.20' pesos es una manera correcta y no como
                        este ejemplo : '1.234,20' debido a que el sistema organiza el formato como corresponde según el tipo de moneda.
                    </div>
                    <input type="hidden" name="MAX_FILE_SIZE" value="2000000" />
                    <div class="form-group">
                        <label>Inserte una plantilla excel para cargar los datos :</label>
                        <input type="file" name="fileExcelTemplateInp" accept=".xlsx" class="file">
                        <div class="input-group col-12">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-folder-open"></i></span>
                            <input type="text" class="form-control input-lg" disabled placeholder="Sin Plantilla Excel">
                            <span class="input-group-btn">
                              <button class="browse btn btn-primary input-lg" type="button"><i class="fa fa-plus"></i></button>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button id="exitExcelTemplateBtn" type="button" class="btn btn-default pull-left" data-dismiss="modal" onclick="ResetControls('ExcelTemplateForm')">Cerrar</button>
                    <button id="sendExcelTemplateBtn" name="sendExcelTemplateBtn" type="button" class="importExcel btn btn-primary">Importar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!--/*=====  End of Section comment block  ======*/-->

<div id="containerResponseAjax"></div>
<script src="view/html/modules/inventory/Inventory.js?v= {$tempParameter}"></script>
{$jquery}