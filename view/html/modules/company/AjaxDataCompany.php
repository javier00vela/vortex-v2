<?php
require_once(__DIR__ . '/../../../../controller/general/Config.php');
require_once(Config::PATH . Config::CONTROLLER.Config::GENERAL . 'ManageArrays.php');
require_once(Config::PATH . Config::BACKEND . 'modules/CompanyVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/RoleCompanyVo.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralDao.php');

class AjaxDataCompany{

    private $isActionFromAjax;
    private $optionInputForm;
    private $idDataInputForm;
    public $dataList = [];

    function __construct()
    {
        if (isset($_POST['isActionFromAjax'])) {
            $this->isActionFromAjax = $_POST['isActionFromAjax'];
            $this->optionInputForm = $_POST['optionInputForm'];
            $this->idDataInputForm = $_POST['idDataInputForm'];
            if(isset($_POST["listData"])){
                $this->dataList = $_POST["listData"];  
            }
            if ($this->isActionFromAjax == "true") {
                if ($this->optionInputForm == 3) {
                    $this->SetDataInControls();
                }else if($this->optionInputForm == 5){
                    $this->GetTypeCompanyLenght($_POST["typeRol"]);
                }else if($this->optionInputForm == 7){
                    $this->GetAllCompanyLenght();
                }else {
                    $this->GetDataInJsonForTbl();
                }
            }
        }
    }

    private function SetDataInControls(){
        $SetInputsDataFJQ = "";
        $companyVo = new CompanyVo();
        $companyVo->id = $this->idDataInputForm;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $companyVo);
        if($companyVo2 = $generalDao->GetVo($generalVo = clone $companyVo)){
            $SetInputsDataFJQ = "
                            <script>
                               (function($){
                        SetValueToInputText('companyNameCtx','{$companyVo2->name}');
                        SetValueToInputText('companyNitCtx','{$companyVo2->nit}');
                        SetValueToInputText('companyPhoneCtx','{$companyVo2->phone}');
                        SetValueToInputText('companyEmailCtx','{$companyVo2->email}');
                        SetValueToInputText('companyWebSiteCtx','{$companyVo2->webSite}');
                        SetValueToInputText('companyAddressCtx','{$companyVo2->address}');
                        SetValueToInputText('companyNameHiddenCtx','{$companyVo2->name}');
                        
                        $('#companyCountryLst').select2('destroy'); 
                        SetValueToSelect('companyCountryLst','{$companyVo2->country}',true);
                        SetSelect2Countries();
                        
                        $('#companyCityLst').select2('destroy');
                        SetValueToInputText('companyCityLst','{$companyVo2->city}');
                        SetSelect2Cities();
                        
                        SetValueToInputText('companyCreationDateCtx','{$companyVo2->creationDate}');
                        SetValueToSelect('companyIdRoleLst','{$companyVo2->idRoleCompany}',true);
                        $('#'+'$companyVo->nameTable'+'PopUpAdd').modal('show');
                        SetValueToInputText('optionInputForm',4);
                        SetValueToInputText('idDataInputForm',$companyVo2->id);
                               })(jQuery);
                            </script>
                        ";
        }
        echo $SetInputsDataFJQ;
    }

    public function GetDataInJsonForTbl()
    {
        $existData = false;
        $companyVo = new CompanyVo();
        $companyVo->isList = true;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $companyVo);
        $buttoms = "";
        $string = "
        {\"data\": [";
        while ($companyVo2 = $generalDao->GetVo($generalVo = clone $companyVo)) {
            if($companyVo2->state == 1){
            $existData = true;
            $json = array();
            $json[] = $companyVo2->id;
            $json[] = htmlspecialchars($companyVo2->name);
            $json[] = htmlspecialchars($companyVo2->nit);
            $json[] = htmlspecialchars($companyVo2->phone);
            $json[] = htmlspecialchars($companyVo2->email);
            $json[] = htmlspecialchars($companyVo2->webSite);
            $json[] = htmlspecialchars($companyVo2->address);
            $json[] = htmlspecialchars(ucfirst($companyVo2->country));
            $json[] = htmlspecialchars($companyVo2->city);
            $json[] = $companyVo2->creationDate;
            $json[] = htmlspecialchars(ucwords(strtolower($this->GetRoleCompany($companyVo2->idRoleCompany))));
            if($companyVo2->id != 1 && $companyVo2->id != 0 ){
                if(!in_array( "editar_empresa" , $this->dataList)){
                $buttoms .= "<tip title='Modificar' class='yellow-tooltip' data-toggle='tooltip' data-placement='top'> <a id='updateBtn' name='updateBtn'  class='btn btn-warning btn-xs ' data-toggle='modal' data-target='#personPopUpAdd'  onclick='UpdateData({$companyVo2->id},\"company\",\"company\")'><i class='fa fa-pencil'></i></a></tip> ";
                }

                if(!in_array( "ver_contactos" , $this->dataList)){
                $buttoms .= "<a id='userHistoryAccessBtn' name='userHistoryAccessBtn' data-toggle='tooltip' data-placement='top' href='Contacts?idCompayInputForm=".$companyVo2->id."&idRole=".$companyVo2->idRoleCompany."' class='btn btn-primary btn-xs blue-tooltip' title='Contactos'><i class='fa fa-users'></i></a> ";
                }
                if(!in_array( "eliminar_empresa" , $this->dataList)){
                    $buttoms .=   "<a id='deleteBtn' name='deleteBtn' data-toggle='tooltip' data-placement='top' class='btn btn-danger  btn-xs red-tooltip' title='Eliminar' onclick='DeleteData({$companyVo2->id},\"company\")'><i class='fa fa-times'></i></a> ";
                }
                    if($companyVo2->idRoleCompany == 3){
                        if(!in_array( "modificar_precios" , $this->dataList)){
                        $buttoms .= " <a id='modyfyPricesBtn' name='modifyPricesBtn' data-toggle='tooltip' data-placement='top' class='btn bg-purple btn-xs purple-tooltip'  title='Modificar Precios' onclick='location.href=\"ModifyPrices?idCompany=$companyVo2->id\"' ><i class='fa fa-dollar'></i></a> ";
                        }
                    }
                
            }else{
                if(!in_array( "editar_empresa" , $this->dataList)){
                    $buttoms .= "<a id='updateBtn' name='updateBtn' data-toggle='tooltip' data-placement='top' class='btn btn-warning btn-xs yellow-tooltip' data-toggle='modal' data-target='#personPopUpAdd' title='Modificar' onclick='UpdateData({$companyVo2->id},\"company\",\"company\")'><i class='fa fa-pencil'></i></a> ";
                }
                if(!in_array( "ver_contactos" , $this->dataList)){
                    $buttoms .= "<a id='userHistoryAccessBtn' data-toggle='tooltip' data-placement='top' name='userHistoryAccessBtn' href='Contacts?idCompayInputForm=".$companyVo2->id."' class='btn btn-primary btn-xs blue-tooltip' title='Contactos'><i class='fa fa-users'></i></a> ";
                }
            }
            $json[] = $buttoms;
            $string .= json_encode($json) . ",";
            $buttoms = "";
            }
        }
        $string .= "-]}-";

        if($existData){
            echo  str_replace(",-]}-", "]}", $string);
        }else{
            echo "{ \"data\": [] }";
        }
    }

    function GetTypeCompanyLenght($idRole)
    {
        $arrayList =  array();
        $roleCompanyVo = new companyVo();
        $roleCompanyVo->idRoleCompany = $idRole;
        $roleCompanyVo->idSearchField = 10;
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $roleCompanyVo);
        print_r($generalDao->GetLength());
    }

    function GetTypeCompanyAll($idRole)
    {
        $arrayList =  array();
        $roleCompanyVo = new companyVo();
        $roleCompanyVo->isList = true;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $roleCompanyVo);
        print_r($generalDao->GetLength());
    }

    function GetAllCompanyLenght()
    {
        $arrayList =  array();
        $companyVo = new companyVo();
        $companyVo->isList = true;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $companyVo);
        while($companyVo2 = $generalDao->GetVo($generalVo = clone $companyVo)) {
            if($companyVo2->id != 0){
                $arrayList[] = $companyVo2->id;
            }
        }
        print_r(count($arrayList));
    }

    function GetRoleCompany($idRole)
    {
        $roleCompanyVo = new RoleCompanyVo();
        $roleCompanyVo->id = $idRole;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $roleCompanyVo);
        if ($roleCompanyVo2 = $generalDao->GetVo($generalVo = clone $roleCompanyVo)) {
            return "" . $roleCompanyVo2->name;
        }
        return "Problema en la consulta";
    }

}

$ajaxDataCompany = new AjaxDataCompany();
