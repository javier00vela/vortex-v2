// jshint unused:true
"use strict";

function LoadTable() {
    RemoveActionModule(3);
    GetArrayFromDataModule(3);
    setTimeout(function() {
        var parameters = {
            "isActionFromAjax": true,
            "optionInputForm": 0,
            "idDataInputForm": 0,
            "listData": arrayResponse
        };


        $('#companyTbl').DataTable({
            responsive: true,
            "drawCallback": function(settings) {
                $('[data-toggle="tooltip"]').tooltip()
            },
            language: {
                "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
            },
            "ajax": {
                url: "view/html/modules/company/AjaxDataCompany.php",
                method: "POST",
                data: parameters
            }
        });
    }, 500);
}
/********************************LOAD COUNTRIES***********************/
function SetSelect2Countries() {
    $("#companyCountryLst").select2({
        placeholder: "--País de Origen--"
    });
}

function GetElementsCountries() {
    var $select = $('#companyCountryLst');
    $.getJSON('backend/DataJson/countries.json', function(data) {
        $.each(data.results, function(key, val) {
            $select.append("<option id='" + val.name + "'    value='" + val.name + "'>" + val.name + "</option>");
        })
    });
}

function SetSelect2Cities() {
    $("#companyCityLst").select2({
        placeholder: "-- Ciudad --"
    });
}

function GetElementsCities() {
    var $select = $('#companyCityLst');
    $.getJSON('backend/DataJson/cities.json', function(data) {
        $.each(data.results, function(key, val) {
            $select.append("<option id='" + val.name + "'  value='" + val.name + "'>" + val.name + "</option>");
        })
    });
}
/***************************UPDATE STATE**************************/
function UpdateStateUser(idUser, state, userVo, idModule) {
    SetValueToInputText("optionInputForm", 5);
    SetValueToInputText("idDataInputForm", idUser);
    SetValueToInputText("isActionFromAjax", "true");
    var parameters = {
        "isActionFromAjax": GetValueToInputText("isActionFromAjax"),
        "optionInputForm": GetValueToInputText("optionInputForm"),
        "idDataInputForm": GetValueToInputText("idDataInputForm"),
        "state": state
    };
    //console.log(parameters);
    $.ajax({
        data: parameters,
        url: 'view/html/modules/users/AjaxData' + UpperFirstLetter(idModule) + '.php',
        type: 'POST',
        success: function(response) {
            $("#containerResponseAjax").html(response);
        }
    });
}
/***********************************************************************/

/*********************************CLICK EVENTS**************************/

$("#userFormBtn").click(function() {
    isUpdateUserForm
    SetValueToInputText("isUpdateUserForm", 1);
    $("#userForm").submit();
})

$("#sendExcelTemplateBtn").click(function() {
    if ($(".file").val() == "") {
        MessageSwalBasic("Error", "Por favor seleccione un archivo excel a cargar", "error");
    } else {
        SetValueToInputText("optionInputTemplateForm", "6");
        $("[name=sendExcelTemplateBtn]").attr("disabled", "true");
        $("[name=sendExcelTemplateBtn]").html("<i class='fa fa-spin fa-refresh'></i> Cargando ");
        $("#excelTemplateForm").submit();
    }
})

function CreateExcelDocs() {
    GetAllDataTableToExcel("Company", "view/docs/excel/ReportCompany.xlsx", "companyTbl");
}


$(function() {
    LoadTable();
    SetSelect2Countries();
    GetElementsCountries();
    SetSelect2Cities();
    GetElementsCities();
    LoadExcelFile();
});