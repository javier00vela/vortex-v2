<?php
require_once(Config::PATH . Config::CONTROLLER . 'general/AccessManager.php');
require_once(Config::PATH . Config::CONTROLLER . 'general/UtilJquery.php');
require_once(Config::PATH . Config::VIEW_HTML . 'general/comps/GeneralCtx.php');
require_once(Config::PATH . Config::VIEW_HTML . 'general/comps/GeneralLst.php');
require_once(Config::PATH . Config::VIEW_HTML . 'general/comps/GeneralWithDataLst.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralDao.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/QuotationVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/ProductVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/CompanyVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/RoleVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/QuotationVo.php');

class HistoryModifyQuotationMainteinceView
{
    private $optionInputForm;
    private $isUpdateUserForm;
    private $codeQuotationInputForm;
    private $codeQuotationFromGet;
    /*=============================================
                  Session Vars
    =============================================*/
    private $manageSession;
    /*=============================================
                  Template Vars
    =============================================*/
    private $utilJQ;
    private $pageTpl, $data, $core;
    /*=============================================
                Controls Vars HTML
    =============================================*/
 

    /*=============================================
                Data Vars
    =============================================*/
    private $generalDao;


    function __construct($pageTpl, $data, $core)
    {

        $this->data = $data;
        $this->core = $core;
        $this->pageTpl = $pageTpl;
            $this->CreateComponents();

        $this->UseDataFromGet();
        $this->AssignDataToTpl();
    }

     private function  UseDataFromGet(){
        $assign = '';
        if(isset($_GET["codeQuotationInputForm"])){
            
            $this->codeQuotationFromGet = $_GET["codeQuotationInputForm"];
                $assign = 'SetValueToInputText("codeQuotationInputForm",'.$this->codeQuotationFromGet.');';
          
        }
       
        $this->utilJQ->AddFunctionJavaScript($assign);
          
    }


    private function CreateComponents()
    {
         $this->codeQuotationInputForm = new GeneralCtx("codeQuotationInputForm","","hidden",false);
        $this->utilJQ = new UtilJquery("HistoryModifyQuotation");
       
    }

    private function AssignDataToTpl()
    {     
        $this->data->assign('codeQuotationInputForm', $this->codeQuotationInputForm->paint());
        $this->data->assign('jquery', $this->utilJQ->PaintJQ());   
         $this->data->assign('tempParameter', time()); 
        $this->SetDataToTpl();
    }   

    private function SetDataToTpl()
    {

        echo $this->core->get($this->pageTpl, $this->data);
        
    }

}   