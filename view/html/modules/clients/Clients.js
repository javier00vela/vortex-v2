// jshint unused:true
"use strict";


function LoadTable() {
    RemoveActionModule(22);
    GetArrayFromDataModule(22);
    setTimeout(function() {
        var parameters = {
            "isActionFromAjax": true,
            "optionInputForm": 0,
            "idDataInputForm": 0,
            "idCompayInputForm": $("#idCompayInputForm").attr("value"),
            "listData": arrayResponse,
        };


        $('#userTbl').DataTable({
            responsive: true,
            "drawCallback": function(settings) {
                $('[data-toggle="tooltip"]').tooltip()
            },
            language: {
                "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
            },
            "ajax": {
                url: "view/html/modules/clients/AjaxDataClients.php",
                method: "POST",
                data: parameters
            }
        });
    }, 500);
}

function ControlsDatePikers() {
    $('#personBirthdayCtx').datepicker({
        autoclose: true
    });
}

/***************************UPDATE STATE**************************/
function UpdateStateUser(idUser, state, userVo, idModule) {
    SetValueToInputText("optionInputForm", 5);
    SetValueToInputText("idDataInputForm", idUser);
    SetValueToInputText("isActionFromAjax", "true");
    var parameters = {
        "isActionFromAjax": GetValueToInputText("isActionFromAjax"),
        "optionInputForm": GetValueToInputText("optionInputForm"),
        "idDataInputForm": GetValueToInputText("idDataInputForm"),
        "state": state
    };
    //console.log(parameters);
    $.ajax({
        data: parameters,
        url: 'view/html/modules/users/AjaxData' + UpperFirstLetter(idModule) + '.php',
        type: 'POST',
        success: function(response) {
            $("#containerResponseAjax").html(response);
        }
    });


    if (state === 0) {
        userVo["state"] = "0";
        userVo = JSON.stringify(userVo);
        $("#activateBtn" + idUser).removeClass('btn-success');
        $("#activateBtn" + idUser).addClass('btn-danger');
        $("#activateBtn" + idUser).html('<i class=\'fa fa-thumbs-up\'></i>');
        $("#activateBtn" + idUser).attr('value', 0);
        $("#activateBtn" + idUser).attr('onclick', "UpdateStateUser(" + idUser + ",1," + userVo + ",'user')");
    } else if (state === 1) {
        userVo["state"] = "1";
        userVo = JSON.stringify(userVo);
        $("#activateBtn" + idUser).removeClass('btn-danger');
        $("#activateBtn" + idUser).addClass('btn-success');
        $("#activateBtn" + idUser).html('<i class=\'fa fa-thumbs-up\'></i>');
        $("#activateBtn" + idUser).attr('value', 1);
        $("#activateBtn" + idUser).attr('onclick', "UpdateStateUser(" + idUser + ",0," + userVo + ",'user')");
    }
    UpdateCredentials(userVo);
}

function UpdateCredentials(userVo) {
    if (userVo['id'] === undefined) {
        userVo = JSON.parse(userVo);
    }
    SetValueToInputText("userIdInputForm", userVo['id']);
    SetValueToInputText("userNicknameCtx", userVo['nickname']);
    SetValueToInputText("userPasswordCtx", userVo['password']);
    SetValueToInputText("userStateInputForm", GetValueToInputText("activateBtn" + userVo['id']));
    $("#userPhotoImg").attr('src', userVo['photo']);
    SetValueToInputText("userIdPersonInputForm", userVo['idPerson']);
}

/***********************************************************************/

/*********************************CLICK EVENTS**************************/

$("#userFormBtn").click(function() {
        SetValueToInputText("isUpdateUserForm", 1);
        $("#userForm").submit();
    })
    /*******************************CHANGE EVENTS*************************************/
$("#userPhotoUploadFile").change(function() {
    var img = this.files[0];
    if (img["type"] !== "image/jpeg" /*&& img["type"] !== "image/png"*/ ) {
        $("#userPhotoUploadFile").val("");
        MessageSwalBasic("Error al Subir Imagen", "Los Tipos de Imagen Permitidos son: JPEG", "error");
    } else if (img["type"] > 27000000) {
        $("#userPhotoUploadFile").val("");
        MessageSwalBasic("Error al Subir Imagen", "¡La Imagen no Debe Pesar mas de 50MB", "error");
    } else {
        var dataImg = new FileReader;
        dataImg.readAsDataURL(img);
        $(dataImg).on("load", function(event) {
            var urlImg = event.target.result;
            $("#userPhotoImg").attr("src", urlImg);
        })
    }
})


function ValidateRolCompany() {
    if ($("#personIdCompanyHidden").val() == 1) {
        $("#userAddBtn").remove();
    }
}


function CreateExcelDocs() {
    GetAllDataTableToExcel("Contacs", "view/docs/excel/ReportContacts.xlsx", "userTbl");
}

function RemoveMainCompany() {
    $("#companyLst option").each(function() {
        if ($(this).val() == 1) {
            $(this).remove();
        }
    });
}


/*****************************************************************************
                CLICKS EVENTS
*******************************************************************************/

/*********************************CLICK EVENTS**************************/

$("#sendExcelTemplateBtn").click(function() {
    if ($(".file").val() == "") {
        MessageSwalBasic("Error", "Por favor seleccione un archivo excel a cargar", "error");
    } else {
        //alert($("#fileExcelTemplateInp").val());
        SetValueToInputText("optionInputTemplateForm", "1");
        $("#excelTemplateForm").submit();
    }
})


/******************************************************************************/
$(function() {
    ControlsDatePikers();
    LoadTable();
    ValidateRolCompany();
    RemoveMainCompany();
});