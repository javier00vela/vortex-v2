<?php
require_once(__DIR__ . '/../../../../controller/general/Config.php');
require_once(Config::PATH . Config::CONTROLLER.Config::GENERAL . 'ManageArrays.php');
require_once(Config::PATH . Config::BACKEND . 'modules/PersonVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/CompanyVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/UserVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/RoleVo.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralDao.php');
require_once(Config::PATH . Config::BACKEND . 'modules/ClientUserVo.php');

class AjaxDataProspect
{

    private $isActionFromAjax;
    private $optionInputForm;
    private $idDataInputForm;
    private $idCompayInputForm;
    public $dataList = [];

    function __construct()
    {
        if (isset($_POST['isActionFromAjax'])) {
            $this->isActionFromAjax = $_POST['isActionFromAjax'];
            $this->optionInputForm = $_POST['optionInputForm'];
            $this->idDataInputForm = $_POST['idDataInputForm'];
            if(isset($_POST["listData"])){
                $this->dataList = $_POST["listData"];  
            }

            if ($this->isActionFromAjax == "true") {
                if ($this->optionInputForm == 3) {
                    $this->SetDataInControls();
                }else if ($this->optionInputForm == 5) {
                    $this->UpdateStateUser();
                }else if ($this->optionInputForm == 6) {
                    $this->GetTypeClientLenght();
                }else {
                    $this->GetDataInJsonForTbl();
                }
            }
        }
    }

    public function SetDataInControls()
    {
        $SetInputsDataFJQ = "";
        $personVo = new PersonVo();
        $personVo->id = $this->idDataInputForm;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $personVo);
        if ($personVo2 = $generalDao->GetVo($generalVo = clone $personVo)) {
            $dataUserClient = ($this->GetClientUserById($personVo2->id) != null ) ? $this->GetClientUserById($personVo2->id) : null;
            $SetInputsDataFJQ = "
                  <script>
                    (function($){
                                SetValueToInputText('personNamesCtx','{$personVo2->names}');
                                SetValueToInputText('personLastNamesCtx','{$personVo2->lastNames}');
                                SetValueToSelect('personDocumentTypeLst','{$personVo2->documentType}',true);
                                SetValueToInputText('personDocumentCtx','{$personVo2->document}');
                                SetValueToSelect('companyLst','{$personVo2->idCompany}',true);
                                SetValueToInputText('personCellPhoneCtx','{$personVo2->cellPhone}');
                                SetValueToInputText('personTelephoneCtx','{$personVo2->telephone}');
                                SetValueToInputText('personEmailCtx','{$personVo2->email}');
                                SetValueToInputText('personAddressCtx','{$personVo2->address}');
                                SetValueToInputText('personCityCtx','{$personVo2->city}');
                                SetValueToInputText('personUserLst','{$dataUserClient->idUser}');
                                SetValueToInputText('personPrefijoLst','{$dataUserClient->prefijo}');
                                SetValueToInputText('personIdRoleHidden','{$personVo2->idRole}');
                                SetValueToInputText('personIdCompanyHidden','{$personVo2->idCompany}');
                                


                                SetTextToButton('btnContacts','Actualizar Contacto');
                                $('#'+'person'+'PopUpAdd').on('hidden.bs.modal', function (e) {
                                    SetTextToButton('btnContacts','Guardar Contacto');
                                 })
                                
                                $('#'+'user'+'PopUpAdd').modal('show');
                                SetValueToInputText('optionInputForm',4);
                                SetValueToInputText('idDataInputForm',$personVo2->id);
                      })(jQuery);
                  </script>
                            ";
        }
        echo $SetInputsDataFJQ;
    }

    public function GetDataInJsonForTbl()
    {
        $existData = false;
        $personVo = new PersonVo();
        $personVo->isList = true;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $personVo);

        $string = "
        {\"data\": [";
        while ($personVo2 = $generalDao->GetVo($generalVo = clone $personVo)) {
            $json = array();
            if($personVo2->state == 1 ){
                if($personVo2->idRole == 7){
                    if($this->GetStateCompany($personVo2->idCompany)){
            $existData = true;
            $json[] = $personVo2->id;
            $json[] = htmlspecialchars(ucwords(strtolower($personVo2->names)));
            $json[] = htmlspecialchars(ucwords(strtolower($personVo2->lastNames)));
            $json[] = htmlspecialchars($personVo2->documentType);
            $json[] = htmlspecialchars($personVo2->document);
            $json[] = htmlspecialchars($personVo2->cellPhone);
            $json[] = htmlspecialchars($personVo2->telephone);
            $json[] = htmlspecialchars($personVo2->email);
            $json[] = htmlspecialchars($personVo2->address);
            $json[] = htmlspecialchars($personVo2->city);
            $json[] = $personVo2->creationDate;
            $json[] = $this->GetTypeRole($personVo2->idRole);
            $json[] = $this->GetCompany($personVo2->idCompany);
            $json[] = ($this->GetClientUserById($personVo2->id) != null ) ? $this->GetPersonById($this->GetClientUserById($personVo2->id)->idUser) : "<p style='color:red'>Sin Asignar</p>";

            $button = "";
            if(!in_array( "modificar_cliente" , $this->dataList)){
                $button .= "<tip title='Modificar' class='yellow-tooltip' data-toggle='tooltip' data-placement='top'><a id='updateBtn' name='updateBtn' class='btn btn-warning btn-xs' data-toggle='modal' data-target='#personPopUpAdd' title='Modificar' onclick='UpdateData({$personVo2->id},\"clients\",\"clients\")'><i class='fa fa-pencil'></i></a></tip> ";
            }
            if(!in_array( "eliminar_cliente" , $this->dataList)){
                $button .= " <tip title='Eliminar' class='red-tooltip' data-toggle='tooltip' data-placement='top'><a id='deleteBtn' name='deleteBtn' class='btn btn-danger  btn-xs'  onclick='DeleteData({$personVo2->id},\"person\")'><i class='fa fa-times'></i></a></tip>";
            }
            $json[] = $button;
        
        $string .= json_encode($json) . ",";
        }
        }
    }
}
        $string .= "-]}-";
        if($existData){
            echo  str_replace(",-]}-", "]}", $string);
        }else{
            echo "{ \"data\": [] }";
        }
    }

    function GetTypeRole($idRole)
    {
        $roleVo = new RoleVo();
        $roleVo->id = $idRole;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $roleVo);
        if ($roleVo2 = $generalDao->GetVo($generalVo = clone $roleVo)) {
            return "" . $roleVo2->name;
        }
        return "Problema en la consulta";
    }

    
    function GetClientUserById($idClient){

        $ClientUserVo = new ClientUserVo();
        $ClientUserVo->idSearchField = 2;
        $ClientUserVo->idClient = $idClient;
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $ClientUserVo);
        if ($ClientUserVo2 = $generalDao->GetVo($generalVo = clone $ClientUserVo)) {
            return $ClientUserVo2;
        }
        return null;
    }



      function GetPersonById($idPerson)
    {
        $nameUser="";
         $personVo= new PersonVo();
         $personVo->idSearchField = 0;
         $personVo->id = $idPerson;
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $personVo);
        if ($personVo2 = $generalDao->GetVo($generalVo = clone $personVo)) {
            return htmlspecialchars($personVo2->names. ' '.$personVo2->lastNames) ;
        }
        return " ";

    }




    function GetCompany($idCompany)
    {
        $companyVo = new CompanyVo();
        $companyVo->id = $idCompany;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $companyVo);
        if ($companyVo2 = $generalDao->GetVo($generalVo = clone $companyVo)) {
            return "" . htmlspecialchars($companyVo2->name);
        }
        return "Problema en la consulta";
    }


    function GetStateCompany($idCompany)
    {
        $companyVo = new CompanyVo();
        $companyVo->id = $idCompany;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $companyVo);
        if ($companyVo2 = $generalDao->GetVo($generalVo = clone $companyVo)) {
            return $companyVo2->state;
        }
        return "Problema en la consulta";
    }



    function GetUserVo($idPerson)
    {
        $userVo = new UserVo();
        $userVo->idPerson = $idPerson;
        $userVo->idSearchField = 5;
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $userVo);
        if ($userVo2 = $generalDao->GetVo($generalVo = clone $userVo)) {
            return $userVo2;
        }
    }

    function GetTypeClientLenght()
    {
        $arrayList =  array();
        $PersonVo = new PersonVo();
        $generalDao = new GeneralDao();
        $generalDao->CustomQuery("SELECT * FROM {$PersonVo->nameTable} WHERE idRole='7' and state='1'");
        print_r($generalDao->GetLength());
    }


    function ValidateExistCompany($company)
    {
        $companyVo = new CompanyVo();
        $companyVo->id = $company ;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $companyVo);
        while($companyVo2 = $generalDao->GetVo($generalVo = clone $companyVo)) {
            if(isset($companyVo2->id)){
            return true;
            }
        }
        return false;
    }



    function UpdateStateUser()
    {
        $userVo = new UserVo();
        $userVo->id = $this->idDataInputForm;
        $userVo->idSearchField = 0;
        $userVo->state = $_POST["state"];
        $userVo->idUpdateField = 3;
        $generalDao = new GeneralDao();
        $generalDao->UpdateByField($generalVo = clone $userVo);
    }
}

$ajaxDataContact = new AjaxDataProspect();
