<?php
require_once(Config::PATH . Config::CONTROLLER . 'general/AccessManager.php');
require_once(Config::PATH . Config::CONTROLLER . 'general/UtilJquery.php');
require_once(Config::PATH . Config::CONTROLLER . 'general/ManageSession.php');
require_once(Config::PATH . Config::VIEW_HTML . 'general/comps/GeneralCtx.php');
require_once(Config::PATH . Config::VIEW_HTML . 'general/comps/GeneralLst.php');
require_once(Config::PATH . Config::VIEW_HTML . 'general/comps/GeneralWithDataLst.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralDao.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/ImpostVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/PhotosDiagnosisVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/DiagnosisVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/CompanyVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/ProductVo.php');
require_once(Config::PATH . Config::CONTROLLER_LIB .'vendor/dompdf/dompdf/src/Autoloader.php');

use Dompdf\Dompdf;
class ListDiagnosisView
{
    private $optionInputForm;
    private $isUpdateUserForm;
    private $idDataInputForm;
    /*=============================================
                  Session Vars
    =============================================*/
    private $manageSession;
    /*=============================================
                  Template Vars
    =============================================*/
    private $utilJQ;
    private $pageTpl, $data, $core;
    /*=============================================
                Controls Vars HTML
    =============================================*/
    private $impostNameCtx;
    private $impostPercentCtx;

    /*=============================================
                Data Vars
    =============================================*/
    private $generalDao;


    function __construct($pageTpl, $data, $core)
    {
        $this->data = $data;
        $this->core = $core;
        $this->pageTpl = $pageTpl;

            $this->CreateComponents();
            $this->UseDataFromThePostback();
        
    }

    private function UseDataFromThePostback()
    {
        if (isset($_POST['optionInputForm'])) {
            $this->optionInputForm = $_POST['optionInputForm'];
            $this->idDataInputForm = $_POST['idDataInputForm'];
            $this->generalDao = new GeneralDao();
            switch ($this->optionInputForm) {
                case "1": {
                    $this->SetData();
                    break;
                }
                case "2": {
                    $this->DeleteData();
                    break;
                }
                case "3": {
                    $this->PrintDataDiagnosis($_POST["id"]);
                    break;
                }
                case "4": {
                    $this->SetUpdate();
                    break;
                }
            }
        }

        $this->AssignDataToTpl();

    }

    private function CreateComponents()
    {
        $this->impostNameCtx = new GeneralCtx("impostNameCtx","Nombre",null,true);
        $this->impostPercentCtx = new GeneralCtx("impostPercentCtx","Porcentaje","number",true);
       
        $this->utilJQ = new UtilJquery("Impost");
    }
    private function SetDataToVo()
    {
        $impostVo = new ImpostVo();
        $impostVo->id = $this->idDataInputForm;
        $impostVo->name = $_POST["impostNameCtx"];
        $impostVo->percent = $_POST["impostPercentCtx"];
       
        return $impostVo;
    }
    private function SetData()
    {
        $impostVo = $this->SetDataToVo();
        $impostVo->id = null;
        $result = $this->generalDao->Set($generalVo = clone $impostVo);
        if (isset($result)) {
            $message = 'MessageSwalBasic("Registrado!","Impuesto Registrado Correctamente","success");';
            $this->utilJQ->AddFunctionJavaScript($message);
        }

    }
    private function SetUpdate()
    {
        $result = $this->generalDao->Update($generalVo = clone $this->SetDataToVo());
        if (isset($result)) {
            $message = 'MessageSwalBasic("Modificado!","Impuesto Modificado Correctamente","success");';
            $this->utilJQ->AddFunctionJavaScript($message);
        }
    }
    private function DeleteData()
    {
        $message = "";
        $impostVo = new ImpostVo();
        $impostVo->id = $this->idDataInputForm;
        if ($this->generalDao->Delete($generalVo = clone $impostVo)) {
            $message = 'MessageSwalBasic("Eliminado!","Impuesto Eliminado Correctamente","success");';
        } else {
            $message = 'MessageSwalBasic("Error!","Problema con la base de datos","error");';
        }
        $this->utilJQ->AddFunctionJavaScript($message);
    }

    public function PrintDataDiagnosis($idDiagnosis){
        $dompdf = new DOMPDF();  
        $content =  $this->SetDataContextHTML($idDiagnosis);
        $dompdf->loadHtml($content);
        $dompdf->setPaper('A3', 'letter');
        $dompdf->set_option('isRemoteEnabled', TRUE);
        $dompdf->render();
        $pdf = $dompdf->output();  
        print_r("--split--Diagnosis_".$idDiagnosis."--split--");
       file_put_contents("view/docs/pdf/Diagnosis_".$idDiagnosis.".pdf", $pdf);
    }

    public function SetDataContextHTML($idQuote){
        $html = '';
        $html =  file_get_contents("view/html/modules/listdiagnosis/templates/diagnosis.html");
        $html = $this->ReplaceActionsCompany(1 , $html);
        $html = $this->ReplaceDiagnosis($html);
        $html = $this->ReplaceProduct($html);
        return $html;
    }

    public function GetCompanyById($id){
        $CompanyVo = new CompanyVo();
        $CompanyVo->id = $id;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $CompanyVo);
        if ($CompanyVo2 = $generalDao->GetVo($generalVo = clone $CompanyVo)) {
            return [$CompanyVo2->name , $CompanyVo2->nit , $CompanyVo2->phone ,  $CompanyVo2->address , $CompanyVo2->country , $CompanyVo2->city ];
        }
        return ["Vortex Company SAS"];
    }

    public function GetPhotosById($id){
        $DiagnosisVo = new PhotosDiagnosisVo();
        $DiagnosisVo->idDiagnosis = $id;
        $DiagnosisVo->idSearchField = 4;
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $DiagnosisVo);
        if ($DiagnosisVo2 = $generalDao->GetVo($generalVo = clone $DiagnosisVo)) {
           return $DiagnosisVo2;
        }
        return false;
    }

    public function GetDiagnosticoById($id){
        $DiagnosisVo = new DiagnosisVo();
        $DiagnosisVo->id = $id;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $DiagnosisVo);
        if ($DiagnosisVo2 = $generalDao->GetVo($generalVo = clone $DiagnosisVo)) {
            if ($DiagnosisVo2->idStatusMachine == "1") {
                $tStado = "Mantenimiento Preventivo";
            } else if ($DiagnosisVo2->idStatusMachine == "2") {
                $tStado = "Funcional Daño Menor";
            } else {
                $tStado = "Mantenimiento Correctivo";
            }
            $foto = $this->GetPhotosById($DiagnosisVo2->id);
            return [$DiagnosisVo2->machineCommentState , $DiagnosisVo2->machineObservation , $DiagnosisVo2->commentClient , $tStado, $DiagnosisVo2->id , date("Y-m-d") ,  $DiagnosisVo2->firmClient,  $DiagnosisVo2->firmMaintenance , $foto->photo1,$foto->photo2 ,$foto->photo3  ];
        }
        return ["Vortex Company SAS"];
    }

    public function GetProductById($id){
        $ProductVo = new ProductVo();
        $ProductVo->id = $id;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $ProductVo);
        if ($ProductVo2 = $generalDao->GetVo($generalVo = clone $ProductVo)) {
            return [$ProductVo2->name , $ProductVo2->reference , $ProductVo2->brand ,  $ProductVo2->presentation , $ProductVo2->typeProduct , $ProductVo2->serial ];
        }
        return ["Vortex Company SAS"];
    }

    private function ReplaceProduct($text){
        $arrayToReplace = [];
        $arrayToReplace = $this->GetProductById($_POST["idProduct"]);
        $arrayReplace = ["#nombre#","#referencia#","#marca#","#presentacion#","#Tproducto#","#serial#"];
        for ($i=0; $i <  count($arrayReplace); $i++) { 
           $text = str_replace($arrayReplace[$i] , $arrayToReplace[$i]  , $text);
            
        }
        return $text;
    }

    private function ReplaceDiagnosis($text){
        $arrayToReplace = [];
        $arrayToReplace = $this->GetDiagnosticoById($_POST["id"]);
        $arrayReplace = ["#estadoActual#" , "#observacionEquipo#" , "#comentarioMaquina#","#estadoMaquina#","#Diagnostico#","#Fcreacion#","#firmaCliente#","#firmaUsuario#","#foto1#","#foto2#","#foto3#"];
        for ($i=0; $i <  count($arrayReplace); $i++) { 
           $text = str_replace($arrayReplace[$i] , $arrayToReplace[$i]  , $text);
            
        }
        return $text;
    }

    private function ReplaceActionsCompany($companyId , $text){
        $arrayToReplace = [];
        $arrayToReplace = $this->GetCompanyById($companyId);
        $arrayReplace = ["#NameCompany#" , "#nit#" , "#telefono#","#direccion#","#pais#","#ciudad#"];
        for ($i=0; $i <  count($arrayReplace); $i++) { 
           $text = str_replace($arrayReplace[$i] , $arrayToReplace[$i]  , $text);
            
        }
        return $text;
    }



    private function AssignDataToTpl()
    {
        $this->data->assign('impostNameCtx', $this->impostNameCtx->paint());
        $this->data->assign('impostPercentCtx', $this->impostPercentCtx->paint());

        $this->data->assign('jquery', $this->utilJQ->PaintJQ());
         $this->data->assign('tempParameter', time());
        $this->SetDataToTpl();
    }   
    private function SetDataToTpl()
    {
        echo $this->core->get($this->pageTpl, $this->data);
    }
  


}   