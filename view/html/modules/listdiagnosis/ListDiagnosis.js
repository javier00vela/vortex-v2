// jshint unused:true
"use strict";

function LoadTable() {
    RemoveActionModule(28);
    GetArrayFromDataModule(28);
    setTimeout(function () {
        var parameters = {
            "isActionFromAjax": true,
            "optionInputForm": 0,
            "idDataInputForm": 0,
            "listData": arrayResponse,
        };

        $('#impostTbl').DataTable({
            responsive: true,
            "drawCallback": function (settings) {
                $('[data-toggle="tooltip"]').tooltip()
            },
            language: {
                "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
            },
            "ajax": {
                url: "view/html/modules/listdiagnosis/AjaxDataListDiagnosis.php",
                method: "POST",
                data: parameters
            }
        });
    }, 500);
}


function PrintDataDiagnosis(id, idProduct) {
    var parameters = {
        "isActionFromAjax": true,
        "optionInputForm": 3,
        "idDataInputForm": 0,
        "id": id,
        "idProduct": idProduct
    }
    console.log(parameters);

    $.ajax({
        url: "ListDiagnosis?=" + id,
        type: 'post',
        data: parameters,

        beforeSend: function () {

            setTimeout(MessageSwalWaiter('cargando...', 'Abriendo Diagnosis!'), 10000);
        },
        success: function (data) {
            var windowOpen = window.open("view/docs/pdf/Diagnosis_" + id + ".pdf?v=" + Math.random(), '_blank');
            if (windowOpen) {
                windowOpen.focus();
                MessageSwalRedirect("GENERADO !!", "Se ha abierto el documento de Orden De Compra ", "success", "BuyListQuotes");

            } else {
                MessageSwalBasic("VERIFICAR !!", "Debes habilitar permisos para abrir ventanas emergentes", "warning");
            }
        }

    });
}

/*********************************CLICK EVENTS**************************/

$(function () {
    LoadTable();

});
