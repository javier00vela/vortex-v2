<?php
require_once(__DIR__ . '/../../../../controller/general/Config.php');
require_once(Config::PATH . Config::CONTROLLER.Config::GENERAL . 'ManageArrays.php');
require_once(Config::PATH . Config::BACKEND . 'modules/RoleVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/ModuleVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/EventsVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/ModuleEventVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/ModuleRoleVo.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralDao.php');


class AjaxDataModules{

    private $isActionFromAjax;
    private $optionInputForm;
    private $idDataInputForm;

    private $idTemp;

    function __construct()
    {
        if (isset($_POST['isActionFromAjax'])) {
            $this->isActionFromAjax = $_POST['isActionFromAjax'];
            $this->optionInputForm = $_POST['optionInputForm'];
            $this->idDataInputForm = $_POST['idDataInputForm'];

            if ($this->isActionFromAjax == "true") {
                if ($this->optionInputForm == 3) {
                    $this->SetDataInControls();
            }else if($this->optionInputForm == 5){
                $this->GetRoles();
            }else if($this->optionInputForm == 7){
                $this->idTemp = $_POST["idRol"];
                $this->GetRolesById();
            }else if($this->optionInputForm == 9){
                 $this->idTemp = $_POST["id"];
                  $this->stateTemp = $_POST["state"];
                $this->SetStateModules();
            }else if($this->optionInputForm == 10){
                $this->idTemp = $_POST["id"];
                  $this->stateTemp = $_POST["state"];
                $this->UpdateTypeNotifications();
            }else {
                    $this->GetDataInJsonForTbl();
                }
            }
        }
    }

    private function SetDataInControls(){
        $SetInputsDataFJQ = "";
        $impostVo = new ImpostVo();
        $impostVo->id = $this->idDataInputForm;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $impostVo);
        if($impostVo2 = $generalDao->GetVo($generalVo = clone $impostVo)){
            $SetInputsDataFJQ = "
                            <script>
                               (function($){
                                SetValueToInputText('impostNameCtx','{$impostVo2->name}');
                                SetValueToInputText('impostPercentCtx','{$impostVo2->percent}');
                                SetValueToInputText('optionInputForm',4);
                                SetValueToInputText('idDataInputForm',$impostVo2->id);
                               })(jQuery);
                            </script>
                        ";
        }
        echo $SetInputsDataFJQ;
    }

    public function GetRoles()
    {
        $rolesVo = new RoleVo();
        $rolesVo->isList = true;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $rolesVo);
        while ($rolesVo2 = $generalDao->GetVo($generalVo = clone $rolesVo)) {
            $arrayList[] =$rolesVo2 ;
        }
        print_r( json_encode($arrayList) );
    }

    public function GetRolesById(){
        $arrayList = [];
        $rolesVo = new ModuleVo();
        $rolesVo->isList = true;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $rolesVo);
        while ($rolesVo2 = $generalDao->GetVo($generalVo = clone $rolesVo)) {
                 $arrayList[] =  ["nombre"=>$rolesVo2->subName ,"events" => $this->GetDataActions($this->idTemp ,$rolesVo2->id )];
         
        }


        print_r( json_encode($arrayList) );
    }


    public  function SetStateModules()
    {
        $roleVo = new ModuleEventVo();
        $roleVo->id = $this->idTemp;
        $roleVo->idSearchField = 0;
        $roleVo->state = $this->DeterminateState($this->stateTemp);
        $roleVo->idUpdateField = 3;
        $generalDao = new GeneralDao();
        $generalDao->UpdateByField($generalVo = clone $roleVo);
    }

    public  function UpdateTypeNotifications()
    {
        $roleVo = new ModuleEventVo();
        $roleVo->id = $this->idTemp;
        $roleVo->idSearchField = 0;
        $roleVo->typeNotification = $_POST["selectedNotification"];
        $roleVo->idUpdateField = 5;
        $generalDao = new GeneralDao();
        $generalDao->UpdateByField($generalVo = clone $roleVo);
        $roleVo->numberNotification = $_POST["daysNotification"];
        $roleVo->idUpdateField = 6;
        $generalDao2 = new GeneralDao();
        $generalDao2->UpdateByField($generalVo = clone $roleVo);
    }

    public function DeterminateState($state){
        if($state == 0){
            return 1;
        }
        return 0;
    }

    public function GetDataActions($rol , $modulo){
        $arrayList = [];
        $rolesVo = new ModuleEventVo();
        $rolesVo->idSearchField = 4;
        $rolesVo->idRol = $rol;
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $rolesVo);
        while ($rolesVo2 = $generalDao->GetVo($generalVo = clone $rolesVo)) {
            if($rolesVo2->idModule == $modulo){
                $arrayList[] =  ["event" => $rolesVo2 , "nameEvent" => $this->GetNameEvent($rolesVo2->idEvent)];
            }
        }
        return  $arrayList;
    }

    public function GetNameEvent($id){
        $rolesVo = new EventsVo();
        $rolesVo->id = $id;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $rolesVo);
        if ($rolesVo2 = $generalDao->GetVo($generalVo = clone $rolesVo)) {
               return $rolesVo2->name;
        }
    }

    
}

$ajaxDataImpost = new AjaxDataModules();
