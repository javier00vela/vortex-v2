<?php
require_once(__DIR__ . '/../../../../controller/general/Config.php');
require_once(Config::PATH . Config::CONTROLLER.Config::GENERAL . 'ManageArrays.php');
require_once(Config::PATH . Config::BACKEND . 'modules/AccessHistoryVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/QuotationVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/InventoryVo.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralDao.php');
require_once(Config::PATH . Config::BACKEND . 'modules/CompanyVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/PersonVo.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralDao.php');
require_once(Config::PATH . Config::BACKEND . 'modules/PersonCreateQuotationVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/ImpostVo.php');

class AjaxDataReportQuotation
{

     private $isActionFromAjax;
    private $optionInputForm;
    private $idDataInputForm;
    

    function __construct(){
       
           $this->isActionFromAjax = $_POST['isActionFromAjax'];
            $this->optionInputForm = $_POST['optionInputForm'];
            $this->idDataInputForm = $_POST['idDataInputForm'];
     
            if ($this->isActionFromAjax == "true") {
                if ($this->optionInputForm == 1) {
                     $this->GetDataInJsonForTbl();
                }else if ($this->optionInputForm == 2) {
                    $this->GetDataInJsonForTblfilter();
                }else if ($this->optionInputForm == 4) {
                    $this->GetDataQuotation();
                }else if ($this->optionInputForm == 5) {
                    $this->GetDataPerson();
                }else if ($this->optionInputForm == 6) {
                    $this->GetDataQuotationForDate();
                }
              
            }
        
    }

   public function GetDataInJsonForTbl()
    {

        $existData = false;
        $quotationVo = new QuotationVo();
        $quotationVo->isList = true;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $quotationVo);

        $string = "
        {\"data\": [";
           while ($quotationVo2 = $generalDao->GetVo($generalVo = clone $quotationVo)) {
            
            $json = array();
            if($this->GetStatusByPerson($quotationVo2->idUser) == 1){
            $existData = true;
            $json[] = $quotationVo2->id;
            $json[] = $quotationVo2->code;
            $json[] = $quotationVo2->version;
            $json[] = $this->GetUserQuotation($quotationVo2->id);
            $json[] = $quotationVo2->generationDate;
            $json[] = $quotationVo2->state;
            $json[] = $quotationVo2->typeQuotation;
            $json[] = $quotationVo2->typeMoney;
                     $json[] = "$". str_replace(",",".",number_format($quotationVo2->total));  
           

            $string .= json_encode($json) . ",";
            }

        }
        $string .= "-]}-";

        if ($existData) {
            echo str_replace(",-]}-", "]}", $string);
        } else {
            echo "{ \"data\": [] }";
        }
    }

     public function GetDataQuotationForDate()
    {

        $existData = false;
        $quotationVo = new QuotationVo();
        $quotationVo->isList = true;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $quotationVo);

        $string = "
        {\"data\": [";
           while ($quotationVo2 = $generalDao->GetVo($generalVo = clone $quotationVo)) {
          //echo date("o-m-d", strtotime($quotationVo2->generationDate)). " <= " .date("o-m-d", strtotime($_POST["max"])) ." && ". date("o-m-d", strtotime($quotationVo2->generationDate))." >= ".date("o-m-d", strtotime($_POST["min"]));
            if(date("o-m-d", strtotime($quotationVo2->generationDate)) <= date("o-m-d", strtotime($_POST["max"])) && date("o-m-d", strtotime($quotationVo2->generationDate)) >= date("o-m-d", strtotime($_POST["min"])) ){
                $existData = true;
                $json = array();
                $json[] = $quotationVo2->id;
                $json[] = $quotationVo2->code;
                $json[] = $quotationVo2->version;
                $json[] = $this->GetUserQuotation($quotationVo2->id);
                $json[] = $quotationVo2->generationDate;
                $json[] = $quotationVo2->state;
                $json[] = $quotationVo2->typeQuotation;
                $json[] = $quotationVo2->typeMoney;
                       $json[] = "$". str_replace(",",".",number_format($quotationVo2->total));  
                $string .= json_encode($json) . ",";   
            }
            
             
        }
      
        $string .= "-]}-";

        if ($existData) {
            echo str_replace(",-]}-", "]}", $string);
        } else {
            echo "{ \"data\": [] }";
        }
    }

    private function GetDataPerson(){
        $generalDao = new GeneralDao();
         $json = array();
         $row;
        $personVo = new PersonVo();
        $personVo->isList = true;
        $generalDao->GetById($generalVo = clone $personVo);
           while ($personVo2 = $generalDao->GetVo($generalVo = clone $personVo)) {
                if($personVo2->idRole != 7 && $personVo2->idRole != 8 && $personVo2->idRole != 9 ){

                        if($personVo2->state == 1){
                            $row =array("id" => $personVo2->id, "name" => htmlspecialchars($personVo2->names)." ".htmlspecialchars($personVo2->lastNames));
                                 //print_r($row);
                              array_push($json, $row); 
                        }
                }
             
                   
                 
            }
              echo json_encode($json);
    }

     private  function GetUserQuotation($idQuotation)
    {
        $nameUser="";
         $usersQuotationVo= new PersonCreateQuotationVo();
         $usersQuotationVo->idSearchField = 5;
         $usersQuotationVo->idQuotation = $idQuotation;
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $usersQuotationVo);
        while ($usersQuotationVo2 = $generalDao->GetVo($generalVo = clone $usersQuotationVo)) {
           
            $nameUser = htmlspecialchars($usersQuotationVo2->completeName);
            
        }
        return $nameUser;

    }

   

    public function GetDataInJsonForTblfilter()
    {
        $generalDao = new GeneralDao();
        $quotationVo = new QuotationVo();
        $quotationVo->idSearchField=6;
        $quotationVo->idUser = $_POST["idUser"];
        $generalDao->GetByField($generalVo = clone $quotationVo);
        $string = "
        {\"data\": [";
          $existData = false;
           while ($quotationVo2 = $generalDao->GetVo($generalVo = clone $quotationVo)) {
                    $existData = true;
                    $json = array();
                    $json[] = $quotationVo2->id;
                    $json[] = $quotationVo2->code;
                    $json[] = $quotationVo2->version;
                    $json[] = $this->GetUserQuotation($quotationVo2->id);
                    $json[] = $quotationVo2->generationDate;
                    $json[] = $quotationVo2->state;
                    $json[] = $quotationVo2->typeQuotation;
                    $json[] = $quotationVo2->typeMoney;
                    $json[] = "$". str_replace(",",".",number_format($quotationVo2->total));  
                   
                    $string .= json_encode($json) . ",";
            }

            $string .= "-]}-";
                    if ($existData) {
                        echo str_replace(",-]}-", "]}", $string);
                    } else {
                        echo "{ \"data\": [] }";
                    }     
    }

    
    public function GetDataQuotation(){
        $quotationVo = new QuotationVo();
        $quotationVo->isList = true;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $quotationVo);
        $json = array();
     
           while ($quotationVo2 = $generalDao->GetVo($generalVo = clone $quotationVo )) {
            if($this->GetStatusByPerson($quotationVo2->idUser) == 1){
            $row = array("id"=>$quotationVo2->id, "typeMoney"=>$quotationVo2->typeMoney, "total"=>$quotationVo2->total, "idUser"=>$quotationVo2->idUser);
            array_push($json, $row);
            }
        }
        echo json_encode($json);
    } 

    public function GetStatusByPerson($id){
        $personVo = new PersonVo();
        $personVo->isList = false;
        $personVo->id = $id;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $personVo);
        if ($personVo2 = $generalDao->GetVo($generalVo = clone $personVo )) {
            return  $personVo2->state;
        }
    }



}

$ajaxDataReportQuotation = new AjaxDataReportQuotation();
