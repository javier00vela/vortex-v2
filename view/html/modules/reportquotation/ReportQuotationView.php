<?php
require_once(Config::PATH . Config::CONTROLLER . 'general/AccessManager.php');
require_once(Config::PATH . Config::CONTROLLER . 'general/UtilJquery.php');
require_once(Config::PATH . Config::VIEW_HTML . 'general/comps/GeneralCtx.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralDao.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralVo.php');
require_once(Config::PATH . Config::VIEW_HTML . 'general/comps/GeneralCtx.php');
require_once(Config::PATH . Config::VIEW_HTML . 'general/comps/GeneralLst.php');
require_once(Config::PATH . Config::VIEW_HTML . 'general/comps/GeneralWithDataLst.php');
require_once(Config::PATH . Config::BACKEND . 'modules/UserVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/PersonVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/AccessHistoryVo.php');
require_once(Config::PATH . Config::CONTROLLER . 'general/ExcelManager.php');

class ReportQuotaionView
{
    private $idUserCtx;
    private $personLst;
    /*=============================================
                  Template Vars
    =============================================*/
    private $utilJQ;
    private $pageTpl, $data, $core;
    /*=============================================
                Data Vars
    =============================================*/
    private $generalDao;
    private $nameColumns =  array("id","codigo","versión","responsable","fecha creación","estado","tipo cotizaciones","moneda","total");



    function __construct($pageTpl, $data, $core)
    {
        $this->data = $data;
        $this->core = $core;
        $this->pageTpl = $pageTpl;
        $this->CreateComponents();
        $this->AssignDataToTpl();
        $this->CreateReportExcel();
    }
    private function CreateComponents(){
        $this->utilJQ = new UtilJquery("ReportQuotaion");
         $this->personLst = new GeneralWithDataLst($this->GetPersons(), "personLst", false, true, "-- Seleccione --", true);
    }



private function CreateReportExcel(){
    if(isset($_POST["data"])){
      ExcelManager::CreateReportExcel($_POST["data"] , $this->nameColumns , "ReportsQuotation" );
  }
}




    private function AssignDataToTpl(){
        $this->data->assign('personLst', $this->personLst->paint());
        $this->data->assign('jquery', $this->utilJQ->PaintJQ());
         $this->data->assign('tempParameter', time());
        $this->SetDataToTpl();
    }

    private function GetPersons(){
       
        $personVo = new PersonVo();
        $generalDao = new GeneralDao();
        $personVo->isList = true;
        $generalDao->GetById($generalVo = clone $personVo);
        while($personVo2 = $generalDao->GetVo($generalVo = clone $personVo)){
             if ($personVo2->idRole==1||$personVo2->idRole==2||$personVo2->idRole==3||$personVo2->idRole==4||$personVo2->idRole==5){
              if($personVo2->state == 1){
                $personsArray[] = $personVo2->id. "_" . $personVo2->names." ".$personVo2->lastNames;
              }
            }  
        }
         return $personsArray;
    }

    private function SetDataToTpl(){

        echo $this->core->get($this->pageTpl, $this->data);
    }

}