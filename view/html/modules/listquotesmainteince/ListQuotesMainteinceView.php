<?php
require_once(Config::PATH . Config::CONTROLLER . 'general/AccessManager.php');
require_once(Config::PATH . Config::CONTROLLER . 'general/UtilJquery.php');
require_once(Config::PATH . Config::VIEW_HTML . 'general/comps/GeneralCtx.php');
require_once(Config::PATH . Config::VIEW_HTML . 'general/comps/GeneralLst.php');
require_once(Config::PATH . Config::VIEW_HTML . 'general/comps/GeneralWithDataLst.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralDao.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/ImpostVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/CompanyVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/RoleVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/QuotationMainteinceVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/UserQuotationVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/ClientUserVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/ElementsMainteinceVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/HistoryTRMVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/PersonVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/PersonCreateQuotationMainteinceVo.php');
require_once(Config::PATH . Config::CONTROLLER_LIB .'vendor/dompdf/dompdf/src/Autoloader.php');

use Dompdf\Dompdf;


class ListQuotesMainteinceView
{
    private $optionInputForm;
    private $isUpdateUserForm;
    private $idDataInputForm;
    /*=============================================
                  Session Vars
    =============================================*/
    private $manageSession;
    /*=============================================
                  Template Vars
    =============================================*/
    private $utilJQ;
    private $pageTpl, $data, $core;
    /*=============================================
                Controls Vars HTML
    =============================================*/
    private $quotationCodeCtx;
    private $quotationGenerationDateCtx;
    private $quotationStateCtx;
    private $quotationPercentCtx;
    private $quotationTotalCtx;
    private $value;
    private $quotationCompanyLst;
    

    /*=============================================
                Data Vars
    =============================================*/
    private $generalDao;


    function __construct($pageTpl, $data, $core)
    {
        $this->data = $data;
        $this->core = $core;
        $this->pageTpl = $pageTpl;

            $this->CreateComponents();
            $this->UseDataFromThePostback();
    }

    private function UseDataFromThePostback()
    {
        if (isset($_POST['optionInputForm'])) {
            $this->optionInputForm = $_POST['optionInputForm'];
            $this->idDataInputForm = $_POST['idDataInputForm'];
            $this->generalDao = new GeneralDao();
            switch ($this->optionInputForm) {
                case "1": {
                    $this->SetData();
                    break;
                }
                case "2": {
                    $this->DeleteData();
                    break;
                }
                case "3": {
                    $products =  $this->GetProductsQuotation($_POST["id"]);
                    $arrayPerson= $this->GetPersonCreateQuotation($_POST["id"]);
                    $quotation = $this->GetDataQuotation($_POST["id"]);
                    $persons = $this->GetPersonsQuotation($_POST["id"]);
                    $this->SavePDF($products , $arrayPerson , $quotation ,  $persons);
                    break;
                }
                case "4": {
                    $this->SetUpdate();
                    break;
                }
                    case "5": {
                   $this->DeleteFilePdf();
                    break;
                }
            }
        }

        $this->AssignDataToTpl();

    }

       private function GetCustomers()
    {
        $companiesArray = array();
        $companyVo = new CompanyVo();
        $companyVo->isList = true;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $companyVo);
        while ($companyVo2 = $generalDao->GetVo($generalVo = clone $companyVo)) {
            if($companyVo2->idRoleCompany == 2 || $companyVo2->idRoleCompany == 4 ){
                if($companyVo2->state == 1){
                        $companiesArray[] = $companyVo2->id . "_" . htmlspecialchars($companyVo2->name);
                }
            }
        
        }
        return $companiesArray;
    }

    private function CreateComponents()
    {
        $this->quotationCompanyLst = new GeneralWithDataLst($this->GetCustomers(), "quotationCompanyLst", false,true, "-- Seleccione --", true);
        $this->quotationCodeCtx = new GeneralCtx("quotationCodeCtx","Codigo",null,true);
        $this->quotationGenerationDateCtx = new GeneralCtx("quotationGenerationDateCtx","Fecha de creación",null,true);
        $this->quotationStateCtx = new GeneralCtx("quotationStateCtx","Estado",null,true);
        $this->quotationPercentCtx = new GeneralCtx("quotationPercentCtx","Porcentaje",null,true);
        $this->quotationTotalCtx = new GeneralCtx("quotationTotalCtx","Total",null,true);
        $this->value = new GeneralCtx("value","values",null,true);
        $this->utilJQ = new UtilJquery("GenerateQuotes");
    }
    private function SetDataToVo()
    {
        $quotationVo = new quotationMainteinceVo();
        $quotationVo->id = $this->idDataInputForm;
        $quotationVo->code = $_POST["quotationCodeCtx"];
        $quotationVo->generateDate = $_POST["quotationGenerationDateCtx"];
        $quotationVo->state = $_POST["quotationStateCtx"];
        $quotationVo->percent = $_POST["quotationPercentCtx"];
        $quotationVo->total = $_POST["quotationTotalCtx"];
       
        return $quotationVo;
    }

    private function DeleteFilePdf(){
        unlink("view/docs/pdf/".$_POST["id"].".pdf");
    }

    private function SetData()
    {
        $quotationVo = $this->SetDataToVo();
        $quotationVo->id = null;
        $result = $this->generalDao->Set($generalVo = clone $quotationVo);
        if (isset($result)) {
            $message = 'MessageSwalBasic("Registrado!","Cotización Registrado Correctamente","success");';
            $this->utilJQ->AddFunctionJavaScript($message);
        }

    }
    private function SetUpdate()
    {
        $result = $this->generalDao->Update($generalVo = clone $this->SetDataToVo());
        if (isset($result)) {
            $message = 'MessageSwalBasic("Modificado!","Cotización Modificado Correctamente","success");';
            $this->utilJQ->AddFunctionJavaScript($message);
        }
    }

    


    private function GetProductsQuotation($id){
        $productList = array();
        $productVo = new ElementsMainteinceVo();
        $productVo->idSearchField = 16;
        $productVo->idQuotation = $id;
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $productVo);
        while ($productVo2 = $generalDao->GetVo($generalVo = clone $productVo)) {
            $productList[] = $productVo2 ;
        }
        return $productList;
    }

    private function GetPersonCreateQuotation($id){
        $personList = array();
        $personCreateVo = new PersonCreateQuotationMainteinceVo();
        $personCreateVo->idSearchField = 5;
        $personCreateVo->idQuotation = $id;
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $personCreateVo);
        while ($personCreateVo2 = $generalDao->GetVo($generalVo = clone $personCreateVo)) {
            $personList[] = $personCreateVo2 ;
        }
        return $personList;
    }

    private function GetDataQuotation($id){
        $quotationList = array();
        $quotationVo = new quotationMainteinceVo();
        $quotationVo->idSearchField = 0;
        $quotationVo->id = $id;
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $quotationVo);
        while ($quotationVo2 = $generalDao->GetVo($generalVo = clone $quotationVo)) {
            $quotationList[] = $quotationVo2 ;
        }
        return $quotationList;
    }

    private function GetPersonsQuotation($id){
        $quotationList = array();
        $quotationVo = new UserQuotationVo();
        $quotationVo->idSearchField = 3;
        $quotationVo->idQuotation = $id;
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $quotationVo);
        while ($quotationVo2 = $generalDao->GetVo($generalVo = clone $quotationVo)) {
            $quotationList[] = $quotationVo2 ;
        }
        return $quotationList;
    }

 

    private function GetRolePerson($idRole){
        $roleVo = new RoleVo();
        $roleVo->id = $idRole;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $roleVo);
        if($role2 = $generalDao->GetVo($generalVo = clone $roleVo)){
            return  $role2->name;
        }
    }
      private function GetPersons($id){
        $usersList = array();
        $personVo = new PersonVo();
        $personVo->idSearchField = 0;
        $personVo->id = $id;
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $personVo);
        while ($personVo2 = $generalDao->GetVo($generalVo = clone $personVo)) {
            $usersList[] = $personVo2 ;
        }
        return $usersList;
    }
    public function GetDataCompany($id)
    {       
        $companyVo = new CompanyVo();
        $generalDao = new GeneralDao();
        $companyVo->id = $id; 
        $generalDao->GetById($generalVo = clone $companyVo);

        if ($companyVo2 = $generalDao->GetVo($generalVo = clone $companyVo)) {
        
           return $companyVo2;
        }
    }

    public function GetNameCompany($id)
    {       
        $companyVo = new CompanyVo();
        $generalDao = new GeneralDao();
        $companyVo->id = $id; 
        $generalDao->GetById($generalVo = clone $companyVo);

        if ($companyVo2 = $generalDao->GetVo($generalVo = clone $companyVo)) {
           return $companyVo2->name;
        }
    }

    private function SavePDF($products , $arrayPerson , $quotation ,  $persons){
        $content = $this->TemplateHtmlForPdf($products , $arrayPerson , $quotation , $persons);
        $dompdf = new DOMPDF();  
        $dompdf->loadHtml($content);
        $dompdf->setPaper('A3', 'letter');
        $dompdf->set_option('isRemoteEnabled', TRUE);
        $dompdf->render();
        $pdf = $dompdf->output();  
        print_r("--split--".$quotation[0]->code."_".$this->GetNameCompany($quotation[0]->idCompany)."_v".$quotation[0]->version."--split--");
       file_put_contents("view/docs/pdf/".$quotation[0]->code."_".$this->GetNameCompany($quotation[0]->idCompany)."_v".$quotation[0]->version.".pdf", $pdf);
    }


    public function HeadStylesPDF($products , $arrayPerson , $quotation , $persons){
         $head = '<html>
                    <head>
                   <style type="text/css" media="screen">
                   <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
                   @font-face {
                    font-family: "Arial Narrow";
                    font-style: normal;
                    font-weight: normal;
                    src: url(dompdf/font/arial_narrow_7.ttf);
                }
                        body{
                            background-image: url("view/img/Fondo1.png");
                            background-repeat: no-repeat;
                            font-family: "font";
                        }
                        #i
                            margin-left:-40px;
                            width:200px;
                            height:180px;
                        }
                        #imgv{

                            margin-left:-40px;
                            width:200px;
                            height:180px;
                        }
                        header {
                            position: fixed;
                            bottom: 10px;
                            top: -60px;
                            height: 300px;
                            padding-left: 875px;
                         
                        }
                        footer {
                            position: fixed; 
                            bottom: -60px; 
                            left: 0px; 
                            right: 0px;
                            height: 50px;         
                            text-align: center;
                            line-height: 35px;
                            opacity: 0.5; 
                        }

                         .alignright {
                             opacity: 0.5;
                               width: 85%;
                            height:auto;
                            text-align: left;
                        }
                        #container{
                            width: 80%;
                        }
                        p{
                            font-family: "Arial Narrow", sans-serif;
                            font-size:18px;
                            
                            
                        }
                          .table, h2 {
                            page-break-inside: avoid !important;
                            border-collapse: separate;
                            border-spacing: 2px 3px;
                        }

                         table th, th {
                            page-break-inside: avoid !important;
                            border: black 1px solid ;
                            text-align: center;
                            font-weight:normal;
                            color:black;
                            padding: 8px;
                        }
                         .react th, th {
                            page-break-inside: avoid !important;
                            border: black 1px solid;
                             font-weight:normal;
                            color:black;
                            padding: 8px;
                        }
                   </style>
                </head>';
                return $head;
    }


    public function ColumnDataCompany($products , $arrayPerson , $quotation , $persons){
        $companyVo= $this->GetDataCompany(1);
        $header =  '<body  style="margin-left:20px;font-family:Narrow">
                        <header style="margin-bottom:35px">
                            <div class="alignright" style="margin-bottom:35px">
                                <div id="logoContainer" style="height:100px; margin-bottom:35px">
                                    <img id="imgv" style="margin-top:3%;display:relative;padding-right:80%" src="view/img/main-logo.png"/>
                                </div> <br> <br>

                                <div style="line-height:0.4;margin-top:7%;text-align:right">
                                     <p style="font-size:85%">'.$companyVo->address.'</p>                                <!--  Dirección   -->
                                     <p style="font-size:85%" > Teléfono: '.$companyVo->phone.'</p>                        <!--  Celular   -->
                                     <p style="font-size:85%" >'.$companyVo->webSite.'</p> <hr>                         <!--  sitio Web   -->
                                    <p style="font-size:85%">'.$companyVo->email.'</p> <hr>                              <!--  Correo   -->
                                    <p style="font-size:85%">'.$companyVo->country.' , '.$companyVo->city.'</p>         <!--  ciudad   -->
                                    <p style="font-size:85%">Sur America</p>                                            <!--  Ubicación   -->
                                </div>        
                            </div>
                        </header>';
        return $header;
    }

    private function SetPriceAsInternational($price){
        $priceData = explode(",", $price);
        $centavos = substr($priceData[1] , 0 , 2);
        if(empty($centavos)){
            $centavos = "00";
        }
        return  str_replace(",",".",number_format($priceData[0]))  .",".$centavos;
    }

    public function GetDateActual($products , $arrayPerson , $quotation , $persons){
        $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
        $mesDate = explode(" ",$quotation[0]->dateUpdate)[0];
        $mes = date("n" , strtotime($mesDate));
        $dia = date("d" ,strtotime(explode(" ",$quotation[0]->dateUpdate)[0]));
        $año = date("Y" ,strtotime(explode(" ",$quotation[0]->dateUpdate)[0]));
        return '<p>Bogotá D.C, '.$dia." de ".$meses[$mes-1]." del ".$año.'  </p>';
    }


    public function DataClientCompany($products , $arrayPerson , $quotation , $persons){
       
        $regards ='
        <div class="serif" style="margin-top:0%;font-family:Narrow;;margin-left:27px">
            <div style="width:76%;line-height:0.5">
                <p style="text-align: right;margin-left:300px;"><label style="font-weight:bold">Cotización No. '.$quotation[0]->code.'</label></p>
                <p style="text-align: right;margin-left:300px;"> V.'.$quotation[0]->version.' </p>
            </div>
        <div style="margin-bottom:25px;">
            '.$this->GetDateActual($products , $arrayPerson , $quotation , $persons).'
        </div> <br>

        <div style="line-height:0.4;margin:2px;">
            <p>Señores </p>
            <p><label style="font-weight: 600; !important">'.strtoupper($this->GetNameCompany($quotation[0]->idCompany)).'</label></p><p>';

            for ($i=0; $i < count($persons) ; $i++) { 
                $regards .=  $this->GetClientUserById($persons[$i]->idPerson)->prefijo." ".ucwords(strtolower($persons[$i]->completeName));
                if($i+1 < count($persons)-1){
                   $regards .= " , "; 
                }else{
                     $regards .= "."; 
                }
            }
           $regards .= "<br><br><br><br><b>".$this->GetCityByIdPerson($quotation[0]->idCompany)."</b>";
        $regards .= '</p>
        </div><br><br>

            <p>Estimado'; if(count($persons) > 1){ $regards .= "s Señores"; }else{ $regards .= " señor(a)"; } 
        $regards .= '</p>
            <p>Para VORTEX es un gusto dejar para su consideración la propuesta económica solicitada</p>
        </div>
        ';   

        return $regards;
    }

      public function GetCityByIdPerson($id){
         $CompanyVo = new CompanyVo();
         $CompanyVo->id = $id;
         $generalDao = new GeneralDao();
         $generalDao->GetById($generalVo = clone $CompanyVo);
        if($CompanyVo2 = $generalDao->GetVo($generalVo = clone $CompanyVo)){
                return $CompanyVo2->city;
        }

        return "Sin Determinar el Lugar";
    }

       private  function GetClientUserById($idClient){
        $ClientUserVo = new ClientUserVo();
        $ClientUserVo->idSearchField = 2;
        $ClientUserVo->idClient = $idClient;
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $ClientUserVo);
        if ($ClientUserVo2 = $generalDao->GetVo($generalVo = clone $ClientUserVo)) {
            return $ClientUserVo2;
        }
        return "error";
    }

    public function GetPriceData($products , $quotation , $i){
        if($quotation[0]->typeMoney == "EUR"){
            $products[$i]->priceUnit =  $this->GetPriceToCurrency("EUR" , $products[$i]->priceUnit , $products )   ;
            // + ((str_replace(",",".",str_replace("$","",$quotation[0]->totalViatic)) / count( $products  ) ))
        }
        if($quotation[0]->typeMoney == "USD"){
            $products[$i]->priceUnit =  $this->GetPriceToCurrency("USD" , $products[$i]->priceUnit , $products ) ;
            //+ ((str_replace(",",".",str_replace("$","",$quotation[0]->totalViatic)) / count( $products  ) )) 
        }

        if($quotation[0]->typeMoney == "COP"){
            $products[$i]->priceUnit = ($products[$i]->priceUnit);
            //  + ((str_replace(",","." ,(str_replace("$","",$quotation[0]->totalViatic))) / count( $products  ) ))
        }

        //print_r( "<br>".$products[$i]->priceUnit . "############". $products[$i]->name."<br>");
        return $products[$i]->priceUnit;
    }

    public function SetItemNameDescription($products , $arrayPerson , $quotation , $persons , $i){
        $IVASUMA = 0;

        //$products[$i]->priceUnit = $this->GetPriceData($products , $quotation , $i);

        $producs = ' '; 
        $producs .= '<div style="width:80%;margin-left:2px;margin:20px;">';
        $producs .= "<b>Servicio #".($i+1)."</b>";
        $producs .= '<br>';
        $producs .= "<p>".$products[$i]->description."</p>";
        $producs .= '</br>';
       /* $producs .= '<table class="table">';
        $producs .= $this->GetHeadTableDescription();
        
        $producs .= $this->GetHeadBodyData($products , $i);
        
        
        $producs .= '<th> $'.$this->GetPriceUnit($products,$quotation, $i).'</th>';
        
            if($products[$i]->IVA == "0" ||  $quotation[0]->isDDP == "0"){
                $ivaState = 0;
                $IVASUMA += 0;
                $producs .= '<th>No Aplica</th>';
            }else{
                $ivaState = $this->GetPercent($quotation[0]->IVA,100);
                $IVASUMA += $this->GetIvaProduct($products , $quotation , $i);
                $producs .= ' <th> $'.$this->GetTotalIva($products , $quotation , $ivaState , $i).'</th>';
            }
    
        $producs .= '<th>$'.$this->GetTotalUnit($products , $quotation , $ivaState , $i).'</th>';
        $producs .= '<th>$'.$this->GetTotalItem($products , $quotation , $ivaState , $i).'</th>';
        $producs .= $this->GetTypeTimeDelivery($products , $i);
     
       $producs.='</table><div>'; */
       $producs.='<div>';
        
        return $producs;
    }

    public function GetTasa($field){
        $arrayList = array();
        $HistoryTRMVo = new HistoryTRMVo();
        $HistoryTRMVo->isList = true;
        $generalDao = new GeneralDao();
        $generalDao->CustomQuery("SELECT * FROM {$HistoryTRMVo->nameTable} WHERE  typeMoney='{$field}'");
        while($HistoryTRMVo2 = $generalDao->GetVo($generalVo = clone $HistoryTRMVo)) {
          return $HistoryTRMVo2->money;
        }   
        return 0;
}

    public function GetPriceToCurrency($type , $money , $products){
        return ( ($money / $this->GetTasa($type)) );
    }

    public function CreateDescriptionsEquipos($products , $arrayPerson , $quotation , $persons ){
        $producs = "";
        $cont = 0;
        for ($i=0; $i < count($products) ; $i++) { 
            if($products[$i]->typeProduct == "Servicio"){
                $producs .= $this->SetItemNameDescription($products , $arrayPerson , $quotation , $persons , $i);
                $cont++;
            }
        }
        if($cont > 0){
            $producs .= $this->SetSpaceTable();
        }
        return $producs;
    }




    public function CreateDetailsTableEquipos($products , $arrayPerson , $quotation , $persons){
        $dataQuotation = "";
        $dataQuotation .= "<table class='table'><tr>
                            <th>Validez de la oferta</th>
                            <th>".$quotation[0]->offerValidity." Días</th>
                            </tr><tr>
                            <th>Lugar de Entrega</th>
                            <th>".$quotation[0]->placeDelivery."</th>
                            </tr><tr>
                            <th>Tipo de Cotización</th>
                            <th>".$quotation[0]->typeQuotation."</th>
                            </tr><tr>
                            <th>Forma de pago</th>
                            <th>".$quotation[0]->payment."</th>
                            </tr><tr>
                            <th>Tipo de Moneda</th>
                            <th>".$quotation[0]->typeMoney."</th>
                            </tr>
                            </table>
                            ";
        return $dataQuotation;
    }

    public function GetHeadTableData(){
        return '<div style="width:80%"><table class="react" >
           <tr>
               <th>Item</th>
               <th colspan="4">Descripción</th>
               <th>Cantidad</th>
               <th>Precio <br> Unidad</th>
               <th>Iva <br> Unidad</th>
               <th>Total <br> Unidad</th>
               <th>Total <br> Item</th>
               <th colspan="5">Tiempo  de <br> Entrega</th>
           </tr>';
    }

    public function GetHeadTableDescription(){
        return '
           <tr>
               <th>Item</th>
               <th colspan="4">Descripción</th>
               <th>Cantidad</th>
               <th>Precio <br> Unidad</th>
               <th>Iva <br> Unidad</th>
               <th>Total <br> Unidad</th>
               <th>Total <br> Item</th>
               <th colspan="5">Tiempo  de <br> Entrega</th>
           </tr>';
    }

    public function GetPriceUnit($products,$quotation , $i){
        if($quotation[0]->typeMoney == "USD" || $quotation[0]->typeMoney == "EUR" ){
            return  $this->SetPriceAsInternational(str_replace(".","," ,(( ($products[$i]->priceUnit * $this->GetPercent($products[$i]->percent , 100) ) + $products[$i]->priceUnit) 
            + ((str_replace(",",".",str_replace("$","",$quotation[0]->totalViatic)) / count( $products  ) )) 
        )));
        }else{
            return str_replace(",","." ,number_format((( ($products[$i]->priceUnit * $this->GetPercent($products[$i]->percent , 100) ) + $products[$i]->priceUnit)) + ((str_replace(",","." ,(str_replace("$","",$quotation[0]->totalViatic))) / count( $products  ) )) ));
        }
    }


    public function GetPercent($price , $percent){
       return ($price/$percent);
    }

    public function GetIvaProduct($products , $quotation , $i){
        return (( ($products[$i]->priceUnit * $this->GetPercent($products[$i]->percent,100)) + $products[$i]->priceUnit) * $products[$i]->quantity) * $this->GetPercent($quotation[0]->IVA,100)+ ((str_replace(",",".",str_replace("$","",$quotation[0]->totalViatic)) / count( $products  ) ) * ($quotation[0]->IVA/100) ) ;
    }

    public function GetTotalIva($products , $quotation ,$ivaState, $i){
        if($quotation[0]->typeMoney == "USD" || $quotation[0]->typeMoney == "EUR" ){
            return $this->SetPriceAsInternational(str_replace(".","," ,(( (( ($products[$i]->priceUnit * $this->GetPercent($products[$i]->percent , 100) ) + $products[$i]->priceUnit))* $ivaState)) + ((str_replace(",",".",str_replace("$","",$quotation[0]->totalViatic)) / count( $products  ) ) * ($quotation[0]->IVA/100) )));
          }else{
              return str_replace(",","." ,number_format(( (( ($products[$i]->priceUnit * $this->GetPercent($products[$i]->percent , 100) ) + $products[$i]->priceUnit))* $ivaState) + ((str_replace(",","",str_replace("$","",$quotation[0]->totalViatic)) / count( $products  ) ) * ($quotation[0]->IVA/100) )  ));
          }
    }

    public function GetTotalUnit($products ,$quotation , $ivaState , $i ){
        if($quotation[0]->typeMoney == "USD" || $quotation[0]->typeMoney == "EUR" ){
            return $this->SetPriceAsInternational(str_replace(".","," ,((((( ($products[$i]->priceUnit * $this->GetPercent($products[$i]->percent , 100) ) + $products[$i]->priceUnit) +  (( ($products[$i]->priceUnit * $this->GetPercent($products[$i]->percent , 100) ) + $products[$i]->priceUnit))* $ivaState )))) + ((str_replace(",",".",str_replace("$","",$quotation[0]->totalViatic)) / count( $products  ) ) * ($quotation[0]->IVA/100) ) +(str_replace(",","",str_replace("$","",$quotation[0]->totalViatic)) / count( $products  ) ) )  );
        }else{
            return str_replace(",","." , number_format(str_replace(",","." , (( ( ($products[$i]->priceUnit * $this->GetPercent($products[$i]->percent, 100) ) + $products[$i]->priceUnit))* $ivaState) + ( (( ($products[$i]->priceUnit * $this->GetPercent($products[$i]->percent , 100) ) + $products[$i]->priceUnit))) + ((str_replace(",","",str_replace("$","",$quotation[0]->totalViatic)) / count( $products  ) ) * ($quotation[0]->IVA/100) ) +(str_replace(",","",str_replace("$","",$quotation[0]->totalViatic)) / count( $products  ) )    )));
        }
    }

    public function GetTotalItem($products , $quotation , $ivaState , $i){
        if($quotation[0]->typeMoney == "USD" || $quotation[0]->typeMoney == "EUR" ){
            return $this->SetPriceAsInternational(str_replace(".","," ,((((( ($products[$i]->priceUnit * $this->GetPercent($products[$i]->percent , 100) ) + $products[$i]->priceUnit) +  (( ($products[$i]->priceUnit * $this->GetPercent($products[$i]->percent , 100) ) + $products[$i]->priceUnit))* $ivaState )))) + ((str_replace(",",".",str_replace("$","",$quotation[0]->totalViatic)) / count( $products  ) ) * ($quotation[0]->IVA/100) ) +(str_replace(",","",str_replace("$","",$quotation[0]->totalViatic)) / count( $products  ) ) *$products[$i]->quantity ));
        }else{
            return str_replace(",","." , number_format(str_replace(",","." , (( ( ($products[$i]->priceUnit * $this->GetPercent($products[$i]->percent, 100) ) + $products[$i]->priceUnit))* $ivaState) + ( (( ($products[$i]->priceUnit * $this->GetPercent($products[$i]->percent , 100) ) + $products[$i]->priceUnit))) + ((str_replace(",","",str_replace("$","",$quotation[0]->totalViatic)) / count( $products  ) ) * ($quotation[0]->IVA/100) ) +(str_replace(",","",str_replace("$","",$quotation[0]->totalViatic)) / count( $products  ) )  * $products[$i]->quantity   )));
        }
    }

    public function GetTypeTimeDelivery($products , $i){
        if(is_numeric($products[$i]->timeDelivery)){
            return ' <th colspan="5"> Tiempo de entrega '.$products[$i]->timeDelivery.' Días</th>';
        }else{
            return '<th colspan="5"> 5 dias, salvo <br> venta previa</th>';
        }
    }

    public function GetSubtotalProducts($products ,$quotation , $i ){
        $subTotal = 0;
            $subTotal+= (( ($products[$i]->priceUnit * ($products[$i]->percent / 100) ) + $products[$i]->priceUnit)*$products[$i]->quantity) + ((str_replace(",",".",str_replace("$","",$quotation[0]->totalViatic)) / count( $products  ) ))  ;
        return $subTotal;
    }

    public function GetHeadBodyData($products , $i){
        if($products[$i]->typeProduct == "Servicio"){
            return '<tr style="text-align:left;">
            <th >'.($i+1).'</th>
            <th colspan="4" style="text-align:left;">'.
            "Nombre Servicio :"." ".$products[$i]->name."<br>".
            "Tiempo :"." ".$products[$i]->presentation."Minutos <br></th>".
            '<th>'.$products[$i]->quantity."</th>";
        }
        else{
            return '<tr style="text-align:left;">
            <th >'.($i+1).'</th>
            <th colspan="4" style="text-align:left;">'
            ."Referencia :"." ".$products[$i]->reference."<br>".
            "Nombre :"." ".$products[$i]->name."<br>".
            "Presentación :"." ".$products[$i]->presentation."<br>".
            "Marca :"." ".$products[$i]->brand."<br>".
            '</th><th >'.$products[$i]->quantity.'</th>';
        }
    }

    public function GetBodyTabla($products , $quotation , $i){
        $dataArray = [];
        $IVASUMA = 0;
        $table = '';
        $ivaState = 0;
        $table .= $this->GetHeadBodyData($products , $i);
        $table .= '<th> $'.$this->GetPriceUnit($products,$quotation, $i).'</td>';
            if($products[$i]->IVA == "0" ||  $quotation[0]->isDDP == "0"){
                $ivaState = 0;
                $IVASUMA += 0;
                $table .= '<th>No Aplica</th>';
            }else{
                $ivaState = $this->GetPercent($quotation[0]->IVA,100);
                $IVASUMA += $this->GetIvaProduct($products , $quotation , $i);
                $table .= ' <th> $'.$this->GetTotalIva($products , $quotation , $ivaState , $i).'</th>';
            }
        $table .= '<th>$'.$this->GetTotalUnit($products , $quotation , $ivaState , $i).'</th>';
        $table .= '<th>$'.$this->GetTotalItem($products , $quotation , $ivaState , $i).'</th>';
        $table .= $this->GetTypeTimeDelivery($products , $i);
       
       $table.=' </tr>';
       $dataArray = ["tabla"=>$table , "IVASUMA" => $IVASUMA  , "ivaState" => $ivaState];
       return $dataArray;
    }

    public function GetSubtotal($subtotal , $quotation){
        if($quotation[0]->typeMoney == "USD" || $quotation[0]->typeMoney == "EUR" ){
            return '<tr>
            <th colspan="6">Subtotal</th>
            <th colspan="9"> $  '.$this->SetPriceAsInternational(str_replace(".","," ,$subtotal)).'</th>' ;
        }else{
            return '<tr>
            <th colspan="6">Subtotal</th>
            <th colspan="9"> $  '.str_replace(",","." ,number_format($subtotal)).'</th>' ;
        }
    }

    public function GetViatic($quotation){
        if($quotation[0]->typeMoney == "USD" || $quotation[0]->typeMoney == "EUR" ){
            return '<tr>
            <th colspan="6">Costos Extras</th>
            <th colspan="9">'.$quotation[0]->totalViatic.'</th>' ;
        }else{
            return '<tr>
            <th colspan="6">Costos Extras</th>
            <th colspan="9">$'.str_replace(",","." ,number_format(str_replace("$","",$quotation[0]->totalViatic))).'</th>' ;
        }
    }

    public function GetViaticPrice($quotation){
        if($quotation[0]->typeMoney == "USD" || $quotation[0]->typeMoney == "EUR" ){
            return str_replace( "$","",str_replace(",","." ,$quotation[0]->totalViatic));
        }else{
            return str_replace( "$","",$quotation[0]->totalViatic) ;
        }
    }

    

    public function GetIVATotal($iva , $quotation){
        if($quotation[0]->typeMoney == "USD" || $quotation[0]->typeMoney == "EUR" ){
            return '<tr>
            <th colspan="6">IVA</th>
            <th colspan="9"> $  '.$this->SetPriceAsInternational(str_replace(".","," ,($iva))).'</th>' ;
        }else{
            return '<tr>
            <th colspan="6">IVA</th>
            <th colspan="9"> $  '.str_replace(",","." ,number_format($iva)).'</th>' ;
        }
    }

    public function GetTotal( $quotation , $subTotal , $ivaTotal ){
        $total = 0;
        if($quotation[0]->typeMoney == "USD" || $quotation[0]->typeMoney == "EUR" ){
            $total = $this->SetPriceAsInternational(str_replace(".","," , $this->FromInternacionalToCalculate(str_replace(".","," ,$subTotal )) + $this->FromInternacionalToCalculate(str_replace(".","," ,$ivaTotal))    ));
            return '<tr>
            <th colspan="6">Total</th>
            <th colspan="9"> $  '.$total.'</th>' ;
        }else{
            $total = $subTotal + $ivaTotal ;
            return '<tr>
            <th colspan="6">Total</th>
            <th colspan="9"> $  '.str_replace(",","." ,number_format($total)).'</th>' ;
        }
    }

    public function MakeTable($products , $arrayPerson , $quotation , $persons){
        $table  = '';
        $ivaTotal = 0;
        $subTotal = 0;
        $ivaSuma = 0;
        $table .=  $this->GetHeadTableData();
            for ($i=0; $i < count($products) ; $i++) { 
                print_r("////////////////////////////////////////*****************************************".(str_replace("$","",$quotation[0]->totalViatic)));
              //  if($products[$i]->typeProduct != "Servicio"){
                    $products[$i]->priceUnit = $this->GetPriceData($products , $quotation , $i);
              //  }
                $dataArray = $this->GetBodyTabla($products , $quotation , $i);
                $table .=  $dataArray["tabla"];
                $subTotal += $this->GetSubtotalProducts($products , $quotation , $i);
                $ivaSuma += $dataArray["IVASUMA"];
            }

        $table.= $this->GetSubtotal( $subTotal , $quotation);

            if($products[$i]->IVA == "0" ||  $quotation[0]->isDDP == "0"){
                $table .= $this->GetIVATotal($ivaTotal , $quotation);
            }else{
                $ivaTotal = $ivaSuma;
                $table .= $this->GetIVATotal($ivaTotal , $quotation);
            }
       // $table.= $this->GetViatic($quotation);
        $table .= $this->GetTotal( $quotation , $subTotal , $ivaTotal ,$this->GetViaticPrice($quotation));
        $table .='</tr>';
        $table.='</table></div><br><br><br>';
        return $table;
    }

    public function FromInternacionalToCalculate($price){
        return str_replace(",","." , str_replace(".","" , $price));
    }

    public function GetUserSeller($idClient){
     $ClientVo = new ClientUserVo();
        $ClientVo->idSearchField = 2;
        $ClientVo->idClient = $idClient;
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $ClientVo);
        if ($ClientVo2 = $generalDao->GetVo($generalVo = clone $ClientVo)) {
            return $this->GetPerson($ClientVo2->idUser);
        }
        return  $this->GetPerson(4);
    }

    public function GetPerson($idClient){
     $ClientVo = new PersonVo();
        $ClientVo->id = $idClient;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $ClientVo);
        if ($ClientVo2 = $generalDao->GetVo($generalVo = clone $ClientVo)) {
            return $ClientVo2;
        }
        return "error";
    }

    public function GetPersonById($idClient){
     $QuotationVo = new quotationMainteinceVo();
        $QuotationVo->id = $idClient;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $QuotationVo);
        if ($QuotationVo2 = $generalDao->GetVo($generalVo = clone $QuotationVo)) {
            return $QuotationVo2->idUser;
        }
        return "error";
    }

        function GetTypeRole($idRole)
    {
        $roleVo = new RoleVo();
        $roleVo->id = $idRole;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $roleVo);
        if ($roleVo2 = $generalDao->GetVo($generalVo = clone $roleVo)) {
            return "" . $roleVo2->name;
        }
        return "Problema en la consulta";
    }


    public function CreateFirmPerson($products , $arrayPerson , $quotation , $persons){
        $madeBy = "";
        $array = [];
        for ($i=0; $i < count($persons) ; $i++) { 
            if(!in_array($persons,$array)){
                $persona = $this->GetUserSeller($persons[$i]->idPerson);
                $madeBy .='
                    <div style="widht:50%;line-height:0.4;margin-top:7%;;margin-left:25px;">
                    <p>'.$persona->names." ".$persona->lastNames.'</p>
                    <p>'.$this->GetTypeRole($persona->idRole).'</p>
                    <p>Cel:'.$persona->cellPhone.'  </p> 
                    <p>E-mail: '.$persona->email.'</p>
                    </div> </div>';
                    array_push($array , $persona->id );
            }
        }
        if(!in_array($this->GetPersonById($arrayPerson[0]->idQuotation),$array)){
        $madeBy .='<div style="widht:50%;line-height:0.4;margin-top:7%;;margin-left:25px;">
                    <p>'.$arrayPerson[0]->completeName.'</p>
                    <p>'.$arrayPerson[0]->rolName.'</p>
                    <p>Cel:'.$arrayPerson[0]->cellPhone.'  </p> 
                    <p>E-mail: '.$arrayPerson[0]->email.'</p>
                    </div> </div>';
        }
            return $madeBy;
    }

    
    public function CreateFooter(){
        $footer =  '<footer>
                        <img src="view/img/footer.png" width="100%" height="100%"/>
                    </footer>
                    ';
        return $footer;
    }

    public function CreateFinalContent(){
        $final = '
                </body>
                </html>';
        return $final ;
    }

    public function SetSpaceTable(){
        return '<div style="page-break-after:always;"></div>';
    }

    public function TemplateHtmlForPdf($products , $arrayPerson , $quotation , $persons){
        $table = "";
        $style = $this->HeadStylesPDF($products , $arrayPerson , $quotation , $persons);
        $header = $this->ColumnDataCompany($products , $arrayPerson , $quotation , $persons);
        $regards =  $this->DataClientCompany($products , $arrayPerson , $quotation , $persons);                   
        $producs =  $this->CreateDescriptionsEquipos($products , $arrayPerson , $quotation , $persons );
        
        $table .= $this->MakeTable($products , $arrayPerson , $quotation , $persons);
        $dataQuotation = $this->CreateDetailsTableEquipos($products , $arrayPerson , $quotation , $persons);

        $madeBy = $this->CreateFirmPerson($products , $arrayPerson , $quotation , $persons);
        $maintence = $this->CreateWordMaintence();
        $footer = $this->CreateFooter();
        $final = $this->CreateFinalContent();
        $leyData = $this->SetDataAllows();

        $html = "";
        $html .= $style;
        $html .= $header;
        $html .= $footer;
        $html .= $regards;
        $html .= $producs;
       
        $html .= $table;
        $html .= $dataQuotation;
        $html .= $maintence; 
        $html .= $madeBy;
        $html .= $leyData;
        $html .= $final;
    
        return $html;
    }


    private function CreateWordMaintence(){
        if(isset($_POST["isMaintence"])){
        return "<div style='width:80%'><h2 style='text-align:center'>CONDICIONES DE SERVICIO</h2><br><p style='text-align:justify'><b>Mantenimiento preventivo</b>: Limpieza interna y externa, verificación de los circuitos electrónicos, verificación de las condiciones ambientales en donde se encuentra el equipo (temperatura y humedad relativa), verificación de circuitos eléctricos, verificaciones de elementos periféricos y estado de los consumibles.
        El mantenimiento preventivo se realiza a equipos que están en funcionamiento, por ende, no se asumen costos de repuesto ni de consumibles del equipo que no estén acordados con el cliente. En caso de alguna falla, esta será reportada al cliente y puesto en el informe.<br><br><br>

        <b>Mantenimiento Correctivo:</b> Incluyendo el protocolo de un servicio de mantenimiento preventivo, se realiza mantenimiento correctivo cuando un equipo presenta una falla o avería y que por su naturaleza no pueden planificarse en el tiempo. Por ende, se asumirán gastos de repuestos según contrato y acordado con el cliente.
        <br><br>
    
        <b>NOTAS</b>: Garantía a partir de la fecha de entrega e instalación por defectos de Servicio y/o materiales suministrados por Vortex Company durante 90 días calendario.
        La visita de servicio será programada en común acuerdo una vez recibida su orden de compra, de acuerdo a las fechas de entrega.<br><br>
        No se realizarán garantias de Servicio en caso golpes, mal uso, de daño por sobre voltajes y/o malas condiciones ambientales.<br><br>

        DOCUMENTACION ENTREGADA<br>
        Informe de servicio y Test de desempeño.<br>
        Estampilla con fecha de prestación de servicio.<br>
        CONDICIONES COMERCIALES<br></p></div>";
        }else{
            return '';
        }
    }

    private function SetDataAllows(){
        return "<br><br><br><div style='width:80%'><p style='font-size:15px;color:gray;text-align:justify'>
        Autorizo a VORTEX COMPANY SAS., o a quien represente sus derechos, en forma permanente e irrevocable, para que con fines estadísticos y de información financiera o comercial, consulte, informe, reporte, procese o divulgue, a las entidades de consulta de bases de datos o Centrales de Información y Riesgo, todo lo referente a mi comportamiento como cliente en general. 
        
        Leído lo anterior, autorizo de manera previa, explícita e inequívoca a VORTEX COMPANY SAS, para el tratamiento de los datos personales suministrados por mi persona dentro de las finalidades legales, contractuales, comerciales y las aquí contempladas. Declaro que soy el titular de la información reportada en este formulario para autorizar el tratamiento de mis datos personales, que la he suministrado de forma voluntaria y es completa, confiable, veraz, exacta y verídica. </p></div>";
    }


    private function DeleteData()
    {
        $message = "";
        $quotationVo = new quotationMainteinceVo();
        $quotationVo->id = $this->idDataInputForm;
        if ($this->generalDao->Delete($generalVo = clone $quotationVo)) {
            $message = 'MessageSwalBasic("Eliminado!","Impuesto Eliminado Correctamente","success");';
        } else {
            $message = 'MessageSwalBasic("Error!","Problema con la base de datos","error");';
        }
        $this->utilJQ->AddFunctionJavaScript($message);
    }

     public function GetImpostIva()
    {
        $iva= 0;
        $impostVo = new ImpostVo();
        $impostVo->idSearchField = 1;
        $impostVo->name = "IVA";
        $generalDao = new GeneralDao();
         $generalDao->GetByField($generalVo = clone $impostVo);
        if($impostVo2 = $generalDao->GetVo($generalVo = clone $impostVo)){
                $iva = $impostVo2->percent;
        }else{
            $iva = 19;
        }
        return $iva;
    }

    private function AssignDataToTpl()
    {
        $this->data->assign('value', $this->value->paint());
        $this->data->assign('quotationCompanyLst', $this->quotationCompanyLst->paint());
        $this->data->assign('quotationCodeCtx', $this->quotationCodeCtx->paint());
        $this->data->assign('quotationGenerationDateCtx', $this->quotationGenerationDateCtx->paint());
        $this->data->assign('quotationStateCtx', $this->quotationStateCtx->paint());
        $this->data->assign('quotationPercentCtx', $this->quotationPercentCtx->paint());
        $this->data->assign('quotationTotalCtx', $this->quotationTotalCtx->paint());
         $this->data->assign('tempParameter', time());

        $this->data->assign('jquery', $this->utilJQ->PaintJQ());
        $this->SetDataToTpl();
    }   
    private function SetDataToTpl()
    {
        echo $this->core->get($this->pageTpl, $this->data);
    }

}   