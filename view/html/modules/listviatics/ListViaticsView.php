<?php
require_once(Config::PATH . Config::CONTROLLER . 'general/AccessManager.php');
require_once(Config::PATH . Config::CONTROLLER . 'general/UtilJquery.php');
require_once(Config::PATH . Config::VIEW_HTML . 'general/comps/GeneralCtx.php');
require_once(Config::PATH . Config::VIEW_HTML . 'general/comps/GeneralLst.php');
require_once(Config::PATH . Config::VIEW_HTML . 'general/comps/GeneralWithDataLst.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralDao.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/ViaticsPropetyVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/ViaticsVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/PricesCompanyVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/CompanyVo.php');

class ListViaticsView
{
    private $optionInputForm;
    private $isUpdateUserForm;
    private $idDataInputForm;
    /*=============================================
                  Session Vars
    =============================================*/
    private $manageSession;
    /*=============================================
                  Template Vars
    =============================================*/
    private $utilJQ;
    private $pageTpl, $data, $core;
    /*=============================================
                Controls Vars HTML
    =============================================*/
    private $TMPnameCtx;
    private $typeMoneyLst;
    private $PorcentToSailCtx;
    private $impostNameCtx;
    private $impostPercentCtx;
    private $impostQuantityCtx;
    private $impostPriceCtx;
    
    private $idCompanyPriceCtx;
    private $percents = ["DDP"=>"" , "FCA"=>"" , "CIF"=>"" ];
   

   private $ddp = 0;
   private $cif= 0;
   private $fca= 0;


    /*=============================================
                Data Vars
    =============================================*/
    private $generalDao;
    public $arrayViatics = [
        "transport" => [
            "price" => 0,
            "quantity" => 0
        ],
        "food" => [
            "price" => 0,
            "quantity" => 0
        ],
        "improvistos" => [
            "price" => 0,
            "quantity" => 0
        ],
        "hospedaje" => [
            "price" => 0,
            "quantity" => 0
        ],
        "total" => 0

    ];


    private function GetDataViatics(int $idViatic){
        $ViaticsPropetyVo = new ViaticsPropetyVo();
        $ViaticsPropetyVo->idViatic = $idViatic;
        $ViaticsPropetyVo->idSearchField = 5;
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $ViaticsPropetyVo);
        while ($ViaticsPropetyVo2 = $generalDao->GetVo($generalVo = clone $ViaticsPropetyVo)) {
            if($ViaticsPropetyVo2->typeViatic == "transport"){
                $this->arrayViatics["transport"]["price"] +=  (str_replace(",",".", ($ViaticsPropetyVo2->price * ($ViaticsPropetyVo2->percent / 100) + $ViaticsPropetyVo2->price ) * $ViaticsPropetyVo2->quantity ));
                $this->arrayViatics["transport"]["quantity"] += 1;
            }else if($ViaticsPropetyVo2->typeViatic == "food"){
                $this->arrayViatics["food"]["price"] +=  (str_replace(",",".", ($ViaticsPropetyVo2->price * ($ViaticsPropetyVo2->percent / 100) + $ViaticsPropetyVo2->price ) * $ViaticsPropetyVo2->quantity ));
                $this->arrayViatics["food"]["quantity"] += 1;
            }else if($ViaticsPropetyVo2->typeViatic == "hospedaje"){
                $this->arrayViatics["hospedaje"]["price"] +=  (str_replace(",",".", ($ViaticsPropetyVo2->price * ($ViaticsPropetyVo2->percent / 100) + $ViaticsPropetyVo2->price ) * $ViaticsPropetyVo2->quantity ));
                $this->arrayViatics["hospedaje"]["quantity"] += 1;
            }else if($ViaticsPropetyVo2->typeViatic == "improvistos"){
                $this->arrayViatics["improvistos"]["price"] +=  (str_replace(",",".", ($ViaticsPropetyVo2->price * ($ViaticsPropetyVo2->percent / 100) + $ViaticsPropetyVo2->price ) * $ViaticsPropetyVo2->quantity ));
                $this->arrayViatics["improvistos"]["quantity"] += 1;
            }  
            $this->arrayViatics["total"] +=   (str_replace(",",".", ($ViaticsPropetyVo2->price * ($ViaticsPropetyVo2->percent / 100) + $ViaticsPropetyVo2->price ) * $ViaticsPropetyVo2->quantity )); 
        }
        $this->arrayViatics["transport"]["price"] =  str_replace(",",".",number_format($this->arrayViatics["transport"]["price"]));
        $this->arrayViatics["food"]["price"] =  str_replace(",",".",number_format($this->arrayViatics["food"]["price"]));
        $this->arrayViatics["hospedaje"]["price"] =  str_replace(",",".",number_format($this->arrayViatics["hospedaje"]["price"]));
        $this->arrayViatics["improvistos"]["price"] =  str_replace(",",".",number_format($this->arrayViatics["improvistos"]["price"]));
        $this->arrayViatics["total"] = str_replace(",",".",number_format($this->arrayViatics["total"]));

        return $this->arrayViatics;
    }


    function __construct($pageTpl, $data, $core)
    {
        $this->data = $data;
        $this->core = $core;
        $this->pageTpl = $pageTpl;
   
        //$this->ValidateIfExistCompany($_GET["idCompany"]);
           
            $this->CreateComponents();
            $this->UseDataFromThePostback();
         
    }

    private function UseDataFromThePostback()
    {
        if (isset($_POST['optionInputForm'])) {
            $this->optionInputForm = $_POST['optionInputForm'];
            $this->idDataInputForm = $_POST['idDataInputForm'];
            $this->generalDao = new GeneralDao();
            switch ($this->optionInputForm) {
                case "1": {
                    $this->SetData();
                    break;
                }
                case "2": {
                    $this->DeleteData();
                    break;
                }
                case "4": {
                    $this->SetUpdate();
                    break;
                }
                case "8": {
                    $this->LoadJsonMoneys();
                    break;
                }
            }
        }

        $this->AssignDataToTpl();

    }

    private function CreateComponents()
    {
        $this->TMPnameCtx = new GeneralCtx("TMPnameCtx","Nombre",null,true);
        $array = array("EUR","USD","COP");
      
        $this->typeMoneyLst = new  GeneralWithDataLst($array, "typeMoneyLst", false,false,"-- seleccione un tipo de moneda --",true);
        $this->impostNameCtx = new GeneralCtx("impostNameCtx","Nombre",null,true);
        $this->impostPercentCtx = new GeneralCtx("impostPercentCtx","Porcentaje",null,true);
        $this->impostQuantityCtx = new GeneralCtx("impostQuantityCtx","Cantidad","number",true);
        $this->impostPriceCtx = new GeneralCtx("impostPriceCtx","Precio",null,true);
       
        $this->utilJQ = new UtilJquery("Impost");
    }
    private function SetDataToVo()
    {
        $ViaticsPropetyVo = new ViaticsPropetyVo();
        $ViaticsPropetyVo->id = $this->idDataInputForm;
        $ViaticsPropetyVo->name = $_POST["impostNameCtx"];
        $ViaticsPropetyVo->percent = $_POST["impostPercentCtx"];
        $ViaticsPropetyVo->quantity = $_POST["impostQuantityCtx"];
        $ViaticsPropetyVo->price = $_POST["impostPriceCtx"];
        $ViaticsPropetyVo->idViatic = $_POST["idViatic"];
        $ViaticsPropetyVo->typeViatic = $_POST["typeViatic"];
       
        return $ViaticsPropetyVo;
    }
    private function SetData()
    {
        $ViaticsPropetyVo = $this->SetDataToVo();
        $ViaticsPropetyVo->id = null;
        $result = $this->generalDao->Set($generalVo = clone $ViaticsPropetyVo);
        if (isset($result)) {
            $message = 'MessageSwalBasic("Registrado!","Costo Registrado Correctamente","success");';
            $this->utilJQ->AddFunctionJavaScript($message);
            header("Refresh:0");
        }

    }

    private function LoadJsonMoneys(){
       $json = file_get_contents("http://api.currencies.zone/v1/quotes/".$_POST["from"]."/COP/json?key=136|QdBH^Od*z2Q~sSLOfnKPYZYEGGECk1_1");
       print_r("---**".$json."---**");
    }

    private function SetUpdate()
    {
        $result = $this->generalDao->Update($generalVo = clone $this->SetDataToVo());
        if (isset($result)) {
            $message = 'MessageSwalBasic("Modificado!","Costo Modificado Correctamente","success");';
            $this->utilJQ->AddFunctionJavaScript($message);
        }
    }

    private function ValidateIfExistCompany($id){
        if(empty($this->GetVoIdCompany($id)->id)){
            header("location: DontFind");
        }
    }


    private function DeleteData()
    {
        $message = "";
        $ViaticsPropetyVo = new ViaticsPropetyVo();
        $ViaticsPropetyVo->id = $this->idDataInputForm;
        if ($this->generalDao->Delete($generalVo = clone $ViaticsPropetyVo)) {
            $message = 'MessageSwalBasic("Eliminado!","Costo Eliminado Correctamente","success");';
        } else {
            $message = 'MessageSwalBasic("Error!","Problema con la base de datos","error");';
        }
        $this->utilJQ->AddFunctionJavaScript($message);
    }

    public function AviaticsGetById($id){
        $ViaticsPropetyVo = new ViaticsVo();
        $ViaticsPropetyVo->id = $id;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $ViaticsPropetyVo);
        if ($ViaticsPropetyVo2 = $generalDao->GetVo($generalVo = clone $ViaticsPropetyVo)) {
            return $ViaticsPropetyVo2;
        }
    }



    private function AssignDataToTpl()
    {
        $this->data->assign('typeMoneyLst', $this->typeMoneyLst->paint());

        $this->data->assign('impostNameCtx', $this->impostNameCtx->paint());
        $this->data->assign('impostPercentCtx', $this->impostPercentCtx->paint());
        $this->data->assign('impostQuantityCtx', $this->impostQuantityCtx->paint());
        $this->data->assign('impostPriceCtx', $this->impostPriceCtx->paint());
        $this->data->assign('idViatic',$_GET['idViatic']);
        $this->data->assign('viaticsData',$this->GetDataViatics($_GET["idViatic"]));
        $this->data->assign('viaticsDataById',$this->AviaticsGetById($_GET["idViatic"]));
        $this->data->assign('DDP', $this->ddp);
        $this->data->assign('CFI', $this->cif);
        $this->data->assign('FCA', $this->fca);
        $this->data->assign('percents', $this->percents);

         $this->data->assign('tempParameter', time());
        $this->data->assign('jquery', $this->utilJQ->PaintJQ());
        $this->SetDataToTpl();
    }   
    private function SetDataToTpl()
    {
        echo $this->core->get($this->pageTpl, $this->data);
    }

    private function GetVoIdCompany($idCompany){
        $pricesCompanyVo = new PricesCompanyVo();
        $generalDao = new GeneralDao();
        $pricesCompanyVo->idCompany = $idCompany;
        $pricesCompanyVo->idSearchField = 4;
        $generalDao->GetByField($generalVo = clone $pricesCompanyVo);

        if($pricesCompanyVo2 = $generalDao->GetVo($generalVo = clone $pricesCompanyVo)){
                return  $pricesCompanyVo2;
          
      }
      return "Error al Realizar Consulta";
    }  


    private function CheckedValidate($validate){
        if($validate == 1){
            return "checked";
        }
        return false;
    } 

        private function GetCompany($idCompany)
    {
        $companyVo = new CompanyVo();
        $companyVo->id = $idCompany;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $companyVo);
        if ($companyVo2 = $generalDao->GetVo($generalVo = clone $companyVo)) {
            return "" . $companyVo2->name;
        }
        return "Problema en la consulta";
    } 
  

}   