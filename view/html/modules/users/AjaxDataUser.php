<?php
require_once(__DIR__ . '/../../../../controller/general/Config.php');
require_once(Config::PATH . Config::CONTROLLER.Config::GENERAL . 'ManageArrays.php');
require_once(Config::PATH . Config::BACKEND . 'modules/PersonVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/CompanyVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/UserVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/RoleVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/ModuleRoleVo.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralDao.php');

class AjaxDataUser
{

    private $isActionFromAjax;
    private $optionInputForm;
    private $idDataInputForm;
    public $dataList = [];

    function __construct()
    {
        if (isset($_POST['isActionFromAjax'])) {
            $this->isActionFromAjax = $_POST['isActionFromAjax'];
            $this->optionInputForm = $_POST['optionInputForm'];
            $this->idDataInputForm = $_POST['idDataInputForm'];
            if(isset($_POST["listData"])){
                $this->dataList = $_POST["listData"];  
            }
            if ($this->isActionFromAjax == "true") {
                if ($this->optionInputForm == 3) {
                    $this->SetDataInControls();
                }else if ($this->optionInputForm == 5) {
                    $this->UpdateStateUser();
                }else if($this->optionInputForm == 7){
                    $this->GetLengthUsers();
                }else if($this->optionInputForm == 9){
                    $this->GetIdUserSession();
                }else {
                    $this->GetDataInJsonForTbl();
                }
            }
        }
    }

    public function SetDataInControls()
    {
        $SetInputsDataFJQ = "";
        $personVo = new PersonVo();
        $personVo->id = $this->idDataInputForm;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $personVo);
        if ($personVo2 = $generalDao->GetVo($generalVo = clone $personVo)) {
            $SetInputsDataFJQ = "
                  <script>
                    (function($){
                                SetValueToInputText('personNamesCtx','{$personVo2->names}');
                                SetValueToInputText('personLastNamesCtx','{$personVo2->lastNames}');
                                SetValueToSelect('personDocumentTypeLst','{$personVo2->documentType}',true);
                                SetValueToInputText('personDocumentCtx','{$personVo2->document}');
                                SetValueToInputText('personCellPhoneCtx','{$personVo2->cellPhone}');
                                SetValueToInputText('personTelephoneCtx','{$personVo2->telephone}');
                                SetValueToInputText('personEmailCtx','{$personVo2->email}');
                                SetValueToInputText('personAddressCtx','{$personVo2->address}');
                                SetValueToInputText('personCityCtx','{$personVo2->city}');
                                SetValueToSelect('personIdRoleLst','{$personVo2->idRole}',true);
                                SetValueToSelect('personIdCompanyLst','{$personVo2->idCompany}',true);
                                $('#'+'user'+'PopUpAdd').modal('show');
                                SetValueToInputText('optionInputForm',4);
                                SetValueToInputText('idDataInputForm',$personVo2->id);
                      })(jQuery);
                  </script>
                            ";
        }
        echo $SetInputsDataFJQ;
    }

    public function GetLengthUsers(){
        $arrayList = array();
        $personVo = new userVo();
        $personVo->isList = true;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $personVo);
     
        print_r($generalDao->GetLength());
    
    }

    public function GetDataInJsonForTbl()
    {
        $existData = false;
        $personVo = new PersonVo();
        $personVo->isList = true;
        $generalDao = new GeneralDao();
     
        //$this->generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $personVo);

        $string = "
        {\"data\": [";
        while ($personVo2 = $generalDao->GetVo($generalVo = clone $personVo)) {
            $button = '           ';
            $json = array();
            $userVo = $this->GetUserVo($personVo2->id);
            if(!empty($userVo)){
                if($personVo2->state == 1){
                    $existData = true;
                $json[] = $personVo2->id;
                $json[] = htmlspecialchars(ucwords($personVo2->names));
                $json[] = htmlspecialchars(ucwords($personVo2->lastNames));
                $json[] = htmlspecialchars($personVo2->documentType);
                $json[] = htmlspecialchars($personVo2->document);
                $json[] = htmlspecialchars($personVo2->cellPhone);
                $json[] = htmlspecialchars($personVo2->telephone);
                $json[] = htmlspecialchars($personVo2->email);
                $json[] = htmlspecialchars($personVo2->address);
                $json[] = htmlspecialchars($personVo2->city);
                $json[] = $personVo2->creationDate;
                
                $userJsonVo =  ManageArrays::ObjectToJson($userVo);

                if($userVo->state == 1){
                    $json[] = "<button id='activateBtn{$userVo->id}' data-toggle='tooltip' data-placement='top' name='activateBtn{$userVo->id}' class='btn btn-success btn-xs red-tooltip' title='Desactivar' value='1' onclick='UpdateStateUser({$userVo->id},0,{$userJsonVo},\"user\")'><i class='fa fa-thumbs-up'></i></button> ";
                }else{
                    $json[] = "<button id='activateBtn{$userVo->id}' data-toggle='tooltip' data-placement='top'  name='activateBtn{$userVo->id}' class='btn btn-danger green-tooltip btn-xs' title='Activar' value='0' onclick='UpdateStateUser({$userVo->id},1,{$userJsonVo},\"user\")' ><i class='fa fa-thumbs-up'></i></button> ";
                }
                $json[] = $this->GetTypeRole($personVo2->idRole);
                $json[] = $this->GetCompany($personVo2->idCompany);
                
                if(!in_array( "editar_usuario" , $this->dataList)){
                    $button .= "<span class='yellow-tooltip ' data-toggle='tooltip' data-placement='top' title='Modificar'><a id='updateBtn' name='updateBtn' class='btn btn-warning btn-xs'  data-toggle='modal' data-target='#personPopUpAdd'   onclick='UpdateData({$personVo2->id},\"users\",\"user\")'><i class='fa fa-pencil'></i></a></span> ";
                }
                if(!in_array( "modificar_credenciales_usuario" , $this->dataList)){
                    $button .=    "<space data-toggle='tooltip' data-placement='top' title='Modificar Usuario y Clave' class='blue-tooltip' ><a id='userCredentialsBtn'  name='userCredentialsBtn' class='btn btn-primary btn-xs ' data-toggle='modal' data-target='#userCredencialsPopUp'  onclick='UpdateCredentials(".$userJsonVo.")'><i class='fa fa-user'></i></a></space> ";
                }
                if(!in_array( "historial_acceso" , $this->dataList)){
                    $button .=    "<a id='userHistoryAccessBtn' name='userHistoryAccessBtn' href='AccessHistory?idUser=".$personVo2->id."' class='btn btn-success btn-xs green-tooltip' data-toggle='tooltip' data-placement='top' title='Historial de Accesos'><i class='fa fa-clock-o'></i></a>";
                }
                if(!in_array( "eliminar_usuario" , $this->dataList)){
                    $button .=    "<a id='deleteBtn' name='deleteBtn' class='btn btn-danger red-tooltip  btn-xs' data-toggle='tooltip' data-placement='top' title='Eliminar' onclick='DeleteData({$personVo2->id},\"person\")'><i class='fa fa-times'></i></a> ";
                }
                $json[] = $button ;
               
                
                }
                
            $string .= json_encode($json) . ",";
         }
         
        }

        $string .= "-]}-";
        if($existData){
            echo  str_replace(",-]}-", "]}", $string);
        }else{
            echo "{ \"data\": [] }";
        }
    }

    function GetTypeRole($idRole)
    {
        $roleVo = new RoleVo();
        $roleVo->id = $idRole;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $roleVo);
        if ($roleVo2 = $generalDao->GetVo($generalVo = clone $roleVo)) {
            return "" . $roleVo2->name;
        }
        return "Problema en la consulta";
    }

    function GetCompany($idCompany)
    {
        $companyVo = new CompanyVo();
        $companyVo->id = $idCompany;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $companyVo);
        if ($companyVo2 = $generalDao->GetVo($generalVo = clone $companyVo)) {
            return "" . $companyVo2->name;
        }
        return "Problema en la consulta";
    }

    function GetUserVo($idPerson)
    {
        $userVo2 = new UserVo();
        $userVo = new UserVo();
        $userVo->idPerson = $idPerson;
        $userVo->idSearchField = 5;
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $userVo);
        if ($userVo2 = $generalDao->GetVo($generalVo = clone $userVo)) {
            return $userVo2;
        }
        return $userVo2;
    }

    function UpdateStateUser()
    {
        $userVo = new UserVo();
        $userVo->id = $this->idDataInputForm;
        $userVo->idSearchField = 0;
        $userVo->state = $_POST["state"];
        $userVo->idUpdateField = 3;
        $generalDao = new GeneralDao();
        $generalDao->UpdateByField($generalVo = clone $userVo);
    }


    function GetIdUserSession()
    {
            session_start();
            $userVo = unserialize($_SESSION['user']["user"]);
            $personVo = unserialize($_SESSION['user']["person"]);
            $this->GetModulesNotAllow($personVo->idRole);
    }


    function GetModulesNotAllow($idRole){
        $nodo = [];
        $userVo = unserialize($_SESSION['user']["user"]);
        $personVo = unserialize($_SESSION['user']["person"]);
        $rolesVo = new ModuleRoleVo();
        $rolesVo->idSearchField = 1;
        $rolesVo->idRol = $idRole;
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $rolesVo);
        while ($rolesVo2 = $generalDao->GetVo($generalVo = clone $rolesVo)) {
            if( $rolesVo2->state == '0'){
                $nodo[] = $rolesVo2->idModule;
            }
        }
        
       

        print_r (json_encode($nodo));
    }


}

$ajaxDataUser = new AjaxDataUser();
