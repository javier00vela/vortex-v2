// jshint unused:true
"use strict";

function ModifyListData() {
    var options = $('#personIdRoleLst option');
    $.map(options, function(option) {
        if (option.innerHTML == "Cliente" || option.innerHTML == "Proveedor" || option.innerHTML == "Prospecto") {
            option.remove();
        }
    });
}

function SeePass() {
    $("#password").click(function() {
        if ($("#userPasswordCtx").attr("type") === "password") {
            return $("#userPasswordCtx").attr("type", "text");
        }
        return $("#userPasswordCtx").attr("type", "password");
    });
}

function LoadTable() {
    RemoveActionModule(2);
    GetArrayFromDataModule(2);
    setTimeout(function() {
        var parameters = {
            "isActionFromAjax": true,
            "optionInputForm": 0,
            "idDataInputForm": 0,
            "listData": arrayResponse,
        };


        $('#userTbl').DataTable({
            responsive: true,
            "drawCallback": function(settings) {
                $('[data-toggle="tooltip"]').tooltip()
            },
            language: {
                "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
            },
            "ajax": {
                url: "view/html/modules/users/AjaxDataUser.php",
                method: "POST",
                data: parameters
            }
        });
    }, 500);
}

function ControlsDatePikers() {
    $('#personBirthdayCtx').datepicker({
        autoclose: true
    });
}

/***************************UPDATE STATE**************************/
function UpdateStateUser(idUser, state, userVo, idModule) {

    SetValueToInputText("optionInputForm", 5);
    SetValueToInputText("idDataInputForm", idUser);
    SetValueToInputText("isActionFromAjax", "true");
    var parameters = {
        "isActionFromAjax": GetValueToInputText("isActionFromAjax"),
        "optionInputForm": GetValueToInputText("optionInputForm"),
        "idDataInputForm": GetValueToInputText("idDataInputForm"),
        "state": state
    };
    //console.log(parameters);
    $.ajax({
        data: parameters,
        url: 'view/html/modules/users/AjaxData' + UpperFirstLetter(idModule) + '.php',
        type: 'POST',
        success: function(response) {
            $("#containerResponseAjax").html(response);
        }
    });

    $('#userTbl tbody').on('click', 'button', function() {
        if ($(window).width() < 1625) {
            if (state === 0) {

                userVo = JSON.stringify(userVo);
                $(this).removeClass('btn-success');
                $(this).addClass('btn-danger');
                $(this).html('<i class=\'fa fa-thumbs-up\'></i>');
                $(this).attr('value', 0);
                $(this).attr('onclick', "UpdateStateUser(" + idUser + ",1," + userVo + ",'user')");
            } else if (state === 1) {
                userVo = JSON.stringify(userVo);
                $(this).removeClass('btn-danger');
                $(this).addClass('btn-success');
                $(this).html('<i class=\'fa fa-thumbs-up\'></i>');
                $(this).attr('value', 1);
                $(this).attr('onclick', "UpdateStateUser(" + idUser + ",0," + userVo + ",'user')");
            }
        }

    });

    if (state === 0) {
        userVo["state"] = "0";
        userVo = JSON.stringify(userVo);
        $("#activateBtn" + idUser).removeClass('btn-success');
        $("#activateBtn" + idUser).addClass('btn-danger');
        $("#activateBtn" + idUser).html('<i class=\'fa fa-thumbs-up\'></i>');
        $("#activateBtn" + idUser).attr('value', 0);
        $("#activateBtn" + idUser).attr('onclick', "UpdateStateUser(" + idUser + ",1," + userVo + ",'user')");
    } else if (state === 1) {
        userVo["state"] = "1";
        userVo = JSON.stringify(userVo);
        $("#activateBtn" + idUser).removeClass('btn-danger');
        $("#activateBtn" + idUser).addClass('btn-success');
        $("#activateBtn" + idUser).html('<i class=\'fa fa-thumbs-up\'></i>');
        $("#activateBtn" + idUser).attr('value', 1);
        $("#activateBtn" + idUser).attr('onclick', "UpdateStateUser(" + idUser + ",0," + userVo + ",'user')");
    }

    UpdateCredentials(userVo);
}

function UpdateCredentials(userVo) {
    if (userVo['id'] === undefined) {
        userVo = JSON.parse(userVo);
    }
    SetValueToInputText("userIdInputForm", userVo['id']);
    SetValueToInputText("userNicknameCtx", userVo['nickname']);
    SetValueToInputText("userPasswordCtx", userVo['password']);
    SetValueToInputText("userStateInputForm", GetValueToInputText("activateBtn" + userVo['id']));
    $("#userPhotoImg").attr('src', userVo['photo']);
    SetValueToInputText("userIdPersonInputForm", userVo['idPerson']);
}

/***********************************************************************/

/*********************************CLICK EVENTS**************************/

$("#userFormBtn").click(function() {
        SetValueToInputText("isUpdateUserForm", 1);
        $("#userForm").submit();
    })
    /*******************************CHANGE EVENTS*************************************/
$("#userPhotoUploadFile").change(function() {
        var img = this.files[0];
        if (img["type"] !== "image/jpeg" /*&& img["type"] !== "image/png"*/ ) {
            $("#userPhotoUploadFile").val("");
            MessageSwalBasic("Error al Subir Imagen", "Los Tipos de Imagen Permitidos son: JPEG", "error");
        } else if (img["type"] > 27000000) {
            $("#userPhotoUploadFile").val("");
            MessageSwalBasic("Error al Subir Imagen", "¡La Imagen no Debe Pesar mas de 50MB", "error");
        } else {
            var dataImg = new FileReader;
            dataImg.readAsDataURL(img);
            $(dataImg).on("load", function(event) {
                var urlImg = event.target.result;
                $("#userPhotoImg").attr("src", urlImg);
            })
        }
    })
    /******************************************************************************/
$(function() {

    ModifyListData();
    ControlsDatePikers();
    SeePass();
    LoadTable();
});

/*
//function ReplaceImage(){
function ConvertImgToJson(img){
    console.log(img);
    var file_tmp_name = {
        tmp_name:img["file"]
    }
    var fileData = {
        file: {
            modified: img.lastModifiedDate,
            tpm_name: img.tpm_name,
            name: img.name,
            size: img.size,
            type: img.type
        }
    }
    alert(JSON.stringify(file_tmp_name));
    return JSON.stringify(fileData);
}*/