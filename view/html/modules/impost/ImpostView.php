<?php
require_once(Config::PATH . Config::CONTROLLER . 'general/AccessManager.php');
require_once(Config::PATH . Config::CONTROLLER . 'general/UtilJquery.php');
require_once(Config::PATH . Config::VIEW_HTML . 'general/comps/GeneralCtx.php');
require_once(Config::PATH . Config::VIEW_HTML . 'general/comps/GeneralLst.php');
require_once(Config::PATH . Config::VIEW_HTML . 'general/comps/GeneralWithDataLst.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralDao.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/ImpostVo.php');

require_once(Config::PATH . Config::CONTROLLER . 'general/MailsUtils.php');

class ImpostView
{
    private $optionInputForm;
    private $isUpdateUserForm;
    private $idDataInputForm;
    /*=============================================
                  Session Vars
    =============================================*/
  
    /*=============================================
                  Template Vars
    =============================================*/
    private $utilJQ;
    private $pageTpl, $data, $core;
    /*=============================================
                Controls Vars HTML
    =============================================*/
    private $impostNameCtx;
    private $impostPercentCtx;

    /*=============================================
                Data Vars
    =============================================*/
    private $generalDao;


    function __construct($pageTpl, $data, $core)
    {
        $this->data = $data;
        $this->core = $core;
        $this->pageTpl = $pageTpl;
        $this->CreateComponents();
        $this->UseDataFromThePostback();
    }

    private function UseDataFromThePostback()
    {
        if (isset($_POST['optionInputForm'])) {
            $this->optionInputForm = $_POST['optionInputForm'];
            $this->idDataInputForm = $_POST['idDataInputForm'];
            $this->generalDao = new GeneralDao();
            switch ($this->optionInputForm) {
                case "1": {
                    $this->SetData();
                    break;
                }
                case "2": {
                    $this->DeleteData();
                    break;
                }
                case "4": {
                    $this->SetUpdate();
                    break;
                }
            }
        }

        $this->AssignDataToTpl();

    }

    public function Testing(){
        $MailsUtils = new MailsUtils();
        $word = [ "one" => "" , "two" => ""];
        $MailsUtils->AddDestinatario("javier00vela@gmail.com");
        $array = $MailsUtils->GetArrayModel();
        $array["Event"] = 1;
        $array["idRol"] = 3;
        $array["WordReplace"] = ["#dia"];
        $array["ToReplace"] = [" el dia de hoy "];
        $array["Alias"] = "vortex";
        $word["one"] = "1";
        $word["two"] = "1";
        $MailsUtils->SendWithSame($array , $word );
    }

    private function CreateComponents()
    {
        $this->impostNameCtx = new GeneralCtx("impostNameCtx","Nombre",null,true);
        $this->impostPercentCtx = new GeneralCtx("impostPercentCtx","Porcentaje",null,true);
       
        $this->utilJQ = new UtilJquery("Impost");
    }
    
    private function SetDataToVo()
    {
        $impostVo = new ImpostVo();
        $impostVo->id = $this->idDataInputForm;
        $impostVo->name = $_POST["impostNameCtx"];
        $impostVo->percent = $_POST["impostPercentCtx"];
       
        return $impostVo;
    }
    private function SetData()
    {
        $impostVo = $this->SetDataToVo();
        $impostVo->id = null;
        $result = $this->generalDao->Set($generalVo = clone $impostVo);
        if (isset($result)) {
            $message = 'MessageSwalBasic("Registrado!","Impuesto Registrado Correctamente","success");';
            $this->utilJQ->AddFunctionJavaScript($message);
        }

    }
    private function SetUpdate()
    {
        $result = $this->generalDao->Update($generalVo = clone $this->SetDataToVo());
        if (isset($result)) {
            $message = 'MessageSwalBasic("Modificado!","Impuesto Modificado Correctamente","success");';
            $this->utilJQ->AddFunctionJavaScript($message);
        }
    }
    private function DeleteData()
    {
        $message = "";
        $impostVo = new ImpostVo();
        $impostVo->id = $this->idDataInputForm;
        if ($this->generalDao->Delete($generalVo = clone $impostVo)) {
            $message = 'MessageSwalBasic("Eliminado!","Impuesto Eliminado Correctamente","success");';
        } else {
            $message = 'MessageSwalBasic("Error!","Problema con la base de datos","error");';
        }
        $this->utilJQ->AddFunctionJavaScript($message);
    }

    private function AssignDataToTpl()
    {
        $this->data->assign('impostNameCtx', $this->impostNameCtx->paint());
        $this->data->assign('impostPercentCtx', $this->impostPercentCtx->paint());
         $this->data->assign('tempParameter', time());
        $this->data->assign('jquery', $this->utilJQ->PaintJQ());
        $this->SetDataToTpl();
    }   
    private function SetDataToTpl()
    {
        echo $this->core->get($this->pageTpl, $this->data);
    }
  


}   