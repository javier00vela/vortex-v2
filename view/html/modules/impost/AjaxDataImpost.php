<?php
require_once(__DIR__ . '/../../../../controller/general/Config.php');
require_once(Config::PATH . Config::CONTROLLER.Config::GENERAL . 'ManageArrays.php');
require_once(Config::PATH . Config::BACKEND . 'modules/ImpostVo.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralDao.php');

class AjaxDataImpost{

    private $isActionFromAjax;
    private $optionInputForm;
    private $idDataInputForm;
    public $dataList = [];

    function __construct()
    {
        if (isset($_POST['isActionFromAjax'])) {
            $this->isActionFromAjax = $_POST['isActionFromAjax'];
            $this->optionInputForm = $_POST['optionInputForm'];
            $this->idDataInputForm = $_POST['idDataInputForm'];
            if(isset($_POST["listData"])){
                $this->dataList = $_POST["listData"];  
            }
            if ($this->isActionFromAjax == "true") {
                if ($this->optionInputForm == 3) {
                    $this->SetDataInControls();
                }else if($this->optionInputForm == 5){
                $this->GetLengthImpost();
                }else {
                    $this->GetDataInJsonForTbl();
                }
            }
        }
    }

    private function SetDataInControls(){
        $SetInputsDataFJQ = "";
        $impostVo = new ImpostVo();
        $impostVo->id = $this->idDataInputForm;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $impostVo);
        if($impostVo2 = $generalDao->GetVo($generalVo = clone $impostVo)){
            $SetInputsDataFJQ = "
                            <script>
                               (function($){
                                SetValueToInputText('impostNameCtx','{$impostVo2->name}');
                                SetValueToInputText('impostPercentCtx','{$impostVo2->percent}');
                                SetValueToInputText('optionInputForm',4);
                                SetValueToInputText('idDataInputForm',$impostVo2->id);
                                
                               })(jQuery);
                            </script>
                        ";
        }
        echo $SetInputsDataFJQ;
    }

    public function GetDataInJsonForTbl()
    {
        $existData = false;
        $impostVo = new ImpostVo();
        $impostVo->isList = true;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $impostVo);

        $string = "
        {\"data\": [";
        while ($impostVo2 = $generalDao->GetVo($generalVo = clone $impostVo)) {
            $existData = true;
            $json = array();
           // $json[] = $impostVo2->id;
            $json[] = htmlspecialchars(ucfirst(strtolower($impostVo2->name)));
            $json[] = $impostVo2->percent;
            $button = "";
            if(ucfirst(strtolower($impostVo2->name)) != "Iva"){
                if(!in_array( "modificar_impuesto" , $this->dataList)){
                $button .= "<tip title='Modificar' class='yellow-tooltip' data-toggle='tooltip' data-placement='top'><button id='updateBtn' name='updateBtn' class='btn btn-warning btn-xs' data-toggle='modal' data-target='#impostPopUpAdd' title='Modificar' onclick='UpdateData({$impostVo2->id},\"impost\",\"impost\");'><i class='fa fa-pencil'></i></button> </tip>";
                }
                if(!in_array( "eliminar_costo" , $this->dataList)){
                $button .= " <tip title='Eliminar' class='red-tooltip' data-toggle='tooltip' data-placement='top'><a id='deleteBtn' name='deleteBtn' class='btn btn-danger  btn-xs' title='Eliminar' onclick='DeleteData({$impostVo2->id},\"impost\")'><i class='fa fa-times'></i></a></tip> ";
                }
            }else{
                if(!in_array( "modificar_impuesto" , $this->dataList)){
                     $button .= "<tip title='Modificar' class='yellow-tooltip' data-toggle='tooltip' data-placement='top'><button id='updateBtn' name='updateBtn' class='btn btn-warning btn-xs' data-toggle='modal' data-target='#impostPopUpAdd' title='Modificar' onclick='UpdateData({$impostVo2->id},\"impost\",\"impost\");'><i class='fa fa-pencil'></i></button> </tip>";
                }
            }
                $json[] = $button;

            $string .= json_encode($json) . ",";

        }
        $string .= "-]}-";

        if($existData){
            echo  str_replace(",-]}-", "]}", $string);
        }else{
            echo "{ \"data\": [] }";
        }
    }

       public function GetLengthImpost(){
        $arrayList = array();
        $personVo = new impostVo();
        $personVo->isList = true;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $personVo);
        print_r($generalDao->GetLength());
    }

    
}

$ajaxDataImpost = new AjaxDataImpost();
