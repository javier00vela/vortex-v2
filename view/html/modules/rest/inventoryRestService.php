<?php
require_once(__DIR__ . '/../../../../controller/general/Config.php');
require_once(Config::PATH . Config::CONTROLLER.Config::GENERAL . 'ManageArrays.php');
require_once(Config::PATH . Config::BACKEND . 'modules/CompanyVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/ProductVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/InventoryVo.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralDao.php');
require_once(Config::PATH . Config::BACKEND . 'modules/PricesCompanyVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/AditionalsImpostVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/HistoryTRMVo.php');
require_once(Config::PATH . Config::CONTROLLER_LIB . 'vendor/phpoffice/phpexcel/Classes/PHPExcel/IOFactory.php');


class InventoryRestService
{
    public $tasaEUR;
    public $tasaUSD;
    


    public function __construct(){
        $this->tasaEUR = $this->GetTasa("EUR");
        $this->tasaUSD = $this->GetTasa("USD");
    }


    public function SendJSONproducts()
    {
        

        $existData = false;
        $inventoryVo = new InventoryVo();
        $inventoryVo->isList = true;
        $generalDao = new GeneralDao();
        $generalDao->CustomQuery("select * from  (select * from inventory Limit 0 , 10 ) inventory ORDER BY id desc");
        $string = "
        {\"data\": [";
        while ($inventoryVo2 = $generalDao->GetVo($generalVo = clone $inventoryVo)) {
            $existData = true;
            $json = array();
            $json[] = $inventoryVo2->id;
            $json[] = htmlspecialchars($inventoryVo2->typeProduct);
            $json[] = htmlspecialchars(ucwords(strtolower($inventoryVo2->name)));
            $json[] = htmlspecialchars($inventoryVo2->brand);
            $json[] = htmlspecialchars($inventoryVo2->reference);
            $json[] =$inventoryVo2->amount;
            $json[] = $inventoryVo2->typeCurrency;
            $json[] = $inventoryVo2->presentation;
            $json[] = ucfirst(strtolower($inventoryVo2->country));
            $json[] = $inventoryVo2->nameCompany;
            $json[] = $inventoryVo2->namePlace;
            $json[] = $this->GetPriceToSell($inventoryVo2);

            $string .= json_encode($json) . ",";
        }
        $string .= "-]}-";
        if($existData){
            echo  str_replace(",-]}-", "]}", $string);
        }else{
            echo "{ \"data\": [] }";
        }

    }

    public function GetPriceToSell($inventoryVo2){
        if($inventoryVo2->typeCurrency == "EUR"){
            $priceCOP =  $this->tasaEUR * str_replace(",",".",str_replace(".","",$inventoryVo2->priceUnit));
        }else if($inventoryVo2->typeCurrency == "USD"){
            $priceCOP =  $this->tasaUSD * str_replace(",",".",str_replace(".","",$inventoryVo2->priceUnit));
        }else{
            $priceCOP = $inventoryVo2->priceUnit;  
        }
        return $this->CalculateInternationalPrice($priceCOP , $inventoryVo2);
    }

    private function CalculateInternationalPrice($priceCOP , $inventoryVo){
        if ($inventoryVo->namePlace == "Internacional") {
            $dataGlobalPrices = $this->GetDataCompanyPrices($inventoryVo->nameCompany);
            return $this->GetCalculePrice($priceCOP, $dataGlobalPrices[1],$dataGlobalPrices[0], "DDP");
        }else{
            return $priceCOP;
        }
    }

    private function GetCalculePrice($priceNormal, $arrayAditionalsImpost, $arrayPricesCompany, $typeQuotation) {
        $impost = $this->GetPriceWithImpost($priceNormal, $arrayAditionalsImpost, $typeQuotation);
        // hasta aqui se suman bien el precio
        $priceTotalVenta = $this->GetPriceSale($priceNormal, $arrayPricesCompany[0]->ganancia, $impost); //se le suma la ganancia propuesta por el proveedor 
        //console.log("precio total con ganancia --> "+priceTotalVenta);
        return $priceTotalVenta;
    }

    private function GetPriceWithImpost($price, $porcentajes, $typeQuotation) {

        $totalPercent = 0;
        $porcentaje = 0;
        for ($i = 0; $i < count($porcentajes); $i++) {
            if ($porcentajes[$i]->DDP == 1) {

                $porcentaje += $this->GetPercent(floatval(str_replace(",",".",$porcentajes[$i]->percent)));
            }

        }

        return $porcentaje;
    }

    private function GetPercent ($percent) {
        $percent2 = "";
        //console.log("porcentaje ingresado : "+percent);
        $percent2 = ($percent / 100);

        return $percent2;
    }

    private function GetPriceSale($price, $sale, $impost) {
        //console.log("================================= se va a calcular el impuesto y el porcentaje ===================================");
        $sale = $this->GetPercent($sale);
        $sale = $impost + $sale;
        //console.log("la suma del impuesto y el procentaje es ====== "+sale);
        $price = $price + ($price * $sale);
        //console.log("se obtiene el precio total con ganancia apartir de esta formulala  "+price+"+"+"("+price+"*"+sale+")");
        //console.log("el resultado obtenido es "+price);
        return $price;
    }

    

    function GetDataCompanyPrices($nameCompany){
        $dataPricesCompany = array();
        $idCompany = $this->GetIdCompanyForName($nameCompany);
        $dataPricesCompany = $this->GetDataPricesCompany($idCompany);
        $dataAditionalsImpost = $this->GetDataAditionalsImpostForCompany($dataPricesCompany[0]->id);
        $dataGlobalPrices = array($dataPricesCompany, $dataAditionalsImpost);
        return  $dataGlobalPrices;
       
    }
    function GetIdCompanyForName($nameCompany){
        $roleCompanyVo = new companyVo();
        $roleCompanyVo->name = $nameCompany;
        $roleCompanyVo->idSearchField = 1;
        $generalDao = new GeneralDao();
       
        $generalDao->GetByField($generalVo = clone $roleCompanyVo);
        if($roleCompanyVo2 = $generalDao->GetVo($generalVo = clone $roleCompanyVo)) {
           // print_r($roleCompanyVo2);
             return $roleCompanyVo2->id;
        }
    }
    function GetDataPricesCompany($idCompany){
        $dataPricesCompany = array();
        $companyVo = new PricesCompanyVo();
        $companyVo->idCompany = $idCompany;
        $companyVo->idSearchField = 4;
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $companyVo);
        while($companyVo2 = $generalDao->GetVo($generalVo = clone $companyVo)) {
           
            array_push($dataPricesCompany,$companyVo2);
        }
        return $dataPricesCompany;
    }


    function GetDataAditionalsImpostForCompany($idPricesCompany){
        $dataAditionalsImpost = array();
        if(isset($idPricesCompany)){
            $aditionalsImpostVo = new AditionalsImpostVo();
            $aditionalsImpostVo->idPricesCompany = $idPricesCompany;
            $aditionalsImpostVo->idSearchField = 3;
            $generalDao = new GeneralDao();
            $generalDao->GetByField($generalVo = clone $aditionalsImpostVo);
            while($aditionalsImpostVo2 = $generalDao->GetVo($generalVo = clone $aditionalsImpostVo)) {
                array_push($dataAditionalsImpost,$aditionalsImpostVo2);
            }
        }
        return $dataAditionalsImpost;
    }


    public function GetTasa($field){
        $arrayList = array();
        $HistoryTRMVo = new HistoryTRMVo();
        $HistoryTRMVo->isList = true;
        $generalDao = new GeneralDao();
        $generalDao->CustomQuery("SELECT * FROM {$HistoryTRMVo->nameTable} WHERE DATE(DATE) >= CURDATE() AND typeMoney='{$field}' ORDER BY DATE DESC , id desc LIMIT 1");
        while($HistoryTRMVo2 = $generalDao->GetVo($generalVo = clone $HistoryTRMVo)) {
          return $HistoryTRMVo2->money;
        }   
        return 0;
    }

}

