<?php
require_once(Config::PATH . Config::CONTROLLER . 'general/AccessManager.php');
require_once(Config::PATH . Config::CONTROLLER . 'general/UtilJquery.php');
require_once(Config::PATH . Config::VIEW_HTML . 'general/comps/GeneralCtx.php');
require_once(Config::PATH . Config::VIEW_HTML . 'general/comps/GeneralLst.php');
require_once(Config::PATH . Config::VIEW_HTML . 'general/comps/GeneralWithDataLst.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralDao.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/PersonVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/CompanyVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/UserVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/RoleVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/VisitsVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/VisitsDescriptionVo.php');

class VisitsDescriptionView
{
    private $optionInputForm;
    private $isUpdateUserForm;
    private $idDataInputForm;
    /*=============================================
                  Session Vars
    =============================================*/
    private $manageSession;
    /*=============================================
                  Template Vars
    =============================================*/
    private $utilJQ;
    private $pageTpl, $data, $core;
    /*=============================================
                Controls Vars HTML
    =============================================*/

    private $clientName;
    private $dateCita;
    private $productDescriptionAtx;
    

    /*=============================================
                Data Vars
    =============================================*/
    private $generalDao;


    function __construct($pageTpl, $data, $core)
    {
        $this->data = $data;
        $this->core = $core;
        $this->pageTpl = $pageTpl;

            $this->CreateComponents();
            $this->UseDataFromThePostback();
        
    }

    private function ValidateIfExistVisits($id){
        if(empty($this->GetVisitsById($id)->id)){
            header("location: DontFind");
        }
    }


     private function GetVisitsById($id){
        $VisitsVo = new VisitsVo();
        $generalDao = new GeneralDao();
        $VisitsVo->id = $id;
        $generalDao->GetById($generalVo = clone $VisitsVo);

        if($VisitsVo2 = $generalDao->GetVo($generalVo = clone $VisitsVo)){
            return  $VisitsVo2;  
         }

        return "Error al Realizar Consulta";
    }

    private function UseDataFromThePostback()
    {

        if (isset($_POST['optionInputForm'])) {
            $this->optionInputForm = $_POST['optionInputForm'];
            $this->idDataInputForm = $_POST['idDataInputForm'];
            $this->generalDao = new GeneralDao();
            switch ($this->optionInputForm) {
                case "1": {
                    $this->SetData();
                    break;
                }
                case "2": {
                    $this->DeleteData();
                    break;
                }
                case "4": {
                    $this->SetUpdate();
                    break;
                }
            }
            //echo '<script>window.location = "Users";</script>';
        }
        if (isset($_POST['isUpdateUserForm'])) {
            $this->isUpdateUserForm = $_POST['isUpdateUserForm'];
            if($this->isUpdateUserForm == "1"){
                $this->UpdateUser();
            }
        }
        $this->AssignDataToTpl();

    }

    private function CreateComponents()
    {
        $this->dateCita = new GeneralCtx("dateCita",null,"date",true);
         $this->productDescriptionAtx = new GeneralCtx("productDescriptionAtx",null,"date",true);
        $this->utilJQ = new UtilJquery("VisitsDescription");
    }


 function GetNameClient($idVisit)
    {
        $array = [];
        $VisitVo = new VisitsVo();
        $VisitVo->id = $idVisit;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $VisitVo);
        if ($VisitVo2 = $generalDao->GetVo($generalVo = clone $VisitVo)) {
            $personVo = new PersonVo();
            $personVo->id = $VisitVo2->idClient;
            $generalDao->GetById($generalVo = clone $personVo);
            if ($personVo2 = $generalDao->GetVo($generalVo = clone $personVo)) {
                return $personVo2->names." ".$personVo2->lastNames;
            }
        }
            return "error en la consulta ";
    }

     function GetCompanys()
    {
        $array = [];
        $companyVo = new CompanyVo();
        $companyVo->isList = true;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $companyVo);
        while ($companyVo2 = $generalDao->GetVo($generalVo = clone $companyVo)) {
            if($companyVo2->idRoleCompany != 1 && $companyVo2->idRoleCompany != 3 ){
                $array[] = $companyVo2->id."_".$companyVo2->name;
            }
           
        }
            return $array;
    }

    private function SetDataToVo()
    {
        $visitsVo = new VisitDescriptionVo();
        $visitsVo->id = $this->idDataInputForm;
        $visitsVo->description = $_POST["productDescriptionAtx"];
        $visitsVo->idVisits = $_GET["idVisit"];
        $visitsVo->dateCreate = date('Y-m-d H:i:s');
        $visitsVo->responsable = $this->GetIdUserSession();
        if(isset($_POST["dateCita"])){
             $visitsVo->dateCita = $_POST["dateCita"];
        }else{
            $visitsVo->dateCita = "sin novedades de cita";
        }

        return $visitsVo;
    }

    private function SetData()
    {
        $visitsVo = $this->SetDataToVo();
        $visitsVo->id = null;
        $result = $this->generalDao->Set($generalVo = clone $visitsVo);
        if (isset($result)) {
            $message = 'MessageSwalBasic("Registrado!","Descripción Registrada Correctamente","success");';
            $this->utilJQ->AddFunctionJavaScript($message);
        }

    }

    private function SetUpdate()
    {
        $visitsVo = new VisitDescriptionVo();
        $visitsVo->id = $_POST["idInfo"];
      $visitsVo->idUpdateField = 1;
        $visitsVo->description = $_POST["productDescriptionAtx"];
       $visitsVo->idSearchField = 0;
        $result = $this->generalDao->UpdateByField($generalVo = clone $visitsVo);
        //if (isset($result)) {
            $message = 'MessageSwalBasic("Modificado!","Descripción Modificado Correctamente","success");';
          //
            $this->utilJQ->AddFunctionJavaScript($message);
        //}
    }
    private function DeleteData()
    {
        $message = "";
        $personVo = new PersonVo();
        $personVo->id = $this->idDataInputForm;
        $personVo->idUpdateField = 14;
        $personVo->isList = false;
        $personVo->state = 0;
        $personVo->idSearchField = 0;
        $this->DeleteUser($personVo->id);
        if ($this->generalDao->UpdateByField($generalVo = clone $personVo)) {
            
        } 
        
         $message = 'MessageSwalBasic("Eliminado!","Registro Eliminado Correctamente","success");';
        $this->utilJQ->AddFunctionJavaScript($message);
    }


    private function GetIdUserSession(){
        $personVo = unserialize($_SESSION['user']["person"]);
        return $personVo->id;
    }


    private function AssignDataToTpl()
    {
        $this->data->assign('dateCita', $this->dateCita->paint());
        $this->data->assign('productDescriptionAtx', $this->productDescriptionAtx->paint());
        $this->data->assign('nameClient', $this->GetNameClient($_GET["idVisit"]));
        $this->data->assign('idVisit', $_GET["idVisit"]);
        $this->data->assign('jquery', $this->utilJQ->PaintJQ());
         $this->data->assign('tempParameter', time());
        $this->SetDataToTpl();
    }

    private function SetDataToTpl()
    {
        echo $this->core->get($this->pageTpl, $this->data);
    }

}