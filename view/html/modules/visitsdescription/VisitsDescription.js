function DesactiveDate() {
    $("#dateCita").attr("disabled", "true");
    if ($("#productDescriptionAtx").val() != "") {
        $("#submit").attr("form", "VisitsForm");
    }
}

function loadDescription(description, id) {
    $("#idInfo").attr("value", id);
    CKEDITOR.instances["productDescriptionAtx"].setData(description);
}

function LoadTable() {
    RemoveActionModule(15);
    GetArrayFromDataModule(15);
    setTimeout(function() {
        var parameters = {
            "isActionFromAjax": true,
            "optionInputForm": 0,
            "idDataInputForm": 0,
            "listData": arrayResponse,
            "idVisit": GetValueToInputText("idVisit")
        };

        $('#userTbl').DataTable({
            responsive: true,
            language: {
                "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
            },
            "ajax": {
                url: "view/html/modules/visitsdescription/AjaxDataVisitsDescription.php",
                method: "POST",
                data: parameters
            }
        });
    }, 500);
}

function ActivateDateCita() {
    $("#ActivateBtn").click(function() {
        if ($(this).hasClass("active")) {
            $(this).attr("class", "desactive btn btn-danger btn-block");
            $(this).text("No Hubo Acuerdo");
            $("#dateCita").removeAttr('disabled');
        } else {
            $(this).attr("class", "active btn btn-success btn-block");
            $(this).text("Hubo Acuerdo");
            $("#dateCita").val('0000-00-00');
            $("#dateCita").attr("disabled", "true");
        }
    })
}

function ValidateIssetAnythingDescription() {
    $("#submit").click(function() {
        $("#submit").attr("form", "VisitsForm");
    })

}




$(function() {
    DesactiveDate();
    ActivateDateCita();
    LoadTable();
    ValidateIssetAnythingDescription();

});