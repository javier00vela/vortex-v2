//===========================Modify Quotation ==============================//

var idQuotation = 0;
var dataArray = [];
var change = 0;
var formatter2 = new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'USD',
    minimumFractionDigits: 2

})

function CleanColorFormulario() { // valida si todos los campos estan selecionados con datos

    $(".box-body select , .box-body input").each(function() {
        $(this).css("border-color", "#d2d6de");
    });
}

function GetModifyQuotation() {
    NotShowLoad();

    var data = $("#ConsultQuotation").val();
    if (data) {
        dataArray.push(JSON.parse(data));
        console.log(dataArray);
        
        if (dataArray[0]["quotationData"][0]["typeMoney"] == "USD") {
            $("#USD").trigger("click");
        }

        if (dataArray[0]["quotationData"][0]["typeMoney"] == "EUR") {
            $("#EUR").trigger("click");

        }
        
        SetPersonCreate(dataArray[0]);
        SetDataQuotation(dataArray[0]);
        SetProducts(dataArray[0]);
        SetCompany(dataArray[0]);
        SetPersons(dataArray[0]);
        SetOptionsQuotation(dataArray[0]);

    }
}

function SetPersons(data) {
    var cont = 0;
    $("#contactsContainer").attr("style", "border: 1px solid #eee;");
    for (var i = 0; i < data["persons"].length; i++) {
        $("[name*=checkbox]").each(function(){
            if($(this).attr("id") == data["persons"][i]["idPerson"] ){
             $(this).click();
            }
        });
        //$("#contactsContainer").append("<div class='checkbox Selected ' style='margin-left: 6px;'><label><input type='checkbox' checked class='" + data["persons"][i]["idCompany"] + "' id='" + data["persons"][i]["idPerson"] + "' name='checkbox' value='" + data["persons"][i]["completeName"] + "'>" + data["persons"][i]["completeName"] + "<input type='hidden' name='idCompany' value='" + data["persons"][i]["idCompany"] + "'><input type='hidden' name='idUser' value='" + data["persons"][i]["idPerson"] + "'></label></div>");
    }

}

function arreglarCP(addr, posicion) {
    return addr.slice(0, posicion) + " " + (addr).slice(posicion);
}

function SetDataQuotation(data) {
    idQuotation = data["quotationData"][0]["code"];
    $("#code").attr("value", data["quotationData"][0]["code"]);
    $("#versionQuotation").attr("value", data["quotationData"][0]["version"]);
    $("#idQuotation").attr("value", data["quotationData"][0]["id"]);
    $("#dateUpdateCtx").val(data["quotationData"][0]["dateUpdate"]);
    SetIsSubDistirbuidor(data["quotationData"][0]["idCompany"]);
    $("#quotationViaticLst").attr("disabled","true");
    checkeable = data["quotationData"][0]["checkeable"];
    $("#status-quotation").attr("value", data["quotationData"][0]["state"]);
    $("#quotationViaticLst").val(data["quotationData"][0]["idViatic"]);
    $("#quotationViaticLst").trigger("change");
    $("#viaticosPopUpAdd").modal('hide');
    $("#totalPrice").attr("value", "$" + ConvertNumber(parseFloat(data["quotationData"][0]["total"])));
    $("#quotationCompanyLst").removeAttr("disabled");

    $("#totalIva").attr("value", "$" + ConvertNumber(parseInt((data["quotationData"][0]["IVA"] / 100) * data["quotationData"][0]["total"])));

    typeQuotationBool = data["quotationData"][0]["isDDP"];

    if (data["quotationData"][0]["typeMoney"] != "COP") {
        setTimeout(() => {
            CleanColorFormulario();
            $(".swal2-container").remove();
        }, 5000);
    } else {
        setTimeout(() => {
            CleanColorFormulario();
            $(".swal2-container").remove();
        }, 4000);
    }
    //alert(isSubDistribuidor);
}

function SetPersonCreate(person) {
    var cont = 0;
    $("#quotationUserLst option").removeAttr('selected');
    $("#quotationUserLst option").each(function() {
        if ($(this).text() == person["person"][0]["completeName"].split(" ")[0]) {
            $(this).attr("selected", "true");
        }
        cont++;
    });
}

function SetCompany(company) {
    var cont = 0;
    $("#quotationCompanyLst option").removeAttr('selected');
    $("#quotationCompanyLst option").each(function() {
        if ($(this).attr("value") == company["quotationData"][0]["idCompany"]) {
            $(this).attr("selected", "true");
        }
        cont++;
    });

}

function DisabledButton(id) {
    var rows = tbl.row($("#content-" + id).attr("data-row-index")).data();
    for (var i = 0; i < 10; i++) {

        values = rows[10].toString();

        values = values.replace("disabled", " none ");
        value = ConvertNumber(parseInt($("#precioOriginalCtx-" + data["id"]).attr("data-price"))); //precio indivual del prodcuto original);
    }

}

function SetProducts(products) {
    for (var i = 0; i < products["product"].length; i++) {
        CreateContentProduct2(products, i);
        CompleteOptionLst();
        listExist.push(products["product"][i]["obj"]["idProduct"]);
        $("#quantityCtx-" + products["product"][i]["obj"]["idProduct"]).val(products["product"][i]["obj"]["quantity"]);
    }

}

function CreateContentProduct2(products, i) {
    IVAArray.push(products["product"][i]["obj"]["idProduct"]);
    if (products["product"][i]["obj"]["amount"] == 0) {
        $("#productContainer").prepend("<div class='box box box-info' name='isDiv' id='" + products["product"][i]["obj"]["idProduct"] + "' > " + products["product"][i]["obj"]["nodo"] + "</div>");
        //CompleteOptionLst(products["product"][i]["obj"]["idProduct"]); //completa las opciones del desplegable de tiempo de entrega
       // CompleteOptionLst("90000000"+products["product"][i]["obj"]["idProduct"]); 
        
    } else {
        $("#productContainer").prepend("<div class='box box box-info' name='isDiv' id='" + products["product"][i]["obj"]["idProduct"] + "' > " + products["product"][i]["obj"]["nodo"] + "</div>");
        //SetOptionTimeDelivery(products["product"][i]["obj"]["idProduct"]);
    }
    console.log();
    //QueryPrices();

    setTimeout(function() { $("#porcent-" + products["product"][i]["obj"]["idProduct"]).val(products["product"][i]["obj"]["percent"]);$("#quantityCtx-"+ products["product"][i]["obj"]["idProduct"]).trigger("change") }, 3000);
    setTimeout(function() { 
        //ValidateOptions("#timeDelivery-" + products["product"][i]["obj"]["idProduct"], products["product"][i]["obj"]["timeDelivery"]);
        //$("#timeDelivery-90000000" + id).html(5);
        //$("#timeDelivery-" + id).html(5);
    }, 3000);
}

function GetDataForChangePrice(products, i) {
    data = [];
    data["lugarDeProducto"] = products["product"][i]["obj"]["namePlace"];
    data["precioUnitarioCOP"] = parseInt(products["product"][i]["precioVentaCOP"]);
    data["moneda"] = products["product"][i]["obj"]["typeCurrency"];
    console.log(data);
    return data;
}

function SetOptionsQuotation(data) {

    ValidateOptions("#quotationOfferLst", data["quotationData"][0]["offerValidity"]);
    ValidateOptions("#quotationPaymentLst", data["quotationData"][0]["payment"]);
    ValidateOptions("#quotationTypeLst", data["quotationData"][0]["typeQuotation"]);
    ValidateOptions("#quotationPlaceLst", data["quotationData"][0]["placeDelivery"]);
    ValidateOptions("#quotationUserLst", data["quotationData"][0]["idUser"]);

}

function GetTotalPorcent(percent, priceUnit) {
    return parseFloat(percent * parseFloat(priceUnit));
}

function ValidateOptions(idLst, value) {

    $(idLst + " option").each(function() {
        if ($(this).val() == value) {
            $(this).attr("selected", "selected");
        }
    });

}

function UpdateQuotation() {

    $("#UpdateQuotationMainteince").click(function() {
        alert("entro");
        var idProducts = [];
        var quotationData = [];
        var totalContacts = [];
        var contactsSelected = [];
        var quantity = [];
        var porcentProducts = [];
        var unityPrice = [];
        var idPersonSelected;
        var timeDelivery = [];
        var date = new Date().toLocaleString().replace("/", "-").replace("/", "-");
        var description = $("#txtDescription").val();
        var nodo = [];

        // traer id productos
        idProducts = GetIdProducts();
        // traer informacion del usuario
        quotationData = GetDataQuotation();
        // traer usuarios checkeds
        totalContacts = GetDataUsers();
        // traer las cantidades seleccionados
        quantity = GetQuantity();
        //traer la ganacia de cada producto
        porcentProducts = GetPorcentProducts();
        idPersonSelected = GetDataUsersList();
        unityPrice = GetUnityProducts();
        timeDelivery = GetTimeDeliveryProducts();
        nodo = GetNodosProducts();
        if (validateFormulario() != false && validateLista() != false && GetValueToInputText("quotationPaymentLst") != "" && GetValueToInputText("quotationTypeLst") != "" && GetValueToInputText("quotationPaymentLst") != "" && GetValueToInputText("quotationTypeLst") != "" && GetValueToInputText("quotationOfferLst") != "" && GetValueToInputText("quotationUserLst") != "" && GetValueToInputText("quotationPlaceLst") != "") {
            if (idProducts.length > 0) {
                if (description) {
                        var parameters = {
                            "isActionFromAjax": true,
                            "optionInputForm": 4,
                            "idDataInputForm": 0,
                            "idc": idQuotation,
                            "id": idProducts,
                            "quotationData": quotationData,
                            "usersSelected": totalContacts,
                            "idCompany": totalContacts[0].idCompany,
                            "timeDelivery": timeDelivery,
                            "porcentProducts": porcentProducts,
                            "quantity": quantity,
                            "person": idPersonSelected,
                            "prices": unityPrice,
                            "date": date,
                            "totalViatic": $("#viaticTotalData").val(),
                            "getTypeProduct": GetTypeProduct(),
                            "typeProduct": ValidateTypeProduct(),
                            "ivaValidate": IVAValidate,
                            "IVAList": IVAArray,
                            "typeQuotationBool": typeQuotationBool,
                            "check": Checkeable,
                            "idMainteince": $("#idMainteince").val(),
                            "dateUpdate": $("#dateUpdateCtx").val(),
                            "nodos": nodo,
                            "idViatic": $("#quotationViaticLst").val()

                        };

                        $.ajax({
                            url: "QuoteMainteince",
                            type: 'post',
                            data: parameters,
                            beforeSend: function() {
                                let timerInterval
                                swal({
                                    title: 'Actualizando cotización!',
                                    html: 'cargando...',
                                    onOpen: () => {
                                        swal.showLoading()
                                        timerInterval = setInterval(() => {
                                            swal.getContent().querySelector('strong')
                                                .textContent = swal.getTimerLeft()
                                        }, 100)
                                    }
                                })
                            },
                            success: function(data) {

                                if (!Checkeable) {
                                    PrintData(idQuotation);
                                    $(".generateQuotation").removeAttr("disabled");
                                    $(".generateQuotation").text("Generar Cotización");
                                } else {
                                    SendMail(idQuotation);
                                    $(".generateQuotation").removeAttr("disabled");
                                    $(".generateQuotation").text("Generar Cotización");
                                    MessageSwalBasic("GENERADA !!", "Se ha generado la cotización , sin embargo esta a la espera a ser verificado los productos con una ganancia menor al 20% de esta cotización", "warning");
                                    setTimeout(function() {
                                        window.location = "ListQuotesMainteince";
                                    }, 7000);
                                }
                            }

                        });
                
                } else {
                    MessageSwalBasic("VERIFICA !!", "Debes Seleccionar un Motivo ", "warning");
                }
            } else {
                MessageSwalBasic("VERIFICA !!", "Debes agregar al menos un producto ", "warning");
            }
        } else {
            MessageSwalBasic("VERIFICA !!", "Debes completar todos los campos ", "warning");
        }
    });
}

function PrintData(id) {
    var parameters = {
        "isActionFromAjax": true,
        "optionInputForm": 3,
        "idDataInputForm": 0,
        "id": id
    }
    var windowOpen = window.open();
    if (windowOpen) {
        
        windowOpen.close();
        $.ajax ({
            url: "ListQuotesMainteince",
            type: 'post',
            data: parameters,

            beforeSend: function() {

                setTimeout(MessageSwalWaiter('cargando...', 'Abriendo cotización!'), 10000);
            },
            success: function(data) {
                data = data.split("--split--")[1];
                windowOpen = window.open("view/docs/pdf/" + data + ".pdf?v=" + Math.random(), '_blank');
                if (windowOpen) {
                    windowOpen.focus();
                    DeletePDFQuotation(data);
                    MessageSwalRedirect("GENERADO !!", "Se ha abierto el documento de cotización ", "success", "ListQuotesMainteince");
                } else {
                    MessageSwalBasic("VERIFICAR !!", "Debes habilitar permisos para abrir ventanas emergentes", "warning");
                }
            }

        });
    } else {
        MessageSwalBasic("VERIFICAR !!", "Debes habilitar permisos para abrir ventanas emergentes", "warning");
    }
}

function ShowOnLoad() {
    $("#loader").addClass("loader");
    $("#loader").html('<div id="conten"><img src="view/img/Cargando.gif" alt=""><h2>Cargando Cotización...</h2></div>');
}

function NotShowLoad() {
    $("#loader").css("display", "none");
}

function ValidateChangeTypeQuotation() {
    $("#quotationTypeLst").change(function() {
        change = 1;

    })
}
function QueryPrices2() {
    $("[name*=totalPriceCtx]").each(function() {
        dataProduct = GetDataProduct($(this));

            CountquantityCtx(dataProduct["id"]);


        });
}
// ======================================================================== //


$(function() {
    //SetDataMoney();

    
    setTimeout(function() {
        ValidateChangeTypeQuotation();
        GetModifyQuotation();
        UpdateQuotation();
        $("html").css({ 'overflow': 'scroll' });
        QueryPrices2();

    }, 4000);
    setTimeout(MessageSwalWaiter('cargando...', 'se esta cargando los datos de la cotización!'), 6000);




});