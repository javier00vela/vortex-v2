<div class="content-wrapper">
    <section class="content-header m-2">
        <h1>
            Agregar/Modificar Precios Viaticos
        </h1>
        <ol class="breadcrumb">
            <li><a href="Home"><i class="fa fa-dashboard"></i> Inicio</a></li>
            <li class="active">lista de cotizaciones de mantenimiento</li>
            <li class="active">Agregar gastos de costos de Viaticos </li>
        </ol>
    </section>
    <section class="content">
        <div class="box m-5">
            <div class="box ">
                <nav class="navbar navbar-light">
                    <div class="container-fluid">
                        <ul class="nav navbar-nav">
                            <li>
                                <a style="cursor: pointer;" id="userAddBtn" name="userAddBtn"
                                    onclick="location.href = 'ListQuotesMainteince'">
                                    <i class="fa fa-arrow-left"></i> Regresar
                                </a>
                            </li>
                            <li>
                                <a style="cursor: pointer;" id="userAddBtn" name="userAddBtn"
                                    onclick="location.href = 'Home' ">
                                    <i class="fa fa-home"></i> Menu Principal
                                </a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
            <div class="box-body">
                <ul class="list-group">
                    <input type="hidden" id="idViatic" value="{$viaticsDataById->id}">
                    <input type="hidden" id="idMainteince" value="{$idMainteinceTxt}">
                    <div class="col-sm-6">
                        <div class="info-box bg-purple">
                            <span class="info-box-icon"><i class="fa fa-cube"></i></span>
                            <div class="info-box-content">
                                <span class="info-box-text">Nombre Viatico:</span>
                                <span class="info-box-number" id="DDP"> {$viaticsDataById->place} </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="info-box bg-red">
                            <span class="info-box-icon"><i class="fa fa-sun-o"></i></span>
                            <div class="info-box-content">
                                <span class="info-box-text">Días Viaje:</span>
                                <span class="info-box-number" id="DDP">{$viaticsDataById->days} día/s</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                            <div class="info-box bg-green">
                                <span class="info-box-icon"><i class="fa fa-dollar"></i></span>
                                <div class="info-box-content">
                                    <span class="info-box-text">Total costos viaticos:</span>
                                    <span class="info-box-number" id="DDP"> ${$viaticsData['totalValues']['cost']} </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="info-box bg-red">
                                <span class="info-box-icon"><i class="fa fa-dollar"></i></span>
                                <div class="info-box-content">
                                    <span class="info-box-text">Total gastos viaticos:</span>
                                    <span class="info-box-number" id="DDP">${$viaticsData['totalValues']['expenses']} </span>
                                </div>
                            </div>
                        </div>
                    <div class="col-sm-12">
                        <div class="info-box bg-green">
                            <span class="info-box-icon"><i class="fa fa-dollar"></i></span>
                            <div class="info-box-content">
                                <span class="info-box-text">Total precio entre gastos y costos del viatico:</span>
                                <span class="info-box-number" id="DDP"> ${math equation="( x - y )"  x=$viaticsData['totalValues']['cost'] y= $viaticsData['totalValues']['expenses'] } COP</span>
                            </div>
                        </div>
                    </div>
                </ul>
            </div>

            <div class="box">
                <nav class="bg-primary">
                    <p class="text-center">Transporte</p>
                </nav>

                <div class="box-body  text-center">
                    <div class="info-box bg-green">
                        <div class="info-box-content">
                            <span class="info-box-text">Diferencia de monto de transporte :</span>
                            <span class="info-box-number" id="FCA">
                                    <span class="info-box-number" id="DDP"> ${math equation="( x - y )"  x=$viaticsData["transport"]["price"] y= $viaticsData["transportExpense"]["price"] }</span>
                            </span>
                        </div>
                    </div>
                    <div class=" card col-sm-6 col-xs-12">
                        <div class=" card-body row">

                            <div class="col-sm-6">
                                <div class="info-box bg-green">
                                    <span class="info-box-icon"><i class="fa fa-dollar"></i></span>
                                    <div class="info-box-content">
                                        <span class="info-box-text">Total Costos Tranporte:</span>
                                        <span class="info-box-number" id="DDP">${$viaticsData["transport"]["price"]}
                                            COP</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="info-box bg-blue">
                                    <span class="info-box-icon"><i class="fa fa-cubes"></i> </span>
                                    <div class="info-box-content">
                                        <span class="info-box-text">Cantidad de registros:</span>
                                        <span class="info-box-number"
                                            id="FCA">{$viaticsData["transport"]["quantity"]}</span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br><br><br><br>
                        <table id="transportTbl" name="transportTbl"
                            class="table table-bordered table-striped dt-responsive tables dataTable no-footer dtr-inline"
                            style="width: 100%;" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>Nombre</th>
                            
                                    <th>Cantidad</th>
                                    <th>Precio individual</th>
                                    <th>Total Precio</th>

                                </tr>
                            </thead>
                        </table>
                    </div>
                    <div class="col-sm-6 col-xs-12">
                        <div class="box">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="info-box bg-green">
                                        <span class="info-box-icon"><i class="fa fa-dollar"></i></span>
                                        <div class="info-box-content">
                                            <span class="info-box-text">Total Gastos Tranporte:</span>
                                            <span class="info-box-number" id="DDP">${$viaticsData["transportExpense"]["price"]}
                                                COP</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="info-box bg-blue">
                                        <span class="info-box-icon"><i class="fa fa-cubes"></i> </span>
                                        <div class="info-box-content">
                                            <span class="info-box-text">Cantidad de registros:</span>
                                            <span class="info-box-number"
                                                id="FCA">{$viaticsData["transportExpense"]["quantity"]}</span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <nav class="navbar navbar-light">
                                <div class="container-fluid">
                                    <ul class="nav navbar-nav">
                                        <li>
                                            <a node="node-agregar_costo" style="cursor: pointer;" id="impostAddBtn"
                                                name="impostAddBtn" data-toggle="modal" data-target="#impostPopUpAdd"
                                                onclick="ResetControls('containerinputs');SetData();$('#typeViatic').val('transport')">
                                                <i class="fa fa-plus-circle"></i> Agregar Gasto
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </nav>
                            <div class="box-body">

                                <table id="transportExpensiveTbl" name="transportTbl"
                                    class="table table-bordered table-striped dt-responsive tables dataTable no-footer dtr-inline"
                                    style="width: 100%;" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>Nombre del gasto</th>
                                            <th>Referencia Gasto (nro. factura)</th>
                                            <th>Precio Gasto</th>
                                            <th>Opciones</th>

                                        </tr>
                                    </thead>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>




                <div class="box">
                    <nav class="bg-primary">
                        <p class="text-center">Alimentación</p>
                    </nav>

                    <div class="box-body  text-center">
                        <div class="info-box bg-green">
                            <div class="info-box-content">
                                <span class="info-box-text">Diferencia de monto de Alimentación :</span>
                                <span class="info-box-number" id="FCA"> ${math equation="( x - y )"  x=$viaticsData["food"]["price"] y= $viaticsData["foodExpense"]["price"] }</span>
                                </span>
                            </div>
                        </div>
                        <div class=" card col-sm-6 col-xs-12">
                            <div class=" card-body row">

                                <div class="col-sm-6">
                                    <div class="info-box bg-green">
                                        <span class="info-box-icon"><i class="fa fa-dollar"></i></span>
                                        <div class="info-box-content">
                                            <span class="info-box-text">Total Costos Alimentación:</span>
                                            <span class="info-box-number" id="DDP">${$viaticsData["food"]["price"]}
                                                COP</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="info-box bg-blue">
                                        <span class="info-box-icon"><i class="fa fa-cubes"></i> </span>
                                        <div class="info-box-content">
                                            <span class="info-box-text">Cantidad de registros:</span>
                                            <span class="info-box-number"
                                                id="FCA">{$viaticsData["food"]["quantity"]}</span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br><br><br><br>
                            <table id="foodTbl" name="transportTbl"
                                class="table table-bordered table-striped dt-responsive tables dataTable no-footer dtr-inline"
                                style="width: 100%;" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>Nombre</th>
                                        <th>Cantidad</th>
                                        <th>Precio individual</th>
                                        <th>Total Precio</th>

                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div class="col-sm-6 col-xs-12">
                            <div class="box">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="info-box bg-green">
                                            <span class="info-box-icon"><i class="fa fa-dollar"></i></span>
                                            <div class="info-box-content">
                                                <span class="info-box-text">Total Gastos Alimentación:</span>
                                                <span class="info-box-number"
                                                    id="DDP">${$viaticsData["foodExpense"]["price"]}
                                                    COP</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="info-box bg-blue">
                                            <span class="info-box-icon"><i class="fa fa-cubes"></i> </span>
                                            <div class="info-box-content">
                                                <span class="info-box-text">Cantidad de registros:</span>
                                                <span class="info-box-number"
                                                    id="FCA">{$viaticsData["foodExpense"]["quantity"]}</span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <nav class="navbar navbar-light">
                                    <div class="container-fluid">
                                        <ul class="nav navbar-nav">
                                            <li>
                                                <a node="node-agregar_costo" style="cursor: pointer;" id="impostAddBtn"
                                                    name="impostAddBtn" data-toggle="modal"
                                                    data-target="#impostPopUpAdd"
                                                    onclick="ResetControls('containerinputs');SetData();$('#typeViatic').val('food')">
                                                    <i class="fa fa-plus-circle"></i> Agregar Alimentación
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </nav>
                                <div class="box-body">

                                    <table id="foodExpensiveTbl" name="transportTbl"
                                        class="table table-bordered table-striped dt-responsive tables dataTable no-footer dtr-inline"
                                        style="width: 100%;" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th>Nombre del gasto</th>
                                                <th>Referencia Gasto (nro. factura)</th>
                                                <th>Precio Gasto</th>
                                                <th>Opciones</th>

                                            </tr>
                                        </thead>
                                    </table>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="box">
                        <nav class="bg-primary">
                            <p class="text-center">Hospedaje</p>
                        </nav>

                        <div class="box-body  text-center">
                            <div class="info-box bg-green">
                                <div class="info-box-content">
                                    <span class="info-box-text">Diferencia de monto de Hospedaje :</span>
                                    <span class="info-box-number"
                                        id="FCA">  ${math equation="( x - y )"  x=$viaticsData["hospedaje"]["price"] y= $viaticsData["hospedajeExpense"]["price"] }</span>
                                    </span>
                                </div>
                            </div>
                            <div class=" card col-sm-6 col-xs-12">
                                <div class=" card-body row">

                                    <div class="col-sm-6">
                                        <div class="info-box bg-green">
                                            <span class="info-box-icon"><i class="fa fa-dollar"></i></span>
                                            <div class="info-box-content">
                                                <span class="info-box-text">Total Costos Hospedaje:</span>
                                                <span class="info-box-number"
                                                    id="DDP">${$viaticsData["hospedaje"]["price"]}
                                                    COP</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="info-box bg-blue">
                                            <span class="info-box-icon"><i class="fa fa-cubes"></i> </span>
                                            <div class="info-box-content">
                                                <span class="info-box-text">Cantidad de registros:</span>
                                                <span class="info-box-number"
                                                    id="FCA">{$viaticsData["hospedaje"]["quantity"]}</span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br><br><br><br>
                                <table id="hospedajeTbl" name="transportTbl"
                                    class="table table-bordered table-striped dt-responsive tables dataTable no-footer dtr-inline"
                                    style="width: 100%;" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>Nombre</th>
                            
                                            <th>Cantidad</th>
                                            <th>Precio individual</th>
                                            <th>Total Precio</th>

                                        </tr>
                                    </thead>
                                </table>
                            </div>
                            <div class="col-sm-6 col-xs-12">
                                <div class="box">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="info-box bg-green">
                                                <span class="info-box-icon"><i class="fa fa-dollar"></i></span>
                                                <div class="info-box-content">
                                                    <span class="info-box-text">Total Gastos hospedaje:</span>
                                                    <span class="info-box-number"
                                                        id="DDP">${$viaticsData["hospedajeExpense"]["price"]}
                                                        COP</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="info-box bg-blue">
                                                <span class="info-box-icon"><i class="fa fa-cubes"></i> </span>
                                                <div class="info-box-content">
                                                    <span class="info-box-text">Cantidad de registros:</span>
                                                    <span class="info-box-number"
                                                        id="FCA">{$viaticsData["hospedajeExpense"]["quantity"]}</span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <nav class="navbar navbar-light">
                                        <div class="container-fluid">
                                            <ul class="nav navbar-nav">
                                                <li>
                                                    <a node="node-agregar_costo" style="cursor: pointer;"
                                                        id="impostAddBtn" name="impostAddBtn" data-toggle="modal"
                                                        data-target="#impostPopUpAdd"
                                                        onclick="ResetControls('containerinputs');SetData();$('#typeViatic').val('hospedaje')">
                                                        <i class="fa fa-plus-circle"></i> Agregar Hospedaje
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </nav>
                                    <div class="box-body">

                                        <table id="hospedajeExpensiveTbl" name="transportTbl"
                                            class="table table-bordered table-striped dt-responsive tables dataTable no-footer dtr-inline"
                                            style="width: 100%;" cellspacing="0">
                                            <thead>
                                                <tr>
                                                    <th>Nombre del gasto</th>
                                                    <th>Referencia Gasto (nro. factura)</th>
                                                    <th>Precio Gasto</th>
                                                    <th>Opciones</th>

                                                </tr>
                                            </thead>
                                        </table>

                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="box">
                            <nav class="bg-primary">
                                <p class="text-center">Improvistos</p>
                            </nav>

                            <div class="box-body  text-center">
                                <div class="info-box bg-green">
                                    <div class="info-box-content">
                                        <span class="info-box-text">Diferencia de monto de Improvisto :</span>
                                        <span class="info-box-number"
                                            id="FCA"> ${math equation="( x - y )"  x=$viaticsData['improvistos']['price'] y= $viaticsData['improvistosExpense']['price'] }</span>
                                        </span>
                                    </div>
                                </div>
                                <div class=" card col-sm-6 col-xs-12">
                                    <div class=" card-body row">

                                        <div class="col-sm-6">
                                            <div class="info-box bg-green">
                                                <span class="info-box-icon"><i class="fa fa-dollar"></i></span>
                                                <div class="info-box-content">
                                                    <span class="info-box-text">Total Costos Improvistos:</span>
                                                    <span class="info-box-number"
                                                        id="DDP">${$viaticsData["improvistos"]["price"]}
                                                        COP</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="info-box bg-blue">
                                                <span class="info-box-icon"><i class="fa fa-cubes"></i> </span>
                                                <div class="info-box-content">
                                                    <span class="info-box-text">Cantidad de registros:</span>
                                                    <span class="info-box-number"
                                                        id="FCA">{$viaticsData["improvistos"]["quantity"]}</span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <br><br><br><br>
                                    <table id="imporvistosTbl" name="transportTbl"
                                        class="table table-bordered table-striped dt-responsive tables dataTable no-footer dtr-inline"
                                        style="width: 100%;" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th>Nombre</th>
                                                <th>Cantidad</th>
                                                <th>Precio individual</th>
                                                <th>Total Precio</th>

                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                                <div class="col-sm-6 col-xs-12">
                                    <div class="box">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="info-box bg-green">
                                                    <span class="info-box-icon"><i class="fa fa-dollar"></i></span>
                                                    <div class="info-box-content">
                                                        <span class="info-box-text">Total Gastos Alimentación:</span>
                                                        <span class="info-box-number"
                                                            id="DDP">${$viaticsData["improvistosExpense"]["price"]}
                                                            COP</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="info-box bg-blue">
                                                    <span class="info-box-icon"><i class="fa fa-cubes"></i> </span>
                                                    <div class="info-box-content">
                                                        <span class="info-box-text">Cantidad de registros:</span>
                                                        <span class="info-box-number"
                                                            id="FCA">{$viaticsData["improvistosExpense"]["quantity"]}</span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <nav class="navbar navbar-light">
                                            <div class="container-fluid">
                                                <ul class="nav navbar-nav">
                                                    <li>
                                                        <a node="node-agregar_costo" style="cursor: pointer;"
                                                            id="impostAddBtn" name="impostAddBtn" data-toggle="modal"
                                                            data-target="#impostPopUpAdd"
                                                            onclick="ResetControls('containerinputs');SetData();$('#typeViatic').val('improvistos')">
                                                            <i class="fa fa-plus-circle"></i> Agregar improvistos
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </nav>
                                        <div class="box-body">

                                            <table id="improvistosExpensiveTbl" name="improvistosTbl"
                                                class="table table-bordered table-striped dt-responsive tables dataTable no-footer dtr-inline"
                                                style="width: 100%;" cellspacing="0">
                                                <thead>
                                                    <tr>
                                                        <th>Nombre del gasto</th>
                                                        <th>Referencia Gasto (nro. factura)</th>
                                                        <th>Precio Gasto</th>
                                                        <th>Opciones</th>

                                                    </tr>
                                                </thead>
                                            </table>

                                        </div>
                                    </div>
                                </div>
                            </div>




                        </div>
    </section>
</div>
<!--=====================================
VENTANA MODAL AGREGAR IMPUESTO O MODIFICAR
======================================-->
<div id="impostPopUpAdd" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form id="expensesviaticsForm" role="form" method="post">
                <!--=====================================
              Hidden
              ======================================-->
                <input id="optionInputForm" name="optionInputForm" type="hidden" value="0">
                <input id="idDataInputForm" name="idDataInputForm" type="hidden" value="0">
                <input id="isActionFromAjax" name="isActionFromAjax" type="hidden" value="false">
                <input type="hidden" id="typeViatic" name="typeViatic" value="">
                <input type="hidden" id="idViatic" name="idViatic" value="{$idViatic}">
                <input type="hidden" id="idMainteince" name="idMainteince" value="{$idMainteinceTxt}">
                <!--=====================================
              CABEZA DEL MODAL
              ======================================-->
                <div class="modal-header" style="background:#3c8dbc; color:white">
                    <button type="button" class="close" data-dismiss="modal" data-target='#impostPopUpAdd'
                        onclick="ResetControls('impostForm')">&times;</button>
                    <h4 class="modal-title">Agregar Gasto de costo de viatico</h4>
                </div>
                <!--=====================================
              CUERPO DEL MODAL
              ======================================-->
                <div class="modal-body">
                    <div class="box-body">
                        <div class="row">
                            <div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-industry"></i></span>
                                    {$impostNameCtx}
                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-dollar"></i></span>
                                        {$impostPriceCtx}
                                    </div>
                                </div>
                            <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-cube"></i></span>
                                    {$impostReferenceCtx}
                                </div>
                            </div>
                           

                            <input type="hidden" name="idCompanyCtx" id="idCompany" value="{$idPricesCompany}">
                        </div>
                    </div>
                </div>
                <!--=====================================
              PIE DEL MODAL
              ======================================-->
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal"
                        data-target='#impostPopUpAdd' onclick="ResetControls('modifyPricesForm')">Salir</button>
                    <button type="submit" class="btn btn-primary">Guardar </button>
                </div>
            </form>

        </div>
    </div>
</div>
<div id="containerResponseAjax"></div>
<script src="view/html/modules/expensesviatics/ExpensesViatics.js?v= {$tempParameter}"></script>
{$jquery}