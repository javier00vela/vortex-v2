<?php
require_once(__DIR__ . '/../../../../controller/general/Config.php');
require_once(Config::PATH . Config::CONTROLLER.Config::GENERAL . 'ManageArrays.php');
require_once(Config::PATH . Config::BACKEND . 'modules/AditionalsImpostVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/ViaticsPropetyVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/ViaticsExpensiveVo.php');

require_once(Config::PATH . Config::BACKEND . 'modules/InventoryVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/CompanyVo.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralDao.php');

class AjaxDataExpensesViatics{

    private $isActionFromAjax;
    private $optionInputForm;
    private $idDataInputForm;
    private $dataList = [];

    function __construct()
    {
        if (isset($_POST['isActionFromAjax'])) {
            $this->isActionFromAjax = $_POST['isActionFromAjax'];
            $this->optionInputForm = $_POST['optionInputForm'];
            $this->idDataInputForm = $_POST['idDataInputForm'];
            $this->generalDao = new GeneralDao();
            if(isset($_POST["listData"])){
                $this->dataList = $_POST["listData"];  
            }
            if ($this->isActionFromAjax == "true") {
                if ($this->optionInputForm == 3) {
                    $this->SetDataInControls();
                }else if($this->optionInputForm == 5){
                $this->GetLengthImpost();
                }else if($this->optionInputForm == 6){
                $this->UpdateByInput($_POST["input"]);
                }else if($this->optionInputForm == 10){
                    $this->GetDataExpensesInJsonForTbl();
                }else {
                    $this->GetDataInJsonForTbl();
                }
            }
        }
    }

    private function SetDataInControls(){
        $SetInputsDataFJQ = "";
        $ViaticsExpensiveVo = new ViaticsExpensiveVo();
        $ViaticsExpensiveVo->id = $this->idDataInputForm;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $ViaticsExpensiveVo);
        if($ViaticsExpensiveVo2 = $generalDao->GetVo($generalVo = clone $ViaticsExpensiveVo)){
            $SetInputsDataFJQ = "
                            <script>
                               (function($){
                                $('.modal-title').html('Modificar costo');
                                SetValueToInputText('impostNameCtx','{$ViaticsExpensiveVo2->name}');
                                SetValueToInputText('impostReferenceCtx','{$ViaticsExpensiveVo2->reference}');
                                SetValueToInputText('impostPriceCtx','{$ViaticsExpensiveVo2->price}');
                                SetValueToInputText('impostQuantityCtx','{$ViaticsExpensiveVo2->quantity}');
                                SetValueToInputText('optionInputForm',4);
                               })(jQuery);
                            </script>
                        ";
        }
        echo $SetInputsDataFJQ;
    }

    public function GetDataInJsonForTbl()
    {
        $existData = false;
        $ViaticsPropetyVo = new ViaticsPropetyVo();
        $ViaticsPropetyVo->idSearchField = 6;
        $ViaticsPropetyVo->typeViatic = $_POST["typePropiety"];
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $ViaticsPropetyVo);

        $string = "
        {\"data\": [";
        while ($ViaticsPropetyVo2 = $generalDao->GetVo($generalVo = clone $ViaticsPropetyVo)) {
            if($_POST["idViatic"] == $ViaticsPropetyVo2->idViatic ){
                $existData = true;
                $json = array();
                $json[] = htmlspecialchars($ViaticsPropetyVo2->name);
                //$json[] = htmlspecialchars($ViaticsPropetyVo2->percent)."%";
                $json[] = htmlspecialchars($ViaticsPropetyVo2->quantity);
                $json[] = "$".htmlspecialchars(str_replace(",",".",number_format(str_replace(",",".",$ViaticsPropetyVo2->price ))));
                $json[] = "$".htmlspecialchars(str_replace(",",".",number_format(str_replace(",",".", ($ViaticsPropetyVo2->price * (0 / 100) + $ViaticsPropetyVo2->price ) * $ViaticsPropetyVo2->quantity ))));

                $string .= json_encode($json) . ",";
            }
        }
        $string .= "-]}-";

        if($existData){
            echo  str_replace(",-]}-", "]}", $string);
        }else{
            echo "{ \"data\": [] }";
        }
    }

    public function GetDataExpensesInJsonForTbl()
    {
        $existData = false;
        $ViaticsExpensiveVo = new ViaticsExpensiveVo();
        $ViaticsExpensiveVo->idSearchField = 6;
        $ViaticsExpensiveVo->typeViatic = $_POST["typePropiety"];
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $ViaticsExpensiveVo);

        $string = "
        {\"data\": [";
        while ($ViaticsExpensiveVo2 = $generalDao->GetVo($generalVo = clone $ViaticsExpensiveVo)) {
            if($_POST["idViatic"] == $ViaticsExpensiveVo2->idViatic && $ViaticsExpensiveVo2->idMainteince == $_POST["idMainteince"]  ){
                $existData = true;
                $button = '';
                $json = array();
                $json[] = htmlspecialchars($ViaticsExpensiveVo2->name);
                //$json[] = htmlspecialchars($ViaticsPropetyVo2->percent)."%";
                $json[] = htmlspecialchars($ViaticsExpensiveVo2->reference);
                $json[] = "$".htmlspecialchars(str_replace(",",".",number_format(str_replace(",",".",($ViaticsExpensiveVo2->price )))));
                if(!in_array( "modificar_costo" , $this->dataList)){
                    $button .= "<button id='updateBtn' name='updateBtn' class='btn btn-warning btn-xs' data-toggle='modal' data-target='#impostPopUpAdd' title='Modificar' onclick='UpdateData({$ViaticsExpensiveVo2->id},\"expensesviatics\",\"Expensesviatics\");$(\"#typeViatic\").val(\"{$_POST["typePropiety"]}\")'><i class='fa fa-pencil'></i></button> ";
                }
                if(!in_array( "eliminar_costo" , $this->dataList)){
                    $button .= "<button id='deleteBtn' name='deleteBtn' class='btn btn-danger  btn-xs' title='Eliminar' onclick='DeleteData({$ViaticsExpensiveVo2->id},\"expensesviatics\")'><i class='fa fa-times'></i></button> ";
                }
                $json[] = $button;
                $string .= json_encode($json) . ",";
            }
        }
        $string .= "-]}-";

        if($existData){
            echo  str_replace(",-]}-", "]}", $string);
        }else{
            echo "{ \"data\": [] }";
        }
    }

    public function GetLengthImpost(){
        $arrayList = array();
        $personVo = new impostVo();
        $personVo->isList = true;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $personVo);
        while ($personVo2 = $generalDao->GetVo($generalVo = clone $personVo)) {
            $arrayList[] =$personVo2 ;
        }
        print_r( json_encode(count($arrayList)) );
    }

      private function UpdateByInput($input){
        switch ($input) {
            case 'TMPnameCtx':
                $this->UpdateByTypeInput( $_POST["inputValue"] , "tpm" , 2);
                break;
            case 'typeMoneyLst':
                 $this->UpdateByTypeInput( $_POST["inputValue"] , "typeMoney" , 1);
                 $this->ChangePricesProduct();
                break;
            
            case 'procentSailCtx':
                  $this->UpdateByTypeInput( $_POST["inputValue"] , "ganancia" , 3);
                break;
             }

    }

    public function GetTasa(){
        $pricesCompanyVo = new PricesCompanyVo();
        $pricesCompanyVo->isList = true;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $pricesCompanyVo);
        while ($pricesCompanyVo2 = $generalDao->GetVo($generalVo = clone $pricesCompanyVo)) {
                return $pricesCompanyVo2->tmp;
        }

    }




    private function ChangePricesProduct()
    {
        if(!$_POST["inputValue"]){ $_POST["inputValue"] = "EUR";  }
        $InventoryVo = new InventoryVo();
        $InventoryVo->idSearchField = 11;
        $InventoryVo->nameCompany = $this->GetNameCompany( $_POST["idCompany"]);
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $InventoryVo);
        while($InventoryVo2 = $generalDao->GetVo($generalVo = clone $InventoryVo)) {
            if($_POST["inputValue"] == "COP" && $InventoryVo2->namePlace == "Internacional"   ){
                # no se actualiza
            }else{
                $this->SetUpdateProducts($InventoryVo2);
            }
        }
    }
  

    private function SetUpdateProducts($InventoryVo2){
        $InventoryVo = new InventoryVo();
        $InventoryVo->id = $InventoryVo2->id;
        $InventoryVo->idSearchField = 0;
        $InventoryVo->typeCurrency = $_POST["inputValue"];
        $InventoryVo->idUpdateField = 8;
        $generalDao = new GeneralDao();
        $generalDao->UpdateByField($generalVo = clone $InventoryVo);

    }


  /*
      while($InventoryVo2 = $generalDao->GetVo($generalVo = clone $InventoryVo)) {
      print_r($InventoryVo2->id);
            $InventoryVo3->id = $InventoryVo2->id;
            $InventoryVo3->idSearchField = 0;
            $InventoryVo3->typeCurrency = $_POST["inputValue"];
            $InventoryVo3->idUpdateField = 8;
            $generalDao = new GeneralDao();
         //   $generalDao->UpdateByField($generalVo = clone $InventoryVo3);
        }
    */    
    

 private  function GetNameCompany($company)
    {

        $companyVo = new CompanyVo();
        $companyVo->idSearchField = 0;
        $companyVo->id = $company;
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $companyVo);
        if ($companyVo2 = $generalDao->GetVo($generalVo = clone $companyVo)) {
            return $companyVo2->name;
        }
        return false;
    }

    private function UpdateByTypeInput($valueInput , $nameInput ,  $number){
        $pricesCompanyVo = new PricesCompanyVo();
        $pricesCompanyVo->id = $_POST["idPriceCompany"];
        $pricesCompanyVo->idUpdateField = $number ;
        $pricesCompanyVo->$nameInput = $valueInput;
        $pricesCompanyVo->idSearchField = 0;
        if ($this->generalDao->UpdateByField($generalVo = clone $pricesCompanyVo)) {}
    }

    
}

$ajaxDataImpost = new AjaxDataExpensesViatics();
