"use strict";

var table = null;

function LoadTable(typeQuotation=false) {
    var parameters = {
        "isActionFromAjax" : true,
        "optionInputForm" : 0,
        "idDataInputForm" : $("#idCompayInputForm").attr("value"),
        "nameCompayInputForm" : $("#nameCompayInputForm").attr("value"),
        "typeQuotation" : typeQuotation
    };
console.log(parameters);
   table = $('#productTbl').DataTable( {
        responsive: true,
           "drawCallback": function( settings ) {
            $('[data-toggle="tooltip"]').tooltip()
        },
           language : {
                "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
            },
        "ajax": {
            url : "view/html/modules/inventoryprice/AjaxDataInventoryPrice.php",
            method : "POST",
            data : parameters
        }
    });
}

/********************************Excel Data***********************/

 function CreateExcelDocs(){
    table.destroy();
    getDataTable( window.location.href.split("vortex/")[1] , "view/docs/excel/inventoryPrices.xlsx");
}


     function getDataTable(module , path){

         var table = $('#productTbl').DataTable(
          {
            language : {
                "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
            },
          });
         var data =  new Array();
         var HTMLDatatable = table.rows(  { page:   'all', order:'index', search:'applied'} ).data();
            $.each(HTMLDatatable, function(i,dataRows){
              console.log(dataRows);
                 data += "||"+dataRows;
            });
            
         data = data.split("||");
         console.log(data);
         
               $.ajax({
                  data: {'data': data},
                  url:   '/vortexcompany/'+module,
                  method: "POST",
                      success:  function (response) {
                          window.location.href = path; //;      
                      }
              });
          }



/********************************SERIAL EVENTS******************************/






/*********************************CLICK EVENTS**************************/

$("#sendExcelTemplateBtn").click(function () {
    if($(".file").val() == ""){
        MessageSwalBasic("Error","Por favor seleccione un archivo excel a cargar","error");
    }else {
        //alert($("#fileExcelTemplateInp").val());
        SetValueToInputText("optionInputTemplateForm","6");
        $("#excelTemplateForm").submit();
    }
})


function Events(){
    $("#typeQuotation option[value=DDP]").attr("selected",true);
    $("#typeQuotation").change(OnChangeTypeQuotation);
}
function OnChangeTypeQuotation(){
    if($("#typeQuotation option:selected").val()!=""){
        table.destroy();
        LoadTable($("#typeQuotation option:selected").val());
        return;
    }
   table.clear().draw();
    MessageSwalBasic("Error","Por favor seleccione un tipo de cotización","error");
  
}
/******************************/

$(function() {
    Events();
   // var ckeditor = document.getElementById("productDescriptionAtx");
    //ckeditor.value = "hola a todos";
    LoadTable("DDP");

   
});
