<?php
require_once(Config::PATH . Config::CONTROLLER . 'general/UtilJquery.php');
require_once(Config::PATH . Config::VIEW_HTML . 'general/comps/GeneralCtx.php');
require_once(Config::PATH . Config::VIEW_HTML . 'general/comps/GeneralLst.php');
require_once(Config::PATH . Config::VIEW_HTML . 'general/comps/GeneralWithDataLst.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralDao.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/CompanyVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/ProductVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/SerialVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/InventoryVo.php');
require_once(Config::PATH . Config::CONTROLLER . 'general/ExcelManager.php');
require_once(Config::PATH . Config::CONTROLLER_LIB . 'vendor/phpoffice/phpexcel/Classes/PHPExcel/IOFactory.php');

class InventoryPriceView
{
    private $optionInputForm;
    private $idDataInputForm;
    private $idCompanyFromGet;
     private $nameCompanyFromGet;
    /*=============================================
                  Session Vars
    =============================================*/
    private $manageSession;
    /*=============================================
                  Template Vars
    =============================================*/
    private $utilJQ;
    private $pageTpl, $data, $core;
    /*=============================================
                Controls Vars HTML
    =============================================*/
    private $idCompayInputForm;
       private $nameCompayInputForm;

       private $typeQuotationLst;
    /*=============================================
                Data Vars
    =============================================*/
    private $generalDao;
    private $nameColumns =  array("id","nombre","marca","referencia","presentación","tipo Moneda","precio unitario","precio Total","precio pesos colombianos" , "precio basico" , "precio venta");


    function __construct($pageTpl, $data, $core)
    {
        $this->data = $data;
        $this->core = $core;
        $this->pageTpl = $pageTpl;
        $this->CreateReportExcel();

            $this->CreateComponents();
            $this->UseDataFromThePostback();
    }

    private function UseDataFromThePostback()
    {
        if (isset($_POST['optionInputForm'])) {
            $this->optionInputForm = $_POST['optionInputForm'];
            $this->idDataInputForm = $_POST['idDataInputForm'];
            $this->generalDao = new GeneralDao();
            switch ($this->optionInputForm) {
                case "1": {
                    $this->SetData();
                    break;
                }
                case "2": {
                    $this->DeleteData();
                    break;
                }
                case "4": {
                    $this->SetUpdate();
                    break;
                }
            }
        }

        if (isset($_POST['optionInputTemplateForm'])) {
            $this->optionInputForm = $_POST['optionInputTemplateForm'];
            switch ($this->optionInputForm) {
                case "6": {
                    $this->GetFileToRead();
                    break;
                }
            }
        }
        $this->UseDataFromGet();
        $this->AssignDataToTpl();

    }


    private function CreateReportExcel(){
      if(isset($_POST["data"])){
             ExcelManager::CreateReportExcel($_POST["data"] , $this->nameColumns , "inventoryPrices" );
       }
}


    private function  UseDataFromGet(){
        $assign = '';
        if(isset($_GET["nameCompayInputForm"])){

            $this->nameCompanyFromGet = $_GET["nameCompayInputForm"];
            $this->idCompanyFromGet = $_GET["idCompayInputForm"];
            $assign = "SetValueToInputText('nameCompayInputForm',\"$this->nameCompanyFromGet\");";
            $assign .= "SetValueToInputText('idCompayInputForm',".$this->idCompanyFromGet.");";
        }
        $this->utilJQ->AddFunctionJavaScript($assign);
    }

    private function CreateComponents()
    {
        $arrayTypeQuotation = array("DDP","FCA","CIF");
        $this->typeQuotationLst = new GeneralWithDataLst($arrayTypeQuotation, "typeQuotation",false,false,"--Seleccione tipo de cotización--",true);
        $this->nameCompayInputForm = new GeneralCtx("nameCompayInputForm","","hidden",false);
        $this->idCompayInputForm = new GeneralCtx("idCompayInputForm","","hidden",false);
        $this->utilJQ = new UtilJquery("Users");
    }

  
    private function AssignDataToTpl()
    {
        $this->data->assign("typeQuotationLst", $this->typeQuotationLst->paint());
         $this->data->assign('nameCompayInputForm', $this->nameCompayInputForm->paint());
          $this->data->assign('idCompayInputForm', $this->idCompayInputForm->paint());
           $this->data->assign('tempParameter', time());

        $this->data->assign('jquery', $this->utilJQ->PaintJQ());
        $this->SetDataToTpl();
    }

    private function SetDataToTpl()
    {
        echo $this->core->get($this->pageTpl, $this->data);
    }

    /********************************************************/
   

   

}