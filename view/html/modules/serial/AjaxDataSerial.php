<?php
require_once(__DIR__ . '/../../../../controller/general/Config.php');
require_once(Config::PATH . Config::CONTROLLER.Config::GENERAL . 'ManageArrays.php');
require_once(Config::PATH . Config::BACKEND . 'modules/CompanyVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/ProductVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/SerialVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/InventoryVo.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralDao.php');
require_once(Config::PATH . Config::CONTROLLER_LIB . 'vendor/phpoffice/phpexcel/Classes/PHPExcel/IOFactory.php');


class AjaxDataSerial
{

    private $isActionFromAjax;
    private $optionInputForm;
    private $idDataInputForm;

    function __construct()
    {
        if (isset($_POST['isActionFromAjax'])) {
            $this->isActionFromAjax = $_POST['isActionFromAjax'];
            $this->optionInputForm = $_POST['optionInputForm'];
            $this->idDataInputForm = $_POST['idDataInputForm'];

            if ($this->isActionFromAjax == true) {
                if ($this->optionInputForm == 1) {
                   $this->SetDataModification();
                   } else if($this->optionInputForm == 2) {
                    $this->SetDataUpdate();
                } else if($this->optionInputForm == 0) {
                    $this->GetDataInJsonForTbl();
                }
            }
        }
    }

   
    public function GetDataInJsonForTbl()
    {
        $existData = false;
        $inventoryVo = new InventoryVo();
        $inventoryVo->isList = false;
        $inventoryVo->id = $_POST["idProduct"];
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $inventoryVo);
        $cont = 0;
           $idCont = 0;
        $string = "
        {\"data\": [";
        while ($inventoryVo2 = $generalDao->GetVo($generalVo = clone $inventoryVo)) {
       
            for ($i=1; $i < $inventoryVo2->amount+1 ; $i++) { 
                
            $idCont++;
            $existData = true;
            $json = array();
            $json[] = $idCont;
            $json[] = $inventoryVo2->typeProduct;
            $json[] = $inventoryVo2->name;
            if(isset($this->GetSerials($inventoryVo->id)[$cont])){
                if($this->GetSerials($inventoryVo->id)[$cont]->numberRow == $i){
                     $json[] = '<input type="text" class="form-control" placeholder="serial #'.$i.'" onchange="updateSerial('.$this->GetSerials($inventoryVo->id)[$cont]->id.' , '.$i.');" id="camp'.$i.'" value="'.htmlspecialchars($this->GetSerials($inventoryVo->id)[$cont]->numberSerial).'">';
                     $cont++;
                }else{
                    $json[] = '<input type="text" class="form-control" placeholder="serial #'.$i.'"  onchange="insertSerial('.$i.' , '.$inventoryVo2->id.');" id="camp'.$i.'">';
                }
            }else{
                  $json[] = '<input type="text" class="form-control" placeholder="serial #'.$i.'"  onchange="insertSerial('.$i.' , '.$inventoryVo2->id.');" id="camp'.$i.'">'; 
            }
            
            $json[] = htmlspecialchars($inventoryVo2->brand);
            $json[] = htmlspecialchars($inventoryVo2->reference);
            $json[] = htmlspecialchars($inventoryVo2->presentation);
            $string .= json_encode($json) . ",";
        
        }
        }
        $string .= "-]}-";
        if($existData){
            echo  str_replace(",-]}-", "]}", $string);
        }else{
            echo "{ \"data\": [] }";
        }
    }


    public function SetDataModification()
    {
        $serialVo = new SerialVo();
        $generalDao = new GeneralDao();
        $serialVo->numberSerial=$_POST["serial"];    
        $serialVo->numberRow=$_POST["idRow"];
        $serialVo->idInventary=$_POST["idProduct"];   
        $generalDao->set($generalVo = clone $serialVo);
        print_r($generalDao);

    }

     public function SetDataUpdate()
    {
        $serialVo = new SerialVo();
        $generalDao = new GeneralDao();
        $serialVo->numberSerial=$_POST["serial"];    
        $serialVo->id=$_POST["idSerial"];
        $serialVo->numberRow=$_POST["idRow"];
         $serialVo->idInventary=$_POST["idProduct"];
        $generalDao->update($generalVo = clone $serialVo);
        print_r($generalDao);

    }

         private  function GetSerials($idInventary)
    {
        $arrayList = [];
         $SerialVo= new SerialVo();
         $SerialVo->idSearchField = 3;
         $SerialVo->isList = false;
         $SerialVo->idInventary = $idInventary;
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $SerialVo);
        while ($SerialVo2 = $generalDao->GetVo($generalVo = clone $SerialVo)) {
            $arrayList[] = $SerialVo2;
        }
        $this->array_sort_by($arrayList , 'numberRow' , $order = SORT_ASC );
        return $arrayList;

    }


    private function array_sort_by(&$arrIni, $col, $order = SORT_ASC)
{
    $arrAux = array();
    foreach ($arrIni as $key=> $row)
    {
        $arrAux[$key] = is_object($row) ? $arrAux[$key] = $row->$col : $row[$col];
        $arrAux[$key] = strtolower($arrAux[$key]);
    }
    array_multisort($arrAux, $order, $arrIni);
}

   

}

$ajaxDataInventory = new AjaxDataSerial();