function LoadTable() {
    var parameters = {
        "isActionFromAjax": true,
        "optionInputForm": 0,
        "idDataInputForm": 0,
        "idProduct": $("#idProductHidden").val()
    };
    console.log(parameters);

    $('#serialTbl').DataTable({
        responsive: true,
        "drawCallback": function(settings) {
            $('[data-toggle="tooltip"]').tooltip()
        },
        language: {
            "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
        },
        "ajax": {
            url: "view/html/modules/serial/AjaxDataSerial.php",
            method: "POST",
            data: parameters
        }
    });
}

$("#sendExcelTemplateBtn").click(function() {
    if ($(".file").val() == "") {
        MessageSwalBasic("Error", "Por favor seleccione un archivo excel a cargar", "error");
    } else {
        //alert($("#fileExcelTemplateInp").val());
        SetValueToInputText("optionInputTemplateForm", "6");
        $("#excelTemplateForm").submit();
    }
})


function insertSerial(row, product) {
    var parameters = {
        "isActionFromAjax": true,
        "optionInputForm": 1,
        "idDataInputForm": 0,
        "idRow": row,
        "serial": $("#camp" + row).val(),
        "idProduct": product
    };

    $.ajax({
        url: "view/html/modules/serial/AjaxDataSerial.php",
        method: "POST",
        data: parameters,
        success: function(data) {
            location.reload();
            ToastShow("success", "SERIAL INSERTADO !!", 'Se ha insertado el serial en la fila #' + row + ".", "2000");
        }
    });
}


function updateSerial(id, row) {
    var parameters = {
        "isActionFromAjax": true,
        "optionInputForm": 2,
        "idDataInputForm": 0,
        "serial": $("#camp" + row).val(),
        "idRow": row,
        "idSerial": id,
        "idProduct": GetValueToInputText("idProductHidden")
    };
    console.log(parameters);

    $.ajax({
        url: "view/html/modules/serial/AjaxDataSerial.php",
        method: "POST",
        data: parameters,
        success: function(data) {
            ToastShow("success", "SERIAL ACTUALIZADO !!", 'Se ha Actualizado el serial #' + row + ' a "' + $("#camp" + row).val() + '.', "2000");
        }
    });
}

$("#sendExcelTemplateBtn").click(function() {
    if ($(".file").val() == "") {
        MessageSwalBasic("Error", "Por favor seleccione un archivo excel a cargar", "error");
    } else {
        SetValueToInputText("optionInputTemplateForm", "6");
        $("[name=sendExcelTemplateBtn]").attr("disabled", "true");
        $("[name=sendExcelTemplateBtn]").html("<i class='fa fa-spin fa-refresh'></i> Cargando ");
        $("#excelTemplateForm").submit();
    }
})


$(function() {
    RemoveActionModule(8);
    LoadExcelFile();
    LoadTable();
});