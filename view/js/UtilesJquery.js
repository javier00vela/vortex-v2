var arrayResponse = [];

function ResetSelect(idSelect) {
    $("#" + idSelect + " option").each(function() {
        $(this).attr('selected', false) // Add $(this).val() to your list
    });
    $("#" + idSelect + " option[value='']").attr('selected', 'selected');
}

function SetValueToSelect(idSelect, valor, bool) {
    if (bool == true) {
        $("#" + idSelect + " option[value='" + valor + "']").attr("selected", true);
    } else if (bool == false) {
        $("#" + idSelect + " option").eq(valor).attr('selected', 'selected');
    }
}


function GetValueSelect(idSelect) {
    var value = $("#" + idSelect + " option:selected").val();
    return value;
}

function CleanInput(idCampo) {
    $("#" + idCampo).get(0).value = "";
}

function GetValueToInputText(idInputText) {
    valor = $('#' + idInputText).val();
    return valor;
}

function SetValueToInputText(idInputText, value) {
    $('#' + idInputText).val(value);
}

function SetTextToButton(idInputText, value) {
    $('.' + idInputText).text(value);
}

function SetCheckedToInputToggle(id, op) {
    if (op == 1) {
        $("#" + id).parent().removeClass("off");
    } else {
        $("#" + id).parent().addClass("off");
    }
}

function SetOnclickToButton(idButton, onclick) {
    $("#" + idButton).attr("onclick", onclick);
}

function Disabled(idInput) {
    $("#" + idInput).attr("disabled", true);
}

function Enabled(idInput) {
    $("#" + idInput).attr("disabled", false);
}

function DisabledReadonly(idInput) {
    $("#" + idInput).attr("readonly", true);
}

function EnabledReadonly(idInput) {
    $("#" + idInput).attr("readonly", false);
}

function DisabledSelect(idSelect) {
    $("#" + idSelect).attr("disabled", true);
}

function EnabledSelect(idSelect) {
    $("#" + idSelect).attr("disabled", false);
}

function DisabledBtn(idBtn) {
    $("#" + idBtn).removeClass();
    $("#" + idBtn).addClass("button large gray disabledBtn");
    $("#" + idBtn).attr("disabled", true);
}

function EnabledBtn(idBtn, colorBtn) {
    $("#" + idBtn).removeClass("button large gray disabledBtn");
    if (colorBtn != "") {
        $("#" + idBtn).addClass("button large " + colorBtn);
        $("#" + idBtn).attr("disabled", false);
    } else {
        DisabledBtn(idBtn);
    }
}


function ShowElement(idElement) {
    $("#" + idElement).css("display", "block");
}

function HideElement(idElement) {
    $("#" + idElement).css("display", "none");
}

function FadeInElement(idElement) {
    $("#" + idElement).fadeIn(500);
}

function FadeOutElement(idElement) {
    $("#" + idElement).fadeOut(500);
}

function ScrollWinTop() {
    $('html,body').animate({
        scrollTop: 0
    }, 1000);
}

function Calendar(id) {
    $("#" + id).datepicker({
        dateFormat: 'yy-mm-dd'
    });
}

function remplazarCaracteres(text, busca, reemplaza) {
    while (text.toString().indexOf(busca) != -1)
        text = text.toString().replace(busca, reemplaza);

    return text;

}

function ConvertNumber(number) {
    Math.round(number);
    return number.toLocaleString().replace(".", ",").replace(".", ",").replace(".", ",");
}

function ordenarAsc(p_array_json, p_key) {
    p_array_json.sort(function(a, b) {
        return a[p_key] > b[p_key];
    });
}

function CalendarRang(id, id2) {
    $("#" + id).datepicker({
        dateFormat: 'yy-mm-dd',
        defaultDate: "+1w",
        changeMonth: true,
        numberOfMonths: 3,
        minDate: '0',
        onClose: function(selectedDate) {
            $("#" + id2).datepicker("option", "minDate", selectedDate);
        }
    });
    $("#" + id2).datepicker({
        dateFormat: 'yy-mm-dd',
        defaultDate: "+1w",
        changeMonth: true,
        numberOfMonths: 3,
        onClose: function(selectedDate) {
            $("#" + id).datepicker("option", "maxDate", selectedDate);
        }
    });
}

function GetValueCvr(idCheckbox) {
    var arr = new Array();
    $.each($("input[name='" + idCheckbox + "[]']:checked"), function() {
        arr.push($(this).val());
    });
    return arr[0];
}

function ValidateOnlyNumbers(idInput) {
    $(document).ready(function() {
        $("#" + idInput).keydown(function(event) {

            if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 || event.keyCode == 39) {
                return true;
            } else {
                if (event.keyCode < 95) {
                    if (event.keyCode < 48 || event.keyCode > 57) {
                        event.preventDefault();
                    }
                } else {
                    if (event.keyCode < 96 || event.keyCode > 105) {
                        event.preventDefault();
                    }
                }
            }
        });
    });
}

function DeleteHtmlById(idElement) {
    $("#" + idElement).remove();
}

function PrintTable(idButton, idArea) {
    $("#" + idButton).click(function() {
        $("#" + idArea).printArea();
    });
}

function PatternTextNumbers(idInput) {
    $('#' + idInput).on('input', function(e) {
        if (!/^[ a-z0-9áéíóúüñ]*$/i.test(this.value)) {
            this.value = this.value.replace(/[^ a-z0-9áéíóúüñ]+/ig, "");
        }
    });
}

function ExportToExcelTable(idButton, idArea) {
    $("#" + idButton).click(function() {
        window.open('data:application/vnd.ms-excel,' + encodeURIComponent($("#" + idArea).html()));
    });
}

function UpperFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function DeleteLastLetter(string) {
    var string = string.substring(0, string.length - 1);
    return string;
}

function ToastShow(type, title, subtitle, time) {
    switch (type) {
        case "success":
            toastr.success(subtitle, title, { timeOut: time });
            break;
        case "warning":
            toastr.warning(subtitle, title, { timeOut: time });
            break;
        case "error":
            toastr.error(subtitle, title, { timeOut: time });
            break;
    }
}

/***************Countries Lst*******************/



/***************CKEDITOR*******************/

function ApplyCKEDITOR(idControlAtx) {
    if (!CKEDITORsf.instances[idControlAtx]) {
        CKEDITOR.replace(idControlAtx);
        CKEDITOR.config.height = 400;
        config.toolbarGroups = [
            { name: 'document', groups: ['mode', 'document', 'doctools'] },
            { name: 'clipboard', groups: ['clipboard', 'undo'] },
            { name: 'editing', groups: ['find', 'selection', 'spellchecker', 'editing'] },
            { name: 'forms', groups: ['forms'] },
            '/',
            { name: 'basicstyles', groups: ['basicstyles', 'cleanup'] },
            { name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi', 'paragraph'] },
            { name: 'links', groups: ['links'] },
            { name: 'insert', groups: ['insert'] },
            '/',
            { name: 'styles', groups: ['styles'] },
            { name: 'colors', groups: ['colors'] },
            { name: 'tools', groups: ['tools'] },
            { name: 'others', groups: ['others'] },
            { name: 'about', groups: ['about'] }
        ];

        config.removeButtons = 'Save,NewPage,Preview,Print,Source,Templates,Scayt,Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,Flash,Iframe,Link,Unlink,Anchor,ShowBlocks,Maximize,About';
    }
}

function ApplyEditor(idControlAtx, html) {
    // $('#'+idControlAtx).trumbowyg();

    CKEDITOR.instances[idControlAtx].setData(html);
}

/****************************************************************/
function MessageWarningSwal(title, text, idForm) {
    swal({
        title: title,
        text: text,
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, Eliminar!',
        cancelButtonText: 'No, Cancelar!',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: false,
        reverseButtons: true
    }).then(function(result) {
        if (result.value) {
            $("#" + idForm + "Form").submit();
        }
    });
}
/****************************CONTROL MODULE***************/
function SetData() {
    SetValueToInputText("optionInputForm", 1);
    SetValueToInputText("idDataInputForm", 0);
}

function DeleteData(id, idModule) {
    SetValueToInputText("optionInputForm", 2);
    SetValueToInputText("idDataInputForm", id);
    MessageWarningSwal("warning", "¿Desea Eliminar el Registro?", idModule);
}

function UpdateData(id, nameFolder, idModule) {
    SetValueToInputText("optionInputForm", 3);
    SetValueToInputText("idDataInputForm", id);
    SetValueToInputText("isActionFromAjax", "true");
    var parameters = {
        "isActionFromAjax": GetValueToInputText("isActionFromAjax"),
        "optionInputForm": GetValueToInputText("optionInputForm"),
        "idDataInputForm": GetValueToInputText("idDataInputForm")
    };
    console.log(parameters);
    $.ajax({
        data: parameters,
        url: 'view/html/modules/' + nameFolder + '/AjaxData' + UpperFirstLetter(idModule) + '.php',
        type: 'POST',
        success: function(response) {
            $("#containerResponseAjax").html(response);
        }
    });

}

function ResetControls(idForm) {
    SetValueToInputText('optionInputForm', 4);
    $("#" + idForm + "  input[type=text]").each(function() {
        CleanInput($(this).attr('id')); //console.log($(this).attr('id')); //<-- Should return all input elements in that specific form.
    });
    $("#" + idForm + "  input[type=number]").each(function() {
        CleanInput($(this).attr('id')); //console.log($(this).attr('id')); //<-- Should return all input elements in that specific form.
    });
    $("#" + idForm + " select").each(function() {
        ResetSelect($(this).attr('id')); //console.log($(this).attr('id')); //<-- Should return all input elements in that specific form.
    });
}

function MessageSwalBasic(title, message, type) {
    swal(
        title,
        message,
        type
    )
}

function MessageSwalRedirect(title, message, type, module) {
    swal({
        title,
        message,
        type
    }).then(function(result) {
        if (result.value) {
            window.location = module;
        }
    });

}

function MessageSwalConfirm(title, message, type, callback){
    swal({
        title: title,
        text: message,
        type: type,
        showCancelButton: true,
        confirmButtonColor: 'green',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Confirmar!'
      }).then((result) => {
        if (result.value) {
            callback();
        }
      })
}

function MessageSwalWaiter(message, title) {
    let timerInterval
    swal({
        title: title,
        html: message,

        onOpen: () => {
            swal.showLoading()
            timerInterval = setInterval(() => {
                swal.getContent()

            }, 1000)
        }
    })
}


function DisableByRoles(id) {
    var userAllows = null;
    var modulesLenght = 0;
    var bool = false;
    $("[class*=field-]").each(function() {
        userAllows = $(this).attr("data-allow");
        modulesLenght = userAllows.split(",");
        if (modulesLenght.indexOf(id) == -1) {
            $(this)[0].remove();
        }
    });
}

function DisabledNodeByUser() {
    var parameters = {
        "isActionFromAjax": true,
        "optionInputForm": 9,
        "idDataInputForm": 0
    };
    $.ajax({
        url: "view/html/modules/users/AjaxDataUser.php",
        method: "POST",
        data: parameters,
        success: function(data) {
            DisableByRoles(data);
        }
    })
}
/*****************************************************************/

function AjaxControlWithFilterLstFunc(ajaxControlConfigDataLst, dataQueryComponentAjax) {
    var valueInputCtx = GetValueToInputText(ajaxControlConfigDataLst.idCtx);
    dataQueryComponentAjax.valueSearchField = valueInputCtx;
    var parameters = {
        "ajaxControlConfigDataLst": ajaxControlConfigDataLst,
        "dataQueryComponentAjax": dataQueryComponentAjax
    };
    $.ajax({
        data: parameters,
        url: ajaxControlConfigDataLst.loadUrl,
        type: 'post',
        beforeSend: function() {
            $("#" + ajaxControlConfigDataLst.idContainerGeneral).html('<img src="../../view/imgs/loader2.gif" width="35" height="35"/>').show();
        },
        success: function(response) {
            $("#" + ajaxControlConfigDataLst.idContainerGeneral).html(response);
        }
    });
}

function AjaxControlTbl(ajaxControlConfigDataTbl) {
    var parameters = {
        "ajaxControlConfigDataTbl": ajaxControlConfigDataTbl
    };
    $.ajax({
        data: parameters,
        url: ajaxControlConfigDataTbl.loadUrl,
        type: 'post',
        beforeSend: function() {
            $("#" + ajaxControlConfigDataTbl.idContainerTbl).html('<img src="../../view/imgs/loader2.gif" width="35" height="35"/>').show();
        },
        success: function(response) {
            $("#" + ajaxControlConfigDataTbl.idContainerTbl).html(response);
        }
    });
}

function AjaxExcelTbl(ajaxControlConfigDataTbl) {
    var parameters = {
        "ajaxControlConfigDataTbl": ajaxControlConfigDataTbl
    };
    $.ajax({
        data: parameters,
        url: ajaxControlConfigDataTbl.loadUrl,
        type: 'post',
        beforeSend: function() {
            $("#" + ajaxControlConfigDataTbl.idContainerTbl).html('<img src="../../view/imgs/loader2.gif" width="35" height="35"/>').show();
        },
        success: function(response) {
            $("#" + ajaxControlConfigDataTbl.idContainerTbl).html(response);
        }
    });
}

function AbleModulesByRol(id) {
    var userAllows = null;
    var modulesLenght = 0;
    var bool = false;
    $("[class*=col-]").each(function() {
        userAllows = $(this).attr("data-allow");
        modulesLenght = userAllows.split(",");
        if (modulesLenght.indexOf(id) == -1) {
            $(this)[0].remove();
        }
    });
}

function GetAllDataTableToExcel(module, path, table) {
    var table = $('#' + table).DataTable();
    var data = new Array();
    var HTMLDatatable = table.rows({ page: 'all', order: 'index', search: 'applied' }).data();

    $.each(HTMLDatatable, function(i, dataRows) {
        cant = parseInt(dataRows.length) - 1;
        for (var e = 0; e < cant; e++) {
            dataRows[e] = dataRows[e].replace(",", "*|*");
        }

        data += "||" + dataRows;
    });
    data = data.split("||");
    console.log(data);
    $.ajax({
        data: { 'data': data },
        url: '/vortexcompany/' + module,
        method: "POST",

        beforeSend: function() {
            MessageSwalWaiter("descargando reporte excel ... ", "Generando Reporte");
        },
        success: function(response) {
            window.location.href = path + "?v=" + Math.random(); //;   
            MessageSwalBasic("GENERADO !!", "Se ha descargado formato excel ", "success");
        }
    });
}


function LoadExcelFile() {
    $(".file").css({ "visibility": "hidden", "position": "absolute" });
    $(document).on('click', '.browse', function() {
        var file = $(this).parent().parent().parent().find('.file');
        file.trigger('click');
    });
    $(document).on('change', '.file', function() {
        $(this).parent().find('.form-control').val($(this).val().replace(/C:\\fakepath\\/i, ''));
    });
}

function RemoveActionModule(idModule, tbl) {
    SetDataRemoveAtModule(idModule, 1);
}

function GetArrayFromDataModule(idModule) {

    SetDataRemoveAtModule(idModule, 2);

}



function SetDataResponseModule(resp) {
    arrayResponse = resp;

}

function SetDataRemoveAtModule(id, action) {
    var parameters = {
        "isActionFromAjax": true,
        "optionInputForm": 8,
        "idDataInputForm": 0,
        "module": id
    };
    $.ajax({
        data: parameters,
        url: "view/html/modules/modulesactions/AjaxDataModulesActions.php",
        type: 'post',
        success: function(response) {
            //console.log(response)
            response = JSON.parse(response);
            if (action == 1) {
                for (var i = 0; i < response.length; i++) {
                    $("[node=node-" + response[i] + "]").each(function() {
                        $(this).remove();
                    });
                }
            }

            if (action == 2) {

                SetDataResponseModule(response);
            }


        }
    });
}