<?php
require_once ('controller/general/Config.php');
require_once (Config::PATH.Config::CONTROLLER_LIB.'vendor/autoload.php');
require_once (Config::PATH.Config::CONTROLLER."general/TemplateCtrl.php");
header("Expires: on, 01 Jan 1970 00:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Cache-Control: no-cache, must-revalidate");
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
session_start();
ob_start();
set_time_limit(300);

date_default_timezone_set('America/New_York');
TemplateCtrl::IncludeTemplate();
ini_set('MAX_EXECUTION_TIME', '0');
error_reporting(0);  
?>
