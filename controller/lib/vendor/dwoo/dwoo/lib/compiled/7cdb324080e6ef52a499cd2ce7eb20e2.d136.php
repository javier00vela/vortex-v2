<?php
/* template head */
/* end template head */ ob_start(); /* template body */ ?><div class="content-wrapper">
    <section class="content-header">
        <h1 class="box-title">Administrar Prospectos</h1>
        <ol class="breadcrumb">
            <li><a href="Home"><i class="fa fa-dashboard"></i> Inicio</a></li>
            <li class="active">Administrar Prospectos</li>
        </ol>
    </section>
    <section class="content">

        <div class="box">
            <div class="box-header ">

                <nav class="navbar navbar-light">
                    <div class="container-fluid">
                        <ul class="nav navbar-nav">
                            <li>
                                <a node="node-agregar_prospecto" style="cursor: pointer; " id="userAddBtn" name="userAddBtn" data-toggle="modal" data-target="#personPopUpAdd" onclick="SetData()">
                                    <i class="fa fa-plus-circle"></i> Agregar Prospecto
                                </a>
                            </li>
                            <li>
                                <a style="cursor: pointer;" id="userAddBtn" name="userAddBtn" onclick="location.href = 'Home' ">
                                    <i class="fa fa-home"></i> Menu Principal
                                </a>
                            </li>


                        </ul>
                    </div>
                </nav>

            </div>
            <div class="box-body">

                <table id="userTbl" name="userTbl" class="table table-bordered table-striped dt-responsive tables dataTable no-footer dtr-inline" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>id</th>
                            <th>Nombres</th>
                            <th>Apellidos</th>
                            <th>Celular</th>
                            <th>Telefono</th>
                            <th>Email</th>
                            <th>Dirección</th>
                            <th>Ciudad</th>
                            <th>Fecha de Creación</th>
                            <th>Role</th>
                            <th>Empresa</th>
                            <th>Marca Seleccionada</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </section>
</div>
<!--=====================================
VENTANA MODAL AGREGAR PERSONA O MODIFICAR
======================================-->
<div id="personPopUpAdd" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form id="personForm" role="form" method="post" enctype="multipart/form-data">
                <!--=====================================
              Hidden
              ======================================-->
                <input id="optionInputForm" name="optionInputForm" type="hidden" value="0">
                <input id="idDataInputForm" name="idDataInputForm" type="hidden" value="0">
                <input id="isActionFromAjax" name="isActionFromAjax" type="hidden" value="false">
                <!--=====================================
              CABEZA DEL MODAL
              ======================================-->
                <div class="modal-header" style="background:#3c8dbc; color:white">
                    <button type="button" class="close" data-dismiss="modal" data-target='#personPopUpAdd' onclick="ResetControls('personForm')">&times;</button>
                    <h4 class="modal-title">Agregar prospecto</h4>
                </div>
                <!--=====================================
              CUERPO DEL MODAL
              ======================================-->
                <div class="modal-body">
                    <div class="box-body">
                        <div class="row">
                            <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-user"></i></span> <?php echo $this->scope["personNamesCtx"];?>
                                    <!-- <input id="userNamesCtx" name="userNamesCtx" type="text" class="form-control input-lg" placeholder="Ingresar nombres" required> -->
                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-user-o"></i></span> <?php echo $this->scope["personLastNamesCtx"];?>
                                    <!-- <input id="userLastNamesCtx" name="userLastNamesCtx" type="text" class="form-control input-lg"  placeholder="Ingresar apellidos" required> -->
                                </div>
                            </div>
                            <!-- <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-id-card"></i></span> <?php echo $this->scope["personDocumentTypeLst"];?>
                   
                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-id-card-o"></i></span> <?php echo $this->scope["personDocumentCtx"];?>
                  
                                </div>
                            </div> -->
                            <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-mobile"></i></span> <?php echo $this->scope["personCellPhoneCtx"];?>
                                    <!-- <input id="userCellNumberCtx" name="userCellNumberCtx" type="text" class="form-control input-lg" placeholder="Ingresar numero celular" required> -->
                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-phone"></i></span> <?php echo $this->scope["personTelephoneCtx"];?>
                                    <!-- <input id="userTelePhoneNumberCtx" name="userTelePhoneNumberCtx" type="text" class="form-control input-lg" name="nuevoTelefono" placeholder="Ingresar Telefono"> -->
                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-envelope-o"></i></span> <?php echo $this->scope["personEmailCtx"];?>
                                    <!-- <input id="userEmailCtx" name="userEmailCtx" type="text" class="form-control input-lg" name="nuevoEmail" placeholder="Ingresar Email" required> -->
                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-map-marker"></i></span> <?php echo $this->scope["personAddressCtx"];?>
                                    <!-- <input id="userEmailCtx" name="userEmailCtx" type="text" class="form-control input-lg" name="nuevoEmail" placeholder="Ingresar Email" required> -->
                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-map-marker"></i></span> <?php echo $this->scope["personCityCtx"];?>
                                    <!-- <input id="userEmailCtx" name="userEmailCtx" type="text" class="form-control input-lg" name="nuevoEmail" placeholder="Ingresar Email" required> -->
                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-building"></i></span> <?php echo $this->scope["companyLst"];?>
                                    <!-- <input id="userEmailCtx" name="userEmailCtx" type="text" class="form-control input-lg" name="nuevoEmail" placeholder="Ingresar Email" required> -->
                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-building"></i></span> <?php echo $this->scope["BrandLst"];?>
                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-list"></i></span>
                                    <div><?php echo $this->scope["personUserLst"];?></div>
                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-cubes"></i></span>
                                    <div><?php echo $this->scope["personPrefijoLst"];?></div>
                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-cube"></i></span> <textarea class="form-control" id="description" rows="10" style="resize:none;" placeholder="Descripción de requerimientos del prospecto" name="description"></textarea>
                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="input-group">
                                    <?php echo $this->scope["personIdRoleHidden"];?>
                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="input-group">
                                    <?php echo $this->scope["personIdCompanyHidden"];?>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <!--=====================================
              PIE DEL MODAL
              ======================================-->
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal" data-target='#personPopUpAdd' onclick="ResetControls('personForm')">Salir</button>
                    <button type="submit" class="btn btn-primary">Guardar Prospecto</button>
                </div>
            </form>

        </div>
    </div>
</div>






<!--==================================================================
=            MODAL AGREGAR CONATCTOS DESDE PLANTILLA DE EXCEL           =
======================================================================-->
<!-- Modal -->
<div id="modalAddTemplateExcel" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">

            <form id="excelTemplateForm" name="ExcelTemplateForm" role="form" method="post" enctype="multipart/form-data">
                <input id="optionInputTemplateForm" name="optionInputTemplateForm" type="hidden" value="0">
                <div class="modal-header" style="background: #3c8dbc; color: white">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Agregar Contactos</h4>
                </div>
                <div class="modal-body">
                    <?php echo $this->scope["idCompayInputForm"];?>
                    <input type="hidden" name="MAX_FILE_SIZE" value="2000000" />
                    <table width="600">
                        <tr>
                            <th>Nombre Archivo:</th>
                            <th><input id="fileExcelTemplateInp" name="fileExcelTemplateInp" type="file" class="file" accept=".xlsx" /></th>
                        </tr>
                    </table>
                </div>
                <div class="modal-footer">
                    <button id="exitExcelTemplateBtn" type="button" class="btn btn-default pull-left" data-dismiss="modal" onclick="ResetControls('ExcelTemplateForm')">Cerrar</button>
                    <button id="sendExcelTemplateBtn" name="sendExcelTemplateBtn" type="button" class="importExcel btn btn-primary">Importar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div id="containerResponseAjax"></div>
<script src="view/html/modules/prospect/Prospect.js?v= <?php echo $this->scope["tempParameter"];?>"></script>
<?php echo $this->scope["jquery"];
 /* end template body */
return $this->buffer . ob_get_clean();
?>