<?php
/* template head */
/* end template head */ ob_start(); /* template body */ ?><form method="post" enctype="multipart/form-data">
    <input id="optionInputForm" name="optionInputForm" type="hidden" value="1">
    <input id="idDataInputForm" name="idDataInputForm" type="hidden" value="0">
    <input id="isActionFromAjax" name="isActionFromAjax" type="hidden" value="false">
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                Diagnostico <strong>('<?php echo $this->readVarInto(array (  1 =>   array (    0 => '->',  ),  2 =>   array (    0 => 'name',  ),  3 =>   array (    0 => '',    1 => '',  ),), $this->scope["productData"], false);?>')</strong>
            </h1>
            <ol class="breadcrumb">
                <li><a href="Home"><i class="fa fa-dashboard"></i> Inicio</a></li>
                <li class="active">Diagnostico <?php echo $this->readVarInto(array (  1 =>   array (    0 => '->',  ),  2 =>   array (    0 => 'name',  ),  3 =>   array (    0 => '',    1 => '',  ),), $this->scope["productData"], false);?></li>
            </ol>
        </section>
        <section class="content">
            <div class="box">
                <nav class="navbar navbar-light">
                    <div class="container-fluid">
                        <ul class="nav navbar-nav">
                            <li>
                                <a style="cursor: pointer;" id="userAddBtn" name="userAddBtn" onclick="location.href = 'Home' ">
                                    <i class="fa fa-home"></i> Menu Principal
                                </a>
                            </li>
                        </ul>
                    </div>
                </nav>
                <input type="hidden" id="diagnosisBool" data-diagnosis="<?php echo $this->scope["diagnosisId"];?>" value="<?php echo $this->scope["diagnosisBool"];?>">
                <div class="box-body ">
                    <div class="row text-center">
                        <div class="col-sm-6">
                            <div class="box box-primary" style="box-shadow: 0 0 4px black;">
                                <div class="box-tools pull-right" data-toggle="tooltip" title="" data-original-title="Status">
                                    <small class="label pull-right bg-green">5%</small>
                                </div>
                                <div class="box-header with-border">
                                    Datos Cliente
                                </div>
                                <div class="box-body">
                                    <ul class="list-group">
                                        <li class="list-group-item"><b>nombre</b> : <?php if ($this->readVarInto(array (  1 =>   array (    0 => '->',  ),  2 =>   array (    0 => 'name',  ),  3 =>   array (    0 => '',    1 => '',  ),), (isset($this->scope["companyData"]) ? $this->scope["companyData"]:null), true) == "") {
?> Equipo Externo <?php 
}
else {
?> <?php echo $this->readVarInto(array (  1 =>   array (    0 => '->',  ),  2 =>   array (    0 => 'name',  ),  3 =>   array (    0 => '',    1 => '',  ),), $this->scope["companyData"], false);?> <?php 
}?></li>
                                        <li class="list-group-item"><b>dirección</b> :  <?php if ($this->readVarInto(array (  1 =>   array (    0 => '->',  ),  2 =>   array (    0 => 'address',  ),  3 =>   array (    0 => '',    1 => '',  ),), (isset($this->scope["companyData"]) ? $this->scope["companyData"]:null), true) == "") {
?> Equipo Externo <?php 
}
else {
?> <?php echo $this->readVarInto(array (  1 =>   array (    0 => '->',  ),  2 =>   array (    0 => 'address',  ),  3 =>   array (    0 => '',    1 => '',  ),), $this->scope["companyData"], false);?> <?php 
}?></li>
                                        <li class="list-group-item"><b>telefóno</b> : <?php if ($this->readVarInto(array (  1 =>   array (    0 => '->',  ),  2 =>   array (    0 => 'phone',  ),  3 =>   array (    0 => '',    1 => '',  ),), (isset($this->scope["companyData"]) ? $this->scope["companyData"]:null), true) == "") {
?> Equipo Externo <?php 
}
else {
?> <?php echo $this->readVarInto(array (  1 =>   array (    0 => '->',  ),  2 =>   array (    0 => 'phone',  ),  3 =>   array (    0 => '',    1 => '',  ),), $this->scope["companyData"], false);?> <?php 
}?> </li>
                                        <li class="list-group-item"><b>correo</b> : <?php if ($this->readVarInto(array (  1 =>   array (    0 => '->',  ),  2 =>   array (    0 => 'email',  ),  3 =>   array (    0 => '',    1 => '',  ),), (isset($this->scope["companyData"]) ? $this->scope["companyData"]:null), true) == "") {
?> Equipo Externo <?php 
}
else {
?> <?php echo $this->readVarInto(array (  1 =>   array (    0 => '->',  ),  2 =>   array (    0 => 'email',  ),  3 =>   array (    0 => '',    1 => '',  ),), $this->scope["companyData"], false);?> <?php 
}?></li>
                                        <li class="list-group-item"><b>Web</b> : <?php if ($this->readVarInto(array (  1 =>   array (    0 => '->',  ),  2 =>   array (    0 => 'webSite',  ),  3 =>   array (    0 => '',    1 => '',  ),), (isset($this->scope["companyData"]) ? $this->scope["companyData"]:null), true) == "") {
?> Equipo Externo <?php 
}
else {
?> <?php echo $this->readVarInto(array (  1 =>   array (    0 => '->',  ),  2 =>   array (    0 => 'webSite',  ),  3 =>   array (    0 => '',    1 => '',  ),), $this->scope["companyData"], false);?> <?php 
}?></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="box box-primary" style="box-shadow: 0 0 4px black;">
                                <div class="box-tools pull-right" data-toggle="tooltip" title="" data-original-title="Status">
                                    <small class="label pull-right bg-green">5%</small>
                                </div>
                                <div class="box-header with-border">
                                    Datos Producto
                                </div>
                                <div class="box-body">
                                    <ul class="list-group">
                                        <li class="list-group-item"><b>nombre</b> : <?php echo $this->readVarInto(array (  1 =>   array (    0 => '->',  ),  2 =>   array (    0 => 'name',  ),  3 =>   array (    0 => '',    1 => '',  ),), $this->scope["productData"], false);?></li>
                                        <li class="list-group-item"><b>referencia</b> : <?php echo $this->readVarInto(array (  1 =>   array (    0 => '->',  ),  2 =>   array (    0 => 'reference',  ),  3 =>   array (    0 => '',    1 => '',  ),), $this->scope["productData"], false);?></li>
                                        <li class="list-group-item"><b>marca</b> : <?php echo $this->readVarInto(array (  1 =>   array (    0 => '->',  ),  2 =>   array (    0 => 'brand',  ),  3 =>   array (    0 => '',    1 => '',  ),), $this->scope["productData"], false);?></li>
                                        <li class="list-group-item"><b>presentación</b> : <?php echo $this->readVarInto(array (  1 =>   array (    0 => '->',  ),  2 =>   array (    0 => 'presentation',  ),  3 =>   array (    0 => '',    1 => '',  ),), $this->scope["productData"], false);?></li>
                                        <li class="list-group-item"><b>Tipo Producto</b> : <?php echo $this->readVarInto(array (  1 =>   array (    0 => '->',  ),  2 =>   array (    0 => 'typeProduct',  ),  3 =>   array (    0 => '',    1 => '',  ),), $this->scope["productData"], false);?></li>
                                    </ul>
                                </div>
                            </div>

                        </div>

                    </div>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="box box-primary" style="box-shadow: 0 0 4px black;">
                                <div class="box-tools pull-right" data-toggle="tooltip" title="" data-original-title="Status">
                                    <small class="label pull-right bg-green">10%</small>
                                </div>
                                <div class="box-header with-border">
                                    Comentario Cliente
                                </div>
                                <div class="box-body">
                                    <textarea class="form-control" name="CommentClient" cols="30" rows="10" style="resize: none;"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="box box-primary" style="box-shadow: 0 0 4px black;">
                                <div class="box-tools pull-right" data-toggle="tooltip" title="" data-original-title="Status">
                                    <small class="label pull-right bg-green">50%</small>
                                </div>
                                <div class="box-header with-border">
                                    Estado Inicial
                                </div>
                                <div class="box-body">

                                    <div class="box" style="box-shadow: 0 0 1px black;">
                                        <div class="box-header with-border">
                                            Estado Encontrado de la maquina
                                        </div>
                                        <div class="box-body">
                                            <textarea class="form-control" name="MachineState" cols="30" rows="10" style="resize: none;"></textarea>
                                        </div>
                                    </div>

                                    <div class="box" style="box-shadow: 0 0 1px black;">
                                        <div class="box-header with-border">
                                            Fotos y/o Evidencias
                                        </div>
                                        <input type="hidden" name="photo1temp" value="null">
                                        <input type="hidden" name="photo2temp" value="null">
                                        <input type="hidden" name="photo3temp" value="null">
                                        <div class="box-body">
                                            <div class="row text-center">
                                                <div class="col-sm-4">

                                                    <img src="view/img/upload.jpg" class="img-responsive" id="photo1" width="350" height="250">

                                                    <input type="hidden" name="MAX_FILE_SIZE" value="2000000" />
                                                    <div class="form-group">
                                                        <label>Inserte una imagen para ser cargada :</label>
                                                        <input type="file" photo-node="0" name="photo1" accept="image/*" class="file">
                                                        <div class="input-group col-12">
                                                            <span class="input-group-addon"><i class="glyphicon glyphicon-folder-open"></i></span>
                                                            <input type="text" class="form-control input-lg" disabled placeholder="Sin Imagen">
                                                            <span class="input-group-btn">
                                          <button class="browse btn btn-primary input-lg" type="button"><i class="fa fa-plus"></i></button>
                                        </span>
                                                        </div>
                                                    </div>


                                                </div>
                                                <div class="col-sm-4">

                                                    <img src="view/img/upload.jpg" class="img-responsive" id="photo2" width="350" height="250">
                                                    <input type="hidden" name="MAX_FILE_SIZE" value="2000000" />
                                                    <div class="form-group">
                                                        <label>Inserte una imagen para ser cargada :</label>
                                                        <input type="file" name="photo2" photo-node="3" accept="image/*" class="file">
                                                        <div class="input-group col-12">
                                                            <span class="input-group-addon"><i class="glyphicon glyphicon-folder-open"></i></span>
                                                            <input type="text" class="form-control input-lg" disabled placeholder="Sin Imagen">
                                                            <span class="input-group-btn">
                                          <button class="browse btn btn-primary input-lg" type="button"><i class="fa fa-plus"></i></button>
                                        </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">

                                                    <img src="view/img/upload.jpg" class="img-responsive" id="photo3" width="350" height="250">
                                                    <input type="hidden" name="MAX_FILE_SIZE" value="2000000" photo-node="7" />
                                                    <div class="form-group">
                                                        <label>Inserte una imagen para ser cargada :</label>
                                                        <input type="file" name="photo3" accept="image/*" class="file" photo-node="6">
                                                        <div class="input-group col-12">
                                                            <span class="input-group-addon"><i class="glyphicon glyphicon-folder-open"></i></span>
                                                            <input type="text" class="form-control input-lg" disabled placeholder="Sin Imagen">
                                                            <span class="input-group-btn">
                                          <button class="browse btn btn-primary input-lg" type="button"><i class="fa fa-plus"></i></button>
                                        </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container-fluid">
                    <div class="row">
                        <div class="box box-primary" style="box-shadow: 0 0 4px black;">
                            <div class="box-tools pull-right" data-toggle="tooltip" title="" data-original-title="Status">
                                <small class="label pull-right bg-green">25%</small>
                            </div>
                            <div class="box-header with-border">
                                Resultados de Funcionamiento
                            </div>
                            <div class="box-body">

                                <div class="box" style="box-shadow: 0 0 1px black;">
                                    <div class="box-header with-border">
                                        Resultados de Evaluación de Funcionamiento
                                    </div>
                                    <div class="box-body">
                                        <div class="col-sm-4">
                                            <input type="checkbox" name="MachineFunction" data-toggle="toggle" data-on="Tuvo funcionamiento Correctivo y satisfacción" data-off="No Tuvo funcionamiento Correctivo y satisfacción" data-width="100%" data-onstyle="success" data-offstyle="danger">
                                        </div>
                                        <div class="col-sm-4">
                                            <input type="checkbox" name="Machinebroke" data-toggle="toggle" data-on="Funcional con Daño Menor" data-off="No Funcional con Daño Menor" data-width="100%" data-onstyle="success" data-offstyle="danger">
                                        </div>
                                        <div class="col-sm-4">
                                            <input type="checkbox" name="MachineCorrective" data-toggle="toggle" data-on=" Equipo para Mantenimiento Correctivo" data-off="Sin Equipo para Mantenimiento Correctivo" data-width="100%" data-onstyle="success" data-offstyle="danger">
                                        </div>
                                    </div>
                                </div>

                                <div class="box" style="box-shadow: 0 0 1px black;">

                                    <div class="box-header with-border">
                                        Observaciones Equipo
                                    </div>
                                    <div class="box-body">
                                        <textarea class="form-control" name="observationMachine" cols="30" rows="10" style="resize: none;"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="container-fluid">
                    <div class="row">
                        <div class="box box-primary" style="box-shadow: 0 0 4px black;">
                            <div class="box-tools pull-right" data-toggle="tooltip" title="" data-original-title="Status">
                                <small class="label pull-right bg-green">5%</small>
                            </div>
                            <div class="box-header with-border">
                                Firmados
                            </div>
                            <div class="box-body">
                                <div class="col-sm-6">
                                    <label>Firma Cliente</label> <?php echo $this->scope["FirmClientCtx"];?>

                                </div>
                                <div class="col-sm-6">
                                    <label>Firma Usuario</label> <?php echo $this->scope["FirmUserCtx"];?>

                                </div>

                            </div>
                            <div class="box-footer">
                                <button type="submit" id="send" class="btn btn-primary btn-block">Agregar</button>
                            </div>

                        </div>
                    </div>
                </div>
        </section>
        </div>
    </div>
</form>
<div id="containerResponseAjax"></div>
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<script src="view/html/modules/diagnosis/Diagnosis.js?v= <?php echo $this->scope["tempParameter"];?>"></script>
<?php echo $this->scope["jquery"];
 /* end template body */
return $this->buffer . ob_get_clean();
?>