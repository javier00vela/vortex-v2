<?php
/* template head */
/* end template head */ ob_start(); /* template body */ ?>

<div class="content-wrapper">
  <section class="content-header">
    <h1>
      Administrar Productos
    </h1>
    <ol class="breadcrumb">
      <li><a href="Home"><i class="fa fa-dashboard"></i> Inicio</a></li>
      <li class="active">Administrar productos</li>
    </ol>
  </section>
  <section class="content">
    <div class="box">
      <div class="box-header with-border">
          <nav class="navbar navbar-light">
              <div class="container-fluid">
                  <ul class="nav navbar-nav">
                      <li>
                          <a style="cursor: pointer;" id="userAddBtn" name="userAddBtn" onclick="location.href = 'Inventory' ">
                              <i class="fa fa-arrow-left"></i>
                              Regresar
                          </a>
                      </li>
                        <li>
                          <a style="cursor: pointer;" id="userAddBtn" name="userAddBtn" onclick="location.href = 'Home' ">
                              <i class="fa fa-home"></i>
                              Menu Principal
                          </a>
                      </li>
                  </ul>
              </div>
          </nav>
      </div>
      <div class="box-body">
        <?php echo $this->scope["idProductInputForm"];?>

        <table id="productTbl" name="productTbl" class="table table-bordered table-striped dt-responsive tables dataTable no-footer dtr-inline" width="100%" cellspacing="0">
            <thead>
                <tr>
                  <th>Id</th>
                  <th>Codigo Cotización</th>
                  <th>Responsable</th>
                  <th>Empresa</th>
                  <th>Usuario(s) cotizados</th>
                  <th>Fecha de Cotización</th>
                  <th>proveedor</th>
                  <th>opciones</th>
                </tr>
            </thead>
        </table>
      </div>
    </div>
  </section>
</div>


<!--/*=====  End of Section comment block  ======*/-->
<div id="containerResponseAjax"></div>
<script src="view/html/modules/historyproducts/HistoryProducts.js?v= <?php echo $this->scope["tempParameter"];?>"></script>

<?php echo $this->scope["jquery"];?>

<?php  /* end template body */
return $this->buffer . ob_get_clean();
?>