<?php
/* template head */
/* end template head */ ob_start(); /* template body */ ?><div class="content-wrapper">
    <section class="content-header m-2">
        <h1>
            Tutoriales
        </h1>
        <ol class="breadcrumb">
            <li><a href="Home"><i class="fa fa-dashboard"></i> Inicio</a></li>
            <li class="active">Tutoriales</li>
        </ol>
    </section>
    <section class="content">
        <div class="box">
            <div class="box-body">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12 col-xs-12">
                            <div class="form-group" style="float: left">
                                <label style="float: left ; text-align: center">Modulo de Inventario</label>
                                <video width="100%" height="360" controls>
                                    <source src="view/videos/modulo_inventario.mp4" type="video/mp4">
                                    Tu navegador no soporta HTML5 video.
                              </video>
                            </div>
                        </div>
                        <div class="col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>Menu Principal</label>
                                <video width="100%" height="360" controls>
                                    <source src="view/videos/inicio_principal.mp4" type="video/mp4">
                                    Tu navegador no soporta HTML5 video.
                              </video>
                            </div>
                        </div>
                        <div class="col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>Modulo de contactos</label>
                                <video width="100%" height="360" controls>
                                    <source src="view/videos/modulo_contactos.mp4" type="video/mp4">
                                    Tu navegador no soporta HTML5 video.
                              </video>
                            </div>
                        </div>
                        <div class="col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>Modulo de costos</label>
                                <video width="100%" height="360" controls>
                                    <source src="view/videos/modulo_costos.mp4" type="video/mp4">
                                    Tu navegador no soporta HTML5 video.
                              </video>
                            </div>
                        </div>
                        <div class="col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>Modulo de empresas</label>
                                <video width="100%" height="360" controls>
                                    <source src="view/videos/modulo_empresas.mp4" type="video/mp4">
                                    Tu navegador no soporta HTML5 video.
                              </video>
                            </div>
                        </div>
                        <div class="col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>Modulo de prospectos</label>
                                <video width="100%" height="360" controls>
                                    <source src="view/videos/modulo_prospectos.mp4" type="video/mp4">
                                    Tu navegador no soporta HTML5 video.
                              </video>
                            </div>
                        </div>
                        <div class="col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>Modulo de proveedores</label>
                                <video width="100%" height="360" controls>
                                    <source src="view/videos/modulo_proveedores.mp4" type="video/mp4">
                                    Tu navegador no soporta HTML5 video.
                              </video>
                            </div>
                        </div>
                        <div class="col-sm-12 col-xs-12" style="float: left">
                                <label>Modulo de usuarios</label>
                                <video width="100%" height="360" controls>
                                    <source src="view/videos/modulo_usuarios.mp4" type="video/mp4">
                                    Tu navegador no soporta HTML5 video.
                              </video>
                       
                        </div>
                        <div class="col-sm-12 col-xs-12" style="float: left">
                            <label>Modulo de calendario</label>
                            <video width="100%" height="360" controls>
                                <source src="view/videos/modulo_calendario.mp4" type="video/mp4">
                                Tu navegador no soporta HTML5 video.
                          </video>
                   
                    </div>
                    <div class="col-sm-12 col-xs-12" style="float: left">
                        <label>Modulo de configuraciones</label>
                        <video width="100%" height="360" controls>
                            <source src="view/videos/modulo_configuraciones.mp4" type="video/mp4">
                            Tu navegador no soporta HTML5 video.
                      </video>
               
                </div>
                <div class="col-sm-12 col-xs-12" style="float: left">
                    <label>Modulo de cotización</label>
                    <video width="100%" height="360" controls>
                        <source src="view/videos/modulo_cotizacion.mp4" type="video/mp4">
                        Tu navegador no soporta HTML5 video.
                  </video>
           
            </div>
            <div class="col-sm-12 col-xs-12" style="float: left">
                <label>Modulo de reportes</label>
                <video width="100%" height="360" controls>
                    <source src="view/videos/modulo_reportes.mp4" type="video/mp4">
                    Tu navegador no soporta HTML5 video.
              </video>
       
        </div>
        <div class="col-sm-12 col-xs-12" style="float: left">
            <label>Modulo de visitas</label>
            <video width="100%" height="360" controls>
                <source src="view/videos/modulo_visitas.mp4" type="video/mp4">
                Tu navegador no soporta HTML5 video.
          </video>
   
    </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div><?php  /* end template body */
return $this->buffer . ob_get_clean();
?>