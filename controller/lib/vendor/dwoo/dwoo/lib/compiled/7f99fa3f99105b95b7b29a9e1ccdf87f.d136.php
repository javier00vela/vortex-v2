<?php
/* template head */
/* end template head */ ob_start(); /* template body */ ?><div class="content-wrapper">
    <section class="content-header m-2">
        <h1>
            Agregar/Modificar Precios Viaticos
        </h1>
        <ol class="breadcrumb">
            <li><a href="Home"><i class="fa fa-dashboard"></i> Inicio</a></li>
            <li class="active">Viaticos</li>
            <li class="active">Agregar/Modificar Precios Viaticos </li>
        </ol>
    </section>
    <section class="content">
        <div class="box m-5">
            <div class="box ">
                <nav class="navbar navbar-light">
                    <div class="container-fluid">
                        <ul class="nav navbar-nav">
                            <li>
                                <a style="cursor: pointer;" id="userAddBtn" name="userAddBtn" onclick="location.href = 'Viatics'">
                                    <i class="fa fa-arrow-left"></i> Regresar
                                </a>
                            </li>
                            <li>
                                <a style="cursor: pointer;" id="userAddBtn" name="userAddBtn"
                                    onclick="location.href = 'Home' ">
                                    <i class="fa fa-home"></i> Menu Principal
                                </a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
            <div class="box-body">
                <ul class="list-group">
                    <input type="hidden" id="idViatic" value="<?php echo $this->readVarInto(array (  1 =>   array (    0 => '->',  ),  2 =>   array (    0 => 'id',  ),  3 =>   array (    0 => '',    1 => '',  ),), $this->scope["viaticsDataById"], false);?>" >
                    <div class="col-sm-6">
                            <div class="info-box bg-purple">
                                <span class="info-box-icon"><i class="fa fa-cube"></i></span>
                                <div class="info-box-content">
                                    <span class="info-box-text">Nombre Viatico:</span>
                                    <span class="info-box-number" id="DDP"> <?php echo $this->readVarInto(array (  1 =>   array (    0 => '->',  ),  2 =>   array (    0 => 'place',  ),  3 =>   array (    0 => '',    1 => '',  ),), $this->scope["viaticsDataById"], false);?> </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                                <div class="info-box bg-red">
                                    <span class="info-box-icon"><i class="fa fa-sun-o"></i></span>
                                    <div class="info-box-content">
                                        <span class="info-box-text">Días Viaje:</span>
                                        <span class="info-box-number" id="DDP"><?php echo $this->readVarInto(array (  1 =>   array (    0 => '->',  ),  2 =>   array (    0 => 'days',  ),  3 =>   array (    0 => '',    1 => '',  ),), $this->scope["viaticsDataById"], false);?> día/s</span>
                                    </div>
                                </div>
                            </div>
                    <div class="col-sm-12">
                        <div class="info-box bg-green">
                            <span class="info-box-icon"><i class="fa fa-dollar"></i></span>
                            <div class="info-box-content">
                                <span class="info-box-text">Total Viaje:</span>
                                <span class="info-box-number" id="DDP"> $<?php echo $this->scope["viaticsData"]["total"];?> COP</span>
                            </div>
                        </div>
                    </div>
                </ul>
            </div>
            <div class="box">
                <nav class="bg-primary">
                    <p class="text-center">Transporte</p>
                </nav>
                <nav class="navbar navbar-light">
                    <div class="container-fluid">
                        <ul class="nav navbar-nav">
                            <li>
                                <a node="node-agregar_costo" style="cursor: pointer;" id="impostAddBtn"
                                    name="impostAddBtn" data-toggle="modal" data-target="#impostPopUpAdd"
                                    onclick="ResetControls('containerinputs');SetData();$('#typeViatic').val('transport')">
                                    <i class="fa fa-plus-circle"></i> Agregar costo
                                </a>
                            </li>
                        </ul>
                    </div>
                </nav>

                <div class="box-body">
                    <div class="col-sm-7 col-xs-12">
                        <table id="transportTbl" name="transportTbl"
                            class="table table-bordered table-striped dt-responsive tables dataTable no-footer dtr-inline"
                            style="width: 100%;" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>Nombre</th>
                                    <th>Porcentaje</th>
                                    <th>Cantidad</th>
                                    <th>Precio individual</th>
                                    <th>Total Precio</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <div class="col-sm-5 col-xs-12">

                        <div class="info-box bg-green">
                            <span class="info-box-icon"><i class="fa fa-dollar"></i></span>
                            <div class="info-box-content">
                                <span class="info-box-text">Total Tranporte:</span>
                                <span class="info-box-number" id="DDP">$<?php echo $this->scope["viaticsData"]["transport"]["price"];?> COP</span>
                            </div>
                        </div>

                        <div class="info-box bg-blue">
                            <span class="info-box-icon"><i class="fa fa-cubes"></i> </span>
                            <div class="info-box-content">
                                <span class="info-box-text">Cantidad de registros:</span>
                                <span class="info-box-number" id="FCA"><?php echo $this->scope["viaticsData"]["transport"]["quantity"];?></span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="box">
                    <nav class="bg-primary">
                        <p class="text-center">Alimentación</p>
                    </nav>
                    <nav class="navbar navbar-light">
                        <div class="container-fluid">
                            <ul class="nav navbar-nav">
                                <li>
                                    <a node="node-agregar_costo" style="cursor: pointer;" id="impostAddBtn"
                                        name="impostAddBtn" data-toggle="modal" data-target="#impostPopUpAdd"
                                        onclick="ResetControls('containerinputs');SetData();$('#typeViatic').val('food')">
                                        <i class="fa fa-plus-circle"></i> Agregar costo
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </nav>

                    <div class="box-body">
                        <div class="col-sm-7 col-xs-12">
                            <table id="foodTbl" name="impostTbl"
                                class="table table-bordered table-striped dt-responsive tables dataTable no-footer dtr-inline"
                                style="width: 100%;" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>Nombre</th>
                                        <th>Porcentaje</th>
                                        <th>Cantidad</th>
                                        <th>Precio individual</th>
                                        <th>Total Precio</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div class="col-sm-5 col-xs-12">

                            <div class="info-box bg-green">
                                <span class="info-box-icon"><i class="fa fa-dollar"></i></span>
                                <div class="info-box-content">
                                    <span class="info-box-text">Total Alimentación:</span>
                                    <span class="info-box-number" id="DDP">$<?php echo $this->scope["viaticsData"]["food"]["price"];?></span>
                                </div>
                            </div>
    
                            <div class="info-box bg-blue">
                                <span class="info-box-icon"><i class="fa fa-cubes"></i> </span>
                                <div class="info-box-content">
                                    <span class="info-box-text">Cantidad de registros:</span>
                                    <span class="info-box-number" id="FCA"><?php echo $this->scope["viaticsData"]["food"]["quantity"];?></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="box">
                        <nav class="bg-primary">
                            <p class="text-center">Hospedaje</p>
                        </nav>
                        <nav class="navbar navbar-light">
                            <div class="container-fluid">
                                <ul class="nav navbar-nav">
                                    <li>
                                        <a node="node-agregar_costo" style="cursor: pointer;" id="impostAddBtn"
                                            name="impostAddBtn" data-toggle="modal" data-target="#impostPopUpAdd"
                                            onclick="ResetControls('containerinputs');SetData();$('#typeViatic').val('hospedaje')">
                                            <i class="fa fa-plus-circle"></i> Agregar costo
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </nav>

                        <div class="box-body">
                            <div class="col-sm-7 col-xs-12">
                                <table id="hospedajeTbl" name="impostTbl"
                                    class="table table-bordered table-striped dt-responsive tables dataTable no-footer dtr-inline"
                                    style="width: 100%;" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>Nombre</th>
                                            <th>Porcentaje</th>
                                            <th>Cantidad Noches</th>
                                            <th>Precio individual</th>
                                            <th>Total Precio</th>
                                            <th>Acciones</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                            <div class="col-sm-5 col-xs-12">
                                <div class="info-box bg-green">
                                    <span class="info-box-icon"><i class="fa fa-dollar"></i></span>
                                    <div class="info-box-content">
                                        <span class="info-box-text">Total Hospedaje:</span>
                                        <span class="info-box-number" id="DDP"><?php echo $this->scope["viaticsData"]["hospedaje"]["price"];?></span>
                                    </div>
                                </div>
        
                                <div class="info-box bg-blue">
                                    <span class="info-box-icon"><i class="fa fa-cubes"></i> </span>
                                    <div class="info-box-content">
                                        <span class="info-box-text">Cantidad de registros:</span>
                                        <span class="info-box-number" id="FCA"><?php echo $this->scope["viaticsData"]["hospedaje"]["quantity"];?></span>
                                        </span>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="box">
                            <nav class="bg-primary">
                                <p class="text-center">Improvisto</p>
                            </nav>
                            <nav class="navbar navbar-light">
                                <div class="container-fluid">
                                    <ul class="nav navbar-nav">
                                        <li>
                                            <a node="node-agregar_costo" style="cursor: pointer;" id="impostAddBtn"
                                                name="impostAddBtn" data-toggle="modal" data-target="#impostPopUpAdd"
                                                onclick="ResetControls('containerinputs');SetData();$('#typeViatic').val('improvistos')">
                                                <i class="fa fa-plus-circle"></i> Agregar costo
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </nav>

                            <div class="box-body">
                                <div class="col-sm-7 col-xs-12">
                                    <table id="imporvistosTbl" name="impostTbl"
                                        class="table table-bordered table-striped dt-responsive tables dataTable no-footer dtr-inline"
                                        style="width: 100%;" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th>Nombre</th>
                                                <th>Porcentaje</th>
                                                <th>Cantidad</th>
                                                <th>Precio individual</th>
                                                <th>Total Precio</th>
                                                <th>Acciones</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                                <div class="col-sm-5 col-xs-12">

                                    <div class="info-box bg-green">
                                        <span class="info-box-icon"><i class="fa fa-dollar"></i></span>
                                        <div class="info-box-content">
                                            <span class="info-box-text">Total Improvistos:</span>
                                            <span class="info-box-number" id="DDP">$<?php echo $this->scope["viaticsData"]["improvistos"]["price"];?></span>
                                        </div>
                                    </div>
            
                                    <div class="info-box bg-blue">
                                        <span class="info-box-icon"><i class="fa fa-cubes"></i> </span>
                                        <div class="info-box-content">
                                            <span class="info-box-text">Cantidad de registros:</span>
                                            <span class="info-box-number" id="FCA"><?php echo $this->scope["viaticsData"]["improvistos"]["quantity"];?></span>
                                            </span>
                                        </div>
                                    </div>

                                </div>
                            </div>


                        </div>
    </section>
</div>
<!--=====================================
VENTANA MODAL AGREGAR IMPUESTO O MODIFICAR
======================================-->
<div id="impostPopUpAdd" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form id="modifyPricesForm" role="form" method="post" >
                <!--=====================================
              Hidden
              ======================================-->
                <input id="optionInputForm" name="optionInputForm" type="hidden" value="0">
                <input id="idDataInputForm" name="idDataInputForm" type="hidden" value="0">
                <input id="isActionFromAjax" name="isActionFromAjax" type="hidden" value="false">
                <input type="hidden"  id="typeViatic" name="typeViatic" value="">
                <input type="hidden" id="idViatic" name="idViatic" value="<?php echo $this->scope["idViatic"];?>">
                <!--=====================================
              CABEZA DEL MODAL
              ======================================-->
                <div class="modal-header" style="background:#3c8dbc; color:white">
                    <button type="button" class="close" data-dismiss="modal" data-target='#impostPopUpAdd'
                        onclick="ResetControls('impostForm')">&times;</button>
                    <h4 class="modal-title">Agregar costo</h4>
                </div>
                <!--=====================================
              CUERPO DEL MODAL
              ======================================-->
                <div class="modal-body">
                    <div class="box-body">
                        <div class="row">
                            <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-industry"></i></span>
                                    <?php echo $this->scope["impostNameCtx"];?>
                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-percent"></i></span>
                                    <?php echo $this->scope["impostPercentCtx"];?>
                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-cubes"></i></span>
                                    <?php echo $this->scope["impostQuantityCtx"];?>
                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-dollar"></i></span>
                                    <?php echo $this->scope["impostPriceCtx"];?>
                                </div>
                            </div>
                           
                            <input type="hidden" name="idCompanyCtx" id="idCompany" value="<?php echo $this->scope["idPricesCompany"];?>">
                        </div>
                    </div>
                </div>
                <!--=====================================
              PIE DEL MODAL
              ======================================-->
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal"
                        data-target='#impostPopUpAdd' onclick="ResetControls('modifyPricesForm')">Salir</button>
                    <button type="submit" class="btn btn-primary">Guardar </button>
                </div>
            </form>

        </div>
    </div>
</div>
<div id="containerResponseAjax"></div>
<script src="view/html/modules/listviatics/ListViatics.js?v= <?php echo $this->scope["tempParameter"];?>"></script>
<?php echo $this->scope["jquery"];
 /* end template body */
return $this->buffer . ob_get_clean();
?>