<?php
/* template head */
/* end template head */ ob_start(); /* template body */ ?><div class="content-wrapper">
    <section class="content-header">
        <h1>
            Administrar Codigos para cotizaciones
        </h1>
        <ol class="breadcrumb">
            <li><a href="Home"><i class="fa fa-dashboard"></i> Inicio</a></li>
            <li class="active"> Administrar Codigos para cotizaciones</li>
        </ol>
    </section>
    <section class="content">
        <div class="box">
            <nav class="navbar navbar-light">
                <div class="container-fluid">
                    <ul class="nav navbar-nav">
                        <li>
                            <a node="node-agregar_codigo" style="cursor: pointer;" id="impostAddBtn" name="impostAddBtn" data-toggle="modal" data-target="#productPopUpAdd" onclick="ResetControls('containerinputs');SetData()">
                                <i class="fa fa-plus-circle"></i> Agregar Codígo
                            </a>
                        </li>
                        <li>
                            <a style="cursor: pointer;" id="userAddBtn" name="userAddBtn" onclick="location.href = 'Home' ">
                                <i class="fa fa-home"></i> Menu Principal
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>

            <div class="box-body">
                <div class="row">
                </div>
                <table id="codesTbl" name="serialTbl" class="table table-bordered table-striped dt-responsive tables dataTable no-footer dtr-inline" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>id</th>
                            <th>Codigo</th>
                            <th>creado por</th>
                            <th>fecha</th>
                            <th>acciones</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </section>
</div>



<!--=====================================
VENTANA MODAL AGREGAR PRODUCTO O MODIFICAR
======================================-->
<div id="productPopUpAdd" class="modal fade " role="dialog">
    <div class="modal-dialog modal-xs">
        <div class="modal-content">
            <form id="inventoryForm" role="form" method="post" enctype="multipart/form-data">
                <!--=====================================
            Hidden
            ======================================-->
                <input id="optionInputForm" name="optionInputForm" type="hidden" value="1">
                <input id="idDataInputForm" name="idDataInputForm" type="hidden" value="0">
                <input id="isActionFromAjax" name="isActionFromAjax" type="hidden" value="false">
                <!--=====================================
            CABEZA DEL MODAL
            ======================================-->
                <div class="modal-header" style="background:#3c8dbc; color:white">
                    <button type="button" class="close" data-dismiss="modal" data-target='#productPopUpAdd' onclick="ResetControls('productForm')">&times;</button>
                    <h4 class="modal-title">Agregar Codigo</h4>
                </div>
                <!--=====================================
            CUERPO DEL MODAL
            ======================================-->
                <div class="modal-body" id="containerinputs">
                    <div class="alert bg-yellow"><strong>Nota !</strong> El siguiente codigo sera utilizado en el momento de generar una cotización con productos menores al 20%, para habilitar la previzualización y modificación de una cotización</div>
                    <div class="box-body">
                        <div class="row">
                            <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-codepen"></i></span> <?php echo $this->scope["codeCdx"];?>

                                </div>
                            </div>
                        </div>
                    </div>
                    <!--=====================================
            PIE DEL MODAL
            ======================================-->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal" data-target='#productPopUpAdd' onclick="ResetControls('product2Form')">Salir</button>
                        <button type="submit" class="btn btn-primary">Guardar Codigo</button>
                    </div>

            </form>

            </div>
        </div>
    </div>
</div>


<div id="containerResponseAjax"></div>
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<script src="view/html/modules/code/Code.js?v= <?php echo $this->scope["tempParameter"];?>"></script>
<?php echo $this->scope["jquery"];
 /* end template body */
return $this->buffer . ob_get_clean();
?>