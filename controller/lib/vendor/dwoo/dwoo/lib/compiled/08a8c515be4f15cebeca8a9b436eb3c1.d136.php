<?php
/* template head */
/* end template head */ ob_start(); /* template body */ ?><div class="content-wrapper">
    <section class="content-header">
        <h1 class="box-title">Administrar Contactos</h1>
        <ol class="breadcrumb">
            <li><a href="Home"><i class="fa fa-dashboard"></i> Inicio</a></li>
            <li class="active">Administrar usuarios</li>
        </ol>
    </section>
    <section class="content">
        <?php echo $this->scope["idCompayInputForm"];?>

        <div class="box">
            <div class="box-header with-border">
                <h3>Contactos de la Compañia: <b style="text-transform: uppercase;">(<?php echo $this->scope["nameCompany"];?>)</b></h3>
            </div>
            <div class="box-header with-border">

                <nav class="navbar navbar-light">
                    <div class="container-fluid">
                        <ul class="nav navbar-nav">
                            <li>
                                <a node="node-agregar_contacto" style="cursor: pointer;" id="userAddBtn" name="userAddBtn" data-toggle="modal" data-target="#personPopUpAdd" onclick="SetData()">
                                    <i class="fa fa-plus-circle"></i> Agregar contácto
                                </a>
                            </li>
                            <li>
                                <a node="node-generar_excel" style="cursor: pointer;" id="userAddBtn" name="userAddBtn" onclick="CreateExcelDocs() ">
                                    <i class="fa fa-file"></i> Generar Excel
                                </a>
                            </li>
                            <li>
                                <a node="node-agregar_excel" style="cursor: pointer;" id="contactosAddFromExcelBtn" name="contactosAddFromExcelBtn" data-toggle="modal" data-target="#modalAddTemplateExcel">
                                    <i class="fa fa-upload"></i> Agregar Contactos desde plantilla de excel
                                </a>
                            </li>
                            <li>
                                <a style="cursor: pointer;" id="userAddBtn" name="userAddBtn" onclick="location.href = 'Company' ">
                                    <i class="fa fa-arrow-left"></i> Regresar
                                </a>
                            </li>

                        </ul>
                    </div>
                </nav>

            </div>
            <div class="box-body">
                <input type="hidden" id="idRole" value="<?php echo $this->scope["idRole"];?>">
                <table id="userTbl" name="userTbl" class="table table-bordered table-striped dt-responsive tables dataTable no-footer dtr-inline" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Nombres</th>
                            <th>Apellidos</th>
                            <th>Prefijo</th>

                            <th>Celular</th>
                            <th>Telefono</th>
                            <th>Email</th>
                            <th>Dirección</th>
                            <th>Ciudad</th>
                            <th>Fecha de Creación</th>
                            <th>Role</th>
                            <th>Empresa</th>
                            <th>Vendedor</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </section>
</div>
<!--=====================================
VENTANA MODAL AGREGAR PERSONA O MODIFICAR
======================================-->
<div id="personPopUpAdd" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form id="personForm" role="form" method="post" enctype="multipart/form-data">
                <!--=====================================
              Hidden
              ======================================-->
                <input id="optionInputForm" name="optionInputForm" type="hidden" value="0">
                <input id="idDataInputForm" name="idDataInputForm" type="hidden" value="0">
                <input id="isActionFromAjax" name="isActionFromAjax" type="hidden" value="false">
                <!--=====================================
              CABEZA DEL MODAL
              ======================================-->
                <div class="modal-header" style="background:#3c8dbc; color:white">
                    <button type="button" class="close" data-dismiss="modal" data-target='#personPopUpAdd' onclick="ResetControls('personForm')">&times;</button>
                    <h4 class="modal-title">Agregar usuario</h4>
                </div>
                <!--=====================================
              CUERPO DEL MODAL
              ======================================-->
                <div class="modal-body">
                    <div class="box-body">
                        <div class="row">
                            <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-user"></i></span> <?php echo $this->scope["personNamesCtx"];?>

                                    <!-- <input id="userNamesCtx" name="userNamesCtx" type="text" class="form-control input-lg" placeholder="Ingresar nombres" required> -->
                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-user-o"></i></span> <?php echo $this->scope["personLastNamesCtx"];?>

                                    <!-- <input id="userLastNamesCtx" name="userLastNamesCtx" type="text" class="form-control input-lg"  placeholder="Ingresar apellidos" required> -->
                                </div>
                            </div>
                            <!--
                            <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-id-card"></i></span> <?php echo $this->scope["personDocumentTypeLst"];?>

                           
                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-id-card-o"></i></span> <?php echo $this->scope["personDocumentCtx"];?>

                             
                                </div>
                            </div>
                        -->
                            <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-mobile"></i></span> <?php echo $this->scope["personCellPhoneCtx"];?>

                                    <!-- <input id="userCellNumberCtx" name="userCellNumberCtx" type="text" class="form-control input-lg" placeholder="Ingresar numero celular" required> -->
                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-phone"></i></span> <?php echo $this->scope["personTelephoneCtx"];?>

                                    <!-- <input id="userTelePhoneNumberCtx" name="userTelePhoneNumberCtx" type="text" class="form-control input-lg" name="nuevoTelefono" placeholder="Ingresar Telefono"> -->
                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-envelope-o"></i></span> <?php echo $this->scope["personEmailCtx"];?>

                                    <!-- <input id="userEmailCtx" name="userEmailCtx" type="text" class="form-control input-lg" name="nuevoEmail" placeholder="Ingresar Email" required> -->
                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-map-marker"></i></span> <?php echo $this->scope["personAddressCtx"];?>

                                    <!-- <input id="userEmailCtx" name="userEmailCtx" type="text" class="form-control input-lg" name="nuevoEmail" placeholder="Ingresar Email" required> -->
                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-map-marker"></i></span> <?php echo $this->scope["personCityCtx"];?>

                                    <!-- <input id="userEmailCtx" name="userEmailCtx" type="text" class="form-control input-lg" name="nuevoEmail" placeholder="Ingresar Email" required> -->
                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                    <div><?php echo $this->scope["personIdRoleHidden"];?></div>
                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-list"></i></span>
                                    <div><?php echo $this->scope["personUserLst"];?></div>
                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-cubes"></i></span>
                                    <div><?php echo $this->scope["personPrefijoLst"];?></div>
                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="input-group">
                                    <div><?php echo $this->scope["personIdCompanyHidden"];?></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--=====================================
              PIE DEL MODAL
              ======================================-->
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal" data-target='#personPopUpAdd' onclick="ResetControls('personForm')">Salir</button>
                    <button type="submit" class="btn btn-primary">Guardar usuario</button>
                </div>
            </form>

        </div>
    </div>
</div>


<!--==================================================================
=            MODAL AGREGAR CONATCTOS DESDE PLANTILLA DE EXCEL           =
======================================================================-->
<!-- Modal -->
<div id="modalAddTemplateExcel" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">

            <form id="excelTemplateForm" name="ExcelTemplateForm" role="form" method="post" enctype="multipart/form-data">
                <input id="optionInputTemplateForm" name="optionInputTemplateForm" type="hidden" value="0">
                <div class="modal-header" style="background: #3c8dbc; color: white">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Agregar Contactos</h4>
                </div>
                <div class="modal-body">
                    <?php echo $this->scope["idCompayInputForm"];?>

                    <input type="hidden" name="MAX_FILE_SIZE" value="2000000" />
                    <div class="form-group">
                        <label>Inserte una plantilla excel para cargar los datos :</label>
                        <input type="file" name="fileExcelTemplateInp" accept=".xlsx" class="file">
                        <div class="input-group col-12">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-folder-open"></i></span>
                            <input type="text" class="form-control input-lg" disabled placeholder="Sin Plantilla Excel">
                            <span class="input-group-btn">
                              <button class="browse btn btn-primary input-lg" type="button"><i class="fa fa-plus"></i></button>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button id="exitExcelTemplateBtn" type="button" class="btn btn-default pull-left" data-dismiss="modal" onclick="ResetControls('ExcelTemplateForm')">Cerrar</button>
                    <button id="sendExcelTemplateBtn" name="sendExcelTemplateBtn" type="button" class="importExcel btn btn-primary">Importar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div id="containerResponseAjax"></div>
<script src="view/html/modules/contacts/Contact.js?v= <?php echo $this->scope["tempParameter"];?>"></script>
<?php echo $this->scope["jquery"];
 /* end template body */
return $this->buffer . ob_get_clean();
?>