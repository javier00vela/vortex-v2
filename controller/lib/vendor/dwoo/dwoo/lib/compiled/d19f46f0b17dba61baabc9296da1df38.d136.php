<?php
/* template head */
/* end template head */ ob_start(); /* template body */ ?><div class="content-wrapper">
    <section class="content-header">
        <h1>
            Administrar Visitas
        </h1>
        <ol class="breadcrumb">
            <li><a href="Home"><i class="fa fa-dashboard"></i> Inicio</a></li>
            <li class="active">Administrar Visitas</li>
        </ol>
    </section>
    <section class="content">
        <div class="box">
            <div class="box-header with-border">

                <nav class="navbar navbar-light">
                    <div class="container-fluid">
                        <ul class="nav navbar-nav">
                            <li>
                                <a node="node-agregar_visita" style="cursor: pointer;" id="userAddBtn" name="userAddBtn" data-toggle="modal" data-target="#personPopUpAdd" onclick="ResetControls('containerinputs');SetData()">
                                    <i class="fa fa-plus-circle"></i> Agregar Visita
                                </a>
                            </li>
                            <li>
                                <a style="cursor: pointer;" id="userAddBtn" name="userAddBtn" onclick="location.href = 'Home' ">
                                    <i class="fa fa-home"></i> Menu Principal
                                </a>
                            </li>
                        </ul>
                    </div>
                </nav>

            </div>
            <div class="box-body">
                <table id="userTbl" name="userTbl" class="table table-bordered table-striped dt-responsive tables dataTable no-footer dtr-inline" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>id</th>
                            <th>Nombres</th>
                            <th>Apellidos</th>
                            <th>Celular</th>
                            <th>Telefono</th>
                            <th>Email</th>
                            <th>Dirección</th>
                            <th>Ciudad</th>
                            <th>Role</th>
                            <th>Empresa</th>
                            <th>Encargado Visita</th>
                            <th>Fecha Creación</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </section>
</div>

<!--=====================================
VENTANA MODAL AGREGAR PERSONA O MODIFICAR
======================================-->
<div id="personPopUpAdd" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form id="VisitsForm" role="form" method="post" enctype="multipart/form-data">
                <!--=====================================
              Hidden
              ======================================-->
                <input id="optionInputForm" name="optionInputForm" type="hidden" value="0">
                <input id="idDataInputForm" name="idDataInputForm" type="hidden" value="0">
                <input id="isActionFromAjax" name="isActionFromAjax" type="hidden" value="false">
                <!--=====================================
              CABEZA DEL MODAL
              ======================================-->
                <div class="modal-header" style="background:#3c8dbc; color:white">
                    <button type="button" class="close" data-dismiss="modal" data-target='#personPopUpAdd' onclick="ResetControls('personForm')">&times;</button>
                    <h4 class="modal-title">Agregar Visita</h4>
                </div>
                <!--=====================================
              CUERPO DEL MODAL
              ======================================-->
                <div class="modal-body" id="containerinputs">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="alert bg-orange">
                                    <p><i class="fa fa-warning">  </i> Recuerda que las visitas son un seguimiento a los clientes y/o proveedores de una empresa(Cliente). </p>
                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-user"></i></span> <?php echo $this->scope["personIdRoleLst"];?>

                                    <!-- <input id="userNamesCtx" name="userNamesCtx" type="text" class="form-control input-lg" placeholder="Ingresar nombres" required> -->
                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-industry"></i></span> <?php echo $this->scope["CompanyLst"];?>

                                    <!-- <input id="userLastNamesCtx" name="userLastNamesCtx" type="text" class="form-control input-lg"  placeholder="Ingresar apellidos" required> -->
                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-users"></i></span> <?php echo $this->scope["ClientsLst"];?>

                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div id="messageModal" class="col-sm-12">

                            </div>
                        </div>
                    </div>
                </div>
                <!--=====================================
              PIE DEL MODAL
              ======================================-->
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal" data-target='#personPopUpAdd' onclick="ResetControls('personForm')">Salir</button>
                    <button type="submit" class="btn btn-primary">Guardar Visita</button>
                </div>
            </form>

        </div>
    </div>
</div>

<div id="containerResponseAjax"></div>
<script src="view/html/modules/visits/Visits.js?v= <?php echo $this->scope["tempParameter"];?>"></script>
<?php echo $this->scope["jquery"];
 /* end template body */
return $this->buffer . ob_get_clean();
?>