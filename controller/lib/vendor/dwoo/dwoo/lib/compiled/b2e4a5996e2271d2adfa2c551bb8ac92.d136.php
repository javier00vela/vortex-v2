<?php
/* template head */
/* end template head */ ob_start(); /* template body */ ?><div class="content-wrapper">
  <section class="content-header">
    <h1>
      Historial de Acceso</b>
    </h1>
    <ol class="breadcrumb">
      <li><a href="Home"><i class="fa fa-dashboard"></i> Inicio</a></li>
      <li class="active">Reportes de Personas</li>
      <li class="active">Historial de Accesso</li>
    </ol>
  </section>
  <section class="content">
    <div class="box">
      
      <div class="container-fluid">
                  <div class="rows text-center">
                      <div class="col-sm-3">
                         <div class="form-group">
                                        <label for="">Roles:</label>
                                        <div id="quotationTimetContainer" name="quotationTimeContainer" class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-history"></i></span>
                                         <?php echo $this->scope["listData"];?>       
                                      </div>
                                </div>
                      </div>
                        <div class="col-sm-3">
                         <div class="form-group">
                                        <label for="">Usuarios:</label>
                                        <div id="quotationTimetContainer" name="quotationTimeContainer" class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-history"></i></span>
                                         <select id="quotationDeliveryLst" name="quotationDeliveryLst" class="form-control" required="" disabled="true">
                                          <option value="" selected="">--Seleccione Usuario--</option>
                                          </select>          
                                      </div>
                                </div>
                      </div>
                        <div class="col-sm-2">
                         <div class="form-group ">
                                        <label for="">Fecha Inicio:</label>
                                        <div name="quotationTimeContainer" class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-history"></i></span>
                                        <input type="date"  id="min" class="form-control" disabled  name="">        
                                      </div>
                                </div>
                      </div>
                        <div class="col-sm-2">
                         <div class="form-group ">
                                        <label for="">Fecha Salida:</label>
                                        <div  name="quotationTimeContainer" class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-history"></i></span>
                                         <input type="date"  id="max" class="form-control" disabled name="">         
                                      </div>
                                </div>
                      </div>
                       <div class="col-sm-2">
                         <div class="form-group ">
                                        <label for="">Opciones:</label><br>
                                        <button class="btn btn-primary"  id="filter"  disabled="true"><i class="fa fa-list"></i> - Filtrar</button>
                                         <button class="btn btn-success" id="excelButton" disabled="true"  onclick=" CreateExcelDocs()"><i class="fa fa-file"></i> - Generar Excel</button>      
                                      </div>
                                </div>
                      </div>
        </div>
      <div class="box-body">
        <table id="accessHistoryTbl" name="accessHistoryTbl" class="table table-bordered table-striped dt-responsive tables dataTable no-footer dtr-inline" style="width: 100%;" cellspacing="0">
            <thead>
                <tr>
                  <th>id</th>
                  <th>Fecha Ingreso</th>
                  <th>Fecha Salida</th>
                  <th>Nro Horas</th>
                  <th>Direccion Ip</th>
                  <th>Dispositivo</th>
                  <th>Navegador</th>
                  <th>Id Usuario</th>
                  <th>Usuario</th>
                  <th>Opciones</th>
                </tr>

            </thead>
        </table>
      </div>
    </div>
  </section>
</div>
<div id="containerResponseAjax"></div>
<script src="view/html/modules/reporthistorialperson/ReportHistorialPerson.js?v= <?php echo $this->scope["tempParameter"];?>"></script>
<script src="https://www.amcharts.com/lib/3/ammap.js"></script>
<script src="https://www.amcharts.com/lib/3/maps/js/worldLow.js"></script>
<script src="https://www.amcharts.com/lib/3/themes/light.js"></script>
<script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
<link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyBSB0rjwAKMTvxmHT9C30-94i3Ii35rwu8"></script>
<?php echo $this->scope["jquery"];?>




<?php  /* end template body */
return $this->buffer . ob_get_clean();
?>