<?php
/* template head */
/* end template head */ ob_start(); /* template body */ ?><div class="content-wrapper">
    <section class="content-header">
        <h1>
            Lista de Diagnosticos de Equipos
        </h1>
        <ol class="breadcrumb">
            <li><a href="Home"><i class="fa fa-dashboard"></i> Inicio</a></li>
            <li class="active"> Lista de Diagnosticos de Equipos</li>
        </ol>
    </section>
    <section class="content">
        <div class="box">

            <nav class="navbar navbar-light">
                <div class="container-fluid">
                    <ul class="nav navbar-nav">
                        <li>
                            <a style="cursor: pointer;" id="userAddBtn" name="userAddBtn" onclick="location.href = 'Home' ">
                                <i class="fa fa-home"></i> Menu Principal
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>

            <div class="box-body">
                <table id="impostTbl" name="impostTbl" class="table table-bordered table-striped dt-responsive tables dataTable no-footer dtr-inline" style="width: 100%;" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Referencia</th>
                            <th>Nombre</th>
                            <th>Marca</th>
                            <th>Tipo De Producto</th>
                            <th>Lugar</th>
                            <th>Proveedor</th>
                            <th>Numero Cotización</th>
                            
                            <th>Fecha de Diagnostico</th>
                            <th>Tipo Mantenimiento</th>
                            <th>Porcentaje </th>
                            <th>Opciones</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </section>
</div>
<!--=====================================
VENTANA MODAL VER CONTACTOS
======================================-->
<div id="ContactPopUpAdd" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form id="impostForm" role="form" method="post" enctype="multipart/form-data">
                <!--=====================================
              Hidden
              ======================================-->
                <input id="optionInputForm" name="optionInputForm" type="hidden" value="0">
                <input id="idDataInputForm" name="idDataInputForm" type="hidden" value="0">
                <input id="isActionFromAjax" name="isActionFromAjax" type="hidden" value="false">
                <!--=====================================
              CABEZA DEL MODAL
              ======================================-->
                <div class="modal-header" style="background:#3c8dbc; color:white">
                    <button type="button" class="close" data-dismiss="modal" data-target='#impostPopUpAdd' onclick="ResetControls('impostForm')">&times;</button>
                    <h4 class="modal-title">Lista De Contactos</h4>
                </div>
                <!--=====================================
              CUERPO DEL MODAL
              ======================================-->
                <div class="modal-body">
                    <div class="box-body">
                        <div class="row" id="ContactTab">

                        </div>
                    </div>
                </div>
                <!--=====================================
              PIE DEL MODAL
              ======================================-->
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal" data-target='#ContactPopUpAdd' onclick="ResetControls('impostForm')">Salir</button>
                </div>
            </form>

        </div>
    </div>
</div>
</div>


<div id="containerResponseAjax"></div>
<script src="view/html/modules/listdiagnosis/ListDiagnosis.js?v= <?php echo $this->scope["tempParameter"];?>"></script>
<?php echo $this->scope["jquery"];
 /* end template body */
return $this->buffer . ob_get_clean();
?>