<?php
/* template head */
/* end template head */ ob_start(); /* template body */ ?><div class="content-wrapper">
    <section class="content-header">
        <h1>

            Cotizaciones de Mantenimiento generadas
        </h1>
        <ol class="breadcrumb">
            <li><a href="Home"><i class="fa fa-dashboard"></i> Inicio</a></li>
            <li class="active">Cotizaciones  de Mantenimiento generadas</li>
        </ol>
    </section>
    <section class="content">
        <div class="box">

            <nav class="navbar navbar-light">
                <div class="container-fluid">
                    <ul class="nav navbar-nav">
                        <li>
                            <a node="node-agregar_cotizacion" href="ListDiagnosis" style="cursor: pointer;" id="quotationAddBtn" name="impostAddBtn" onclick="SetData()">
                                <i class="fa fa-plus-circle"></i> Lista de Diagnosis
                            </a>
                        </li>
                        <li>
                            <a style="cursor: pointer;" id="userAddBtn" name="userAddBtn" onclick="location.href = 'Home' ">
                                <i class="fa fa-home"></i> Menu Principal
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>

            <div class="box-body">
                <table id="quotationTbl" name="quotationTbl" class="table table-bordered table-striped dt-responsive tables dataTable no-footer dtr-inline text-center" style="width: 100%;" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Código</th>
                            <th>Versión</th>
                            <th>Responsable</th>
                            <th>Fecha de Creación</th>
                            <th>Estado</th>
                            <th>Tipo de cotización</th>
                            <th>Total</th>
                            <th>Moneda</th>
                            <th>Validez de oferta</th>
                            <!-- <th>Actualizar</th>  -->
                            <th>Acciones</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </section>
</div>

<button type="button" data-controls-modal="your_div_id" data-backdrop="static" data-keyboard="false" class="btn btn-info btn-lg hidden" id="btnOpenModalProducts" data-toggle="modal" data-target="#quotationPopUpProducts"></button>

<!--=====================================
      VENTANA MODAL cheked productos cotizados
      ======================================-->
<div id="quotationPopUpProducts" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <!--=====================================
                  Hidden
                  ======================================-->
            <input id="optionInputForm" name="optionInputForm" type="hidden" value="0">
            <input id="idDataInputForm" name="idDataInputForm" type="hidden" value="0">
            <input id="isActionFromAjax" name="isActionFromAjax" type="hidden" value="true">
            <!--=====================================
                  CABEZA DEL MODAL
                  ======================================-->
            <div class="modal-header" style="background:#3c8dbc; color:white">
                <!-- <button type="button" class="close" data-dismiss="modal" data-target='#quotationPopUpAdd' onclick="ResetControls('quotationForm')">&times;</button>-->
                <h4 class="modal-title">Chequear los productos finalmente cotizados de la cotización No <span id="spanIdQuotation"></span></h4>
            </div>
            <!--=====================================
                  CUERPO DEL MODAL
                  ======================================-->
            <div class="modal-body">
                <div class="alert alert-warning">
                    Recuerde que al checkear los productos y guardar los productos aceptados en el siguiente recuadro, no podrá modificar más los productos de esta cotización.
                </div>
                <div class="box-body">
                    <div class="row">
                        <table id="tblProductsQuotation" class="table table-bordered text-center">
                            <thead>
                                <tr class="">
                                    <th class="hidden"></th>
                                    <th>nombre</th>
                                    <th>tipo de producto</th>
                                    <th>marca</th>
                                    <th>presentación</th>
                                    <th>ganancia</th>
                                    <th>moneda (producto/cotización)</th>
                                    <th>total Unitario</th>
                                    <th>cantidad</th>
                                    <th>total</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                        <h4><b> Monto de cotización generada : <span id="spanTotalQuotation"></span></b></h4>
                    </div>
                </div>
            </div>
            <!--=====================================
                  PIE DEL MODAL
                  ======================================-->
            <div class="modal-footer">
                <button class="btn btn-primary" id="btnSaveProducts">Guardar Cambios </button>
                <button id="exitExcelTemplateBtn" type="button" class="btn btn-default pull-left" data-dismiss="modal" onclick="ResetControls('ExcelTemplateForm')">Cerrar</button>
            </div>


        </div>
    </div>
</div>





<!--=====================================
      VENTANA MODAL actualizar fecha
      ======================================-->
<div id="fechaUpdatePopUpProducts" class="modal fade" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <!--=====================================
                  CABEZA DEL MODAL
                  ======================================-->
            <div class="modal-header" style="background:#3c8dbc; color:white">
                <!-- <button type="button" class="close" data-dismiss="modal" data-target='#quotationPopUpAdd' onclick="ResetControls('quotationForm')">&times;</button>-->
                <h4 class="modal-title"> Actualizar Cotización.</span>
                </h4>
            </div>
            <!--=====================================
                  CUERPO DEL MODAL
                  ======================================-->
            <div class="modal-body">
                <div class="box-body">
                    <div class="form-group">
                        <label>Fecha a actualizar</label>
                        <input type="text" id="updatedate" readonly data-provide="datepicker" class="form-control">
                    </div>
                </div>
            </div>
            <!--=====================================
                  PIE DEL MODAL
                  ======================================-->
            <div class="modal-footer">
                <button class="btn btn-primary" id="btnSaveDate">Guardar Cambios </button>
            </div>


        </div>
    </div>
</div>



<!--=====================================
      VENTANA MODAL actualizar personas
      ======================================-->
<div id="PersonsPopUpProducts" class="modal fade" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <!--=====================================
                  CABEZA DEL MODAL
                  ======================================-->
            <div class="modal-header" style="background:#3c8dbc; color:white">
                <!-- <button type="button" class="close" data-dismiss="modal" data-target='#quotationPopUpAdd' onclick="ResetControls('quotationForm')">&times;</button>-->
                <h4 class="modal-title"> Actualizar Personas.</span>
                </h4>
            </div>
            <!--=====================================
                  CUERPO DEL MODAL
                  ======================================-->
            <div class="modal-body">
                <div class="box-body">
                    <div class="container-fluid">
                        <div class="form-group">
                            <label for="">Lista de Empresas</label> <?php echo $this->scope["quotationCompanyLst"];?>
                            <div id="contactsContainer"></div>
                        </div>
                    </div>
                </div>
            </div>
            <!--=====================================
                  PIE DEL MODAL
                  ======================================-->
            <div class="modal-footer">
                <button class="btn btn-primary" id="btnSaveContact">Guardar Cambios </button>
            </div>


        </div>
    </div>
</div>





<div id="containerResponseAjax"></div>
<script src="view/html/modules/listquotesmainteince/ListQuotesMainteince.js?v= <?php echo $this->scope["tempParameter"];?>"></script>
<?php echo $this->scope["jquery"];
 /* end template body */
return $this->buffer . ob_get_clean();
?>