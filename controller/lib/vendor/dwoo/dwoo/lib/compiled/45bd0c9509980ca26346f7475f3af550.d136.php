<?php
/* template head */
/* end template head */ ob_start(); /* template body */ ?><div class="content-wrapper">
    <section class="content-header">
        <h1>
            Lista de Ordenes de Compras
        </h1>
        <ol class="breadcrumb">
            <li><a href="Home"><i class="fa fa-dashboard"></i> Inicio</a></li>
            <li class="active">Lista de zOrdenes de Compras</li>
        </ol>
    </section>
    <section class="content">
        <div class="box">
            <nav class="navbar navbar-light">
                <div class="container-fluid">
                    <ul class="nav navbar-nav">
                        <li>
                            <a style="cursor: pointer;" id="userAddBtn" name="userAddBtn" onclick="location.href = 'Home' ">
                                <i class="fa fa-home"></i> Menu Principal
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>

            <div class="box-body">
                <table id="QuotesTbl" name="QuotesTbl" class="table table-bordered table-striped dt-responsive tables dataTable no-footer dtr-inline" style="width: 100%;" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Código Cotización</th>
                            <th>Fecha De Generación</th>
                            <th>Tipo de Cotización</th>
                            <th>Versión Cotización</th>
                            <th>Cantidad Productos</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </section>
</div>
<!--=====================================
VENTANA MODAL AGREGAR IMPUESTO O MODIFICAR
======================================-->
<div id="impostPopUpAdd" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form id="impostForm" role="form" method="post" enctype="multipart/form-data">
                <!--=====================================
              Hidden
              ======================================-->
                <input id="optionInputForm" name="optionInputForm" type="hidden" value="0">
                <input id="idDataInputForm" name="idDataInputForm" type="hidden" value="0">
                <input id="isActionFromAjax" name="isActionFromAjax" type="hidden" value="false">
                <!--=====================================
              CABEZA DEL MODAL
              ======================================-->
                <div class="modal-header" style="background:#3c8dbc; color:white">
                    <button type="button" class="close" data-dismiss="modal" data-target='#impostPopUpAdd' onclick="ResetControls('impostForm')">&times;</button>
                    <h4 class="modal-title">Agregar impuesto</h4>
                </div>
                <!--=====================================
              CUERPO DEL MODAL
              ======================================-->
                <div class="modal-body">
                    <div class="box-body">
                        <div class="row">
                            <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-industry"></i></span> <?php echo $this->scope["impostNameCtx"];?>

                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-percent"></i></span> <?php echo $this->scope["impostPercentCtx"];?>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--=====================================
              PIE DEL MODAL
              ======================================-->
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal" data-target='#impostPopUpAdd' onclick="ResetControls('impostForm')">Salir</button>
                    <button type="submit" class="btn btn-primary">Guardar </button>
                </div>
            </form>

        </div>
    </div>
</div>
<div id="containerResponseAjax"></div>
<script src="view/html/modules/buylistquotes/BuyListQuotes.js?v= <?php echo $this->scope["tempParameter"];?>"></script>
<?php echo $this->scope["jquery"];
 /* end template body */
return $this->buffer . ob_get_clean();
?>