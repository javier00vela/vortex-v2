<?php
/* template head */
/* end template head */ ob_start(); /* template body */ ?><div class="content-wrapper">
    <section class="content-header">
        <h1>
            Administrar TRM
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="Home"><i class="fa fa-dashboard"></i> Inicio</a>
            </li>
            <li class="active">Administrar TRM</li>
        </ol>
    </section>
    <section class="content">
        <div class="box">
            <nav class="navbar navbar-light">
                <div class="container-fluid">
                    <ul class="nav navbar-nav">
                        <li class="hidden">
                            <a style="cursor: pointer;" id="impostAddBtn" name="impostAddBtn" data-toggle="modal" data-target="#TRMPopUpAdd" onclick="ResetControls('containerinputs');SetData()">
                                <i class="fa fa-plus-circle"></i> Agregar TRM
                            </a>
                        </li>
                        <li>
                            <a style="cursor: pointer;" id="userAddBtn" name="userAddBtn" onclick="location.href = 'Home' ">
                                <i class="fa fa-home"></i> Menu Principal
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>

            <div class="box-body" style="height:800px;">
                <div class="alert bg-yellow"><strong>Nota !</strong>Recuerde que la tasa de la TRM tiene plazo segun el tipo de moneda y fecha seleccionada en la lista TRM que se encuentra en la parte inferior izquierda.Cuando Finalize la fecha regida en la TRM se enviara un correo
                    a los coordinadores generales que tengan permisos de correos(<a href="Mails">Modulo Correos</a>) para modificar la TRM actual </div>
                <div class="alert alert-warning alert-dismissible m-3 hidden" role="alert">
                    <strong>Recuerda!</strong> recuerda que los costos/impuestos gestionan gastos extras en los productos.
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span style="color:black" aria-hidden="true">&times;</span>
          </button>
                </div>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-7" style="border: 2px solid #337ab7;padding:5px;">
                            <table id="impostTbl" name="impostTbl" class="table table-bordered table-striped dt-responsive tables dataTable no-footer dtr-inline" style="width: 100%;" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>id</th>
                                        <th>Tipo Moneda</th>
                                        <th>TRM</th>
                                        <th>fecha generada</th>
                                        <th>fecha final</th>
                                        <th>Opciones</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div class="col-sm-5">
                            <div class="box">
                                <div class="box-header bg-primary">

                                </div>
                                <div class="monthly" id="mycalendar" style="width:100%;height:500px;border:#3c8dbc;object-fit: cover;  "></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<!--=====================================
VENTANA MODAL AGREGAR TRM
======================================-->
<div id="TRMPopUpAdd" class="modal fade " role="dialog">
    <div class="modal-dialog modal-xs">
        <div class="modal-content">
            <form id="Form" role="form" method="post" enctype="multipart/form-data">
                <!--=====================================
              Hidden
              ======================================-->
                <input id="optionInputForm" name="optionInputForm" type="hidden" value="4">
                <input id="idDataInputForm" name="idDataInputForm" type="hidden" value="0">
                <input id="isActionFromAjax" name="isActionFromAjax" type="hidden" value="false">
                <!--=====================================
              CABEZA DEL MODAL
              ======================================-->
                <div class="modal-header" style="background:#3c8dbc; color:white">
                    <button type="button" class="close" data-dismiss="modal" data-target='#productPopUpAdd' onclick="ResetControls('productForm')">&times;</button>
                    <h4 class="modal-title">Actualizar TRM</h4>
                </div>
                <!--=====================================
              CUERPO DEL MODAL
              ======================================-->
                <div class="modal-body" id="containerinputs">
                    <div class="alert bg-yellow"><strong>Nota : La TRM se actualizara según el tipo de moneda que desees modificar por tiempo estimado ingresado en el formulario !</strong> </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-codepen"></i></span> <?php echo $this->scope["pricectx"];?>

                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-codepen"></i></span> <?php echo $this->scope["typepriceLs"];?>

                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-codepen"></i></span> <?php echo $this->scope["date"];?>

                                </div>
                            </div>
                        </div>
                    </div>
                    <!--=====================================
              PIE DEL MODAL
              ======================================-->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal" data-target='#productPopUpAdd' onclick="ResetControls('product2Form')">Salir</button>
                        <button type="submit" class="btn btn-primary">Guardar</button>
                    </div>

            </form>

            </div>
        </div>
    </div>
    <!--=====================================
VENTANA MODAL AGREGAR IMPUESTO O MODIFICAR
======================================-->
    <div id="impostPopUpAdd" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <form id="impostForm" role="form" method="post" enctype="multipart/form-data">
                    <!--=====================================
              Hidden
              ======================================-->
                    <input id="optionInputForm" name="optionInputForm" type="hidden" value="0" />
                    <input id="idDataInputForm" name="idDataInputForm" type="hidden" value="0" />
                    <input id="isActionFromAjax" name="isActionFromAjax" type="hidden" value="false" />


            </div>



            <!--=====================================
              CABEZA DEL MODAL
              ======================================-->
            <div class="modal-header" style="background:#3c8dbc; color:white">
                <button type="button" class="close" data-dismiss="modal" data-target="#impostPopUpAdd" onclick="ResetControls('impostForm')">
            &times;
          </button>
                <h4 class="modal-title">Agregar impuesto</h4>
            </div>
            <!--=====================================
              CUERPO DEL MODAL
              ======================================-->
            <div class="modal-body">
                <div class="box-body">
                    <div class="row">
                        <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-industry"></i
                  ></span> <?php echo $this->scope["impostNameCtx"];?>

                            </div>
                        </div>
                        <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-percent"></i
                  ></span> <?php echo $this->scope["impostPercentCtx"];?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--=====================================
              PIE DEL MODAL
              ======================================-->
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal" data-target="#impostPopUpAdd" onclick="ResetControls('impostForm')">
            Salir
          </button>
                <button type="submit" class="btn btn-primary">Guardar</button>
            </div>
            </form>
        </div>
    </div>
</div>
<div id="containerResponseAjax"></div>
<link rel="stylesheet" href="view/bower_components/Monthly-master/css/monthly.css">
<script src="view/html/modules/calendartrm/CalendarTRM.js?v= <?php echo $this->scope["tempParameter"];?>"></script>
<script type="text/javascript" src="view/bower_components/Monthly-master/js/monthly.js"></script>
<?php echo $this->scope["jquery"];
 /* end template body */
return $this->buffer . ob_get_clean();
?>