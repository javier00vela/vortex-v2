<?php
/* template head */
/* end template head */ ob_start(); /* template body */ ?><div class="content-wrapper">
  <section class="content-header">
    <h1>
     Permisos de Productos Cotización #<?php echo $this->scope["idContent"];?>

    </h1>
    <ol class="breadcrumb">
      <li><a href="Home"><i class="fa fa-dashboard"></i> Inicio</a></li>
      <li class="active"> Permisos de Productos Cotización</li>
    </ol>
  </section>
  <section class="content">
    <div class="box">
        <nav class="navbar navbar-light">
            <div class="container-fluid">
                <ul class="nav navbar-nav">
                      <li>
                          <a style="cursor: pointer;" id="userAddBtn" name="userAddBtn" onclick="location.href = 'Home' ">
                              <i class="fa fa-home"></i>
                              Menu Principal
                          </a>
                      </li>
                </ul>
            </div>
        </nav>
        <input type="hidden" id="idUserCreate" value="<?php echo $this->scope["idUserCreate"];?>">
      <input type="hidden" id="idContent" value="<?php echo $this->scope["idContent"];?>">
      <div class="box-body">
          <div class="alert alert-warning alert-dismissible m-3" role="alert">
            <strong>Información cotización</strong><br>
           la cotización con codigo #<?php echo $this->readVarInto(array (  1 =>   array (    0 => '->',  ),  2 =>   array (    0 => 'id',  ),  3 =>   array (    0 => '',    1 => '',    2 => '',  ),), $this->scope["quotation"]["0"], false);?> generada <?php echo $this->readVarInto(array (  1 =>   array (    0 => '->',  ),  2 =>   array (    0 => 'generationDate',  ),  3 =>   array (    0 => '',    1 => '',    2 => '',  ),), $this->scope["quotation"]["0"], false);?> con el valor general de $<?php echo number_format($this->readVarInto(array (  1 =>   array (    0 => '->',  ),  2 =>   array (    0 => 'total',  ),  3 =>   array (    0 => '',    1 => '',    2 => '',  ),), (isset($this->scope["quotation"]["0"]) ? $this->scope["quotation"]["0"]:null), true));?> <?php echo $this->readVarInto(array (  1 =>   array (    0 => '->',  ),  2 =>   array (    0 => 'typeMoney',  ),  3 =>   array (    0 => '',    1 => '',    2 => '',  ),), $this->scope["quotation"]["0"], false);?> con su versión numero # <?php echo $this->readVarInto(array (  1 =>   array (    0 => '->',  ),  2 =>   array (    0 => 'version',  ),  3 =>   array (    0 => '',    1 => '',    2 => '',  ),), $this->scope["quotation"]["0"], false);?> para la empresa '<?php echo $this->scope["company"];?>'.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span style="color:black" aria-hidden="true">&times;</span>
            </button>
          </div>
        <table id="impostTbl" name="impostTbl" class="table table-bordered table-striped dt-responsive tables dataTable no-footer dtr-inline" style="width: 100%;" cellspacing="0">
            <thead>
                <tr>
                  <th>Codigo </th>
                  <th>tipo </th>
                  <th>Nombre</th>
                  <th>Marca</th>
                  <th>Refererencia </th>
                  <th>Presentación </th>
                  <th>Producto sin porciento Producto</th>
                  <th>Cantidad</th>  
                  <th>Producto con porciento Producto</th>
                  <th>Empresa</th>
                  <th>Tiempo Entrega</th>  
                  <th>Porcentaje</th>
                  <th>Habilitar</th>  
                </tr>
            </thead>
        </table>
  
        <div class="col-sm-12">
          <button class="btn btn-primary btn-block" onclick="AbleQuote(<?php echo $this->scope["idContent"];?>)">Habilitar Cotización</button>
        </div>
      </div>
    </div>
  </section>
</div>


<div class="modal" id="price" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header bg-primary">
        <h5 class="modal-title">Asignar Ganancia</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <input type="hidden" class="form-control" id="idData" >
          <input type="hidden" class="form-control" id="percentClassic" >
          <input type="hidden" class="form-control" id="priceDataSend" >
        <div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <label>Precio con porcentaje Actual</label>
          <div class="input-group" id="setDataPercent">
             
              <span id="iconPercent" class="input-group-addon"><i class="fa fa-codepen"></i></span>
              <input type="text" class="form-control" disabled id="priceData" >
          </div>
      </div>
      <div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
          <label>porcentaje Actual</label>
          <div  class="input-group">
            
              <span  class="input-group-addon"><i class="fa fa-codepen"></i></span>
              <input type="text" class="form-control" id="percentData" >
          </div>
      </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" onClick="UpdatePercent();">Actualizar</button>
        <button type="button" class="btn btn-secondary" onClick="ClickTriggerAction();" data-dismiss="modal">Cancelar</button>
      </div>
    </div>
  </div>
</div>




<div id="containerResponseAjax"></div>

<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<script src="view/html/modules/configdata/ConfigData.js?v= <?php echo $this->scope["tempParameter"];?>"></script>

<?php echo $this->scope["jquery"];?>










<?php  /* end template body */
return $this->buffer . ob_get_clean();
?>