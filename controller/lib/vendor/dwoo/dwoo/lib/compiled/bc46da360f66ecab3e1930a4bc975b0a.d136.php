<?php
/* template head */
/* end template head */ ob_start(); /* template body */ ?><div class="content-wrapper">
    <section class="content-header">
        <h1>
            Seguimiento Cotización

        </h1>
        <ol class="breadcrumb">
            <li><a href="Home"><i class="fa fa-dashboard"></i> Inicio</a></li>
            <li class="active">Seguimiento de cotización</li>
        </ol>
    </section>
    <section class="content">
        <div class="box ">
            <nav class="navbar navbar-light box">
                <div class="container-fluid">
                    <ul class="nav navbar-nav">
                        <li>
                            <a node="node-agregar_seguimiento" style="cursor: pointer;" id="tracingAddBtn" name="tracingAddBtn" data-toggle="modal" data-target="#ModalCreateSeguimiento" onclick="ApplyEditor('descriptionAtx',''); //ResetControls('containerInputs');">
                                <i class="fa fa-plus-circle"></i> Agregar Seguimiento
                            </a>
                        </li>
                        <li>
                            <a style="cursor: pointer;" id="userAddBtn" name="userAddBtn" onclick="location.href = 'Home' ">
                                <i class="fa fa-home"></i> Menu Principal
                            </a>
                        </li>
                        <li>
                            <a style="cursor: pointer;" id="userAddBtn" name="userAddBtn" onclick="location.href = 'GenerateQuotes' ">
                                <i class="fa fa-arrow-left"></i> Regresar
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>


            <div class="box-header with-border">
                <h3 class="box-title">Seguimiento de la Cotización N° <?php echo $this->scope["idQuotationInputForm"];?></h3>
                <!-- /.box-tools -->
            </div>
            <div class="box-body">
                <?php echo $this->scope["codeQuotationInputForm"];?>

                <table id="quotationTbl" name="quotationTbl" class="table table-bordered table-striped dt-responsive tables dataTable no-footer dtr-inline" style="width: 100%;" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Usuario</th>
                            <th>Asunto</th>
                            <th>Fecha</th>
                            <th>Oportunidad</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </section>
</div>



<div id="ModalCreateSeguimiento" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <form id="tracingForm" role="form" method="post" enctype="multipart/form-data">

                <!--=====================================
              Hidden
              ======================================-->
                <input id="optionInputForm" name="optionInputForm" type="hidden" value="1">
                <input id="idDataInputForm" name="idDataInputForm" type="hidden" value="0">
                <input id="isActionFromAjax" name="isActionFromAjax" type="hidden" value="false">

                <input id="idQuotation" name="idQuotation" type="hidden">

                <div class="modal-header" style="background:#3c8dbc; color:white">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Registro de Seguimiento</h4>
                </div>

                <div class="modal-body" id="containerInputs">

                    <div class="row">
                        <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <label>Asunto:</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-codepen"></i></span> <?php echo $this->scope["subjectCtx"];?>

                            </div>
                        </div>
                        <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <label>Responsable:</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-address-book"></i></span> <?php echo $this->scope["personResponsableLst"];?>

                            </div>
                        </div>

                        <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <label>Fecha:</label>
                            <div class="input-group">

                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span> <?php echo $this->scope["dateCtx"];?>

                            </div>
                        </div>

                        <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <label>Predicción Actual:</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-cube"></i></span> <?php echo $this->scope["prediction"];?>

                            </div>
                        </div>



                    </div>
                    <div class="form-group">
                        <div class="input-group  col-xs-12">
                            <textarea class="ckeditor" id="descriptionAtx" name="descriptionAtx"></textarea>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button class="btn btn-primary" type="submit">Actualizar</button>
                </div>
        </div>

        </form>
    </div>
</div>
</div>
<div id="containerResponseAjax"></div>
<script src="view/html/modules/tracingquotation/TracingQuotation.js?v= <?php echo $this->scope["tempParameter"];?>"></script>
<?php echo $this->scope["jquery"];
 /* end template body */
return $this->buffer . ob_get_clean();
?>