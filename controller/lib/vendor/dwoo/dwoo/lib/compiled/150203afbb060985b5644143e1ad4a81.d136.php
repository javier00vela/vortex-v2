<?php
/* template head */
/* end template head */ ob_start(); /* template body */ ?><div class="content-wrapper">
  <section class="content-header">

      <h1>
          Administrar Precios del Inventario
      </h1>
    <ol class="breadcrumb">
      <li><a href="Home"><i class="fa fa-dashboard"></i> Inicio</a></li>
      <li class="active">Inventario</li>
    </ol>
  </section>

  <section class="content">
     <?php echo $this->scope["nameCompayInputForm"];?>

      <?php echo $this->scope["idCompayInputForm"];?>

    <div class="box">
      <div class="box-header with-border">
          <nav class="navbar navbar-light">
              <div class="container-fluid">
                  <ul class="nav navbar-nav">
                          <li>
                          <a style="cursor: pointer;" id="userAddBtn" name="userAddBtn" onclick="history.back();">
                              <i class="fa fa-arrow-left"></i>
                              Regresar
                          </a>
                      </li>
                     
                      <li>
                          <a style="cursor: pointer;" id="userAddBtn" name="userAddBtn" onclick="location.href = 'Home' ">
                              <i class="fa fa-home"></i>
                              Menu Principal
                          </a>
                      </li>

                        <li>
                          <a style="cursor: pointer;" class="hidden" id="userAddBtn" name="userAddBtn" onclick="CreateExcelDocs()">
                              <i class="fa fa-file"></i>
                              Generar Excel
                          </a>
                      </li>

                  </ul>
              </div>
          </nav>
          <div class="row">
                <div class=""></div>
                  <div class="col-md-12">
                      <div class="a alert-success-2">
                          <strong>Nota!</strong>
                          <p> 
                              <ul>
                                <li>  la columna "precio Total"  representa el precio unitario + los costos adicionales del producto.</li>
                                <li>  la columna "precio Venta"  representa el precio unitario + los costos adicionales + la ganancia del producto.</li>
                              </ul>
                          </p>
                      </div>
                  </div>
              </div>
              <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-codepen"></i></span>
                        <?php echo $this->scope["typeQuotationLst"];?>

                    </div>
            </div>
        </div>
       
      
      <div class="box-body">
           
        <table id="productTbl" name="productTbl" class="table table-bordered table-striped dt-responsive tables dataTable no-footer dtr-inline" width="100%" cellspacing="0">
            <thead>
                <tr>
                  <th>Id</th>
                  
                  <th>Nombre</th>
                  
                    <th>Referencia</th>
                    <th>Presentación</th>
                    <th>Tipo de Moneda</th>
                    <th>Precio Unitario Normal</th>
                    <th>Precio Total </th>
                    <th>Precio Total <b>(COP)</b></th>
                    <th>Precio Venta <b>(COP)</b></th>
                    <th>Precio Venta</th>
                </tr>
            </thead>
        </table>
      </div>
    </div>
  </section>
</div>
<!--==================================================================
=            MODAL AGREGAR PRODUCTO DESDE PLANTILLA DE EXCEL           =
======================================================================-->
<!-- Modal -->
<div id="modalAddTemplateExcel" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">

            <form id="excelTemplateForm" name="ExcelTemplateForm" role="form" method="post" enctype="multipart/form-data">
                <input id="optionInputTemplateForm" name="optionInputTemplateForm" type="hidden" value="0">
                <div class="modal-header" style="background: #3c8dbc; color: white">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Agregar Productos</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="MAX_FILE_SIZE" value="2000000" />
                    <table width="600">
                        <tr>
                            <th>Names file:</th>
                            <th><input id="fileExcelTemplateInp" name="fileExcelTemplateInp" type="file" class="file" accept=".xlsx"/></th>
                        </tr>
                    </table>
                </div>
                <div class="modal-footer">
                    <button id="exitExcelTemplateBtn" type="button" class="btn btn-default pull-left" data-dismiss="modal" onclick="ResetControls('ExcelTemplateForm')">Exit</button>
                    <button id="sendExcelTemplateBtn" name="sendExcelTemplateBtn" type="button" class="importExcel btn btn-primary">Importar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!--/*=====  End of Section comment block  ======*/-->

<div id="containerResponseAjax"></div>
<script src="view/html/modules/inventoryprice/InventoryPrice.js?v= <?php echo $this->scope["tempParameter"];?>"></script>
<?php echo $this->scope["jquery"];
 /* end template body */
return $this->buffer . ob_get_clean();
?>