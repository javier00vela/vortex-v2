<?php
/* template head */
/* end template head */ ob_start(); /* template body */ ?><div id="back"></div>
<div class="login-box">
    <div class="login-logo">
        <img src="view/img/template/logo-blanco-bloque.png" class="img-responsive" style="padding:30px 100px 0px 100px">
    </div>
    <div class="login-box-body">
        <p class="login-box-msg">Ingresar al sistema</p>
        <form id="loginForm" role="form" method="post" enctype="multipart/form-data">
            <div class="form-group has-feedback">
                <input id="userNicknameCtx" name="userNicknameCtx"  type="text" class="form-control" placeholder="Usuario"  required>
                <span class="glyphicon glyphicon-user form-control-feedback"></span>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <input id="userPasswordCtx" name="userPasswordCtx" type="password" class="form-control" placeholder="Contraseña"  required>
                    <span class="input-group-addon" id="password" ><i class="fa fa-eye"></i></span>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4 col-xs-12">
                    <button type="submit" id="generateQuotation"  class="btn btn-primary btn-block btn-flat btn-xs-block">Ingresar</button>
                </div>
            </div>
        </form>
    </div>
</div>
<script type="text/javascript" src="view/html/modules/login/Login.js"></script><?php  /* end template body */
return $this->buffer . ob_get_clean();
?>