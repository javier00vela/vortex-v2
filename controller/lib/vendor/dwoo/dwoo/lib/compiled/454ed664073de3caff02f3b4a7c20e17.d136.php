<?php
/* template head */
/* end template head */ ob_start(); /* template body */ ?><div class="content-wrapper">
    <section class="content-header">
        <h1>
            Administrar Empresa
        </h1>
        <ol class="breadcrumb">
            <li><a href="Home"><i class="fa fa-dashboard"></i> Inicio</a></li>
            <li class="active">Administrar Empresas</li>
        </ol>
    </section>
    <section class="content">
        <div class="box">

            <nav class="navbar navbar-light">
                <div class="container-fluid">
                    <ul class="nav navbar-nav">
                        <li>
                            <a node="node-agregar_empresa" style="cursor: pointer;" id="companyAddBtn" name="companyAddBtn" data-toggle="modal" data-target="#companyPopUpAdd" onclick="SetData()">
                                <i class="fa fa-plus-circle"></i> Agregar Empresa
                            </a>
                        </li>
                        <li>
                            <a node="node-agregar_empresa_excel" style="cursor: pointer;" id="productsAddFromExcelBtn" name="productsAddFromExcelBtn" data-toggle="modal" data-target="#modalAddTemplateExcel">
                                <i class="fa fa-upload"></i> Agregar Empresas desde plantilla de excel
                            </a>
                        </li>
                        <li>
                            <a node="node-generar_excel" style="cursor: pointer;" id="userAddBtn" name="userAddBtn" onclick=" CreateExcelDocs()">
                                <i class="fa fa-file"></i> Generar Excel
                            </a>
                        </li>
                        <li>
                            <a style="cursor: pointer;" id="userAddBtn" name="userAddBtn" onclick="location.href = 'Home' ">
                                <i class="fa fa-home"></i> Menu Principal
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>

            <div class="box-body">
                <table id="companyTbl" name="companyTbl" class="table table-bordered table-striped dt-responsive tables dataTable no-footer dtr-inline" style="width: 100%;" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Nombre</th>
                            <th>Nit</th>
                            <th>Telefono</th>
                            <th>Email</th>
                            <th>Sitio Web</th>
                            <th>Direccion</th>
                            <th>Pais</th>
                            <th>Ciudad</th>
                            <th>Fecha de Creacion</th>
                            <th>Role</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </section>
</div>
<!--=====================================
VENTANA MODAL AGREGAR COMPAÑIA O MODIFICAR
======================================-->
<div id="companyPopUpAdd" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form id="companyForm" role="form" method="post" enctype="multipart/form-data">
                <!--=====================================
              Hidden
              ======================================-->
                <input id="optionInputForm" name="optionInputForm" type="hidden" value="1">
                <input id="idDataInputForm" name="idDataInputForm" type="hidden" value="0">
                <input id="isActionFromAjax" name="isActionFromAjax" type="hidden" value="false">
                <!--=====================================
              CABEZA DEL MODAL
              ======================================-->
                <div class="modal-header" style="background:#3c8dbc; color:white">
                    <button type="button" class="close" data-dismiss="modal" data-target='#companyPopUpAdd' onclick="ResetControls('companyForm')">&times;</button>
                    <h4 class="modal-title">Agregar empresa</h4>
                </div>
                <!--=====================================
              CUERPO DEL MODAL
              ======================================-->
                <div class="modal-body">
                    <div class="box-body">
                        <div class="row">
                            <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-industry"></i></span> <?php echo $this->scope["companyNameCtx"];?>

                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-check"></i></span> <?php echo $this->scope["companyNitCtx"];?>

                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-phone"></i></span> <?php echo $this->scope["companyPhoneCtx"];?>

                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-envelope-o"></i></span> <?php echo $this->scope["companyEmailCtx"];?>

                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-globe"></i></span> <?php echo $this->scope["companyWebSiteCtx"];?>

                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-location-arrow"></i></span> <?php echo $this->scope["companyAddressCtx"];?>

                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-flag-o"></i></span> <?php echo $this->scope["companyCountryLst"];?>

                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-flag"></i></span> <?php echo $this->scope["companyCityLst"];?>

                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-users"></i></span> <?php echo $this->scope["companyIdRoleLst"];?>

                                </div>
                            </div>
                            <?php echo $this->scope["companyNameHiddenCtx"];?>

                        </div>
                    </div>
                </div>
                <!--=====================================
              PIE DEL MODAL
              ======================================-->
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal" data-target='#companyPopUpAdd' onclick="ResetControls('companyForm')">Salir</button>
                    <button type="submit" class="btn btn-primary">Guardar Empresa</button>
                </div>
            </form>

        </div>
    </div>
</div>

<!--==================================================================
=            MODAL AGREGAR PRODUCTO DESDE PLANTILLA DE EXCEL           =
======================================================================-->
<!-- Modal -->
<div id="modalAddTemplateExcel" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">

            <form id="excelTemplateForm" name="ExcelTemplateForm" role="form" method="post" enctype="multipart/form-data">
                <input id="optionInputTemplateForm" name="optionInputTemplateForm" type="hidden" value="0">
                <div class="modal-header" style="background: #3c8dbc; color: white">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Agregar Empresa</h4>
                </div>
                <div class="modal-body">
                    <div class="alert bg-orange">Recuerda agregar un archivo excel(xlsx) con una cantidad entre 1 a 15000 registros con el fin de garantizar un uso optimo y rapido del funcionamiento de la carga de empresas en el sistema.</div>
                    <input type="hidden" name="MAX_FILE_SIZE" value="2000000" />
                    <div class="form-group">
                        <label>Inserte una plantilla excel para cargar los datos :</label>
                        <input type="file" name="fileExcelTemplateInp" accept=".xlsx" class="file">
                        <div class="input-group col-12">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-folder-open"></i></span>
                            <input type="text" class="form-control input-lg" disabled placeholder="Sin Plantilla Excel">
                            <span class="input-group-btn">
                              <button class="browse btn btn-primary input-lg" type="button"><i class="fa fa-plus"></i></button>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button id="exitExcelTemplateBtn" type="button" class="btn btn-default pull-left" data-dismiss="modal" onclick="ResetControls('ExcelTemplateForm')">Cerrar</button>
                    <button id="sendExcelTemplateBtn" name="sendExcelTemplateBtn" type="button" class="importExcel btn btn-primary">Importar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div id="containerResponseAjax"></div>
<script src="view/html/modules/company/Company.js?v=<?php echo $this->scope["tempParameter"];?>"></script>
<?php echo $this->scope["jquery"];
 /* end template body */
return $this->buffer . ob_get_clean();
?>