<?php
/* template head */
/* end template head */ ob_start(); /* template body */ ?>

<div class="content-wrapper">
  <section class="content-header m-2">
    <h1>
    Configuraciones  
    </h1>
    <ol class="breadcrumb">
      <li><a href="Home"><i class="fa fa-dashboard"></i> Inicio</a></li>
      <li class="active">Configuraciones</li>
    </ol>
  </section>

  <div class="content">
      <div class="box">   
          <div class="box-header with-border text-center">
              <h2 class=" box-title">Opciones Generales</h2>
          </div>

      <div class="box-body">
  <div class="container-fluid" >
    <div class="row">
    <div class="col-lg-6 col-xs-6" style="margin-top: 30px";>
        <div class="small-box bg-green">
          <div class="inner">
            <h2>Gestionar</b></h2>
            <h4>Codigos Descuento</h4>
          </div>
          <div class="icon">
            <i class="fa fa-users"></i>
          </div>
          <a href="Code" class="small-box-footer">Gestionar Codigo <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div>


      <div class="col-lg-6 col-xs-6" style="margin-top: 30px";>
        <div class="small-box bg-red">
          <div class="inner">
            <h2>Actualizar TRM </b></h2>
            <h4>Proveedores</h4>
          </div>
          <div class="icon">
            <i class="fa fa-industry"></i>
          </div>
          <a href="CalendarTRM" class="small-box-footer">Gestionar TRM <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div>

      <div class="col-lg-6 col-xs-6" style="margin-top: 30px";>
        <div class="small-box bg-blue">
          <div class="inner">
            <h2>Gestionar</b></h2>
            <h4>Modulos</h4>
          </div>
          <div class="icon">
            <i class="fa fa-cube"></i>
          </div>
          <a href="Modules" class="small-box-footer">Ir al Modulo <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div>

       <div class="col-lg-6 col-xs-6 " style="margin-top: 30px";>
        <div class="small-box bg-orange">
          <div class="inner">
            <h2>Gestionar</b></h2>
            <h4>Correos/notificaciones</h4>
          </div>
          <div class="icon">
            <i class="  fa fa-at"></i>
          </div>
          <a href="Mails" class="small-box-footer">Ir al Modulo <i class="fa fa-arrow-circle-right"></i></a>
         
        </div>
      </div>
    </div>
  </div>
    </div>

    </div>
</div>


</div>


<!--=====================================
VENTANA MODAL AGREGAR TRM
======================================-->
<div id="TRMPopUpAdd" class="modal fade " role="dialog" >
    <div class="modal-dialog modal-xs">
        <div class="modal-content">
          <form id="Form" role="form" method="post" enctype="multipart/form-data">
              <!--=====================================
              Hidden
              ======================================-->
              <input id="optionInputForm" name="optionInputForm" type="hidden" value="4">
              <input id="idDataInputForm" name="idDataInputForm" type="hidden" value="0">
              <input id="isActionFromAjax" name="isActionFromAjax" type="hidden" value="false">
              <!--=====================================
              CABEZA DEL MODAL
              ======================================-->
              <div class="modal-header" style="background:#3c8dbc; color:white">
                  <button type="button" class="close" data-dismiss="modal" data-target='#productPopUpAdd' onclick="ResetControls('productForm')">&times;</button>
                  <h4 class="modal-title">Actualizar Proveedor</h4>
              </div>
              <!--=====================================
              CUERPO DEL MODAL
              ======================================-->
              <div class="modal-body" id="containerinputs">
                  <div class="alert bg-yellow"><strong>Nota !</strong> </div>
                  <div class="box-body">
                    <div class="row">
                      <div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
                          <div class="input-group">
                              <span class="input-group-addon"><i class="fa fa-codepen"></i></span>
                              <?php echo $this->scope["pricectx"];?>

                          </div>
                      </div>
                        <div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
                          <div class="input-group">
                              <span class="input-group-addon"><i class="fa fa-codepen"></i></span>
                              <?php echo $this->scope["typepriceLs"];?>

                          </div>
                      </div>
                  </div>
              </div>
              <!--=====================================
              PIE DEL MODAL
              ======================================-->
              <div class="modal-footer">
                  <button type="button" class="btn btn-default pull-left" data-dismiss="modal" data-target='#productPopUpAdd' onclick="ResetControls('product2Form')">Salir</button>
                  <button type="submit" class="btn btn-primary">Guardar Codigo</button>
              </div>
              
          </form>

        </div>
    </div>
</div>
</div>




<!--=====================================
VENTANA MODAL AGREGAR TMP O MODIFICAR 
======================================-->
<div id="exchangePopUpShow" class="modal fade " role="dialog" >
    <div class="modal-dialog modal-xs" >
        <div class="modal-content">
          
            <div class="modal-header" style="background:#3c8dbc; color:white">
                <button type="button" class="close" data-dismiss="modal" data-target='#productPopUpAdd' onclick="ResetControls('productForm')">&times;</button>
                <h4 class="modal-title">Administrar Exchange</h4>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <div class="box-body">
                       <table id="exchangeTbl" name="serialTbl" class="table table-bordered table-striped dt-responsive tables dataTable no-footer dtr-inline" width="100%" cellspacing="0">
                          <thead>
                              <tr>
                                <th>id</th>
                                <th>correo</th>
                                <th>clave</th>
                                <th>acciones</th>
                              </tr>
                          </thead>
                      </table>
                </div>     
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">cerrar</button>
            </div>
        </div>
    </div>
</div>





<!--=====================================
VENTANA MODAL AGREGAR PRODUCTO O MODIFICAR
======================================-->
<div id="productPopUpAdd" class="modal fade " role="dialog" >
    <div class="modal-dialog modal-xs">
        <div class="modal-content">
          <form id="inventoryForm" role="form" method="post" enctype="multipart/form-data">
              <!--=====================================
              Hidden
              ======================================-->
              <input id="optionInputForm" name="optionInputForm" type="hidden" value="1">
              <input id="idDataInputForm" name="idDataInputForm" type="hidden" value="0">
              <input id="isActionFromAjax" name="isActionFromAjax" type="hidden" value="false">
              <!--=====================================
              CABEZA DEL MODAL
              ======================================-->
              <div class="modal-header" style="background:#3c8dbc; color:white">
                  <button type="button" class="close" data-dismiss="modal" data-target='#productPopUpAdd' onclick="ResetControls('productForm')">&times;</button>
                  <h4 class="modal-title">Agregar Codigo</h4>
              </div>
              <!--=====================================
              CUERPO DEL MODAL
              ======================================-->
              <div class="modal-body" id="containerinputs">
                  <div class="alert bg-yellow"><strong>Nota !</strong> El siguiente codigo sera utilizado en el momento de generar una cotización, para habilitar el permiso de tener la ganancia de un producto menor al 20%</div>
                  <div class="box-body">
                    <div class="row">
                      <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
                          <div class="input-group">
                              <span class="input-group-addon"><i class="fa fa-codepen"></i></span>
                              <?php echo $this->scope["codeCdx"];?>

                          </div>
                      </div>
                  </div>
              </div>
              <!--=====================================
              PIE DEL MODAL
              ======================================-->
              <div class="modal-footer">
                  <button type="button" class="btn btn-default pull-left" data-dismiss="modal" data-target='#productPopUpAdd' onclick="ResetControls('product2Form')">Salir</button>
                  <button type="submit" class="btn btn-primary">Guardar Codigo</button>
              </div>
              
          </form>

        </div>
    </div>
</div>
</div>

<!--=====================================
VENTANA MODAL AGREGAR PRODUCTO O MODIFICAR
======================================-->
<div id="adminproductPopUpAdd" class="modal fade " role="dialog" >
    <div class="modal-dialog modal-xl" >
        <div class="modal-content">
          <form id="managersForm" role="form" method="post" enctype="multipart/form-data">
              <!--=====================================
              Hidden
              ======================================-->
              <input id="optionInputForm" name="optionInputForm" type="hidden" value="0">
              <input id="idDataInputForm" name="idDataInputForm" type="hidden" value="0">
              <input id="isActionFromAjax" name="isActionFromAjax" type="hidden" value="false">
              <!--=====================================
              CABEZA DEL MODAL
              ======================================-->
              <div class="modal-header" style="background:#3c8dbc; color:white">
                  <button type="button" class="close" data-dismiss="modal" data-target='#productPopUpAdd' onclick="ResetControls('productForm')">&times;</button>
                  <h4 class="modal-title">Administrar Codigos</h4>
              </div>
              <!--=====================================
              CUERPO DEL MODAL
              ======================================-->
              <div class="modal-body" id="containerinputs">
                  <div class="box-body">
                    <div class="row">
                       <table id="codesTbl" name="serialTbl" class="table table-bordered table-striped dt-responsive tables dataTable no-footer dtr-inline" width="100%" cellspacing="0">
                          <thead>
                              <tr>
                                <th>id</th>
                                <th>Codigo</th>
                                <th>creado por</th>
                                <th>fecha</th>
                                <th>acciones</th>
                              </tr>
                          </thead>
                      </table>
                  </div>
              </div>
            </div>
              <!--=====================================
              PIE DEL MODAL
              ======================================-->
              <div class="modal-footer">
                  <button type="button" class="btn btn-default pull-left" data-dismiss="modal" data-target='#productPopUpAdd' onclick="ResetControls('product2Form')">Salir</button>
              </div>
              
          </form>

        </div>
    </div>
  </div>







<div id="containerResponseAjax"></div>

<script src="view/html/modules/managers/Managers.js?v= <?php echo $this->scope["tempParameter"];?>"></script>

<?php echo $this->scope["jquery"];?>










<?php  /* end template body */
return $this->buffer . ob_get_clean();
?>