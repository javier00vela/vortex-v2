<?php
/* template head */
/* end template head */ ob_start(); /* template body */ ?><div class="content-wrapper">
    <section class="content-header">
        <h1>
            Calendario de Actividades
        </h1>
        <ol class="breadcrumb">
            <li><a href="Home"><i class="fa fa-dashboard"></i> Inicio</a></li>
            <li class="active">Calendario de Actividades</li>
        </ol>
    </section>
    <section class="content">
        <input type="hidden" id="idPerson" value="<?php echo $this->scope["PersonId"];?>">
        <div class="box">
            <nav class="navbar navbar-light">
                <div class="container-fluid">
                    <ul class="nav navbar-nav">
                            <li>
                                <a node="node-insertar-evento" style="cursor: pointer;" id="calendarAddBtn" name="productAddBtn" data-toggle="modal" data-target="#calendarPopUpAdd" onclick="ResetControls('containerinputs');SetData()">
                                        <i class="fa fa-plus-circle"></i> Agregar Evento al Calendario
                                    </a>
                                </li>
                           
                            <li>
                                <a href="ModifyEventsStatics" node="node-modificar-evento" style="cursor: pointer;" id="calendarAddBtn" name="productAddBtn">
                                        <i class="fa fa-cog"></i> Modificar Eventos del Calendario
                                    </a>
                                </li>
                            <li>
                            <a style="cursor: pointer;" id="userAddBtn" name="userAddBtn" onclick="location.href = 'Home' ">
                                <i class="fa fa-home"></i> Menu Principal
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>

            <div class="box-body">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="">Filtrar Eventos</label>
                            <select id="eventChange" onChange="ChangeEventCalendar()" class="form-control">
                                <option value="Todos"> -- Mostrar Todos Los Eventos --</option>
                                <option value="Quotes"> -- Mostrar Eventos de Cotización --</option>
                                <option value="Visits"> -- Mostrar Eventos de Visitas --</option>
                                <option value="Prospects"> -- Mostrar Eventos de Prospectos --</option>
                                <option value="Estaticos"> -- Mostrar Otros Eventos --</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6" node="node-filtro_persona_calendario">
                        <div class="form-group">
                            <label for="">Filtrar Persona</label> <?php echo $this->scope["listPerson"];?>

                        </div>
                    </div>
                </div>
                <div id="calendarDiv">
                    <div class="monthly" id="mycalendar1" style="width:100% ; height: 30%; max-height: 30%"></div>
                </div>
            </div>



        </div>
    </section>
</div>

<!--=====================================
VENTANA MODAL AGREGAR PRODUCTO O MODIFICAR
======================================-->
<div node="node-insertar-evento" id="calendarPopUpAdd" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <form id="calendarForm" role="form" method="post" enctype="multipart/form-data">
                    <!--=====================================
                  Hidden
                  ======================================-->
                    <input id="optionInputForm" name="optionInputForm" type="hidden" value="0">
                    <input id="idDataInputForm" name="idDataInputForm" type="hidden" value="0">
                    <input id="isActionFromAjax" name="isActionFromAjax" type="hidden" value="false">
                    <!--=====================================
                  CABEZA DEL MODAL
                  ======================================-->
                    <div class="modal-header" style="background:#3c8dbc; color:white">
                        <button type="button" class="close" data-dismiss="modal" data-target='#productPopUpAdd' onclick="ResetControls('productForm')">&times;</button>
                        <h4 class="modal-title">Agregar Evento Estatico</h4>
                    </div>
                    <!--=====================================
                  CUERPO DEL MODAL
                  ======================================-->
                    <div class="modal-body" id="containerinputs">
                        <div class="box-body">
                            <div class="row">
                                <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-user"></i></span> <?php echo $this->scope["listPerson"];?>

                                    </div>
                                </div>
                                <div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span> <?php echo $this->scope["fechaCtx"];?>

                                    </div>
                                </div>
                                <div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span> <?php echo $this->scope["fechaFinishCtx"];?>

                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                        <div class="form-group">
                                                <div class='input-group date' id='datetimepicker3'>
                                                    <?php echo $this->scope["hourInit"];?>

                                                    <span class="input-group-addon">
                                                        <span class="glyphicon glyphicon-time"></span>
                                                    </span>
                                                </div>    
                                            </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                        <div class="form-group">
                                                <div class='input-group date' id='datetimepicker4'>
                                                    <?php echo $this->scope["hourFinish"];?>

                                                    <span class="input-group-addon">
                                                        <span class="glyphicon glyphicon-time"></span>
                                                    </span>
                                                </div>    
                                            </div>
                                </div>
                            <div class="form-group">
                                <div class="input-group  col-xs-12">
                                    <textarea required class="form-control" id="eventDescription" rows="10" cols="10" name="eventDescription" placeholder="Nombre del evento a agregar."></textarea>
                                </div>
                            </div>
    
                        </div>
                    </div>
                    <!--=====================================
                  PIE DEL MODAL
                  ======================================-->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal" data-target='#productPopUpAdd' onclick="ResetControls('product2Form')">Salir</button>
                        <button type="submit" class="btn btn-primary" name="sendData">Guardar Producto</button>
                    </div>
    
                </form>
    
            </div>
        </div>
    </div>



<div id="containerResponseAjax"></div>

<link rel="stylesheet" href="view/bower_components/Monthly-master/css/monthly.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="view/bower_components/Monthly-master/js/monthly.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css">
<script src="view/html/modules/mycalendar/MyCalendar.js?v= <?php echo $this->scope["tempParameter"];?>"></script>

<?php echo $this->scope["jquery"];
 /* end template body */
return $this->buffer . ob_get_clean();
?>