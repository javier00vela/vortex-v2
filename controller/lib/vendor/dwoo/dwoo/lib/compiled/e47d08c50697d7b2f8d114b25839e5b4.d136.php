<?php
/* template head */
/* end template head */ ob_start(); /* template body */ ?><div class="content-wrapper">
    <section class="content-header">
        <h1>
            Lista de Equipos Vendidos y/o equipos externos
        </h1>
        <ol class="breadcrumb">
            <li><a href="Home"><i class="fa fa-dashboard"></i> Inicio</a></li>
            <li class="active">Lista de Equipos Vendidos y/o equipos externos</li>
        </ol>
    </section>
    <section class="content">
        <div class="box">

            <nav class="navbar navbar-light">
                <div class="container-fluid">
                    <ul class="nav navbar-nav">
                        <li>
                            <a node="node-agregar_equipo" style="cursor: pointer;" id="machineAddBtn"
                                name="machineAddBtn" data-toggle="modal" data-target="#machinePopUpAdd"
                                onclick="ResetControls('containerinputs');SetData()">
                                <i class="fa fa-plus-circle"></i> Agregar Equipo Externo
                            </a>
                        </li>
                        <li>
                            <a style="cursor: pointer;" id="userAddBtn" name="userAddBtn"
                                onclick="location.href = 'Home' ">
                                <i class="fa fa-home"></i> Menu Principal
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>

            <div class="box-body">
                <div class="alert alert-warning">
                    <p>
                        Recuerde que en este modulo se encuentran listados solo los equipos de cada cotización que se
                        encuentra en estado de compra.
                    </p>
                </div>
                <table id="impostTbl" name="impostTbl"
                    class="table table-bordered table-striped dt-responsive tables dataTable no-footer dtr-inline"
                    style="width: 100%;" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Referencia</th>
                            <th>Nombre</th>
                            <th>Marca</th>
                            <th>Tipo De Producto</th>
                            <th>Lugar</th>
                            <th>Numero Factura</th>
                            <th>Proveedor</th>
                            <th>Serial</th>
                            <th>Cliente</th>
                            <th>Lugar Maquina</th>
                            <th>Opciones </th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </section>
</div>

<!--=====================================
VENTANA MODAL AGREGAR EQUIPO
======================================-->
<div id="machinePopUpAdd" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form id="machineForm" role="form" method="post" enctype="multipart/form-data">
                <!--=====================================
                  Hidden
                  ======================================-->
                <input id="optionInputForm" name="optionInputForm" type="hidden" value="0">
                <input id="idDataInputForm" name="idDataInputForm" type="hidden" value="0">
                <input id="isActionFromAjax" name="isActionFromAjax" type="hidden" value="false">
                <!--=====================================
                  CABEZA DEL MODAL
                  ======================================-->
                <div class="modal-header" style="background:#3c8dbc; color:white">
                    <button type="button" class="close" data-dismiss="modal" data-target='#machinePopUpAdd'
                        onclick="ResetControls('machineForm')">&times;</button>
                    <h4 class="modal-title">Agregar Equipo Externo</h4>
                </div>
                <!--=====================================
                  CUERPO DEL MODAL
                  ======================================-->
                <div class="modal-body">
                    <div class="box-body">
                        <div class="row">
                            <div class="alert bg-primary">
                                Si la empresa que estas buscando no se encuentra puedes agregar un nuevo registro
                                dandole click <a data-toggle="modal" data-target="#companyPopUpAdd" onclick="SetData()"
                                    role="button">Aquí</a>
                            </div>
                            <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-industry"></i></span>
                                    <?php echo $this->scope["productNameCtx"];?>

                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-cube"></i></span>
                                    <?php echo $this->scope["productBrandCtx"];?>

                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-industry"></i></span>
                                    <?php echo $this->scope["quotationCompanyLst"];?>

                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-cubes"></i></span>
                                    <input type="text" class="form-control" disabled="true" value="Equipos">
                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-cube"></i></span>
                                    <?php echo $this->scope["productReferenceCtx"];?>

                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-cubes"></i></span>
                                    <?php echo $this->scope["productCountryCtx"];?>

                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-cube"></i></span>
                                    <?php echo $this->scope["productPlaceLst"];?>

                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-cube"></i></span>
                                    <?php echo $this->scope["productPresentationCtx"];?>

                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-flag"></i></span>
                                    <?php echo $this->scope["productNameProviderCtx"];?>

                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-flag"></i></span>
                                    <?php echo $this->scope["productSerialLst"];?>

                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-flag"></i></span>
                                    <?php echo $this->scope["quotationNumberInventoryLst"];?>

                                </div>
                            </div>


                        </div>
                        <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-cube"></i></span>
                                <textarea class="form-control" id="descripcion" name="descripcion" rows="10"
                                    placeholder="agregar descripción producto"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <!--=====================================
                  PIE DEL MODAL
                  ======================================-->
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal"
                        data-target='#impostPopUpAdd' onclick="ResetControls('impostForm')">Salir</button>
                    <button type="submit" form="machineForm" class="btn btn-primary">Guardar </button>
                </div>
            </form>

        </div>
    </div>
</div>
<!--=====================================
VENTANA MODAL VER CONTACTOS
======================================-->
<div id="ContactPopUpAdd" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form id="impostForm" method="post" enctype="multipart/form-data">
                <!--=====================================
              Hidden
              ======================================-->
                <input id="optionInputForm" name="optionInputForm" type="hidden" value="0">
                <input id="idDataInputForm" name="idDataInputForm" type="hidden" value="0">
                <input id="isActionFromAjax" name="isActionFromAjax" type="hidden" value="false">
                <!--=====================================
              CABEZA DEL MODAL
              ======================================-->

                <div class="modal-header" style="background:#3c8dbc; color:white">
                    <button type="button" class="close" data-dismiss="modal" data-target='#impostPopUpAdd'
                        onclick="ResetControls('impostForm')">&times;</button>
                    <h4 class="modal-title">Lista De Contactos</h4>
                </div>
                <!--=====================================
              CUERPO DEL MODAL
              ======================================-->
                <div class="modal-body">
                    <div class="box-body">
                        <div class="row" id="ContactTab">

                        </div>
                    </div>
                </div>
                <!--=====================================
              PIE DEL MODAL
              ======================================-->
                <div class="modal-footer">
                    <button type="button" form="impostForm" class="btn btn-default pull-left" data-dismiss="modal"
                        data-target='#ContactPopUpAdd' onclick="ResetControls('impostForm')">Salir</button>
                </div>
            </form>

        </div>
    </div>
</div>
</div>

<!--=====================================
VENTANA MODAL AGREGAR COMPAÑIA O MODIFICAR
======================================-->
<div id="companyPopUpAdd" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form id="companyForm" role="form" method="post" enctype="multipart/form-data">
                <!--=====================================
                  Hidden
                  ======================================-->
                <input id="optionInputForm" name="optionInputForm" type="hidden" value="1">
                <input id="idDataInputForm" name="idDataInputForm" type="hidden" value="0">
                <input id="isActionFromAjax" name="isActionFromAjax" type="hidden" value="false">
                <!--=====================================
                  CABEZA DEL MODAL
                  ======================================-->
                <div class="modal-header" style="background:#3c8dbc; color:white">
                    <button type="button" class="close" data-dismiss="modal" data-target='#companyPopUpAdd'
                        onclick="ResetControls('companyForm')">&times;</button>
                    <h4 class="modal-title">Agregar empresa</h4>
                </div>
                <!--=====================================
                  CUERPO DEL MODAL
                  ======================================-->
                <div class="modal-body">
                    <div class="box-body">
                        <div class="row">
                            <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-industry"></i></span>
                                    <?php echo $this->scope["companyNameCtx"];?>

                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-check"></i></span> <?php echo $this->scope["companyNitCtx"];?>

                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                                    <?php echo $this->scope["companyPhoneCtx"];?>

                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>
                                    <?php echo $this->scope["companyEmailCtx"];?>

                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-globe"></i></span>
                                    <?php echo $this->scope["companyWebSiteCtx"];?>

                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-location-arrow"></i></span>
                                    <?php echo $this->scope["companyAddressCtx"];?>

                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-flag-o"></i></span>
                                    <?php echo $this->scope["companyCountryLst"];?>

                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-flag"></i></span> <?php echo $this->scope["companyCityLst"];?>

                                </div>
                            </div>

                            <?php echo $this->scope["companyNameHiddenCtx"];?>

                        </div>
                        <input type="hidden" name="companyIdRoleLst" value="2">
                        <input type="hidden" name="redirect" value="true">
                    </div>
                </div>
                <!--=====================================
                  PIE DEL MODAL
                  ======================================-->
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal"
                        data-target='#companyPopUpAdd' onclick="ResetControls('companyForm')">Salir</button>
                    <button onclick="this.form.action='Company';this.form.sub.mit()" form="companyForm"
                        class="btn btn-primary">Guardar Empresa</button>
                </div>
            </form>

        </div>
    </div>
</div>

<div id="containerResponseAjax"></div>
<script src="view/html/modules/maintenance/Maintenance.js?v= <?php echo $this->scope["tempParameter"];?>"></script>
<?php echo $this->scope["jquery"];
 /* end template body */
return $this->buffer . ob_get_clean();
?>