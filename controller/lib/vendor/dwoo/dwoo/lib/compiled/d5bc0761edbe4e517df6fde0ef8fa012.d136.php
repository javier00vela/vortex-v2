<?php
/* template head */
/* end template head */ ob_start(); /* template body */ ?><div class="content-wrapper">
    <section class="content-header">
        <h1>
            Administrar Cotización de Mantenimiento
        </h1>
        <ol class="breadcrumb">
            <li><a href="Home"><i class="fa fa-dashboard"></i> Inicio</a></li>
            <li class="active">Administrar Cotización  de Mantenimiento</li>
        </ol>
    </section>
    <input type="hidden" id="idMainteince" value="<?php echo $this->scope["idMainteince"];?>">
    <section class="content">
        <div class="row">
            <section class="col-md-6">
                <div class="row">
                    <div class="col-sm-6 col-xs-12  hidden">
                        <div class="form-group">
                            <label>Seleccione Rango De Registros a Mostrar(<i class="fa fa-info red-tooltip"  data-toggle='tooltip' data-placement='top' title="Recuerde que el filtro esta sincronizado a la cantidad de filas que existen "></i>)</label>                            <?php echo $this->scope["productListFilterLst"];?>
                        </div>
                    </div>
                    <div class="col-sm-12 col-xs-12">

                        <label for="input">Palabra Clave a Buscar (<kdb class="red-tooltip"  data-toggle='tooltip' data-placement='top' title="Recuerde que el filtro esta sincronizado con el 'nombre' - ' marca ' - 'referencia' "><i class="fa fa-info"></i></kdb>)  </label>
                        <div class="input-group">
                            <input class="form-control" type="text" id="wordKey" placeholder="Agregar palabra clave de Busqueda">
                            <span class="btn input-group-addon bg-blue" id="wordKeyBtn">Buscar</span>
                        </div>

                    </div>
                </div>
                <div class="box">
                    <div class="box-header"><h4 class="text-center"> Lista de Consumibles </h4></div>
                    <div class="box-body">
                        <table id="quotationTbl" name="quotationTbl" class="table table-bordered table-striped dt-responsive tables dataTable no-footer dtr-inline" width="100%" cellspacing="0">
                            <thead>
                                <th>Referencia</th>
                                <th>Nombre</th>
                                <th>presentación</th>
                                <th>Marca</th>
                                <th>Precio COP</th>
                                <th>Precio Unitario</th>
                                <th>Tipo de Moneda</th>
                                <th>Tipo de Producto</th>
                                <th>Id</th>
                                <th>Lugar</th>
                                <th>Proveedor</th>
                                <th>Cantidad Actual</th>
                                <th>Tipo de Moneda</th>
                                <th>Aplica IVA</th>
                                <th>Acciones</th>

                            </thead>
                        </table>
                       
                    </div>
                </div>


                <div class="box">
                        <div class="box-header"><h4 class="text-center"> Lista de Procesos </h4> </div>
                    <div class="box-body">
                        <table id="processTbl" name="processTbl" class="table table-bordered table-striped dt-responsive tables dataTable no-footer dtr-inline" width="100%" cellspacing="0">
                            <thead>
                                <th>Referencia</th>
                                <th>Nombre</th>
                                <th>tiempo</th>
                                <th>Marca</th>
                                <th>Precio COP</th>
                                <th>Precio Unitario</th>
                                <th>Tipo de Moneda</th>
                                <th>Tipo de Producto</th>
                                <th>Id</th>
                                <th>Lugar</th>
                                <th>Proveedor</th>
                                <th>Cantidad Actual</th>
                                <th>Tipo de Moneda</th>
                                <th>Aplica IVA</th>
                                <th>servicio a aplicar</th>
                                <th>código proceso</th>
                                <th>Acciones</th>
                            </thead>
                        </table>
                    </div>

                    
                </div>
            </section>
            <section class="col-md-6 text-center">
                <label>&nbsp</label>
                <div class="box">
                    <div class="box-body">

                        <input id="optionInputForm" name="optionInputForm" type="hidden">
                        <input id="idDataInputForm" name="idDataInputForm" type="hidden">
                        <input id="isActionFromAjax" name="isActionFromAjax" type="hidden">
                        <div class="form-group">
                            <input type="hidden" id="nameCompany" value="<?php echo $this->scope["nameCompany"];?>">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-industry"></i></span> <?php echo $this->scope["quotationCompanyLst"];?>
                                </div>
                            </div>
                        
                            
    
                            <input type="hidden" id="user-id" value=" <?php echo unserialize($_SESSION['user']['user'])->id ?>">
                            <input id="isContactsContainerShow" name="isContactsContainerShow" type="hidden" value="0">
                            <div id="contactsContainer" name="contactsContainer" class="form-group" style="border: 1px solid #eee;display: none;">
                                <label id="titleContacts" style="margin-left: 6px;">Dirigido a:</label>
                            </div>

                        <input type="hidden" id="user-id" value=" <?php echo unserialize($_SESSION['user']['user'])->id ?>">
                        <input id="isContactsContainerShow" name="isContactsContainerShow" type="hidden" value="0">
                        <div id="contactsContainer" name="contactsContainer" class="form-group" style="border: 1px solid #eee;display: none;">
                            <label id="titleContacts" style="margin-left: 6px;">Dirigido a:</label>
                        </div>
                        
                        <div class="row">
                            <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-calendar-check-o"></i></span> <?php echo $this->scope["dateUpdateCtx"];?>
                                    <!-- <input id="userNamesCtx" name="userNamesCtx" type="text" class="form-control input-lg" placeholder="Ingresar nombres" required> -->
                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-12">
                                <div id="typePricesContainer" name="TypePricesContainer" class="form-group">
                                    <label id="titleTypePrices" style="margin-left: 6px;">Precio Moneda:</label>

                                    <div class="form-group col-sm-4 col-xs-6 ">
                                        <label>Código</label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-key"></i></span>
                                            <input type="text" class="form-control" id="code-quotation" readonly="readonly">
                                            <input type="hidden" class="form-control" id="code" readonly="readonly" value="0">
        
                                        </div>
                                    </div>
                                    <div class="form-group col-sm-4 col-xs-6">
                                        <label for="">Estado</label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
        
                                            <input type="text" class="form-control" id="status-quotation" readonly="readonly" value="Creada">
                                        </div>
                                    </div>
                                    <div class="form-group col-xs-12 col-sm-4">
                                        <div id="typePricesContainer" name="TypePricesContainer" class="form-group">
                                            <div class='checkbox visible-xs text-center m-3' style="margin-top:15px;">
                                                <div class="row">
                                                    <div class="col-xs-4">
                                                        <input type='radio' id='COP' name='checkboxTypePrice' value='COP' checked="checked">
                                                        <id>COP
                                                    </div>
                                                    <div class="col-xs-4">
                                                        <input type='radio' id='USD' name='checkboxTypePrice' value='USD'>
                                                        <id>USD
                                                    </div>
                                                    <div class="col-xs-4">
                                                        <input type='radio' id='EUR' name='checkboxTypePrice' value='EUR'>
                                                        <id>EUR
                                                    </div>
                                                </div>
                                            </div>
        
                                            <div class='checkbox hidden-xs' style='margin-left: 6px;'><label style="padding-right: 10px;">
                                                        <input type='radio' id='COP' name='checkboxTypePrice'  value='COP' checked="checked" ><id >COP  </label><label style="padding-right: 10px;margin-top: 10px">
                                                        <input type='radio' id='USD' name='checkboxTypePrice' value='USD'><id>USD  </label><label style="padding-right: 10px;">
                                                        <input type='radio' id='EUR'  name='checkboxTypePrice' value='EUR'><id>EUR  </label>
                                            </div>
        
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <input type="hidden" id="versionQuotation" value="1">

                        <div class="form-group">
                                <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-cubes"></i></span> <?php echo $this->scope["quotationViaticLst"];?>
                                        <span class="input-group-addon" id="buttonViaticoSee"></span> 
                                </div>
                        </div>

                        
                        
                        <div class="row">
                            <div class="form-group col-sm-6 col-xs-12">
                                <label for="">Validez de la oferta:</label>
                                <div id="quotationOfferContainer" name="quotationOfferContainer" class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-history"></i></span> <?php echo $this->scope["quotationOfferLst"];?>
                                </div>
                            </div>

                            <div class="form-group col-sm-6 col-xs-12">
                                <label for="">Forma de pago:</label>
                                <div id="quotationPaymentContainer" name="quotationPaymentContainer" class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-dollar"></i></span> <?php echo $this->scope["quotationPaymentLst"];?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-6 col-xs-12">
                                <label for="">Tipo Cotización:</label>
                                <div id="quotationTypeContainer" name="quotationTypeContainer" class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-cube"></i></span> <?php echo $this->scope["quotationTypeLst"];?>
                                </div>
                            </div>
                            <div class="form-group col-sm-6 col-xs-12">
                                <label for="">Lugar de Entrega: </label>
                                <div id="quotationPlaceContainer" name="quotationPlaceContainer" class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-globe"></i></span> <?php echo $this->scope["quotationPlaceLst"];?>
                                </div>
                            </div>
                        </div>
                        <div id="productContainer" name="productContainer" style=""></div>
                        <div class="row">
                            <div class="col-sm-12 col-xs-12">
                                    <div id="viaticTotal"></div>
                            </div>
                                
                            <div class="form-group col-sm-6 col-xs-12">
                                <label for="">IVA</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-dollar"></i></span>
                                    <input type="text" class="form-control" id="totalIva" readonly="readonly" value="$0">
                                </div>
                            </div>


                            <div class="form-group col-sm-6 col-xs-12">
                                <label for="">Total</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-dollar"></i></span>
                                    <input type="text" class="form-control" id="totalPrice" readonly="readonly" value="$0">
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="hidden form-group col-sm-12 col-xs-12">
                                <div class="alert bg-primary text-center">
                                    <input type="checkbox" name="checkbox" style="margin-top: 5px" class="checked" id="aplicaIVA" checked><label>¿Desea Aplicar el IVA en esta Cotización? </label>
                                </div>

                            </div>
                        </div>
                        <div class="row">

                            <div class="form-group col-sm-6 col-xs-12">
                                <label for="">Persona Cotización:</label>
                                <div id="usersContainer" name="usersContainer" class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-users"></i></span> <?php echo $this->scope["quotationUserLst"];?>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="form-group col-xs-12">
                                <button id="generateQuotation" class="btn btn-primary btn-xs-block generateQuotation">Generar cotización</button>
                            </div>
                        </div>

                    </div>
                </div>
            </section>
        </div>
    </section>
</div>

<!--=====================================
VENTANA MODAL AGREGAR PERSONA O MODIFICAR
======================================-->
<div id="personPopUpAdd" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form id="personForm" role="form" method="post" enctype="multipart/form-data">
                <!--=====================================
                  Hidden
                  ======================================-->
                <input id="optionInputForm" name="optionInputForm" type="hidden" value="0">
                <input id="idDataInputForm" name="idDataInputForm" type="hidden" value="0">
                <input id="isActionFromAjax" name="isActionFromAjax" type="hidden" value="false">
                <!--=====================================
                  CABEZA DEL MODAL
                  ======================================-->
                <div class="modal-header" style="background:#3c8dbc; color:white">
                    <button type="button" class="close" data-dismiss="modal" data-target='#personPopUpAdd' onclick="ResetControls('personForm')">&times;</button>
                    <h4 class="modal-title">Agregar prospecto</h4>
                </div>
                <!--=====================================
                  CUERPO DEL MODAL
                  ======================================-->
                <div class="modal-body">
                    <div class="box-body">
                        <div class="row">

                            <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-user"></i></span> <?php echo $this->scope["personNamesCtx"];?>
                                    <!-- <input id="userNamesCtx" name="userNamesCtx" type="text" class="form-control input-lg" placeholder="Ingresar nombres" required> -->
                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-user-o"></i></span> <?php echo $this->scope["personLastNamesCtx"];?>
                                    <!-- <input id="userLastNamesCtx" name="userLastNamesCtx" type="text" class="form-control input-lg"  placeholder="Ingresar apellidos" required> -->
                                </div>
                            </div>
                            <!-- <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-id-card"></i></span> <?php echo $this->scope["personDocumentTypeLst"];?>

                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-id-card-o"></i></span> <?php echo $this->scope["personDocumentCtx"];?>

                                </div>
                            </div>-->
                            <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-mobile"></i></span> <?php echo $this->scope["personCellPhoneCtx"];?>
                                    <!-- <input id="userCellNumberCtx" name="userCellNumberCtx" type="text" class="form-control input-lg" placeholder="Ingresar numero celular" required> -->
                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-phone"></i></span> <?php echo $this->scope["personTelephoneCtx"];?>
                                    <!-- <input id="userTelePhoneNumberCtx" name="userTelePhoneNumberCtx" type="text" class="form-control input-lg" name="nuevoTelefono" placeholder="Ingresar Telefono"> -->
                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-envelope-o"></i></span> <?php echo $this->scope["personEmailCtx"];?>
                                    <!-- <input id="userEmailCtx" name="userEmailCtx" type="text" class="form-control input-lg" name="nuevoEmail" placeholder="Ingresar Email" required> -->
                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-map-marker"></i></span> <?php echo $this->scope["personAddressCtx"];?>
                                    <!-- <input id="userEmailCtx" name="userEmailCtx" type="text" class="form-control input-lg" name="nuevoEmail" placeholder="Ingresar Email" required> -->
                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-map-marker"></i></span> <?php echo $this->scope["personCityCtx"];?>
                                    <!-- <input id="userEmailCtx" name="userEmailCtx" type="text" class="form-control input-lg" name="nuevoEmail" placeholder="Ingresar Email" required> -->
                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-building"></i></span> <?php echo $this->scope["companyLst"];?>
                                    <!-- <input id="userEmailCtx" name="userEmailCtx" type="text" class="form-control input-lg" name="nuevoEmail" placeholder="Ingresar Email" required> -->
                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-list"></i></span>
                                    <div><?php echo $this->scope["personUserLst"];?></div>
                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-cubes"></i></span>
                                    <div><?php echo $this->scope["personPrefijoLst"];?></div>
                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="input-group">
                                    <div><?php echo $this->scope["personIdRoleHidden"];?></div>
                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="input-group">
                                    <div><?php echo $this->scope["personIdCompanyHidden"];?></div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
                <!--=====================================
                  PIE DEL MODAL
                  ======================================-->
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal" data-target='#personPopUpAdd' onclick="ResetControls('personForm')">Salir</button>
                    <button id="InsertClient" form="withoutForm" class="btn btn-primary">Guardar Prospecto</button>
                </div>
            </form>

        </div>
    </div>
</div>


<!--=====================================
VENTANA MODAL AGREGAR PERSONA O MODIFICAR
======================================-->
<div id="viaticosPopUpAdd" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form id="personForm" role="form" method="post" enctype="multipart/form-data">
                <!--=====================================
                  Hidden
                  ======================================-->
                <input id="optionInputForm" name="optionInputForm" type="hidden" value="0">
                <input id="idDataInputForm" name="idDataInputForm" type="hidden" value="0">
                <input id="isActionFromAjax" name="isActionFromAjax" type="hidden" value="false">
                <!--=====================================
                  CABEZA DEL MODAL
                  ======================================-->
                <div class="modal-header" style="background:#3c8dbc; color:white">
                    <button type="button" class="close" data-dismiss="modal" data-target='#personPopUpAdd' onclick="ResetControls('personForm')">&times;</button>
                    <h4 class="modal-title">Lista de Viaticos</h4>
                </div>
                <!--=====================================
                  CUERPO DEL MODAL
                  ======================================-->
                <div class="modal-body">
                    <div class="box-body">
                        <div class="row">
                            <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div id="viaticDIV">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--=====================================
                  PIE DEL MODAL
                  ======================================-->
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal" data-target='#personPopUpAdd' onclick="ResetControls('personForm')">Salir</button>
                </div>
            </form>

        </div>
    </div>
</div>






<!-- lolo -->
<div id="containerResponseAjax"></div>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.0/dist/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.0/dist/additional-methods.min.js"></script>
<script src="view/html/modules/quotemainteince/QuotationJS/QuotationCallAjax.js?v= <?php echo $this->scope["tempParameter"];?>"></script>
<script src="view/html/modules/quotemainteince/QuotationJS/GeneralFuncionsQuotation.js?v= <?php echo $this->scope["tempParameter"];?>"></script>
<script src="view/html/modules/quotemainteince/QuotationJS/PricesInternational.js?v= <?php echo $this->scope["tempParameter"];?>"></script>
<script src="view/html/modules/quotemainteince/QuoteMainteince.js?v= <?php echo $this->scope["tempParameter"];?>"></script>
<?php echo $this->scope["jquery"];
 /* end template body */
return $this->buffer . ob_get_clean();
?>