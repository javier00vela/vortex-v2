<?php 

class ExcelManager
{
	
private static $numDataRows;
private static $lettersArray;
private static $columnsName;
private static $nameFile;
private static $dataGet;
const  EXCEL_PATH = "excel/";

private  function CreateObjectExcel(){
   $excelObject = new PHPExcel();
   return $excelObject;
}

private  function MakeAbecedary(){
	$array = [];
	for($i=65; $i<=90; $i++) {  
    	$array[] = chr($i); 
    }
    return $array; 
}


		private  function SaveExcel($path , $excelName, $excelObject){
		  $excelObject = new PHPExcel_Writer_Excel2007($excelObject);
		  $excelObject->save($path.$excelName.".xlsx");
		}


		public static function CreateReportExcel($data , $columnsNames , $fileName){
		    if(isset($data)){
		    	self::$lettersArray =  self::MakeAbecedary();
		    	self::$columnsName = $columnsNames;
		    	self::$nameFile = $fileName;
		    	self::$dataGet = $data;
		    	self::LoadDataReportExcel();
		  }
		}

		 private  function loadData(){
		 	$data = [];
		    self::$numDataRows= self::$dataGet;
		    if(isset(self::$numDataRows)){
		        for ($i=0; $i < count(self::$numDataRows) ; $i++) { 
		           	$data[] = explode(",",self::$numDataRows[$i]);
		            $data[$i] = str_replace("*|*",",",$data[$i]);
		            print_r($data[$i]);
		           		if($i == count(self::$numDataRows)){
		            		unset($data[$i]);
		           		}
		        }
		        return $data;
		     }
		 }


	private  function LoadDataReportExcel(){
		 self::$dataGet = self::loadData();
		 $excelObject = self::CreateObjectExcel();
		 self::SetBodyExcel($excelObject );	 
		 self::SetHeaderExcel($excelObject);
		 self::SaveExcel(Config::VIEW_DOCS.self::EXCEL_PATH , self::$nameFile, $excelObject);  
		    
	}

	private function SetBodyExcel($excelObject ){
		  $cont = 1;
		    for ($i=0; $i < count(self::$numDataRows) ;$i++) { 
		    	for ($e=0; $e < count(self::$columnsName);$e++) { 
			      		$excelObject->setActiveSheetIndex(0)->setCellValue(self::$lettersArray[$e].$cont,self::$dataGet[$i][$e]);    	
			  }
				$cont++;
		    }
		   return $excelObject;
	}

	private function SetHeaderExcel($excelObject){
		$style = array('font' => array('size' => 14,'bold' => true,'color' => array('rgb' => 'FFFFFF')) , 'alignment' => array('horizontal' => 'center' , 'vertical' => 'center'),'fill' => array(
            'type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => '3980E0')
        ));
		$excelObject->getActiveSheet()->getStyle('A1:'.self::$lettersArray[count(self::$columnsName)]."1")->applyFromArray($style);

		for ($i=0; $i < count(self::$columnsName) ; $i++) { 
					$excelObject->getActiveSheet()->getColumnDimension(strtoupper(self::$lettersArray[$i]))->setAutoSize(true);
			      	$excelObject->setActiveSheetIndex(0)->setCellValue(self::$lettersArray[$i]."1",self::$columnsName[$i]);
		}
		return $excelObject;
	}

		

}
 ?>