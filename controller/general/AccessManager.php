<?php 
require_once(Config::PATH . Config::BACKEND . 'modules/PersonVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/UserVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/RoleVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/ModuleRoleVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/ModuleVo.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralDao.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralVo.php');

class AccessManager{

private $module;
private $role;
private $allowsArray = [];


	public function __construct($module){
		$this->module = $module ;
		$this->LoadModulesAllows();		
		$this->role = $this->GetRoleBySesssion();	
		$this->validateURL();		
	}



	public function validateURL(){	
		$isAllow = false;
			for ($e=0; $e < count($this->allowsArray[$this->role]) ; $e++) { 
				if($this->allowsArray[$this->role][$e] == $this->module){
				 require_once(Config::PATH . Config::MODULES_VIEW . '' . strtolower($this->module) . '/' . $this->module . 'View.php');
				  $isAllow = true;
				}
			}
			return $isAllow;
	    }


	public function LoadModulesAllows(){
		$cnt = 0;
        $rolesVo = new ModuleRoleVo();
        $rolesVo->isList = true;

        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $rolesVo);
        while ($rolesVo2 = $generalDao->GetVo($generalVo = clone $rolesVo)) {
        	if(!in_array($this->GetNameRole($rolesVo2->idRol), $this->allowsArray)){
                 $this->allowsArray[] = $this->GetNameRole($rolesVo2->idRol);
             }
             $cnt++;
 
            	if($rolesVo2->state == 1){
            		$this->allowsArray[$this->GetNameRole($rolesVo2->idRol)][] = $this->GetNameModule($rolesVo2->idModule); 

            	}
        }
    }



    public function GetNameModule($id){
        $moduleVo = new ModuleVo();
        $moduleVo->isList = false;
        $moduleVo->idSearchField = 0;
        $moduleVo->id = $id;
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $moduleVo);
        while($moduleVo2 = $generalDao->GetVo($generalVo = clone $moduleVo)) {
            return $moduleVo2->name ;
        }
        return "error al realizar consulta";
    }

    public function GetNameRole($id){
        $roleVo = new RoleVo();
        $roleVo->isList = false;
        $roleVo->idSearchField = 0;
        $roleVo->id = $id;
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $roleVo);
        while($roleVo2 = $generalDao->GetVo($generalVo = clone $roleVo)) {
            return $roleVo2->name ;
        }
        return "error al realizar consulta";
    }
	

	

	public function GetRoleBySesssion(){

		$userVo = unserialize($_SESSION['user']["user"]);
		$personVo = unserialize($_SESSION['user']["person"]);
        $RoleVo= new RoleVo();
        $RoleVo->id = $personVo->idRole;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $RoleVo);
        while ($RoleVo2 = $generalDao->GetVo($generalVo = clone $RoleVo)) {
            $name = $RoleVo2->name; 
        }
        return $name;
	}	

}

 ?>