<?php 
require_once(Config::PATH . Config::CONTROLLER_LIB .'vendor/phpmailer/phpmailer/src/Exception.php');
require_once(Config::PATH . Config::CONTROLLER_LIB .'vendor/phpmailer/phpmailer/src/PHPMailer.php');
require_once(Config::PATH . Config::CONTROLLER_LIB .'vendor/phpmailer/phpmailer/src/SMTP.php');

class PHPMailerUtils{
	private $mailer;
	private $mail;
	private $password;
	private $port;
	private $from;
	private $domain;



	public function __construct($data){
		$this->mail = $data["mail"];
		$this->password = $data["password"];
		$this->from = $data["from"];
		$this->port = $data["port"];
		$this->domain = $data["domain"];
		$this->SeTDataConfig();

	}

	private function MakeObjectMailer(){
		$mail =	new PHPMailer\PHPMailer\PHPMailer;
		return $mail;
	}

	public function SeTDataConfig(){
		$this->mailer= $this->MakeObjectMailer();
		$this->mailer->isSMTP();
		$this->mailer->Host =$this->domain;
		$this->mailer->CharSet ="UTF-8";
		$this->mailer->Port = $this->port;
		$this->mailer->SMTPAuth=true;
		$this->mailer->Username = $this->mail;
		$this->mailer->SMTPDebug=4;
		$this->mailer->Password = $this->password;
		$this->mailer->setFrom($this->from);
		$this->mailer->SMTPSecure="tls";//ssl
	}


	public function AddImages(){
		$this->mailer->AddEmbeddedImage("http://52.207.211.4/vortex/view/img/main-logo.png","1");
		$this->mailer->AddEmbeddedImage("http://52.207.211.4/vortex/view/img/Fondo1.png","2");
	}
	public function AddDestinatario($email){
		$this->mailer->addAddress($email);
	}

	public function SendDataMail($subject , $contenido, $alias){
		
		$this->mailer->FromName = $alias; 
		$this->mailer->Subject=$subject;
		$this->mailer->IsHTML(TRUE);
		//$this->mailer->msgHTML($contenido);
	
		$this->mailer->Body=$contenido;
		$response = array();
		if ($this->mailer->send()){
			$response["CODE"] = 1;
		}else{
			$response["CODE"] = $this->mailer->ErrorInfo;
		}
		//print_r($response);
	  }
	  

}
 ?>