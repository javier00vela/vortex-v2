<?php
require_once(Config::PATH . Config::BACKEND . 'general/GeneralVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/UserVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/PersonVo.php');

class ManageSession{

    function __construct()
    {

    }

    public function IsSessionTimeEnd(){
        $_SESSION["startSession"] = "true";
    }


    public function CreateSessionUser($userVo){
        $_SESSION['time'] = time();
        $_SESSION["startSession"] = "true";
        $_SESSION["user"]["user"] = serialize($userVo);
    }

    public function CreateSessionPerson($personVo){
        $_SESSION["startSession"] = "true";
        $_SESSION['user']["person"] = serialize($personVo);
    }

    public  function UpdateTimeSession(){
        $_SESSION['time'] = time();
        $_SESSION["startSession"] = "true";
    }

    public function GetUser(){
        $_SESSION["startSession"] = "true";
        $userVo = unserialize($_SESSION["user"]["user"]);
        
        return $userVo;
    }

    /**
     * @return bool
     */
    public function IsSessionStart(){
        session_start();
        if(empty($_SESSION["startSession"])){
            $_SESSION["startSession"] = "true";
        }
      
    }

}


