<?php

class Config {

    const NAME_PROJECT = "vortex";
    const BACKEND = "backend/";
    const BACKEND_LIB = "backend/lib/";
    const GENERAL = "general/";
    const CONTROLLER_LIB = "controller/lib/";
    const CONTROLLER = "controller/";
    const VIEW_HTML = "view/html/";
    const VIEW_DOCS = "view/docs/";
    const VIEW_HTML_MODULES = "view/html/modules/";
    const MODULES_VIEW = "view/html/modules/";
    const VIEW_IMGS = "view/imgs/";
    const KEY_PROYECT = "c4ca4238a0b923820dcc509a6f75849b";
    const MODE_DEBUG = true;

/*********************************************************************/
  //const PATH= "/Applications/XAMPP/htdocs/scrummini/";
  //  cons PATH= "/opt/lampp/htdocs/scrummini/";
    const PATH = "C:/xampp/htdocs/vortex/";
    const HOST = "localhost";
    const USER = "root";
    const PASS = "";
    const BD = "vortexdb";
    const REDIRECTS="localhost/vortex/";
    const REDIRECTION = "http://localhost/vortex/controller/general/index.php?view=";

/*********************************************************************/
    // const PATH= "/home2/middle12/public_html/gekkoins.com/scrummini/";
    // const HOST = "localhost";
    // const USUARIO = "middle12_scrum";
    // const CLAVE = "scrum1234";
    // const BD = "middle12_scrumdb";
    // const REDIRECTS="http://gekkoins.com/scrummini/";

}


/*

class Config {

    const NAME_PROJECT = "vortex";
    const BACKEND = "backend/";
    const BACKEND_LIB = "backend/lib/";
    const GENERAL = "general/";
    const CONTROLLER_LIB = "controller/lib/";
    const CONTROLLER = "controller/";
    const VIEW_HTML = "view/html/";
    const VIEW_HTML_MODULES = "view/html/modules/";
    const MODULES_VIEW = "view/html/modules/";
    const VIEW_IMGS = "view/imgs/";
    const VIEW_DOCS = "view/docs/";
    const KEY_PROYECT = "c4ca4238a0b923820dcc509a6f75849b";

const PATH= "/Applications/XAMPP/htdocs/scrummini/";
  cons PATH= "/opt/lampp/htdocs/scrummini/";
 const PATH = "C:/xampp/htdocs/vortex/";
  const HOST = "localhost";
  const USER = "root";
  const PASS = "";
  const BD = "vortexdb";
    const REDIRECTS="http://arvack.com/vortex/";
    const REDIRECTION = "http://arvack.com/vortex/controller/general/index.php?view=";

     const PATH= "/home2/middle12/public_html/arvack.com/vortex/";
     const HOST = "localhost";
     const USER = "middle12_vortex";
     const PASS = "Vortex153";
     const BD = "middle12_vortexdb";
     const REDIRECTS="http://gekkoins.com/scrummini/";

}

*/