<?php

require_once(Config::PATH . Config::CONTROLLER . 'general/PHPMailer.php');
require_once(Config::PATH . Config::BACKEND . 'modules/AlertsVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/ModuleEventVo.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralDao.php');

class MailsUtils{

    private $Mailer = null;
    private $dataMailer = [
        'mail' => "sysvortex@arvack.com", 
        'password' => "sysvortex153",
        'domain' => "mail.arvack.com", 
        'port' => 25 ,
        "from" => "sysvortex@arvack.com"
    ];

    public function __construct(){
       $this->Mailer =  new PHPMailerUtils($this->dataMailer);
    }

    public function GetArrayModel(){
        return [
            "Event" => "",
            "IdRol" => 0,
            "WordReplace" => [],
            "ToReplace" => [],
            "Alias" => ""

        ];
    }

    
    public function AddDestinatario($AddDestinatario){
        $this->Mailer->AddDestinatario($AddDestinatario);
    }


    public function ReplaceDescription($description , $array , $toArray){
        $descriptionReturn = $description; 
        for($i = 0 ; $i < count($array) ; $i++ ){
            $descriptionReturn = str_replace($array[$i] , $toArray[$i] , $descriptionReturn);
        }
        return $descriptionReturn;
    }

    public function SendWithDate($data , $date){

        if (strtotime($date["init"]) == strtotime($date["finish"])) {
            //print_r($date["init"]." compare con ".$date["finish"]."#");
           // print_r($data);
            $this->Send($data);
        }

    }

    public function SendWithSame($data , $words){
        
        if ($words["one"] == $words["two"]){
            
            $this->Send($data);
        }

    }

    private function Send($data){
        $this->Mailer->SendDataMail($this->GetEventById($data["Event"])->tittle, $this->ReplaceDescription($this->GetEventById($data["Event"])->description , $data["WordReplace"] , $data["ToReplace"]  ) , $data["Alias"]);
    }

    private function ValidateAbleEvent($idRol , $idEvent){
        $rolesVo = new ModuleEventVo();
        $rolesVo->idEvent = $idEvent;
        $bool = false;
        $rolesVo->idSearchField = 2;
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $rolesVo);
        while ($rolesVo2 = $generalDao->GetVo($generalVo = clone $rolesVo)) {
               if($rolesVo2->idRol == $idRol && $rolesVo2->state == '1' ){
                    $bool = true;
               }else{
                    $bool = true;
               }
        }

        return $bool;
    }

    public function GetEventById($id){
        $rolesVo = new AlertsVo();
        $rolesVo->idEvent = $id;
        print_r($id."es te es ");
        $rolesVo->idSearchField = 3;
        $generalDao = new GeneralDao();
        $generalDao->GetByField($generalVo = clone $rolesVo);
        if ($rolesVo2 = $generalDao->GetVo($generalVo = clone $rolesVo)) {
               return $rolesVo2;
        }
    }



}

?>