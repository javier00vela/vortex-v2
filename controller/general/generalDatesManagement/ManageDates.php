<?php

require_once(Config::PATH . Config::GENERAL . 'generalDatesManagement/Holiday.php');

/**
 * ManageDates
 *
 * class responsible for manage dates
 *
 * @author		Harold D Duque C
 * @version		1.0
 * @since		May of 2014
 */
class ManageDates {

  public static function GetBussinesDays($fechainicio, $fechafin, $diasferiados = array()) {
      // Convirtiendo en timestamp las fechas
      $fechainicio = strtotime($fechainicio);
      $fechafin = strtotime($fechafin);

      // Incremento en 1 dia
      $diainc = 24 * 60 * 60;

      // Arreglo de dias habiles, inicianlizacion
      $diashabiles = array();

      // Se recorre desde la fecha de inicio a la fecha fin, incrementando en 1 dia
      for ($midia = $fechainicio; $midia <= $fechafin; $midia += $diainc) {
          // Si el dia indicado, no es sabado o domingo es habil
          if (!in_array(date('N', $midia), array(6, 7))) { // DOC: http://www.php.net/manual/es/function.date.php
              // Si no es un dia feriado entonces es habil
              if (!in_array(date('Y-m-d', $midia), $diasferiados)) {
                  array_push($diashabiles, date('Y-m-d', $midia));
              }
          }
      }

      return $diashabiles;
  }

  public static function GetDatesHoliday($year) {

      $arrayHoliday = new Holiday();
      $holi = $arrayHoliday->getFestivos($year);

      foreach ($holi[$year] as $key1 => $value) {
          $valueMonth = ManageDates::AddZeroToDate($key1);
          foreach ($value as $key2 => $value) {
              $arrayDate[] = $year . "-" . $valueMonth . "-" . ManageDates::AddZeroToDate($key2);
          }
      }
      asort($arrayDate, SORT_STRING | SORT_FLAG_CASE | SORT_NATURAL);
      return $arrayDate;
  }

  public static function AddZeroToDate($date) {
      $date;
      if ($date < 10) {
          $complete = "0" . $date;
      } else {
          return $date;
      }
      return $complete;
  }

  public static function GetNumberBussinesDays($dateStart, $dateFinish) {
      $arrayDates = ManageDates::GetDatesHoliday(date("Y"));
      $arrayBussinesDates = ManageDates::GetBussinesDays($dateStart, $dateFinish, $arrayDates);
      return count($arrayBussinesDates);
  }

  public static function GetNumberBussinesHours($numberDays) {
      return ($numberDays*8);
  }

/********************************************************************************/

    public static function GetFormatDate($date) {
        return date('Y-m-d', strtotime(str_replace('-', '/', $date)));
    }

    public static function GetNumberOfDaysBetweenTwoDates($date1,$date2) {
        $date1 = strtotime($date1);
        $date2 = strtotime($date2);
        $datediff = $date1 - $date2;
        return ((-1)*floor($datediff/(60*60*24)))+1;
    }

    public static function ValidateHigherDate($date1, $date2) {
        $date1 = strtotime($date1);
        $date2 = strtotime($date2);
        if($date1 > $date2){
            return true;
        }else {
            return false;
        }
    }

    public static function MonthOfADate($date) {
        return date('m', strtotime(str_replace('-', '/', $date)));
    }

    public static function AllDatesBetweenTwoDates($date1, $date2) {

        $date1 = ManageDates::GetFormatDate($date1);
        $date2 = ManageDates::GetFormatDate($date2);

        $arrayDates = array();
        $diferenceDays = ManageDates::GetNumberOfDaysBetweenTwoDates($date1, $date2);
        $nextDay = ManageDates::NextDay ($date1);

        $arrayDates[] = $date1;
        for ($i = 1; $i < $diferenceDays; $i++)
        {
            $arrayDates[] = $nextDay;
            $nextDay = ManageDates::NextDay ( $nextDay );
        }

        return ($arrayDates);
    }

    public static function NextDay ($date){
        $date = new DateTime($date);
        $date->add (new DateInterval('P1D'));
        return ($date->format('Y-m-d'));
    }

    public static function GetDayOfTheWeek($date){
        $date = new DateTime($date);
        return $date->format('w');
    }

    public static function ExistDayBetweenDates($numDay,$dateCompareto){
        $option = false;
        if($numDay == 7 && ManageDates::GetDayOfTheWeek($dateCompareto) == 0){
            $option = true;
        }else if($numDay == ManageDates::GetDayOfTheWeek($dateCompareto)){
            $option = true;
        }
        return $option;
    }

    public static function GetDateTime() {
        return date("Y-m-d H:i:s");
    }

}
