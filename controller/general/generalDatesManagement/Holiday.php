<?php
/**
 * Description of Holidays
 *
 *
 */
class Holiday {

  private $ano;
  private $hoy;
	private $festivos;
	private $pascua_mes;
	private $pascua_dia;

	public function GetFestivos($ano=''){
		$this->Holiday($ano);
		return $this->festivos;
	}

	public function Holiday($ano='')
	{
		$this->hoy=date('d/m/Y');

		if($ano=='')
			$ano=date('Y');

		$this->ano=$ano;

		$this->pascua_mes=date("m", easter_date($this->ano));
		$this->pascua_dia=date("d", easter_date($this->ano));

		$this->festivos[$ano][1][1]   = true;		// Primero de Enero
		$this->festivos[$ano][5][1]   = true;		// Dia del Trabajo 1 de Mayo
		$this->festivos[$ano][7][20]  = true;		// Independencia 20 de Julio
		$this->festivos[$ano][8][7]   = true;		// Batalla de Boyacá 7 de Agosto
		$this->festivos[$ano][12][8]  = true;		// Maria Inmaculada 8 diciembre (religiosa)
		$this->festivos[$ano][12][25] = true;		// Navidad 25 de diciembre

		$this->CalculaEmiliani(1, 6);				// Reyes Magos Enero 6
		$this->CalculaEmiliani(3, 19);				// San Jose Marzo 19
		$this->CalculaEmiliani(6, 29);				// San Pedro y San Pablo Junio 29
		$this->CalculaEmiliani(8, 15);				// Asunción Agosto 15
		$this->CalculaEmiliani(10, 12);			// Descubrimiento de América Oct 12
		$this->CalculaEmiliani(11, 1);				// Todos los santos Nov 1
		$this->CalculaEmiliani(11, 11);			// Independencia de Cartagena Nov 11

		//otras fechas calculadas a partir de la pascua.

		$this->OtrasFechasCalculadas(-3);			//jueves santo
		$this->OtrasFechasCalculadas(-2);			//viernes santo

		$this->OtrasFechasCalculadas(43,true);		//Ascención el Señor pascua
		$this->OtrasFechasCalculadas(64,true);		//Corpus Cristi
		$this->OtrasFechasCalculadas(71,true);		//Sagrado Corazón

		// otras fechas importantes que no son festivos

		// $this->OtrasFechasCalculadas(-46);		// Miércoles de Ceniza
		// $this->OtrasFechasCalculadas(-46);		// Miércoles de Ceniza
		// $this->OtrasFechasCalculadas(-48);		// Lunes de Carnaval Barranquilla
		// $this->OtrasFechasCalculadas(-47);		// Martes de Carnaval Barranquilla
	}

	protected function CalculaEmiliani($mes_festivo,$dia_festivo)
	{
		// funcion que mueve una fecha diferente a lunes al siguiente lunes en el
		// calendario y se aplica a fechas que estan bajo la ley emiliani
		//global  $y,$dia_festivo,$mes_festivo,$festivo;
		// Extrae el dia de la semana
		// 0 Domingo  6 Sábado
		$dd = date("w",mktime(0,0,0,$mes_festivo,$dia_festivo,$this->ano));
		switch ($dd) {
		case 0:                                    // Domingo
		$dia_festivo = $dia_festivo + 1;
		break;
		case 2:                                    // Martes.
		$dia_festivo = $dia_festivo + 6;
		break;
		case 3:                                    // Miércoles
		$dia_festivo = $dia_festivo + 5;
		break;
		case 4:                                     // Jueves
		$dia_festivo = $dia_festivo + 4;
		break;
		case 5:                                     // Viernes
		$dia_festivo = $dia_festivo + 3;
		break;
		case 6:                                     // Sábado
		$dia_festivo = $dia_festivo + 2;
		break;
		}
		$mes = date("n", mktime(0,0,0,$mes_festivo,$dia_festivo,$this->ano))+0;
		$dia = date("d", mktime(0,0,0,$mes_festivo,$dia_festivo,$this->ano))+0;
		$this->festivos[$this->ano][$mes][$dia] = true;
	}

	protected function OtrasFechasCalculadas($cantidadDias=0,$siguienteLunes=false)
	{
		$mes_festivo = date("n", mktime(0,0,0,$this->pascua_mes,$this->pascua_dia+$cantidadDias,$this->ano));
		$dia_festivo = date("d", mktime(0,0,0,$this->pascua_mes,$this->pascua_dia+$cantidadDias,$this->ano));

		if ($siguienteLunes)
		{
			$this->CalculaEmiliani($mes_festivo, $dia_festivo);
		}
		else
		{
			$this->festivos[$this->ano][$mes_festivo+0][$dia_festivo+0] = true;
		}
	}
	public function EsFestivo($dia,$mes)
	{
		//echo (int)$mes;
		if($dia=='' or $mes=='')
		{
			return false;
		}

		if (isset($this->festivos[$this->ano][(int)$mes][(int)$dia]))
		{
			return true;
		}
		else
		{
			return FALSE;
		}

	}
}
