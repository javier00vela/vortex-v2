
<?php

/**
 * Licence Manager
 *
 * class responsible for manage Licence
 *
 * @author		Javier J Vela C
 * @version		1.0
 * @since		September of 2018
 */

require_once(Config::PATH . Config::BACKEND . 'modules/LicenceVo.php');

class LicenceManager {
    public $id ;
    public $initLicence ;
    public $endLicence ;
    public $state;

    public function __construct(){
        if(Config::MODE_DEBUG != true){
            $listLicences = $this->ServiceLicencesApi();
            $this->initLicence = (string)$listLicences[0]["fechaInicial"];
            $this->endLicence = (string)$listLicences[0]["fechaFinal"];
            $this->state = (string)$listLicences[0]["estado"];
        }
    }


    public function ServiceLicencesApi(){
        $jsonArray = [];
         $json = file_get_contents("http://arvack.com/licencia/?controller=Licencia&action=servicelicences&key=".Config::KEY_PROYECT);
        foreach (json_decode($json) as $array) {
            $jsonArray[] = (array)$array;
        }
        return $jsonArray;
    }

    public  function ValidateDateLicence() {
        $ableLicence = false;
            if(Config::MODE_DEBUG == false){
              if(  $this->initLicence <= date("o-m-d")   &&  $this->endLicence >= date("o-m-d")  ){
                if($this->state== 1){
                    $ableLicence = true;
                }
              }
            }else{
               $ableLicence = true; 
            }
        return $ableLicence;

    }

     public  function AddDateLicence($init , $end) {
        $licenceVo= new LicenceVo();
         $licenceVo->initDate = $init;
         $licenceVo->endDate = $end;
          $result = $this->generalDao->Set($generalVo = clone $licenceVo);
        if (isset($result)) {
            $message = 'MessageSwalBasic("Renovado!","Licencia Renovada Correctamente","success");';
            $this->utilJQ->AddFunctionJavaScript($message);
        }

    }


    public  function GetAllLicences(){
        $listLicences = array();
        $licenceVo= new LicenceVo();
        $licenceVo->isList = true;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $licenceVo);
        while ($licenceVo2 = $generalDao->GetVo($generalVo = clone $licenceVo)) {
            $listLicences[] = $licenceVo2; 
        }
        return $listLicences;
    }
}