<?php
require_once "config.php";
require_once(Config::PATH . Config::BACKEND . 'general/GeneralVo.php');
require_once(Config::PATH . Config::BACKEND . 'general/GeneralDao.php');
require_once(Config::PATH . Config::BACKEND . 'modules/PersonVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/CompanyVo.php');
require_once(Config::PATH . Config::BACKEND . 'modules/QuotationVo.php');
require_once(Config::PATH . Config::CONTROLLER_LIB .'vendor/phpmailer/phpmailer/src/Exception.php');
require_once(Config::PATH . Config::CONTROLLER_LIB .'vendor/phpmailer/phpmailer/src/PHPMailer.php');
require_once(Config::PATH . Config::CONTROLLER_LIB .'vendor/phpmailer/phpmailer/src/SMTP.php');
require_once(Config::PATH . Config::CONTROLLER . 'general/PHPMailer.php');

class ManageOfferValidityQuotation{
    public $reminderDays = 7;//los dias previos de vencer la cotización
    public $currentDate;//fecha de hoy

    public $dateExpirationQuotation; // fecha de expiracion de la cotizacion

    function __construct($reminderDays = null){
        $this->reminderDays = isset($reminderDays) ? $reminderDays : $this->reminderDays;
        $this->currentDate = date('Y-m-j');
    }

    public function GetDataQuotation()//listamos todas las cotizaciones del momento
    {
        $quotationVo = new QuotationVo();
        $quotationVo->isList = true;
        $generalDao = new GeneralDao();
        $generalDao->GetById($generalVo = clone $quotationVo);
        while ($quotationVo2 = $generalDao->GetVo($generalVo = clone $quotationVo)) {
                $quotationVo2->generationDate = date("Y-m-j");//formato de fecha
                $this->dateExpirationQuotation = $this->GetDateExpirationQuotation($quotationVo2->generationDate, $quotationVo2->offerValidity);
                $dateSendReminder = $this->GetDateSendReminder($this->dateExpirationQuotation);
                if($this->currentDate == $dateSendReminder && $quotationVo2->reminder==0){
                   // echo "se debe enviar el recordatorio de la cotizacion ".$quotationVo2->code;
                    $dataResponsable = $this->GetDataResponsableQuotation($quotationVo2->idUser);
                    $this->SendReminder($dataResponsable,$quotationVo2);
                }
        }
       
    }

    public function GetDateExpirationQuotation($createDate,$timeValidity){//devuelve la fecha en la que la cotización vence
        $dateExpirationQuotation =  strtotime ( '+'.$timeValidity.' day' ,strtotime ($createDate));
        $dateExpirationQuotation = date ( 'Y-m-j' , $dateExpirationQuotation );//aplicamos formato
        return $dateExpirationQuotation;
    }

    public function GetDateSendReminder($dateExpirationQuotation){//devuelve la fecha en la que se debe enviar el recordatorio
        $dateSendReminder = strtotime('-'.$this->reminderDays.' day', strtotime($dateExpirationQuotation));
        $dateSendReminder = date ( 'Y-m-j' , $dateSendReminder );//aplicamos formato
        return $dateSendReminder;
    }
    public function CreateMessageReminder($dataResponsable,$quotationVo,$companyHomeVo){
        $companyQuotation = $this->GetDataCompany($quotationVo->idCompany);
        $file = file_get_contents("../../view/docs/plantillas/vista.html");
        $file = str_replace("#noCotizacion#", $quotationVo->code, $file);
        $file = str_replace("#dateHoy#",  $this->currentDate, $file); 
        $file = str_replace("#nit#", $companyHomeVo->nit, $file);
        $file = str_replace("#phone#", $companyHomeVo->phone, $file);
        $file = str_replace("#email#", $companyHomeVo->email, $file);
        $file = str_replace("#addressCompany#", $companyHomeVo->address, $file);
        $file = str_replace("#company#", $companyQuotation->name, $file); 
        $file = str_replace("#offerValidity#", $quotationVo->offerValidity, $file); 
        $file = str_replace("#placeDelivery#", $quotationVo->placeDelivery, $file);
        $file = str_replace("#payment#", $quotationVo->payment, $file); 
        $file = str_replace("#generationDate#", $quotationVo->generationDate, $file);
        $file = str_replace("#total#", number_format($quotationVo->total), $file);
        $file = str_replace("#money#", $quotationVo->typeMoney, $file);
        $file = str_replace("#dateExpirationQuotation#", $this->dateExpirationQuotation, $file);
        $file = str_replace("#username#", $dataResponsable->names. " ".$dataResponsable->lastNames , $file);
        //$file = str_replace('src="', 'src="cid:', $file); 
        //$file = str_replace('background="', 'background="cid:', $file);
       // $file = str_replace('../../', '', $file);
        
        return $file;
    }
  

    public function SendReminder($dataResponsable,$quotationVo){
        $data = array("mail"=>"yury.bonilla@colombia-cdlo.org","password"=>"Lupe1233910991","from"=>"yury.bonilla@colombia-cdlo.org","port"=>587, "domain"=>"office365");
        $PHPMailer = new PHPMailerUtils($data);
        $PHPMailer->AddDestinatario($dataResponsable->email);
        $PHPMailer->AddImages();
        $message = $this->CreateMessageReminder($dataResponsable,$quotationVo,$this->GetDataCompany(1));
        $mensaje=$PHPMailer->SendDataMail("RECORDATORIO-COTIZACIÓN No ".$quotationVo->code,$message,"VORTEX");
        print_r($mensaje);
       if($mensaje["CODE"]==1){
           $this->UpdateQuotation($quotationVo);
       }
    }

    public function GetDataCompany($id){
        $companyVo = new CompanyVo();
        $generalDao = new GeneralDao();
        $companyVo->id = $id;
        $generalDao->GetById($generalVo = clone $companyVo);

        if($companyVo2 = $generalDao->GetVo($generalVo = clone $companyVo)){
            return  $companyVo2;  
        }
    }

    public function GetDataResponsableQuotation($id){
        $personVo = new PersonVo();
        $generalDao = new GeneralDao();
        $personVo->id = $id;
        $generalDao->GetById($generalVo = clone $personVo);

        if($personVo2 = $generalDao->GetVo($generalVo = clone $personVo)){
            return  $personVo2;  
        }
    }
    public function UpdateQuotation($quotationVo2){
        $quotationVo = new QuotationVo();
        $generalDao = new GeneralDao();
        $quotationVo->id = $quotationVo2->id;
        $quotationVo->idSearchField = 0;
        $quotationVo->idUpdateField = 14;
        $quotationVo->isList = false;
        $quotationVo->reminder = 1;//se cambia el estado a 1 para saber que ya se envio el recordatorio
        $generalDao->UpdateByField($generalVo = clone $quotationVo);
    }

}
/*
$fecha = date('Y-m-j');
echo "fecha hoy ". $fecha. "<br>";
$nuevafecha = strtotime ( '+20 day' , strtotime ( $fecha ) ) ;
$nuevafecha = date ( 'Y-m-j' , $nuevafecha );
 echo "fecha que vence <br>";
echo $nuevafecha;
$fechaCorreo= strtotime('-10 day', strtotime($nuevafecha));
$fechaCorreo = date ( 'Y-m-j' , $fechaCorreo );
echo "fecha d envio de correo ".$fechaCorreo;*/
$obj = new ManageOfferValidityQuotation(10);
$obj->GetDataQuotation();
?>