import { Component, OnInit } from '@angular/core';
import * as XLSX from 'ts-xlsx';
@Component({
  selector: 'app-LoadExcel',
  templateUrl: './loadExcel.component.html',
  styleUrls: ['./loadExcel.component.css']
})
export class LoadExcelComponent implements OnInit {

  imgURL:any;
  message:any;
  public checkInfo = { students : false , work : false };
  public imagePath;
  progress = 0;
  cantEst = 0;
  cantAct = 0;

  constructor() {
     /* set up XMLHttpRequest */
      

  }

  SaveExcelData(files , type){
    if (files.length === 0){
          return;
        }
      var mimeType = files[0].type;
      var reader = new FileReader();
      this.imagePath = files;
      reader.readAsDataURL(files[0]); 
      reader.onload = (_event) => { 
      this.imgURL = reader.result; 
          if (type === "student") {
            this.ReadFileExcel(this.imgURL , "student");
          }else{
            this.ReadFileExcel(this.imgURL , "working");
          }
  }
}

  SetLenghtActivity(data){
     this.cantAct = data.length;
  }

  SetLenghtStudent(data){
     this.cantEst = data.length;
  }

  ReadFileExcel(route , list){
        var callStudent = (data) => {   this.SetLenghtStudent(data);};
        var callActivity = (data) => {   this.SetLenghtActivity(data);};
        var url = route;
        var oReq = new XMLHttpRequest();
        oReq.open("GET", url, true);
        oReq.responseType = "arraybuffer";
        if (list === "student"){
          this.progress = this.progress + 50; 
          this.checkInfo.students = true;
        }else{
          this.progress = this.progress + 50; 
          this.checkInfo.work = true;
        }
        oReq.onload = function(e ) {
         
          var arraybuffer = oReq.response;
          var data = new Uint8Array(arraybuffer);
          var arr = new Array();
          for(var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
          var bstr = arr.join("");
          var workbook = XLSX.read(bstr, {type:"binary"});
          var first_sheet_name = workbook.SheetNames[0];
          var worksheet = workbook.Sheets[first_sheet_name];
            if (list === "student"){
              callStudent(XLSX.utils.sheet_to_json(worksheet,{raw:true}));
              localStorage.setItem("StudentsList",JSON.stringify(XLSX.utils.sheet_to_json(worksheet,{raw:true})));
      
            }else{
              callActivity(XLSX.utils.sheet_to_json(worksheet,{raw:true}));
              localStorage.setItem("WorkList",JSON.stringify(XLSX.utils.sheet_to_json(worksheet,{raw:true})));
            }
        }
        
        oReq.send();
  }

  ngOnInit() {
    console.log();
  }

}
