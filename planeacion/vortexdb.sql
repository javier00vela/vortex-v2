-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 23-09-2019 a las 17:40:44
-- Versión del servidor: 10.3.16-MariaDB
-- Versión de PHP: 7.2.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `vortexdb`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `accesshistory`
--

CREATE TABLE `accesshistory` (
  `id` int(11) NOT NULL,
  `login` datetime NOT NULL DEFAULT current_timestamp(),
  `logout` datetime NOT NULL,
  `numHours` varchar(15) NOT NULL,
  `ipAddress` varchar(30) NOT NULL,
  `device` varchar(20) NOT NULL,
  `browser` varchar(30) NOT NULL,
  `idUser` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `accesshistory`
--

INSERT INTO `accesshistory` (`id`, `login`, `logout`, `numHours`, `ipAddress`, `device`, `browser`, `idUser`) VALUES
(1, '2019-05-08 09:37:03', '2019-05-08 11:30:10', '1h 20min', '190.145.242.208', 'Computador', 'Chrome', 1),
(2, '2019-05-08 10:09:40', '2019-05-08 11:30:10', '1h 20min', '181.61.25.241', 'Computador', 'Chrome', 1),
(3, '2019-05-08 11:43:22', '2019-05-28 07:16:38', '0h 20min', '181.61.25.241', 'Computador', 'Chrome', 1),
(4, '2019-05-08 12:12:30', '2019-05-28 07:16:38', '0h 20min', '190.145.242.208', 'Computador', 'Chrome', 1),
(5, '2019-05-08 13:03:22', '2019-05-28 07:16:38', '0h 20min', '152.202.1.155', 'celular', 'Mobile', 1),
(6, '2019-05-08 14:14:08', '0000-00-00 00:00:00', '0', '190.255.168.62', 'Computador', 'Chrome', 4),
(7, '2019-05-08 15:42:36', '2019-05-28 07:16:38', '0h 20min', '181.61.25.241', 'celular', 'Chrome', 1),
(8, '2019-05-09 08:05:58', '2019-05-28 07:16:38', '0h 20min', '181.61.25.241', 'Computador', 'Chrome', 1),
(9, '2019-05-09 10:04:06', '2019-05-28 07:16:38', '0h 20min', '190.145.242.208', 'Computador', 'Chrome', 1),
(10, '2019-05-09 15:36:49', '2019-05-28 07:16:38', '0h 20min', '190.145.242.208', 'Computador', 'Chrome', 1),
(11, '2019-05-10 09:16:17', '2019-05-28 07:16:38', '0h 20min', '186.155.15.155', 'Computador', 'Chrome', 1),
(12, '2019-05-10 09:28:26', '2019-05-28 07:16:38', '0h 20min', '181.61.25.241', 'Computador', 'Chrome', 1),
(13, '2019-05-10 17:29:49', '0000-00-00 00:00:00', '0', '190.255.168.62', 'Computador', 'Chrome', 4),
(14, '2019-05-13 13:02:23', '2019-05-28 07:16:38', '0h 20min', '181.61.25.241', 'Computador', 'Chrome', 1),
(15, '2019-05-13 13:12:09', '2019-05-28 07:16:38', '0h 20min', '181.61.25.241', 'Computador', 'Chrome', 1),
(16, '2019-05-14 08:34:58', '2019-05-28 07:16:38', '0h 20min', '190.145.240.80', 'Computador', 'Chrome', 1),
(17, '2019-05-14 10:00:35', '2019-05-28 07:16:38', '0h 20min', '181.61.25.241', 'Computador', 'Chrome', 1),
(18, '2019-05-14 10:06:34', '2019-05-28 07:16:38', '0h 20min', '190.24.29.99', 'celular', 'Chrome', 1),
(19, '2019-05-15 09:29:26', '2019-05-28 07:16:38', '0h 20min', '190.145.243.80', 'Computador', 'Chrome', 1),
(20, '2019-05-15 15:38:58', '0000-00-00 00:00:00', '0', '190.255.168.62', 'Computador', 'Chrome', 4),
(21, '2019-05-16 08:42:41', '2019-05-28 07:16:38', '0h 20min', '181.61.25.241', 'Computador', 'Chrome', 1),
(22, '2019-05-16 09:16:03', '2019-05-28 07:16:38', '0h 20min', '190.145.243.80', 'Computador', 'Firefox', 1),
(23, '2019-05-16 09:18:24', '2019-05-28 07:16:38', '0h 20min', '190.145.243.80', 'Computador', 'IE', 1),
(24, '2019-05-16 09:22:44', '2019-05-28 07:16:38', '0h 20min', '181.61.25.241', 'Computador', 'Firefox', 1),
(25, '2019-05-16 10:41:05', '2019-05-28 07:16:38', '0h 20min', '190.145.243.80', 'Computador', 'Chrome', 1),
(26, '2019-05-16 11:07:27', '2019-05-28 07:16:38', '0h 20min', '181.61.25.241', 'Computador', 'Chrome', 1),
(27, '2019-05-16 11:18:04', '0000-00-00 00:00:00', '0', '190.255.168.62', 'Computador', 'Chrome', 4),
(28, '2019-05-17 10:50:16', '2019-05-28 07:16:38', '0h 20min', '190.145.243.16', 'Computador', 'Chrome', 1),
(29, '2019-05-17 10:58:46', '2019-05-28 07:16:38', '0h 20min', '186.155.15.215', 'Computador', 'Chrome', 1),
(30, '2019-05-17 11:08:12', '2019-05-28 07:16:38', '0h 20min', '181.61.25.241', 'Computador', 'Chrome', 1),
(31, '2019-05-20 10:57:34', '2019-05-28 07:16:38', '0h 20min', '179.50.15.34', 'Computador', 'Chrome', 1),
(32, '2019-05-20 11:45:06', '0000-00-00 00:00:00', '0', '186.30.161.146', 'Computador', 'Firefox', 4),
(33, '2019-05-20 16:29:07', '0000-00-00 00:00:00', '0', '190.255.168.62', 'Computador', 'Chrome', 4),
(34, '2019-05-21 06:31:11', '2019-05-28 07:16:38', '0h 20min', '190.145.241.208', 'Computador', 'Chrome', 1),
(35, '2019-05-21 09:27:22', '0000-00-00 00:00:00', '0', '190.255.168.62', 'Computador', 'Chrome', 4),
(36, '2019-05-21 10:11:30', '2019-05-28 07:16:38', '0h 20min', '181.61.25.241', 'Computador', 'Chrome', 1),
(37, '2019-05-21 11:02:28', '2019-05-28 07:16:38', '0h 20min', '190.145.241.208', 'Computador', 'Chrome', 1),
(38, '2019-05-22 08:55:47', '2019-05-28 07:16:38', '0h 20min', '181.61.25.241', 'Computador', 'Chrome', 1),
(39, '2019-05-22 10:11:49', '2019-05-28 07:16:38', '0h 20min', '181.61.25.241', 'Computador', 'Chrome', 1),
(40, '2019-05-22 14:47:07', '0000-00-00 00:00:00', '0', '190.255.168.62', 'Computador', 'Chrome', 4),
(41, '2019-05-23 09:46:23', '0000-00-00 00:00:00', '0', '190.255.168.62', 'Computador', 'Chrome', 4),
(42, '2019-05-23 10:11:52', '2019-05-28 07:16:38', '0h 20min', '181.61.25.241', 'Computador', 'Chrome', 1),
(43, '2019-05-23 15:29:13', '0000-00-00 00:00:00', '0', '190.255.168.62', 'Computador', 'Chrome', 4),
(44, '2019-05-23 16:06:33', '2019-05-28 07:16:38', '0h 20min', '190.145.243.16', 'Computador', 'Chrome', 1),
(45, '2019-05-24 07:37:51', '2019-05-28 07:16:38', '0h 20min', '181.61.25.241', 'Computador', 'Chrome', 1),
(46, '2019-05-26 16:22:57', '2019-05-28 07:16:38', '0h 20min', '181.61.25.241', 'Computador', 'Chrome', 1),
(47, '2019-05-27 06:32:24', '2019-05-28 07:16:38', '0h 20min', '190.145.243.144', 'Computador', 'Chrome', 1),
(48, '2019-05-27 16:54:55', '0000-00-00 00:00:00', '0', '190.255.168.62', 'Computador', 'Chrome', 4),
(49, '2019-05-27 21:56:20', '2019-05-28 07:16:38', '0h 20min', '152.202.80.203', 'celular', 'Mobile', 1),
(50, '2019-05-28 05:08:45', '2019-05-28 07:16:38', '0h 20min', '190.145.243.144', 'Computador', 'Chrome', 1),
(51, '2019-05-28 06:56:11', '2019-05-28 07:16:38', '0h 20min', '190.145.243.144', 'Computador', 'Chrome', 1),
(52, '2019-05-28 07:17:29', '2019-05-28 09:31:16', '2h 13min', '190.145.243.144', 'Computador', 'Chrome', 2),
(53, '2019-05-28 07:27:25', '2019-05-31 15:48:33', '0h 0min', '190.145.243.144', 'Computador', 'Chrome', 1),
(54, '2019-05-28 09:31:21', '2019-05-31 15:48:33', '0h 0min', '190.145.243.144', 'Computador', 'Chrome', 1),
(55, '2019-05-28 09:53:58', '2019-05-31 15:48:33', '0h 0min', '181.61.25.241', 'Computador', 'Chrome', 1),
(56, '2019-05-28 10:44:17', '2019-05-31 15:48:33', '0h 0min', '181.61.25.241', 'Computador', 'Firefox', 1),
(57, '2019-05-28 13:21:46', '2019-05-31 15:48:33', '0h 0min', '190.145.243.144', 'Computador', 'Chrome', 1),
(58, '2019-05-28 16:57:10', '0000-00-00 00:00:00', '0', '190.255.168.62', 'Computador', 'Firefox', 7),
(59, '2019-05-29 06:58:39', '2019-05-31 15:48:33', '0h 0min', '190.145.243.144', 'Computador', 'Chrome', 1),
(60, '2019-05-29 08:21:32', '0000-00-00 00:00:00', '0', '179.14.85.127', 'Computador', 'Firefox', 4),
(61, '2019-05-29 10:03:50', '2019-05-31 15:48:33', '0h 0min', '181.61.25.241', 'Computador', 'Chrome', 1),
(62, '2019-05-29 17:12:18', '2019-05-31 15:48:33', '0h 0min', '181.61.25.241', 'Computador', 'Chrome', 1),
(63, '2019-05-30 05:34:15', '2019-05-31 15:48:33', '0h 0min', '190.145.243.16', 'Computador', 'Chrome', 1),
(64, '2019-05-30 08:03:16', '2019-05-31 15:48:33', '0h 0min', '181.61.25.241', 'Computador', 'Chrome', 1),
(65, '2019-05-30 08:15:05', '2019-05-31 15:48:33', '0h 0min', '190.145.243.16', 'Computador', 'Chrome', 1),
(66, '2019-05-30 08:49:10', '0000-00-00 00:00:00', '0', '179.14.85.127', 'Computador', 'Firefox', 4),
(67, '2019-05-30 12:24:00', '2019-05-31 15:48:33', '0h 0min', '181.61.25.241', 'Computador', 'Chrome', 1),
(68, '2019-05-30 13:21:46', '2019-06-04 05:34:37', '0h 0min', '190.255.168.62', 'Computador', 'Chrome', 5),
(69, '2019-05-30 16:41:54', '0000-00-00 00:00:00', '0', '190.255.168.62', 'Computador', 'Firefox', 7),
(70, '2019-05-30 18:53:08', '2019-05-31 15:48:33', '0h 0min', '179.14.85.127', 'celular', 'Mobile', 1),
(71, '2019-05-30 20:03:10', '2019-05-31 15:48:33', '0h 0min', '179.14.85.127', 'celular', 'Mobile', 1),
(72, '2019-05-30 21:36:16', '0000-00-00 00:00:00', '0', '179.14.85.127', 'Computador', 'Firefox', 4),
(73, '2019-05-31 04:48:23', '2019-05-31 15:48:33', '0h 0min', '190.145.240.80', 'Computador', 'Chrome', 1),
(74, '2019-05-31 07:05:12', '0000-00-00 00:00:00', '0', '179.14.85.127', 'Computador', 'Firefox', 4),
(75, '2019-05-31 07:57:20', '2019-05-31 15:48:33', '0h 0min', '181.61.25.241', 'Computador', 'Chrome', 1),
(76, '2019-05-31 11:51:15', '0000-00-00 00:00:00', '0', '190.255.168.62', 'Computador', 'Firefox', 7),
(77, '2019-05-31 13:08:15', '2019-06-04 05:34:37', '0h 0min', '190.255.168.62', 'Computador', 'Chrome', 5),
(78, '2019-05-31 13:22:55', '2019-06-04 05:34:37', '0h 0min', '190.255.168.62', 'Computador', 'Chrome', 5),
(79, '2019-05-31 15:16:18', '2019-05-31 15:58:21', '0h 9min', '190.255.168.62', 'Computador', 'Chrome', 3),
(80, '2019-05-31 15:48:14', '2019-05-31 15:48:33', '0h 0min', '181.61.25.241', 'Computador', 'Chrome', 1),
(81, '2019-05-31 15:48:39', '2019-05-31 15:58:21', '0h 9min', '181.61.25.241', 'Computador', 'Chrome', 3),
(82, '2019-05-31 15:56:48', '2019-06-04 10:00:04', '4h 27min', '186.155.19.217', 'Computador', 'Chrome', 1),
(83, '2019-05-31 15:58:28', '2019-06-04 10:00:04', '4h 27min', '181.61.25.241', 'Computador', 'Chrome', 1),
(84, '2019-06-01 10:59:36', '0000-00-00 00:00:00', '0', '179.14.85.127', 'Computador', 'Firefox', 4),
(85, '2019-06-02 10:57:03', '2019-06-04 10:00:04', '4h 27min', '181.61.25.241', 'Computador', 'Chrome', 1),
(86, '2019-06-04 05:32:10', '2019-06-04 10:00:04', '4h 27min', '190.145.241.144', 'Computador', 'Chrome', 1),
(87, '2019-06-04 05:33:44', '2019-06-04 05:33:55', '0h 0min', '190.145.241.144', 'Computador', 'Chrome', 3),
(88, '2019-06-04 05:34:31', '2019-06-04 05:34:37', '0h 0min', '190.145.241.144', 'Computador', 'Chrome', 5),
(89, '2019-06-04 05:35:12', '0000-00-00 00:00:00', '0', '190.145.241.144', 'Computador', 'Chrome', 7),
(90, '2019-06-04 07:59:42', '0000-00-00 00:00:00', '0', '179.14.85.127', 'Computador', 'Firefox', 4),
(91, '2019-06-04 08:16:52', '2019-06-04 10:00:04', '4h 27min', '181.61.25.241', 'Computador', 'Chrome', 1),
(92, '2019-06-04 08:33:36', '0000-00-00 00:00:00', '0', '190.255.168.62', 'Computador', 'Chrome', 3),
(93, '2019-06-04 10:00:20', '2019-06-04 20:32:35', '0h 3min', '190.145.241.144', 'Computador', 'Chrome', 1),
(94, '2019-06-04 10:13:04', '2019-06-04 20:32:35', '0h 3min', '181.61.25.241', 'Computador', 'Chrome', 1),
(95, '2019-06-04 14:29:09', '0000-00-00 00:00:00', '0', '190.255.168.62', 'Computador', 'Firefox', 7),
(96, '2019-06-04 20:28:40', '2019-06-04 20:32:35', '0h 3min', '181.48.181.135', 'Computador', 'Firefox', 1),
(97, '2019-06-05 06:09:14', '2019-06-09 17:09:24', '0h 0min', '190.145.243.16', 'Computador', 'Chrome', 1),
(98, '2019-06-05 12:44:06', '0000-00-00 00:00:00', '0', '190.255.168.62', 'Computador', 'Chrome', 5),
(99, '2019-06-06 05:17:11', '2019-06-09 17:09:24', '0h 0min', '190.145.243.16', 'Computador', 'Chrome', 1),
(100, '2019-06-06 06:10:43', '2019-06-09 17:09:24', '0h 0min', '190.145.243.16', 'Computador', 'Chrome', 1),
(101, '2019-06-06 07:14:02', '2019-06-09 17:09:24', '0h 0min', '190.145.243.16', 'Computador', 'Chrome', 1),
(102, '2019-06-06 09:29:03', '0000-00-00 00:00:00', '0', '190.255.168.62', 'Computador', 'Chrome', 4),
(103, '2019-06-06 14:47:52', '0000-00-00 00:00:00', '0', '190.255.168.62', 'Computador', 'Chrome', 3),
(104, '2019-06-06 15:16:15', '0000-00-00 00:00:00', '0', '152.201.204.88', 'Computador', 'Firefox', 4),
(105, '2019-06-07 08:06:35', '2019-06-09 17:09:24', '0h 0min', '190.145.243.16', 'Computador', 'Chrome', 1),
(106, '2019-06-07 08:46:40', '0000-00-00 00:00:00', '0', '190.255.162.179', 'Computador', 'Chrome', 5),
(107, '2019-06-07 09:00:10', '0000-00-00 00:00:00', '0', '190.255.162.179', 'Computador', 'Chrome', 3),
(108, '2019-06-07 09:10:52', '0000-00-00 00:00:00', '0', '190.255.162.179', 'Computador', 'Chrome', 4),
(109, '2019-06-07 09:34:21', '0000-00-00 00:00:00', '0', '190.255.162.179', 'Computador', 'Chrome', 4),
(110, '2019-06-07 13:45:40', '2019-06-09 17:09:24', '0h 0min', '186.154.37.254', 'Computador', 'Chrome', 1),
(111, '2019-06-07 17:19:56', '0000-00-00 00:00:00', '0', '190.255.162.179', 'Computador', 'Chrome', 3),
(112, '2019-06-08 06:37:53', '0000-00-00 00:00:00', '0', '152.202.82.130', 'celular', 'Mobile', 4),
(113, '2019-06-09 12:17:59', '0000-00-00 00:00:00', '0', '38.108.32.98', 'Computador', 'Firefox', 4),
(114, '2019-06-09 16:53:57', '2019-06-09 17:09:24', '0h 0min', '181.61.25.241', 'Computador', 'Chrome', 1),
(115, '2019-06-09 17:09:17', '2019-06-09 17:09:24', '0h 0min', '186.155.17.241', 'Computador', 'Chrome', 1),
(116, '2019-06-10 07:59:24', '2019-06-26 09:15:34', '16h 57min', '190.145.242.208', 'Computador', 'Chrome', 1),
(117, '2019-06-10 08:03:59', '0000-00-00 00:00:00', '0', '190.145.242.208', 'Computador', 'Chrome', 2),
(118, '2019-06-10 08:27:54', '0000-00-00 00:00:00', '0', '191.7.48.45', 'Computador', 'Firefox', 4),
(119, '2019-06-10 09:22:15', '0000-00-00 00:00:00', '0', '191.7.48.45', 'Computador', 'Firefox', 4),
(120, '2019-06-10 09:47:25', '0000-00-00 00:00:00', '0', '190.255.162.179', 'Computador', 'Chrome', 3),
(121, '2019-06-11 07:59:36', '2019-06-26 09:15:34', '16h 57min', '190.145.242.208', 'Computador', 'Chrome', 1),
(122, '2019-06-11 08:33:22', '2019-06-26 09:15:34', '16h 57min', '181.61.25.241', 'Computador', 'Chrome', 1),
(123, '2019-06-11 15:00:56', '0000-00-00 00:00:00', '0', '191.7.48.45', 'celular', 'Mobile', 4),
(124, '2019-06-11 16:38:44', '0000-00-00 00:00:00', '0', '186.84.20.243', 'Computador', 'Chrome', 3),
(125, '2019-06-12 05:17:31', '2019-06-26 09:15:34', '16h 57min', '190.145.242.208', 'Computador', 'Chrome', 1),
(126, '2019-06-12 08:04:22', '2019-06-26 09:15:34', '16h 57min', '190.145.242.208', 'Computador', 'Chrome', 1),
(127, '2019-06-12 09:04:29', '2019-06-26 09:15:34', '16h 57min', '181.61.25.241', 'Computador', 'Chrome', 1),
(128, '2019-06-12 09:52:32', '0000-00-00 00:00:00', '0', '191.7.48.45', 'Computador', 'Firefox', 4),
(129, '2019-06-12 10:04:39', '0000-00-00 00:00:00', '0', '186.84.20.243', 'Computador', 'Chrome', 5),
(130, '2019-06-12 10:58:50', '0000-00-00 00:00:00', '0', '190.145.242.208', 'Computador', 'Chrome', 2),
(131, '2019-06-12 14:24:06', '0000-00-00 00:00:00', '0', '191.7.48.45', 'Computador', 'Firefox', 4),
(132, '2019-06-12 14:38:11', '2019-06-26 09:15:34', '16h 57min', '190.145.242.208', 'Computador', 'Chrome', 1),
(133, '2019-06-13 06:10:47', '2019-06-26 09:15:34', '16h 57min', '190.145.242.208', 'Computador', 'Chrome', 1),
(134, '2019-06-13 08:04:34', '2019-06-26 09:15:34', '16h 57min', '190.145.242.208', 'Computador', 'Chrome', 1),
(135, '2019-06-13 08:44:23', '0000-00-00 00:00:00', '0', '186.84.20.243', 'Computador', 'Chrome', 5),
(136, '2019-06-13 12:39:16', '0000-00-00 00:00:00', '0', '186.84.20.243', 'Computador', 'Chrome', 5),
(137, '2019-06-13 12:39:22', '2019-06-26 09:15:34', '16h 57min', '190.24.59.202', 'Computador', 'Chrome', 1),
(138, '2019-06-13 14:02:47', '0000-00-00 00:00:00', '0', '186.84.20.243', 'Computador', 'Chrome', 4),
(139, '2019-06-13 15:03:47', '0000-00-00 00:00:00', '0', '186.84.20.243', 'Computador', 'Chrome', 3),
(140, '2019-06-14 08:05:10', '2019-06-26 09:15:34', '16h 57min', '190.145.242.208', 'Computador', 'Chrome', 1),
(141, '2019-06-14 16:21:55', '0000-00-00 00:00:00', '0', '186.84.20.243', 'Computador', 'Chrome', 5),
(142, '2019-06-14 17:50:21', '0000-00-00 00:00:00', '0', '186.84.20.243', 'Computador', 'Chrome', 3),
(143, '2019-06-17 04:49:48', '2019-06-26 09:15:34', '16h 57min', '190.145.241.80', 'Computador', 'Chrome', 1),
(144, '2019-06-17 08:22:29', '0000-00-00 00:00:00', '0', '186.84.20.243', 'Computador', 'Chrome', 3),
(145, '2019-06-17 09:44:04', '2019-06-26 09:15:34', '16h 57min', '181.61.25.241', 'Computador', 'Chrome', 1),
(146, '2019-06-18 05:20:38', '2019-06-26 09:15:34', '16h 57min', '190.145.241.80', 'Computador', 'Chrome', 1),
(147, '2019-06-18 08:47:11', '0000-00-00 00:00:00', '0', '50.248.109.65', 'Computador', 'Firefox', 4),
(148, '2019-06-18 09:27:35', '0000-00-00 00:00:00', '0', '186.84.20.243', 'Computador', 'Chrome', 5),
(149, '2019-06-18 13:27:33', '2019-06-26 09:15:34', '16h 57min', '181.61.25.241', 'Computador', 'Chrome', 1),
(150, '2019-06-18 16:02:54', '0000-00-00 00:00:00', '0', '186.84.20.243', 'Computador', 'Chrome', 3),
(151, '2019-06-18 20:26:09', '0000-00-00 00:00:00', '0', '191.98.71.244', 'Computador', 'Firefox', 7),
(152, '2019-06-19 05:05:00', '2019-06-26 09:15:34', '16h 57min', '190.145.241.80', 'Computador', 'Chrome', 1),
(153, '2019-06-19 14:37:55', '0000-00-00 00:00:00', '0', '186.84.20.243', 'Computador', 'Chrome', 3),
(154, '2019-06-19 14:38:11', '2019-06-26 09:15:34', '16h 57min', '181.61.25.241', 'Computador', 'Chrome', 1),
(155, '2019-06-20 14:42:06', '0000-00-00 00:00:00', '0', '186.84.20.243', 'Computador', 'Chrome', 5),
(156, '2019-06-21 07:56:19', '2019-06-26 09:15:34', '16h 57min', '190.145.241.80', 'Computador', 'Chrome', 1),
(157, '2019-06-21 09:11:24', '0000-00-00 00:00:00', '0', '186.84.20.243', 'Computador', 'Chrome', 3),
(158, '2019-06-21 09:25:38', '2019-06-26 09:15:34', '16h 57min', '181.61.25.241', 'Computador', 'Chrome', 1),
(159, '2019-06-21 10:51:34', '0000-00-00 00:00:00', '0', '186.84.20.243', 'Computador', 'Chrome', 4),
(160, '2019-06-21 14:49:48', '0000-00-00 00:00:00', '0', '186.84.20.243', 'Computador', 'Chrome', 5),
(161, '2019-06-21 16:06:17', '2019-06-26 09:15:34', '16h 57min', '181.61.25.241', 'Computador', 'Chrome', 1),
(162, '2019-06-25 10:07:42', '0000-00-00 00:00:00', '0', '186.84.20.243', 'Computador', 'Chrome', 3),
(163, '2019-06-25 11:06:46', '0000-00-00 00:00:00', '0', '186.84.20.243', 'Computador', 'Chrome', 5),
(164, '2019-06-25 13:24:45', '2019-06-26 09:15:34', '16h 57min', '181.61.25.241', 'Computador', 'Chrome', 1),
(165, '2019-06-25 16:17:53', '2019-06-26 09:15:34', '16h 57min', '181.61.25.241', 'Computador', 'Chrome', 1),
(166, '2019-06-26 07:37:45', '0000-00-00 00:00:00', '0', '186.84.20.243', 'Computador', 'Chrome', 4),
(167, '2019-06-26 08:07:00', '2019-06-26 09:15:34', '16h 57min', '190.145.243.16', 'Computador', 'Chrome', 1),
(168, '2019-06-26 10:38:06', '0000-00-00 00:00:00', '0', '186.84.20.243', 'Computador', 'Chrome', 3),
(169, '2019-06-26 11:34:55', '2019-06-29 12:01:17', '0h 59min', '181.61.25.241', 'Computador', 'Chrome', 1),
(170, '2019-06-26 16:05:54', '0000-00-00 00:00:00', '0', '190.255.164.132', 'Computador', 'Firefox', 7),
(171, '2019-06-27 07:15:02', '0000-00-00 00:00:00', '0', '181.236.146.33', 'celular', 'Mobile', 4),
(172, '2019-06-27 08:34:16', '0000-00-00 00:00:00', '0', '186.84.20.243', 'Computador', 'Chrome', 5),
(173, '2019-06-27 16:08:10', '2019-06-29 12:01:17', '0h 59min', '190.145.243.16', 'Computador', 'Chrome', 1),
(174, '2019-06-27 17:26:48', '0000-00-00 00:00:00', '0', '186.84.20.243', 'Computador', 'Chrome', 4),
(175, '2019-06-27 23:06:19', '0000-00-00 00:00:00', '0', '181.236.146.33', 'celular', 'Mobile', 4),
(176, '2019-06-28 10:02:59', '0000-00-00 00:00:00', '0', '186.84.20.243', 'Computador', 'Chrome', 5),
(177, '2019-06-28 10:13:27', '0000-00-00 00:00:00', '0', '186.84.20.243', 'Computador', 'Chrome', 4),
(178, '2019-06-28 15:15:53', '2019-06-29 12:01:17', '0h 59min', '186.155.14.197', 'Computador', 'Chrome', 1),
(179, '2019-06-29 11:01:41', '2019-06-29 12:01:17', '0h 59min', '181.61.25.241', 'Computador', 'Chrome', 1),
(180, '2019-06-29 11:11:39', '2019-06-29 12:01:17', '0h 59min', '181.48.181.135', 'Computador', 'Chrome', 1),
(181, '2019-06-29 11:13:40', '2019-06-29 12:01:17', '0h 59min', '190.217.108.76', 'celular', 'Chrome', 1),
(182, '2019-06-29 12:01:22', '2019-06-29 12:31:44', '1h 20min', '181.61.25.241', 'Computador', 'Chrome', 1),
(183, '2019-07-01 11:19:00', '0000-00-00 00:00:00', '0', '181.61.25.241', 'Computador', 'Chrome', 1),
(184, '2019-08-07 22:11:38', '0000-00-00 00:00:00', '0', '::1', 'Computador', 'Chrome', 1),
(185, '2019-08-09 07:54:00', '0000-00-00 00:00:00', '0', '::1', 'Computador', 'Chrome', 1),
(186, '2019-08-09 11:03:53', '0000-00-00 00:00:00', '0', '::1', 'Computador', 'Chrome', 1),
(187, '2019-08-09 21:11:58', '0000-00-00 00:00:00', '0', '::1', 'Computador', 'Chrome', 1),
(188, '2019-08-11 09:32:30', '0000-00-00 00:00:00', '0', '::1', 'Computador', 'Chrome', 1),
(189, '2019-08-13 09:14:54', '0000-00-00 00:00:00', '0', '::1', 'Computador', 'Chrome', 1),
(190, '2019-08-15 10:55:00', '0000-00-00 00:00:00', '0', '::1', 'Computador', 'Chrome', 1),
(191, '2019-08-18 16:28:52', '0000-00-00 00:00:00', '0', '::1', 'Computador', 'Chrome', 1),
(192, '2019-08-21 08:43:26', '0000-00-00 00:00:00', '0', '::1', 'Computador', 'Chrome', 1),
(193, '2019-08-22 13:42:05', '0000-00-00 00:00:00', '0', '::1', 'Computador', 'Chrome', 1),
(194, '2019-08-25 11:12:35', '0000-00-00 00:00:00', '0', '::1', 'Computador', 'Chrome', 1),
(195, '2019-08-25 17:00:08', '0000-00-00 00:00:00', '0', '::1', 'Computador', 'Chrome', 1),
(196, '2019-08-27 09:13:41', '0000-00-00 00:00:00', '0', '::1', 'Computador', 'Chrome', 1),
(197, '2019-08-28 17:15:40', '0000-00-00 00:00:00', '0', '::1', 'Computador', 'Chrome', 1),
(198, '2019-08-28 19:47:12', '0000-00-00 00:00:00', '0', '::1', 'Computador', 'Chrome', 1),
(199, '2019-08-29 10:08:37', '0000-00-00 00:00:00', '0', '::1', 'Computador', 'Chrome', 1),
(200, '2019-08-29 11:30:30', '0000-00-00 00:00:00', '0', '::1', 'Computador', 'Chrome', 1),
(201, '2019-08-29 14:25:42', '0000-00-00 00:00:00', '0', '::1', 'Computador', 'Chrome', 1),
(202, '2019-08-30 13:46:58', '0000-00-00 00:00:00', '0', '::1', 'Computador', 'Chrome', 1),
(203, '2019-08-30 14:50:44', '0000-00-00 00:00:00', '0', '::1', 'Computador', 'Chrome', 1),
(204, '2019-09-02 13:44:25', '0000-00-00 00:00:00', '0', '::1', 'Computador', 'Chrome', 1),
(205, '2019-09-02 13:47:59', '0000-00-00 00:00:00', '0', '::1', 'Computador', 'Chrome', 1),
(206, '2019-09-05 13:39:29', '0000-00-00 00:00:00', '0', '::1', 'Computador', 'Chrome', 1),
(207, '2019-09-05 17:23:30', '0000-00-00 00:00:00', '0', '::1', 'Computador', 'Chrome', 1),
(208, '2019-09-09 09:05:00', '0000-00-00 00:00:00', '0', '::1', 'Computador', 'Chrome', 1),
(209, '2019-09-10 09:05:43', '0000-00-00 00:00:00', '0', '::1', 'Computador', 'Chrome', 1),
(210, '2019-09-10 13:46:57', '0000-00-00 00:00:00', '0', '::1', 'Computador', 'Chrome', 1),
(211, '2019-09-11 11:27:09', '0000-00-00 00:00:00', '0', '::1', 'Computador', 'Chrome', 1),
(212, '2019-09-12 09:12:13', '0000-00-00 00:00:00', '0', '::1', 'Computador', 'Chrome', 1),
(213, '2019-09-15 12:32:57', '0000-00-00 00:00:00', '0', '::1', 'Computador', 'Chrome', 1),
(214, '2019-09-20 07:30:01', '0000-00-00 00:00:00', '0', '::1', 'Computador', 'Chrome', 1),
(215, '2019-09-21 14:00:22', '0000-00-00 00:00:00', '0', '::1', 'Computador', 'Chrome', 1),
(216, '2019-09-21 16:13:27', '0000-00-00 00:00:00', '0', '::1', 'Computador', 'Chrome', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `aditionalsimposts`
--

CREATE TABLE `aditionalsimposts` (
  `id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `percent` varchar(50) DEFAULT NULL,
  `idPricesCompany` int(11) DEFAULT NULL,
  `CIF` int(11) DEFAULT NULL,
  `FCA` int(11) DEFAULT NULL,
  `DDP` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `aditionalsimposts`
--

INSERT INTO `aditionalsimposts` (`id`, `name`, `percent`, `idPricesCompany`, `CIF`, `FCA`, `DDP`) VALUES
(0, NULL, NULL, 0, 0, 0, 0),
(1, '', '', 1, 0, 0, 0),
(2, '', '', 2, 0, 0, 0),
(3, '', '', 3, 0, 0, 0),
(4, '', '', 4, 0, 0, 0),
(5, '', '', 5, 0, 0, 0),
(6, '', '', 6, 0, 0, 0),
(7, '', '', 7, 0, 0, 0),
(8, '', '', 8, 0, 0, 0),
(9, '', '', 9, 0, 0, 0),
(10, '', '', 10, 0, 0, 0),
(11, '', '', 11, 0, 0, 0),
(12, '', '', 12, 0, 0, 0),
(13, '', '', 13, 0, 0, 0),
(14, '', '', 14, 0, 0, 0),
(15, '', '', 15, 0, 0, 0),
(16, '', '', 16, 0, 0, 0),
(17, '', '', 17, 0, 0, 0),
(18, '', '', 18, 0, 0, 0),
(19, '', '', 19, 0, 0, 0),
(20, '', '', 20, 0, 0, 0),
(21, '', '', 21, 0, 0, 0),
(22, '', '', 22, 0, 0, 0),
(23, '', '', 23, 0, 0, 0),
(24, '', '', 24, 0, 0, 0),
(25, '', '', 25, 0, 0, 0),
(26, '', '', 26, 0, 0, 0),
(27, '', '', 27, 0, 0, 0),
(28, '', '', 28, 0, 0, 0),
(29, '', '', 29, 0, 0, 0),
(30, '', '', 30, 0, 0, 0),
(32, 'Diferencia al cambio', '1', 1, 0, 0, 1),
(33, 'Flete', '70', 1, 1, 0, 1),
(34, 'Seguro', '0.75', 1, 1, 0, 1),
(35, 'Bodega', '0.5', 1, 0, 0, 1),
(36, 'SIA', '0.75', 1, 0, 0, 1),
(37, 'ComercioExt', '0.5', 1, 0, 0, 1),
(38, '', '', 31, 0, 0, 0),
(39, '', '', 32, 0, 0, 0),
(40, '', '', 33, 0, 0, 0),
(41, '', '', 34, 0, 0, 0),
(42, '', '', 35, 0, 0, 0),
(43, '', '', 36, 0, 0, 0),
(45, 'Comericio Ext', '0,5', 32, 1, 0, 1),
(46, 'Diferencia al Cambio', '1', 32, 0, 0, 1),
(47, 'Flete', '5', 32, 1, 0, 1),
(48, 'Seguro', '0,35', 32, 1, 0, 1),
(49, 'SIA', '0,35', 32, 0, 0, 1),
(50, 'Bodega', '0,5', 32, 0, 0, 1),
(51, '', '', 37, 0, 0, 0),
(52, '', '', 38, 0, 0, 0),
(53, '', '', 39, 0, 0, 0),
(54, '', '', 40, 0, 0, 0),
(55, '', '', 41, 0, 0, 0),
(56, '', '', 42, 0, 0, 0),
(57, '', '', 43, 0, 0, 0),
(58, '', '', 44, 0, 0, 0),
(59, '', '', 45, 0, 0, 0),
(60, '', '', 46, 0, 0, 0),
(62, '', '', 47, 0, 0, 0),
(64, '', '', 49, 0, 0, 0),
(65, '', '', 50, 0, 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alert`
--

CREATE TABLE `alert` (
  `id` int(11) NOT NULL,
  `tittle` varchar(50) NOT NULL,
  `descrition` text NOT NULL,
  `idEvent` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `alert`
--

INSERT INTO `alert` (`id`, `tittle`, `descrition`, `idEvent`) VALUES
(2, 'correo de visitas', 'se debe realizar una visita al cliente <b>#cliente</b> el dia de hoy \'#fecha\'.', 4),
(3, 'correos de cotizaciones enviadas', 'correos de cotizaciones enviadas', 5),
(4, 'Correos de seguimiento de prospecto', 'Se debe realizar seguimiento del prospecto  <b>#prospecto</b>  de la empresa \'#empresa\'.', 6);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientuser`
--

CREATE TABLE `clientuser` (
  `id` int(11) NOT NULL,
  `idClient` int(11) NOT NULL DEFAULT 0,
  `idUser` int(11) NOT NULL DEFAULT 0,
  `prefijo` varchar(20) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `clientuser`
--

INSERT INTO `clientuser` (`id`, `idClient`, `idUser`, `prefijo`) VALUES
(1, 10, 4, 'Ingeniero'),
(2, 11, 3, 'SeÃ±or'),
(3, 12, 3, 'SeÃ±or'),
(4, 13, 6, 'Ingeniera'),
(5, 14, 2, 'Ingeniero'),
(6, 15, 2, 'Ingeniero'),
(7, 16, 5, 'Doctor'),
(8, 17, 3, 'SeÃ±or'),
(9, 18, 3, 'SeÃ±ora'),
(10, 19, 3, 'Ingeniera'),
(11, 20, 5, 'SeÃ±ora'),
(12, 21, 5, 'Doctor'),
(13, 22, 3, 'SeÃ±ora'),
(14, 23, 3, 'SeÃ±ora'),
(15, 24, 3, 'SeÃ±ora'),
(16, 25, 3, 'SeÃ±or'),
(17, 26, 3, 'Ingeniera'),
(18, 27, 3, 'SeÃ±or'),
(19, 28, 3, 'Ingeniera'),
(20, 29, 3, 'Ingeniero'),
(21, 30, 3, 'SeÃ±ora'),
(22, 31, 3, 'Ingeniero'),
(23, 32, 3, 'SeÃ±or'),
(24, 33, 3, 'Ingeniero'),
(25, 34, 3, 'Ingeniero'),
(26, 35, 3, 'Ingeniero'),
(27, 36, 3, 'SeÃ±or'),
(28, 37, 3, 'SeÃ±or'),
(29, 38, 3, 'Ingeniera'),
(30, 39, 7, 'Doctor'),
(31, 40, 3, 'Doctora'),
(32, 41, 5, 'Doctor'),
(33, 42, 5, 'Doctora'),
(34, 43, 5, 'Doctor'),
(35, 44, 3, 'Ingeniera'),
(36, 45, 5, 'Doctor'),
(37, 46, 3, 'Doctor'),
(38, 47, 4, 'Ingeniero'),
(39, 48, 7, 'Doctor'),
(40, 49, 3, 'SeÃ±ora'),
(41, 54, 3, 'Doctora'),
(42, 56, 4, 'Doctora'),
(43, 57, 1, 'SeÃ±or');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `company`
--

CREATE TABLE `company` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `nit` varchar(45) NOT NULL,
  `phone` varchar(30) NOT NULL,
  `email` varchar(100) NOT NULL,
  `webSite` varchar(100) NOT NULL,
  `address` varchar(45) NOT NULL,
  `country` varchar(60) NOT NULL,
  `city` varchar(60) NOT NULL,
  `creationDate` datetime NOT NULL,
  `state` tinyint(1) NOT NULL,
  `idRoleCompany` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `company`
--

INSERT INTO `company` (`id`, `name`, `nit`, `phone`, `email`, `webSite`, `address`, `country`, `city`, `creationDate`, `state`, `idRoleCompany`) VALUES
(0, 'externo', 'hjjb', 'jkbjh', 'jkhjkh', 'jkhh', 'jkh', 'Afghanistan', 'Kabul', '2019-08-13 11:50:53', 1, 2),
(1, 'Vortex Company SAS', '900130648-4', '5275224', 'vortex@vortexcompany.co', 'www.vortexcompany.co', 'Cl 173 49 b 61', 'Colombia', 'Bogota', '2019-05-08 10:50:04', 1, 1),
(2, 'proveedor', '786', '76876', '76786', '8767', '678678', 'Aruba', 'Kabul', '2019-08-13 11:50:37', 1, 3),
(3, 'cliente', 'hjjb', 'jkbjh', 'jkhjkh', 'jkhh', 'jkh', 'Afghanistan', 'Kabul', '2019-08-13 11:50:53', 1, 2),
(5, 'tertret', 'ertert', 'erert', 'ertert', 'ertter', 'tretert', 'Puerto Rico', 'Qandahar', '2019-08-27 10:42:17', 1, 2),
(6, 'aqui company', '3242347866', '876768', '78876', '8767867', '687687', 'Afghanistan', 'Qandahar', '2019-08-27 10:47:26', 1, 2),
(7, 'gfdg', 'fdgdfg', 'dgdfgdfg', 'gfdg', 'gdfgfd', 'gfdg', 'Afghanistan', 'Qandahar', '2019-08-27 10:48:19', 1, 2),
(8, 'gfdg', 'fdgdfg', 'dgdfgdfg', 'gfdg', 'gdfgfd', 'gfdg', 'Afghanistan', 'Qandahar', '2019-08-27 10:48:54', 1, 2),
(9, 'fdgg', 'fdgf', 'gdfgdf', 'fdgdfg', 'dfgdfg', 'gfdfg', 'Afghanistan', 'Qandahar', '2019-08-27 10:49:18', 1, 1),
(10, 'fgfd', 'gdfg', 'gfgdfg', 'fdgdfg', 'dfg', 'dfgdfg', 'Afghanistan', 'Qandahar', '2019-08-27 10:49:38', 1, 2),
(11, 'fgfd', 'gdfg', 'gfgdfg', 'fdgdfg', 'dfg', 'dfgdfg', 'Afghanistan', 'Qandahar', '2019-08-27 10:49:59', 1, 2),
(12, 'fdgdfg', 'dfgdf', 'gdfgdfg', 'fdgdfg', 'dfgdfg', 'gffg', 'Angola', 'Kabul', '2019-08-27 10:51:00', 1, 2),
(13, 'un proveedor x', '1212', '213123', '23123', '123123', '123213', 'Afghanistan', 'Herat', '2019-09-21 15:04:45', 1, 3),
(14, 'a la versh', '43435', '78789', '8977', '878', '9878', 'Afghanistan', 'Qandahar', '2019-09-21 15:05:04', 1, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `diagnosis`
--

CREATE TABLE `diagnosis` (
  `id` int(11) NOT NULL,
  `commentClient` varchar(1000) DEFAULT NULL,
  `machineCommentState` longblob DEFAULT NULL,
  `machineObservation` longblob DEFAULT NULL,
  `firmClient` text DEFAULT NULL,
  `firmMaintenance` text DEFAULT NULL,
  `idProduct` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `idStatusMachine` int(11) NOT NULL,
  `noteGeneral` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `diagnosis`
--

INSERT INTO `diagnosis` (`id`, `commentClient`, `machineCommentState`, `machineObservation`, `firmClient`, `firmMaintenance`, `idProduct`, `dateCreated`, `idStatusMachine`, `noteGeneral`) VALUES
(1, '<p>trhrthtrtyhyhyh</p>', 0x3c703e7479687479687974683c2f703e, 0x3c703e676666686766683c2f703e, 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAkMAAAGNCAYAAAASM3XCAAAgAElEQVR4Xu3d26ttVR0H8HlKT95Ss0SzoEOFFKUUEREmUYE91EP0VBAU9BaBENVDPtdDgj74YFH/QEUEkg8aFRERUVB0oZAu0kuWpXn3HPfZJ8bOeZh7nXWZa17HmL/PhqByXsbv8xt7+d1zjjnXiXPnzp2r/BAgQIAAAQIEggqcEIaCdl7ZBAgQIECAwJGAMGQiECBAgAABAqEFhKHQ7Vc8AQIECBAgIAyZAwQIECBAgEBoAWEodPsVT4AAAQIECAhD5gABAgQIECAQWkAYCt1+xRMgQIAAAQLCkDlAgAABAgQIhBYQhkK3X/EECBAgQICAMGQOECBAgAABAqEFhKHQ7Vc8AQIECBAgIAyZAwQIECBAgEBoAWEodPsVT4AAAQIECAhD5gABAgQIECAQWkAYCt1+xRMgQIAAAQLCkDlAgAABAgQIhBYQhkK3X/EECBAgQICAMGQOECBAgAABAqEFhKHQ7Vc8AQIECBAgIAyZAwQIECBAgEBoAWEodPsVT4AAAQIECAhD5gABAgQIECAQWkAYCt1+xRMgQIAAAQLCkDlAgAABAgQIhBYQhkK3X/EECBAgQICAMGQOECBAgAABAqEFhKHQ7Vc8AQIECBAgIAyZAwQIECBAgEBoAWEodPsVT4AAAQIECAhD5gABAgQIECAQWkAYCt1+xRMgQIAAAQLCkDlAgAABAgQIhBYQhkK3X/EECBAgQICAMGQOECBAgAABAqEFhKHQ7Vc8AQIECBAgIAyZAwQIECBAgEBoAWEodPsVT4AAAQIECAhD5gABAgQIECAQWkAYCt1+xRMgQIAAAQLCkDlAgAABAgQIhBYQhkK3X/EECBAgQICAMGQOECBAgAABAqEFhKHQ7Vc8AQIECBAgIAyZAwQIECBAgEBoAWEodPsVT4AAAQIECAhD5gABAgQIECAQWkAYCt1+xRMgQIAAAQLCkDlAgAABAgQIhBYQhkK3X/EECBAgQICAMGQOECBAgAABAqEFhKHQ7Vc8AQIECBAgIAyZAwQIECBAgEBoAWEodPsVT4AAAQIECAhD5gABAgQIECAQWkAYCt1+xRMgQIAAAQLCkDlAgAABAgQIhBYQhkK3X/EECBAgQICAMGQOECBAgAABAqEFhKHQ7Vc8AQIECBAgIAyZAwQIECBAgEBoAWEodPsVT4AAAQIECAhD5gABAgQIECAQWkAYCt1+xRMgQIAAAQLCkDlAgAABAgQIhBYQhkK3X/EECBAgQICAMGQOECBAgAABAqEFhKHQ7Vc8AQIECBAgIAyZAwQIECBAgEBoAWEodPsVT4AAAQIECAhD5gABAgQIECAQWkAYCt1+xRMgQIAAAQLCkDlAgAABAgQIhBYQhkK3X/EECBAgQICAMGQOECBAgAABAqEFhKHQ7Vc8AQIECBAgIAyZAwQIECBAgEBoAWEodPsVT4AAAQIECAhD5gABAgQIECAQWkAYCt1+xRMgQIAAAQLCkDlAgAABAgQIhBYQhkK3X/EECBAgQICAMGQOECBAgAABAqEFhKHQ7Vc8AQIECBAgIAyZAwQIECBAgEBoAWEodPsVT4AAAQIECAhD5gABAgQIECAQWkAYCt1+xRMgQIAAAQLCkDlAgAABAgQIhBYQhkK3X/EECBAgQICAMGQOECBAgAABAqEFhKHQ7Vc8AQIECBAgIAyZAwQIECBAgEBoAWEodPsVT4AAAQIECAhD5gABAgQIECAQWkAYCt1+xRMgQIAAAQLCkDlAgAABAgQIhBYQhkK3X/EECBAgQICAMGQOECBAgAABAqEFhKHQ7Vc8AQIECBAgIAyZAwQIECBAgEBoAWEodPsVT4AAAQIECAhD5gABAgQIECAQWkAYCt1+xRMgQIAAAQLCkDlAgAABAgQIhBYQhkK3X/EECBAgQICAMGQOECBAgAABAqEFhKHQ7Vc8AQIECBAgIAyZAwQIECBAgEBoAWEodPsVT4AAAQIECAhD5gABAgQIECAQWkAYCt1+xRMgQIAAAQLCkDlAgAABAgQIhBYQhkK3X/EECBAgQICAMGQOECBAgAABAqEFhKHQ7Vc8AQIECBAgIAyZAwQIECBAgEBoAWEodPsVT4AAAQIECAhD5gABAgQIECAQWkAYCt1+xRMgQIAAAQLCkDlAgAABAgQIhBYQhkK3X/EECBAgQICAMGQOECBAgAABAqEFhKHQ7Vc8AQIECBAgIAyZAwQIECBAgEBoAWEodPsVT4AAAQIECAhD5gABAgQIECAQWkAYCt1+xRMgQIAAAQLCkDlAgAABAgQIhBYQhkK3X/EECBAgQICAMGQOECBAgAABAqEFhKHQ7Vc8AQIECBAgIAyZAwQIECBAgEBoAWEodPsVT4AAAQIECAhD5gABAgQIECAQWkAYCt1+xRMgQIAAAQLCkDlAgAABAgQIhBYQhkK3X/EECBAgQICAMGQOECBAgAABAqEFhKHQ7Vc8AQIECBAgIAyZAwQIECBAgEBoAWEodPsVT4AAAQIECAhD5gABAgQIECAQWkAYCt1+xRMgQIAAAQLCkDlAgAABAgQIhBYQhkK3X/EECBAgQICAMGQOECBAgAABAqEFhKHQ7Vc8AQIECBAgIAyZAwQIECBAgEBoAWEodPsVT4AAAQIECAhD5gABAgQIECAQWkAYCt1+xRMgQIAAAQLCkDlAgAABAgQIhBYQhkK3X/EECBAgQICAMGQOECBAgAABAqEFhKHQ7Vc8AQIECBAgIAyZAwQIECBAgEBoAWEodPsVT4AAAQIECAhD5gABAgQIECAQWkAYCt1+xRMgQIAAAQLCkDlAgAABAgQIhBYQhkK3X/EECBAgQICAMGQOECBAgAABAqEFhKHQ7Vc8AQIECBAgIAyZAwQIECBAgEBoAWEodPsVT4AAAQIECAhD5gABAgQIECAQWkAYCt1+xRMgQIAAAQLCkDlAgAABAgQIhBYQhkK3X/EECBAgQICAMGQOECBAgAABAqEFhKHQ7Vc8AQIECBAgIAyZAwQIECBAgEBoAWEodPsVT4AAAQIECAhD5gABAgQIECAQWkAYCt1+xRMgQIAAAQLCkDlAgAABAgQIhBYQhkK3X/EECBAgQICAMGQOECBAgAABAqEFhKHQ7Vc8AQIECBAgIAyZAwQIECBAgEBoAWEodPsVT4AAAQIECAhD5gABAgQIECAQWkAYCt1+xRMgQIAAAQLCkDlAgAABAgQIhBYQhkK3X/EECBAgQICAMGQOECBAgAABAqEFhKHQ7Vc8AQIECBAgIAyZAwQIECBAgEBoAWEodPsVT4AAAQIECAhD5gABAgQIECAQWkAYCt1+xRMgQIAAAQLCkDlAgAABAgQIhBYQhkK3X/EECBAgQICAMGQOECBAgAABAqEFhKHQ7Vc8AQIECBAgIAyZAwQIECBAgEBoAWEodPsVT4AAAQIECAhD5gABAgQIECAQWkAYCt1+xRMgQIAAAQLCkDlAgAABAgQIhBYQhkK3X/EECBAgQICAMGQOECBAgAABAqEFhKHQ7Vc8AQIECBAgIAyZAwQIECBAgEBoAWEodPsVT4AAAQIECAhD5gABAgQIECAQWkAYCt1+xRMgQIAAAQLCkDlAgAABAgQIhBYQhkK3X/EEyhd46UtfWp07d+7oP21+2m7X5li2IUBgGQLC0DL6qAoCixP49re/XX384x8/H3KGCjFDHWdx4AoiEFhAGArcfKUTmFvgkksuqc6cOXM0jCFDyokTJ86X9o1vfKP69Kc/PXepzk+AQMYCwlDGzTE0AqULPP7449WrX/3q0QLPS17ykurg4KB0JuMnMKrALbfcUv385z8/do59/vhIf1y8853vrH7xi1+MOs45Dy4Mzanv3AQWKPDII49UN9xww6BXehJT+kBO4ee5556rLr744gXKKYlAP4H3vve91U9/+tPBf/eao0q/h+k/Z8+e7TfYzPYWhjJriOEQKFGgvt3V5q/N+hbWyZMnq7/97W/Va17zmrUf3nX4efLJJ6vLLrusRBZjJjCqQPrjIP20+b3bNpDmbeW03bvf/e7qZz/7WbXt4YT0O/nMM8+MWt+UBxeGptR2LgILErjooouqw8PDrR/E6cN601+Qaf/Vf5Y+lP/+979Xr33taxckpRQC/QX2fWoy/S5dccUVVfpjYoifd73rXdUvf/nLY7/vfUPYEOMa6hjC0FCSjkMggED6QE4BaNNPfTVn2zqedBXp9OnTFxziZS97WfX8888HUFQige0CV155ZfX000+3vuIzxa2r1d/9d7zjHdWvfvWrxbRSGFpMKxVCYDiBfZ7ySh/E6ZbXriCTrgKlq0GrP+lD1iLo4XrnSOUKpCupu6621Le0tv1RMrTA6riWFoSSlzA09KxxPAIFCewTepplpQ/kfT6M133Ib7uFVhChoRLoJfCBD3yg+vGPf7wxBKXftVtvvbX6yU9+0us8XXZ++9vfXv3mN785tuvb3va26te//nWXw2W9jzCUdXsMjsBugW9961vVJz/5yfOPr9d77PoLc/eR/79Fc8FzelT+0ksvbbvr0dNfq+PYN0i1PpkNCRQkkELQj370owtGnMvvx+rvbi7jGqvFwtBYso5LYCCB73znO9XHPvaxwd/EvDq8ZujZdctrV2mbFkc/+uij1Stf+cpdu/vnBBYrkHsIinQ1qDnJhKHF/soprCSBrrer2ta4+uhsWuPzzW9+s/rEJz7R9hCttrM4uhWTjQIKXH311dUTTzyR7ZWgNLBoV4OEoYC/iEqeX+Dee++tPvvZz+71paKbRl2Hm/e85z3VD37wgyo9iTXnjxA0p75z5yyQewi6+eabq9///vcX3M6OtqbPlaGcf4uMrUiBIa7y1GEnfSClKzif+tSnsrRIt9PSy9dW1wV5QizLdhnUhAKbQtBVV11V/fe//51wJOtPlULQ7373u6yvVE2JJAxNqe1c2Qqs3kaaYqDNNTrpwzGFqJJ+LI4uqVvGOpXApsfjhaCpOtDtPMJQNzd7LUxgzDBUvxDtrrvuqm6//fbi5da9eDHV+NRTT1WXX3558fUpgEAXgdxD0KZb2Ut/SqxtL4WhtlK2IxBcYN0TYonk4Ycfrl73utcF11F+VIFNIej9739/9cMf/nB2FiGoXQuEoXZOtiIQVsDi6LCtV/gWgdxD0Kbx+dqb9U0Vhvy6EyCwViB9YeqpU6csjjY/CDQELr744gu+Pibdanrf+96XxZUgIajbdBWGurnZi8CiBSyOXnR7FddRIOf38GwKQTfddFP129/+tmPFcXYThuL0WqUEdgpsenN0+iLV9GHrh0BEgQ9+8IPVgw8+eKz02267rXrggQdm5xCChmmBMDSMo6MQKFpg3bqgdOn/H//4R3XdddcVXZvBE+gjkOPVoLTu58yZMxeUlX5n3/rWt7oS1KHhwlAHNLsQWJLAur8sLbJcUofV0lVg9ZUbr3jFK6rHHnus6+F67feWt7yl+uMf/7j22+09Ht+L9mhnYai/oSMQKFJgXQjy5ugiW2nQAwtcc8011eOPP37sqKtvWR/4lBsPt+k22NG/wE+cqA4PD6cayqLPIwwtur2KI3ChwKZ1QT5UzRYCeXxZ6boXm9a9EYDGmaXC0DiujkogO4FN7wua6y/e7IAMKLTA3Iuk063pF154YeNtsPRI/+nTp0P3aMzihaExdR2bQCYC1gVl0gjDyFLgwx/+cHX//fefH9tUV1+2rQNKg4n2zfFzTg5haE595yYwssC6EOQDdmR0hy9KYDUIpdvI6QrNmD/WAY2p2+3YwlA3N3sRyFrAuqCs22NwGQk0nxj70Ic+VH3/+98fZXS71gG9+c1vrv7whz+Mcm4H3S0gDO02sgWBYgSsCyqmVQaagUAzCKU1Oeve3dNnmNYB9dGbdl9haFpvZyMwmoBH5UejdeAFCowZhLbdBnObOs/JJAzl2RejItBawPeItaayIYEjgebvzFCLpa0DKntyCUNl98/oAwtYFxS4+UrvLDBkEHrTm95UPfTQQxsfh7cOqHObJt9RGJqc3AkJ9BPYtC7IV2j0c7X38gVOnjx57Emxru/Y2nYVKJ3D+4DKm0vCUHk9M+LAAtYFBW6+0nsJ9A1Cu64CeYN7r/bMvrMwNHsLDIDAboF1j+UOtdZh99ltQaBsgdV3Ce1zRWg1RDUlxngCrWzpckcvDJXbOyMPILDulpgQFKDxShxUoMu7hDbdCvP7N2hrsjmYMJRNKwyEwHGB5gd4/U+sCzJLCOwn0Aw1u16q6CrQfrZL2loYWlI31bIIAeuCFtFGRWQiUP9Rse2KzrarQDfeeGP1pz/9KZNqDGMsAWFoLFnHJbCngHVBe4LZnMAOgeuvv7765z//ebTVRz7ykep73/ve+T0siDZ9mgLCkPlAYGYB64JmboDTL1ageau5XjTtKtBi292rMGGoF5+dCfQTWPfBbF1QP1N7E6gFmrfI0v+37ikyC6LNlyQgDJkHBGYQsC5oBnSnDCWw7cWICcJj8aGmw85ihaGdRDYgMJyAW2LDWToSgU0CHos3N/YVEIb2FbM9gY4C6z6g93n5W8fT2o1ACAGPxYdo82hFCkOj0Towgf8LrLsalILR2bNnEREg0FNg1+0wf3D0BA6yuzAUpNHKnEfA1aB53J112QLrXkNRV5wWRNcByOLoZc+DIasThobUdCwCLwq4GmQqEBhWYNsVoBR66pcjNrdzVWjYHiz5aMLQkrurtlkEXA2ahd1JFyiw6xbYutvNbd44vUAqJfUUEIZ6AtqdQC2w7mpQupx/cHAAiQCBlgJdAlB9aFeFWiLb7AIBYcikIDCAwOoHuLUKA6A6RAiBdHvrz3/+89oXIiaA9LuU/qh44YUXdnq4KrSTyAYbBIQhU4NADwFXg3rg2TWswJAByFWhsNNo0MKFoUE5HSySgKtBkbqt1r4CYwSg5phcFerbodj7C0Ox+6/6DgIXXXTRBe8IsjaoA6RdFi+QvvIivU9r01Nd+9wC24ZlrdDip9LoBQpDoxM7wZIEXA1aUjfVMoZAmwD0xje+sXrooYcGO72rQoNRhj2QMBS29QrfR2Dd1SDfLr+PoG2XLDBHAKo9XRVa8syarjZhaDprZypUwNWgQhtn2KMKpFvD6fbXtltgQ18BWleQq0KjtjnMwYWhMK1W6L4CrgbtK2b7pQts+xqMVPscr5Sow5C3TS999o1bnzA0rq+jFypQf8DWw5/jQ75QOsNemECOAahJLAwtbMLNVI4wNBO80+YpsO6D39qgPHtlVMMK7LrttRpADg8Phx1Ax6MJQx3h7HZMQBgyIQi8KOBqkKmwdIG0huevf/3rUZn73lZa9z1gOXgJQzl0ofwxCEPl91AFPQVcDeoJaPfsBNJ6t3TlZt/AUxdSB4zXv/71R1+VkfOPMJRzd8oZmzBUTq+MdAQBV4NGQHXIyQTS1ZouV3maoSf9DqQXI5b6IwyV2rm8xi0M5dUPo5lIwNWgiaCdprdAfZWna+ipw0IKTgcHB73Hk9sBhKHcOlLmeIShMvtm1D0EvDeoB55dRxPYZwHzukHUoSCXhc2jQa0cWBiaSnrZ5xGGlt1f1W344Kz/b0+KmSJTCvRZwFyPM/3Lf6lXefbtxRve8IbOC8L3PZftly0gDC27v6p7UWD1BYreG2RqjCkw1ALmaFd59u2Jr+LYV8z2mwSEIXNj8QKrt8VyfUR48Y1YYIFDhJ7SFzDP2dbmAxBdn5ybc/zOnY+AMJRPL4xkBIHVp8Wat8VW/1nzNkT671dffXX12GOPjTAqhyxNoM96nnqenTp16vwtndLqz3W8vpcs186UNy5hqLyeGXELgV23xTYFoRaHPvr+pfrHbYw2YuVs0+dRdaFn+j4334f0l7/8ZfoBOONiBIShxbRSIbVAm9ti1157bfWf//yn80vp2mg3/2ot+T0ubWotbZshQo8gPH/XPUk2fw+WMgJhaCmdVMeRwLbbYl2J+vyLc/WcaXwvf/nLqyeeeKLrcOzXUiC9Pfnhhx8+2rrLepLUK+t5WmLPtJkwNBP8Ak8rDC2wqRFLuuSSS6rTp0+fL32sp8Wuv/766l//+lenf7mu9uXKK68UigaYrELPAIiFHkIYKrRxGQ5bGMqwKYa0n8CcL1G84YYbqkceeaTz1Yf9KrV1HwHv5+mjl+e+wlCefSlxVMJQiV0z5mNXgJocOb1Esc8TSFrcXUDo6W5X2p7CUGkdy3e8wlC+vTGyLQKrt8XSpl3WhcyJvPrEWz2WsW7xzVmrcxMYQ0AYGkM15jGFoZh9L7rq1S9ZLT08XHXVVdWTTz65sSf1B/4VV1yxdbuim2rwBDoICEMd0OyyVkAYMjGKElhdH5TTbbG+kCkUPfXUU62vcAlJfcXtX7pA/TvwhS98ofrqV79aejnGP6OAMDQjvlO3F1jCbbH21VZVetLs6aefPtpln9t/6arZwcHBPqeyLYFiBXwdR7Gty27gwlB2LTGgVYGl3Rbr0+G2ISn9S+Lyyy8/utLkh8BSBdLLU//9738flVf67fKl9qiUuoShUjoVdJyrt8Vc+Vg/EVadmlv5l0TQX54gZTfn/qte9arq0UcfDVK5MocUEIaG1HSswQSi3RYbCi693fqZZ55Ze2sthaLLLrvs/O23oc7pOATmFnC7bO4OlH9+Yaj8Hi6uArfFhmnppkf309FdYRvG2FHyEPjiF79Y3XnnnW6X5dGOIkchDBXZtuUOevW7xdIlcF9y2r/fm26juYXW39YR8hBozvEvf/nL1Ze+9KU8BmYURQgIQ0W0afmDXHcVY0mPzefSwW0verz00kuPbrH5IVCqQPOPqToQ1f/fqVOnqvvuu6+66aabSi3PuEcUEIZGxHXodgJzfrdYuxEub6v0Asdnn3127doit9CW1+8oFX3lK1+p7rjjjvPl3nXXXdXnPve58//7ox/9aPXd7343Coc69xAQhvbAsumwAusWSbsaNKxxm6O5hdZGyTalCKwGoq997WvV4eFh9ZnPfOaohH3e21VKzcbZX0AY6m/oCB0EXA3qgDbyLutuoVmzNTK6w48icPfddx+7IlSfxB9bo3Av4qDC0CLaWFYRq4uk3ZbJq3/pZY3pFlr94y/pvPpjNO0Evv71r1ef//znz79K4sYbb6zuueee6rbbbmt3AFuFEhCGQrV73mJXH5l3yXrefmw6++rtS2Eozz4ZFQECwwkIQ8NZOtIWAbfFypgeq0Eo/e/nnnuujMEbJQECBDoKCEMd4ezWTsAi6XZOOWy12ivrK3LoijEQIDCFgDA0hXLQc7gaVE7jV3slCJXTOyMlQKC/gDDU39ARVgTWXQ2ySDrPaeJll3n2xagIEJhWQBia1nvxZ1sNQr7uIc+WrwusepVnr4yKAIHxBYSh8Y1DnaH52Lx31OTZ+nUvWXRbLM9eGRUBAtMICEPTOIc4SzMIuS2WX8vXvdpAn/LrkxERIDC9gDA0vfkiz9i82uB2S34ttpg9v54YEQEC+QgIQ/n0otiRNK84CEJ5tdGrDfLqh9EQIJCngDCUZ1+KGZW3FefbKleD8u2NkREgkJeAMJRXP4obTXOdkEW4+bTP97/l0wsjIUAgfwFhKP8eZTtCC6bza8269wb5brH8+mREBAjkJSAM5dWPYkZjwXR+rXJbLL+eGBEBAmUICENl9CmrUVownVU7Kouk8+qH0RAgUJ6AMFRez2YfcfP2mFsw87UjhaAzZ85UzR54mm++fjgzAQLlCghD5fZulpE3b8VYMD1LC6p164LSSLzxe55+OCsBAuULCEPl93DSCuqrQq5ATMp+dLJNIUgvpu+FMxIgsCwBYWhZ/Ry1muZVIbfHRqU+dvB13yWWNhCCpuuBMxEgsGwBYWjZ/R20OleFBuXcebBNIcj3ie2kswEBAgT2EhCG9uKKu7GrQtP0ft2TYfWZrdGapgfOQoBAPAFhKF7PO1XsqlAnttY7WQ/UmsqGBAgQGFxAGBqcdHkHdFVovJ5aDzSerSMTIECgrYAw1FYq8HauCg3ffOuBhjd1RAIECHQVEIa6ygXZz1Wh4RrdfHP36lGtBxrO2ZEIECCwr4AwtK9YsO1dFerX8LQW6PDw8NhbousjJtuTJ09Wzz//fL+T2JsAAQIEegkIQ734lr2zq0Ld+rstAKUjelN0N1d7ESBAYCwBYWgs2cKP23zE28v9djdz3feENfdiuNvQFgQIEJhLQBiaSz7z8/oy1nYN2rQQOu0tALUztBUBAgTmFhCG5u5AhudvvvPGv9AvbNCuAGQdUIaT2pAIECCwRUAYMj0uEHBV6MJJse1JMAuh/RIRIECgbAFhqOz+DT765lWPyN+BtWsRdApAyerg4GDwHjggAQIECEwrIAxN65392aJeFdq1ALpunCfBsp/CBkiAAIG9BYShvcmWu0PzqlCElwBuW/tTdzmFw/Sfs2fPLrfxKiNAgEBwAWEo+ARolr/0FyxuW/fTdHD1xy8FAQIEYgkIQ7H6vbHa5nuFzp07twiVFH5SLbvq8cTcItqtCAIECHQWEIY60y1rx9LfNt12zU/qmqe/ljV3VUOAAIG+AsJQX8GF7F9aGErjTT+7rvrU4ceTXwuZqMogQIDACALC0AioJR4y5/VCbW931e4eey9xBhozAQIE5hMQhuazz+rMuYShXe/3WUXztFdW08hgCBAgUKSAMFRk24YfdB2Gxnikvg449ajb3NpaV2E9xsPDw+EBHJEAAQIEwgoIQ2Fbf7zwOmjsG1TqtTvpaPvuu4veQuddQv45AQIECAwhIAwNobiAY7QJQ/s8sdWGpPm2awuc24jZhgABAgTGEBCGxlAt8Jib1gx1Wbxcl+92VoETwZAJECAQUEAYCtj0dSW3+WqKej+Llk0aAgQIEFiSgDC0pG72rGVbILJ+pyeu3QkQIEAgWwFhKNvWGBgBAgQIECAwhYAwNIWycxAgQIAAAQLZCghD2bbGwJDcM/8AAALeSURBVAgQIECAAIEpBIShKZSdgwABAgQIEMhWQBjKtjUGRoAAAQIECEwhIAxNoewcBAgQIECAQLYCwlC2rTEwAgQIECBAYAoBYWgKZecgQIAAAQIEshUQhrJtjYERIECAAAECUwgIQ1MoOwcBAgQIECCQrYAwlG1rDIwAAQIECBCYQkAYmkLZOQgQIECAAIFsBYShbFtjYAQIECBAgMAUAsLQFMrOQYAAAQIECGQrIAxl2xoDI0CAAAECBKYQEIamUHYOAgQIECBAIFsBYSjb1hgYAQIECBAgMIWAMDSFsnMQIECAAAEC2QoIQ9m2xsAIECBAgACBKQSEoSmUnYMAAQIECBDIVkAYyrY1BkaAAAECBAhMISAMTaHsHAQIECBAgEC2AsJQtq0xMAIECBAgQGAKAWFoCmXnIECAAAECBLIVEIaybY2BESBAgAABAlMICENTKDsHAQIECBAgkK2AMJRtawyMAAECBAgQmEJAGJpC2TkIECBAgACBbAWEoWxbY2AECBAgQIDAFALC0BTKzkGAAAECBAhkKyAMZdsaAyNAgAABAgSmEBCGplB2DgIECBAgQCBbAWEo29YYGAECBAgQIDCFgDA0hbJzECBAgAABAtkKCEPZtsbACBAgQIAAgSkEhKEplJ2DAAECBAgQyFZAGMq2NQZGgAABAgQITCEgDE2h7BwECBAgQIBAtgLCULatMTACBAgQIEBgCgFhaApl5yBAgAABAgSyFRCGsm2NgREgQIAAAQJTCAhDUyg7BwECBAgQIJCtgDCUbWsMjAABAgQIEJhCQBiaQtk5CBAgQIAAgWwFhKFsW2NgBAgQIECAwBQCwtAUys5BgAABAgQIZCsgDGXbGgMjQIAAAQIEphAQhqZQdg4CBAgQIEAgWwFhKNvWGBgBAgQIECAwhYAwNIWycxAgQIAAAQLZCghD2bbGwAgQIECAAIEpBIShKZSdgwABAgQIEMhW4H97F9ksliZ7wAAAAABJRU5ErkJggg==', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAkMAAAGNCAYAAAASM3XCAAAgAElEQVR4Xu3dS6glV7kH8IqPbtsOiUmMooIDRTSo8Y3gRBB16EBQURAEXyOViOLAiSMVEQVxooKggoKCA4eiDoWAzxiRKHEiqGgS0z6Sc05OnyPVcbd19qm9d1Xteqy1vl/gcu9NdtVa3+9byf531araN5yenp5W/iJAgAABAgQIBBW4QRgK2nllEyBAgAABAtcEhCELgQABAgQIEAgtIAyFbr/iCRAgQIAAAWHIGiBAgAABAgRCCwhDoduveAIECBAgQEAYsgYIECBAgACB0ALCUOj2K54AAQIECBAQhqwBAgQIECBAILSAMBS6/YonQIAAAQIEhCFrgAABAgQIEAgtIAyFbr/iCRAgQIAAAWHIGiBAgAABAgRCCwhDoduveAIECBAgQEAYsgYIECBAgACB0ALCUOj2K54AAQIECBAQhqwBAgQIECBAILSAMBS6/YonQIAAAQIEhCFrgAABAgQIEAgtIAyFbr/iCRAgQIAAAWHIGiBAgAABAgRCCwhDoduveAIECBAgQEAYsgYIECBAgACB0ALCUOj2K54AAQIECBAQhqwBAgQIECBAILSAMBS6/YonQIAAAQIEhCFrgAABAgQIEAgtIAyFbr/iCRAgQIAAAWHIGiBAgAABAgRCCwhDoduveAIECBAgQEAYsgYIECBAgACB0ALCUOj2K54AAQIECBAQhqwBAgQIECBAILSAMBS6/YonQIAAAQIEhCFrgAABAgQIEAgtIAyFbr/iCRAgQIAAAWHIGiBAgAABAgRCCwhDoduveAIECBAgQEAYsgYIECBAgACB0ALCUOj2K54AAQIECBAQhqwBAgQIECBAILSAMBS6/YonQIAAAQIEhCFrgAABAgQIEAgtIAyFbr/iCRAgQIAAAWHIGiBAgAABAgRCCwhDoduveAIECBAgQEAYsgYIECBAgACB0ALCUOj2K54AAQIECBAQhqwBAgQIECBAILSAMBS6/YonQIAAAQIEhCFrgAABAgQIEAgtIAyFbr/iCRAgQIAAAWHIGiBAgAABAgRCCwhDoduveAIECBAgQEAYsgYIECBAgACB0ALCUOj2K54AAQIECBAQhqwBAgQIECBAILSAMBS6/YonQIAAAQIEhCFrgAABAgQIEAgtIAyFbr/iCRAgQIAAAWHIGiBAgAABAgRCCwhDoduveAIECBAgQEAYsgYIECBAgACB0ALCUOj2K54AAQIECBAQhqwBAgQIECBAILSAMBS6/YonQIAAAQIEhCFrgAABAgQIEAgtIAyFbr/iCRAgQIAAAWHIGiBAgAABAgRCCwhDoduveAIECBAgQEAYsgYIECBAgACB0ALCUOj2K54AAQIECBAQhqwBAgQIECBAILSAMBS6/YonQIAAAQIEhCFrgAABAgQIEAgtIAyFbr/iCRAgQIAAAWHIGiBAgAABAgRCCwhDoduveAIECBAgQEAYsgYIECBAgACB0ALCUOj2K54AAQIECBAQhqwBAgQIECBAILSAMBS6/YonQIAAAQIEhCFrgAABAgQIEAgtIAyFbr/iCRAgQIAAAWHIGiBAgAABAgRCCwhDoduveAIECBAgQEAYsgYIECBAgACB0ALCUOj2K54AAQIECBAQhqwBAgQIECBAILSAMBS6/YonQIAAAQIEhCFrgAABAgQIEAgtIAyFbr/iCRAgQIAAAWHIGiBAgAABAgRCCwhDoduveAIECBAgQEAYsgYIECBAgACB0ALCUOj2K54AAQIECBAQhqwBAgQIECBAILSAMBS6/YonQIAAAQIEhCFrgAABAgQIEAgtIAyFbr/iCRAgQIAAAWHIGiBAgAABAgRCCwhDoduveAIECBAgQEAYsgYIECBAgACB0ALCUOj2K54AAQIECBAQhqwBAgQIECBAILSAMBS6/YonQIAAAQIEhCFrgAABAgQIEAgtIAyFbr/iCRAgQIAAAWHIGiBAgAABAgRCCwhDoduveAIECBAgQEAYsgYIECBAgACB0ALCUOj2K54AAQIECBAQhqwBAgQIECBAILSAMBS6/YonQIAAAQIEhCFrgAABAgQIEAgtIAyFbr/iCRAgQIAAAWHIGiBAgAABAgRCCwhDoduveAIECBAgQEAYsgYIECBAgACB0ALCUOj2K54AAQIECBAQhqwBAgQIECBAILSAMBS6/YonQIAAAQIEhCFrgAABAgQIEAgtIAyFbr/iCRAgQIAAAWHIGiBAgAABAgRCCwhDoduveAIECBAgQEAYsgYIECBAgACB0ALCUOj2K54AAQIECBAQhqwBAgQIECBAILSAMBS6/YonQIAAAQIEhCFrgAABAgQIEAgtIAyFbr/iCRAgQIAAAWHIGiBAgAABAgRCCwhDoduveAIECBAgQEAYsgYIECBAgACB0ALCUOj2K54AAQIECBAQhqwBAgQIECBAILSAMBS6/YonQIAAAQIEhCFrgAABAgQIEAgtIAyFbr/iCRAgQIAAAWHIGiBAgAABAgRCCwhDoduveAIECBAgQEAYsgYIECBAgACB0ALCUOj2K54AAQIECBAQhqwBAgQIECBAILSAMBS6/YonQIAAAQIEhCFrgAABAgQIEAgtIAyFbr/iCRAgQIAAAWHIGiBAgAABAgRCCwhDoduveAIECBAgQEAYsgYIECBAgACB0ALCUOj2K54AAQIECBAQhqwBAgQIECBAILSAMBS6/YonQIAAAQIEhCFrgAABAgQIEAgtIAyFbr/iCRAgQIAAAWHIGiBAgAABAgRCCwhDoduveAIECBAgQEAYsgYIECBAgACB0ALCUOj2K54AAQIECBAQhqwBAgQIECBAILSAMBS6/YonQIAAAQIEhCFrgAABAgQIEAgtIAyFbr/iCRAgQIAAAWHIGiBAgAABAgRCCwhDoduveAIECBAgQEAYsgYIECBAgACB0ALCUOj2K54AAQIECBAQhqwBAgQIECBAILSAMBS6/YonQIAAAQIEhCFrgAABAgQIEAgtIAyFbr/iCRAgQIAAAWHIGiBAgAABAgRCCwhDoduveAIECBAgQEAYsgYIECBAgACB0ALCUOj2K54AAQIECBAQhqwBAgQIECBAILSAMBS6/YonQIAAAQIEhCFrgAABAgQIEAgtIAyFbr/iCRAgQIAAAWHIGiBAgAABAgRCCwhDoduveAIECBAgQEAYsgYIECBAgACB0ALCUOj2K54AAQIECBAQhqwBAgQIECBAILSAMBS6/YonQIAAAQIEhCFrgAABAgQIEAgtIAyFbr/iCRAgQIAAAWHIGiBAgAABAgRCCwhDoduveAIECBAgQEAYsgYIECBAgACB0ALCUOj2K54AAQIECBAQhqwBAgQIECBAILSAMBS6/YonQIAAAQIEhCFrgAABAgQIEAgtIAyFbr/iCRAgQIAAAWHIGiBAgAABAgRCCwhDoduveAIECBAgQEAYsgYIECBAgACB0ALCUOj2K54AAQIECBAQhqwBAgQIECBAILSAMBS6/YonQIAAAQIEhCFrgAABAgQIEAgtIAwNbP8NN9ww8EiH9RFoOl+4cKF68MEHq8uXL/c5hc8SIECAAIGtAsLQwAUiDA2Em+GwujcnJyczjGQIAgQIEChBQBgqoYuF1PCUpzylOjo6ul7N6enpXpXte/xegzuYAAECBLIREIayaZWJdhFYv2InEHVR8xkCBAjEFhCGYve/iOqf9KQnVVevXj1XiyBURHsVQYAAgckFhKHJiQ0wpUDb3q3679WbrQ8ODqYc2rkJECBAoBABYaiQRkYro95fdHh4eKZsG6ejrQL1EiBAYBwBYWgcR2eZUeAJT3hC1bwFJgTNiG8oAgQIFCggDBXY1FJLWg9BdZ0XL150O6zUhquLAAECMwkIQzNBG2aYwBOf+MSN7wyyQXqYqaMIECBA4KyAMGRFJCewet9QW9ixOTq5dpkQAQIEshcQhrJvYTkFtN0GW1VX/7O2x+fLqV4lBAgQILCUgDC0lLxxrwlsekdQ/c9sjLZICBAgQGAOAWFoDmVjnBFwG8yCIECAAIGUBIShlLpR+Fy2bYZ2G6zw5iuPAAECCQsIQwk3p4SpuQpUQhfVQIAAgbIFhKGy+7tYdds2Q9dXiI6Pjxebm4EJECDQFHjf+95X3X///dXrX//66lOf+hScgALCUMCmT1XytttgNkNPpe68BAgMEfjNb35TfehDH6ruvvvu6y9u/cQnPlF99rOfHXI6x2QuIAxl3sClp+822NIdMD4BAl0FfvrTn1bvfe97qz/84Q9nXtVx6dKl6iMf+Uj1mc98puupfK4wAWGosIbOVY53As0lbRwCBPYR2BSA6nPWP+fz2te+tvryl79cvfSlL91nGMdmLiAMZd7AOafvNtic2sYiQGAfgTvuuKO67777zvyoc32++g9yL3jBC6qvf/3r1ete97p9hnBsQQLCUEHNnKIUt8GmUHVOAgSmEHjXu95Vfe973zv3gIYANIV2WecUhsrq52jV1CHo8PCw9XzeCTQasxMRILCnwA9/+MPq7W9/e3XlypVzZ7rpppuuhaM3v/nNe47i8NIFhKHSOzygvrb9QJ4GGwDpEAIEJhP42Mc+Vn3hC184dxus/omft73tbdW3v/3tycZ24vIEhKHyejq4orbfCXMVaDCnAwkQmEDgzjvvrOrH4pt/1X9Yq/cB1XuE/EVgiIAwNEStwGPq/5is/8fl5OSkwEqVRIBAjgK33HJL9fDDD5+Zev2Htbvuuqv6/Oc/n2NJ5pyQgDCUUDOWmErbLbH6cdODg4MlpmNMAgQInBFoC0H1k63f/e53q7e+9a20CIwiIAyNwpjfSdo2SNsXlF8fzZhAiQLf//73q3e84x3nngq7+eabz10dKrF+Nc0vIAzNb774iG1Xg05PTxeflwkQIBBboN4U/cUvfrFav0UvBMVeF3NULwzNoZzIGG0vTfSjqYk0xzQIBBb4wAc+UH3ta187J1C/Ffqee+4JLKP0uQSEobmkFxpn00sT3RJbqCGGJUDgusC3vvWt6j3vec+ZK0H1f5s++tGP2hRtncwqIAzNyj3PYNveGl3PwAbpefpgFAIENgu0Xal+//vfX331q1/FRmB2AWFodvJpBtwVgOo/bV24cMFTYtPwOysBAh0FLl++XD3yyCNnPv285z2vuv/++zuewccIjC8gDI1vOusZ216UuJqAADRrKwxGgMAWgec///nVH//4RyHIKklSQBhKsi27J7UpBAlAu+18ggCB+QTaQtClS5fOXR2ab0ZGInBeQBjKbFVsCkGeCsuskaZLoHCBtifE6v9+PfbYY4VXrrwcBYShTLomBGXSKNMkEFjgOc95TvWXv/zl3I+n1lesv/GNb1Tvfve7A+soPWUBYSjl7lRVJQQl3iDTIxBY4OUvf3l17733VlevXt2o8M1vflMICrxGcildGEq0U0JQoo0xLQKBBd75zndW9U9lHB0dbVW48cYbr70n6IMf/GBgLaXnJCAMJdKttp/IaE7NnqBEGmUaZwTq2x9tf1mvZSyUr3zlK9WHP/zhneGnfm3Hl770JeGnjLaHrEIYSqDt24LQ0C+V1XuHVuWl/ttjzS/V9d8lSqBFptAisCkI7cLyxOMuoeX+eR1+Pv7xj1f/+te/tk6iDj/1L8Z/5zvfWW6yRiYwooAwNCLm0FMN/VIZOl6ux9VO9f9s25+Qa225znvftSsYLdf5+gdRP/3pT1cPPfTQuR9GXZ9V/Qe2+nfCfvWrXy03YSMTmFBAGJoQt+up9/1C6TpOyZ8TlJbtbv1lWf+1zxXIfY5dtvr0R18FnwceeKDzZJ/1rGdVf/7znzt/3gcJ5CwgDGXQva5haUggWH2JrRhK+EIqoYYMluXGKdab/+tbnUP6MOSYnK2mmPtb3vKW6kc/+lH16KOPdj59/d+OW2+9teoTljqf3AcJZCAgDOXQpA2bVNennsMv0TfD11RffFOdN4OlMvsUd/0mXp8J5bB++9Qzx2df+MIXXvuJi+Pj487D1WG1/i2w++67r/MxPkigdAFhKKMO15up6y/6Ll/2QzdeZ8RhqgsIDAk/qyubdRBefWnvOk/9WXvDzjb49ttv77S/p3lU7fymN72p+sEPfrDAajEkgXwEhKF8erX1tsTUXxzrt+qaX2wFECphg8Cu0NI8bOiVnW0/NlyfP1qwrzc1f+5zn6uuXLnSa13edttt1Sc/+cnqrrvu6nWcDxMgUFXCUGGrYNf7iuYqtxme6sdwDw4O5hraOP8TWN8PVv/tLlcVuwIODT/bzl8Hn02vVijxybM3vvGN1U9+8pNR+7Lu23ZlrmuPfY5AFAFhKEqnd9S52vTa/NiYX5ybho8Qmla3NzcZzOE8xjKfIvxsm9euYF/PZ+krlF0fbnjmM59Z/e1vf7tWbkr9HvLQxRhryTkIpCYgDKXWkQznk9sLHjMknnTKbV/oKb34co5bdUOAuwahPufeN5w0/1AzJHRdvHjRVdw+DfPZYgSEoWJamX4hQlP/Hu36wl36ykj/ivY/YtcVo+YIU1892tWfbVdEl3qB6K4HMea+Arj/inAGAvsLCEP7GzoDAQILCvR9r9EYX/Z9A1m9ubn5Dp8hV22mJG7bq+WJvinFnTs1AWEotY6YDwECewv0DStdrrDVVzYPDw+3zm3Tba7mfN7whjdUP/7xj/eucYoTrLulFtqmqNk5CdQCwpB1QIBA8QL7XD3aFoK63IZrBowxrkpN3az1egWiqcWdPwUBYSiFLpgDAQKzC/S5etScXJ/3HuUWhFZ1NvdCCUOzL00DLiAgDC2AbkgCBNIT6Hr1qMvVnRtvvLH6z3/+c73ILsekJCIMpdQNc5lDQBiaQ9kYBAhkIbDrbdjrRay/CHI9BNWfzy0Irea8qtWVoSyWrknuKSAM7QnocAIEyhBoe0y++d6dbW/H3iRw+fLl6t///nd2QCuLHINcdtgmnISAMJREG0yCAIGlBNo2SO96+WCXW2q5Bonm1bFdDkv1zLgExhYQhsYWdT4CBLIRWN9E3SXA1Fd7HnnkkV41dnnqrNcJJ/yw/UIT4jp1sgLCULKtMTECBKYUWL8ttusqyKanz+rzvOY1r6nuvvvua9Pd9Ybn+jMpv9DQLbIpV51zpyogDKXaGfMiQGASgbbbYps2CW+7CtTlKlKX31Vb34Q9SdEdT9oMfDZOd0TzsSIEhKEi2qgIAgS6CHS9LbZps/T6VaAuYzY/0+XdRkveUnOLrG9Hfb4UAWGolE6qgwCBrQLrAafttti2W2EnJyejCnfZhL0acI6rR80rZn1eLDkqipMRWEhAGFoI3rAECMwrsO2qx6YQdOnSpd6bpYdW1eWqUTMcjf2r926RDe2c40oQEIZK6KIaCBDYKtD2Rf/qV7+6+vnPf37uuDpkvPKVr6x+9rOfLara58pRPdF9rh41H6dPeXP3og0xeNECwlDR7VUcAQKroLD63/XtoEcffbQ1BI19K2xs/Tqo1H912dy8uhLWpSZ7hcbulPPlJiAM5dYx8yVAoJfArjdHd3kqrNeAM364y9Nqzem0bc5uBqFdrxeYsTRDEZhVQBialdtgBAjMLdD2Mxv1HObcDzRnzV3ec9Q2n5xD4Zy+xipTQBgqs6+qIhBaoN4P9Itf/KL1dtKrXvWqxfcDzdmcIVePLly4UB0cHMw5TWMRWFRAGFqU3+AECIwp8NSnPrV1P9BqjC57bcacT6rnam6Y3jXHfTZm7zq3f04gFQFhKJVOmAcBAr0Fdt0Sqr/ImwFIGHqcuPl03eqdQl0f7V/ypZC9F4gDCHQUEIY6QvkYAQLLCtR7fA4PDzs9SdV8PLy5gVoYqqrmVaFt+4SEo2XXu9HnFRCG5vU2GgECHQT6BJ/6dLseI29uoo7+duWhj9Hvugq3amt9/rFfCNlhyfgIgb0EhKG9+BxMgMC+Aq94xSuqX//6152u+DS/cF/2spdVv/zlLzsN3/Z4fcRQNOZj9MJRp6XnQ5kICEOZNMo0CZQi0OfFgaurPvX7b9pelNjHZNOm4SihqHnba4rH6Pu8MXuK8fusBZ8lsC4gDFkTBAhMKtB178mYwWdbQV2epCrt5YPNH2GtbebYOyUcTfqvlZOPLCAMjQzqdARSFlhdldk2xzm+KHcZtb0o8WlPe1r10EMP7Tq08z/fFYpKunoxdJ9QZ8wOHxzyvqN6vR4fH3c4u48Q2E9AGNrPz9EEZhdY/Yl7NXAK4WV2hI4D9g1VJe4tanuMviPfpB/rG45WVw7r/93l99YmnbyTFycgDBXXUgXlKLDajFrPvZRws+llfbfeemv18MMPn2tTynXnepVo6n1CY/+71nc/2Wp8T7CN3Yl45xOG4vVcxTMIPP3pTz9zS2eOL/rmVZA6cDzwwAOjVdpl38/cX0gphKqmeWpXK9avcs2xBkdbcI0TDbmC1AxJflpkiq6Ud05hqLyeqmgGgdtvv7168MEHr4005ZfM6st27ve29HlsOrUQ0Kf9t9xyS3XlypXrh0zVy2Zouvnmm6t//OMffabZ+7NLbJjuPck9D+i6RteH2fVOqj2n5fBMBYShTBtn2tMLDL1kv21mzS/FlDaHdv3Tdz3/l7zkJdU999wzfQMSGmE9XIw9tdr1pptuar19OGSsMd8nNGT8JY8Z8u9t843lS87d2MsJCEPL2Rt5YYHmRuR9rwisvnxSCjjbeO+8887q3nvv3XlVy490nlXcFhpXVvXTT6urZfusq6G/ARY5CG1a830f88/l3+OF/xNa1PDCUFHtVExT4BnPeMb1fTP7fCmtzpnb5fXmY/Rd659730/OK7btybPmWtn1hTrkCsau/jT3drnasX119bkaat9Rzv+mdpu7MNTNyacSFRjzKaxV2Lntttuqv//974lW/Pi0XvziF1e/+93vrv3fXYPOpoJyfVIqpQbt2mDe1XjIPpi2dwh1HS8lw6Xn0vXqkZC5dKemGV8YmsbVWUcSiHorq34D8mOPPTZK2Fm/svXkJz/52q+/+2t8gS5XG/reeuz6Jb1eza6rSONXX9YZt13563MFsCyVcqsRhsrtbbaVPfvZz67++te/DrrikfKtrOZVrDGu6LR9+dV/74477qh++9vfZtv/kiY+1hWjdZP6bdz//Oc/B4VlIan/CusScuuz9g26/WfiiKkEhKGpZJ13kEDzkn/bCVLaqPyiF72o+v3vf399mvvertoGttQj9oOa6KBWgV1XeIbc2lp/ym0VvvqsxSHjRm/xrl42fYZuhI9uPHf9wtDc4sbbKPDc5z63+tOf/hROqBkAc35nT7jG7VHwrisN9VXELr/J1fXJsS57kYSi4Q3tckttPSDV/79/34ebj32kMDS2qPPtJbDrtsJeJ5/p4OYXVP0nyKOjo5lGNkyOAtt+MHbbbZfmOhuyqXfbuF3DWI7ec8x5yJOCq3m5jTlHh86PIQwt425UAgQInBMY8oeBMa4sto3rStG4C7TPrbXVyELpuD3YdjZhaD5rIxEgQKCzwJBgtPU/9jfccO0fb3v/0fqYAlHndg36YJeA1Gf/16BJOOiagDBkIRAgQCADgX1uvexTni/jffT+f2yX4NM2Ev9x/HedRRjaJeSfEyBAIAGBPj++Oub7uXwZD2t+l03rbWdO6YnZYZXneZQwlGffzJoAgWACXZ8cC8aSRLm7ng5cn6Tbj0m07cwkhKH0emJGBAgQOPsf6v/t96n/Zv128oODA0ILCvS56uPpsAUb1WNoYagHlo8SIEBgbgE/vjq3+Pnx+mxm9xbq5fs1ZAbC0BA1xxAgQGAGgeaXsFsrM4D/b4iuV35c9ZmvJ1OPJAxNLez8BAgQGCDQfKuxIDQAsOchXa7++GmNnqgZfVwYyqhZpkqAQAyBPk+OxRAZv8ouj7q75TW+e6pnFIZS7Yx5ESAQUkAQmq7tXa7+DPlpk+lm7MxzCQhDc0kbhwABAh0EPELfAanjR1z96QjlY95AbQ0QIEAgFYF9f3w1lTqWnIerP0vq5zu2K0P59s7MCRAoSMAj9MOa2fXqz7bfZBs2sqNKEhCGSuqmWggQyFLAk2P92tb02nSkJ/D6mUb/tDAUfQWonwCBRQVsmN7N3+XnLjz2vtvRJzYLCENWBwECBBYUsGG6Hd/VnwUXZcChhaGATVcyAQJpCDT3CdVf/sfHx2lMbIFZuPqzALohrwsIQxYDAQIEFhCwT+hx9F1Pf9n7s8DiDDikMBSw6UomQGBZAfuENocgv/e17NqMOrowFLXz6iZAYDGB5j6h09PTxeYx98D1Y/BXr15tHTb6bcK5e2G8swLCkBVBgACBGQWaQShKANh0K8wtsBkXnqG2CghDFggBAgRmEmiGgtKDwLYN0X7/a6YFZ5jOAsJQZyofJECAwHCBKPuEtt0Ku3jxYnVwcDAc0ZEEJhIQhiaCdVoCBAg0BUrfJ+RWmPWes4AwlHP3zJ0AgSwESn2xolthWSw/k+wgIAx1QPIRAgQIDBUo8QdY3Qobuhocl6qAMJRqZ8yLAIHsBZqhoYQN026FZb8kFbBBQBiyNAgQIDCRQAn7hLbdCovyaoCJlofTJiQgDCXUDFMhQKAcgdz3CW26FVbXdeHCBU+FlbNUVVJVlTBkGRAgQGBkgZz3CbkVNvJicLosBIShLNpkkgQI5CKQ4w+wrr8DqWntVlguK8889xEQhvbRcywBAgQaArm9WLEZ3JqNdCvMso4mIAxF67h6CRCYTCCXfUJuhU22BJw4UwFhKNPGmTYBAmkJpL5PyK2wtNaL2aQlIAyl1Q+zIUAgQ4GU9wltuwrkqbAMF5spTyIgDE3C6qQECEQRSHWfkFthUVagOscQEIbGUHQOAgTCCqT0YsVtP5PhqbCwS1ThHQSEoQ5IPkKAAIE2gebVlyXDxqb9QCX8BIiVR2AOAWFoDmVjECBQnEAzCC0ZOtpuhy05n+IaraAQAsJQiDYrkgCBMQVS2CfU9uY5jBAAAAXxSURBVI6gOhhdvXp1zFKdi0AIAWEoRJsVSYDAmAJL7hNquyXmStCY3XWuiALCUMSuq5kAgcECS75Yse2W2MWLF/1o6uBuOpDA4wLCkJVAgACBjgJL7RNqC0FLbtjuyOVjBLIREIayaZWJEiCwpMD6Y+unp6eTT8ctscmJDUDAlSFrgAABAl0F5t4n5JZY1874HIH9BVwZ2t/QGQgQKFxgzn1CbokVvpiUl6SAMJRkW0yKAIFUBOb6Ada2t0d7SiyVVWAepQsIQ6V3WH0ECAwWmOsHWJtXnlaTnWNP0mAYBxIoTEAYKqyhyiFAYByBOV6s6JbYOL1yFgL7CghD+wo6ngCBIgWm3CfklliRS0ZRGQsIQxk3z9QJEJhGYMp9Qm6JTdMzZyWwj4AwtI+eYwkQKE6gGVbG3MDsllhxS0VBBQkIQwU1UykECOwnMEUQcktsv544msAcAsLQHMrGIEAgaYH1zdJj/Pq7t0cn3XKTI3BGQBiyIAgQCC0wRRDy9ujQS0rxGQoIQxk2zZQJEBhHoPkeofqM+/4CvFti4/TFWQjMLSAMzS1uPAIEkhAYMwi5JZZES02CwGABYWgwnQMJEMhVYP021j5XhNwSy3UVmDeB/wsIQ1YDAQKhBNbDy9CfvVi/slQjjrHxOlQzFEsgEQFhKJFGmAYBAtMLjPHovFti0/fJCATmFhCG5hY3HgECiwiMEYTcElukdQYlMLmAMDQ5sQEIEFhSYP1KzpC3Src9JeaW2JJdNTaBcQWEoXE9nY0AgYQExniH0PrVoCFhKiESUyFAoEVAGLIsCBAoUmB9g3PfKzltG6Trv3d8fFykl6IIRBYQhiJ3X+0EChXY5x1CNkgXuiiURWCLgDBkeRAgUJTAPu8QskG6qKWgGAKdBYShzlQ+SIBA6gJD3yFkg3TqnTU/AtMKCEPT+jo7AQIzCQwNQjZIz9QgwxBIWEAYSrg5pkaAQDeBIe8QskG6m61PEYggIAxF6LIaCRQqMOQdQjZIF7oYlEVgDwFhaA88hxIgsJzAkHcI2SC9XL+MTCBlAWEo5e6YGwECrQLrG553vUPIBmkLiQCBbQLCkPVBgEBWAn3fIWSDdFbtNVkCiwgIQ4uwG5QAgSECfYKQDdJDhB1DIKaAMBSz76omkJ1A10fnbZDOrrUmTGBxAWFo8RaYAAECuwS6BiEbpHdJ+ucECLQJCEPWBQECSQt0eYeQDdJJt9DkCCQvIAwl3yITJBBXoEsQskE67vpQOYGxBIShsSSdhwCBUQV2BSEbpEfldjICoQWEodDtVzyB9AR2vUzRBun0emZGBHIXEIZy76D5EyhIYFcQskG6oGYrhUBCAsJQQs0wFQKRBbYFobYQVN8mOz4+jkymdgIERhIQhkaCdBoCBIYLrD8NdvHixerg4KBqe0qs3kt0cnIyfDBHEiBAYE1AGLIkCBBYVKDtrdL1hA4PD8/N6/T0dNG5GpwAgTIFhKEy+6oqAlkItAWho6Ojaj30rK4UZVGUSRIgkJ2AMJRdy0yYQBkC60Govv21HoLcEiuj16ogkLqAMJR6h8yPQIECbRuim2UKQQU2XUkEEhYQhhJujqkRKFFgVxByS6zErquJQNoCwlDa/TE7AkUJbAtCHpUvqtWKIZCVgDCUVbtMlkC+As2f13BLLN8+mjmBEgWEoRK7qiYCCQm0/XxGPT37ghJqkqkQCC4gDAVfAMonMKVA20sT6/HsC5pS3bkJEOgrIAz1FfN5AgQ6CbT9qny9Z+jq1audjvchAgQIzCUgDM0lbRwCQQT8qnyQRiuTQEECwlBBzVQKgaUF/Kr80h0wPgECQwSEoSFqjiFA4IxA2y0xG6QtEgIEchEQhnLplHkSSFSg7ZF5e4MSbZZpESDQKiAMWRgECAwSsDdoEJuDCBBIUEAYSrAppkQgdQF7g1LvkPkRINBHQBjqo+WzBAhcE2jeGrM3yKIgQCB3AWEo9w6aP4GZBQShmcENR4DA5ALC0OTEBiBQnkC9X+jo6Kg6OTkprzgVESAQTuC/bIOUWQ9IJzwAAAAASUVORK5CYII=', 2, '2019-09-22 16:08:41', 1, 'en mi opinion esto no sirve oa un qlo. ');
INSERT INTO `diagnosis` (`id`, `commentClient`, `machineCommentState`, `machineObservation`, `firmClient`, `firmMaintenance`, `idProduct`, `dateCreated`, `idStatusMachine`, `noteGeneral`) VALUES
(2, '<p>trgtrgrtgrtgrtgtr</p>\r\n', 0x3c703e7472677472677274673c2f703e0d0a, 0x3c703e67657267653c2f703e0d0a, 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAv8AAAHaCAYAAACXciY6AAAgAElEQVR4Xu3dCbAcVfk34MMWQgAhhCWskRCIgKKUBqFYUhHCIoGgFLhQGMGlXHArXNBScYn1V8GlULQUi0i5RNRC0WAAxWAUUUQBFWTRBDRCIJAQCJCdr87UN/Hmknun596Ze7v7faYqlQAz3ed93ubm1z2nT2/27LPPPpu8CBAgQIAAAQIECBCovcBmwn/te6xAAgQIECBAgAABAg0B4d+BQIAAAQIECBAgQCCIgPAfpNHKJECAAAECBAgQICD8OwYIECBAgAABAgQIBBEQ/oM0WpkECBAgQIAAAQIEhH/HAAECBAgQIECAAIEgAsJ/kEYrkwABAgQIECBAgIDw7xggQIAAAQIECBAgEERA+A/SaGUSIECAAAECBAgQEP4dAwQIECBAgAABAgSCCAj/QRqtTAIECBAgQIAAAQLCv2OAAAECBAgQIECAQBAB4T9Io5VJgAABAgQIECBAQPh3DBAgQIAAAQIECBAIIiD8B2m0MgkQIECAAAECBAgI/44BAgQIECBAgAABAkEEhP8gjVYmAQIECBAgQIAAAeHfMUCAAAECBAgQIEAgiIDwH6TRyiRAgAABAgQIECAg/DsGCBAgQIAAAQIECAQREP6DNFqZBAgQIECAAAECBIR/xwABAgQIECBAgACBIALCf5BGK5MAAQIECBAgQICA8O8YIECAAAECBAgQIBBEQPgP0mhlEiBAgAABAgQIEBD+HQMECBAgQIAAAQIEgggI/0EarUwCBAgQIECAAAECwr9jgAABAgQIECBAgEAQAeE/SKOVSYAAAQIECBAgQED4dwwQIECAAAECBAgQCCIg/AdptDIJECBAgAABAgQICP+OAQIECBAgQIAAAQJBBIT/II1WJgECBAgQIECAAAHh3zFAgAABAgQIECBAIIiA8B+k0cokQIAAAQIECBAgIPw7BggQIECAAAECBAgEERD+gzRamQQIECBAgAABAgSEf8cAAQIECBAgQIAAgSACwn+QRiuTAAECBAgQIECAgPDvGCBAgAABAgQIECAQRED4D9JoZRIgQIAAAQIECBAQ/h0DBAgQIECAAAECBIIICP9BGq1MAgQIECBAgAABAsK/Y4AAAQIECBAgQIBAEAHhP0ijlUmAAAECBAgQIEBA+HcMECBAgAABAgQIEAgiIPwHabQyCRAgQGD4Be655540efLktHTp0rR+/fr07LPPbhhUzz/3Hml//62/qkaMGJFWrVo1/IUbAQECpREQ/kvTCgMhQIAAgbIJjBo1Kq1evboR0osG9bLVkMezxRZbpLVr15ZxaMZEgMAQCwj/QwxudwQIECAwPAI77bRTeuKJJyof5Aeit9VWWzVOYrwIECAg/DsGCBAgQKByAnvttVdavHjxsAX5zTbbrE+znv8t/3nzzTdPo0ePTvPmzUsHHXRQYestt9yyMTUovwY67ae5s7z/PNXIiwABAsK/Y4AAAQIEhlXg1FNPTXPnzm1rDnwnB9w7yOd/ztNkvvnNb6Y3vvGNndzVhm098MADjROBlStXbgj2gwn4zRqaY//ud7+bzjzzzK6M3UYJEKi2gPBf7f4ZPQECBEolcPvtt6dJkyY9J8h34up1kUI3FeTzlffrrrsuveIVryiyiY6/J59I9L5nYCA7adaW65kzZ0468cQTB7IZnyFAILiA8B/8AFA+AQIE+hJYsmRJ2nPPPdO6deueM+1kMFep2xHf1BSafEX+nHPOaWczQ/regYb9nlfvDzvssHTzzTcP6bjtjACBGALCf4w+q5IAAQIbCeQbQHOoH6or8nk/vYN8/udddtklPfTQQ5XuTtGw3zPcjxkzJj3yyCOVrtvgCRCopoDwX82+GTUBAgQKCeR13ptLPHbyav2mgvy2226bHn/88Y1CfqFBVuxN7YT97NQ8yapYmYZLgEBNBYT/mjZWWQQIxBIYTMjfVJDPAffuu+9O++67byzITVQr7Ic/BAAQqJWA8F+rdiqGAIG6C2y//fbp6aefHtANpM2Qn8Ps8uXLU36AldfGAnkVnvHjxxfyzZ7514IFC9K4ceNQEiBAoBICwn8l2mSQBAhEE9h5553TsmXLCoXQ3jY9V4X561//2tba8tGchf1oHVcvAQLCv2OAAAECwyiQH1aVb3gdyFKQPUP+HXfckQ4++OBhrKQauxb2q9EnoyRAoHsCwn/3bG2ZAAECGwQmTpyY/vnPfw4o5OeNNJ8U+4tf/CIdf/zxZAsKCPsFobyNAIEwAsJ/mFYrlACBoRA46qijGuuzD+RKfs+Q//Wvfz295S1vGYoh12ofwn6t2qkYAgS6ICD8dwHVJgkQqL/AKaeckvJV+MGE/Hw1/21ve1u69NJL6w/WpQqF/S7B2iwBArUVEP5r21qFESDQCYG//OUvadKkSYMO+bNnz05nnnlmJ4ZkGymlvGLR+vXr+7VorsZjnX2HDAECBP4nIPw7GggQIJBS+t73vpdmzJjRCJQDeRhWM2gedthhjWk/Xp0VaD6RuL/eCPudNbc1AgTqKSD817OvqiJAoB+BLbfcctAhf7/99kv33nsv5y4J5KVOly5d2u+JmLDfJXybJUCg1gLCf63bqzgCcQXyXPAc0JtTQ9q9mt8MlmPHjk3//e9/40IOUeUnn3xymjt3bqGwf/XVV6dp06YN0cjshgABAvUSEP7r1U/VEAgn0KmQv+OOO6bHHnssnN9wFTxnzpw0ffr0lvdS5JOwk046KV1zzTXDNVT7JUCAQK0EhP9atVMxBOot0Jz3nasc6JX8UaNGpSeffLLeUCWtLt+k22p1pBz299hjj7Ro0aKSVmFYBAgQqLaA8F/t/hk9gVoKTJgwIS1cuLBlUNxU8c2n3o4YMSKtXLmylj5VKaroTbr5pGDNmjVVKcs4CRAgUGkB4b/S7TN4AtUXGGjQb4b8fPPu6tWrqw9Rgwry/RGPPPJIoXn7lt+sQcOVQIBAJQWE/0q2zaAJVFMg34B7//33t3VFvxnyXR0uX8+nTp2abrjhhpZTsDbffPMk7Jevf0ZEgEBMAeE/Zt9VTaDrAgMN+jns77333o2TBK9yCVx11VXpjDPOaHnylns4ZcqUxomBFwECBAiUS0D4L1c/jIZAJQXGjx+f8qo7rW7m7FlccylNQb/cLS96k+7o0aOtllTuVhodAQIEGgLCvwOBAIG2BAT9trgq9+aiN+nmqTxr166tXH0GTIAAgegCwn/0I0D9BPoREPTrf3jsuuuu6dFHHy10k+6CBQvSuHHj6o+iQgIECNRYQPivcXOVRqAdgcEE/X322aexNKdX+QVOOOGE9Mtf/rLlTbp5WtZHPvKRNHPmzPIXZYQECBAgUFhA+C9M5Y0E6iMg6Nenl60q+c53vpPe+MY3trwfI4f9I444It10002tNum/EyBAgECFBYT/CjfP0AkUERD0iyjV5z2LFy9Oe+21V1q/fn3LqTzbbbddeuKJJ+pTvEoIECBAoKWA8N+SyBsIVEdA0K9Orzo50vw043zzbV5tqa9XvrLvJt1OqtsWAQIEqikg/Fezb0ZNIAn6cQ+C3XbbLS1ZsqRl2M+Bf968eemYY46Ji6VyAgQIENhIQPh3QBCogICgX4EmdXGIRx99dGMufn9X9vPuc9g/77zz0iWXXNLF0dg0AQIECFRZQPivcveMvZYCgn4t29pWUZ/85CfTpz71qUI36U6YMCHde++9bW3fmwkQIEAgroDwH7f3Ki+BgKBfgiaUYAjXXXddOvnkkwvdpLv11lunZ555pgSjNgQCBAgQqKKA8F/FrhlzJQUE/Uq2rSuDvu+++9LBBx/sJt2u6NooAQIECPQnIPw7Pgh0QUDQ7wJqxTfZzpN0r7766jRt2rSKV2z4BAgQIFBGAeG/jF0xpkoJDCbojxs3Li1YsKBS9RpsMYHDDz88/elPf2pM5en3Csxmm6UZM2akWbNmFduwdxEgQIAAgUEICP+DwPPRmAJbbLFFyxsxe8rkFVjyL0G/3sdL87jIVbZab3+PPfZIixYtqjeI6ggQIECglALCfynbYlBlEmgn7Av6Zepcd8dS9LjIx0R+75o1a7o7IFsnQIAAAQIFBIT/AkjeEksgPwW11XrqWUTQj3dctLqq3/O4WLduXSwg1RIgQIBAJQSE/0q0ySC7JTB58uT029/+tq2wL9R1qxvl2e6JJ56Yrr/+esdFeVpiJAQIECDQIQHhv0OQNlMNgT333DM99NBDhUNd/hZg7dq11SjOKAcsMHHixJSX3yz6jY/jYsDUPkiAAAECwywg/A9zA+y+uwKjRo1KK1euLBzqttxyy7R69eruDsrWh11gzJgxadmyZYWPixEjRjSOIy8CBAgQIFB1AeG/6h00/o0EckjLV+qLXsHdZptt0lNPPUWx5gK5z6tWrSp8XOy4445p6dKlNVdRHgECBAhEFBD+I3a9RjXnK/V5HfWiYX/33XdP//3vf2skoJRNCbR7XEyYMCHde++9MAkQIECAQO0FhP/at7heBRZdXjFXnVfjmTp1arruuuvqhaCa5wi0e1xMnz49/eQnPyFJgAABAgTCCQj/4VpenYLnz5+fpkyZ0vIJqc2Kctg/77zz0iWXXFKdIo20bYHmcZG/7Wn1jU8+JvLLcdE2sw8QIECAQE0FhP+aNraKZc2ZMyflK7J5Gk+rV3ON/auvvjpNmzat1dv99woLNI+LomE/HxuOiwo33NAJECBAoKsCwn9XeW28P4F3v/vd6atf/WrLq7d5G82wP2/evHTMMceArbFA87jIJRa5sp+PDcdFjQ8IpREgQIBARwWE/45y2lh/AieffHKaO3duy0DXM+x7oFb9jynHRf17rEICBAgQKI+A8F+eXtRuJC984QvTXXfdVTjse3BS7Q6BTRbkuIjRZ1USIECAQDkFhP9y9qWSoxo7dmx65JFHCod9D9SqZJvbHnS7x8XIkSPT008/3fZ+fIAAAQIECBBoLSD8tzbyjj4Etttuu0ZIazUvO388z8vefvvt0/Lly3nWXCD3OT84rehxsdNOO6VHH3205irKI0CAAAEC5RAQ/svRh0qMot0HJ+2///7pnnvuqURtBjlwgXaPiwMPPDDdeeedA9+hTxIgQIAAAQIDFhD+B0xX/w+2++Ck008/Pf3oRz+qP0zwCh0XwQ8A5RMgQIBApQWE/0q3r/ODz8Gu1Tr7zQcntXpf50dni8Ml4LgYLnn7JUCAAAECnRUQ/jvrWbmtbbXVVikvp9nf/OzmGvuW3axcewc84G222SatWrXKcTFgQR8kQIAAAQLlFBD+y9mXro3qyCOPTDfffLNQ1zXham74pS99abrtttscF9Vsn1ETIECAAIHCAsJ/YapqvnHOnDlp+vTpLafy5DX2v/GNb6Q3v/nN1SzUqNsSuOiii9IFF1zguGhLzZsJECBAgED1BYT/6vfwORUUWX0lT+U59NBD05///OcaCihpUwJFbtTNx8URRxyRbrrpJogECBAgQIBADQWE/xo0tci66jnU5fC3Zs2aGlSshCICRZ7DkI8LD9Uqouk9BAgQIECgHgLCfwX7eM4556Qrrrii5UOU8lSeBQsWpHHjxlWwSkNuV+AVr3hFuvHGG83bbxfO+wkQIECAQCAB4b8CzX7ggQfS+PHjG6Gu1ao8M2bMSLNmzapAVYY4WIGi93Pkq/sf+chH0syZMwe7S58nQIAAAQIEKi4g/Je0gUWX4Nxxxx3T0qVLS1qFYXVaoOj9HAcccEC6++67O7172yNAgAABAgQqLiD8l6SBu+yyS3rsscdaXtnPU3nWrl1bklEbRrcFRo8enZYvX97yuMgnBatXr+72cGyfAAECBAgQqLiA8D9MDWxnqcWrr746TZs2bZhGardDKXDiiSem66+/vmXYz1N53M8xlJ2xLwIECBAgUA8B4X8I+1h0qcWjjz46/eY3vxnCkdnVcAnMnz8/TZkypdD9HOedd1665JJLhmuo9kuAAAECBAjUQED472ITR4wY0Zii0+omXVM2utiEEm666Lz9XXfdNS1evLiEFRgSAQIECBAgUFUB4b+Dncs3Wf7zn/8stATnunXrOrhnmyqzwG677ZaWLFnS8iTQ/Rxl7qKxESBAgACBeggI/4PoYztTNiy1OAjoin30rLPOSrNnz24Z9vO8/Xnz5qVjjjmmYhUaLgECBAgQIFBVAeG/zc4VnbKRp/ysXLmyza17exUF2nkOw2mnnZauuuqqKpZpzAQIECBAgEANBIT/Fk0cOXJkYwnFVvP2Tdmowf8NbZRQ9DkMeanOvISrFwECBAgQIECgDALCf68uHHbYYenWW28tNG/flI0yHMJDM4Z99tknLVq0yEng0HDbCwECBAgQINAlAeE/pVR0Cc6xY8emBx98sEutsNkyCeQbdPONuq2+8cnz9t28XabOGQsBAgQIECDQn0DY8J8D//r16/u0yaHOEpyx/ucpehI4adKk9Mc//jEWjmoJECBAgACBWgiECv9bb711Y/7+pl457LuKW4tjunARhx9+eLrllltaXt0fNWpUWrFiReHteiMBAgQIECBAoKwCocL/+PHj08KFCzf0Iof9fLV3zZo1Ze2PcXVYoNVqTU4COwxucwQIECBAgECpBEKF/yyfV+UR+Et1DHZ1MHm51XzlvtXc/bzW/o033tjVsdg4AQIECBAgQGC4BcKF/+EGt//uC7RahtPV/e73wB4IECBAgACBcgoI/+Xsi1G1IfCtb30rvfWtb215df/5z39+WrBgQRtb9lYCBAgQIECAQL0EhP969TNMNfnhWcuXL+838OcpXvfdd1/K93p4ESBAgAABAgQIpCT8OwoqITB//vw0ZcqUlsuz7rDDDmnZsmWVqMkgCRAgQIAAAQJDLSD8D7W4/RUWKPJU3Xx1f/bs2enMM88svF1vJECAAAECBAhEFRD+o3a+pHUXWYrTak0lbZ5hESBAgAABAqUXEP5L36L6D3DdunUpr9DT13KceXWeBx98MI0dO7b+GCokQIAAAQIECHRRQPjvIq5N9y+QA//atWuf86Yc9vN0nk39N6YECBAgQIAAAQIDFxD+B27nkwMQeOCBB9K+++67yav8OfTfe++9acKECQPYso8QIECAAAECBAi0EhD+Wwn57x0RyGvs//vf/95k6M/z/NesWdOR/dgIAQIECBAgQIBA3wLCv6OjqwL9Te1ZtGhR2mOPPbq6fxsnQIAAAQIECBD4n4Dw72joqkCeytPzlefy5xt8vQgQIECAAAECBIZeQPgfevNQe2yG/7333rsx7ceLAAECBAgQIEBg+ASE/+GzD7Hnnlf+85+nTZuWfvazn4WoXZEECBAgQIAAgbIJCP9l60jNxpNv5t3UNJ98IpB/HX/88Wnu3Lk1q1o5BAgQIECAAIFyCgj/5exL7UaV5/r39RCvZrFOCGrXdgURIECAAAECJRMQ/kvWkLoPZ7fddktLlixpeSIwUIfmNKP8+1FHHZV+85vfDHRTPtemwKte9ap05513psceeyw9/fTTjeVb169f3+dWWp0Mtrn7Ab991113TQ8//PCAP++DBAgQKLPA1772tfT+978/jRw5Mu2+++7pJS95STr99NPTq1/96jIP29i6KCD8dxHXplsLnHTSSen6669vnAwMRRhsnhzk6UirV69uPcBg75g6dWr61a9+Fazq/5W7xRZbeLJ02O4rnEB9BG666aZ06qmnpqVLlxYuKn9Df+GFF6aPf/zjhT/jjdUUEP6r2bdaj3qoTwiamD2/Ndh5553DXQ3uvSxrrQ+yAsU5ESiA5C0ECJRKYMqUKWn+/Pn9futaZMCzZ89Or33ta4u81XsqKCD8V7BphlxM4Jlnnkm77LJLyr934puFnicH73jHO9JXvvKVYgMp+bvOPffcNGvWrI1G2deJQL4ylB/cNmrUqJSny7zgBS9IP/nJT0peYevh5YfNLV68uM9vn5wItDb0DgIEhkfgRz/6UTrnnHPSU089tdEA8s/rfDLQ17e5V111VfrhD3+Y/va3v6WHHnooPf744xv9DBw7dmzj33vVT0D4r19PVVRQ4F3velfKcyGb040GO+2oecPyNtts0/iqdcSIEQVHMnxv22mnndKyZcs2DCAH++jToVqdCGy77bZpxYoVw9c0eyZAgEBK6dBDD0133HHHcy5a5Itec+bMSYcddljbTpMmTUq33nrrRp/zLUDbjKX/gPBf+hYZ4HAJ5JuTH3300Y6cHDRPDMr0dOPeKzC9+MUvTrfffvtwcZdyv/2dCAz2ZLGUBRsUAQKlFrj44ovTRz/60bRq1aqNxpm/nTz77LOf8y3uQIvp/ffDIYcc0jjR8KqHgPBfjz6qYogF1q5d25j6kn/Pr3aD4HCfDPT+wd7u+IeYuxS729SJALdStMYgCNReYPz48en+++9/zt81e+21V/rPf/7TlfrzNwvNC0J5haDbbrutK/ux0aEXEP6H3tweAwhMnjw5/e53vyv8rUFzjn1/S2N2iq33U5eHYp+dGnsZtuOKWBm6YAwE6i9wwQUXpC9+8YuNZZN7vvKU0ve9733ps5/9bFcRev5dkS929b6noKs7t/GuCgj/XeW1cQIbC+SvZovefJx/8L7pTW9Kl112WUcYDz/88PTHP/5xw7by9gX/gdHmvwjzjeTNV77PIz/bwIsAAQKDEbjrrrvScccd95wbbfPP6/333z/dcsstaYcddhjMLlp+9s9//nN62ctetuF92223XXryySdbfs4bqiMg/FenV0ZaQ4F8BSdPHSoyfST/8D/iiCNSXr+53VfeT8+rR2PGjGncz+A1cIF8j8Rf//pXJ1MDJ/RJAgT+v8AHPvCBxlX+3hdk8oWFz33ucykvUDEUryuvvHKjJT67Oa1oKOqxj00LCP+ODAIlEsirNOQn5BY9GchXgu65555+K+g9TSUvCXf55ZeXqOpqD8U0qmr3z+gJDLdAXkGs5zeH+WdKXnWn5ze1QzHGj33sY2nmzJkbdvXSl770OSv/DMU47KP7AsJ/943tgcCABfbbb7+0cOHCwicD+Yr+kiVLNuwvP8m45wpDRU4qBjzYwB/seQLAOPCBoHQCbQjkNfbzg7SaPzPyN7Tf/e530xlnnNHGVjrz1le/+tUbPbMljyGPz6ueAsJ/PfuqqpoKHHnkkenmm28udDLQk8D8/u4dEPkEbcGCBRt2IPx3z9qWCdRF4DWvec1G4fqggw5Kd95557CUl1fy6bmM5yc+8Yl04YUXDstY7HRoBIT/oXG2FwJdETjrrLNSfgBLkcA53MuLdgVgmDfae0qVk6xhbojdE6iAQH5y7sMPP7xhpHm+/+c///lhGXlewrjnU3x/8IMfpHxi4lVvAeG/3v1VXUCB3oG0L4Lec9UnTpyY8koTXq0Fet9AnT/h6cit3byDQGSBvGZ+XkWnORUz/6z+/e9/n17+8pcPC0texafn8p1FLiINy0DttOMCwn/HSW2QwPALNIN9XuFn6623Lry86KZG3vskYfTo0aFXCurpkb1c7R/+490ICJRdIE+l+eQnP7lhmMO94lpeRWjlypUbxiP4l/0I6uz4hP/OetoagVIINAPqpn6gt/OsgXaLae43/z5y5MhKPRQmP1DnO9/5TuPEpq/lV3t7TpgwId13333tMnk/AQKBBE466aR07bXXbqj4lFNOST/72c+GVODSSy9N559/flq1atVG+3XxYkjbUJqdCf+laYWBEOicQDOE77jjjmnZsmWFN3zmmWemn/70p42vpZtBtxtXhHqeJOSTkdNOO23AK0sMJLQXBunjjf7CHKygzxOIIXD33XenQw45ZMNzVr797W+nGTNmDEnx48ePT/fff3+f94Tl58bkaUde8QSE/3g9V3EAgZ5TU/LVnosvvrjjVee1qfPXxt08Sej4oAe5QaF/kIA+TiCYwLHHHpt+/etfpzy/v+eyy91g+MxnPtOYWtTzgY699/O85z2vcaHlhBNO6MYQbLMiAsJ/RRplmATaEbjsssvSW9/61g0f6cbV+yLjyU+r3HXXXRvfPvQcw1CPp/c8/Tz2/O/ycxB23nnndPbZZ6fPfvazRUryHgIECBQWyNMf81SbF73oRRs9EbzwBlq8cc8992ys1tPXz9R80nHcccel6667rlO7tJ0aCAj/NWiiEghsSqD3Em7ve9/70vTp09PkyZNLCXbAAQekf/3rXy2XLRXaS9k+gyJAoJdAXs4zL+uZX3PmzEknn3zyoI0++MEPpi996UuN+5L6eu2www7p8ccfH/S+bKC+AsJ/fXurMgKNr5r7u8qeg3Re9WHWrFkpz/f3IkCAAIHOCMycOTN97GMfS3lJzSeffHLAG+19Iaf3hvLP+VNPPXWjJ/QOeGc+GEJA+A/RZkVGFjj44IPbXr8/nxTss88+jZvFvAgQIECgfYF8Q+0f/vCHdPjhhzeezF709e53vzt97Wtf6/cegbzk8tKlS4tu0vsIbCQg/DsgCAQTGDduXPrPf/7TcnpNb5Y8Pz6vzLOpf5/nte60005p9913T4ceemj68pe/HExVuQQIENhYYPvtt08rVqxIn/70p9NHP/rRfnl22WWXfp+fkn/2nnvuuemb3/wmZgKDFhD+B01oAwSqL5BXf8h/sTz99NNtnxR0qvpNPTwrn3DkJ+c6ueiUsu0QIDBUAnm+f573v3jx4rTbbrtttNvXv/716corr0x5UYS+Xvkz+bNeBDotIPx3WtT2CNRIIC8HN2/evD4felWlUpsnF/kK2qhRo9Jee+2VTjzxxPSFL3yhSmUYKwECFRHIT/XNr+bveapOfzfi5osdb3/729Mll1xSkQoNs6oCwn9VO2fcBEoukNea/sAHPpBuv/329OCDDzbmp+bnAuR/v6kn6A718p9F+PIJQ/41YsSIlNfHnjhxYpo/f36Rj3oPAQLBBfJNuNdcc02/V/fzVMn889GLwFAKCP9DqW1fBFoxoNQAABbLSURBVAh0VOC9731vuu222xrrXJfh5KL57UJefSNPVcrPODj66KPTFVdc0dG6bYwAgXIK5GU2n3jiiT4Hl6/uf+hDH0p5JSAvAsMlIPwPl7z9EiBQCoF//OMf6YILLmicRDz22GONB/LkebhD9U1E89uFHAryDYLjx49PeT5wPrHxIkCg3AKvfOUrGw/Q6mvufv7/Oy+ycNdddzWWVfYiUAYB4b8MXTAGAgQqJZBXM/r+97+fFixY0Fi/uzmNaShPGPK3C1tvvXUaM2ZMmjRpUuNK4oEHHlgpR4MlUDWBHPRPP/309NRTT/U59DxNMK/wkx/I5UWgjALCfxm7YkwECNRK4JFHHkkf/vCHG/cL5ClK+d6H5pXCoThh6Hmz87bbbtu42XnatGmNBxDlf/YiQKBvgSOPPLKxTn9f/6/m/7/yN3Z/+9vfXN13IFVCQPivRJsMkgCBaAI33HBD42p+npa0fPnytHr16kb4GIqThWzd82bnPI85f6uQ1yo/9thjo7VCvcEEfvzjH6c3vOEN6Zlnnumz8vytW14p7J3vfGcwHeXWQUD4r0MX1UCAQHiBPA0hTzWYM2dOWrRoUWNawrp16xouQ3HC0Ptm57yKyTHHHJP+7//+r3HjsxeBMgvkhxPecccd/V7dP+igg9Lf//73MpdhbAQKCQj/hZi8iQABAvUTcLNz/XqqomIC+Um573nPexpT8Pp65RW7Lr/88vS6172u2Ea9i0BFBIT/ijTKMAkQIFAGgYsuuqjxZNKFCxemFStWNJ7bMFTfLuT95G8Ymjc754cmvexlL0t5mkZeLcmruMDPf/7zxpKU+Yb13Mf8TVF+wnf+PQfi/CuvfJV/z1PO8q/8TVLud/NZHflG9/zn/O/zn/Pv+V6W/Kv55/ytU/5zc8paq9+bFWxqilvPb7AG8m1W/kw+fvqbu//iF7+4sfKXF4E6Cwj/de6u2ggQIFACgRkzZqTf/va3Kd/4PNQ3O5egfEMosUC+4T2v3JUfyOVFIIqA8B+l0+okQIBARQTyFeepU6eme+65p3F1eqhvdq4Ik2EOQCAvw5m/LbrpppsG8GkfIVAPAeG/Hn1UBQECBEILnH/++enaa69t3Oycp6/0vNm5eTNyO0A9P9P7882VkJrba/5zq9+32GKLDdOW8p/z9KX8K/85T1vKv2+11VaNP+df+c/5V/73ObTmX3keel5pJv+ef+Ur16NGjWr8vt122zUeFPe85z0vnXLKKe2U670ECAQSEP4DNVupBAgQIECAAAECsQWE/9j9Vz0BAgQIECBAgEAgAeE/ULOVSoAAAQIECBAgEFtA+I/df9UTIECAAAECBAgEEhD+AzVbqQQIECBAgAABArEFhP/Y/Vc9AQIECBAgQIBAIAHhP1CzlUqAAAECBAgQIBBbQPiP3X/VEyBAgAABAgQIBBIQ/gM1W6kECBAgQIAAAQKxBYT/2P1XPQECBAgQIECAQCAB4T9Qs5VKgAABAgQIECAQW0D4j91/1RMgQIAAAQIECAQSEP4DNVupBAgQIECAAAECsQWE/9j9Vz0BAgQIECBAgEAgAeE/ULOVSoAAAQIECBAgEFtA+I/df9UTIECAAAECBAgEEhD+AzVbqQQIECBAgAABArEFhP/Y/Vc9AQIECBAgQIBAIAHhP1CzlUqAAAECBAgQIBBbQPiP3X/VEyBAgAABAgQIBBIQ/gM1W6kECBAgQIAAAQKxBYT/2P1XPQECBAgQIECAQCAB4T9Qs5VKgAABAgQIECAQW0D4j91/1RMgQIAAAQIECAQSEP4DNVupBAgQIECAAAECsQWE/9j9Vz0BAgQIECBAgEAgAeE/ULOVSoAAAQIECBAgEFtA+I/df9UTIECAAAECBAgEEhD+AzVbqQQIECBAgAABArEFhP/Y/Vc9AQIECBAgQIBAIAHhP1CzlUqAAAECBAgQIBBbQPiP3X/VEyBAgAABAgQIBBIQ/gM1W6kECBAgQIAAAQKxBYT/2P1XPQECBAgQIECAQCAB4T9Qs5VKgAABAgQIECAQW0D4j91/1RMgQIAAAQIECAQSEP4DNVupBAgQIECAAAECsQWE/9j9Vz0BAgQIECBAgEAgAeE/ULOVSoAAAQIECBAgEFtA+I/df9UTIECAAAECBAgEEhD+AzVbqQQIECBAgAABArEFhP/Y/Vc9AQIECBAgQIBAIAHhP1CzlUqAAAECBAgQIBBbQPiP3X/VEyBAgAABAgQIBBIQ/gM1W6kECBAgQIAAAQKxBYT/2P1XPQECBAgQIECAQCAB4T9Qs5VKgAABAgQIECAQW0D4j91/1RMgQIAAAQIECAQSEP4DNVupBAgQIECAAAECsQWE/9j9Vz0BAgQIECBAgEAgAeE/ULOVSoAAAQIECBAgEFtA+I/df9UTIECAAAECBAgEEhD+AzVbqQQIECBAgAABArEFhP/Y/Vc9AQIECBAgQIBAIAHhP1CzlUqAAAECBAgQIBBbQPiP3X/VEyBAgAABAgQIBBIQ/gM1W6kECBAgQIAAAQKxBYT/2P1XPQECBAgQIECAQCAB4T9Qs5VKgAABAgQIECAQW0D4j91/1RMgQIAAAQIECAQSEP4DNVupBAgQIECAAAECsQWE/9j9Vz0BAgQIECBAgEAgAeE/ULOVSoAAAQIECBAgEFtA+I/df9UTIECAAAECBAgEEhD+AzVbqQQIECBAgAABArEFhP/Y/Vc9AQIECBAgQIBAIAHhP1CzlUqAAAECBAgQIBBbQPiP3X/VEyBAgAABAgQIBBIQ/gM1W6kECBAgQIAAAQKxBYT/2P1XPQECBAgQIECAQCAB4T9Qs5VKgAABAgQIECAQW0D4j91/1RMgQIAAAQIECAQSEP4DNVupBAgQIECAAAECsQWE/9j9Vz0BAgQIECBAgEAgAeE/ULOVSoAAAQIECBAgEFtA+I/df9UTIECAAAECBAgEEhD+AzVbqQQIECBAgAABArEFhP/Y/Vc9AQIECBAgQIBAIAHhP1CzlUqAAAECBAgQIBBbQPiP3X/VEyBAgAABAgQIBBIQ/gM1W6kECBAgQIAAAQKxBYT/2P1XPQECBAgQIECAQCAB4T9Qs5VKgAABAgQIECAQW0D4j91/1RMgQIAAAQIECAQSEP4DNVupBAgQIECAAAECsQWE/9j9Vz0BAgQIECBAgEAgAeE/ULOVSoAAAQIECBAgEFtA+I/df9UTIECAAAECBAgEEhD+AzVbqQQIECBAgAABArEFhP/Y/Vc9AQIECBAgQIBAIAHhP1CzlUqAAAECBAgQIBBbQPiP3X/VEyBAgAABAgQIBBIQ/gM1W6kECBAgQIAAAQKxBYT/2P1XPQECBAgQIECAQCAB4T9Qs5VKgAABAgQIECAQW0D4j91/1RMgQIAAAQIECAQSEP4DNVupBAgQIECAAAECsQWE/9j9Vz0BAgQIECBAgEAgAeE/ULOVSoAAAQIECBAgEFtA+I/df9UTIECAAAECBAgEEhD+AzVbqQQIECBAgAABArEFhP/Y/Vc9AQIECBAgQIBAIAHhP1CzlUqAAAECBAgQIBBbQPiP3X/VEyBAgAABAgQIBBIQ/gM1W6kECBAgQIAAAQKxBYT/2P1XPQECBAgQIECAQCAB4T9Qs5VKgAABAgQIECAQW0D4j91/1RMgQIAAAQIECAQSEP4DNVupBAgQIECAAAECsQWE/9j9Vz0BAgQIECBAgEAgAeE/ULOVSoAAAQIECBAgEFtA+I/df9UTIECAAAECBAgEEhD+AzVbqQQIECBAgAABArEFhP/Y/Vc9AQIECBAgQIBAIAHhP1CzlUqAAAECBAgQIBBbQPiP3X/VEyBAgAABAgQIBBIQ/gM1W6kECBAgQIAAAQKxBYT/2P1XPQECBAgQIECAQCAB4T9Qs5VKgAABAgQIECAQW0D4j91/1RMgQIAAAQIECAQSEP4DNVupBAgQIECAAAECsQWE/9j9Vz0BAgQIECBAgEAgAeE/ULOVSoAAAQIECBAgEFtA+I/df9UTIECAAAECBAgEEhD+AzVbqQQIECBAgAABArEFhP/Y/Vc9AQIECBAgQIBAIAHhP1CzlUqAAAECBAgQIBBbQPiP3X/VEyBAgAABAgQIBBIQ/gM1W6kECBAgQIAAAQKxBYT/2P1XPQECBAgQIECAQCAB4T9Qs5VKgAABAgQIECAQW0D4j91/1RMgQIAAAQIECAQSEP4DNVupBAgQIECAAAECsQWE/9j9Vz0BAgQIECBAgEAgAeE/ULOVSoAAAQIECBAgEFtA+I/df9UTIECAAAECBAgEEhD+AzVbqQQIECBAgAABArEFhP/Y/Vc9AQIECBAgQIBAIAHhP1CzlUqAAAECBAgQIBBbQPiP3X/VEyBAgAABAgQIBBIQ/gM1W6kECBAgQIAAAQKxBYT/2P1XPQECBAgQIECAQCAB4T9Qs5VKgAABAgQIECAQW0D4j91/1RMgQIAAAQIECAQSEP4DNVupBAgQIECAAAECsQWE/9j9Vz0BAgQIECBAgEAgAeE/ULOVSoAAAQIECBAgEFtA+I/df9UTIECAAAECBAgEEhD+AzVbqQQIECBAgAABArEFhP/Y/Vc9AQIECBAgQIBAIAHhP1CzlUqAAAECBAgQIBBbQPiP3X/VEyBAgAABAgQIBBIQ/gM1W6kECBAgQIAAAQKxBYT/2P1XPQECBAgQIECAQCAB4T9Qs5VKgAABAgQIECAQW0D4j91/1RMgQIAAAQIECAQSEP4DNVupBAgQIECAAAECsQWE/9j9Vz0BAgQIECBAgEAgAeE/ULOVSoAAAQIECBAgEFtA+I/df9UTIECAAAECBAgEEhD+AzVbqQQIECBAgAABArEFhP/Y/Vc9AQIECBAgQIBAIAHhP1CzlUqAAAECBAgQIBBbQPiP3X/VEyBAgAABAgQIBBIQ/gM1W6kECBAgQIAAAQKxBYT/2P1XPQECBAgQIECAQCAB4T9Qs5VKgAABAgQIECAQW0D4j91/1RMgQIAAAQIECAQSEP4DNVupBAgQIECAAAECsQWE/9j9Vz0BAgQIECBAgEAgAeE/ULOVSoAAAQIECBAgEFtA+I/df9UTIECAAAECBAgEEhD+AzVbqQQIECBAgAABArEFhP/Y/Vc9AQIECBAgQIBAIAHhP1CzlUqAAAECBAgQIBBbQPiP3X/VEyBAgAABAgQIBBIQ/gM1W6kECBAgQIAAAQKxBYT/2P1XPQECBAgQIECAQCAB4T9Qs5VKgAABAgQIECAQW0D4j91/1RMgQIAAAQIECAQSEP4DNVupBAgQIECAAAECsQWE/9j9Vz0BAgQIECBAgEAgAeE/ULOVSoAAAQIECBAgEFtA+I/df9UTIECAAAECBAgEEhD+AzVbqQQIECBAgAABArEFhP/Y/Vc9AQIECBAgQIBAIAHhP1CzlUqAAAECBAgQIBBbQPiP3X/VEyBAgAABAgQIBBIQ/gM1W6kECBAgQIAAAQKxBYT/2P1XPQECBAgQIECAQCAB4T9Qs5VKgAABAgQIECAQW0D4j91/1RMgQIAAAQIECAQSEP4DNVupBAgQIECAAAECsQWE/9j9Vz0BAgQIECBAgEAgAeE/ULOVSoAAAQIECBAgEFtA+I/df9UTIECAAAECBAgEEhD+AzVbqQQIECBAgAABArEFhP/Y/Vc9AQIECBAgQIBAIAHhP1CzlUqAAAECBAgQIBBbQPiP3X/VEyBAgAABAgQIBBIQ/gM1W6kECBAgQIAAAQKxBYT/2P1XPQECBAgQIECAQCAB4T9Qs5VKgAABAgQIECAQW0D4j91/1RMgQIAAAQIECAQSEP4DNVupBAgQIECAAAECsQWE/9j9Vz0BAgQIECBAgEAgAeE/ULOVSoAAAQIECBAgEFtA+I/df9UTIECAAAECBAgEEhD+AzVbqQQIECBAgAABArEFhP/Y/Vc9AQIECBAgQIBAIAHhP1CzlUqAAAECBAgQIBBbQPiP3X/VEyBAgAABAgQIBBIQ/gM1W6kECBAgQIAAAQKxBYT/2P1XPQECBAgQIECAQCAB4T9Qs5VKgAABAgQIECAQW0D4j91/1RMgQIAAAQIECAQSEP4DNVupBAgQIECAAAECsQWE/9j9Vz0BAgQIECBAgEAgAeE/ULOVSoAAAQIECBAgEFtA+I/df9UTIECAAAECBAgEEhD+AzVbqQQIECBAgAABArEFhP/Y/Vc9AQIECBAgQIBAIAHhP1CzlUqAAAECBAgQIBBbQPiP3X/VEyBAgAABAgQIBBIQ/gM1W6kECBAgQIAAAQKxBYT/2P1XPQECBAgQIECAQCAB4T9Qs5VKgAABAgQIECAQW0D4j91/1RMgQIAAAQIECAQSEP4DNVupBAgQIECAAAECsQWE/9j9Vz0BAgQIECBAgEAgAeE/ULOVSoAAAQIECBAgEFtA+I/df9UTIECAAAECBAgEEhD+AzVbqQQIECBAgAABArEFhP/Y/Vc9AQIECBAgQIBAIAHhP1CzlUqAAAECBAgQIBBbQPiP3X/VEyBAgAABAgQIBBIQ/gM1W6kECBAgQIAAAQKxBYT/2P1XPQECBAgQIECAQCAB4T9Qs5VKgAABAgQIECAQW0D4j91/1RMgQIAAAQIECAQSEP4DNVupBAgQIECAAAECsQWE/9j9Vz0BAgQIECBAgEAgAeE/ULOVSoAAAQIECBAgEFtA+I/df9UTIECAAAECBAgEEhD+AzVbqQQIECBAgAABArEFhP/Y/Vc9AQIECBAgQIBAIAHhP1CzlUqAAAECBAgQIBBbQPiP3X/VEyBAgAABAgQIBBIQ/gM1W6kECBAgQIAAAQKxBYT/2P1XPQECBAgQIECAQCAB4T9Qs5VKgAABAgQIECAQW0D4j91/1RMgQIAAAQIECAQSEP4DNVupBAgQIECAAAECsQWE/9j9Vz0BAgQIECBAgEAgAeE/ULOVSoAAAQIECBAgEFtA+I/df9UTIECAAAECBAgEEhD+AzVbqQQIECBAgAABArEFhP/Y/Vc9AQIECBAgQIBAIAHhP1CzlUqAAAECBAgQIBBbQPiP3X/VEyBAgAABAgQIBBIQ/gM1W6kECBAgQIAAAQKxBYT/2P1XPQECBAgQIECAQCAB4T9Qs5VKgAABAgQIECAQW0D4j91/1RMgQIAAAQIECAQSEP4DNVupBAgQIECAAAECsQWE/9j9Vz0BAgQIECBAgEAgAeE/ULOVSoAAAQIECBAgEFtA+I/df9UTIECAAAECBAgEEhD+AzVbqQQIECBAgAABArEFhP/Y/Vc9AQIECBAgQIBAIAHhP1CzlUqAAAECBAgQIBBbQPiP3X/VEyBAgAABAgQIBBIQ/gM1W6kECBAgQIAAAQKxBYT/2P1XPQECBAgQIECAQCAB4T9Qs5VKgAABAgQIECAQW0D4j91/1RMgQIAAAQIECAQSEP4DNVupBAgQIECAAAECsQWE/9j9Vz0BAgQIECBAgEAgAeE/ULOVSoAAAQIECBAgEFtA+I/df9UTIECAAAECBAgEEhD+AzVbqQQIECBAgAABArEFhP/Y/Vc9AQIECBAgQIBAIAHhP1CzlUqAAAECBAgQIBBbQPiP3X/VEyBAgAABAgQIBBIQ/gM1W6kECBAgQIAAAQKxBYT/2P1XPQECBAgQIECAQCAB4T9Qs5VKgAABAgQIECAQW0D4j91/1RMgQIAAAQIECAQSEP4DNVupBAgQIECAAAECsQWE/9j9Vz0BAgQIECBAgEAgAeE/ULOVSoAAAQIECBAgEFtA+I/df9UTIECAAAECBAgEEvh/1+ktkNeVMzkAAAAASUVORK5CYII=', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAv8AAAHaCAYAAACXciY6AAAgAElEQVR4Xu3dWaglV9k/4PLrTncmYpJOdwdFBaMGIQSFGBxJRDQQA0EIXgjeeqUIErxzwAtBQQRBvPdGECEKIeBM1CBBRE1EQjSCitpTOm0MPZ5z+s86/uukemcPVbVqWKvqOfBhTNeqetfzrvb77Tqrar/q8uXLlws/BAgQIECAAAECBAhMXuBVwv/ke2yCBAgQIECAAAECBHYFhH8LgQABAgQIECBAgMBMBIT/mTTaNAkQIECAAAECBAgI/9YAAQIECBAgQIAAgZkICP8zabRpEiBAgAABAgQIEBD+rQECBAgQIECAAAECMxEQ/mfSaNMkQIAAAQIECBAgIPxbAwQIECBAgAABAgRmIiD8z6TRpkmAAAECBAgQIEBA+LcGCBAgQIAAAQIECMxEQPifSaNNkwABAgQIECBAgIDwbw0QIECAAAECBAgQmImA8D+TRpsmAQIECBAgQIAAAeHfGiBAgAABAgQIECAwEwHhfyaNNk0CBAgQIECAAAECwr81QIAAAQIECBAgQGAmAsL/TBptmgQIECBAgAABAgSEf2uAAAECBAgkJPB///d/u9Xs7OwkVJVSCBCYioDwP5VOmgcBAgQIZCewf//+Ynt7e2ndr3rVq3wAyK6jCiaQvoDwn36PVEiAAAECExQId/gvX768dmab/nyCLKZEgEDPAsJ/z8BOT4AAAQIEFgVe//rXF//4xz/2/nW4yx/+L/wWoPqhQPi3dggQ6FpA+O9a1PkIECBAgMAGgRD0y5/FgC/8Wz4ECPQpIPz3qevcBAgQIEBgQaAa/F/3utcVf//73684Yt0HA5gECBCIFRD+YwWNJ0CAAAECNQX27du39xDvqgd6hf+amA4jQKCVgPDfis0gAgQIECDQTGBxn/+q/fxl+Pe2n2a+jiZAoJ6A8F/PyVEECBAgQCBKoO4dfeE/itlgAgQ2CAj/lggBAgQIEOhZoPoQb9j6s7W1tfKKwn/PzXB6AjMXEP5nvgBMnwABAgT6Faizz79aQRn+N31I6LdqZydAYKoCwv9UO2teBAgQIJCEQN3tPmWx5fHe8Z9E+xRBYHICwv/kWmpCBAgQIJCKwKbXei6rU/hPpXvqIDBNAeF/mn01KwIECBAYWWD//v2739gbfsKe//Kf15XlC75GbprLE5iBgPA/gyabIgECBAgML9AmyDfdIjT8rFyRAIHcBYT/3DuofgIECBBIUqDpW3uqHxaOHj1aHDt2LMl5KYoAgbwFhP+8+6d6AgQIEEhQoLrlp+6Du00/LCQ4bSURIJCBgPCfQZOUSIAAAQJ5CTTd8tP0+Lw0VEuAQEoCwn9K3VALAQIECExCoMld/FtvvbU4fvz47rzDuJ2dnUkYmAQBAmkKCP9p9kVVBAgQIJCpQNMtPx7yzbTRyiaQqYDwn2njlE2AAAECaQo02cLjrn+aPVQVgSkLCP9T7q65ESBAgMDgAk22/LjrP3h7XJDA7AWE/9kvAQAECBAg0JVAky0/1WP37dtXbG1tdVWG8xAgQGClgPBvcRAgQIAAgY4Emmz5cde/I3SnIUCgkYDw34jLwQQIECBAYLVA3S0/1Q8J7vpbUQQIDCkg/A+p7VoECBAgMFmBJlt+3PWf7DIwMQLJCwj/ybdIgQQIECCQg0DdLT/V444ePVocO3Ysh+mpkQCBiQgI/xNppGkQIECAwLgCdbf81D1u3Nm4OgECUxUQ/qfaWfMiQIAAgUEFylB/+fLlldet+9uBQQt3MQIEZiUg/M+q3SZLgAABAn0JbAr/vtCrL3nnJUCgiYDw30TLsQQIECBAYIXApvDvIV9LhwCBFASE/xS6oAYCBAgQyF5gXfh31z/79poAgckICP+TaaWJECBAgMCYAuvCv7v+Y3bGtQkQqAoI/9YDAQIECBDoQGBV+K++/98XenUA7RQECEQJCP9RfAYTIECAAIH/CZThP7zRZ3t7e4/FXX8rhACBlASE/5S6oRYCBAgQyFZg2fv7q6/2dNc/29YqnMCkBIT/SbXTZAgQIEBgLIFl4d9d/7G64boECKwSEP6tDQIECBAg0IHAYviv3vU/evRocezYsQ6u4hQECBCIExD+4/yMJkCAAAECuwKL4X/ZbwJQESBAYGwB4X/sDrg+AQIECExCoBr2w4QuX768O6/yPycxSZMgQCB7AeE/+xaaAAECBAikIFDd31/WE/7dzs5OCuWpgQABArsCwr+FQIAAAQIEOhBYFv7d9e8A1ikIDCgQ3soV/t5O+UO78D/ggnIpAgQIEJiuwGL4d9d/ur02s/wFrr/++uLs2bNrt+UtfmdH/rP+3wyE/6l00jwIECBAYFSBxfDvrv+o7XBxArsC73nPe4pf//rXrZ+9meIHAOHfXw4CBAgQINCBQDX8+0KvDkCdgkBDgRDUw0/TD97l393qVp/q3+epfQAQ/hsuLIcTIECAAIFlAr7Qy7ogMIxAuS+/bch/17veVTzxxBNri73rrruK3/72t7vH3H333cWTTz45zOQGuIrwPwCySxAgQIDAtAWqX+jV5s7jtHXMjkA7gRtuuKF46aWXGt/JD1cLH8avvfba3fFNft7+9rcXf/jDH6645tSe3xH+m6wIxxIgQIAAgSUC9vtbFgTaC9xzzz3FL3/5y90TtLmbH/7+bW9vty5gWeAvT3bNNdfsPhg8pR/hf0rdNBcCBAgQGFxg8a5/mwAzeNEuSGAkgdgtO129gnNd4C9/c9DVtUaiXnlZ4T+1jqiHAAECBLIS8H7/rNql2IEE2m7ZKf8+ve997ysef/zxTqutE/gPHjxYnDt3rtPrpnYy4T+1jqiHAAECBLIRqN71D/9c3ilsunUhmwkrlMCCQOyWnfC+/RdffLE3V4H/lbTCf2/LzYkJECBAYMoCt956a3H8+PHdKZYPBJZ3Laf2gOCU+2hu9QVuvPHG3aDe9MPtsldp1r9q8yMF/vVmwn/zNWUEAQIECBDYDfzlTxmGqr8JaBqQkBJITWD//v27v82qu5b73LKzyUbg3yT08p8L//WtHEmAAAECBHYFlt31L2nc/bdIchVoEvbDOu97y84mR4F/k9DyPxf+27kZRYAAAQIzFlj3hV6+7GvGCyOzqS97U9WyKYQ1HY7d2toafYYCf3wLhP94Q2cgQIAAgRkJhLuj5TvFw2sLFwPRm970puK5557bFVn25zOiMtWEBD7wgQ8UP//5z2tt4QlhP7yt58yZM0nMQODvtg3Cf7eezkaAAAECExeoc2e/zjETZzK9kQWaPJwb1uv73//+4qc//enIVb98eYG/v1YI//3ZOjMBAgQITEyguk1i3V396m8HbrvttuIvf/nLxCRMJzWBpvv1U/wCK4F/mFUl/A/j7CoECBAgMAGBJnf0mxw7ARpTGFggfBANP5vexDP0azabMgj8TcXijxf+4w2dgQABAgRmIFC963/06NHi2LFja2fttZ8zWBQDTbHpfv1UHs5dxSPwD7RwVlxG+B/X39UJECBAIBOBNq/wbDMmEw5l9ijQNOyn9HCuwN/jwujo1MJ/R5BOQ4AAAQLTFWh7F7/tuOlKmtkygdwfzhX481rXwn9e/VItAQIECAwssO4LveqU4u5/HaV5HTOFh3MF/nzXrPCfb+9UToAAAQIDCMQ+uBs7foApukTPAjl+mVYTEnv4m2iNf6zwP34PVECAAAECiQrE3vUP0/KlX4k2t6eymu7XT/3hXHf4e1ooI55W+B8R36UJECBAIG2Bru7ad3WetLXmWV3T/fo5PJwr8E97LQv/0+6v2REgQIBAS4HqF3Wt+0KvOqf3pV91lPI4pmnYT+2bc5sq29LTVCz944X/9HukQgIECBAYQaDru/Vdn28EkllecsoP57rDP8slXQj/8+y7WRMgQIDAGoHqA5qxd/3Ly1TPGQLlpUuX9CBBgak/nCvwJ7joBi5J+B8Y3OUIECBAIG2BEPZ3dnZ2iwx368t/7qLq6t3/N7/5zcWzzz7bxWmdo6VA04dzc96vv4zIlp6WCyfzYcJ/5g1UPgECBAh0J3D06NHixIkTeye8fPlydycviuItb3lL8ec//7m383da7ARPdvPNNxdnzpwp6vQ1fFCbWtgPLRX4J7iwG05J+G8I5nACBAgQmK7AEPvy+/zNwnQ7025mTcN+7g/nrlIS+Nutn6mOEv6n2lnzIkCAAIFGAtXgf+TIkeL48eONxjc5uLqvPPzz9vZ2k+GOXSHQNOx3uaUrtaYI/Kl1JJ16hP90eqESAgQIEBhJYIwwbv9/fLPrhv1gHX7jMvWHrAX++DU1hzMI/3PosjkSIECAwEqBavDv+gHfTexDbDPaVENOfy7sv7JbAn9OKziNWoX/NPqgCgIECBAYQaC6/z5cvs6DoF2W6QHg9ZrC/nIfgb/Lv4XzO5fwP7+emzEBAgQIFEXR95t96iKP+ZuHujUOdZywv1pa4B9qFU7/OsL/9HtshgQIECCwRGDIB3w3NWCuXwAm7K9fGeE3U+G3Uat+IxXW8MGDB4tz585tWmL+nMCegPBvMRAgQIDA7ASqwb+rb/CNRZzDA8BXXXXV7puNNm2vmssDutU1Ez4Ahp86NgJ/7N+2eY8X/ufdf7MnQIDA7ATGeLNPHeQp7v8X9l/Z+TvvvLP44x//uDHkV0e6w1/nb5Bj6goI/3WlHEeAAAEC2Quk/gVbqde3aQE0Cfs33nhjcfr06U2nzPrPr7766uLixYuNg36Y9B133FE89dRTWc9f8WkKCP9p9kVVBAgQINCxQCoP+G6aVqq/mVhWt7D/ssqm/fnL/MqtXlP+srFN692fDy8g/A9v7ooECBAgMLBALsG/ZEl1/7+w/78O1d2fX13moacHDhwozp8/P/DqdzkCVwoI/1YEAQIECExeIKU3+9TFTuELwOYe9tvszw/9Db2zbafuSnfc0ALC/9DirkeAAAECgwrktI2mCjPGA8BzDvsx+/Nt2xn0r7SLRQoI/5GAhhMgQIBAugK5f4FW3/XX3b4S7mRP6QHdtvvzg0N4VakfAjkLCP85d0/tBAgQILBSIPc355QTq34AqP67NiF02bmWAU4p7Nf9gFN1sD/f/7BMWUD4n3J3zY0AAQIzFcjtAd9NbVoV2kNI3b9//+7rJJf9zCnst9m2E8zsz9+0+vz51ASE/6l11HwIECBAYDfQlT9Hjhwpjh8/PgmV6m8zFidUznnTN8SWgfeDH/xg8cMf/jBLl7bbdsJk7c/PsuWK7lBA+O8Q06kIECBAYHyBqQb/Rdm6d/VzD/ttt+3Ynz/+30UVpCkg/KfZF1URIECAQAuBXN/sU3eqTQJ/ec4QgnO4212+VjPUXee3F9X5eX9+3RXkOAJFIfxbBQQIECAwCYGpPOBbbUbdsB8CftjG87e//a149tln14bn6m9GFhs/1N1y+/Mn8VfOJDIVEP4zbZyyCRAgQOBlgfDQa/XtN03uHKfk2DTsr9uzH+6Gb21tNbqLnopF+QElh99YpGKmDgJ1BYT/ulKOI0CAAIEkBXJ9s899991X/PjHP64Vzss7+20f0E35g0CY21C/cUhyASuKwMACwv/A4C5HgAABAt0K5PKA75Bhv1vhl8928ODB4tKlS2tPv+y3LqFH4d97f35fnXFeAvUFhP/6Vo4kQIAAgcQEqsE/bJlp88VXfU1pCmG/LxvnJUBgPAHhfzx7VyZAgACBCIHq/vgU3mjTNOzbzx7RfEMJEGgtIPy3pjOQAAECBMYSSOHNPsL+WN13XQIEYgSE/xg9YwkQIEBgcIGxHvAV9gdvtQsSINCDgPDfA6pTEiBAgEB/AtV9/n2+0lPY76+HzkyAwHgCwv949q5MgAABAg0F+nyzj7DfsBkOJ0AgSwHhP8u2KZoAAQLzE6g+4NvFm30eeOCB4rHHHqv9nn0P6M5vzZkxgSkKCP9T7Ko5ESBAYGICXTzgK+xPbFGYDgECrQSE/1ZsBhEgQIDAUAL79++/4v39dff51w375VYid/aH6qjrECAwpoDwP6a+axMgQIDAWoEmb/apG/bDBVP4XgCtJ0CAwBgCwv8Y6q5JgAABArUENj3gW30OYN0Jhf1a3A4iQGAGAsL/DJpsigQIEMhRoBr8ywd8hf0cO6lmAgRSEhD+U+qGWggQIEBgV6BuyC+53Nm3cAgQIFBPQPiv5+QoAgQIEBhAoG7oD2H//vvvLx599NEBqnIJAgQITEdA+J9OL82EAAEC2QkcOHCg2Nra2viufWE/u9YqmACBRAWE/0QboywCBAhMUaBu2A9z//CHP+zO/hQXgTkRIDCqgPA/Kr+LEyBAYNoCdcN+uLNffX//kSNHiuPHj08bx+wIECAwgoDwPwK6SxIgQGCqAk3C/qFDh4qTJ0/uUlT3+pdv9pmqkXkRIEBgTAHhf0x91yZAgEDmAocPHy6ef/75Wnv2q2G/Ou1q8PfWnswXhPIJEEheQPhPvkUKJECAQDoCXYT96mz2799fbG9v7/2r6tafdGatEgIECExHQPifTi/NhAABAp0LNAn7IchfvHixdg1Hjx4tTpw4IfjXFnMgAQIE4gWE/3hDZyBAgMBkBPoM+4tI1W/w9YDvZJaQiRAgkLiA8J94g5RHgACBPgUeeOCB4rHHHqu1Z7/pnf11dVeD/759+3bf9e+HAAECBPoXEP77N3YFAgQIJCMwVtivAnjAN5nloBACBGYoIPzPsOmmHCdQ3rHs68HExfedx1Vr9NwF6ob94DTEm3bCXf6dnZ3dtgxxvbn33/wJECCwKCD8WxMEagpU71aWQ7r8ALB4/i7PXXOKDpuAQGphv0rqAd8JLDBTIEAgewHhP/sWmsBQAtU9yl2H/74/WAxl5DrjCCxbP8sqGftOe/XvkA+346wVVyVAgIDwbw0QqCFQfRf5wYMHiwsXLuyO6iLALO5/Ls/ZxblrTM0hGQrkEvartN7sk+FCUzIBApMUEP4n2VaT6lqgGrZCKO9y3//i3dAuz921g/ONI9Ak7N9///3Fo48+Ok6hK65arT/8c/VLvZIqVDEECBCYgYDwP4Mmm2K8QF8BvRqKwm8Uzp8/3+kHi/iZO8MYAgcOHCguXbq08dJhXaYY9quFe7PPxjY6gAABAoMKCP+DcrtYrgJl+C/3THd1d37ZHuiuzp2r9RzrDmE/vOd+01avHMJ+tX/VN/uEf79pfnPsvTkTIEBgaAHhf2hx18tSoAzk5ZcRdRHQl931DzhdnDtL5BkVPdWwX22hN/vMaEGbKgECWQkI/1m1S7FjCSwG8i4C+uJvE8Lcqg8Wu0s6Vre7v26TsH/o0KHi5MmT3Rcx8Bk94DswuMsRIECgpoDwXxPKYfMW6Dr8Lz5AXOqu+vfz1s9v9ocPHy6ef/75jdtcwrqaStivdqka/MvfluXXRRUTIEBgmgLC/zT7alYdC3Qd/pfd9Q8lew96x40b6HRzD/tVZm/2GWjRuQwBAgRaCgj/LeEMm5dAl+F/3d194T+PdSXsL+9T9QHfsb9QLI+VpEoCBAgMLyD8D2/uihkKdBn+V931r975F5zSWiRNwn54buPixYtpTWCAajzgOwCySxAgQKADAeG/A0SnmL5AV+F/057+xbcKTV82zRk+8MADxWOPPVZrz/5cw361c4J/mutYVQQIEFgmIPxbFwRqCHQR/q+++uriwoULu1dbdWe/i7cI1ZiOQxYEhP24JeHNPnF+RhMgQGBIAeF/SG3Xylagi/C/aT//pt8KZIuXYOF1w34o/aqrrprlNp66bfOAb10pxxEgQCANAeE/jT6oInGB2PBf566/8N/fImgS9j1vUb8P1TXLrb6bIwkQIDCmgPA/pr5rZyMQG/433fUPEHWOyQYsgUKrwXRdOUJru2ZV3+wTzuBL6do5GkWAAIGhBYT/ocVdL0uBmPBf565/NfwLo+2WiLDfzq3NKA/4tlEzhgABAmkICP9p9EEViQuU4T8EzO3t7b279HXudta9o7/uFaCJ84xSXpOwf//99xePPvroKHVO8aIe8J1iV82JAIG5CAj/c+m0eUYJLAb4um/lCa+BDB8Wwk/YJrG1tbWyjrrnjJpIxoOF/TSaJ/in0QdVECBAoK2A8N9WzrhZCSzuby4nv+nOf927/tUPCZvOORf4AwcO7H5Y2uQRjN3ZH2ZVeLPPMM6uQoAAgT4FhP8+dZ17UgLVIF9ObN3+/DZ3/cN5N4XdSaFWJiPsp93Z6gdgz6Wk3SvVESBAYJ2A8G99EGggUH14tzoshKHwf+fOnStCiA0/de/6V48tnyloUFK2hzYJ+4uTHPID0pDXSrWZ1Q+yc/6Ammp/1EWAAIEmAsJ/Ey3HEvj/Ast+C7AKJxz7s5/9rLj33nuXHjKH9/t//etfLx5++OHd32rkFqZzq7frv6Te7NO1qPMRIEBgXAHhf1x/V89UoAz/Bw8eLC5cuNB4Fst+KxD+3X333Vd85CMfKT7xiU80PucQA8JcP/rRjxZPPvlkcerUqWJnZ2fvslMNyVOdV9314gHfulKOI0CAQB4Cwn8efVJlYgKr3swTHlAN21nmHhjbtKsaMsM/P/HEE8U73/nONqcypiOBak/mtCWtIz6nIUCAQJICwn+SbVFU6gLrXsu5ahtP+PfhZ24fDIJV2DN+8eLF1NuqvopAdR17wNfSIECAwHQEhP/p9NJMBhRoE/6Xlbf45WHlMSEsV7fUVD80hDFDfYBY9mzDpmsL+wMuxJ4u5c0+PcE6LQECBBIQEP4TaIIS8hNYF/7rvuUnhwd9wzMNly5dWvthQ9jPb/2uq9gDvtPqp9kQIEBgUUD4tyYItBCoE/43bZUoz7HpuBbltR4SHjb+wQ9+IOy3Fsx/oAd88++hGRAgQGCdgPBvfRBoIRAb/qt3/cPd9fPnz7eoopsht956a3HixImNgf/BBx8sHnnkkW4u6ixJCgj+SbZFUQQIEOhUQPjvlNPJ5iJQJ/yvejvK4heFbdpD34dp9cPHsvOH+V111VWtXmPaR73O2b9AdU14s0//3q5AgACBsQSE/7HkXTdrgTrhf1Wor/tMQJdA7u53qTm9c3nAd3o9NSMCBAisEhD+rQ0CLQRWhf/qXf1l4b8asvq+u7rp7n6YdvhOgjZfUtaCzJBEBcKbpba3t/eqG+M3UYnSKIsAAQKTFBD+J9lWk+pbYFX43/QGnz7v+ru733fXp3d+b/aZXk/NiAABApsEhP9NQv6cwBKBNuG/j4d8w8PCm748K6W3CVlMaQl4wDetfqiGAAECQwgI/0Mou8bkBFaF/1V39qtbK2LCeN1XcR45cqQ4duzY5NxNqDuB6lrtewtad1U7EwECBAjECgj/sYLGz1KgafiP2e7j7v4sl1ivk67+Firmw2ivRTo5AQIECPQiIPz3wuqkUxfYFP6rgarpdh9396e+esadnzf7jOvv6gQIEBhbQPgfuwOun6VA3fBfffvPujus4e7+pUuXNn7R1s7OTpZeik5DwAO+afRBFQQIEBhTQPgfU9+1sxXYFP7Lb+1dt91n06s4fdFWtssj2cJjtp8lOymFESBAgEAjAeG/EZeDCfxPoE74r74/P2y1uOWWW4oTJ05svLv/4IMPFo888ghqAp0KeLNPp5xORoAAgWwFhP9sW6fwMQWWhf/qXuomtfmirSZajm0jUP0tkzf7tBE0hgABAtMREP6n00szGVBgWfjftI2nLC+MdXd/wGbN/FLe7DPzBWD6BAgQWBAQ/i0JAi0EquG/Tuh3d78FsiHRAtXvlwgnu3z5cvQ5nYAAAQIE8hYQ/vPun+pHEAiv4vz+97+/9srhw4Ev2hqhOS65J+DNPhYDAQIECCwTEP6tCwI1BOp80VZ5Gl+aVAPUIb0LeMC3d2IXIECAQJYCwn+WbVN03wIf/ehHi+9973uttkkI/313x/k3CVSDf3gQfWtra9MQf06AAAECMxEQ/mfSaNPcLFBn73412JcBqwxXi/998xUdQaB7AQ/4dm/qjAQIEJiSgPA/pW6aSyOB6rfvrhrYZO/+qnf/NyrKwQQiBAT/CDxDCRAgMBMB4X8mjTbNonjta19b/Pvf/964lafNtp3qW1W8UcVqG0NA8B9D3TUJECCQn4Dwn1/PVNxAoO7d/Yceeqj47ne/2+DMVx5aDV7Cf2tGA1sKCP4t4QwjQIDADAWE/xk2fS5TrrOHvw+L8E7/8HPttdcWL7zwQh+XcE4CewKCv8VAgAABAk0EhP8mWo7NSiCE73PnzmVRc/mBIRR7++23F0899VQWdStyXAHBf1x/VydAgECOAsJ/jl1Tc22B6isPaw/K5MAwt6uuumqv2gsXLmRSuTK7EFj8zZbtZl2oOgcBAgSmLyD8T7/HZjiAQPVDxqoQVt7dv3Tp0gAVrb5EeDXpTTfdVJw8eXLUOly8vYDg397OSAIECMxdQPif+wow/04E6oT/8kKLv424/vrri//+979L6/j0pz9dfOtb39r7s/BlTX3e4Q0PSN9xxx3Fb37zm05cnKR7gfDhbWdnZ+/Efa6H7qt3RgIECBAYW0D4H7sDrj8Jgbrhv/pK0OrEuw5wd955Z/HMM8/sXqKLDwxhfgcPHiw+9alPFV/96lcn0bMcJyH459g1NRMgQCAtAeE/rX6oJlOBMvxv+o6AxQc0y9Dfdfivy3j48OHd3zrEPC8QAml49iCXh6vr2qR2nOCfWkfUQ4AAgTwFhP88+6bqxATqhv/F41L+VuCwBSg8n1DdYtKUPZwjbGvyfEFTuSuPF/zj/IwmQIAAgZcFhH+rgUAHAk3Df/gNwPb2dpFy+F/F8tnPfrb4xje+UVy8eLH18we2EdVfdIJ/fStHEiBAgMBmAeF/s5EjCGwUKEN8GeqXDaju9y+3+eQY/tdhvOMd7yiefvrpTj4Y2EZUFIL/xr96DiBAgACBhgLCf0MwhxNYJlAnxFf3+081/K9bHeH5gtOnT9tGVPOvkOBfE8phBAgQINBIQBxLGP8AABrFSURBVPhvxOVgAssF6oT/ZVuD6oybunmX24im8ppSwX/qq978CBAgMJ6A8D+evStPSKBOiF+2NajOuAkxNZ5Kl9uIcnlNqeDfeJkYQIAAAQINBIT/BlgOJbBMYNle/mXHLQv6wn/7NdXFa0pD78L3F7ztbW8rfvWrX7UvpqORi8E/zPHEiRMdnd1pCBAgQIBAUQj/VgGBSIFle/mF/0jUyOFdvKa0LKH8gPCZz3ym+NKXvhRZ2erhR44cueKVqIJ/b9ROTIAAgVkLCP+zbr/JdyHQNPy/+tWvLs6cObN7aXf+u+hA/XN08XzBsqtdd911u/+67QcEwb9+Dx1JgAABAnECwn+cn9EEiqbhv/otwMJ/Wgvove99b/G73/1u9xuPw/cwdPmz6gOC4N+lsnMRIECAwCYB4X+TkD8nsEFA+J/XEunzA0L4MHjttdcWL7300rxQzZYAAQIEBhMQ/gejdqGpCpR378P8yvf3L5vrsuPc+Z/mqnjNa15T/Oc//ynCF5WtWxNNZ19+OAjjfEBoqud4AgQIEAgCwr91QCBSQPiPBJzh8MWtPl0SlB8QbrjhhuJf//pXl6d2LgIECBCYgIDwP4EmmsK4AnXD/9y/4XfcLqVz9SZ7/Pv6DUJYi9dcc03hA0I660IlBAgQGEpA+B9K2nUmK7Dsm3uXTVb4n+wSqD2xJsG/7knLB4m73mLkA0LdDjiOAAECeQkI/3n1S7UJCtQN/9UvcCr3gdvzn2BDeyppMfiH9bC1tdXT1a48bV8fEFL7krQ+MT/2sY8V//znP/cucezYsd0vYHvDG95wxWV///vf91mGcxMgQCBaQPiPJnSCuQvUDf/BaTHsC//zWD2LwT/cVe/6VaIxkp///OeLr33ta7unOHv2bMypXjH2wIEDu1uMws9dd91V/OQnP+n0/G1Pdt999xW/+MUv9oaH17t2+XD2urrCh6bqz6VLl9pOwzgCBAg0FhD+G5MZQOBKgTLA1wl05bHlF30J//NYTdXnQuqskxRV+vyAsDjfPj8wvPWtby2eeeaZaOLyN3ldfWAIH5C6/uAVPUknIEBgkgLC/yTbalJDCjQJ8Iu/JWgydsg5uVZ3AlMI/nU1hvyA0PQDw7LQf/Dgwb3ThO074Tc05c/nPve54kMf+lDdqa887uGHHy5+9KMfXfHnf/rTn/b+e/U3QLl+MIxGcgICBAYVEP4H5XaxKQo0CfDC/xRXwOo5zSn4N+3sl7/85eIrX/nK3rAXX3yx6Smij+/zNwxNigtf7BYe2C5/vv3tbxcf//jHm5zCsQQIEKgtIPzXpnIggeUCbcJ/OFPYLtBkLP+8BAT/7vs15Q8Mt912W/HXv/7VB4Dul40zEiCwICD8WxIEIgWaBPjF7wRoMjayTMMHFBD8B8ReuNQ999xzxYO84Y/D9p5yi88Yv2Foq/Hud7+7eOKJJ9oON44AAQJLBYR/C4NApECTAL/4rv8mYyPLNHwgAcF/IOiFyzz99NPF3XffXZw/f37vT26//fZWD/em8BuGMAnbf8ZZS65KYOoCwv/UO2x+vQs0CfDV8B/eFlI+7NfVG0N6n6wLrBUQ/MdZIJ/85CeLb37zm3sXf+Mb31g899xzgxaz6gND+Du/s7PTqhb/u9CKzSACBDYICP+WCIFIgSbhv/pFX9XL+n/ykU1IYLjgP14TDh8+XJw6daoI78//zne+Uzz00EPjFePKBAgQSFxA+E+8QcpLX6BJ+A+zqYbE8r+3vTOYvs48Kqz2NPyzfg7b96Z/B4etztUIECCQloDwn1Y/VJOhQNPgsRj+3fXPsOmVkgX/8ftX9uCLX/xi8YUvfGH8glRAgACBhAWE/4Sbo7Q8BJqG/+q+/zBD4T+PPi+rstpLd/zH6+O9995bPP7447sF+Ps0Xh9cmQCBPASE/zz6pMqEBZqG/7AvuXzQ1zd6JtzYDaUJ/mn1Ltz1Dz/lf6ZVnWoIECCQjoDwn04vVJKpQNPwXw2Nwn+eTRf88+ybqgkQIECgKIR/q4BApEDT8G+PeCT4yMMF/5Eb4PIECBAgECUg/EfxGUzg5bf31NlrfN111xVnz569gq3OOM5pCAj+afRBFQQIECDQXkD4b29nJIFdgSZ3/hcf9g3jhf88FpLgn0efVEmAAAEC6wWEfyuEQKRAk/BfHhv+swz93hIT2YABhgv+AyC7BAECBAgMIiD8D8LsIlMWqBv+q2/5ufbaa6/Y/uPuf7orRPBPtzcqI0CAAIHmAsJ/czMjCFwhUDf8V0NkCPuL/x1regK+kyG9nqiIAAECBOIEhP84P6MJ1N7zX93ys7OzU9x0003FmTNndgVvvPHG4oUXXqCZkIDgn1AzlEKAAAECnQkI/51ROtFcBerc+d+3b18RAn/4qW7xWfxAMFfD1OZd7ddiz1KrVT0ECBAgQKCJgPDfRMuxBJYI1An/1Xf7Lwv/AmY6S0vwT6cXKiFAgACB7gWE/+5NnXFmApvCf/VB3xAst7a29oTs+09rsQj+afVDNQQIECDQvYDw372pM85MYFP4X3XXv2Qq/9y+/3EXjuA/rr+rEyBAgMAwAsL/MM6uMmGBdeE/vNLz3Llzu7MPd/m3t7dfIWHf//iLQ/AfvwcqIECAAIFhBIT/YZxdZcIC68L/prv+5YeC8jkA7/sffqEI/sObuyIBAgQIjCcg/I9n78oTEagT/td9i++qNwFNhCfpaQj+SbdHcQQIECDQg4Dw3wOqU85LYFX4b/Iwr60/w68ZwX94c1ckQIAAgfEFhP/xe6CCzAVWhf8mgb7O9qDMmZIqX/BPqh2KIUCAAIEBBYT/AbFdapoCy8J/k7v+QcXWn+HWxmLwP3z4cHHixInhCnAlAgQIECAwooDwPyK+S09DYFn4b3Mnv8lvCqYhN/wsjhw5Upw8eXLvwoL/8D1wRQIECBAYV0D4H9ff1ScgsBj+q3f9r7nmmuLs2bO1Ztn0twW1TuqgPQHB32IgQIAAAQJFIfxbBQQiBcrwH8L7gQMHivPnz++dscmrO6vfBNxkXGT5sxgu+M+izSZJgAABAjUEhP8aSA4hsE6gusWnelybAG/rT/drTfDv3tQZCRAgQCBfAeE/396pPCGBxQ8Aq77Nd1PJtv5sEmr254J/My9HEyBAgMD0BYT/6ffYDAcSuPrqq4sLFy4Ube74lyVWt/7cdNNNxenTpweqfnqXEfyn11MzIkCAAIF4AeE/3tAZCHQqYOtPPKfgH2/oDAQIECAwTQHhf5p9NauMBWz9iWue4B/nZzQBAgQITFtA+J92f80uQ4Gbb765eOGFF3Yrt/WnWQMXg3/4Qq+tra1mJ3E0AQIECBCYsIDwP+Hmmlq+Arb+NO/dYvBv+9B18ysbQYAAAQIE8hEQ/vPplUpnJGDrT/NmV9+4JPg39zOCAAECBOYhIPzPo89mmZlAdeuPrSubmyf4bzZyBAECBAgQCALCv3VAIFEBW3/qNUbwr+fkKAIECBAgIPxbAwQSFrD1Z3NzBP/NRo4gQIAAAQJVAXf+rQcCCQuU4dbWn1c2SfBPeOEqjQABAgSSFRD+k22NwggURTXgxnxz8NQsBf+pddR8CBAgQGAoAeF/KGnXIdBCwNYfd/xbLBtDCBAgQIDASgHh3+IgkLhAeZfb6yuv/E0Ij8QXrvIIECBAIEkB4T/JtiiKwMsCtv78z6LqEP55Z2fHMiFAgAABAgQaCgj/DcEcTmBogfCwbxl0w/v/n3/++aFLGP16gv/oLVAAAQIECExEQPifSCNNY9oCc777X33uwR3/aa9zsyNAgACB/gWE//6NXYFAtMD+/fuL7e3t3fPMKQAL/tFLxwkIECBAgMAVAsK/BUEgE4E53f2vhv65feDJZDkqkwABAgQyFRD+M22csucncOjQoeL06dOTvvu/GPrDZL3VZ35r3YwJECBAoD8B4b8/W2cm0LlANRxP6eHfZaF/TtubOl8oTkiAAAECBFYICP+WBoHMBKa0/af6LEO1Dbfccktx8uTJzDqjXAIECBAgkL6A8J9+j1RI4AqBKTz8K/Rb1AQIECBAYBwB4X8cd1clECWQ691/oT+q7QYTIECAAIFoAeE/mtAJCIwjkNMHgMOHDxenTp16BVT4ArOtra1xAF2VAAECBAjMUED4n2HTTXkaAtWHZFMN0UL/NNaaWRAgQIDAdASE/+n00kxmKJDq3f9Vod9rO2e4SE2ZAAECBJISEP6TaodiCDQTSPHhX6/tbNZDRxMgQIAAgSEFhP8htV2LQA8Cqbz7X+jvoblOSYAAAQIEOhYQ/jsGdToCYwiMuf1H6B+j465JgAABAgTaCQj/7dyMIpCUwBgP/wr9SS0BxRAgQIAAgVoCwn8tJgcRSF9gqLv/Qn/6a0GFBAgQIEBglYDwb20QmIjAoUOHitOnT+/OJnwQ2NnZ6XRm4XWiy855yy23FCdPnuz0Wk5GgAABAgQI9CMg/Pfj6qwERhHo4+Ff38o7SitdlAABAgQI9CIg/PfC6qQExhPoavuP0D9eD12ZAAECBAj0JSD89yXrvARGEqhuz2nzpVq+lXekxrksAQIECBAYQED4HwDZJQgMLdDm7r/QP3SXXI8AAQIECAwvIPwPb+6KBHoXaPLwr9DfeztcgAABAgQIJCMg/CfTCoUQ6Fag+vDvqrf/eG1nt+bORoAAAQIEUhcQ/lPvkPoIRAgshvvwG4FTp04VQn8EqqEECBAgQCBjAeE/4+YpnUAdgWVBvzquj+8EqFOXYwgQIECAAIHhBYT/4c1dkcDgAste2yn0D94GFyRAgAABAqMLCP+jt0ABBIYTCB8Cwrf0dv3tv8PNwJUIECBAgACBGAHhP0bPWAIECBAgQIAAAQIZCQj/GTVLqQQIECBAgAABAgRiBIT/GD1jCRAgQIAAAQIECGQkIPxn1CylEiBAgAABAgQIEIgREP5j9IwlQIAAAQIECBAgkJGA8J9Rs5RKgAABAgQIECBAIEZA+I/RM5YAAQIECBAgQIBARgLCf0bNUioBAgQIECBAgACBGAHhP0bPWAIECBAgQIAAAQIZCQj/GTVLqQQIECBAgAABAgRiBIT/GD1jCRAgQIAAAQIECGQkIPxn1CylEiBAgAABAgQIEIgREP5j9IwlQIAAAQIECBAgkJGA8J9Rs5RKgAABAgQIECBAIEZA+I/RM5YAAQIECBAgQIBARgLCf0bNUioBAgQIECBAgACBGAHhP0bPWAIECBAgQIAAAQIZCQj/GTVLqQQIECBAgAABAgRiBIT/GD1jCRAgQIAAAQIECGQkIPxn1CylEiBAgAABAgQIEIgREP5j9IwlQIAAAQIECBAgkJGA8J9Rs5RKgAABAgQIECBAIEZA+I/RM5YAAQIECBAgQIBARgLCf0bNUioBAgQIECBAgACBGAHhP0bPWAIECBAgQIAAAQIZCQj/GTVLqQQIECBAgAABAgRiBIT/GD1jCRAgQIAAAQIECGQkIPxn1CylEiBAgAABAgQIEIgREP5j9IwlQIAAAQIECBAgkJGA8J9Rs5RKgAABAgQIECBAIEZA+I/RM5YAAQIECBAgQIBARgLCf0bNUioBAgQIECBAgACBGAHhP0bPWAIECBAgQIAAAQIZCQj/GTVLqQQIECBAgAABAgRiBIT/GD1jCRAgQIAAAQIECGQkIPxn1CylEiBAgAABAgQIEIgREP5j9IwlQIAAAQIECBAgkJGA8J9Rs5RKgAABAgQIECBAIEZA+I/RM5YAAQIECBAgQIBARgLCf0bNUioBAgQIECBAgACBGAHhP0bPWAIECBAgQIAAAQIZCQj/GTVLqQQIECBAgAABAgRiBIT/GD1jCRAgQIAAAQIECGQkIPxn1CylEiBAgAABAgQIEIgREP5j9IwlQIAAAQIECBAgkJGA8J9Rs5RKgAABAgQIECBAIEZA+I/RM5YAAQIECBAgQIBARgLCf0bNUioBAgQIECBAgACBGAHhP0bPWAIECBAgQIAAAQIZCQj/GTVLqQQIECBAgAABAgRiBIT/GD1jCRAgQIAAAQIECGQkIPxn1CylEiBAgAABAgQIEIgREP5j9IwlQIAAAQIECBAgkJGA8J9Rs5RKgAABAgQIECBAIEZA+I/RM5YAAQIECBAgQIBARgLCf0bNUioBAgQIECBAgACBGAHhP0bPWAIECBAgQIAAAQIZCQj/GTVLqQQIECBAgAABAgRiBIT/GD1jCRAgQIAAAQIECGQkIPxn1CylEiBAgAABAgQIEIgREP5j9IwlQIAAAQIECBAgkJGA8J9Rs5RKgAABAgQIECBAIEZA+I/RM5YAAQIECBAgQIBARgLCf0bNUioBAgQIECBAgACBGAHhP0bPWAIECBAgQIAAAQIZCQj/GTVLqQQIECBAgAABAgRiBIT/GD1jCRAgQIAAAQIECGQkIPxn1CylEiBAgAABAgQIEIgREP5j9IwlQIAAAQIECBAgkJGA8J9Rs5RKgAABAgQIECBAIEZA+I/RM5YAAQIECBAgQIBARgLCf0bNUioBAgQIECBAgACBGAHhP0bPWAIECBAgQIAAAQIZCQj/GTVLqQQIECBAgAABAgRiBIT/GD1jCRAgQIAAAQIECGQkIPxn1CylEiBAgAABAgQIEIgREP5j9IwlQIAAAQIECBAgkJGA8J9Rs5RKgAABAgQIECBAIEZA+I/RM5YAAQIECBAgQIBARgLCf0bNUioBAgQIECBAgACBGAHhP0bPWAIECBAgQIAAAQIZCQj/GTVLqQQIECBAgAABAgRiBIT/GD1jCRAgQIAAAQIECGQkIPxn1CylEiBAgAABAgQIEIgREP5j9IwlQIAAAQIECBAgkJGA8J9Rs5RKgAABAgQIECBAIEZA+I/RM5YAAQIECBAgQIBARgLCf0bNUioBAgQIECBAgACBGAHhP0bPWAIECBAgQIAAAQIZCQj/GTVLqQQIECBAgAABAgRiBIT/GD1jCRAgQIAAAQIECGQkIPxn1CylEiBAgAABAgQIEIgREP5j9IwlQIAAAQIECBAgkJGA8J9Rs5RKgAABAgQIECBAIEZA+I/RM5YAAQIECBAgQIBARgLCf0bNUioBAgQIECBAgACBGAHhP0bPWAIECBAgQIAAAQIZCQj/GTVLqQQIECBAgAABAgRiBIT/GD1jCRAgQIAAAQIECGQkIPxn1CylEiBAgAABAgQIEIgREP5j9IwlQIAAAQIECBAgkJGA8J9Rs5RKgAABAgQIECBAIEZA+I/RM5YAAQIECBAgQIBARgLCf0bNUioBAgQIECBAgACBGAHhP0bPWAIECBAgQIAAAQIZCQj/GTVLqQQIECBAgAABAgRiBIT/GD1jCRAgQIAAAQIECGQkIPxn1CylEiBAgAABAgQIEIgREP5j9IwlQIAAAQIECBAgkJGA8J9Rs5RKgAABAgQIECBAIEZA+I/RM5YAAQIECBAgQIBARgLCf0bNUioBAgQIECBAgACBGAHhP0bPWAIECBAgQIAAAQIZCQj/GTVLqQQIECBAgAABAgRiBIT/GD1jCRAgQIAAAQIECGQkIPxn1CylEiBAgAABAgQIEIgREP5j9IwlQIAAAQIECBAgkJGA8J9Rs5RKgAABAgQIECBAIEZA+I/RM5YAAQIECBAgQIBARgLCf0bNUioBAgQIECBAgACBGAHhP0bPWAIECBAgQIAAAQIZCQj/GTVLqQQIECBAgAABAgRiBIT/GD1jCRAgQIAAAQIECGQkIPxn1CylEiBAgAABAgQIEIgREP5j9IwlQIAAAQIECBAgkJGA8J9Rs5RKgAABAgQIECBAIEZA+I/RM5YAAQIECBAgQIBARgLCf0bNUioBAgQIECBAgACBGAHhP0bPWAIECBAgQIAAAQIZCQj/GTVLqQQIECBAgAABAgRiBIT/GD1jCRAgQIAAAQIECGQkIPxn1CylEiBAgAABAgQIEIgREP5j9IwlQIAAAQIECBAgkJGA8J9Rs5RKgAABAgQIECBAIEZA+I/RM5YAAQIECBAgQIBARgLCf0bNUioBAgQIECBAgACBGAHhP0bPWAIECBAgQIAAAQIZCQj/GTVLqQQIECBAgAABAgRiBIT/GD1jCRAgQIAAAQIECGQkIPxn1CylEiBAgAABAgQIEIgREP5j9IwlQIAAAQIECBAgkJGA8J9Rs5RKgAABAgQIECBAIEZA+I/RM5YAAQIECBAgQIBARgLCf0bNUioBAgQIECBAgACBGAHhP0bPWAIECBAgQIAAAQIZCQj/GTVLqQQIECBAgAABAgRiBIT/GD1jCRAgQIAAAQIECGQkIPxn1CylEiBAgAABAgQIEIgREP5j9IwlQIAAAQIECBAgkJGA8J9Rs5RKgAABAgQIECBAIEZA+I/RM5YAAQIECBAgQIBARgLCf0bNUioBAgQIECBAgACBGAHhP0bPWAIECBAgQIAAAQIZCQj/GTVLqQQIECBAgAABAgRiBIT/GD1jCRAgQIAAAQIECGQkIPxn1CylEiBAgAABAgQIEIgREP5j9IwlQIAAAQIECBAgkJGA8J9Rs5RKgAABAgQIECBAIEZA+I/RM5YAAQIECBAgQIBARgLCf0bNUioBAgQIECBAgACBGAHhP0bPWAIECBAgQIAAAQIZCQj/GTVLqQQIECBAgAABAgRiBIT/GD1jCRAgQIAAAQIECGQkIPxn1CylEiBAgAABAgQIEIgREP5j9IwlQIAAAQIECBAgkJGA8J9Rs5RKgAABAgQIECBAIEZA+I/RM5YAAQIECBAgQIBARgLCf0bNUioBAgQIECBAgACBGAHhP0bPWAIECBAgQIAAAQIZCQj/GTVLqQQIECBAgAABAgRiBIT/GD1jCRAgQIAAAQIECGQkIPxn1CylEiBAgAABAgQIEIgREP5j9IwlQIAAAQIECBAgkJGA8J9Rs5RKgAABAgQIECBAIEZA+I/RM5YAAQIECBAgQIBARgLCf0bNUioBAgQIECBAgACBGAHhP0bPWAIECBAgQIAAAQIZCQj/GTVLqQQIECBAgAABAgRiBIT/GD1jCRAgQIAAAQIECGQkIPxn1CylEiBAgAABAgQIEIgREP5j9IwlQIAAAQIECBAgkJGA8J9Rs5RKgAABAgQIECBAIEZA+I/RM5YAAQIECBAgQIBARgL/Dx1IjPnpZ0qeAAAAAElFTkSuQmCC', 2, '2019-09-15 13:34:41', 1, '');
INSERT INTO `diagnosis` (`id`, `commentClient`, `machineCommentState`, `machineObservation`, `firmClient`, `firmMaintenance`, `idProduct`, `dateCreated`, `idStatusMachine`, `noteGeneral`) VALUES
(3, '<p>fdgf</p>\r\n', 0x3c703e6664676466673c2f703e0d0a, 0x3c703e6664676664676664673c2f703e0d0a, 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAqMAAAGqCAYAAAA/eE09AAAgAElEQVR4Xu3dSastV/038IqJ98YYe2ODfbAXhIA6iIgQRQdR8AVkJAjOxInOgh0iouBMHTjUFyDJIIIaO3CiKERjg7EP9knMTTz3es65f+r41H3W3dlNrepWU5/Aw+PfVK3m81vH/d2rmn3N5cuXLzf+IUCAAAECBAgQIJBA4BphNIG6LgkQIECAAAECBM4EhFELgQABAgQIECBAIJmAMJqMXscECBAgQIAAAQLCqDVAgAABAgQIECCQTEAYTUavYwIECBAgQIAAAWHUGiBAgAABAgQIEEgmIIwmo9cxAQIECBAgQICAMGoNECBAgAABAgQIJBMQRpPR65gAAQIECBAgQEAYtQYIECBAgAABAgSSCQijyeh1TIAAAQIECBAgIIxaAwQIECBAgAABAskEhNFk9DomQIAAAQIECBAQRq0BAgQIECBAgACBZALCaDJ6HRMgQIAAAQIECAij1gABAgQIECBAgEAyAWE0Gb2OCRAgQIAAAQIEhFFrgAABAgQIECBAIJmAMJqMXscECBAgQIAAAQLCqDVAgAABAgQIECCQTEAYTUavYwIECBAgQIAAAWHUGiBAgAABAgQIEEgmIIwmo9cxAQIECBAgQICAMGoNECBAgAABAgQIJBMQRpPR65gAAQIECBAgQEAYtQYIECBAgAABAgSSCQijyeh1TIAAAQIECBAgIIxaAwQIECBAgAABAskEhNFk9DomQIAAAQIECBAQRq0BAgQIECBAgACBZALCaDJ6HRMgQIAAAQIECAij1gABAgQIECBAgEAyAWE0Gb2OCRAgQIAAAQIEhFFrgAABAgQIECBAIJmAMJqMXscECBAgQIAAAQLCqDVAgAABAgQIECCQTEAYTUavYwIECBAgQIAAAWHUGiBAgAABAgQIEEgmIIwmo9cxAQIECBAgQICAMGoNECBAgAABAgQIJBMQRpPR65gAAQIECBAgQEAYtQYIECBAgAABAgSSCQijyeh1TIAAAQIECBAgIIxaAwQIECBAgAABAskEhNFk9DomQIAAAQIECBAQRq0BAgQIECBAgACBZALCaDJ6HRMgQIAAAQIECAij1gABAgQIECBAgEAyAWE0Gb2OCRAgQIAAAQIEhFFrgAABAgQIECBAIJmAMJqMXscECBAgQIAAAQLCqDVAgAABAgQIECCQTEAYTUavYwIECBAgQIAAAWHUGiBAgAABAgQIEEgmIIwmo9cxAQIECBAgQICAMGoNECBAgAABAgQIJBMQRpPR65gAAQIECBAgQEAYtQYIECBAgAABAgSSCQijyeh1TIAAAQIECBAgIIxaAwQIECBAgAABAskEhNFk9DomQKB0gSc96UnN5cuXz6ZxzTXXNKenp6VPyfgJECCwuIAwuji5DgkQqEHg+uuvby5evHjVVATSGiprDgQILC0gjC4trj8CBKoQ2NwV7XZIr7322ub4+LiKOZoEAQIElhAQRpdQ1gcBAtUJtLug7T/dbmj3f7f/XRdMq5u0CREgQGAGAWF0BlRNEiBQv0AXPs+fP98cHR011113XXNycnI28XbXtPvP9UuYIQECBMYJCKPj/JxNgMAKBdpL8d3DSuEuqN3RFS4GUyZAYLSAMDqaUAMECKxNILxfNAyjmw81uVy/tpVhvgQIDBEQRoeoOYcAgVUL7NsBDYNqdwl/1VgmT4AAgQMCwqglQoAAgUiBzYeXNk8/9O8ju3M4AQIEqhYQRqsur8kRIDCHQBc2dz2otOsy/hxj0SYBAgRKFxBGS6+g8RMgsKhAeF/orntCw2Ncql+0PDojQKBAAWG0wKIZMgEC6QT67nq6VJ+uRnomQKAsAWG0rHoZLQECiQX6hsy+oTXxdHRPgACB5ALCaPISGAABAqUIxFx+jzm2lPkbJwECBOYQEEbnUNUmAQJVCsTudvbdRa0Sy6QIECDQU0AY7QnlMAIECMSGy9jwSpgAAQJrFBBG11h1cyZAIFpgyGX3IedED8wJBAgQKFxAGC28gIZPgMAyAkN3OWN3U5eZjV4IECCQj4Awmk8tjIQAgYwFhobKoSE2YwpDI0CAwKQCwuiknBojQKBWgS6Mxr7Evs9L8ms1My8CBAj0ERBG+yg5hgCBVQtce+21zenp6ZnBrl9d2gfUBdkh564a3uQJEFiFgDC6ijKbJAECYwTGXmoXRsfoO5cAgdoFhNHaK2x+BAiMFujCpJ3R0ZQaIECAwBMEhFGLggABAgcEhj681DVrZ9QSI0CAwG4BYdTqIECAQM8w2l6uPzk5ifYSRqPJnECAwIoEhNEVFdtUCRAYJjA2TI49f9ionUWAAIEyBITRMupklAQIJBIY+yR9O2xhNFHxdEuAQBECwmgRZTJIAgRSCYx9kl4YTVU5/RIgUIqAMFpKpYyTAIEkAmOfpBdGk5RNpwQIFCQgjBZULEMlQGB5gbFP0gujy9dMjwQIlCUgjJZVL6MlQGBhgS6MDn2SXhhduGC6I0CgOAFhtLiSGTABAksKTPHw0RRtLDlnfREgQGBJAWF0SW19ESBQnMAUQXKKNoqDM2ACBAj0FBBGe0I5jACB9QlM8SR9qyaMrm/tmDEBAv0FhNH+Vo4kQGBlAsLoygpuugQIJBEQRpOw65QAgRIEhNESqmSMBAiULiCMll5B4ydAYDYBYXQ2Wg0TIEDgioAwajEQIEBgh8AUL7xvm3bPqCVGgACB3QLCqNVBgAABYdQaIECAQDIBYTQZvY4JEMhdYIpfX7IzmnuVjY8AgdQCwmjqCuifAIFsBaYIo9dee21zenp6NsfLly9nO1cDI0CAQCoBYTSVvH4JEMheYIowOtVDUNljGSABAgQGCgijA+GcRoBA/QLCaP01NkMCBNILCKPpa2AEBAhkKjDFU/B2RjMtrmERIJCNgDCaTSkMhACB3ASE0dwqYjwECNQoIIzWWFVzIkBgEgFhdBJGjRAgQGCvgDBqgRAgQGCHgDBqaRAgQGB+AWF0fmM9ECBQqIAwWmjhDJsAgaIEhNGiymWwBAgsKTBFGJ3qJ0WXnLe+CBAgsKSAMLqktr4IEChKQBgtqlwGS4BAoQLCaKGFM2wCBOYXEEbnN9YDAQIEhFFrgAABAjsEhFFLgwABAvMLCKPzG+uBAIFCBYTRQgtn2AQIFCUgjBZVLoMlQGBJAWF0SW19ESCwVgFhdK2VN28CBA4KCKMHiRxAgACB0QLC6GhCDRAgUKvAlGG0bev09LRWKvMiQIDAYAFhdDCdEwkQqF1AGK29wuZHgEAOAsJoDlUwBgIEshQQRrMsi0ERIFCZgDBaWUFNhwCB6QSE0ekstUSAAIFdAsKotUGAAIEdAsKopUGAAIH5BYTR+Y31QIBAoQLCaKGFM2wCBIoSEEaLKpfBEiCwpIAwuqS2vggQWKuAMLrWyps3AQIHBYTRg0QOIECAwGgBYXQ0oQYIEKhVYMoweu211zbHx8e1UpkXAQIEBgsIo4PpnEiAQO0CU4bRy5cv185lfgQIEBgkIIwOYnMSAQJrEBBG11BlcyRAILWAMJq6AvonQCBbAWE029IYGAECFQkIoxUV01QIEJhWQBid1lNrBAgQ2CYgjFoXBAgQ2CHQhdGhDx896UlParp7Rd0zapkRIEBgu4AwamUQIEDgQBht//WQMCmMWloECBA4LCCMHjZyBAECKxW47rrrmpOTk7PZD9kd7XZW2///9PR0pYqmTYAAgf0CwqgVQmBmgSnuO5x5iJrfI9DVb8juaHduu0PahVrYBAgQIHC1gDBqRRCYSSC8RNt1MeRS70zD02xPgXB39Pz5883R0VGvM6+//vrm4sWLZ8eure7d2rcj3GupOIjA6gWE0dUvAQBzCGwLomsMJXPYpmhzyOX2td4vurn21xbEU6xPfRIoXUAYLb2Cxp+lQBhewg9jH8xZluvgoIYEyzGX9w8OKNMDNoPokPtsM52aYREgMKOAMDojrqbXKdB+AO96WEUYLXNNhJfc+16qH7KbWqZO04Q+3RzcJ1tqNY2bwPICwujy5nqsXCDcEQunKoiWXfjYcLmWh5c2v3y5T7TsdW70BFIICKMp1PVZrcC2INp3J61alEomFnOpfi0PL21elhdEK1nspkFgYQFhdGFw3dUrsO3yvA/neuodBsxDdY0JriUKbbss387D7n+J1TRmAukFhNH0NTCCggX++9//Nu3O57YPYQ9vFFzYHUMPQ+a+QFrzw0u77ok+FNDrWw1mRIDAVALC6FSS2lmdwK7XN7Ufym1IbT+0/VOfQJ9AGnt/aSlK2y7Ld1/E7IqWUkXjJJCfgDCaX02MKHOBXSG0HbbdocyLN9Hwwp3PbTvg3b+vZXc8fPF/R9heEehe6t/+d8LoRItLMwRWKCCMzlx0v0QyM/CCzW/7QN78qU8fyAsWJHFXYSANH1IL10kN62HzobzwC1etO8CJl5buCaxOQBidueQ13zs2M102ze96WKMLID6QsynVogPZXBdd8Kzl4aVtVwDC0F3LPBddNDojQGCrgDA688Loc3/ZzEPQ/ECBz33uc81HPvKRJ1x+3Lz0upb3SQ5krPq0zYd52kBa+hfQbV++tt1+Uvo8q16YJkegMAFhdIGChf+j/YpXvKJ54IEHFuhVF0MFdu2EbvtADsNIDZdkh5qt+bx9D7Lt+iWuHL3atdyu4c11vGtd13ZfbI41MSYCaxEQRheo9LYPKz+VtwB8ZBfb7gltm2g/dO+///7mNa95zRNadKkyErnSw/e97qhdI8fHx1nNvP3CdenSpb0PHe17+Mq6z6qcBkOgeAFhdKES7gs6Je2eLMS1WDf33Xdf88Y3vnHrh/K+ENoN0P2ii5WqiI62/QLXroF3x547d645OjqaZX5taOz+6btz3+eNENb9LOXSKIHVCgijC5d+1w5K3w+KhYdbbXd33nln86lPfWrUy+rDy/l+8rPapRI1sX2v/YpqaMGD22AZE4jD/w2z7hcslK4IVCwgjCYq7rbLZP6Hff5i7Ls8GevvUuX89Sqth30P9XT3ZLZzSvXlsx1f+/9OTk4G03pwaTCdEwkQ2CEgjCZeGps7pX0ukSUecpHd77tN4vOf/3zz4Q9/OHpeLlVGk1V/wpg10a7R7pad2LAaBsSYXc7YgoRfwGK/vMX25XgCBNYjIIxmUOttT2/feuutzQ9+8IMMRlf2EPaF0B/96EfNLbfcMniCXuk0mK7aE7s1UWNQC/+WfGmudgmbGIEkAsJoEvbtnW57+OH2229v7rrrroxGmf9Q7r333ua2227beil0qrcYeKVT/utg6RHWviZcnl96RemPwHoEhNHMar3radwPfvCDzRe/+MXMRpvHcB588MHm5ptvPntVTfvPtkucU/9GuPtF86h9TqOoeU24PJ/TSjMWAvUJCKMZ1vQXv/hF8/rXv35rqPrCF77QfOhDH8pw1MsMqXsAaVfo3BzFXJdLx9wbuIyUXpYWqHVNhLcRuTy/9KrSH4F1CAijGdf5a1/7WnPHHXdsDaXf/e53m7e97W0Zjz5+aO3tCO973/uuPMTRN3Bu9tR+YP7whz9s3vKWt8QPoscZ4b1zc4XdHsNwSGYCtYZRl+czW2iGQ6BCAWG0gKJ+4hOfaD72sY8lex1MzkTtB2V7CbF9aXgbEpf4x4fzEsrl9dGti9gn4XOeaXh5fupbXXKet7ERILCsgDC6rPeo3j7wgQ80X/nKV1YXSsOn1nP4WcVad8BGLc6Vn1zj/aKbb/moKWSvfLmaPoHsBITR7EpyeEDvfe97m7vvvrvoUBruLrb/+Z577mne+c53Hp584iNqDB2JSavovsbd8hrnVMViMwkCFQoIoxUW1ZTmE7ArOp9tyS3XuC68R7fkFWnsBMoSEEbLqpfRJhTwm9wJ8TPvugtutdxX6QpA5gvO8AhUJiCMVlZQ05lPwGXL+WxLbjl8u0It91Va6yWvSGMnUJ6AMFpezYw4gUD4MEctu18JGKvsssZdxBpvO6hy8ZkUgUoEhNFKCmka8wrYKZrXt+TWa1wb3Zy8R7fklWnsBMoREEbLqZWRJhSwU5QQP/Oua1sbNe70Zr6EDI/A6gWE0dUvAQCHBHw4HxJa97+vbRexxp3eda9QsyeQv4Awmn+NjDCxQG07X4k5q+q+xi8q1ntVS9RkCBQhIIwWUSaDTCXgd+hTyZfRb81h1P2iZaxBoyRQg4AwWkMVzWE2AZcsZ6OtouHadhFrDNfdQvO3XMWfnElUKiCMVlpY0xov4HVO4w1rb6G2XymqMbCFP1YRBtPT09Pal6f5EShGQBgtplQGurRAzbtES1vW2F/4ZaW2l923obSWsBYG7HAd1lKzGv+2zGl9AsLo+mpuxj0FarsE23PaDuspUNuXlRrvjw6DaFuvLmD74Yqei9xhBBYSEEYXgtZNWQK1BY2y9MsYbW2XtGueTzu3cCfUrmgZf2NGuR4BYXQ9tTbTCIHaPpgjpu7QngI17ZyHtxy0X8ROTk56KuR3WDiXdnRtncI52RXNr2ZGREAYtQYIbAjUeLlSkacXqOll97VcCQj/dtuKdyHUl8vp178WCUwpIIxOqamtKgR8cFVRxlknET6hXcMl3xp2eTefmu92QMOAald01j8LjRMYLCCMDqZzYo0CNV2urLE+ucyplp3EbvewC9SlBuuwHu2cwhf2+3KZy1+NcRDYLSCMWh0EAoGaQobCzidQw05ip1P6XDaDaBio7YrO9zegZQJTCgijU2pqq3iB0j+Yiy9AIROoZZ2El7ZL/PnPcNdz27tR7YoW8gdlmKsXEEZXvwQAdAJ2Ra2FPgI1vey+5LB2KIi2tazlS0OfdekYAiULCKMlV8/YJxUo+YN5UgiN7RWo6UtLt+ZLerBn89VNu15FVVOd/EkSqF1AGK29wubXS8C9Zb2YHBTstrUYpT7w0469xLDWN4jaFfWnSqAsAWG0rHoZ7UwCdkVngq2w2Vou/Za25jdf3bTvHtfwy+VTnvKU5vHHH69wJZoSgXoEhNF6amkmIwRqCRgjCJzaU6DES9vbplbSmo8JouGuaOm71z2XpMMIFC8gjBZfQhMYK1Di5cqxc3b+MIFwx63kS/RhYMv9ftF97xDdVsUbbrih+c9//nP2r3Kf27BV6CwC9QkIo/XV1IwiBUraIYqcmsMnFqjli0spoXrfO0R3lbaWGk28dDVHIGsBYTTr8hjc3AJ+h35u4braL+0+y1ID2+aDSu08+u5E+3JZ19+c2axDQBhdR53NcodALeFCgZcRqCXo5LzuN3dDt73MvtSQvcwq1QuB8gSE0fJqZsQTCtQSLiYk0dQOgRpfdh8T9OZeGNt2Q2Pv+cw5ZM/tp30CJQsIoyVXz9hHCbi3bBTf6k6uab10oa3vpe+5i735tPyQkOxdwXNXSfsE5hMQRuez1XLmAnZFMy9QZsOrZb3k9vBSuJvZlnzXLyodWg52RQ8J+fcE8hUQRvOtjZHNKODBpRlxK2w6twA3hjiXHd7N3dB2TkN3asM5xV7aH2PpXAIEphEQRqdx1EphAnZRCitY4uHWtF5yCKNjHlLathRqqk/ipa57AkkEhNEk7DpNLdB9eA29JJh6/PpfVqCm9ZIyuIU7zF0F9/2sZ58qh8G2feH9Y4891uc0xxAgkJGAMJpRMQxlGYEcdoaWmalephCobb2kCqNT74Z2tU01nynWljYIEPifgDBqJaxOoJYHUVZXuEQTri3sLLn+t+2EtmUcuxvaLQW7oon+KHRLYGIBYXRiUM3lLeDBpbzrk9vowndf1vJgzBJhdHMXNAyPJycnk5V5iblMNlgNESCwU0AYtThWJVDbLteqipdgsrVdoj+7HHbNNWeSU4frNrhfunRp6xPxc9ybXWNtEixxXRLIQkAYzaIMBrGUQE0PoixltuZ+atx5m/qF99te0dSF3nPnzjVHR0eTL6GnPvWpzeOPP37W7pAX5E8+IA0SIDBKQBgdxefkkgTspJRUrfRjrfWWjqnC6L4Qenp6OmsBXeGYlVfjBBYXEEYXJ9dhKoEad7lSWa6h31q/vIwNo9t+Q75dD1Nf9t+1xuyKruGvzxzXJiCMrq3iK51vuIsz1ZO8K6VczbRr/fIyJozO9XqmmEVlVzRGy7EEyhAQRsuok1GOFPABNhJwZaeHX16G/kRlrmRDwui23dCldkJDx/DWCfeK5rrCjItAvIAwGm/mjMIEanw9T2ElKG64NX95iQ2jU/6G/NiFUHNdxto4n0DJAsJoydUz9l4Ctd7712vyDhokUPNbF2LCaBj+Wsg5XtHUt0DhrmiKXdm+43QcAQLxAsJovJkzChOo9d6/wspQzHBr//LSJ4zO8RvyYxeAXdGxgs4nkK+AMJpvbYxsAoGa7/2bgEcTWwRq//Kyudt5aBHkcG9m+AWhfZr+woULh4bt3xMgUJCAMFpQsQw1XsBuSrzZms8I7y+u9a0LMWE0F4PavyCs+W/O3Am0AsKodVCtgAeXqi3tbBOr/RJ9B3cokOYSQtvxrqUmsy1qDRMoQEAYLaBIhjhMwIfYMLc1n2UHLq/q33jjjc1jjz12NqgcbhfIS8doCNQjIIzWU0sz2RAQLCyJGIHwoZ3a3i0a45DTsW6zyakaxkJgPgFhdD5bLScUsCuaEL/QrgWfvArnVU551cNoCMwpIIzOqavtZAKCRTL6Yjuu+d2iJRbF33CJVTNmAsMEhNFhbs7KWMCDSxkXJ9Oh2UnPqzBe5ZRXPYyGwNwCwujcwtpfXECwWJy8+A7twuVVQvd751UPoyEwt4AwOrew9hcX8EG2OHnRHdpJz6t8vkzmVQ+jIbCEgDC6hLI+FhPwQbYYdTUdWTP5lNKrnPKphZEQWFJAGF1SW1+zC7jcOjtxdR3YSc+npP5+86mFkRBYUkAYXVJbX7MLdB9m7W/SHx8fz96fDsoWCF8flNOvDpWtOmz0XuU0zM1ZBGoQEEZrqKI5nAm43GohxApYM7Fi8x2/1K5oW/PT09P5JqJlAgSiBYTRaDIn5CogWORamXzH5RJ9PrWZsxbhrms7Yz8tmk/djYTA2d/kZb97ZyVUIjDnh1klRKYRCLS3cnQ7ZP5nMO3SmOuLZFjjzRmqedqa651AKCCMWg/VCPgFnWpKuchElrosvMhkCu9kji+SYX238QijhS8aw69KQBitqpzrnUz4rkgfMutdBzEz9+UlRmu+Y8NL6O2rnR599NHRnYVBtP3PL3jBC5oHH3zw7PL82SXBa65x3+hoZQ0QmE5AGJ3OUksJBea6zJdwSrqeUcB6mRE3sukpd6hf/OIXN3/+85+vjCAMnWHNn//85zd/+ctfIkfqcAIE5hIQRueS1e6iAlN+oC06cJ0lEbBDloT9CZ0+7WlPay5cuHD23499HdtmEG3D58nJyZW2u/uD7YrmUXujIBAKCKPWQxUCwkUVZVxkEt4tughzr06m2qHefFApDLabT9K7jadXaRxEYFEBYXRRbp3NJeBl93PJ1teuXfR8ajrFl8gw0LYze9GLXtT86U9/Optkezn+b3/725UJC6L51N5ICNgZtQaqEgh3PnzYVFXayScTPug29rLw5INbWYNT7IpuBtHNv//wi8fznve85q9//evKlE2XQBkCdkbLqJNR7hGY4kMN8DoE7IrmU+extYgJor545FN3IyGwTUAYtS6KFxj7oVY8gAn0Egh3RT3E0otstoPG/g79S1/60uaPf/zjzsvvYVANH2SabUIaJkBglIAwOorPyTkITHHfWQ7zMIZ5BXxpmdc3pvWxtdh3fhhEfemIqYpjCaQTEEbT2et5IgEPL00EWXEzdkXzKW4YFttXO/373/+OGty+XU9PzkdROphANgLCaDalMJAhAh5eGqK2vnPcV5xPzcfsioaX57fteo5pOx8hIyGwPgFhdH01r2rGQkZV5ZxtMkLKbLRRDY/dFd1Xx/Df3XTTTVe90ilqkA4mQGBxAWF0cXIdTikgZEypWWdbm09dt7P0CrA0tR5zf3dYx5e85CXNH/7whyuT8MBSmnrqlcBUAsLoVJLaSSIw5sMtyYB1uqjAtiAqjC5agq2BMfbLQPgLS5uX5/f9uzQz1SsBArECwmismOOzEujC6Pnz55ujo6OsxmYwaQXCXfNwJLFBKO0s6uj96U9/evPoo4+eTWbIE+67roC0L7L/+9//fgVJbetYL2axPgFhdH01r2bG4Y6ID6Fqyjp6IptPVHcN+sIymnZwA2NupwnP3bw8P6bdwZNxIgECkwsIo5OTanApAQ8vLSWddz/ta5suXbq09z5QQTRdDcfsiu57Z2gYRP3CUrr66pnAFALC6BSK2kgi4H7RJOxZdBruivcZkJ3zPkr9jvn4xz9+duC9997bfPvb3z540tDdy33vDPVi+4PsDiBQlIAwWlS5DDYUEEbXsx767H5uro0ugNoVnXadhOGy/c9ve9vbmu985zs7OxnyoxQvf/nLm9///vdX2gy/TAii09ZTawRyEBBGc6iCMUQLhL+oY9crmq+YE3Y9Dd9NoAs63RroHo4ZuhtXDEzCgX7sYx87673bIf3MZz7TfPSjH906oqG30oT1e9nLXtb87ne/O2tfEE1YeF0TmFFAGJ0RV9PzCQz9kJtvRFqeSmDXA0ht+21IaWt/fHx81t3m5fo2lIbn2xUdV5W3vvWtzV//+tfm4Ycfbh566KEz++6frgb7vgwO+Tvd9c5QQXRcLZ1NIGcBYTTn6hjbTgGX6OtaHIcuw297QCXcHW81wt3RTseu+f8kvvzlLzef+MQnrlo07auWLly4cPbfDXndUnveobAf+3e6K3AKonX9vZsNgU0BYdSaKE7AzldxJds54H2X4dt/d3JysvXcXUE0XBu1PGHd/rRl90+7Q9nuSG7enrDEiuh2pcO+ut3RXf3HhNFdDywJoktUVx8E0goIo2n99T5AYMilvwHdOGUmgX1PwvfdoQvvKQx353K+V3RbqJyJOLrZ1hEkglsAABiCSURBVK0N+C984QvPzv3Nb34T3ca2E/qG5l0PLAmik5RBIwSyFxBGsy+RAW4KxOy20MtDoN31Oj093fou0Lae586d6/0LWruCaLhbOnZX9NOf/nTz2c9+didee3l7167tkuJtWHv+859/VZftrxL95Cc/WXIYW/uK+dK47YElQTR5CQ2AwGICwuhi1DqaQiC8lOd+wClE52uje9hlV532XYbfNqo777yz+eQnP3nlX7UBZmzonG/2cS0/5znPuXLCrbfe2nz961+PayDDo/uG0W0PLAmiGRbUkAjMKCCMzoir6ekF+n7ATd+zFvcJHAqe4bltiKz1i0SNoXLoyu9zBWPznaXt7rkgOlTceQTKFRBGy63dKkfe5wOuBJh2Hu3/a3d63//+9zdf+tKXkg/7zW9+c3Pfffc9YRwXL14sMjw++clPbp7ylKfsdH3kkUeSm9c8gH1/q5sPK3X3CguiNa8IcyOwW0AYtTqKEQgffCl5Z+2OO+5ovvrVrxbjvsRA29q2DyKF/zz22GNn/+ehgBLeK9r3Aagl5rT2PnY9vLT5BoXudo1DdV67p/kTqFlAGK25upXNLecnpWOo21+U+cMf/hBzSpbHdvXodh9vueWW5vvf//6kY+0TUNy6MSn5JI3tqkn4N9x29IpXvKJ54IEHDn7hmGRQGiFAIFsBYTTb0hjYpkD3QRb74EuOkpsh69WvfvXZ63TaJ7Sn3PVt+2kvV2/+c+nSpbP/qm9fnX17T99S//QJou1Yarl1YynXJfrZ/OJ48803N7/97W+v6rpbe33rvMS49UGAQBoBYTSNu14jBWq5RB9Oe6kP4ZiHi8Jwt2Tw3FwOfW3sikb+IS10ePgFYfPWifD/3vZzrgsNUTcECGQkIIxmVAxD2S1QyyX6oaGr79ooMXiOMbEr2ndlLHtcWJdw9z18FZcgumxN9EYgZwFhNOfqGNsVgZou0Y8JX5s7q+3/nfOl9tgl3HdHtG3Xrmis7jLHbz6g1PUartPnPve5zT//+c8rA+q7hpeZgV4IEFhaQBhdWlx/0QJrCB37Qlj7tHh7j2fMB3aKezyjC7txQkwQbU+1KzpWfJ7zNx9S2rxMvxlE23ez/uMf/5hnMFolQKAIAWG0iDKte5BrCR27dpQOVb/E4Dl2dzi0Cn+b/pCVfz+vwCtf+cqrftd+26u2wrAqiM5bD60TKEVAGC2lUiseZ/fhVctPP3al3Pd77bvK3b0sP4ffRZ9qScbuiIa7ou1/jtkxnmrM2nmiwOY9oNveehEG0RreimEdECAwjYAwOo2jVmYSqOUS/ZDguUnaXt78+9//PpN0mmaHBFG7omlqta/Xbbv6m18SBNH86mZEBHIREEZzqYRxbBUo8RJ9u0PUfhDH7Ni182w/0I+Pj684vOc972nuvvvuJ7jcfvvtzV133VX8igl30mJ+OanWNyuUWtA+DywN+dJRqodxEyAQLyCMxps5YyGB8Gcec71EP1Xw3Ed60003bX3AIybsLlSy3t2EtW1P6juX8DfN3Svam3u2AzeDaFvH8GdANy/dx3zpmG3QGiZAIDsBYTS7khhQJ5DbJfohl9rbD99z5841R0dHowu7bQeq1A/3obubQ88bja+BJwi0v+wV7uR3Xyg2n6YP/55rutfZkiBAYDoBYXQ6Sy1NLJDDJfqYJ9ynDJ77KLeN6V3veldzzz33TFyBeZoLw0rM7ma4y5brTvk8Ynm2uuuLwbYw+uxnP/uq94rmOSOjIkAglYAwmkpev3sFwsu4MYFlLGufy+65vEppM5SWsEsajjn2aWq7omNX93Tnh7V41ate1fzqV7+60rgHlaZz1hKBtQgIo2updGHzXOoSfd8Xyuca9Nodp4ceeuiq6ua6SzrmIRZP0OfzB9znC0X7pc4l+XxqZiQEchcQRnOv0ErHN+cl+s2HKrYRL3XJfary5r5LOvTJ+dYnfGgp1y8FU9Ux93bG1DH3uRkfAQLpBITRdPZ63iMw5Yvu+zx4VMPL5DcfKGl5+z6lPudiHPrkfDcml+fnrE7/tl/96lc3v/71r6+ckMPa6j96RxIgkLOAMJpzdVY6tiku0fd58Ki03c++y2HzAZI2jP/3v//te/rkx40Jky7PT16OwQ2OqePgTp1IgMAqBITRVZS5rEkO+dDr++DR5ovly5LpP9pcdkmHPjnfzjTcUXV5vn/t5zgyrGPqLzdzzE+bBAikFRBG0/rrfYtA98G372nr0h88WqLw7373u5tvfOMbV3X1rGc9q/nXv/61RPdnvyjVXcqNfXK+HeCQLyWLTGxFnbz2ta9tfvnLX16ZsS8FKyq+qRJYUEAYXRBbV4cFwgckNu9Jq/HBo8Mi449I8XDT2IeOwjF7p+j4NTCkhRTrZsg4nUOAQPkCwmj5NaxqBpuv/2knt+9BiRoePFqigEvvko7d1Rx7/hKmtfaxuRvazrO97ePSpUu1Ttm8CBBILCCMJi7Amrvvc5/nNp9aHzxaYi0stds15tVcgugSK2F7H0utj3Qz1DMBAjkKCKM5VqWiMbUfbod2Nw9Ntw0na3nw6JDFFP9+2y7pM57xjObhhx+eovmr7hWNff2P91hOUoLoRuyGRpM5gQCBCQWE0Qkx19hU9yDRFIEzDC6xIWaN9mPnvO31V7fddlvzzW9+c1TTdkVH8S1+st3Qxcl1SIDAhoAwakkcFOheGj9F4Gzb2LXLOSbEHJyEA7YKvOMd72i+9a1vXfXvxjwxHT64dP78+ebo6Ki3vHeK9qaa5EC7oZMwaoQAgQkEhNEJEGtpou/rkvbNtwuU586diwoibZtT/upSLTVZah7bdkmHXLofer/n2Kfvl3KqpR+7obVU0jwI1CEgjNZRx8Gz6PNTmZuNd4Hj9PR0cL+bJ07xq0uTDWbFDW0LpX1vmQhfUh/7OqahIXbFpYqe+hve8Ibm5z//+RPO86R8NKUTCBCYWEAYnRi0hOb6BNClX5nkEn0+K2fopfuhXyi8U3Te2re3S2x7LdOY2zHmHbHWCRBYm4AwupKKH3phfOrXJfX51aWVlCqbacY+4DTkC4Wf/Jyv3Nvq1/YmhM5nrmUCBIYJCKPD3Io4q08AnfJS+1CUfb+6NLRN500nEF5C3xVmhu6Kujw/XZ3altpL8ffff//WH4po7+O+ePHitB1qjQABAhMICKMTIObUxK7dkG6MOe6KCCQ5raDtY3nmM5/ZPPLII1f9y3AtDanh2N+uz19tuRHuuxT/ute9rvnZz3623GD0RIAAgUgBYTQSLMfDDwXQ9t+fnJzkOPSzMQ25vJvtZCof2La11tave8gp5sGlIQG2ct7o6bkUH03mBAIEMhQQRjMsyqEh9XkFU+4BtJtj+Eqfvk9tH/Lx7+cV2PaAU/elou9tH2EQjX0f6byzK6P1XSG0lL/7MpSNkgCBpQSE0aWkR/Zz6Gc1S/3JzKH3Go7kdPoEApv3krZN9tkZ9U7R4fi7Qqj7QYebOpMAgfQCwmj6Glw1gj6vXepOKDWAhhN2iT6zBdhzOIduDdn3s6Iuz/dEDg7bdXtE353o+B6dQYAAgeUEhNHlrJ/QU7uL1F6ajrk8nfoVTFNzdcHEpdqpZedrLwxG3UNMfd9N6ic/4+oihMZ5OZoAgTIFhNGF6tbnPs/NoSz94vmFKK504xL90uLj+9sWRMNWw0vw4X/fntf+0k/3aqEc3+owXme6FoTQ6Sy1RIBA/gLC6Iw1igmgNVxyj6UURmPF0h4f84L6Q5fxY64GpJ31sr0Loct6640AgTwEhNGJ63AogHaXpdsHDo6Ojibuvazm3C9aVr2GfHl4+tOf3jz66KNbJ2p39P+zCKFl/S0YLQEC0woIoxN57tsJqu0+z4nIrrxf1OtophKdt52hXx7CHdVtI2zbvfHGG5t///vf804gw9aF0AyLYkgECCwuIIyOJN/30mm7n7txw4Diku3IRbjA6WN+snXz6fk175bue2jRTvECC1kXBAhkKSCMDizLrh0NAbQf6JBLvv1adtQcAkNfx3ToJz/3XVF42tOeVtxu6S233NL89Kc/PStB3y9ZQugcK1abBAiUJCCMRlYr3CHqTvVhEokY/ARozId2fC/OmEqgC6Mxt1SEfyuH/kbe/va3N9/73ve2Briu7xtuuKG5cOHCVFMa3M6QV7Jt6yzGcvBgnUiAAIECBITRiCJt7uIc+oCNaHp1hw69/3B1UBlMeMgu9uZ9on13CdvpbvvCt4uhW0dTvvz9TW96U/PjH/84anfz0Pja9+j+5z//yaCahkCAAIH8BITRiJoIUBFYBw71svvpLOduaci6H3pZP5zLvt3Suecc0/4cgTimf8cSIECgdAFhNKKC3YdOzC5PRPOrOXTMwzCrQcpoorFfHMIgOscva7U7te0/S/0ddvNpd3sff/zxjCpjKAQIEKhDQBiNqKMwGoG159Ahl32n6VkrsQKxXxwOPbAU23/M8e3robqwGBtU7W7GSDuWAAEC0woIoxGewmgE1p5Dh1z2naZnrcQKxHxxOPRTobF9O54AAQIE1iEgjEbUWRiNwBJGp8FK3Erfez9jnpxPPCXdEyBAgEBmAsJoREGE0QisHmE09lLqNL1rJUagzy72mCfnY8biWAIECBCoU0AYjairMBqBtePQmMu+43vTwliBPu8X7bt7OnYszidAgACBOgWE0Yi6CqMRWMLoeKwMWji05ud+cj4DAkMgQIAAgZkFhNEI4EMfzBFNrfbQPpd9V4uT4cT3rfkwiPo1oQyLZ0gECBAoREAYjSiUMBqBtePQPpd9x/eihakEtq35zV9Iao+Z8heQphq7dggQIECgDAFhNKJOwmgE1pZDwwddPLw0znKps8M1v/mgUjsGO6JLVUI/BAgQqFdAGI2orTAagbXlUA8vjfNLcXZ4KX6z/zl+XSnFHPVJgAABAmkFhNEIf2E0AmvLoZ66HueX6uzNQGo3NFUl9EuAAIE6BYTRiLrG/kZ3RNOrONTDS2WWWd3KrJtREyBAoBQBYTSiUj6UI7D27Iy6vDvOMcXZ7f2iR0dHKbrWJwECBAhULiCMRhTYPY8RWBuHhk9ge3hpuKMzCRAgQIBAbQLCaERFPQ0egbVxqCA/3M6ZBAgQIECgZgFhNLK6LtVHgv2/w7kNc3MWAQIECBCoXUAYjaywJ8IjwYTRYWDOIkCAAAECKxEQRiML7XJzJFjTNNddd11zcnJydqL7ReP9nEGAAAECBGoWEEYjqxveN+pnEPvh2U3u5+QoAgQIECCwRgFhdEDVw91RgXQ/YBjevSx9wGJzCgECBAgQqFxAGB1YYIG0H5xd0X5OjiJAgAABAmsVEEZHVF4gPYznKfrDRo4gQIAAAQJrFhBGR1Y/DKTti92Pj49HtljP6R72qqeWZkKAAAECBOYSEEYnkHUpejuiXdEJFpcmCBAgQIBA5QLC6AQF9oT9ExHtik6wsDRBgAABAgRWICCMTlRku6NXQ/KYaGFphgABAgQIVC4gjE5YYJel/4fZ3jt7enp69p/Pnz/fHB0dTaisKQIECBAgQKAmAWF0wmqGl6bXHMLsik64qDRFgAABAgQqFxBGJy7w2oNY+NOf3i4w8eLSHAECBAgQqFBAGJ24qOEl6jWGsbWH8YmXk+YIECBAgED1AsLoDCVeayDz058zLCZNEiBAgACBygWE0RkKvNZXPa01hM+whDRJgAABAgRWIyCMzlTqNQYzbxOYaTFplgABAgQIVCwgjM5U3LXtjnrJ/UwLSbMECBAgQKByAWF0xgKvKaDZFZ1xIWmaAAECBAhULCCMzlzcNVyuX1Ponnm5aJ4AAQIECKxOQBidueRreBH+GgL3zMtE8wQIECBAYLUCwugCpa85rPnpzwUWkC4IECBAgEDFAsLoAsUNf5Wo3Sk9OTlZoNdluqg5aC8jqBcCBAgQILBuAWF0ofrXGNr89OdCi0c3BAgQIECgYgFhdKHihq96aru8fPnyQj3P102NAXs+LS0TIECAAAEC2wSE0QXXRfgwUw2X673OacHFoysCBAgQIFCpgDC6cGFr2U30OqeFF47uCBAgQIBApQLC6MKFreVyvV3RhReO7ggQIECAQKUCwmiCwpZ+ud7rnBIsGl0SIECAAIFKBYTRRIUt+XJ9yWNPVG7dEiBAgAABAjsEhNFES6PUy/XhuNsd0uPj40SCuiVAgAABAgRqEBBGE1axxMv1HlxKuGB0TYAAAQIEKhQQRhMXtbRL3h5cSrxgdE+AAAECBCoTEEYTF7Sky/V2RRMvFt0TIECAAIEKBYTRDIpayuV6u6IZLBZDIECAAAEClQkIo5kUNLxcf/78+ebo6CiTkf1vGOHv0Oc4vqywDIYAAQIECBDoLSCM9qaa/8Cc7x/NeWzzV0YPBAgQIECAwFwCwuhcsgPaDV8m34a/09PTAa1Mf0p4X2t7S8HJycn0nWiRAAECBAgQWKWAMJpZ2cP7R3O5HO7BpcwWieEQIECAAIGKBITRDIuZ2yVxDy5luEgMiQABAgQIVCIgjGZYyPCyeOrL9XZFM1wghkSAAAECBCoSEEYzLWYYAlP+7GZuu7SZlsuwCBAgQIAAgYECwuhAuCVOSx0Ew9c5pQzES1jrgwABAgQIEEgjIIymce/Va+pfZ0odhnshOYgAAQIECBAoWkAYzbx84eX6pe8f9eBS5ovD8AgQIECAQAUCwmgBRQx3KJe6XO7BpQIWhiESIECAAIEKBITRQoq49CVzu6KFLAzDJECAAAEChQsIo4UUcMn7R/0OfSGLwjAJECBAgEAFAsJoQUVc6udCl96FLagEhkqAAAECBAhMLCCMTgw6d3PhvZxz/E58uAO71P2pc5tpnwABAgQIEMhXQBjNtzY7RxbuXE79+/UeXCpwQRgyAQIECBAoWEAYLbB4c94/6sGlAheEIRMgQIAAgYIFhNFCixc+ZNRO4fLly6NnYld0NKEGCBAgQIAAgUgBYTQSLKfDp34hvgeXcqqusRAgQIAAgXUICKOF13mqF+J7cKnwhWD4BAgQIECgUAFhtNDChcOe4oEml+grWAimQIAAAQIEChQQRgss2uaQp3igyYNLFSwEUyBAgAABAgUKCKMFFm3bkMe8ED88d4oHoSohNQ0CBAgQIEBgAQFhdAHkpboIL7XHvH/Ug0tLVUg/BAgQIECAwKaAMFrZmhhyub07Z45fdKqM13QIECBAgACBiQWE0YlBUzcX+yBS7PGp56d/AgQIECBAoC4BYbSuejbhw0ztjufp6eneGQ7ZSa2MzHQIECBAgACBhALCaEL8ubru+6qnofeYzjVu7RIgQIAAAQLrExBGK635oYeSpv71pkoZTYsAAQIECBCYWUAYnRk4VfP73j0qiKaqin4JECBAgACBTQFhtOI1EYbObdPsc09pxTymRoAAAQIECGQgIIxmUIQ5h7ArkAqic6prmwABAgQIEOgrIIz2lSr4uM1L9jEvxC942oZOgAABAgQIFCAgjBZQJEMkQIAAAQIECNQqIIzWWlnzIkCAAAECBAgUICCMFlAkQyRAgAABAgQI1CogjNZaWfMiQIAAAQIECBQgIIwWUCRDJECAAAECBAjUKiCM1lpZ8yJAgAABAgQIFCAgjBZQJEMkQIAAAQIECNQqIIzWWlnzIkCAAAECBAgUICCMFlAkQyRAgAABAgQI1CogjNZaWfMiQIAAAQIECBQgIIwWUCRDJECAAAECBAjUKiCM1lpZ8yJAgAABAgQIFCAgjBZQJEMkQIAAAQIECNQqIIzWWlnzIkCAAAECBAgUICCMFlAkQyRAgAABAgQI1CogjNZaWfMiQIAAAQIECBQgIIwWUCRDJECAAAECBAjUKiCM1lpZ8yJAgAABAgQIFCAgjBZQJEMkQIAAAQIECNQqIIzWWlnzIkCAAAECBAgUICCMFlAkQyRAgAABAgQI1CogjNZaWfMiQIAAAQIECBQgIIwWUCRDJECAAAECBAjUKiCM1lpZ8yJAgAABAgQIFCAgjBZQJEMkQIAAAQIECNQqIIzWWlnzIkCAAAECBAgUICCMFlAkQyRAgAABAgQI1CogjNZaWfMiQIAAAQIECBQgIIwWUCRDJECAAAECBAjUKiCM1lpZ8yJAgAABAgQIFCAgjBZQJEMkQIAAAQIECNQqIIzWWlnzIkCAAAECBAgUICCMFlAkQyRAgAABAgQI1CogjNZaWfMiQIAAAQIECBQgIIwWUCRDJECAAAECBAjUKiCM1lpZ8yJAgAABAgQIFCAgjBZQJEMkQIAAAQIECNQqIIzWWlnzIkCAAAECBAgUICCMFlAkQyRAgAABAgQI1CogjNZaWfMiQIAAAQIECBQgIIwWUCRDJECAAAECBAjUKiCM1lpZ8yJAgAABAgQIFCDwf9BqXbZ7iwlQAAAAAElFTkSuQmCC', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAqMAAAGqCAYAAAA/eE09AAAgAElEQVR4Xu3dCZQU5bn/8QdZRoZVEEV0gsgWE0LCECBoAtEocQVcUCMcA8GYGDXG3QTk5gDuccdck6uCiQQOaJIhAW8EDWAuomwqqAiCCgoCIzvDDDPM/M9T+Tdpmp7u6uqqft+q+tY595ArVe/7vJ+35syPWt5qUFdXVydsCCCAAAIIIIAAAggYEGhAGDWgTpcIIIAAAggggAACjgBhlBMBAQQQQAABBBBAwJgAYdQYPR0jgAACCCCAAAIIEEY5BxBAAAEEEEAAAQSMCRBGjdHTMQIIIIAAAggggABhlHMAAQQQQAABBBBAwJgAYdQYPR0jgAACCCCAAAIIEEY5BxBAAAEEEEAAAQSMCRBGjdHTMQIIIIAAAggggABhlHMAAQQQQAABBBBAwJgAYdQYPR0jgAACCCCAAAIIEEY5BxBAAAEEEEAAAQSMCRBGjdHTMQIIIIAAAggggABhlHMAAQQQQAABBBBAwJgAYdQYPR0jgAACCCCAAAIIEEY5BxBAAAEEEEAAAQSMCRBGjdHTMQIIIIAAAggggABhlHMAAQQQQAABBBBAwJgAYdQYPR0jgAACCCCAAAIIEEY5BxBAAAEEEEAAAQSMCRBGjdHTMQIIIIAAAggggABhlHMAAQQQQAABBBBAwJgAYdQYPR0jgAACCCCAAAIIEEY5BxBAAAEEEEAAAQSMCRBGjdHTMQIIIIAAAggggABhlHMAAQQQQAABBBBAwJgAYdQYPR0jgAACCCCAAAIIEEY5BxBAAAEEEEAAAQSMCRBGjdHTMQIIIIAAAggggABhlHMAAQQQQAABBBBAwJgAYdQYPR0jgAACCCCAAAIIEEY5BxBAAAEEEEAAAQSMCRBGjdHTMQIIIIAAAggggABhlHMAAQQQQAABBBBAwJgAYdQYPR0jgAACCCCAAAIIEEY5BxBAAAEEEEAAAQSMCRBGjdHTMQIIIIAAAggggABhlHMAAQQQQAABBBBAwJgAYdQYPR0jgAACCCCAAAIIEEY5BxBAAAEEEEAAAQSMCRBGjdHTMQIIIIAAAggggABhlHMAAQQQQAABBBBAwJgAYdQYPR0jgAACCCCAAAIIEEY5BxBAAAEEEEAAAQSMCRBGjdHTMQIIIIAAAggggABhlHMAAQQQQAABBBBAwJgAYdQYPR0jgAACCCCAAAIIEEY5BxBAAAEEEEAAAQSMCRBGjdHTMQIIIIAAAggggABhlHMAAQQQQAABBBBAwJgAYdQYPR0jgAACCCCAAAIIEEY5BxBAAAEEEEAAAQSMCRBGjdHTMQIIIIAAAggggABhlHMAAQQQQAABBBBAwJgAYdQYPR0jgAACCCCAAAIIEEY5BxBAAAEEEEAAAQSMCRBGjdHTMQIIIIAAAggggABhlHMAAQQQQAABBBBAwJgAYdQYPR0jgAACCCCAAAIIEEY5BxBAAAEEEEAAAQSMCRBGjdHTMQIIIIAAAggggABhlHMAAQQQQAABBBBAwJgAYdQYPR0jgAACCCCAAAIIEEY5BxBAAAEEEEAAAQSMCRBGjdHTMQIIIIAAAggggABhlHMAAQQQQAABBBBAwJgAYdQYPR0jgAACCCCAAAIIEEY5BxBAAAEEEEAAAQSMCRBGjdHTMQIIIIAAAggggABhlHMAAQQQQAABBBBAwJgAYdQYPR0jgAACCCCAAAIIEEY5BxBAAAEEEEAAAQSMCRBGjdHTMQIIIIAAAggggABhlHMAAQQQQAABBBBAwJgAYdQYPR0jgAACCCCAAAIIEEY5BxBAAAEEEEAAAQSMCRBGjdHTMQIIIIAAAggggABhlHMAAQQQQAABBBBAwJgAYdQYPR0jgAACCCCAAAIIEEY5BxBAAAEEEEAAAQSMCRBGjdHTMQIIIIAAAggggABhlHMAAQQQQAABBBBAwJgAYdQYPR0jgAACCCCAAAIIEEY5BxBAAAEEEEAAAQSMCRBGjdHTMQIIhFWgqKhI6urq5MCBA2EdAnUjgAAC1ggQRq2ZCgpBAAHbBWpra6VRo0ZOENUt8aftdVMfAgggYLMAYdTm2aE2BBCwQqBdu3ZSXl5+RC0XXHCB/O1vf7OiRopAAAEEwipAGA3rzFE3AggEKjBt2jS58sor6+2jVatWsnPnzkBroHEEEEAgDgKE0TjMMmNEAIGcBJo0aSLV1dX1HtOgQQPRW/ZsCCCAAAL5CxBG8zekBQQQiIjA2LFj5e677z5sNI0bN3ZeVNIAmth4VjQiE84wEEDACgHCqBXTQBEIIGBaQENnTU3NoTKSr34mB9EHHnhAbrvtNtPl0j8CCCAQGQHCaGSmkoEggIAXgUGDBsncuXMPO/Siiy6SP//5z85/a9q0qVRWVjr/u1u3bvLBBx946YZjEEAAAQTqESCMcmoggEAsBf7whz/ID3/4w8PGnvosaN++fWXJkiXOPonb9bHEYtAIIIBAgAKE0QBxaRoBBOwTWLRokZx++ulHFHbGGWfIq6++eui/l5WVydChQw/9/zwnat9cUhECCERDgDAajXlkFAgg4ELgqKOOOmKhev1vBw8ePOLo5OdEN2zYICUlJS56YBcEEEAAgVwFCKO5irE/AgiETqBhw4ZHLMWUaXmmrl27yocffuiM8/LLL5fp06eHbswUjAACCIRFgDAalpmiTgQQyFkg+eWjxMEaQl955RXR2/LpNn1ZSY/Trb6rpjkXwgEIIIAAAvUKEEY5ORBAIHICLVu2lD179hwxLl1HdMKECRnHm3wVtaqqSnQBfDYEEEAAgeAECKPB2dIyAggUWKBnz56ycuXKI3r9zne+IwsXLsxajb5dr2/Z69a9e3dZvXp11mPYAQEEEEAgPwHCaH5+HI0AAhYIXH/99fLkk08eUYk++7lmzRrXFfKVJddU7IgAAgj4JkAY9Y2ShhBAwIRAujfkW7duLTt27MipnGbNmklFRYVzzMyZM+XSSy/N6Xh2RgABBBDwJkAY9ebGUQggYFjgK1/5irz//vuHVVFUVHToa0m5lPfHP/5RrrrqKueQ4uJi2bdvXy6Hsy8CCCCAQB4ChNE88DgUAQTMCKReDc20TJObCrk970aJfRBAAIFgBAijwbjSKgIIBCDQqlUr2b1792Et9+vXTxYvXuy5t+Q1RUeOHCmTJ0/23BYHIoAAAgjkLkAYzd2MIxBAoMACtbW1oksuJW9+rAHKmqIFnki6QwABBNIIEEY5LRBAwGqBxo0bS01NzWE1/uY3v5Fbbrkl77pZUzRvQhpAAAEE8hYgjOZNSAMIIBCEwA033CCTJk06rGn9MlLijfd8+9Rb8s8995zTzCmnnCLr1q3Lt0mORwABBBDwIEAY9YDGIQggEKxAum/JHzx40Pk8p18bLy35JUk7CCCAQH4ChNH8/DgaAQR8FEhe6zPRbJcuXWTt2rU+9iKS3M/TTz8to0eP9rV9GkMAAQQQcC9AGHVvxZ4IIBCQQLqF6/Ndrqm+UqdNmyZXXnml89d+3vYPiIZmEUAAgcgLEEYjP8UMEAE7BV577TUZOHCg1NXVHVHgTTfdJA8//HAghXN7PhBWGkUAAQQ8CxBGPdNxIAIIeBEYMGCAaBBNt7Vt21bKy8u9NOvqmOQ1RYcNGyYzZsxwdRw7IYAAAggEJ0AYDc6WlhFAIEng+OOPl61bt6Y1Oeecc+Sll14K1Is1RQPlpXEEEEDAswBh1DMdByKAgBuBJk2aSHV19RG76u3yjz76SDp27Oimmbz3SX5Df/PmzdK+ffu826QBBBBAAIH8BQij+RvSAgIIpAjoVcji4uK0z4NqCNVF7P1cpinbBOjb8s8++6yz28knn+yEYDYEEEAAATsECKN2zANVIBAJgfHjx8uvf/3rtCFUv6R04MABI+PkpSUj7HSKAAIIuBIgjLpiYicEEMgk8OUvf1k++OCDtLu0a9eu3mdFC6HavHlz2bdvn9PVI488Ir/4xS8K0S19IIAAAgi4FCCMuoRiNwQQOFLg6KOPlqqqqrQ0l1xyibzwwgtG2aZPny4/+MEPnBpYU9ToVNA5AgggUK8AYZSTAwEEchZIt0i9NqK3w99//33p3r17zm0GcUBynfoSVaNGjYLohjYRQAABBPIQIIzmgcehCMRJYPbs2XLhhRfW+1JSbW2tVRylpaWyYsUKp6af/OQn8tRTT1lVH8UggAACCPxbgDDKmYAAAhkFevXqJW+99VbafYqKikTfnLdt0xeltDbd9GpouqWlbKuZehBAAIG4ChBG4zrzjBuBLALJL/6k7tqpUydZv369tYb6fGgiJL/yyity5plnWlsrhSGAAAJxFyCMxv0MYPwIpAgkLw6f/Ff6POgvf/lLufvuu602Gzt27KEav/a1r8k777xjdb0UhwACCMRdgDAa9zOA8SMgInv37pWWLVuG5nnQTJPGmqKc0ggggEC4BAij4ZovqkXAV4Ef//jH8swzz1i3SL3XQfbs2VNWrlzpHD5mzBiZOHGi16Y4DgEEEECgQAKE0QJB0w0CNgl06NBB9Pvs6bZjjz1Wtm3bZlO5rmqpqKiQZs2aOfvy0pIrMnZCAAEErBAgjFoxDRSBQGEEmjRpkvbNcr21fdFFF8mLL75YmEIC6EU/N6rfvNdNF+LXsbIhgAACCNgvQBi1f46oEIG8BHSZI/1SUl1d3RHtaAjVtTi//vWv59WH6YPvuOMOeeCBB5wyevfuLUuXLjVdEv0jgAACCLgUIIy6hGI3BMImoIu8/+xnP0sbQvXLRAcPHgzbkOqtN/HSkv5p2+L7kUFmIAgggEBAAoTRgGBpFgFTAqeeeqqsXr06bfctWrSQ3bt3myotkH5PPPFE2bRpk9O2PmZw8cUXB9IPjSKAAAIIBCNAGA3GlVYRKKjAli1bRF9KSndVUK8WfvOb35Q333yzoDUVorP//d//lXPPPdfpShe615eY2BBAAAEEwiVAGA3XfFEtAocEkl/YSceiIfTxxx+X66+/PrJq+tZ84nGDdM/ERnbgDAwBBBCIkABhNEKTyVCiLaAvIenLSNlCV1yemxwyZIjMmjXLmfTzzjtPZs+eHe0TgNEhgAACERUgjEZ0YhlWuAX0dvsxxxwje/bscRU+p0yZIldddVW4B51j9YmXlqL2MlaODOyOAAIIhF6AMBr6KYzHAO6991559dVXZe7cuZEd8PHHH+8sNu/myufZZ58t//jHPyJrkW1gGtR37tzp7Pa3v/1NLrjggmyH8PcIIIAAApYKEEYtnRjKOlwgikv36KcrV61a5Sp86stJn376KaeFiHNrXm/R66ahdPv27bgggAACCIRYgDAa4smLS+ndunWTtWvXHjZcDaePPvqo/PznPw8Nw/Dhw2XatGlZw6cOqKioSCorK0MztkIW2rBhw0OrBmS7ilzIuugLAQQQQMCbAGHUmxtHGRDQZwPr+4rQcccdJ59//rmBqo7scsaMGXLttdc6t5G1XjeBKS4vHeU7Qeecc86hxxNuuOEGZ7UANgQQQACBcAsQRsM9f7GrXpcpevLJJzOOO3FLXxd41/U39S10P7cHH3xQxo8fL/v27XOadRM2U/vXGrdu3SrHHnusn6VFvq3E3OrV0cR36CM/aAaIAAIIRFyAMBrxCY7y8PR76itXrvQUBgvtoiFq1KhR8swzzxS668j0V1xcLPv373fGoy9vDRo0KDJjYyAIIIBAnAUIo3Ge/YiNvb7b+IUYZuKKnQYmfeO/f//+heg2Nn3os7ZXXnmlM96SkhLZsGFDbMbOQBFAAIGoCxBGoz7DMR+fLoQ+YsQI2bVrlyPh5Za6HpcIm61bt+btbQPnVPI/NLzOoYGy6RIBBBBAwIUAYdQFErsggIA5gV69eslbb73lFHD77bfL/fffb64YekYAAQQQ8F2AMOo7KQ0igIBfAvr5U13mSjf9Dn11dbVfTdMOAggggIAlAoRRSyaCMhBA4EiBxo0bH3prXlcv0Gdy2RBAAAEEoiVAGI3WfDIaBCIjMHbsWLn77rud8XzjG9+QFStWRGZsDAQBBBBA4D8ChFHOBgQQsFIg8dKYFsdLS1ZOEUUhgAACvggQRn1hpBEEEPBToGfPns4asrpNnz5dLr/8cj+bpy0EEEAAAYsECKMWTQalIICAyLx58+Tss892KJo2bSoVFRWwIIAAAghEWIAwGuHJZWgIhFFA35o/ePCgU7r+qWuMsiGAAAIIRFeAMBrduWVkCIRO4Oabb5ZHHnnEqbt3796ydOnS0I2BghFAAAEEchMgjObmxd4IIBCgQOKlJf2ztrY2wJ5oGgEEEEDAFgHCqC0zQR0IxFygQ4cOsnnzZkfhxRdflIsvvjjmIgwfAQQQiIcAYTQe88woEbBaYNasWTJkyBCnxtatW8uOHTusrpfiEEAAAQT8EyCM+mdJSwgg4FGgYcOGh27Ls6aoR0QOQwABBEIqQBgN6cRRNgJREbjgggtk9uzZznD06uhf//rXqAyNcSCAAAIIuBAgjLpAYhcEEAhOIPHSki7hlFjSKbjeaBkBBBBAwDYBwqhtM0I9CMRIQJ8P3bVrlzPil156Sc4555wYjZ6hIoAAAgioAGGU8wABBIwI/PnPf5ZLLrnE6VvfpP/ss8+M1EGnCCCAAAJmBQijZv3pHYHYCuinPisrK53x89JSbE8DBo4AAghwZZRzAAEECi+QvJTT5ZdfLtOnTy98EfSIAAIIIGCFAFdGrZgGikAgXgJFRUVy4MABrorGa9oZLQIIIJBWgDDKiYEAAgUVmD9/vpxxxhlOn/3795dFixYVtH86QwABBBCwS4Awatd8UA0CkRdo0aKF7N27l6uikZ9pBogAAgi4EyCMunNiLwQQ8EFg+/bt0rZtW6elXr16yfLly31olSYQQAABBMIsQBgN8+xROwIhE2jXrp2Ul5c7VesC97rQPRsCCCCAQLwFCKPxnn9Gj0DBBHQZJ13OSbf27dvL5s2bC9Y3HSGAAAII2CtAGLV3bqgMgUgJnHDCCfL55587Y9q/f78cffTRkRofg0EAAQQQ8CZAGPXmxlEIIJCDQG1trTRs2NA54thjj5Vt27blcDS7IoAAAghEWYAwGuXZZWwIWCJQWloqK1ascKr54osvpE2bNpZURhkIIIAAAqYFCKOmZ4D+EYiBQIMGDZxR6q15vUXPhgACCCCAQEKAMMq5gAACgQqcdtpp8vrrrzt9/POf/5Tvfve7gfZH4wgggAAC4RIgjIZrvqgWgdAJJK6KNmnSRKqqqkJXPwUjgAACCAQrQBgN1pfWEYi1wI033iiPP/64Y1BWViaDBw+OtQeDRwABBBA4UoAwylmBAAKBCeii9nV1ddKoUSOprq4OrB8aRgABBBAIrwBhNLxzR+UIWC0wbtw4mTBhglPjxIkTZcyYMVbXS3EIIIAAAmYECKNm3OnVYoHE1Ty/Skw8MzlkyBD5y1/+4lez1rej64rq+qLqqZ/+ZEMAAQQQQCCdAGGU8wKBFIFEeAwSRvs47rjjDn2RKMi+TLR99913y9ixY52u77rrLhk/fryJMugTAQQQQCAEAoTREEwSJRZWwO8ro26qj1o41Tfn9RlRHZdeHWVDAAEEEECgPgHCKOcGAvUIJG4zZwJKXEXt06ePvPHGG1kt3QZdbbdx48ahXApp1qxZoo8k6Hb55ZfL9OnTs7qwAwIIIIBAfAUIo/Gde0aeg0Dz5s2loqLCeTPc7eYmqBYVFTlXEN20G5arjDqmAwcOOExuxuXWk/0QQAABBKIpQBiN5rwyqoAF9ApnPmGrvmDZvn172bp1q6sQp23o7fDKysqAR+u++fnz58sZZ5zhHNC/f39ZtGiR+4PZEwEEEEAglgKE0VhOO4MOQqBfv36yZMmSnEOqhsrS0lJZunTpEWXlEk71YG1L/++SSy6RGTNmBDHMjG22aNFC9u7dm7NBwQulQwQQQAABawQIo9ZMBYVEWSCXoJppKaSLLrrI+ZJRrldlC3EVdfv27dK2bVuntl69esny5cujPKWMDQEEEEDAJwHCqE+QNINArgLFxcXOLfZMz1Vmumqa6O+yyy6TF154wVNA9fMqart27aS8vNypQ9cVTTzKkKsL+yOAAAIIxEuAMBqv+Wa0Fgtke9M+cQve7QLyRx99tPMiUa4vEXm5iqqhumnTpo6uPlqwefNmi6UpDQEEEEDAJgHCqE2zQS0I/H8BXVZKQ2S2q6a5ruEZ1FXUE0444dAC/vv37xcNwmwI+CnQqFEjZ83axM9E0KtL6Dls08uBflrSFgK2CRBGbZsR6kEgReCb3/ym8/xltmCqvzx1+Skvm9erqBqa9eqr/qnbscceK9u2bfNSAscgkFYg0x2DXK/6uyXWn4eqqipn96BDr9uabNov9RGc5HnAy6aZCk8thNHwzBWVIuAIuF2MP9erpqm8Xq6innjiifLpp58yUwjkJaBXQd08jhJEGE0OonEKo27uxrid1CDmxW3f7BdOAcJoOOeNqhFwBNxeNdWF6PX2uR+b26uo+sutpqbGjy5pIyYC2Z6bTjAEefUt8bGKQvRlelpTH33wo54g58aP+mjDTgHCqJ3zQlUIeBJw88vcz18Wp512mrz++utZa9U+S0pK5JNPPsm6LzvES8DtP24SVyn1HA/qHznpfn6idpXvpJNOkk2bNmV97Cf5LEysd5x6tVp/rtu0aXNoFY14nbmM1k8BwqifmrSFgEUC+na7PveW7VlT/YXi5pZouqElriLpl6ASz9i5eYyAq6YWnSiGSnFzniQCaL6PnLgZYurt+cQxUQmjmf6hmukfqOmO8/MftG7mhn2iL0AYjf4cM0IEHAE3v/z1l8zXv/51WbFiRVa1G2+8UR5//HFnP12If/DgwUcc07FjR9m4cWPWQMxV06zckdjB7VVQE2En9fZ84mcmqKuwhZjQbAG0Q4cOaZ/x1qudO3fuTPtzyz8kCzFz8euDMBq/OWfECDhfSHr77bezhsRMV00Tv+j0ubPq6mpXqm4CMb/sXFGGYqdcwmeQt9+zYaW7KprpS2jZ2jP199meAdWf50zOmX4+jznmGNGvrLEhEIQAYTQIVdpEIGQCbt6kTb5qOm7cOJkwYYIzyokTJ8qYMWNyHjFXTXMms/4AN+eRDiLXDzgEPfAwvrSUWF7JzWMEmf6BlymAmrhCHfRc076dAoRRO+eFqhAwJuDmqmmiOD+vHnHV1NiUe+rY7VXP5Mb9PF88FZ3mINuXcvrSl77k3Ep3EzrdWmf7WbNxnvyab9qxU4Awaue8UBUC1gi4udqlV1D8XD6Kq6bWTP+hQtycB/VVbfMVttSrormGPj9nyquxjkHfkt+wYUO95WQLoDbPkZ/GtGWnAGHUznmhKgSsFXC7fJQOwK+3oLP9ItW+eNbUv1NGv/ila9i6DWaJZxG1gtSVGfQfKbZ+VjP1XHY7Xj+kc7nNnugvEZzd/lxl+7khgPoxk7ThhwBh1A9F2kAgJgIPP/yw3HLLLc5oH3roIedZ0WzLRyXTJH6Z9ujRQ9555x1Palw19cSW8aBcbrkn5nDfvn2iy4elBqXk/99taPJ/RNlbTL09H0QQTbxQpNXk2n62l43qGyEBNPvcs4d9AoRR++aEihCwVqBx48bOguP6p36TPnXzcrUnObzoeqW5XkXL9stX28/ljX9r8X0qTOdPg5gGRTcBKVsoSudv89VQZUwNol7rzed8Tw3x2W6zZ5r+bD8DXAH16YeHZgITIIwGRkvDCERLYMSIETJ16lRnUE8//bSMHj3a1QC9PgeX+svazVU2t1dNO3XqJOvWrXNVf9h30jVjE7fc3YbPbdu2Sdu2bbMOPYxvoeugkuvOFNROOeUU+fjjj12F9qxYPq8ioP/AyvSxCgKomxlhH1sECKO2zAR1IGC5QOL5uhYtWsju3bvzrrZnz56yatUqpx03ISm1Qze3/LNdMdI2o3bV9OWXX5Zzzz3XMc3m6nWJpXTPDXu9upj3iZRjA/W9sORH8Ey0HeSaqdkWsnfzj7YcydgdgcAFCKOBE9MBAuEX+P73vy8acnRbvHix9OvXL9BB5Xv7U0NB6i3/zp07y0cffZR1of8wXjV1+8xntlvu2SY13eLwYboC5+blu/oMkkNsoQNfpqugYfLPdn7x9/EVIIzGd+4ZOQKuBBLPiOrOeuu2vLzc1XFB7JTvLX+tKREqsl01tP2qqRuLdKHc67ykC3JuDL3259dxXq545hva/aq9vvCs9ekdil27dvnVFe0gYFSAMGqUn84RsF+ge/fusmbNGqdQfYO6uLjYuqLzveXvdUCpt3y1HX25S1cY8HvTNvXt9UwBMIgQZdunMrt06SLr169Py+slHAdhls/cZ7oKymL0+chyrM0ChFGbZ4faEDAssHPnTtFvUuumoXT16tWGK8q9+3xv+efeI0fYLmDjrW2ugtp+1lBfkAKE0SB1aRuBkAu0adNGduzY4YyiurraedknLlviNnhivF6uusXFyrZxJq5Y1xc6bZjLxBqk9dXCVVDbzirqCVKAMBqkLm0jEGKBhQsXysCBA50R6NvZc+bMCfFo/Cs925I62lPqG/qpwVb3sSEQ+aeSX0vpHnfQFvN5USjd4wXapokvdbl5vldr41nQ/M4jjg6vAGE0vHNH5QgEKtCyZUvZs2eP8wsyn1AQaJGGG/frDf10t2jVXddz/dGPflTQUda39FFBi8izs0xLegX5jwD9mdm7d69Tvdt+vC6vlScRhyNglQBh1KrpoBgE7BB45pln5Oqrr3aKGT58uDz//PN2FGZ5FbleNa0vhJoI/6lXEsP6j5BUU13/NPFCmZ9XRb0+i2zbC1OW/0hRXkwECKMxmWiGiUAuArpGZ+IZUf2TLXcBN1dNk1s1Gf6iEkRTr+pqENXP1iauUrq9WpmYl3y/La/tmPiHRe5nK0cgYFaAMGrWn94RsE7gnnvukTFjxjh1TZo0Sa677jrragxjQW6umuqyUBqeCrml3t5zLiQAACAASURBVNIO44szmcJ0ckDNFEazvVCUaU641V7IM5a+oihAGI3irDImBPIQSNzm1F/w+/fvz6MlDk2+wpbpO+LppDTgXHPNNfLUU08FBpnulnZlZWVg/QXRcGqYTr3CnCmMNm/eXCoqKni+M4iJoU0EchAgjOaAxa4IRF1g6NChUlZW5gxzwYIFMmDAgKgPObDx6XO3P/7xj9MGndTvuGe7aqqBqlu3br6u8xqFF5VSw3S6Z0LThdFs33fXk6JZs2bOC3xsCCAQvABhNHhjekAgNAKJX9KtW7c+tL5oaIo3XOibb74p/fv3d8JnfbeD3bxA8+Uvf9n54lV9bWi40it6u3fv9jzi5IBm8llVzwNI+qxroo3UgJ/47/UtG5Xcd1gN8vHjWARsEiCM2jQb1IKAQYE+ffrI0qVLnQqWLVsmpaWlBqsJR9dunjPM5+3pn/70p/L73/8+423kXJ4zjcKLSrmOIdOXjfTvampqwnGyUSUCERYgjEZ4chkaAm4F9Lk5vS2p2ymnnCLr1q1ze2is9tMglPx2dn2D1wCqKxL4/fxlptvLWkum50yj8KJStudD65uPhBsvGsXqx5XBhkiAMBqiyaJUBIIS0AD60UcfOc3rmowapNhEcgmfhb7KlvgoQaZAnHjONAovKrl5PpRzFgEEwilAGA3nvFE1Ar4JLF++XHr37u2017dvX3njjTd8aztsDe3cuVPatm2b8bnPxBVIvcqW6xvyQXlke840ud/6nq0MqjY/2k23fqjfV539qJM2EEDAmwBh1JsbRyEQGQF9WWnXrl2x/eynfr6xVatWWRcn10D03nvviQY/mze/nzM1OdZcnw81WSt9I4CAdwHCqHc7jkQg9AJz586VQYMGOeMYNmyYzJgxI/RjcjMAXT9V30jP9HWcoJ77dFNfvvukhrhM7RViPVMv4/H6fKiXvjgGAQTMChBGzfrTOwJGBZo2beq8ZBOHpW30s6Ya0rIF0LB/vjE1iCZ/USmX50xNnpg8H2pSn74RKLwAYbTw5vSIgBUCDz/8sNxyyy1OLQ899JDcfPPNVtTlZxEaLHXpo6gH0IRZahDNtK5ptudM/VjP1Mtc8nyoFzWOQSDcAoTRcM8f1SPgWUBDmq6xmMs6lZ47K+CBN9xwgzz55JMZ1+aM4pXg1CCay4tKbp4z1RUWdKWFoDaeDw1KlnYRsF+AMGr/HFEhAr4LjBgxQqZOneq0+/TTT8vo0aN976NQDd57770yZsyYrN8Xj2IAre+KaC5BNN08ZVvPVP/++uuvl8cee8yXaeb5UF8YaQSB0AoQRkM7dRSOgHeBRNho0aJFXp+V9F6BtyNvuummQwGovs9lJrcc5QAaVBBNnRk3z5n26NFD3nnnHU+TyvOhntg4CIFICRBGIzWdDAaB7ALf//735eWXX3Z2XLx4sfTr1y/7QQXcw823xOsrJ8xvwHshzufWvJf+evbsKatWrcp4FVqXCtuxY4er5nk+1BUTOyEQeQHCaOSnmAEi8B+BxDOi+l90cffy8nKreHINonELn8mTVeggmnqi3HjjjfLEE09kDKbJb/Jnqj0OV7Ct+kGjGAQsEyCMWjYhlINAkALdu3eXNWvWOF3s27dPiouLg+wu57ZzDaPJHbi5bZ9zQZYeYDqIpmPJ9pypzq3ezk+9skoQtfQkoywECihAGC0gNl0hYFJAP3V5zDHHOCVoKF29erXJcurt22sgjUsYtTGIpk6mnmf6Va9sc5Jp6SkrT06KQgCBQAQIo4Gw0igC9gm0adPm0LN8ugB8o0aN7Cuynoq0Vl0rNFO4yRZ8QjPYDIWGIYimlq/Pma5cuTIjf32386MwZ4wBAQSyCxBGsxuxBwKhF1i4cKEMHDjQGce5554rc+bMCf2Y4jiA5KvG+S7fVCi/1Nv3OoZM/3DQv9e1Yv1aNqpQ46QfBBDwLkAY9W7HkQiERiCxPA/P54Vmyo4oNGxBNPUqrg4oNUDr7Xx9fKS+LfGcqddlo8I721SOQLwECKPxmm9GG0OBZ555Rq6++mpn5MOHD5fnn38+hgrhHnLy1cUwPGfpZRF7N8tGabv62doHHngg3BNK9QggcJgAYZQTAoGIC+hnHBPPiOqfbOESSA52YbiynXpb3svzoG6WjdJZVJtevXrJkiVLwjWpVIsAAoRRzgEE4iJwzz33OJ/K1G3SpEly3XXXxWXokRhn6q1um1/ScnNb3uuk6D+odI3cbOPXsN67d2/CqVdojkPAkABXRg3B0y0ChRBIXKXSoLB///5CdEkfPgokPyeaLYj52G3OTaV7SUlXPwhqSxd80/Wlfvpxh23btgVVCu0igIAPAoRRHxBpAgEbBYYOHSplZWVOaQsWLJABAwbYWCY11SMQludEbfikZ7t27eSLL77IeuVUqfmHGT9yCNgnQBi1b06oCAFfBBJhJpdvhfvSMY3kLRCG50R17deDBw8eNlZbrt726dNHli1bljWcapBu3769bNq0Ke85owEEEPAuQBj1bseRCFgroL+Mly5d6tSnv5RLS0utrZXCDhcIw3OifrykVMh515+H5cuXOx9OqG/TYDp27FgZP358IUujLwQQEBHCKKcBAhETqKiokGbNmjmjOuWUU2TdunURG2G0h2Pzc6JBvqRUyFm9/fbb5aGHHqo3nOpVX1aeKOSM0FfcBQijcT8DGH/kBDSAfvTRR864qqqqRN9EZguHgM3PiXpZOzQc6iKpV3qT6z799NPlX//6V1iGQp0IhFKAMBrKaaNoBNIL6K1IXdpGt759+8obb7wBVUgEbH5ONGy35b1O+be//W35v//7v7SHe1kv1WsdHIdA3AQIo3GbccYbaQF9WWnXrl0ShsXRIz0ROQ7O1udE092Wt+UlpRyJc95dH3XRR17Sba1atcr4GdOcO+MABGIuQBiN+QnA8KMjMHfuXBk0aJAzoGHDhsmMGTOiM7iIjyT5yqMtYa/Qa4faOsXjxo2TiRMn1vtmvl7R1peefvWrX9k6BOpCwHoBwqj1U0SBCLgTaNOmjezYscN5/i11yR13LbCXCYHkq4+2fHfehrVDTcxFtj47dOggmzdvrnc3dTvzzDNl3rx52Zri7xFAIEmAMMrpgEAEBGbNmiVDhgxxRvLb3/5Wrr322giMKh5DsOnt+Si/pOT32XTWWWfJq6++mnUt0+R+TzjhBNY09XsiaC8SAoTRSEwjg4i7QMuWLWXPnj3Okk579+6NO0doxm/TVdG4vKTkx8nx8ssvy8iRI2XLli0Z1y5N15ctj2H44UAbCPglQBj1S5J2EDAkMGXKFBk1apTT+7Rp0+SKK64wVAnd5ipgw1XRqKwdmqt9uv1feOEFeeKJJ+Sdd95J29yBAwfqfalJryonNl1cP13o5MqoH7NEG1EUIIxGcVYZU6wEEm/96tVRfZOeLRwCNlwV5bb8v88VDaGjR4+W3bt3uz559B8SLVq0kNtuu835chMbAgh4FyCMerfjSASMC/z3f/+3/OxnP3PqKCsrk8GDBxuviQLcCZi+Kpr6kpItL0+50/Nnr3QhtLi4WBo3bpy2g7Zt24p+nemDDz7wpwBaQQABR4AwyomAQIgFElfX9JdkeXl5iEcSr9JNXhXVMJW62kLcnmP85JNPRL9Xv23btkMnnl7lfPbZZ+XSSy+N18nIaBGwQIAwasEkUAICXgR++ctfyn333eccql+NOe2007w0wzEGBExdFWXtUBENoqeeeqrs37/fmXlCqIEfALpEIEWAMMopgUBIBfRWYk1Njejah5999llIRxG/sk1cFeUlpX+fZzt37nR+XjSIqskf//hHroTG70eQEVsoQBi1cFIoCYFsArqO6FNPPeXstmzZMiktLc12CH9viUChr4ryktJ/Jr5Xr17y1ltvSdOmTeX999+Xjh07WnJWUAYC8RYgjMZ7/hl9SAUSz/196Utfcm47soVDoNBXRVk79PDzQpdBe/TRR2X+/PnSunXrcJw0VIlADAQIozGYZIYYLYGrrrrKub2o29q1a6VLly7RGmCER1Ooq6Lclo/wScTQEIigAGE0gpPKkKItkLjt2rlzZ/nwww+jPdgIja5QV0W5LR+hk4ahIBATAcJoTCaaYUZD4MILL5S///3vzmD09rzepmcLh0Ahroqm3paP49qh4TgbqBIBBJIFCKOcDwiEREDfnG/SpInzmcHevXvL0qVLQ1I5ZQZ9VTTdbfm4rR3KWYYAAuEVIIyGd+6oPGYCAwYMkNdee80Z9b59+0S/FMMWDoEgr4qmLmKvfem30dkQQACBsAgQRsMyU9QZa4GKigrRb9Drdvrpp8u//vWvWHuEafBBXhXlbfkwnQnUigAC9QkQRjk3EAiBQN++fWXJkiWiV70qKyud2/Vs4RAI6qpo6rfli4qKnHODDQEEEAibAGE0bDNGvbET0K/GHHPMMc64v/e978m8efNiZxDWAQdxVTT1+VBuy4f17KBuBBBICBBGORcQsFygR48e8u677zpXRQ8cOCD6jCBbOAT8virKsk3hmHeqRACB3AQIo7l5sTcCBRXQdUS7du3q9PmDH/xA/vSnPxW0fzrzLuD3VdHU50O5Le99bjgSAQTsEiCM2jUfVIPAYQKdOnWSjz/+WDSIHDx4EJ0QCfh1VZRlm0I06ZSKAAKeBAijntg4CIHgBfTWvN6i1+3qq6+W//mf/wm+U3rwRcCvq6LclvdlOmgEAQQsFyCMWj5BlBdfgZKSEvn000+dZ0Srq6vjCxHCkSffUve6+DzLNoVw4ikZAQQ8CRBGPbFxEALBCixatMhZT1S3O++8U+69995gO6R1XwUSt+i9vunOsk2+TgeNIYCA5QKEUcsniPLiKdC+fXvZsmWLNG7c2HmDni1cAl7DKF9TCtc8Uy0CCPgjQBj1x5FWEPBNYNasWTJkyBCnvQcffFBuvfVW39qmocIIJMJoLm+8c1u+MHNDLwggYJ8AYdS+OaGimAu0atVKdu/e7Xx7Xr9BzxYugeSrm26fF+W2fLjmmGoRQMBfAcKov560hkBeAtOnT3fWE9Vt8uTJMnLkyLza4+DCC+Ty8hLLNhV+fugRAQTsEyCM2jcnVBRjgRYtWsjevXulefPmsmfPnhhLhHfobtcXTb0t7/Vlp/BKUTkCCCDwbwHCKGcCApYITJkyRUaNGuVUM23aNLniiissqYwychHI9vJSagjVtnU90Zqamly6YV8EEEAgMgKE0chMJQMJu4A+I7p//37RZ0Z37twZ9uHEsv5Mz4umuyXP1dBYniYMGgEEUgQIo5wSCFgg8Jvf/EZuu+02p5KysjIZPHiwBVVRQq4C9T0vmu5qaC5v2udaB/sjgAACYRIgjIZptqg1sgJNmjRxvrKk64tu3rw5suOM+sBSnxfllnzUZ5zxIYCAHwKEUT8UaQOBPARuvPFGefzxx50Wli1bJqWlpXm0xqEmBVKXaEquhVvyJmeGvhFAwGYBwqjNs0NtsRBIPGd40kknycaNG2Mx5qgOMt2VUB0rt+SjOuOMCwEE/BAgjPqhSBsIeBQYPXq0PPvss87Rq1atkq9+9aseW+IwWwQSgVSvhOr/5i15W2aGOhBAwFYBwqitM0NdsRDQJX1qa2ulU6dOsn79+liMmUEigAACCCCQLEAY5XxAwJCAfmlJv7ik29q1a6VLly6GKqFbBBBAAAEEzAkQRs3Z03OMBfTWrb5Br98u11vzeoueDQEEEEAAgTgKEEbjOOuM2bjAmWeeKf/85z+dOnbs2CGtW7c2XhMFIIAAAgggYEKAMGpCnT5jLXDgwAHRr/HoVdF+/frJ4sWLY+3B4BFAAAEE4i1AGI33/DN6AwKnnXaavP76607PVVVVzu16NgQQQAABBOIqQBiN68wzbiMCFRUV0qxZM6fvgQMHyvz5843UQacIIIAAAgjYIkAYtWUmqCMWAr1795bly5eLrkGpt+t1wXs2BBBAAAEE4ixAGI3z7DP2ggps2LBBOnbs6PQ5ePBgKSsrK2j/dIYAAggggICNAoRRG2eFmiIpoOuIrlu3zvkqz8GDByM5RgaFAAIIIIBArgKE0VzF2B8BDwLvvvuu9OjRwznyqquukueee85DKxyCAAIIIIBA9AQIo9GbU0ZkoYDentfb9Pr5T75VbuEEURICCCCAgDEBwqgxejqOi4C+sKQvLun285//XB577LG4DJ1xIoAAAgggkFWAMJqViB0QyE+gQ4cOsnnzZufN+erq6vwa42gEEEAAAQQiJkAYjdiEMhy7BBYtWiSnn366U9Sdd94p9957r10FUg0CCCCAAAKGBQijhieA7qMtcOqpp8rq1auladOmogvesyGAAAIIIIDA4QKEUc4IBAISWLJkifTt29dpfebMmXLppZcG1BPNIoAAAgggEF4Bwmh4547KLRfo3LmzrF+/XkpKSpw36dkQQAABBBBA4EgBwihnBQIBCMyZM0fOP/98p2X9/rx+h54NAQQQQAABBAijnAMIFETgpJNOks8++0y6du0qa9asKUifdIIAAggggEAYBbgyGsZZo2arBV544QUZNmyYU+Obb74pffr0sbpeikMAAQQQQMCkAGHUpD59R1KgXbt2Ul5eLj179pS33347kmNkUAgggAACCPglQBj1S5J2EBBxvjk/cuRIx2LlypWHvkcPDgIIIIAAAgikFyCMcmYg4KNA69atZdeuXdKvXz9ZvHixjy3TFAIIIIAAAtEUIIxGc14ZlQGBe+65R8aMGeP0rEs56ZJObAgggAACCCCQWYAwyhmCgE8C+pWlyspKOeOMM+TVV1/1qVWaQQABBBBAINoChNFozy+jK5DA7bffLg8++KA0aNBAPv/8cznuuOMK1DPdIIAAAgggEG4Bwmi454/qLRCoqamRZs2ayYEDB+TCCy+UWbNmWVAVJSCAAAIIIBAOAcJoOOaJKi0WuPbaa+Wpp56So446Sr744gvRl5jYEEAAAQQQQMCdAGHUnRN7IZBWQK+GNm/eXKqrq+WKK66QadOmIYUAAggggAACOQgQRnPAYlcEUgVGjBghU6dOlYYNG0pFRYU0adIEJAQQQAABBBDIQYAwmgMWuyKQLKDhs0WLFlJbWyvXXHON/O53vwMIAQQQQAABBHIUIIzmCMbuCCQEhgwZ4ryspFdD9+3bJ40aNQIHAQQQQAABBHIUIIzmCMbuCKjA1q1bpX379lJXVye33nqrs6wTGwIIIIAAAgjkLkAYzd2MIxCQs846S1555RU5+uijZf/+/YgggAACCCCAgEcBwqhHOA6Lr8DGjRulY8eOzlXR8ePHy1133RVfDEaOAAIIIIBAngKE0TwBOTx+Av3795fFixc7C93v3bs3fgCMGAEEEEAAAR8FCKM+YtJU9AXWrl0r3bp1cwY6adIkue6666I/aEaIAAIIIIBAgAKE0QBxaTp6At/4xjfk7bfflrZt20p5eXn0BsiIEEAAAQQQKLAAYbTA4HQXXoElS5ZI3759nQHMnDlTLr300vAOhsoRQAABBBCwRIAwaslEUIb9At27d5c1a9ZIhw4d5LPPPrO/YCpEAAEEEEAgBAKE0RBMEiWaF1iwYIF897vfdQqZPXu2nHfeeeaLogIEEEAAAQQiIEAYjcAkMoTgBXQppw0bNkinTp1k/fr1wXdIDwgggAACCMREgDAak4lmmN4F5syZI+eff77TwPz582XgwIHeG+NIBBBAAAEEEDhMgDDKCYFAFgH97OeWLVvk1FNPlffeew8vBBBAAAEEEPBRgDDqIyZNRU/gueeek5EjRzoDW7lypfTo0SN6g2RECCCAAAIIGBQgjBrEp2v7BXQ90e3bt0tpaaksW7bM/oKpEAEEEEAAgZAJEEZDNmGUWziBJ598Uq6//nqnQ13SqWvXroXrnJ4QQAABBBCIiQBhNCYTzTBzF2jevLns27dPvv3tb8trr72WewMcgQACCCCAAAJZBQijWYnYIY4C//Vf/yXjx4+XBg0ayCeffCIlJSVxZGDMCCCAAAIIBC5AGA2cmA7CKNC0aVOprKyUQYMGyT/+8Y8wDoGaEUAAAQQQCIUAYTQU00SRhRT4xS9+IY899phzVVRfXmrdunUhu6cvBBBAAAEEYiVAGI3VdDPYbAI1NTVSXFws1dXVcvHFF8uLL76Y7RD+HgEEEEAAAQTyECCM5oHHodET+NGPfiSTJ0+Whg0byu7du51gyoYAAggggAACwQkQRoOzpeWQCVRUVEjLli3l4MGD8sMf/lCmTJkSshFQLgIIIIAAAuETIIyGb86oOCCByy67TGbOnCmNGzeWvXv3SpMmTQLqiWYRQAABBBBAICFAGOVcQEBEdu7cKfq1pdraWmeh+yeeeAIXBBBAAAEEECiAAGG0AMh0Yb/AeeedJy+99JJzNbSqqsr+gqkQAQQQQACBiAgQRiMykQzDu8DWrVulffv2UldXJ2PGjJGJEyd6b4wjEUAAAQQQQCAnAcJoTlzsHEWBAQMGOJ/7bNasmfOsKBsCCCCAAAIIFE6AMFo4a3qyUGDt2rXSrVs3p7IHH3xQbr31VgurpCQEEEAAAQSiK0AYje7cMjIXAn369JGlS5dKq1atnJeY2BBAAAEEEECgsAKE0cJ605tFAqtWrZKvfe1rTkW6pqiuLcqGAAIIIIAAAoUVIIwW1pveLBK47rrr5Le//a2ceOKJ8umnn1pUGaUggAACCCAQHwHCaHzmmpEmCejXltq1ayf657x58+R73/sePggggAACCCBgQIAwagCdLs0LTJo0SW644Qbp3LmzfPjhh+YLogIEEEAAAQRiKkAYjenEx33YXbp0kXXr1omGUr1dz4YAAggggAACZgQIo2bc6dWggN6WP/vss6W4uFi2bdvm/MmGAAIIIIAAAmYECKNm3OnVoMDQoUOlrKyMb9AbnAO6RgABBBBAICFAGOVciJXAxo0b5eSTT5ba2lrnWVF9ZpQNAQQQQAABBMwJEEbN2dOzAYE777xT7r//fjnrrLNk7ty5BiqgSwQQQAABBBBIFiCMcj7ERqCqqko6dOgg27dvl7/+9a8yZMiQ2IydgSKAAAIIIGCrAGHU1pmhLt8F9CtLo0aNkpKSEvn444/lqKOO8r0PGkQAAQQQQACB3AQIo7l5sXeIBUpLS2XFihVy3333yR133BHikVA6AggggAAC0REgjEZnLhlJBoHFixdL//79paioSDZt2iRt2rTBCwEEEEAAAQQsECCMWjAJlBC8wPDhw+VPf/qTjBw5UiZPnhx8h/SAAAIIIIAAAq4ECKOumNgpzAL6wtLxxx8vNTU1snz5cunVq1eYh0PtCCCAAAIIREqAMBqp6WQw6QQmTJgg48aNc27TL1q0CCQEEEAAAQQQsEiAMGrRZFCK/wK6uL0u57RlyxaZOnWqXHnllf53QosIIIAAAggg4FmAMOqZjgPDIDBz5ky57LLLnBeWNJA2atQoDGVTIwIIIIAAArERIIzGZqrjOdCBAwfKwoUL5a677pLx48fHE4FRI4AAAgggYLEAYdTiyaG0/ATee+89+epXv+osbq/LOelLTGwIIIAAAgggYJcAYdSu+aAaHwV+8pOfyO9//3sZNmyYzJgxw8eWaQoBBBBAAAEE/BIgjPolSTtWCezevVtOOOEEqaiokAULFsiAAQOsqo9iEEAAAQQQQODfAoRRzoRICjzyyCNy8803y1e+8hV59913IzlGBoUAAggggEAUBAijUZhFxnCYQF1dnXTt2lXWrVsnv/vd7+Saa65BCAEEEEAAAQQsFSCMWjoxlOVdYM6cOXL++edLy5YtZfPmzVJcXOy9MY5EAAEEEEAAgUAFCKOB8tK4CYHzzjtPXnrpJec2/UMPPWSiBPpEAAEEEEAAAZcChFGXUOwWDgG9Na+36HX75JNPpKSkJByFUyUCCCCAAAIxFSCMxnTiozpsvRqqLy/p1dHZs2dHdZiMCwEEEEAAgcgIEEYjM5UMRJdx0uWcdFknfW703HPPBQUBBBBAAAEELBcgjFo+QZTnXkAXuNeF7jt37ixr166VBg0auD+YPRFAAAEEEEDAiABh1Ag7nQYhoJ/+1E+APvzww3LTTTcF0QVtIoAAAggggIDPAoRRn0FpzozAwoULZeDAgc4yTrqcky7rxIYAAggggAAC9gsQRu2fIyp0IXDZZZfJzJkznQXudaF7NgQQQAABBBAIhwBhNBzzRJUZBLZs2SIdOnSQ2tpa59Of+glQNgQQQAABBBAIhwBhNBzzRJUZBMaNGycTJkyQAQMGyIIFC7BCAAEEEEAAgRAJEEZDNFmUeqRATU2NHH/88bJ9+3aZMWOGDBs2DCYEEEAAAQQQCJEAYTREk0WpRwpMnTpVRowY4QTSTZs2yVFHHQUTAggggAACCIRIgDAaosmi1CMF+vfvL4sXL3Zu048dOxYiBBBAAAEEEAiZAGE0ZBNGuf8RWLFihZSWlkqjRo1EX2Jq06YNPAgggAACCCAQMgHCaMgmjHL/IzBq1CiZMmWKDB8+XJ5//nloEEAAAQQQQCCEAoTREE4aJYvzwpIu51RVVSWvv/66fOtb34IFAQQQQAABBEIoQBgN4aRRssj9998vd955p/Tq1UuWL18OCQIIIIAAAgiEVIAwGtKJi3PZurj9ySefLBs3bpTJkyfLyJEj48zB2BFAAAEEEAi1AGE01NMXz+LLyspk6NChzgtLupxTUVFRPCEYNQIIIIAAAhEQIIxGYBLjNoSzzz5b5s2bJ3fccYfcd999cRs+40UAAQQQQCBSAoTRSE1n9Aezbt066dKli7O4/ccffywlJSXRHzQjRAABBBBAIMIChNEIT24Uh1ZZWSmPPvqovPfee/KHP/whikNkTAgggAACCMRKgDAaq+lmsAgggAACCCCAgF0ChFG75oNqEEAAAQQQQACBWAkQRmM13QwWAQQQQAABBBCwS4Awatd8UA0Con0+9wAAAhdJREFUCCCAAAIIIBArAcJorKabwSKAAAIIIIAAAnYJEEbtmg+qQQABBBBAAAEEYiVAGI3VdDNYBBBAAAEEEEDALgHCqF3zQTUIIIAAAggggECsBAijsZpuBosAAggggAACCNglQBi1az6oBgEEEEAAAQQQiJUAYTRW081gEUAAAQQQQAABuwQIo3bNB9UggAACCCCAAAKxEiCMxmq6GSwCCCCAAAIIIGCXAGHUrvmgGgQQQAABBBBAIFYChNFYTTeDRQABBBBAAAEE7BIgjNo1H1SDAAIIIIAAAgjESoAwGqvpZrAIIIAAAggggIBdAoRRu+aDahBAAAEEEEAAgVgJEEZjNd0MFgEEEEAAAQQQsEuAMGrXfFANAggggAACCCAQKwHCaKymm8EigAACCCCAAAJ2CRBG7ZoPqkEAAQQQQAABBGIlQBiN1XQzWAQQQAABBBBAwC4Bwqhd80E1CCCAAAIIIIBArAQIo7GabgaLAAIIIIAAAgjYJUAYtWs+qAYBBBBAAAEEEIiVAGE0VtPNYBFAAAEEEEAAAbsECKN2zQfVIIAAAggggAACsRIgjMZquhksAggggAACCCBglwBh1K75oBoEEEAAAQQQQCBWAoTRWE03g0UAAQQQQAABBOwSIIzaNR9UgwACCCCAAAIIxEqAMBqr6WawCCCAAAIIIICAXQKEUbvmg2oQQAABBBBAAIFYCfw/BbWPazcAd3sAAAAASUVORK5CYII=', 2, '2019-09-15 14:34:47', 1, '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `elementsmainteince`
--

CREATE TABLE `elementsmainteince` (
  `id` int(11) NOT NULL,
  `idProduct` int(11) NOT NULL,
  `typeProduct` varchar(45) NOT NULL,
  `name` varchar(150) NOT NULL,
  `brand` varchar(45) NOT NULL,
  `reference` varchar(45) NOT NULL,
  `presentation` varchar(45) NOT NULL,
  `amount` int(11) NOT NULL,
  `minimumAmount` int(11) NOT NULL,
  `typeCurrency` varchar(45) NOT NULL,
  `priceUnit` varchar(50) NOT NULL,
  `priceUnitIndividual` varchar(50) NOT NULL,
  `country` varchar(50) NOT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish2_ci NOT NULL,
  `percent` varchar(50) NOT NULL,
  `quantity` int(11) NOT NULL,
  `nameCompany` varchar(300) NOT NULL,
  `idQuotation` int(11) NOT NULL,
  `timeDelivery` varchar(35) NOT NULL,
  `namePlace` varchar(300) NOT NULL,
  `IVA` int(11) DEFAULT NULL,
  `FLETE` varchar(11) DEFAULT NULL,
  `nodo` text NOT NULL,
  `isFromCompany` int(4) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `elementsmainteince`
--

INSERT INTO `elementsmainteince` (`id`, `idProduct`, `typeProduct`, `name`, `brand`, `reference`, `presentation`, `amount`, `minimumAmount`, `typeCurrency`, `priceUnit`, `priceUnitIndividual`, `country`, `description`, `percent`, `quantity`, `nameCompany`, `idQuotation`, `timeDelivery`, `namePlace`, `IVA`, `FLETE`, `nodo`, `isFromCompany`) VALUES
(1, 1, 'Consumibles', 'aceite', '76', '76', '876', 7, 7, 'USD', '768000', '240,00', 'Angola', '<p>sdsfdsfsdfsdf</p>', '20', 5, 'proveedor', 1, '5 dias, salvo venta previa', 'Internacional', 1, '0', '<div class=\"box-header with-border\"><h3 class=\"box-title \" id=\"item-value-1\" data-id-producto=\"id-1\"><b>3</b> - aceite <br><p class=\"text-center\"><b>876</b></p><p></p></h3></div><form class=\"form-horizontal\"><div class=\"box-body\"><input type=\"hidden\" id=\"product-id\" value=\"0\"><input type=\"hidden\" id=\"priceUnitario-1\" value=\"768000\"><input type=\"hidden\" id=\"priceUnitarioCOP-1\" value=\"768000\"><input type=\"hidden\" id=\"precioColombiano-1\" value=\"768000\"><input type=\"hidden\" id=\"type-1\" value=\"Internacional\"><div class=\"row\"><div class=\"col-sm-12\"><div style=\"border-radius:5px;margin:5px;\" class=\" bg-null text-center\"> <input type=\"checkbox\" name=\"checkbox\" style=\"margin-top: 5px\" class=\"hidden checked\" id=\"IVA-1\" data-iva=\"1\" disabled=\"\" onclick=\"OnClickIVA(1)\" checked=\"\" data-check=\"true\" 3=\"\" <=\"\" div=\"\"></div></div><div class=\"row\" data-row-index=\"0\" id=\"content-1\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Cantidad</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-cube\"></i></span><input id=\"quantityCtx-1\" name=\"quantityCtx\" type=\"number\" min=\"0\" onchange=\"return CountquantityCtx(1)\" class=\"form-control\" value=\"1\"></div></div><input id=\"unitePriceCtx-1\" name=\"unitePriceCtx\" type=\"hidden\" class=\"form-control\" data-price=\"768000\" value=\"100\"><input id=\"precioOriginalCtx-1\" name=\"precioOriginalCtx\" type=\"hidden\" class=\"form-control\" data-price=\"240\" value=\"100\"><input id=\"precioVentaCtx-1\" name=\"precioVentaCtx\" type=\"hidden\" class=\"form-control\" data-price=\"240\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Ganancia</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"  fa fa-bookmark-o\"></i></span><input id=\"porcent-1\" name=\"porcentCtx\" type=\"number\" min=\"0\" class=\"form-control\" data=\"1\" onchange=\"ValidateChangePercents(1)\" value=\"20\"></div></div></div><br><div class=\"row\" data-row-index=\"0\" id=\"contentProductQxt\"><input type=\"hidden\" name=\"precioUnitarioConGanancia\" id=\"precioUnitarioConGanancia-1\" value=\"768000153600\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Tiempo Entrega</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-history\"></i></span><select name=\"timeDelivery\" id=\"timeDelivery-1\" data=\"1\" class=\"form-control\" style=\"border-color: rgb(210, 214, 222);\"><option value=\"5 dias, salvo venta previa\">5 dias, salvo venta previa</option></select></div></div><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Precio</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-dollar\"></i></span><input id=\"totalPriceCtx-1\" valor-modificable=\"$4,608,000\" undefined]=\"\" name=\"totalPriceCtx-1\" data-type=\"COP\" data-value=\"4608000\" data-place=\"Internacional\" data-money=\"USD\" data-typeproduct=\"Consumibles\" data-id=\"1\" data-proveedor=\"proveedor\" data-priceu=\"[object Undefined]\" type=\"text\" class=\"form-control\" disabled=\"\" lol=\"\" value=\"$4,608,000\"></div></div></div></div><div class=\"box-footer\"><button type=\"button\" onclick=\"DeleteProduct(1)\" class=\"btn btn-danger btn-xs-block\">Cancelar</button></div></div></form>', 1),
(2, 2, 'Servicio', 'sdfds3', ' - ', ' - ', '233', 0, 0, 'COP', '233', '233', ' - ', '<p>dfsdffsdfsd</p>', '20', 5, ' - ', 1, ' - ', ' - ', 1, '0', '<div class=\"box-header with-border\"><h3 class=\"box-title \" id=\"item-value-900000002\" data-id-producto=\"id-900000002\"><b>2</b> - Sdfds3 <br><p class=\"text-center\"><b>233 minutos</b></p><p></p></h3></div><form class=\"form-horizontal\"><div class=\"box-body\"><input type=\"hidden\" id=\"product-id\" value=\"undefined\"><input type=\"hidden\" id=\"priceUnitario-900000002\" value=\"3827\"><input type=\"hidden\" id=\"priceUnitarioCOP-900000002\" value=\"3827\"><input type=\"hidden\" id=\"precioColombiano-900000002\" value=\"3827\"><input type=\"hidden\" id=\"type-900000002\" value=\"Nacional\"><div class=\"row\"><div class=\"col-sm-12\"><div style=\"border-radius:5px;margin:5px;\" class=\" bg-null text-center\"> <input type=\"checkbox\" name=\"checkbox\" style=\"margin-top: 5px\" class=\"hidden checked\" id=\"IVA-900000002\" data-iva=\"900000002\" disabled=\"\" onclick=\"OnClickIVA(900000002)\" checked=\"\" data-check=\"true\" 2=\"\" <=\"\" div=\"\"></div></div><div class=\"row\" data-row-index=\"undefined\" id=\"content-900000002\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Cantidad</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-cube\"></i></span><input id=\"quantityCtx-900000002\" name=\"quantityCtx\" type=\"number\" min=\"0\" onchange=\"return CountquantityCtx(900000002)\" class=\"form-control\" value=\"1\"></div></div><input id=\"unitePriceCtx-900000002\" name=\"unitePriceCtx\" type=\"hidden\" class=\"form-control\" data-price=\"3827\" value=\"100\"><input id=\"precioOriginalCtx-900000002\" name=\"precioOriginalCtx\" type=\"hidden\" class=\"form-control\" data-price=\"3827\" value=\"100\"><input id=\"precioVentaCtx-900000002\" name=\"precioVentaCtx\" type=\"hidden\" class=\"form-control\" data-price=\"3827\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Ganancia</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"  fa fa-bookmark-o\"></i></span><input id=\"porcent-900000002\" name=\"porcentCtx\" type=\"number\" min=\"0\" class=\"form-control\" data=\"900000002\" onchange=\"ValidateChangePercents(900000002)\" value=\"20\"></div></div></div><br><div class=\"row\" data-row-index=\"undefined\" id=\"contentProductQxt\"><input type=\"hidden\" name=\"precioUnitarioConGanancia\" id=\"precioUnitarioConGanancia-900000002\" value=\"3827765.4000000000001\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Tiempo Entrega</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-history\"></i></span><select name=\"timeDelivery\" id=\"timeDelivery-900000002\" data=\"900000002\" class=\"form-control\" style=\"border-color: red; border-style: solid;\"><option value=\"--Tiempo de Entrega--\"> --Tiempo de Entrega-- </option><option value=\"5\">5 dÃ­as</option><option value=\"15\">15 dÃ­as</option><option value=\"30\">30 dÃ­as</option><option value=\"45\">45 dÃ­as</option><option value=\"60\">60 dÃ­as</option><option value=\"90\">90 dÃ­as</option><option value=\"120\">120 dÃ­as</option><option value=\"150\">150 dÃ­as</option><option value=\"180\">180 dÃ­as</option></select></div></div><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Precio</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-dollar\"></i></span><input id=\"totalPriceCtx-900000002\" valor-modificable=\"$22,962\" undefined]=\"\" name=\"totalPriceCtx-900000002\" data-type=\"COP\" data-value=\"22962\" data-place=\"Nacional\" data-money=\"COP\" data-typeproduct=\"Servicio\" data-id=\"900000002\" data-proveedor=\"externo\" data-priceu=\"[object Undefined]\" type=\"text\" class=\"form-control\" disabled=\"\" lol=\"\" value=\"$22,962\"></div></div></div></div><div class=\"box-footer\"><button type=\"button\" onclick=\"DeleteProduct(900000002)\" class=\"btn btn-danger btn-xs-block\">Cancelar</button></div></div></form>', 1),
(3, 1, 'Servicio', 'aplicar aceite a maquinas', ' - ', ' - ', '180', 0, 0, 'COP', '50000', '50000', ' - ', '<p>aplicar aceite a la maquina</p>', '20', 5, ' - ', 1, ' - ', ' - ', 1, '0', '<div class=\"box-header with-border\"><h3 class=\"box-title \" id=\"item-value-900000001\" data-id-producto=\"id-900000001\"><b>1</b> - Aplicar Aceite A Maquinas <br><p class=\"text-center\"><b>180 minutos</b></p><p></p></h3></div><form class=\"form-horizontal\"><div class=\"box-body\"><input type=\"hidden\" id=\"product-id\" value=\"undefined\"><input type=\"hidden\" id=\"priceUnitario-900000001\" value=\"173250\"><input type=\"hidden\" id=\"priceUnitarioCOP-900000001\" value=\"173250\"><input type=\"hidden\" id=\"precioColombiano-900000001\" value=\"173250\"><input type=\"hidden\" id=\"type-900000001\" value=\"Nacional\"><div class=\"row\"><div class=\"col-sm-12\"><div style=\"border-radius:5px;margin:5px;\" class=\" bg-null text-center\"> <input type=\"checkbox\" name=\"checkbox\" style=\"margin-top: 5px\" class=\"hidden checked\" id=\"IVA-900000001\" data-iva=\"900000001\" disabled=\"\" onclick=\"OnClickIVA(900000001)\" checked=\"\" data-check=\"true\" 1=\"\" <=\"\" div=\"\"></div></div><div class=\"row\" data-row-index=\"undefined\" id=\"content-900000001\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Cantidad</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-cube\"></i></span><input id=\"quantityCtx-900000001\" name=\"quantityCtx\" type=\"number\" min=\"0\" onchange=\"return CountquantityCtx(900000001)\" class=\"form-control\" value=\"1\"></div></div><input id=\"unitePriceCtx-900000001\" name=\"unitePriceCtx\" type=\"hidden\" class=\"form-control\" data-price=\"173250\" value=\"100\"><input id=\"precioOriginalCtx-900000001\" name=\"precioOriginalCtx\" type=\"hidden\" class=\"form-control\" data-price=\"173250\" value=\"100\"><input id=\"precioVentaCtx-900000001\" name=\"precioVentaCtx\" type=\"hidden\" class=\"form-control\" data-price=\"173250\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Ganancia</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"  fa fa-bookmark-o\"></i></span><input id=\"porcent-900000001\" name=\"porcentCtx\" type=\"number\" min=\"0\" class=\"form-control\" data=\"900000001\" onchange=\"ValidateChangePercents(900000001)\" value=\"20\"></div></div></div><br><div class=\"row\" data-row-index=\"undefined\" id=\"contentProductQxt\"><input type=\"hidden\" name=\"precioUnitarioConGanancia\" id=\"precioUnitarioConGanancia-900000001\" value=\"17325034650\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Tiempo Entrega</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-history\"></i></span><select name=\"timeDelivery\" id=\"timeDelivery-900000001\" data=\"900000001\" class=\"form-control\" style=\"border-color: rgb(210, 214, 222);\"><option value=\"--Tiempo de Entrega--\"> --Tiempo de Entrega-- </option><option value=\"5\">5 dÃ­as</option><option value=\"15\">15 dÃ­as</option><option value=\"30\">30 dÃ­as</option><option value=\"45\">45 dÃ­as</option><option value=\"60\">60 dÃ­as</option><option value=\"90\">90 dÃ­as</option><option value=\"120\">120 dÃ­as</option><option value=\"150\">150 dÃ­as</option><option value=\"180\">180 dÃ­as</option></select></div></div><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Precio</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-dollar\"></i></span><input id=\"totalPriceCtx-900000001\" valor-modificable=\"$1,039,500\" undefined]=\"\" name=\"totalPriceCtx-900000001\" data-type=\"COP\" data-value=\"1039500\" data-place=\"Nacional\" data-money=\"COP\" data-typeproduct=\"Servicio\" data-id=\"900000001\" data-proveedor=\"externo\" data-priceu=\"[object Undefined]\" type=\"text\" class=\"form-control\" disabled=\"\" lol=\"\" value=\"$1,039,500\"></div></div></div></div><div class=\"box-footer\"><button type=\"button\" onclick=\"DeleteProduct(900000001)\" class=\"btn btn-danger btn-xs-block\">Cancelar</button></div></div></form>', 1),
(4, 1, 'Consumibles', 'aceite', '76', '76', '876', 7, 7, 'USD', '768000', '240,00', 'Angola', '<p>sdsfdsfsdfsdf</p>', '20', 5, 'proveedor', 2, '5 dias, salvo venta previa', 'Internacional', 1, '0', '<div class=\"box-header with-border\"><h3 class=\"box-title \" id=\"item-value-1\" data-id-producto=\"id-1\"><b>3</b> - aceite <br><p class=\"text-center\"><b>876</b></p><p></p></h3></div><form class=\"form-horizontal\"><div class=\"box-body\"><input type=\"hidden\" id=\"product-id\" value=\"0\"><input type=\"hidden\" id=\"priceUnitario-1\" value=\"768000\"><input type=\"hidden\" id=\"priceUnitarioCOP-1\" value=\"768000\"><input type=\"hidden\" id=\"precioColombiano-1\" value=\"768000\"><input type=\"hidden\" id=\"type-1\" value=\"Internacional\"><div class=\"row\"><div class=\"col-sm-12\"><div style=\"border-radius:5px;margin:5px;\" class=\" bg-null text-center\"> <input type=\"checkbox\" name=\"checkbox\" style=\"margin-top: 5px\" class=\"hidden checked\" id=\"IVA-1\" data-iva=\"1\" disabled=\"\" onclick=\"OnClickIVA(1)\" checked=\"\" data-check=\"true\" 3=\"\" <=\"\" div=\"\"></div></div><div class=\"row\" data-row-index=\"0\" id=\"content-1\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Cantidad</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-cube\"></i></span><input id=\"quantityCtx-1\" name=\"quantityCtx\" type=\"number\" min=\"0\" onchange=\"return CountquantityCtx(1)\" class=\"form-control\" value=\"1\"></div></div><input id=\"unitePriceCtx-1\" name=\"unitePriceCtx\" type=\"hidden\" class=\"form-control\" data-price=\"768000\" value=\"100\"><input id=\"precioOriginalCtx-1\" name=\"precioOriginalCtx\" type=\"hidden\" class=\"form-control\" data-price=\"240\" value=\"100\"><input id=\"precioVentaCtx-1\" name=\"precioVentaCtx\" type=\"hidden\" class=\"form-control\" data-price=\"240\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Ganancia</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"  fa fa-bookmark-o\"></i></span><input id=\"porcent-1\" name=\"porcentCtx\" type=\"number\" min=\"0\" class=\"form-control\" data=\"1\" onchange=\"ValidateChangePercents(1)\" value=\"20\"></div></div></div><br><div class=\"row\" data-row-index=\"0\" id=\"contentProductQxt\"><input type=\"hidden\" name=\"precioUnitarioConGanancia\" id=\"precioUnitarioConGanancia-1\" value=\"768000153600\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Tiempo Entrega</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-history\"></i></span><select name=\"timeDelivery\" id=\"timeDelivery-1\" data=\"1\" class=\"form-control\" style=\"border-color: rgb(210, 214, 222);\"><option value=\"5 dias, salvo venta previa\">5 dias, salvo venta previa</option></select></div></div><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Precio</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-dollar\"></i></span><input id=\"totalPriceCtx-1\" valor-modificable=\"$4,608,000\" undefined]=\"\" name=\"totalPriceCtx-1\" data-type=\"COP\" data-value=\"4608000\" data-place=\"Internacional\" data-money=\"USD\" data-typeproduct=\"Consumibles\" data-id=\"1\" data-proveedor=\"proveedor\" data-priceu=\"[object Undefined]\" type=\"text\" class=\"form-control\" disabled=\"\" lol=\"\" value=\"$4,608,000\"></div></div></div></div><div class=\"box-footer\"><button type=\"button\" onclick=\"DeleteProduct(1)\" class=\"btn btn-danger btn-xs-block\">Cancelar</button></div></div></form>', 1),
(5, 2, 'Servicio', 'sdfds3', ' - ', ' - ', '233', 0, 0, 'COP', '3827', '233', ' - ', '<p>dfsdffsdfsd</p>', '20', 5, ' - ', 2, ' - ', ' - ', 1, '0', '<div class=\"box-header with-border\"><h3 class=\"box-title \" id=\"item-value-900000002\" data-id-producto=\"id-900000002\"><b>2</b> - Sdfds3 <br><p class=\"text-center\"><b>233 minutos</b></p><p></p></h3></div><form class=\"form-horizontal\"><div class=\"box-body\"><input type=\"hidden\" id=\"product-id\" value=\"undefined\"><input type=\"hidden\" id=\"priceUnitario-900000002\" value=\"3827\"><input type=\"hidden\" id=\"priceUnitarioCOP-900000002\" value=\"3827\"><input type=\"hidden\" id=\"precioColombiano-900000002\" value=\"3827\"><input type=\"hidden\" id=\"type-900000002\" value=\"Nacional\"><div class=\"row\"><div class=\"col-sm-12\"><div style=\"border-radius:5px;margin:5px;\" class=\" bg-null text-center\"> <input type=\"checkbox\" name=\"checkbox\" style=\"margin-top: 5px\" class=\"hidden checked\" id=\"IVA-900000002\" data-iva=\"900000002\" disabled=\"\" onclick=\"OnClickIVA(900000002)\" checked=\"\" data-check=\"true\" 2=\"\" <=\"\" div=\"\"></div></div><div class=\"row\" data-row-index=\"undefined\" id=\"content-900000002\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Cantidad</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-cube\"></i></span><input id=\"quantityCtx-900000002\" name=\"quantityCtx\" type=\"number\" min=\"0\" onchange=\"return CountquantityCtx(900000002)\" class=\"form-control\" value=\"1\"></div></div><input id=\"unitePriceCtx-900000002\" name=\"unitePriceCtx\" type=\"hidden\" class=\"form-control\" data-price=\"3827\" value=\"100\"><input id=\"precioOriginalCtx-900000002\" name=\"precioOriginalCtx\" type=\"hidden\" class=\"form-control\" data-price=\"3827\" value=\"100\"><input id=\"precioVentaCtx-900000002\" name=\"precioVentaCtx\" type=\"hidden\" class=\"form-control\" data-price=\"3827\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Ganancia</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"  fa fa-bookmark-o\"></i></span><input id=\"porcent-900000002\" name=\"porcentCtx\" type=\"number\" min=\"0\" class=\"form-control\" data=\"900000002\" onchange=\"ValidateChangePercents(900000002)\" value=\"20\"></div></div></div><br><div class=\"row\" data-row-index=\"undefined\" id=\"contentProductQxt\"><input type=\"hidden\" name=\"precioUnitarioConGanancia\" id=\"precioUnitarioConGanancia-900000002\" value=\"3827765.4000000000001\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Tiempo Entrega</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-history\"></i></span><select name=\"timeDelivery\" id=\"timeDelivery-900000002\" data=\"900000002\" class=\"form-control\" style=\"border-color: rgb(210, 214, 222); border-style: solid;\"><option value=\"--Tiempo de Entrega--\"> --Tiempo de Entrega-- </option><option value=\"5\">5 dÃ­as</option><option value=\"15\">15 dÃ­as</option><option value=\"30\">30 dÃ­as</option><option value=\"45\">45 dÃ­as</option><option value=\"60\">60 dÃ­as</option><option value=\"90\">90 dÃ­as</option><option value=\"120\">120 dÃ­as</option><option value=\"150\">150 dÃ­as</option><option value=\"180\">180 dÃ­as</option></select></div></div><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Precio</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-dollar\"></i></span><input id=\"totalPriceCtx-900000002\" valor-modificable=\"$22,962\" undefined]=\"\" name=\"totalPriceCtx-900000002\" data-type=\"COP\" data-value=\"22962\" data-place=\"Nacional\" data-money=\"COP\" data-typeproduct=\"Servicio\" data-id=\"900000002\" data-proveedor=\"externo\" data-priceu=\"[object Undefined]\" type=\"text\" class=\"form-control\" disabled=\"\" lol=\"\" value=\"$22,962\"></div></div></div></div><div class=\"box-footer\"><button type=\"button\" onclick=\"DeleteProduct(900000002)\" class=\"btn btn-danger btn-xs-block\">Cancelar</button></div></div></form>', 1),
(6, 1, 'Servicio', 'aplicar aceite a maquinas', ' - ', ' - ', '180', 0, 0, 'COP', '173250', '50000', ' - ', '<p>aplicar aceite a la maquina</p>', '20', 5, ' - ', 2, ' - ', ' - ', 1, '0', '<div class=\"box-header with-border\"><h3 class=\"box-title \" id=\"item-value-900000001\" data-id-producto=\"id-900000001\"><b>1</b> - Aplicar Aceite A Maquinas <br><p class=\"text-center\"><b>180 minutos</b></p><p></p></h3></div><form class=\"form-horizontal\"><div class=\"box-body\"><input type=\"hidden\" id=\"product-id\" value=\"undefined\"><input type=\"hidden\" id=\"priceUnitario-900000001\" value=\"173250\"><input type=\"hidden\" id=\"priceUnitarioCOP-900000001\" value=\"173250\"><input type=\"hidden\" id=\"precioColombiano-900000001\" value=\"173250\"><input type=\"hidden\" id=\"type-900000001\" value=\"Nacional\"><div class=\"row\"><div class=\"col-sm-12\"><div style=\"border-radius:5px;margin:5px;\" class=\" bg-null text-center\"> <input type=\"checkbox\" name=\"checkbox\" style=\"margin-top: 5px\" class=\"hidden checked\" id=\"IVA-900000001\" data-iva=\"900000001\" disabled=\"\" onclick=\"OnClickIVA(900000001)\" checked=\"\" data-check=\"true\" 1=\"\" <=\"\" div=\"\"></div></div><div class=\"row\" data-row-index=\"undefined\" id=\"content-900000001\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Cantidad</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-cube\"></i></span><input id=\"quantityCtx-900000001\" name=\"quantityCtx\" type=\"number\" min=\"0\" onchange=\"return CountquantityCtx(900000001)\" class=\"form-control\" value=\"1\"></div></div><input id=\"unitePriceCtx-900000001\" name=\"unitePriceCtx\" type=\"hidden\" class=\"form-control\" data-price=\"173250\" value=\"100\"><input id=\"precioOriginalCtx-900000001\" name=\"precioOriginalCtx\" type=\"hidden\" class=\"form-control\" data-price=\"173250\" value=\"100\"><input id=\"precioVentaCtx-900000001\" name=\"precioVentaCtx\" type=\"hidden\" class=\"form-control\" data-price=\"173250\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Ganancia</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"  fa fa-bookmark-o\"></i></span><input id=\"porcent-900000001\" name=\"porcentCtx\" type=\"number\" min=\"0\" class=\"form-control\" data=\"900000001\" onchange=\"ValidateChangePercents(900000001)\" value=\"20\"></div></div></div><br><div class=\"row\" data-row-index=\"undefined\" id=\"contentProductQxt\"><input type=\"hidden\" name=\"precioUnitarioConGanancia\" id=\"precioUnitarioConGanancia-900000001\" value=\"17325034650\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Tiempo Entrega</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-history\"></i></span><select name=\"timeDelivery\" id=\"timeDelivery-900000001\" data=\"900000001\" class=\"form-control\" style=\"border-color: rgb(210, 214, 222);\"><option value=\"--Tiempo de Entrega--\"> --Tiempo de Entrega-- </option><option value=\"5\">5 dÃ­as</option><option value=\"15\">15 dÃ­as</option><option value=\"30\">30 dÃ­as</option><option value=\"45\">45 dÃ­as</option><option value=\"60\">60 dÃ­as</option><option value=\"90\">90 dÃ­as</option><option value=\"120\">120 dÃ­as</option><option value=\"150\">150 dÃ­as</option><option value=\"180\">180 dÃ­as</option></select></div></div><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Precio</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-dollar\"></i></span><input id=\"totalPriceCtx-900000001\" valor-modificable=\"$1,039,500\" undefined]=\"\" name=\"totalPriceCtx-900000001\" data-type=\"COP\" data-value=\"1039500\" data-place=\"Nacional\" data-money=\"COP\" data-typeproduct=\"Servicio\" data-id=\"900000001\" data-proveedor=\"externo\" data-priceu=\"[object Undefined]\" type=\"text\" class=\"form-control\" disabled=\"\" lol=\"\" value=\"$1,039,500\"></div></div></div></div><div class=\"box-footer\"><button type=\"button\" onclick=\"DeleteProduct(900000001)\" class=\"btn btn-danger btn-xs-block\">Cancelar</button></div></div></form>', 1),
(7, 1, 'Consumibles', 'aceite', '76', '76', '876', 7, 7, 'USD', '768000', '240,00', 'Angola', '<p>sdsfdsfsdfsdf</p>', '20', 1, 'proveedor', 1, '5 dias, salvo venta previa', 'Internacional', 1, '0', '<div class=\"box-header with-border\"><h3 class=\"box-title \" id=\"item-value-1\" data-id-producto=\"id-1\"><b>2</b> - aceite <br><p class=\"text-center\"><b>876</b></p><p></p></h3></div><form class=\"form-horizontal\"><div class=\"box-body\"><input type=\"hidden\" id=\"product-id\" value=\"0\"><input type=\"hidden\" id=\"priceUnitario-1\" value=\"768000\"><input type=\"hidden\" id=\"priceUnitarioCOP-1\" value=\"768000\"><input type=\"hidden\" id=\"precioColombiano-1\" value=\"768000\"><input type=\"hidden\" id=\"type-1\" value=\"Internacional\"><div class=\"row\"><div class=\"col-sm-12\"><div style=\"border-radius:5px;margin:5px;\" class=\" bg-null text-center\"> <input type=\"checkbox\" name=\"checkbox\" style=\"margin-top: 5px\" class=\"hidden checked\" id=\"IVA-1\" data-iva=\"1\" disabled=\"\" onclick=\"OnClickIVA(1)\" checked=\"\" data-check=\"true\" 2=\"\" <=\"\" div=\"\"></div></div><div class=\"row\" data-row-index=\"0\" id=\"content-1\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Cantidad</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-cube\"></i></span><input id=\"quantityCtx-1\" name=\"quantityCtx\" type=\"number\" min=\"0\" onchange=\"return CountquantityCtx(1)\" class=\"form-control\" value=\"1\"></div></div><input id=\"unitePriceCtx-1\" name=\"unitePriceCtx\" type=\"hidden\" class=\"form-control\" data-price=\"768000\" value=\"100\"><input id=\"precioOriginalCtx-1\" name=\"precioOriginalCtx\" type=\"hidden\" class=\"form-control\" data-price=\"240\" value=\"100\"><input id=\"precioVentaCtx-1\" name=\"precioVentaCtx\" type=\"hidden\" class=\"form-control\" data-price=\"240\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Ganancia</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"  fa fa-bookmark-o\"></i></span><input id=\"porcent-1\" name=\"porcentCtx\" type=\"number\" min=\"0\" class=\"form-control\" data=\"1\" onchange=\"ValidateChangePercents(1)\" value=\"20\"></div></div></div><br><div class=\"row\" data-row-index=\"0\" id=\"contentProductQxt\"><input type=\"hidden\" name=\"precioUnitarioConGanancia\" id=\"precioUnitarioConGanancia-1\" value=\"921600\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Tiempo Entrega</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-history\"></i></span><select name=\"timeDelivery\" id=\"timeDelivery-1\" data=\"1\" class=\"form-control\" style=\"border-color: rgb(210, 214, 222);\"><option value=\"5 dias, salvo venta previa\">5 dias, salvo venta previa</option></select></div></div><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Precio</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-dollar\"></i></span><input id=\"totalPriceCtx-1\" valor-modificable=\"$921,600\" undefined]=\"\" name=\"totalPriceCtx-1\" data-type=\"COP\" data-value=\"921600\" data-place=\"Internacional\" data-money=\"USD\" data-typeproduct=\"Consumibles\" data-id=\"1\" data-proveedor=\"proveedor\" data-priceu=\"[object Undefined]\" type=\"text\" class=\"form-control\" disabled=\"\" lol=\"\" value=\"$921,600\"></div></div></div></div><div class=\"box-footer\"><button type=\"button\" onclick=\"DeleteProduct(1)\" class=\"btn btn-danger btn-xs-block\">Cancelar</button></div></div></form>', 1),
(8, 1, 'Servicio', 'aplicar aceite a maquinas', ' - ', ' - ', '180', 0, 0, 'COP', '173250', '50000', ' - ', '<p>aplicar aceite a la maquina</p>', '20', 1, ' - ', 1, ' - ', ' - ', 1, '0', '<div class=\"box-header with-border\"><h3 class=\"box-title \" id=\"item-value-900000001\" data-id-producto=\"id-900000001\"><b>1</b> - Aplicar Aceite A Maquinas <br><p class=\"text-center\"><b>180 minutos</b></p><p></p></h3></div><form class=\"form-horizontal\"><div class=\"box-body\"><input type=\"hidden\" id=\"product-id\" value=\"undefined\"><input type=\"hidden\" id=\"priceUnitario-900000001\" value=\"173250\"><input type=\"hidden\" id=\"priceUnitarioCOP-900000001\" value=\"173250\"><input type=\"hidden\" id=\"precioColombiano-900000001\" value=\"173250\"><input type=\"hidden\" id=\"type-900000001\" value=\"Nacional\"><div class=\"row\"><div class=\"col-sm-12\"><div style=\"border-radius:5px;margin:5px;\" class=\" bg-null text-center\"> <input type=\"checkbox\" name=\"checkbox\" style=\"margin-top: 5px\" class=\"hidden checked\" id=\"IVA-900000001\" data-iva=\"900000001\" disabled=\"\" onclick=\"OnClickIVA(900000001)\" checked=\"\" data-check=\"true\" 1=\"\" <=\"\" div=\"\"></div></div><div class=\"row\" data-row-index=\"undefined\" id=\"content-900000001\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Cantidad</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-cube\"></i></span><input id=\"quantityCtx-900000001\" name=\"quantityCtx\" type=\"number\" min=\"0\" onchange=\"return CountquantityCtx(900000001)\" class=\"form-control\" value=\"1\"></div></div><input id=\"unitePriceCtx-900000001\" name=\"unitePriceCtx\" type=\"hidden\" class=\"form-control\" data-price=\"173250\" value=\"100\"><input id=\"precioOriginalCtx-900000001\" name=\"precioOriginalCtx\" type=\"hidden\" class=\"form-control\" data-price=\"173250\" value=\"100\"><input id=\"precioVentaCtx-900000001\" name=\"precioVentaCtx\" type=\"hidden\" class=\"form-control\" data-price=\"173250\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Ganancia</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"  fa fa-bookmark-o\"></i></span><input id=\"porcent-900000001\" name=\"porcentCtx\" type=\"number\" min=\"0\" class=\"form-control\" data=\"900000001\" onchange=\"ValidateChangePercents(900000001)\" value=\"20\"></div></div></div><br><div class=\"row\" data-row-index=\"undefined\" id=\"contentProductQxt\"><input type=\"hidden\" name=\"precioUnitarioConGanancia\" id=\"precioUnitarioConGanancia-900000001\" value=\"207900\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Tiempo Entrega</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-history\"></i></span><select name=\"timeDelivery\" id=\"timeDelivery-900000001\" data=\"900000001\" class=\"form-control\" style=\"border-color: red; border-style: solid;\"><option value=\"--Tiempo de Entrega--\"> --Tiempo de Entrega-- </option><option value=\"5\">5 dÃ­as</option><option value=\"15\">15 dÃ­as</option><option value=\"30\">30 dÃ­as</option><option value=\"45\">45 dÃ­as</option><option value=\"60\">60 dÃ­as</option><option value=\"90\">90 dÃ­as</option><option value=\"120\">120 dÃ­as</option><option value=\"150\">150 dÃ­as</option><option value=\"180\">180 dÃ­as</option></select></div></div><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Precio</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-dollar\"></i></span><input id=\"totalPriceCtx-900000001\" valor-modificable=\"$207,900\" undefined]=\"\" name=\"totalPriceCtx-900000001\" data-type=\"COP\" data-value=\"207900\" data-place=\"Nacional\" data-money=\"COP\" data-typeproduct=\"Servicio\" data-id=\"900000001\" data-proveedor=\"externo\" data-priceu=\"[object Undefined]\" type=\"text\" class=\"form-control\" disabled=\"\" lol=\"\" value=\"$207,900\"></div></div></div></div><div class=\"box-footer\"><button type=\"button\" onclick=\"DeleteProduct(900000001)\" class=\"btn btn-danger btn-xs-block\">Cancelar</button></div></div></form>', 1),
(9, 1, 'Consumibles', 'aceite', '76', '76', '876', 7, 7, 'USD', '768000', '240,00', 'Angola', '<p>sdsfdsfsdfsdf</p>', '20', 1, 'proveedor', 1, '5 dias, salvo venta previa', 'Internacional', 1, '0', '<div class=\"box-header with-border\"><h3 class=\"box-title \" id=\"item-value-1\" data-id-producto=\"id-1\"><b>2</b> - aceite <br><p class=\"text-center\"><b>876</b></p><p></p></h3></div><form class=\"form-horizontal\"><div class=\"box-body\"><input type=\"hidden\" id=\"product-id\" value=\"0\"><input type=\"hidden\" id=\"priceUnitario-1\" value=\"768000\"><input type=\"hidden\" id=\"priceUnitarioCOP-1\" value=\"768000\"><input type=\"hidden\" id=\"precioColombiano-1\" value=\"768000\"><input type=\"hidden\" id=\"type-1\" value=\"Internacional\"><div class=\"row\"><div class=\"col-sm-12\"><div style=\"border-radius:5px;margin:5px;\" class=\" bg-null text-center\"> <input type=\"checkbox\" name=\"checkbox\" style=\"margin-top: 5px\" class=\"hidden checked\" id=\"IVA-1\" data-iva=\"1\" disabled=\"\" onclick=\"OnClickIVA(1)\" checked=\"\" data-check=\"true\" 2=\"\" <=\"\" div=\"\"></div></div><div class=\"row\" data-row-index=\"0\" id=\"content-1\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Cantidad</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-cube\"></i></span><input id=\"quantityCtx-1\" name=\"quantityCtx\" type=\"number\" min=\"0\" onchange=\"return CountquantityCtx(1)\" class=\"form-control\" value=\"1\"></div></div><input id=\"unitePriceCtx-1\" name=\"unitePriceCtx\" type=\"hidden\" class=\"form-control\" data-price=\"768000\" value=\"100\"><input id=\"precioOriginalCtx-1\" name=\"precioOriginalCtx\" type=\"hidden\" class=\"form-control\" data-price=\"240\" value=\"100\"><input id=\"precioVentaCtx-1\" name=\"precioVentaCtx\" type=\"hidden\" class=\"form-control\" data-price=\"240\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Ganancia</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"  fa fa-bookmark-o\"></i></span><input id=\"porcent-1\" name=\"porcentCtx\" type=\"number\" min=\"0\" class=\"form-control\" data=\"1\" onchange=\"ValidateChangePercents(1)\" value=\"20\"></div></div></div><br><div class=\"row\" data-row-index=\"0\" id=\"contentProductQxt\"><input type=\"hidden\" name=\"precioUnitarioConGanancia\" id=\"precioUnitarioConGanancia-1\" value=\"921600\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Tiempo Entrega</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-history\"></i></span><select name=\"timeDelivery\" id=\"timeDelivery-1\" data=\"1\" class=\"form-control\" style=\"border-color: rgb(210, 214, 222);\"><option value=\"5 dias, salvo venta previa\">5 dias, salvo venta previa</option></select></div></div><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Precio</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-dollar\"></i></span><input id=\"totalPriceCtx-1\" valor-modificable=\"$921,600\" undefined]=\"\" name=\"totalPriceCtx-1\" data-type=\"COP\" data-value=\"921600\" data-place=\"Internacional\" data-money=\"USD\" data-typeproduct=\"Consumibles\" data-id=\"1\" data-proveedor=\"proveedor\" data-priceu=\"[object Undefined]\" type=\"text\" class=\"form-control\" disabled=\"\" lol=\"\" value=\"$921,600\"></div></div></div></div><div class=\"box-footer\"><button type=\"button\" onclick=\"DeleteProduct(1)\" class=\"btn btn-danger btn-xs-block\">Cancelar</button></div></div></form>', 1),
(10, 1, 'Servicio', 'aplicar aceite a maquinas', ' - ', ' - ', '180', 0, 0, 'COP', '173250', '50000', ' - ', '<p>aplicar aceite a la maquina</p>', '20', 1, ' - ', 1, ' - ', ' - ', 1, '0', '<div class=\"box-header with-border\"><h3 class=\"box-title \" id=\"item-value-900000001\" data-id-producto=\"id-900000001\"><b>1</b> - Aplicar Aceite A Maquinas <br><p class=\"text-center\"><b>180 minutos</b></p><p></p></h3></div><form class=\"form-horizontal\"><div class=\"box-body\"><input type=\"hidden\" id=\"product-id\" value=\"undefined\"><input type=\"hidden\" id=\"priceUnitario-900000001\" value=\"173250\"><input type=\"hidden\" id=\"priceUnitarioCOP-900000001\" value=\"173250\"><input type=\"hidden\" id=\"precioColombiano-900000001\" value=\"173250\"><input type=\"hidden\" id=\"type-900000001\" value=\"Nacional\"><div class=\"row\"><div class=\"col-sm-12\"><div style=\"border-radius:5px;margin:5px;\" class=\" bg-null text-center\"> <input type=\"checkbox\" name=\"checkbox\" style=\"margin-top: 5px\" class=\"hidden checked\" id=\"IVA-900000001\" data-iva=\"900000001\" disabled=\"\" onclick=\"OnClickIVA(900000001)\" checked=\"\" data-check=\"true\" 1=\"\" <=\"\" div=\"\"></div></div><div class=\"row\" data-row-index=\"undefined\" id=\"content-900000001\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Cantidad</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-cube\"></i></span><input id=\"quantityCtx-900000001\" name=\"quantityCtx\" type=\"number\" min=\"0\" onchange=\"return CountquantityCtx(900000001)\" class=\"form-control\" value=\"1\"></div></div><input id=\"unitePriceCtx-900000001\" name=\"unitePriceCtx\" type=\"hidden\" class=\"form-control\" data-price=\"173250\" value=\"100\"><input id=\"precioOriginalCtx-900000001\" name=\"precioOriginalCtx\" type=\"hidden\" class=\"form-control\" data-price=\"173250\" value=\"100\"><input id=\"precioVentaCtx-900000001\" name=\"precioVentaCtx\" type=\"hidden\" class=\"form-control\" data-price=\"173250\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Ganancia</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"  fa fa-bookmark-o\"></i></span><input id=\"porcent-900000001\" name=\"porcentCtx\" type=\"number\" min=\"0\" class=\"form-control\" data=\"900000001\" onchange=\"ValidateChangePercents(900000001)\" value=\"20\"></div></div></div><br><div class=\"row\" data-row-index=\"undefined\" id=\"contentProductQxt\"><input type=\"hidden\" name=\"precioUnitarioConGanancia\" id=\"precioUnitarioConGanancia-900000001\" value=\"207900\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Tiempo Entrega</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-history\"></i></span><select name=\"timeDelivery\" id=\"timeDelivery-900000001\" data=\"900000001\" class=\"form-control\" style=\"border-color: red; border-style: solid;\"><option value=\"--Tiempo de Entrega--\"> --Tiempo de Entrega-- </option><option value=\"5\">5 dÃ­as</option><option value=\"15\">15 dÃ­as</option><option value=\"30\">30 dÃ­as</option><option value=\"45\">45 dÃ­as</option><option value=\"60\">60 dÃ­as</option><option value=\"90\">90 dÃ­as</option><option value=\"120\">120 dÃ­as</option><option value=\"150\">150 dÃ­as</option><option value=\"180\">180 dÃ­as</option></select></div></div><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Precio</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-dollar\"></i></span><input id=\"totalPriceCtx-900000001\" valor-modificable=\"$207,900\" undefined]=\"\" name=\"totalPriceCtx-900000001\" data-type=\"COP\" data-value=\"207900\" data-place=\"Nacional\" data-money=\"COP\" data-typeproduct=\"Servicio\" data-id=\"900000001\" data-proveedor=\"externo\" data-priceu=\"[object Undefined]\" type=\"text\" class=\"form-control\" disabled=\"\" lol=\"\" value=\"$207,900\"></div></div></div></div><div class=\"box-footer\"><button type=\"button\" onclick=\"DeleteProduct(900000001)\" class=\"btn btn-danger btn-xs-block\">Cancelar</button></div></div></form>', 1),
(11, 1, 'Servicio', 'aplicar aceite a maquinas', ' - ', ' - ', '180', 0, 0, 'COP', '173250', '50000', ' - ', '<p>aplicar aceite a la maquina</p>', '20', 1, ' - ', 2, ' - ', ' - ', 1, '0', ' <div class=\"box-header with-border\"><h3 class=\"box-title \" id=\"item-value-900000001\" data-id-producto=\"id-900000001\"><b>1</b> - Aplicar Aceite A Maquinas <br><p class=\"text-center\"><b>180 minutos</b></p><p></p></h3></div><form class=\"form-horizontal\"><div class=\"box-body\"><input type=\"hidden\" id=\"product-id\" value=\"undefined\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"priceUnitario-900000001\" value=\"173250\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"priceUnitarioCOP-900000001\" value=\"173250\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"precioColombiano-900000001\" value=\"173250\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"type-900000001\" value=\"Nacional\" style=\"border-color: rgb(210, 214, 222);\"><div class=\"row\"><div class=\"col-sm-12\"><div style=\"border-radius:5px;margin:5px;\" class=\" bg-null text-center\"> <input type=\"checkbox\" name=\"checkbox\" style=\"margin-top: 5px; border-color: rgb(210, 214, 222);\" class=\"hidden checked\" id=\"IVA-900000001\" data-iva=\"900000001\" disabled=\"\" onclick=\"OnClickIVA(900000001)\" checked=\"\" data-check=\"true\" 1=\"\" <=\"\" div=\"\"></div></div><div class=\"row\" data-row-index=\"undefined\" id=\"content-900000001\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Cantidad</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-cube\"></i></span><input id=\"quantityCtx-900000001\" name=\"quantityCtx\" type=\"number\" min=\"0\" onchange=\"return CountquantityCtx(900000001)\" class=\"form-control\" value=\"1\" style=\"border-color: rgb(210, 214, 222);\"></div></div><input id=\"unitePriceCtx-900000001\" name=\"unitePriceCtx\" type=\"hidden\" class=\"form-control\" data-price=\"173250\" value=\"100\" style=\"border-color: rgb(210, 214, 222);\"><input id=\"precioOriginalCtx-900000001\" name=\"precioOriginalCtx\" type=\"hidden\" class=\"form-control\" data-price=\"173250\" value=\"100\" style=\"border-color: rgb(210, 214, 222);\"><input id=\"precioVentaCtx-900000001\" name=\"precioVentaCtx\" type=\"hidden\" class=\"form-control\" data-price=\"173250\" style=\"border-color: rgb(210, 214, 222);\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Ganancia</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"  fa fa-bookmark-o\"></i></span><input id=\"porcent-900000001\" name=\"porcentCtx\" type=\"number\" min=\"0\" class=\"form-control\" data=\"900000001\" onchange=\"ValidateChangePercents(900000001)\" value=\"20\" style=\"border-color: rgb(210, 214, 222);\"></div></div></div><br><div class=\"row\" data-row-index=\"undefined\" id=\"contentProductQxt\"><input type=\"hidden\" name=\"precioUnitarioConGanancia\" id=\"precioUnitarioConGanancia-900000001\" value=\"17325034650\" style=\"border-color: rgb(210, 214, 222);\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Tiempo Entrega</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-history\"></i></span><select name=\"timeDelivery\" id=\"timeDelivery-900000001\" data=\"900000001\" class=\"form-control\" style=\"border-color: rgb(210, 214, 222); border-style: solid;\"><option value=\"--Tiempo de Entrega--\"> --Tiempo de Entrega-- </option><option value=\"5\">5 dÃ­as</option><option value=\"15\">15 dÃ­as</option><option value=\"30\">30 dÃ­as</option><option value=\"45\">45 dÃ­as</option><option value=\"60\">60 dÃ­as</option><option value=\"90\">90 dÃ­as</option><option value=\"120\">120 dÃ­as</option><option value=\"150\">150 dÃ­as</option><option value=\"180\">180 dÃ­as</option></select></div></div><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Precio</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-dollar\"></i></span><input id=\"totalPriceCtx-900000001\" valor-modificable=\"$207,900\" undefined]=\"\" name=\"totalPriceCtx-900000001\" data-type=\"COP\" data-value=\"207900\" data-place=\"Nacional\" data-money=\"COP\" data-typeproduct=\"Servicio\" data-id=\"900000001\" data-proveedor=\"externo\" data-priceu=\"[object Undefined]\" type=\"text\" class=\"form-control\" disabled=\"\" lol=\"\" value=\"$207,900\" style=\"border-color: rgb(210, 214, 222);\"></div></div></div></div><div class=\"box-footer\"><button type=\"button\" onclick=\"DeleteProduct(900000001)\" class=\"btn btn-danger btn-xs-block\">Cancelar</button></div></div></form>', 1),
(12, 1, 'Consumibles', 'aceite', '76', '76', '876', 7, 7, 'USD', '768000', '240,00', 'Angola', '<p>sdsfdsfsdfsdf</p>', '20', 1, 'proveedor', 2, '5 dias, salvo venta previa', 'Internacional', 1, '0', ' <div class=\"box-header with-border\"><h3 class=\"box-title \" id=\"item-value-1\" data-id-producto=\"id-1\"><b>2</b> - aceite <br><p class=\"text-center\"><b>876</b></p><p></p></h3></div><form class=\"form-horizontal\"><div class=\"box-body\"><input type=\"hidden\" id=\"product-id\" value=\"0\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"priceUnitario-1\" value=\"768000\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"priceUnitarioCOP-1\" value=\"768000\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"precioColombiano-1\" value=\"768000\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"type-1\" value=\"Internacional\" style=\"border-color: rgb(210, 214, 222);\"><div class=\"row\"><div class=\"col-sm-12\"><div style=\"border-radius:5px;margin:5px;\" class=\" bg-null text-center\"> <input type=\"checkbox\" name=\"checkbox\" style=\"margin-top: 5px; border-color: rgb(210, 214, 222);\" class=\"hidden checked\" id=\"IVA-1\" data-iva=\"1\" disabled=\"\" onclick=\"OnClickIVA(1)\" checked=\"\" data-check=\"true\" 2=\"\" <=\"\" div=\"\"></div></div><div class=\"row\" data-row-index=\"0\" id=\"content-1\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Cantidad</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-cube\"></i></span><input id=\"quantityCtx-1\" name=\"quantityCtx\" type=\"number\" min=\"0\" onchange=\"return CountquantityCtx(1)\" class=\"form-control\" value=\"1\" style=\"border-color: rgb(210, 214, 222);\"></div></div><input id=\"unitePriceCtx-1\" name=\"unitePriceCtx\" type=\"hidden\" class=\"form-control\" data-price=\"768000\" value=\"100\" style=\"border-color: rgb(210, 214, 222);\"><input id=\"precioOriginalCtx-1\" name=\"precioOriginalCtx\" type=\"hidden\" class=\"form-control\" data-price=\"240\" value=\"100\" style=\"border-color: rgb(210, 214, 222);\"><input id=\"precioVentaCtx-1\" name=\"precioVentaCtx\" type=\"hidden\" class=\"form-control\" data-price=\"240\" style=\"border-color: rgb(210, 214, 222);\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Ganancia</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"  fa fa-bookmark-o\"></i></span><input id=\"porcent-1\" name=\"porcentCtx\" type=\"number\" min=\"0\" class=\"form-control\" data=\"1\" onchange=\"ValidateChangePercents(1)\" value=\"20\" style=\"border-color: rgb(210, 214, 222);\"></div></div></div><br><div class=\"row\" data-row-index=\"0\" id=\"contentProductQxt\"><input type=\"hidden\" name=\"precioUnitarioConGanancia\" id=\"precioUnitarioConGanancia-1\" value=\"768000153600\" style=\"border-color: rgb(210, 214, 222);\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Tiempo Entrega</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-history\"></i></span><select name=\"timeDelivery\" id=\"timeDelivery-1\" data=\"1\" class=\"form-control\" style=\"border-color: rgb(210, 214, 222);\"><option value=\"5 dias, salvo venta previa\" selected=\"selected\">5 dias, salvo venta previa</option></select></div></div><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Precio</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-dollar\"></i></span><input id=\"totalPriceCtx-1\" valor-modificable=\"$921,600\" undefined]=\"\" name=\"totalPriceCtx-1\" data-type=\"COP\" data-value=\"921600\" data-place=\"Internacional\" data-money=\"USD\" data-typeproduct=\"Consumibles\" data-id=\"1\" data-proveedor=\"proveedor\" data-priceu=\"[object Undefined]\" type=\"text\" class=\"form-control\" disabled=\"\" lol=\"\" value=\"$921,600\" style=\"border-color: rgb(210, 214, 222);\"></div></div></div></div><div class=\"box-footer\"><button type=\"button\" onclick=\"DeleteProduct(1)\" class=\"btn btn-danger btn-xs-block\">Cancelar</button></div></div></form>', 1);
INSERT INTO `elementsmainteince` (`id`, `idProduct`, `typeProduct`, `name`, `brand`, `reference`, `presentation`, `amount`, `minimumAmount`, `typeCurrency`, `priceUnit`, `priceUnitIndividual`, `country`, `description`, `percent`, `quantity`, `nameCompany`, `idQuotation`, `timeDelivery`, `namePlace`, `IVA`, `FLETE`, `nodo`, `isFromCompany`) VALUES
(13, 1, 'Servicio', 'aplicar aceite a maquinas', ' - ', ' - ', '180', 0, 0, 'COP', '173250', '50000', ' - ', '<p>aplicar aceite a la maquina</p>', '20', 1, ' - ', 2, ' - ', ' - ', 1, '0', ' <div class=\"box-header with-border\"><h3 class=\"box-title \" id=\"item-value-900000001\" data-id-producto=\"id-900000001\"><b>1</b> - Aplicar Aceite A Maquinas <br><p class=\"text-center\"><b>180 minutos</b></p><p></p></h3></div><form class=\"form-horizontal\"><div class=\"box-body\"><input type=\"hidden\" id=\"product-id\" value=\"undefined\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"priceUnitario-900000001\" value=\"173250\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"priceUnitarioCOP-900000001\" value=\"173250\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"precioColombiano-900000001\" value=\"173250\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"type-900000001\" value=\"Nacional\" style=\"border-color: rgb(210, 214, 222);\"><div class=\"row\"><div class=\"col-sm-12\"><div style=\"border-radius:5px;margin:5px;\" class=\" bg-null text-center\"> <input type=\"checkbox\" name=\"checkbox\" style=\"margin-top: 5px; border-color: rgb(210, 214, 222);\" class=\"hidden checked\" id=\"IVA-900000001\" data-iva=\"900000001\" disabled=\"\" onclick=\"OnClickIVA(900000001)\" checked=\"\" data-check=\"true\" 1=\"\" <=\"\" div=\"\"></div></div><div class=\"row\" data-row-index=\"undefined\" id=\"content-900000001\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Cantidad</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-cube\"></i></span><input id=\"quantityCtx-900000001\" name=\"quantityCtx\" type=\"number\" min=\"0\" onchange=\"return CountquantityCtx(900000001)\" class=\"form-control\" value=\"1\" style=\"border-color: rgb(210, 214, 222);\"></div></div><input id=\"unitePriceCtx-900000001\" name=\"unitePriceCtx\" type=\"hidden\" class=\"form-control\" data-price=\"173250\" value=\"100\" style=\"border-color: rgb(210, 214, 222);\"><input id=\"precioOriginalCtx-900000001\" name=\"precioOriginalCtx\" type=\"hidden\" class=\"form-control\" data-price=\"173250\" value=\"100\" style=\"border-color: rgb(210, 214, 222);\"><input id=\"precioVentaCtx-900000001\" name=\"precioVentaCtx\" type=\"hidden\" class=\"form-control\" data-price=\"173250\" style=\"border-color: rgb(210, 214, 222);\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Ganancia</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"  fa fa-bookmark-o\"></i></span><input id=\"porcent-900000001\" name=\"porcentCtx\" type=\"number\" min=\"0\" class=\"form-control\" data=\"900000001\" onchange=\"ValidateChangePercents(900000001)\" value=\"20\" style=\"border-color: rgb(210, 214, 222);\"></div></div></div><br><div class=\"row\" data-row-index=\"undefined\" id=\"contentProductQxt\"><input type=\"hidden\" name=\"precioUnitarioConGanancia\" id=\"precioUnitarioConGanancia-900000001\" value=\"207900\" style=\"border-color: rgb(210, 214, 222);\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Tiempo Entrega</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-history\"></i></span><select name=\"timeDelivery\" id=\"timeDelivery-900000001\" data=\"900000001\" class=\"form-control\" style=\"border-color: red; border-style: solid;\"><option value=\"--Tiempo de Entrega--\"> --Tiempo de Entrega-- </option><option value=\"5\">5 dÃ­as</option><option value=\"15\">15 dÃ­as</option><option value=\"30\">30 dÃ­as</option><option value=\"45\">45 dÃ­as</option><option value=\"60\">60 dÃ­as</option><option value=\"90\">90 dÃ­as</option><option value=\"120\">120 dÃ­as</option><option value=\"150\">150 dÃ­as</option><option value=\"180\">180 dÃ­as</option></select></div></div><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Precio</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-dollar\"></i></span><input id=\"totalPriceCtx-900000001\" valor-modificable=\"$207,900\" undefined]=\"\" name=\"totalPriceCtx-900000001\" data-type=\"COP\" data-value=\"207900\" data-place=\"Nacional\" data-money=\"COP\" data-typeproduct=\"Servicio\" data-id=\"900000001\" data-proveedor=\"externo\" data-priceu=\"[object Undefined]\" type=\"text\" class=\"form-control\" disabled=\"\" lol=\"\" value=\"$207,900\" style=\"border-color: rgb(210, 214, 222);\"></div></div></div></div><div class=\"box-footer\"><button type=\"button\" onclick=\"DeleteProduct(900000001)\" class=\"btn btn-danger btn-xs-block\">Cancelar</button></div></div></form>', 1),
(14, 1, 'Consumibles', 'aceite', '76', '76', '876', 7, 7, 'USD', '768000', '240,00', 'Angola', '<p>sdsfdsfsdfsdf</p>', '20', 1, 'proveedor', 2, '5 dias, salvo venta previa', 'Internacional', 1, '0', ' <div class=\"box-header with-border\"><h3 class=\"box-title \" id=\"item-value-1\" data-id-producto=\"id-1\"><b>2</b> - aceite <br><p class=\"text-center\"><b>876</b></p><p></p></h3></div><form class=\"form-horizontal\"><div class=\"box-body\"><input type=\"hidden\" id=\"product-id\" value=\"0\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"priceUnitario-1\" value=\"768000\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"priceUnitarioCOP-1\" value=\"768000\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"precioColombiano-1\" value=\"768000\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"type-1\" value=\"Internacional\" style=\"border-color: rgb(210, 214, 222);\"><div class=\"row\"><div class=\"col-sm-12\"><div style=\"border-radius:5px;margin:5px;\" class=\" bg-null text-center\"> <input type=\"checkbox\" name=\"checkbox\" style=\"margin-top: 5px; border-color: rgb(210, 214, 222);\" class=\"hidden checked\" id=\"IVA-1\" data-iva=\"1\" disabled=\"\" onclick=\"OnClickIVA(1)\" checked=\"\" data-check=\"true\" 2=\"\" <=\"\" div=\"\"></div></div><div class=\"row\" data-row-index=\"0\" id=\"content-1\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Cantidad</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-cube\"></i></span><input id=\"quantityCtx-1\" name=\"quantityCtx\" type=\"number\" min=\"0\" onchange=\"return CountquantityCtx(1)\" class=\"form-control\" value=\"1\" style=\"border-color: rgb(210, 214, 222);\"></div></div><input id=\"unitePriceCtx-1\" name=\"unitePriceCtx\" type=\"hidden\" class=\"form-control\" data-price=\"768000\" value=\"100\" style=\"border-color: rgb(210, 214, 222);\"><input id=\"precioOriginalCtx-1\" name=\"precioOriginalCtx\" type=\"hidden\" class=\"form-control\" data-price=\"240\" value=\"100\" style=\"border-color: rgb(210, 214, 222);\"><input id=\"precioVentaCtx-1\" name=\"precioVentaCtx\" type=\"hidden\" class=\"form-control\" data-price=\"240\" style=\"border-color: rgb(210, 214, 222);\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Ganancia</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"  fa fa-bookmark-o\"></i></span><input id=\"porcent-1\" name=\"porcentCtx\" type=\"number\" min=\"0\" class=\"form-control\" data=\"1\" onchange=\"ValidateChangePercents(1)\" value=\"20\" style=\"border-color: rgb(210, 214, 222);\"></div></div></div><br><div class=\"row\" data-row-index=\"0\" id=\"contentProductQxt\"><input type=\"hidden\" name=\"precioUnitarioConGanancia\" id=\"precioUnitarioConGanancia-1\" value=\"921600\" style=\"border-color: rgb(210, 214, 222);\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Tiempo Entrega</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-history\"></i></span><select name=\"timeDelivery\" id=\"timeDelivery-1\" data=\"1\" class=\"form-control\" style=\"border-color: rgb(210, 214, 222);\"><option value=\"5 dias, salvo venta previa\" selected=\"selected\">5 dias, salvo venta previa</option></select></div></div><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Precio</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-dollar\"></i></span><input id=\"totalPriceCtx-1\" valor-modificable=\"$921,600\" undefined]=\"\" name=\"totalPriceCtx-1\" data-type=\"COP\" data-value=\"921600\" data-place=\"Internacional\" data-money=\"USD\" data-typeproduct=\"Consumibles\" data-id=\"1\" data-proveedor=\"proveedor\" data-priceu=\"[object Undefined]\" type=\"text\" class=\"form-control\" disabled=\"\" lol=\"\" value=\"$921,600\" style=\"border-color: rgb(210, 214, 222);\"></div></div></div></div><div class=\"box-footer\"><button type=\"button\" onclick=\"DeleteProduct(1)\" class=\"btn btn-danger btn-xs-block\">Cancelar</button></div></div></form>', 1),
(15, 1, 'Servicio', 'aplicar aceite a maquinas', ' - ', ' - ', '180', 0, 0, 'COP', '173250', '50000', ' - ', '<p>aplicar aceite a la maquina</p>', '20', 1, ' - ', 2, ' - ', ' - ', 1, '0', ' <div class=\"box-header with-border\"><h3 class=\"box-title \" id=\"item-value-900000001\" data-id-producto=\"id-900000001\"><b>1</b> - Aplicar Aceite A Maquinas <br><p class=\"text-center\"><b>180 minutos</b></p><p></p></h3></div><form class=\"form-horizontal\"><div class=\"box-body\"><input type=\"hidden\" id=\"product-id\" value=\"undefined\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"priceUnitario-900000001\" value=\"173250\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"priceUnitarioCOP-900000001\" value=\"173250\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"precioColombiano-900000001\" value=\"173250\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"type-900000001\" value=\"Nacional\" style=\"border-color: rgb(210, 214, 222);\"><div class=\"row\"><div class=\"col-sm-12\"><div style=\"border-radius:5px;margin:5px;\" class=\" bg-null text-center\"> <input type=\"checkbox\" name=\"checkbox\" style=\"margin-top: 5px; border-color: rgb(210, 214, 222);\" class=\"hidden checked\" id=\"IVA-900000001\" data-iva=\"900000001\" disabled=\"\" onclick=\"OnClickIVA(900000001)\" checked=\"\" data-check=\"true\" 1=\"\" <=\"\" div=\"\"></div></div><div class=\"row\" data-row-index=\"undefined\" id=\"content-900000001\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Cantidad</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-cube\"></i></span><input id=\"quantityCtx-900000001\" name=\"quantityCtx\" type=\"number\" min=\"0\" onchange=\"return CountquantityCtx(900000001)\" class=\"form-control\" value=\"1\" style=\"border-color: rgb(210, 214, 222);\"></div></div><input id=\"unitePriceCtx-900000001\" name=\"unitePriceCtx\" type=\"hidden\" class=\"form-control\" data-price=\"173250\" value=\"100\" style=\"border-color: rgb(210, 214, 222);\"><input id=\"precioOriginalCtx-900000001\" name=\"precioOriginalCtx\" type=\"hidden\" class=\"form-control\" data-price=\"173250\" value=\"100\" style=\"border-color: rgb(210, 214, 222);\"><input id=\"precioVentaCtx-900000001\" name=\"precioVentaCtx\" type=\"hidden\" class=\"form-control\" data-price=\"173250\" style=\"border-color: rgb(210, 214, 222);\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Ganancia</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"  fa fa-bookmark-o\"></i></span><input id=\"porcent-900000001\" name=\"porcentCtx\" type=\"number\" min=\"0\" class=\"form-control\" data=\"900000001\" onchange=\"ValidateChangePercents(900000001)\" value=\"20\" style=\"border-color: rgb(210, 214, 222);\"></div></div></div><br><div class=\"row\" data-row-index=\"undefined\" id=\"contentProductQxt\"><input type=\"hidden\" name=\"precioUnitarioConGanancia\" id=\"precioUnitarioConGanancia-900000001\" value=\"17325034650\" style=\"border-color: rgb(210, 214, 222);\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Tiempo Entrega</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-history\"></i></span><select name=\"timeDelivery\" id=\"timeDelivery-900000001\" data=\"900000001\" class=\"form-control\" style=\"border-color: rgb(210, 214, 222);\"><option value=\"--Tiempo de Entrega--\"> --Tiempo de Entrega-- </option><option value=\"5\">5 dÃ­as</option><option value=\"15\">15 dÃ­as</option><option value=\"30\">30 dÃ­as</option><option value=\"45\">45 dÃ­as</option><option value=\"60\">60 dÃ­as</option><option value=\"90\">90 dÃ­as</option><option value=\"120\">120 dÃ­as</option><option value=\"150\">150 dÃ­as</option><option value=\"180\">180 dÃ­as</option></select></div></div><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Precio</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-dollar\"></i></span><input id=\"totalPriceCtx-900000001\" valor-modificable=\"$1,039,500\" undefined]=\"\" name=\"totalPriceCtx-900000001\" data-type=\"COP\" data-value=\"1039500\" data-place=\"Nacional\" data-money=\"COP\" data-typeproduct=\"Servicio\" data-id=\"900000001\" data-proveedor=\"externo\" data-priceu=\"[object Undefined]\" type=\"text\" class=\"form-control\" disabled=\"\" lol=\"\" value=\"$1,039,500\" style=\"border-color: rgb(210, 214, 222);\"></div></div></div></div><div class=\"box-footer\"><button type=\"button\" onclick=\"DeleteProduct(900000001)\" class=\"btn btn-danger btn-xs-block\">Cancelar</button></div></div></form>', 1),
(16, 2, 'Servicio', 'sdfds3', ' - ', ' - ', '233', 0, 0, 'COP', '3827', '233', ' - ', '<p>dfsdffsdfsd</p>', '20', 1, ' - ', 2, ' - ', ' - ', 1, '0', ' <div class=\"box-header with-border\"><h3 class=\"box-title \" id=\"item-value-900000002\" data-id-producto=\"id-900000002\"><b>2</b> - Sdfds3 <br><p class=\"text-center\"><b>233 minutos</b></p><p></p></h3></div><form class=\"form-horizontal\"><div class=\"box-body\"><input type=\"hidden\" id=\"product-id\" value=\"undefined\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"priceUnitario-900000002\" value=\"3827\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"priceUnitarioCOP-900000002\" value=\"3827\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"precioColombiano-900000002\" value=\"3827\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"type-900000002\" value=\"Nacional\" style=\"border-color: rgb(210, 214, 222);\"><div class=\"row\"><div class=\"col-sm-12\"><div style=\"border-radius:5px;margin:5px;\" class=\" bg-null text-center\"> <input type=\"checkbox\" name=\"checkbox\" style=\"margin-top: 5px; border-color: rgb(210, 214, 222);\" class=\"hidden checked\" id=\"IVA-900000002\" data-iva=\"900000002\" disabled=\"\" onclick=\"OnClickIVA(900000002)\" checked=\"\" data-check=\"true\" 2=\"\" <=\"\" div=\"\"></div></div><div class=\"row\" data-row-index=\"undefined\" id=\"content-900000002\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Cantidad</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-cube\"></i></span><input id=\"quantityCtx-900000002\" name=\"quantityCtx\" type=\"number\" min=\"0\" onchange=\"return CountquantityCtx(900000002)\" class=\"form-control\" value=\"1\" style=\"border-color: rgb(210, 214, 222);\"></div></div><input id=\"unitePriceCtx-900000002\" name=\"unitePriceCtx\" type=\"hidden\" class=\"form-control\" data-price=\"3827\" value=\"100\" style=\"border-color: rgb(210, 214, 222);\"><input id=\"precioOriginalCtx-900000002\" name=\"precioOriginalCtx\" type=\"hidden\" class=\"form-control\" data-price=\"3827\" value=\"100\" style=\"border-color: rgb(210, 214, 222);\"><input id=\"precioVentaCtx-900000002\" name=\"precioVentaCtx\" type=\"hidden\" class=\"form-control\" data-price=\"3827\" style=\"border-color: rgb(210, 214, 222);\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Ganancia</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"  fa fa-bookmark-o\"></i></span><input id=\"porcent-900000002\" name=\"porcentCtx\" type=\"number\" min=\"0\" class=\"form-control\" data=\"900000002\" onchange=\"ValidateChangePercents(900000002)\" value=\"20\" style=\"border-color: rgb(210, 214, 222);\"></div></div></div><br><div class=\"row\" data-row-index=\"undefined\" id=\"contentProductQxt\"><input type=\"hidden\" name=\"precioUnitarioConGanancia\" id=\"precioUnitarioConGanancia-900000002\" value=\"3827765.4000000000001\" style=\"border-color: rgb(210, 214, 222);\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Tiempo Entrega</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-history\"></i></span><select name=\"timeDelivery\" id=\"timeDelivery-900000002\" data=\"900000002\" class=\"form-control\" style=\"border-color: rgb(210, 214, 222); border-style: solid;\"><option value=\"--Tiempo de Entrega--\"> --Tiempo de Entrega-- </option><option value=\"5\">5 dÃ­as</option><option value=\"15\">15 dÃ­as</option><option value=\"30\">30 dÃ­as</option><option value=\"45\">45 dÃ­as</option><option value=\"60\">60 dÃ­as</option><option value=\"90\">90 dÃ­as</option><option value=\"120\">120 dÃ­as</option><option value=\"150\">150 dÃ­as</option><option value=\"180\">180 dÃ­as</option></select></div></div><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Precio</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-dollar\"></i></span><input id=\"totalPriceCtx-900000002\" valor-modificable=\"$4592\" undefined]=\"\" name=\"totalPriceCtx-900000002\" data-type=\"COP\" data-value=\"4592.4\" data-place=\"Nacional\" data-money=\"COP\" data-typeproduct=\"Servicio\" data-id=\"900000002\" data-proveedor=\"externo\" data-priceu=\"[object Undefined]\" type=\"text\" class=\"form-control\" disabled=\"\" lol=\"\" value=\"$4592\" style=\"border-color: rgb(210, 214, 222);\"></div></div></div></div><div class=\"box-footer\"><button type=\"button\" onclick=\"DeleteProduct(900000002)\" class=\"btn btn-danger btn-xs-block\">Cancelar</button></div></div></form>', 1),
(17, 1, 'Consumibles', 'aceite', '76', '76', '876', 7, 7, 'USD', '768000', '240,00', 'Angola', '<p>sdsfdsfsdfsdf</p>', '20', 5, 'proveedor', 2, '5 dias, salvo venta previa', 'Internacional', 1, '0', ' <div class=\"box-header with-border\"><h3 class=\"box-title \" id=\"item-value-1\" data-id-producto=\"id-1\"><b>3</b> - aceite <br><p class=\"text-center\"><b>876</b></p><p></p></h3></div><form class=\"form-horizontal\"><div class=\"box-body\"><input type=\"hidden\" id=\"product-id\" value=\"0\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"priceUnitario-1\" value=\"768000\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"priceUnitarioCOP-1\" value=\"768000\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"precioColombiano-1\" value=\"768000\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"type-1\" value=\"Internacional\" style=\"border-color: rgb(210, 214, 222);\"><div class=\"row\"><div class=\"col-sm-12\"><div style=\"border-radius:5px;margin:5px;\" class=\" bg-null text-center\"> <input type=\"checkbox\" name=\"checkbox\" style=\"margin-top: 5px; border-color: rgb(210, 214, 222);\" class=\"hidden checked\" id=\"IVA-1\" data-iva=\"1\" disabled=\"\" onclick=\"OnClickIVA(1)\" checked=\"\" data-check=\"true\" 3=\"\" <=\"\" div=\"\"></div></div><div class=\"row\" data-row-index=\"0\" id=\"content-1\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Cantidad</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-cube\"></i></span><input id=\"quantityCtx-1\" name=\"quantityCtx\" type=\"number\" min=\"0\" onchange=\"return CountquantityCtx(1)\" class=\"form-control\" value=\"1\" style=\"border-color: rgb(210, 214, 222);\"></div></div><input id=\"unitePriceCtx-1\" name=\"unitePriceCtx\" type=\"hidden\" class=\"form-control\" data-price=\"768000\" value=\"100\" style=\"border-color: rgb(210, 214, 222);\"><input id=\"precioOriginalCtx-1\" name=\"precioOriginalCtx\" type=\"hidden\" class=\"form-control\" data-price=\"240\" value=\"100\" style=\"border-color: rgb(210, 214, 222);\"><input id=\"precioVentaCtx-1\" name=\"precioVentaCtx\" type=\"hidden\" class=\"form-control\" data-price=\"240\" style=\"border-color: rgb(210, 214, 222);\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Ganancia</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"  fa fa-bookmark-o\"></i></span><input id=\"porcent-1\" name=\"porcentCtx\" type=\"number\" min=\"0\" class=\"form-control\" data=\"1\" onchange=\"ValidateChangePercents(1)\" value=\"20\" style=\"border-color: rgb(210, 214, 222);\"></div></div></div><br><div class=\"row\" data-row-index=\"0\" id=\"contentProductQxt\"><input type=\"hidden\" name=\"precioUnitarioConGanancia\" id=\"precioUnitarioConGanancia-1\" value=\"768000153600\" style=\"border-color: rgb(210, 214, 222);\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Tiempo Entrega</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-history\"></i></span><select name=\"timeDelivery\" id=\"timeDelivery-1\" data=\"1\" class=\"form-control\" style=\"border-color: rgb(210, 214, 222);\"><option value=\"5 dias, salvo venta previa\" selected=\"selected\">5 dias, salvo venta previa</option></select></div></div><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Precio</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-dollar\"></i></span><input id=\"totalPriceCtx-1\" valor-modificable=\"$4,608,000\" undefined]=\"\" name=\"totalPriceCtx-1\" data-type=\"COP\" data-value=\"4608000\" data-place=\"Internacional\" data-money=\"USD\" data-typeproduct=\"Consumibles\" data-id=\"1\" data-proveedor=\"proveedor\" data-priceu=\"[object Undefined]\" type=\"text\" class=\"form-control\" disabled=\"\" lol=\"\" value=\"$4,608,000\" style=\"border-color: rgb(210, 214, 222);\"></div></div></div></div><div class=\"box-footer\"><button type=\"button\" onclick=\"DeleteProduct(1)\" class=\"btn btn-danger btn-xs-block\">Cancelar</button></div></div></form>', 1),
(18, 1, 'Servicio', 'aplicar aceite a maquinas', ' - ', ' - ', '180', 0, 0, 'COP', '173250', '50000', ' - ', '<p>aplicar aceite a la maquina</p>', '20', 1, ' - ', 3, ' - ', ' - ', 1, '0', ' <div class=\"box-header with-border\"><h3 class=\"box-title \" id=\"item-value-900000001\" data-id-producto=\"id-900000001\"><b>1</b> - Aplicar Aceite A Maquinas <br><p class=\"text-center\"><b>180 minutos</b></p><p></p></h3></div><form class=\"form-horizontal\"><div class=\"box-body\"><input type=\"hidden\" id=\"product-id\" value=\"undefined\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"priceUnitario-900000001\" value=\"173250\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"priceUnitarioCOP-900000001\" value=\"173250\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"precioColombiano-900000001\" value=\"173250\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"type-900000001\" value=\"Nacional\" style=\"border-color: rgb(210, 214, 222);\"><div class=\"row\"><div class=\"col-sm-12\"><div style=\"border-radius:5px;margin:5px;\" class=\" bg-null text-center\"> <input type=\"checkbox\" name=\"checkbox\" style=\"margin-top: 5px; border-color: rgb(210, 214, 222);\" class=\"hidden checked\" id=\"IVA-900000001\" data-iva=\"900000001\" disabled=\"\" onclick=\"OnClickIVA(900000001)\" checked=\"\" data-check=\"true\" 1=\"\" <=\"\" div=\"\"></div></div><div class=\"row\" data-row-index=\"undefined\" id=\"content-900000001\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Cantidad</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-cube\"></i></span><input id=\"quantityCtx-900000001\" name=\"quantityCtx\" type=\"number\" min=\"0\" onchange=\"return CountquantityCtx(900000001)\" class=\"form-control\" value=\"1\" style=\"border-color: rgb(210, 214, 222);\"></div></div><input id=\"unitePriceCtx-900000001\" name=\"unitePriceCtx\" type=\"hidden\" class=\"form-control\" data-price=\"173250\" value=\"100\" style=\"border-color: rgb(210, 214, 222);\"><input id=\"precioOriginalCtx-900000001\" name=\"precioOriginalCtx\" type=\"hidden\" class=\"form-control\" data-price=\"173250\" value=\"100\" style=\"border-color: rgb(210, 214, 222);\"><input id=\"precioVentaCtx-900000001\" name=\"precioVentaCtx\" type=\"hidden\" class=\"form-control\" data-price=\"173250\" style=\"border-color: rgb(210, 214, 222);\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Ganancia</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"  fa fa-bookmark-o\"></i></span><input id=\"porcent-900000001\" name=\"porcentCtx\" type=\"number\" min=\"0\" class=\"form-control\" data=\"900000001\" onchange=\"ValidateChangePercents(900000001)\" value=\"20\" style=\"border-color: rgb(210, 214, 222);\"></div></div></div><br><div class=\"row\" data-row-index=\"undefined\" id=\"contentProductQxt\"><input type=\"hidden\" name=\"precioUnitarioConGanancia\" id=\"precioUnitarioConGanancia-900000001\" value=\"17325034650\" style=\"border-color: rgb(210, 214, 222);\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Tiempo Entrega</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-history\"></i></span><select name=\"timeDelivery\" id=\"timeDelivery-900000001\" data=\"900000001\" class=\"form-control\" style=\"border-color: rgb(210, 214, 222); border-style: solid;\"><option value=\"--Tiempo de Entrega--\"> --Tiempo de Entrega-- </option><option value=\"5\">5 dÃ­as</option><option value=\"15\">15 dÃ­as</option><option value=\"30\">30 dÃ­as</option><option value=\"45\">45 dÃ­as</option><option value=\"60\">60 dÃ­as</option><option value=\"90\">90 dÃ­as</option><option value=\"120\">120 dÃ­as</option><option value=\"150\">150 dÃ­as</option><option value=\"180\">180 dÃ­as</option></select></div></div><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Precio</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-dollar\"></i></span><input id=\"totalPriceCtx-900000001\" valor-modificable=\"$207,900\" undefined]=\"\" name=\"totalPriceCtx-900000001\" data-type=\"COP\" data-value=\"207900\" data-place=\"Nacional\" data-money=\"COP\" data-typeproduct=\"Servicio\" data-id=\"900000001\" data-proveedor=\"externo\" data-priceu=\"[object Undefined]\" type=\"text\" class=\"form-control\" disabled=\"\" lol=\"\" value=\"$207,900\" style=\"border-color: rgb(210, 214, 222);\"></div></div></div></div><div class=\"box-footer\"><button type=\"button\" onclick=\"DeleteProduct(900000001)\" class=\"btn btn-danger btn-xs-block\">Cancelar</button></div></div></form>', 1),
(19, 1, 'Consumibles', 'aceite', '76', '76', '876', 7, 7, 'USD', '768000', '240,00', 'Angola', '<p>sdsfdsfsdfsdf</p>', '20', 1, 'proveedor', 3, '5 dias, salvo venta previa', 'Internacional', 1, '0', ' <div class=\"box-header with-border\"><h3 class=\"box-title \" id=\"item-value-1\" data-id-producto=\"id-1\"><b>2</b> - aceite <br><p class=\"text-center\"><b>876</b></p><p></p></h3></div><form class=\"form-horizontal\"><div class=\"box-body\"><input type=\"hidden\" id=\"product-id\" value=\"0\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"priceUnitario-1\" value=\"768000\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"priceUnitarioCOP-1\" value=\"768000\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"precioColombiano-1\" value=\"768000\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"type-1\" value=\"Internacional\" style=\"border-color: rgb(210, 214, 222);\"><div class=\"row\"><div class=\"col-sm-12\"><div style=\"border-radius:5px;margin:5px;\" class=\" bg-null text-center\"> <input type=\"checkbox\" name=\"checkbox\" style=\"margin-top: 5px; border-color: rgb(210, 214, 222);\" class=\"hidden checked\" id=\"IVA-1\" data-iva=\"1\" disabled=\"\" onclick=\"OnClickIVA(1)\" checked=\"\" data-check=\"true\" 2=\"\" <=\"\" div=\"\"></div></div><div class=\"row\" data-row-index=\"0\" id=\"content-1\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Cantidad</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-cube\"></i></span><input id=\"quantityCtx-1\" name=\"quantityCtx\" type=\"number\" min=\"0\" onchange=\"return CountquantityCtx(1)\" class=\"form-control\" value=\"1\" style=\"border-color: rgb(210, 214, 222);\"></div></div><input id=\"unitePriceCtx-1\" name=\"unitePriceCtx\" type=\"hidden\" class=\"form-control\" data-price=\"768000\" value=\"100\" style=\"border-color: rgb(210, 214, 222);\"><input id=\"precioOriginalCtx-1\" name=\"precioOriginalCtx\" type=\"hidden\" class=\"form-control\" data-price=\"240\" value=\"100\" style=\"border-color: rgb(210, 214, 222);\"><input id=\"precioVentaCtx-1\" name=\"precioVentaCtx\" type=\"hidden\" class=\"form-control\" data-price=\"240\" style=\"border-color: rgb(210, 214, 222);\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Ganancia</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"  fa fa-bookmark-o\"></i></span><input id=\"porcent-1\" name=\"porcentCtx\" type=\"number\" min=\"0\" class=\"form-control\" data=\"1\" onchange=\"ValidateChangePercents(1)\" value=\"20\" style=\"border-color: rgb(210, 214, 222);\"></div></div></div><br><div class=\"row\" data-row-index=\"0\" id=\"contentProductQxt\"><input type=\"hidden\" name=\"precioUnitarioConGanancia\" id=\"precioUnitarioConGanancia-1\" value=\"768000153600\" style=\"border-color: rgb(210, 214, 222);\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Tiempo Entrega</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-history\"></i></span><select name=\"timeDelivery\" id=\"timeDelivery-1\" data=\"1\" class=\"form-control\" style=\"border-color: rgb(210, 214, 222);\"><option value=\"5 dias, salvo venta previa\" selected=\"selected\">5 dias, salvo venta previa</option></select></div></div><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Precio</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-dollar\"></i></span><input id=\"totalPriceCtx-1\" valor-modificable=\"$921,600\" undefined]=\"\" name=\"totalPriceCtx-1\" data-type=\"COP\" data-value=\"921600\" data-place=\"Internacional\" data-money=\"USD\" data-typeproduct=\"Consumibles\" data-id=\"1\" data-proveedor=\"proveedor\" data-priceu=\"[object Undefined]\" type=\"text\" class=\"form-control\" disabled=\"\" lol=\"\" value=\"$921,600\" style=\"border-color: rgb(210, 214, 222);\"></div></div></div></div><div class=\"box-footer\"><button type=\"button\" onclick=\"DeleteProduct(1)\" class=\"btn btn-danger btn-xs-block\">Cancelar</button></div></div></form>', 1),
(20, 1, 'Servicio', 'aplicar aceite a maquinas', ' - ', ' - ', '180', 0, 0, 'COP', '173250', '50000', ' - ', '<p>aplicar aceite a la maquina</p>', '20', 1, ' - ', 3, ' - ', ' - ', 1, '0', ' <div class=\"box-header with-border\"><h3 class=\"box-title \" id=\"item-value-900000001\" data-id-producto=\"id-900000001\"><b>1</b> - Aplicar Aceite A Maquinas <br><p class=\"text-center\"><b>180 minutos</b></p><p></p></h3></div><form class=\"form-horizontal\"><div class=\"box-body\"><input type=\"hidden\" id=\"product-id\" value=\"undefined\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"priceUnitario-900000001\" value=\"173250\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"priceUnitarioCOP-900000001\" value=\"173250\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"precioColombiano-900000001\" value=\"173250\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"type-900000001\" value=\"Nacional\" style=\"border-color: rgb(210, 214, 222);\"><div class=\"row\"><div class=\"col-sm-12\"><div style=\"border-radius:5px;margin:5px;\" class=\" bg-null text-center\"> <input type=\"checkbox\" name=\"checkbox\" style=\"margin-top: 5px; border-color: rgb(210, 214, 222);\" class=\"hidden checked\" id=\"IVA-900000001\" data-iva=\"900000001\" disabled=\"\" onclick=\"OnClickIVA(900000001)\" checked=\"\" data-check=\"true\" 1=\"\" <=\"\" div=\"\"></div></div><div class=\"row\" data-row-index=\"undefined\" id=\"content-900000001\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Cantidad</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-cube\"></i></span><input id=\"quantityCtx-900000001\" name=\"quantityCtx\" type=\"number\" min=\"0\" onchange=\"return CountquantityCtx(900000001)\" class=\"form-control\" value=\"1\" style=\"border-color: rgb(210, 214, 222);\"></div></div><input id=\"unitePriceCtx-900000001\" name=\"unitePriceCtx\" type=\"hidden\" class=\"form-control\" data-price=\"173250\" value=\"100\" style=\"border-color: rgb(210, 214, 222);\"><input id=\"precioOriginalCtx-900000001\" name=\"precioOriginalCtx\" type=\"hidden\" class=\"form-control\" data-price=\"173250\" value=\"100\" style=\"border-color: rgb(210, 214, 222);\"><input id=\"precioVentaCtx-900000001\" name=\"precioVentaCtx\" type=\"hidden\" class=\"form-control\" data-price=\"173250\" style=\"border-color: rgb(210, 214, 222);\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Ganancia</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"  fa fa-bookmark-o\"></i></span><input id=\"porcent-900000001\" name=\"porcentCtx\" type=\"number\" min=\"0\" class=\"form-control\" data=\"900000001\" onchange=\"ValidateChangePercents(900000001)\" value=\"20\" style=\"border-color: rgb(210, 214, 222);\"></div></div></div><br><div class=\"row\" data-row-index=\"undefined\" id=\"contentProductQxt\"><input type=\"hidden\" name=\"precioUnitarioConGanancia\" id=\"precioUnitarioConGanancia-900000001\" value=\"207900\" style=\"border-color: rgb(210, 214, 222);\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Tiempo Entrega</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-history\"></i></span><select name=\"timeDelivery\" id=\"timeDelivery-900000001\" data=\"900000001\" class=\"form-control\" style=\"border-color: rgb(210, 214, 222); border-style: solid;\"><option value=\"--Tiempo de Entrega--\"> --Tiempo de Entrega-- </option><option value=\"5\">5 dÃ­as</option><option value=\"15\">15 dÃ­as</option><option value=\"30\">30 dÃ­as</option><option value=\"45\">45 dÃ­as</option><option value=\"60\">60 dÃ­as</option><option value=\"90\">90 dÃ­as</option><option value=\"120\">120 dÃ­as</option><option value=\"150\">150 dÃ­as</option><option value=\"180\">180 dÃ­as</option></select></div></div><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Precio</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-dollar\"></i></span><input id=\"totalPriceCtx-900000001\" valor-modificable=\"$207,900\" undefined]=\"\" name=\"totalPriceCtx-900000001\" data-type=\"COP\" data-value=\"207900\" data-place=\"Nacional\" data-money=\"COP\" data-typeproduct=\"Servicio\" data-id=\"900000001\" data-proveedor=\"externo\" data-priceu=\"[object Undefined]\" type=\"text\" class=\"form-control\" disabled=\"\" lol=\"\" value=\"$207,900\" style=\"border-color: rgb(210, 214, 222);\"></div></div></div></div><div class=\"box-footer\"><button type=\"button\" onclick=\"DeleteProduct(900000001)\" class=\"btn btn-danger btn-xs-block\">Cancelar</button></div></div></form>', 1),
(21, 1, 'Consumibles', 'aceite', '76', '76', '876', 7, 7, 'USD', '768000', '240,00', 'Angola', '<p>sdsfdsfsdfsdf</p>', '20', 1, 'proveedor', 3, '5 dias, salvo venta previa', 'Internacional', 1, '0', ' <div class=\"box-header with-border\"><h3 class=\"box-title \" id=\"item-value-1\" data-id-producto=\"id-1\"><b>2</b> - aceite <br><p class=\"text-center\"><b>876</b></p><p></p></h3></div><form class=\"form-horizontal\"><div class=\"box-body\"><input type=\"hidden\" id=\"product-id\" value=\"0\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"priceUnitario-1\" value=\"768000\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"priceUnitarioCOP-1\" value=\"768000\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"precioColombiano-1\" value=\"768000\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"type-1\" value=\"Internacional\" style=\"border-color: rgb(210, 214, 222);\"><div class=\"row\"><div class=\"col-sm-12\"><div style=\"border-radius:5px;margin:5px;\" class=\" bg-null text-center\"> <input type=\"checkbox\" name=\"checkbox\" style=\"margin-top: 5px; border-color: rgb(210, 214, 222);\" class=\"hidden checked\" id=\"IVA-1\" data-iva=\"1\" disabled=\"\" onclick=\"OnClickIVA(1)\" checked=\"\" data-check=\"true\" 2=\"\" <=\"\" div=\"\"></div></div><div class=\"row\" data-row-index=\"0\" id=\"content-1\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Cantidad</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-cube\"></i></span><input id=\"quantityCtx-1\" name=\"quantityCtx\" type=\"number\" min=\"0\" onchange=\"return CountquantityCtx(1)\" class=\"form-control\" value=\"1\" style=\"border-color: rgb(210, 214, 222);\"></div></div><input id=\"unitePriceCtx-1\" name=\"unitePriceCtx\" type=\"hidden\" class=\"form-control\" data-price=\"768000\" value=\"100\" style=\"border-color: rgb(210, 214, 222);\"><input id=\"precioOriginalCtx-1\" name=\"precioOriginalCtx\" type=\"hidden\" class=\"form-control\" data-price=\"240\" value=\"100\" style=\"border-color: rgb(210, 214, 222);\"><input id=\"precioVentaCtx-1\" name=\"precioVentaCtx\" type=\"hidden\" class=\"form-control\" data-price=\"240\" style=\"border-color: rgb(210, 214, 222);\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Ganancia</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"  fa fa-bookmark-o\"></i></span><input id=\"porcent-1\" name=\"porcentCtx\" type=\"number\" min=\"0\" class=\"form-control\" data=\"1\" onchange=\"ValidateChangePercents(1)\" value=\"20\" style=\"border-color: rgb(210, 214, 222);\"></div></div></div><br><div class=\"row\" data-row-index=\"0\" id=\"contentProductQxt\"><input type=\"hidden\" name=\"precioUnitarioConGanancia\" id=\"precioUnitarioConGanancia-1\" value=\"921600\" style=\"border-color: rgb(210, 214, 222);\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Tiempo Entrega</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-history\"></i></span><select name=\"timeDelivery\" id=\"timeDelivery-1\" data=\"1\" class=\"form-control\" style=\"border-color: rgb(210, 214, 222);\"><option value=\"5 dias, salvo venta previa\" selected=\"selected\">5 dias, salvo venta previa</option></select></div></div><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Precio</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-dollar\"></i></span><input id=\"totalPriceCtx-1\" valor-modificable=\"$921,600\" undefined]=\"\" name=\"totalPriceCtx-1\" data-type=\"COP\" data-value=\"921600\" data-place=\"Internacional\" data-money=\"USD\" data-typeproduct=\"Consumibles\" data-id=\"1\" data-proveedor=\"proveedor\" data-priceu=\"[object Undefined]\" type=\"text\" class=\"form-control\" disabled=\"\" lol=\"\" value=\"$921,600\" style=\"border-color: rgb(210, 214, 222);\"></div></div></div></div><div class=\"box-footer\"><button type=\"button\" onclick=\"DeleteProduct(1)\" class=\"btn btn-danger btn-xs-block\">Cancelar</button></div></div></form>', 1),
(22, 1, 'Servicio', 'aplicar aceite a maquinas', ' - ', ' - ', '180', 0, 0, 'COP', '173250', '50000', ' - ', '<p>aplicar aceite a la maquina</p>', '20', 1, ' - ', 3, ' - ', ' - ', 1, '0', ' <div class=\"box-header with-border\"><h3 class=\"box-title \" id=\"item-value-900000001\" data-id-producto=\"id-900000001\"><b>1</b> - Aplicar Aceite A Maquinas <br><p class=\"text-center\"><b>180 minutos</b></p><p></p></h3></div><form class=\"form-horizontal\"><div class=\"box-body\"><input type=\"hidden\" id=\"product-id\" value=\"undefined\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"priceUnitario-900000001\" value=\"173250\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"priceUnitarioCOP-900000001\" value=\"173250\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"precioColombiano-900000001\" value=\"173250\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"type-900000001\" value=\"Nacional\" style=\"border-color: rgb(210, 214, 222);\"><div class=\"row\"><div class=\"col-sm-12\"><div style=\"border-radius:5px;margin:5px;\" class=\" bg-null text-center\"> <input type=\"checkbox\" name=\"checkbox\" style=\"margin-top: 5px; border-color: rgb(210, 214, 222);\" class=\"hidden checked\" id=\"IVA-900000001\" data-iva=\"900000001\" disabled=\"\" onclick=\"OnClickIVA(900000001)\" checked=\"\" data-check=\"true\" 1=\"\" <=\"\" div=\"\"></div></div><div class=\"row\" data-row-index=\"undefined\" id=\"content-900000001\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Cantidad</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-cube\"></i></span><input id=\"quantityCtx-900000001\" name=\"quantityCtx\" type=\"number\" min=\"0\" onchange=\"return CountquantityCtx(900000001)\" class=\"form-control\" value=\"1\" style=\"border-color: rgb(210, 214, 222);\"></div></div><input id=\"unitePriceCtx-900000001\" name=\"unitePriceCtx\" type=\"hidden\" class=\"form-control\" data-price=\"173250\" value=\"100\" style=\"border-color: rgb(210, 214, 222);\"><input id=\"precioOriginalCtx-900000001\" name=\"precioOriginalCtx\" type=\"hidden\" class=\"form-control\" data-price=\"173250\" value=\"100\" style=\"border-color: rgb(210, 214, 222);\"><input id=\"precioVentaCtx-900000001\" name=\"precioVentaCtx\" type=\"hidden\" class=\"form-control\" data-price=\"173250\" style=\"border-color: rgb(210, 214, 222);\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Ganancia</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"  fa fa-bookmark-o\"></i></span><input id=\"porcent-900000001\" name=\"porcentCtx\" type=\"number\" min=\"0\" class=\"form-control\" data=\"900000001\" onchange=\"ValidateChangePercents(900000001)\" value=\"20\" style=\"border-color: rgb(210, 214, 222);\"></div></div></div><br><div class=\"row\" data-row-index=\"undefined\" id=\"contentProductQxt\"><input type=\"hidden\" name=\"precioUnitarioConGanancia\" id=\"precioUnitarioConGanancia-900000001\" value=\"17325034650\" style=\"border-color: rgb(210, 214, 222);\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Tiempo Entrega</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-history\"></i></span><select name=\"timeDelivery\" id=\"timeDelivery-900000001\" data=\"900000001\" class=\"form-control\" style=\"border-color: rgb(210, 214, 222);\"><option value=\"--Tiempo de Entrega--\"> --Tiempo de Entrega-- </option><option value=\"5\">5 dÃ­as</option><option value=\"15\">15 dÃ­as</option><option value=\"30\">30 dÃ­as</option><option value=\"45\">45 dÃ­as</option><option value=\"60\">60 dÃ­as</option><option value=\"90\">90 dÃ­as</option><option value=\"120\">120 dÃ­as</option><option value=\"150\">150 dÃ­as</option><option value=\"180\">180 dÃ­as</option></select></div></div><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Precio</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-dollar\"></i></span><input id=\"totalPriceCtx-900000001\" valor-modificable=\"$1,039,500\" undefined]=\"\" name=\"totalPriceCtx-900000001\" data-type=\"COP\" data-value=\"1039500\" data-place=\"Nacional\" data-money=\"COP\" data-typeproduct=\"Servicio\" data-id=\"900000001\" data-proveedor=\"externo\" data-priceu=\"[object Undefined]\" type=\"text\" class=\"form-control\" disabled=\"\" lol=\"\" value=\"$1,039,500\" style=\"border-color: rgb(210, 214, 222);\"></div></div></div></div><div class=\"box-footer\"><button type=\"button\" onclick=\"DeleteProduct(900000001)\" class=\"btn btn-danger btn-xs-block\">Cancelar</button></div></div></form>', 1),
(23, 2, 'Servicio', 'sdfds3', ' - ', ' - ', '233', 0, 0, 'COP', '3827', '233', ' - ', '<p>dfsdffsdfsd</p>', '20', 1, ' - ', 3, ' - ', ' - ', 1, '0', ' <div class=\"box-header with-border\"><h3 class=\"box-title \" id=\"item-value-900000002\" data-id-producto=\"id-900000002\"><b>2</b> - Sdfds3 <br><p class=\"text-center\"><b>233 minutos</b></p><p></p></h3></div><form class=\"form-horizontal\"><div class=\"box-body\"><input type=\"hidden\" id=\"product-id\" value=\"undefined\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"priceUnitario-900000002\" value=\"3827\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"priceUnitarioCOP-900000002\" value=\"3827\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"precioColombiano-900000002\" value=\"3827\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"type-900000002\" value=\"Nacional\" style=\"border-color: rgb(210, 214, 222);\"><div class=\"row\"><div class=\"col-sm-12\"><div style=\"border-radius:5px;margin:5px;\" class=\" bg-null text-center\"> <input type=\"checkbox\" name=\"checkbox\" style=\"margin-top: 5px; border-color: rgb(210, 214, 222);\" class=\"hidden checked\" id=\"IVA-900000002\" data-iva=\"900000002\" disabled=\"\" onclick=\"OnClickIVA(900000002)\" checked=\"\" data-check=\"true\" 2=\"\" <=\"\" div=\"\"></div></div><div class=\"row\" data-row-index=\"undefined\" id=\"content-900000002\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Cantidad</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-cube\"></i></span><input id=\"quantityCtx-900000002\" name=\"quantityCtx\" type=\"number\" min=\"0\" onchange=\"return CountquantityCtx(900000002)\" class=\"form-control\" value=\"1\" style=\"border-color: rgb(210, 214, 222);\"></div></div><input id=\"unitePriceCtx-900000002\" name=\"unitePriceCtx\" type=\"hidden\" class=\"form-control\" data-price=\"3827\" value=\"100\" style=\"border-color: rgb(210, 214, 222);\"><input id=\"precioOriginalCtx-900000002\" name=\"precioOriginalCtx\" type=\"hidden\" class=\"form-control\" data-price=\"3827\" value=\"100\" style=\"border-color: rgb(210, 214, 222);\"><input id=\"precioVentaCtx-900000002\" name=\"precioVentaCtx\" type=\"hidden\" class=\"form-control\" data-price=\"3827\" style=\"border-color: rgb(210, 214, 222);\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Ganancia</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"  fa fa-bookmark-o\"></i></span><input id=\"porcent-900000002\" name=\"porcentCtx\" type=\"number\" min=\"0\" class=\"form-control\" data=\"900000002\" onchange=\"ValidateChangePercents(900000002)\" value=\"20\" style=\"border-color: rgb(210, 214, 222);\"></div></div></div><br><div class=\"row\" data-row-index=\"undefined\" id=\"contentProductQxt\"><input type=\"hidden\" name=\"precioUnitarioConGanancia\" id=\"precioUnitarioConGanancia-900000002\" value=\"3827765.4000000000001\" style=\"border-color: rgb(210, 214, 222);\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Tiempo Entrega</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-history\"></i></span><select name=\"timeDelivery\" id=\"timeDelivery-900000002\" data=\"900000002\" class=\"form-control\" style=\"border-color: rgb(210, 214, 222); border-style: solid;\"><option value=\"--Tiempo de Entrega--\"> --Tiempo de Entrega-- </option><option value=\"5\">5 dÃ­as</option><option value=\"15\">15 dÃ­as</option><option value=\"30\">30 dÃ­as</option><option value=\"45\">45 dÃ­as</option><option value=\"60\">60 dÃ­as</option><option value=\"90\">90 dÃ­as</option><option value=\"120\">120 dÃ­as</option><option value=\"150\">150 dÃ­as</option><option value=\"180\">180 dÃ­as</option></select></div></div><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Precio</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-dollar\"></i></span><input id=\"totalPriceCtx-900000002\" valor-modificable=\"$4592\" undefined]=\"\" name=\"totalPriceCtx-900000002\" data-type=\"COP\" data-value=\"4592.4\" data-place=\"Nacional\" data-money=\"COP\" data-typeproduct=\"Servicio\" data-id=\"900000002\" data-proveedor=\"externo\" data-priceu=\"[object Undefined]\" type=\"text\" class=\"form-control\" disabled=\"\" lol=\"\" value=\"$4592\" style=\"border-color: rgb(210, 214, 222);\"></div></div></div></div><div class=\"box-footer\"><button type=\"button\" onclick=\"DeleteProduct(900000002)\" class=\"btn btn-danger btn-xs-block\">Cancelar</button></div></div></form>', 1);
INSERT INTO `elementsmainteince` (`id`, `idProduct`, `typeProduct`, `name`, `brand`, `reference`, `presentation`, `amount`, `minimumAmount`, `typeCurrency`, `priceUnit`, `priceUnitIndividual`, `country`, `description`, `percent`, `quantity`, `nameCompany`, `idQuotation`, `timeDelivery`, `namePlace`, `IVA`, `FLETE`, `nodo`, `isFromCompany`) VALUES
(24, 1, 'Consumibles', 'aceite', '76', '76', '876', 7, 7, 'USD', '768000', '240,00', 'Angola', '<p>sdsfdsfsdfsdf</p>', '20', 5, 'proveedor', 3, '5 dias, salvo venta previa', 'Internacional', 1, '0', ' <div class=\"box-header with-border\"><h3 class=\"box-title \" id=\"item-value-1\" data-id-producto=\"id-1\"><b>3</b> - aceite <br><p class=\"text-center\"><b>876</b></p><p></p></h3></div><form class=\"form-horizontal\"><div class=\"box-body\"><input type=\"hidden\" id=\"product-id\" value=\"0\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"priceUnitario-1\" value=\"768000\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"priceUnitarioCOP-1\" value=\"768000\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"precioColombiano-1\" value=\"768000\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"type-1\" value=\"Internacional\" style=\"border-color: rgb(210, 214, 222);\"><div class=\"row\"><div class=\"col-sm-12\"><div style=\"border-radius:5px;margin:5px;\" class=\" bg-null text-center\"> <input type=\"checkbox\" name=\"checkbox\" style=\"margin-top: 5px; border-color: rgb(210, 214, 222);\" class=\"hidden checked\" id=\"IVA-1\" data-iva=\"1\" disabled=\"\" onclick=\"OnClickIVA(1)\" checked=\"\" data-check=\"true\" 3=\"\" <=\"\" div=\"\"></div></div><div class=\"row\" data-row-index=\"0\" id=\"content-1\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Cantidad</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-cube\"></i></span><input id=\"quantityCtx-1\" name=\"quantityCtx\" type=\"number\" min=\"0\" onchange=\"return CountquantityCtx(1)\" class=\"form-control\" value=\"1\" style=\"border-color: rgb(210, 214, 222);\"></div></div><input id=\"unitePriceCtx-1\" name=\"unitePriceCtx\" type=\"hidden\" class=\"form-control\" data-price=\"768000\" value=\"100\" style=\"border-color: rgb(210, 214, 222);\"><input id=\"precioOriginalCtx-1\" name=\"precioOriginalCtx\" type=\"hidden\" class=\"form-control\" data-price=\"240\" value=\"100\" style=\"border-color: rgb(210, 214, 222);\"><input id=\"precioVentaCtx-1\" name=\"precioVentaCtx\" type=\"hidden\" class=\"form-control\" data-price=\"240\" style=\"border-color: rgb(210, 214, 222);\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Ganancia</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"  fa fa-bookmark-o\"></i></span><input id=\"porcent-1\" name=\"porcentCtx\" type=\"number\" min=\"0\" class=\"form-control\" data=\"1\" onchange=\"ValidateChangePercents(1)\" value=\"20\" style=\"border-color: rgb(210, 214, 222);\"></div></div></div><br><div class=\"row\" data-row-index=\"0\" id=\"contentProductQxt\"><input type=\"hidden\" name=\"precioUnitarioConGanancia\" id=\"precioUnitarioConGanancia-1\" value=\"768000153600\" style=\"border-color: rgb(210, 214, 222);\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Tiempo Entrega</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-history\"></i></span><select name=\"timeDelivery\" id=\"timeDelivery-1\" data=\"1\" class=\"form-control\" style=\"border-color: rgb(210, 214, 222);\"><option value=\"5 dias, salvo venta previa\" selected=\"selected\">5 dias, salvo venta previa</option></select></div></div><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Precio</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-dollar\"></i></span><input id=\"totalPriceCtx-1\" valor-modificable=\"$4,608,000\" undefined]=\"\" name=\"totalPriceCtx-1\" data-type=\"COP\" data-value=\"4608000\" data-place=\"Internacional\" data-money=\"USD\" data-typeproduct=\"Consumibles\" data-id=\"1\" data-proveedor=\"proveedor\" data-priceu=\"[object Undefined]\" type=\"text\" class=\"form-control\" disabled=\"\" lol=\"\" value=\"$4,608,000\" style=\"border-color: rgb(210, 214, 222);\"></div></div></div></div><div class=\"box-footer\"><button type=\"button\" onclick=\"DeleteProduct(1)\" class=\"btn btn-danger btn-xs-block\">Cancelar</button></div></div></form>', 1),
(25, 1, 'Servicio', 'aplicar aceite a maquinas', ' - ', ' - ', '180', 0, 0, 'COP', '173250', '50000', ' - ', '<p>aplicar aceite a la maquina</p>', '20', 1, ' - ', 4, ' - ', ' - ', 1, '0', ' <div class=\"box-header with-border\"><h3 class=\"box-title \" id=\"item-value-900000001\" data-id-producto=\"id-900000001\"><b>1</b> - Aplicar Aceite A Maquinas <br><p class=\"text-center\"><b>180 minutos</b></p><p></p></h3></div><form class=\"form-horizontal\"><div class=\"box-body\"><input type=\"hidden\" id=\"product-id\" value=\"undefined\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"priceUnitario-900000001\" value=\"173250\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"priceUnitarioCOP-900000001\" value=\"173250\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"precioColombiano-900000001\" value=\"173250\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"type-900000001\" value=\"Nacional\" style=\"border-color: rgb(210, 214, 222);\"><div class=\"row\"><div class=\"col-sm-12\"><div style=\"border-radius:5px;margin:5px;\" class=\" bg-null text-center\"> <input type=\"checkbox\" name=\"checkbox\" style=\"margin-top: 5px; border-color: rgb(210, 214, 222);\" class=\"hidden checked\" id=\"IVA-900000001\" data-iva=\"900000001\" disabled=\"\" onclick=\"OnClickIVA(900000001)\" checked=\"\" data-check=\"true\" 1=\"\" <=\"\" div=\"\"></div></div><div class=\"row\" data-row-index=\"undefined\" id=\"content-900000001\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Cantidad</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-cube\"></i></span><input id=\"quantityCtx-900000001\" name=\"quantityCtx\" type=\"number\" min=\"0\" onchange=\"return CountquantityCtx(900000001)\" class=\"form-control\" value=\"1\" style=\"border-color: rgb(210, 214, 222);\"></div></div><input id=\"unitePriceCtx-900000001\" name=\"unitePriceCtx\" type=\"hidden\" class=\"form-control\" data-price=\"173250\" value=\"100\" style=\"border-color: rgb(210, 214, 222);\"><input id=\"precioOriginalCtx-900000001\" name=\"precioOriginalCtx\" type=\"hidden\" class=\"form-control\" data-price=\"173250\" value=\"100\" style=\"border-color: rgb(210, 214, 222);\"><input id=\"precioVentaCtx-900000001\" name=\"precioVentaCtx\" type=\"hidden\" class=\"form-control\" data-price=\"173250\" style=\"border-color: rgb(210, 214, 222);\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Ganancia</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"  fa fa-bookmark-o\"></i></span><input id=\"porcent-900000001\" name=\"porcentCtx\" type=\"number\" min=\"0\" class=\"form-control\" data=\"900000001\" onchange=\"ValidateChangePercents(900000001)\" value=\"20\" style=\"border-color: rgb(210, 214, 222);\"></div></div></div><br><div class=\"row\" data-row-index=\"undefined\" id=\"contentProductQxt\"><input type=\"hidden\" name=\"precioUnitarioConGanancia\" id=\"precioUnitarioConGanancia-900000001\" value=\"17325034650\" style=\"border-color: rgb(210, 214, 222);\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Tiempo Entrega</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-history\"></i></span><select name=\"timeDelivery\" id=\"timeDelivery-900000001\" data=\"900000001\" class=\"form-control\" style=\"border-color: red; border-style: solid;\"><option value=\"--Tiempo de Entrega--\"> --Tiempo de Entrega-- </option><option value=\"5\">5 dÃ­as</option><option value=\"15\">15 dÃ­as</option><option value=\"30\">30 dÃ­as</option><option value=\"45\">45 dÃ­as</option><option value=\"60\">60 dÃ­as</option><option value=\"90\">90 dÃ­as</option><option value=\"120\">120 dÃ­as</option><option value=\"150\">150 dÃ­as</option><option value=\"180\">180 dÃ­as</option></select></div></div><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Precio</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-dollar\"></i></span><input id=\"totalPriceCtx-900000001\" valor-modificable=\"$207,900\" undefined]=\"\" name=\"totalPriceCtx-900000001\" data-type=\"COP\" data-value=\"207900\" data-place=\"Nacional\" data-money=\"COP\" data-typeproduct=\"Servicio\" data-id=\"900000001\" data-proveedor=\"externo\" data-priceu=\"[object Undefined]\" type=\"text\" class=\"form-control\" disabled=\"\" lol=\"\" value=\"$207,900\" style=\"border-color: rgb(210, 214, 222);\"></div></div></div></div><div class=\"box-footer\"><button type=\"button\" onclick=\"DeleteProduct(900000001)\" class=\"btn btn-danger btn-xs-block\">Cancelar</button></div></div></form>', 1),
(26, 1, 'Consumibles', 'aceite', '76', '76', '876', 7, 7, 'USD', '768000', '240,00', 'Angola', '<p>sdsfdsfsdfsdf</p>', '20', 1, 'proveedor', 4, '5 dias, salvo venta previa', 'Internacional', 1, '0', ' <div class=\"box-header with-border\"><h3 class=\"box-title \" id=\"item-value-1\" data-id-producto=\"id-1\"><b>2</b> - aceite <br><p class=\"text-center\"><b>876</b></p><p></p></h3></div><form class=\"form-horizontal\"><div class=\"box-body\"><input type=\"hidden\" id=\"product-id\" value=\"0\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"priceUnitario-1\" value=\"768000\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"priceUnitarioCOP-1\" value=\"768000\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"precioColombiano-1\" value=\"768000\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"type-1\" value=\"Internacional\" style=\"border-color: rgb(210, 214, 222);\"><div class=\"row\"><div class=\"col-sm-12\"><div style=\"border-radius:5px;margin:5px;\" class=\" bg-null text-center\"> <input type=\"checkbox\" name=\"checkbox\" style=\"margin-top: 5px; border-color: rgb(210, 214, 222);\" class=\"hidden checked\" id=\"IVA-1\" data-iva=\"1\" disabled=\"\" onclick=\"OnClickIVA(1)\" checked=\"\" data-check=\"true\" 2=\"\" <=\"\" div=\"\"></div></div><div class=\"row\" data-row-index=\"0\" id=\"content-1\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Cantidad</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-cube\"></i></span><input id=\"quantityCtx-1\" name=\"quantityCtx\" type=\"number\" min=\"0\" onchange=\"return CountquantityCtx(1)\" class=\"form-control\" value=\"1\" style=\"border-color: rgb(210, 214, 222);\"></div></div><input id=\"unitePriceCtx-1\" name=\"unitePriceCtx\" type=\"hidden\" class=\"form-control\" data-price=\"768000\" value=\"100\" style=\"border-color: rgb(210, 214, 222);\"><input id=\"precioOriginalCtx-1\" name=\"precioOriginalCtx\" type=\"hidden\" class=\"form-control\" data-price=\"240\" value=\"100\" style=\"border-color: rgb(210, 214, 222);\"><input id=\"precioVentaCtx-1\" name=\"precioVentaCtx\" type=\"hidden\" class=\"form-control\" data-price=\"240\" style=\"border-color: rgb(210, 214, 222);\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Ganancia</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"  fa fa-bookmark-o\"></i></span><input id=\"porcent-1\" name=\"porcentCtx\" type=\"number\" min=\"0\" class=\"form-control\" data=\"1\" onchange=\"ValidateChangePercents(1)\" value=\"20\" style=\"border-color: rgb(210, 214, 222);\"></div></div></div><br><div class=\"row\" data-row-index=\"0\" id=\"contentProductQxt\"><input type=\"hidden\" name=\"precioUnitarioConGanancia\" id=\"precioUnitarioConGanancia-1\" value=\"768000153600\" style=\"border-color: rgb(210, 214, 222);\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Tiempo Entrega</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-history\"></i></span><select name=\"timeDelivery\" id=\"timeDelivery-1\" data=\"1\" class=\"form-control\" style=\"border-color: rgb(210, 214, 222);\"><option value=\"5 dias, salvo venta previa\" selected=\"selected\">5 dias, salvo venta previa</option></select></div></div><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Precio</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-dollar\"></i></span><input id=\"totalPriceCtx-1\" valor-modificable=\"$921,600\" undefined]=\"\" name=\"totalPriceCtx-1\" data-type=\"COP\" data-value=\"921600\" data-place=\"Internacional\" data-money=\"USD\" data-typeproduct=\"Consumibles\" data-id=\"1\" data-proveedor=\"proveedor\" data-priceu=\"[object Undefined]\" type=\"text\" class=\"form-control\" disabled=\"\" lol=\"\" value=\"$921,600\" style=\"border-color: rgb(210, 214, 222);\"></div></div></div></div><div class=\"box-footer\"><button type=\"button\" onclick=\"DeleteProduct(1)\" class=\"btn btn-danger btn-xs-block\">Cancelar</button></div></div></form>', 1),
(27, 1, 'Servicio', 'aplicar aceite a maquinas', ' - ', ' - ', '180', 0, 0, 'COP', '173250', '50000', ' - ', '<p>aplicar aceite a la maquina</p>', '20', 1, ' - ', 4, ' - ', ' - ', 1, '0', ' <div class=\"box-header with-border\"><h3 class=\"box-title \" id=\"item-value-900000001\" data-id-producto=\"id-900000001\"><b>1</b> - Aplicar Aceite A Maquinas <br><p class=\"text-center\"><b>180 minutos</b></p><p></p></h3></div><form class=\"form-horizontal\"><div class=\"box-body\"><input type=\"hidden\" id=\"product-id\" value=\"undefined\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"priceUnitario-900000001\" value=\"173250\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"priceUnitarioCOP-900000001\" value=\"173250\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"precioColombiano-900000001\" value=\"173250\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"type-900000001\" value=\"Nacional\" style=\"border-color: rgb(210, 214, 222);\"><div class=\"row\"><div class=\"col-sm-12\"><div style=\"border-radius:5px;margin:5px;\" class=\" bg-null text-center\"> <input type=\"checkbox\" name=\"checkbox\" style=\"margin-top: 5px; border-color: rgb(210, 214, 222);\" class=\"hidden checked\" id=\"IVA-900000001\" data-iva=\"900000001\" disabled=\"\" onclick=\"OnClickIVA(900000001)\" checked=\"\" data-check=\"true\" 1=\"\" <=\"\" div=\"\"></div></div><div class=\"row\" data-row-index=\"undefined\" id=\"content-900000001\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Cantidad</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-cube\"></i></span><input id=\"quantityCtx-900000001\" name=\"quantityCtx\" type=\"number\" min=\"0\" onchange=\"return CountquantityCtx(900000001)\" class=\"form-control\" value=\"1\" style=\"border-color: rgb(210, 214, 222);\"></div></div><input id=\"unitePriceCtx-900000001\" name=\"unitePriceCtx\" type=\"hidden\" class=\"form-control\" data-price=\"173250\" value=\"100\" style=\"border-color: rgb(210, 214, 222);\"><input id=\"precioOriginalCtx-900000001\" name=\"precioOriginalCtx\" type=\"hidden\" class=\"form-control\" data-price=\"173250\" value=\"100\" style=\"border-color: rgb(210, 214, 222);\"><input id=\"precioVentaCtx-900000001\" name=\"precioVentaCtx\" type=\"hidden\" class=\"form-control\" data-price=\"173250\" style=\"border-color: rgb(210, 214, 222);\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Ganancia</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"  fa fa-bookmark-o\"></i></span><input id=\"porcent-900000001\" name=\"porcentCtx\" type=\"number\" min=\"0\" class=\"form-control\" data=\"900000001\" onchange=\"ValidateChangePercents(900000001)\" value=\"20\" style=\"border-color: rgb(210, 214, 222);\"></div></div></div><br><div class=\"row\" data-row-index=\"undefined\" id=\"contentProductQxt\"><input type=\"hidden\" name=\"precioUnitarioConGanancia\" id=\"precioUnitarioConGanancia-900000001\" value=\"207900\" style=\"border-color: rgb(210, 214, 222);\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Tiempo Entrega</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-history\"></i></span><select name=\"timeDelivery\" id=\"timeDelivery-900000001\" data=\"900000001\" class=\"form-control\" style=\"border-color: rgb(210, 214, 222); border-style: solid;\"><option value=\"--Tiempo de Entrega--\"> --Tiempo de Entrega-- </option><option value=\"5\">5 dÃ­as</option><option value=\"15\">15 dÃ­as</option><option value=\"30\">30 dÃ­as</option><option value=\"45\">45 dÃ­as</option><option value=\"60\">60 dÃ­as</option><option value=\"90\">90 dÃ­as</option><option value=\"120\">120 dÃ­as</option><option value=\"150\">150 dÃ­as</option><option value=\"180\">180 dÃ­as</option></select></div></div><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Precio</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-dollar\"></i></span><input id=\"totalPriceCtx-900000001\" valor-modificable=\"$207,900\" undefined]=\"\" name=\"totalPriceCtx-900000001\" data-type=\"COP\" data-value=\"207900\" data-place=\"Nacional\" data-money=\"COP\" data-typeproduct=\"Servicio\" data-id=\"900000001\" data-proveedor=\"externo\" data-priceu=\"[object Undefined]\" type=\"text\" class=\"form-control\" disabled=\"\" lol=\"\" value=\"$207,900\" style=\"border-color: rgb(210, 214, 222);\"></div></div></div></div><div class=\"box-footer\"><button type=\"button\" onclick=\"DeleteProduct(900000001)\" class=\"btn btn-danger btn-xs-block\">Cancelar</button></div></div></form>', 1),
(28, 1, 'Consumibles', 'aceite', '76', '76', '876', 7, 7, 'USD', '768000', '240,00', 'Angola', '<p>sdsfdsfsdfsdf</p>', '20', 1, 'proveedor', 4, '5 dias, salvo venta previa', 'Internacional', 1, '0', ' <div class=\"box-header with-border\"><h3 class=\"box-title \" id=\"item-value-1\" data-id-producto=\"id-1\"><b>2</b> - aceite <br><p class=\"text-center\"><b>876</b></p><p></p></h3></div><form class=\"form-horizontal\"><div class=\"box-body\"><input type=\"hidden\" id=\"product-id\" value=\"0\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"priceUnitario-1\" value=\"768000\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"priceUnitarioCOP-1\" value=\"768000\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"precioColombiano-1\" value=\"768000\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"type-1\" value=\"Internacional\" style=\"border-color: rgb(210, 214, 222);\"><div class=\"row\"><div class=\"col-sm-12\"><div style=\"border-radius:5px;margin:5px;\" class=\" bg-null text-center\"> <input type=\"checkbox\" name=\"checkbox\" style=\"margin-top: 5px; border-color: rgb(210, 214, 222);\" class=\"hidden checked\" id=\"IVA-1\" data-iva=\"1\" disabled=\"\" onclick=\"OnClickIVA(1)\" checked=\"\" data-check=\"true\" 2=\"\" <=\"\" div=\"\"></div></div><div class=\"row\" data-row-index=\"0\" id=\"content-1\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Cantidad</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-cube\"></i></span><input id=\"quantityCtx-1\" name=\"quantityCtx\" type=\"number\" min=\"0\" onchange=\"return CountquantityCtx(1)\" class=\"form-control\" value=\"1\" style=\"border-color: rgb(210, 214, 222);\"></div></div><input id=\"unitePriceCtx-1\" name=\"unitePriceCtx\" type=\"hidden\" class=\"form-control\" data-price=\"768000\" value=\"100\" style=\"border-color: rgb(210, 214, 222);\"><input id=\"precioOriginalCtx-1\" name=\"precioOriginalCtx\" type=\"hidden\" class=\"form-control\" data-price=\"240\" value=\"100\" style=\"border-color: rgb(210, 214, 222);\"><input id=\"precioVentaCtx-1\" name=\"precioVentaCtx\" type=\"hidden\" class=\"form-control\" data-price=\"240\" style=\"border-color: rgb(210, 214, 222);\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Ganancia</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"  fa fa-bookmark-o\"></i></span><input id=\"porcent-1\" name=\"porcentCtx\" type=\"number\" min=\"0\" class=\"form-control\" data=\"1\" onchange=\"ValidateChangePercents(1)\" value=\"20\" style=\"border-color: rgb(210, 214, 222);\"></div></div></div><br><div class=\"row\" data-row-index=\"0\" id=\"contentProductQxt\"><input type=\"hidden\" name=\"precioUnitarioConGanancia\" id=\"precioUnitarioConGanancia-1\" value=\"921600\" style=\"border-color: rgb(210, 214, 222);\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Tiempo Entrega</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-history\"></i></span><select name=\"timeDelivery\" id=\"timeDelivery-1\" data=\"1\" class=\"form-control\" style=\"border-color: rgb(210, 214, 222);\"><option value=\"5 dias, salvo venta previa\" selected=\"selected\">5 dias, salvo venta previa</option></select></div></div><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Precio</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-dollar\"></i></span><input id=\"totalPriceCtx-1\" valor-modificable=\"$921,600\" undefined]=\"\" name=\"totalPriceCtx-1\" data-type=\"COP\" data-value=\"921600\" data-place=\"Internacional\" data-money=\"USD\" data-typeproduct=\"Consumibles\" data-id=\"1\" data-proveedor=\"proveedor\" data-priceu=\"[object Undefined]\" type=\"text\" class=\"form-control\" disabled=\"\" lol=\"\" value=\"$921,600\" style=\"border-color: rgb(210, 214, 222);\"></div></div></div></div><div class=\"box-footer\"><button type=\"button\" onclick=\"DeleteProduct(1)\" class=\"btn btn-danger btn-xs-block\">Cancelar</button></div></div></form>', 1),
(29, 1, 'Servicio', 'aplicar aceite a maquinas', ' - ', ' - ', '180', 0, 0, 'COP', '173250', '50000', ' - ', '<p>aplicar aceite a la maquina</p>', '20', 1, ' - ', 4, ' - ', ' - ', 1, '0', ' <div class=\"box-header with-border\"><h3 class=\"box-title \" id=\"item-value-900000001\" data-id-producto=\"id-900000001\"><b>1</b> - Aplicar Aceite A Maquinas <br><p class=\"text-center\"><b>180 minutos</b></p><p></p></h3></div><form class=\"form-horizontal\"><div class=\"box-body\"><input type=\"hidden\" id=\"product-id\" value=\"undefined\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"priceUnitario-900000001\" value=\"173250\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"priceUnitarioCOP-900000001\" value=\"173250\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"precioColombiano-900000001\" value=\"173250\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"type-900000001\" value=\"Nacional\" style=\"border-color: rgb(210, 214, 222);\"><div class=\"row\"><div class=\"col-sm-12\"><div style=\"border-radius:5px;margin:5px;\" class=\" bg-null text-center\"> <input type=\"checkbox\" name=\"checkbox\" style=\"margin-top: 5px; border-color: rgb(210, 214, 222);\" class=\"hidden checked\" id=\"IVA-900000001\" data-iva=\"900000001\" disabled=\"\" onclick=\"OnClickIVA(900000001)\" checked=\"\" data-check=\"true\" 1=\"\" <=\"\" div=\"\"></div></div><div class=\"row\" data-row-index=\"undefined\" id=\"content-900000001\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Cantidad</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-cube\"></i></span><input id=\"quantityCtx-900000001\" name=\"quantityCtx\" type=\"number\" min=\"0\" onchange=\"return CountquantityCtx(900000001)\" class=\"form-control\" value=\"1\" style=\"border-color: rgb(210, 214, 222);\"></div></div><input id=\"unitePriceCtx-900000001\" name=\"unitePriceCtx\" type=\"hidden\" class=\"form-control\" data-price=\"173250\" value=\"100\" style=\"border-color: rgb(210, 214, 222);\"><input id=\"precioOriginalCtx-900000001\" name=\"precioOriginalCtx\" type=\"hidden\" class=\"form-control\" data-price=\"173250\" value=\"100\" style=\"border-color: rgb(210, 214, 222);\"><input id=\"precioVentaCtx-900000001\" name=\"precioVentaCtx\" type=\"hidden\" class=\"form-control\" data-price=\"173250\" style=\"border-color: rgb(210, 214, 222);\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Ganancia</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"  fa fa-bookmark-o\"></i></span><input id=\"porcent-900000001\" name=\"porcentCtx\" type=\"number\" min=\"0\" class=\"form-control\" data=\"900000001\" onchange=\"ValidateChangePercents(900000001)\" value=\"20\" style=\"border-color: rgb(210, 214, 222);\"></div></div></div><br><div class=\"row\" data-row-index=\"undefined\" id=\"contentProductQxt\"><input type=\"hidden\" name=\"precioUnitarioConGanancia\" id=\"precioUnitarioConGanancia-900000001\" value=\"17325034650\" style=\"border-color: rgb(210, 214, 222);\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Tiempo Entrega</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-history\"></i></span><select name=\"timeDelivery\" id=\"timeDelivery-900000001\" data=\"900000001\" class=\"form-control\" style=\"border-color: rgb(210, 214, 222);\"><option value=\"--Tiempo de Entrega--\"> --Tiempo de Entrega-- </option><option value=\"5\">5 dÃ­as</option><option value=\"15\">15 dÃ­as</option><option value=\"30\">30 dÃ­as</option><option value=\"45\">45 dÃ­as</option><option value=\"60\">60 dÃ­as</option><option value=\"90\">90 dÃ­as</option><option value=\"120\">120 dÃ­as</option><option value=\"150\">150 dÃ­as</option><option value=\"180\">180 dÃ­as</option></select></div></div><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Precio</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-dollar\"></i></span><input id=\"totalPriceCtx-900000001\" valor-modificable=\"$1,039,500\" undefined]=\"\" name=\"totalPriceCtx-900000001\" data-type=\"COP\" data-value=\"1039500\" data-place=\"Nacional\" data-money=\"COP\" data-typeproduct=\"Servicio\" data-id=\"900000001\" data-proveedor=\"externo\" data-priceu=\"[object Undefined]\" type=\"text\" class=\"form-control\" disabled=\"\" lol=\"\" value=\"$1,039,500\" style=\"border-color: rgb(210, 214, 222);\"></div></div></div></div><div class=\"box-footer\"><button type=\"button\" onclick=\"DeleteProduct(900000001)\" class=\"btn btn-danger btn-xs-block\">Cancelar</button></div></div></form>', 1),
(30, 2, 'Servicio', 'sdfds3', ' - ', ' - ', '233', 0, 0, 'COP', '3827', '233', ' - ', '<p>dfsdffsdfsd</p>', '20', 1, ' - ', 4, ' - ', ' - ', 1, '0', ' <div class=\"box-header with-border\"><h3 class=\"box-title \" id=\"item-value-900000002\" data-id-producto=\"id-900000002\"><b>2</b> - Sdfds3 <br><p class=\"text-center\"><b>233 minutos</b></p><p></p></h3></div><form class=\"form-horizontal\"><div class=\"box-body\"><input type=\"hidden\" id=\"product-id\" value=\"undefined\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"priceUnitario-900000002\" value=\"3827\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"priceUnitarioCOP-900000002\" value=\"3827\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"precioColombiano-900000002\" value=\"3827\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"type-900000002\" value=\"Nacional\" style=\"border-color: rgb(210, 214, 222);\"><div class=\"row\"><div class=\"col-sm-12\"><div style=\"border-radius:5px;margin:5px;\" class=\" bg-null text-center\"> <input type=\"checkbox\" name=\"checkbox\" style=\"margin-top: 5px; border-color: rgb(210, 214, 222);\" class=\"hidden checked\" id=\"IVA-900000002\" data-iva=\"900000002\" disabled=\"\" onclick=\"OnClickIVA(900000002)\" checked=\"\" data-check=\"true\" 2=\"\" <=\"\" div=\"\"></div></div><div class=\"row\" data-row-index=\"undefined\" id=\"content-900000002\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Cantidad</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-cube\"></i></span><input id=\"quantityCtx-900000002\" name=\"quantityCtx\" type=\"number\" min=\"0\" onchange=\"return CountquantityCtx(900000002)\" class=\"form-control\" value=\"1\" style=\"border-color: rgb(210, 214, 222);\"></div></div><input id=\"unitePriceCtx-900000002\" name=\"unitePriceCtx\" type=\"hidden\" class=\"form-control\" data-price=\"3827\" value=\"100\" style=\"border-color: rgb(210, 214, 222);\"><input id=\"precioOriginalCtx-900000002\" name=\"precioOriginalCtx\" type=\"hidden\" class=\"form-control\" data-price=\"3827\" value=\"100\" style=\"border-color: rgb(210, 214, 222);\"><input id=\"precioVentaCtx-900000002\" name=\"precioVentaCtx\" type=\"hidden\" class=\"form-control\" data-price=\"3827\" style=\"border-color: rgb(210, 214, 222);\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Ganancia</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"  fa fa-bookmark-o\"></i></span><input id=\"porcent-900000002\" name=\"porcentCtx\" type=\"number\" min=\"0\" class=\"form-control\" data=\"900000002\" onchange=\"ValidateChangePercents(900000002)\" value=\"20\" style=\"border-color: rgb(210, 214, 222);\"></div></div></div><br><div class=\"row\" data-row-index=\"undefined\" id=\"contentProductQxt\"><input type=\"hidden\" name=\"precioUnitarioConGanancia\" id=\"precioUnitarioConGanancia-900000002\" value=\"3827765.4000000000001\" style=\"border-color: rgb(210, 214, 222);\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Tiempo Entrega</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-history\"></i></span><select name=\"timeDelivery\" id=\"timeDelivery-900000002\" data=\"900000002\" class=\"form-control\" style=\"border-color: rgb(210, 214, 222); border-style: solid;\"><option value=\"--Tiempo de Entrega--\"> --Tiempo de Entrega-- </option><option value=\"5\">5 dÃ­as</option><option value=\"15\">15 dÃ­as</option><option value=\"30\">30 dÃ­as</option><option value=\"45\">45 dÃ­as</option><option value=\"60\">60 dÃ­as</option><option value=\"90\">90 dÃ­as</option><option value=\"120\">120 dÃ­as</option><option value=\"150\">150 dÃ­as</option><option value=\"180\">180 dÃ­as</option></select></div></div><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Precio</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-dollar\"></i></span><input id=\"totalPriceCtx-900000002\" valor-modificable=\"$4592\" undefined]=\"\" name=\"totalPriceCtx-900000002\" data-type=\"COP\" data-value=\"4592.4\" data-place=\"Nacional\" data-money=\"COP\" data-typeproduct=\"Servicio\" data-id=\"900000002\" data-proveedor=\"externo\" data-priceu=\"[object Undefined]\" type=\"text\" class=\"form-control\" disabled=\"\" lol=\"\" value=\"$4592\" style=\"border-color: rgb(210, 214, 222);\"></div></div></div></div><div class=\"box-footer\"><button type=\"button\" onclick=\"DeleteProduct(900000002)\" class=\"btn btn-danger btn-xs-block\">Cancelar</button></div></div></form>', 1),
(31, 1, 'Consumibles', 'aceite', '76', '76', '876', 7, 7, 'USD', '768000', '240,00', 'Angola', '<p>sdsfdsfsdfsdf</p>', '20', 5, 'proveedor', 4, '5 dias, salvo venta previa', 'Internacional', 1, '0', ' <div class=\"box-header with-border\"><h3 class=\"box-title \" id=\"item-value-1\" data-id-producto=\"id-1\"><b>3</b> - aceite <br><p class=\"text-center\"><b>876</b></p><p></p></h3></div><form class=\"form-horizontal\"><div class=\"box-body\"><input type=\"hidden\" id=\"product-id\" value=\"0\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"priceUnitario-1\" value=\"768000\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"priceUnitarioCOP-1\" value=\"768000\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"precioColombiano-1\" value=\"768000\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"type-1\" value=\"Internacional\" style=\"border-color: rgb(210, 214, 222);\"><div class=\"row\"><div class=\"col-sm-12\"><div style=\"border-radius:5px;margin:5px;\" class=\" bg-null text-center\"> <input type=\"checkbox\" name=\"checkbox\" style=\"margin-top: 5px; border-color: rgb(210, 214, 222);\" class=\"hidden checked\" id=\"IVA-1\" data-iva=\"1\" disabled=\"\" onclick=\"OnClickIVA(1)\" checked=\"\" data-check=\"true\" 3=\"\" <=\"\" div=\"\"></div></div><div class=\"row\" data-row-index=\"0\" id=\"content-1\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Cantidad</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-cube\"></i></span><input id=\"quantityCtx-1\" name=\"quantityCtx\" type=\"number\" min=\"0\" onchange=\"return CountquantityCtx(1)\" class=\"form-control\" value=\"1\" style=\"border-color: rgb(210, 214, 222);\"></div></div><input id=\"unitePriceCtx-1\" name=\"unitePriceCtx\" type=\"hidden\" class=\"form-control\" data-price=\"768000\" value=\"100\" style=\"border-color: rgb(210, 214, 222);\"><input id=\"precioOriginalCtx-1\" name=\"precioOriginalCtx\" type=\"hidden\" class=\"form-control\" data-price=\"240\" value=\"100\" style=\"border-color: rgb(210, 214, 222);\"><input id=\"precioVentaCtx-1\" name=\"precioVentaCtx\" type=\"hidden\" class=\"form-control\" data-price=\"240\" style=\"border-color: rgb(210, 214, 222);\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Ganancia</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"  fa fa-bookmark-o\"></i></span><input id=\"porcent-1\" name=\"porcentCtx\" type=\"number\" min=\"0\" class=\"form-control\" data=\"1\" onchange=\"ValidateChangePercents(1)\" value=\"20\" style=\"border-color: rgb(210, 214, 222);\"></div></div></div><br><div class=\"row\" data-row-index=\"0\" id=\"contentProductQxt\"><input type=\"hidden\" name=\"precioUnitarioConGanancia\" id=\"precioUnitarioConGanancia-1\" value=\"768000153600\" style=\"border-color: rgb(210, 214, 222);\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Tiempo Entrega</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-history\"></i></span><select name=\"timeDelivery\" id=\"timeDelivery-1\" data=\"1\" class=\"form-control\" style=\"border-color: rgb(210, 214, 222);\"><option value=\"5 dias, salvo venta previa\" selected=\"selected\">5 dias, salvo venta previa</option></select></div></div><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Precio</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-dollar\"></i></span><input id=\"totalPriceCtx-1\" valor-modificable=\"$4,608,000\" undefined]=\"\" name=\"totalPriceCtx-1\" data-type=\"COP\" data-value=\"4608000\" data-place=\"Internacional\" data-money=\"USD\" data-typeproduct=\"Consumibles\" data-id=\"1\" data-proveedor=\"proveedor\" data-priceu=\"[object Undefined]\" type=\"text\" class=\"form-control\" disabled=\"\" lol=\"\" value=\"$4,608,000\" style=\"border-color: rgb(210, 214, 222);\"></div></div></div></div><div class=\"box-footer\"><button type=\"button\" onclick=\"DeleteProduct(1)\" class=\"btn btn-danger btn-xs-block\">Cancelar</button></div></div></form>', 1),
(32, 1, 'Servicio', 'aplicar aceite a maquinas', ' - ', ' - ', '180', 0, 0, 'COP', '173250', '50000', ' - ', '<p>aplicar aceite a la maquina</p>', '20', 1, ' - ', 5, ' - ', ' - ', 1, '0', ' <div class=\"box-header with-border\"><h3 class=\"box-title \" id=\"item-value-900000001\" data-id-producto=\"id-900000001\"><b>1</b> - Aplicar Aceite A Maquinas <br><p class=\"text-center\"><b>180 minutos</b></p><p></p></h3></div><form class=\"form-horizontal\"><div class=\"box-body\"><input type=\"hidden\" id=\"product-id\" value=\"undefined\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"priceUnitario-900000001\" value=\"173250\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"priceUnitarioCOP-900000001\" value=\"173250\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"precioColombiano-900000001\" value=\"173250\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"type-900000001\" value=\"Nacional\" style=\"border-color: rgb(210, 214, 222);\"><div class=\"row\"><div class=\"col-sm-12\"><div style=\"border-radius:5px;margin:5px;\" class=\" bg-null text-center\"> <input type=\"checkbox\" name=\"checkbox\" style=\"margin-top: 5px; border-color: rgb(210, 214, 222);\" class=\"hidden checked\" id=\"IVA-900000001\" data-iva=\"900000001\" disabled=\"\" onclick=\"OnClickIVA(900000001)\" checked=\"\" data-check=\"true\" 1=\"\" <=\"\" div=\"\"></div></div><div class=\"row\" data-row-index=\"undefined\" id=\"content-900000001\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Cantidad</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-cube\"></i></span><input id=\"quantityCtx-900000001\" name=\"quantityCtx\" type=\"number\" min=\"0\" onchange=\"return CountquantityCtx(900000001)\" class=\"form-control\" value=\"1\" style=\"border-color: rgb(210, 214, 222);\"></div></div><input id=\"unitePriceCtx-900000001\" name=\"unitePriceCtx\" type=\"hidden\" class=\"form-control\" data-price=\"173250\" value=\"100\" style=\"border-color: rgb(210, 214, 222);\"><input id=\"precioOriginalCtx-900000001\" name=\"precioOriginalCtx\" type=\"hidden\" class=\"form-control\" data-price=\"173250\" value=\"100\" style=\"border-color: rgb(210, 214, 222);\"><input id=\"precioVentaCtx-900000001\" name=\"precioVentaCtx\" type=\"hidden\" class=\"form-control\" data-price=\"173250\" style=\"border-color: rgb(210, 214, 222);\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Ganancia</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"  fa fa-bookmark-o\"></i></span><input id=\"porcent-900000001\" name=\"porcentCtx\" type=\"number\" min=\"0\" class=\"form-control\" data=\"900000001\" onchange=\"ValidateChangePercents(900000001)\" value=\"20\" style=\"border-color: rgb(210, 214, 222);\"></div></div></div><br><div class=\"row\" data-row-index=\"undefined\" id=\"contentProductQxt\"><input type=\"hidden\" name=\"precioUnitarioConGanancia\" id=\"precioUnitarioConGanancia-900000001\" value=\"17325034650\" style=\"border-color: rgb(210, 214, 222);\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Tiempo Entrega</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-history\"></i></span><select name=\"timeDelivery\" id=\"timeDelivery-900000001\" data=\"900000001\" class=\"form-control\" style=\"border-color: rgb(210, 214, 222); border-style: solid;\"><option value=\"--Tiempo de Entrega--\"> --Tiempo de Entrega-- </option><option value=\"5\">5 dÃ­as</option><option value=\"15\">15 dÃ­as</option><option value=\"30\">30 dÃ­as</option><option value=\"45\">45 dÃ­as</option><option value=\"60\">60 dÃ­as</option><option value=\"90\">90 dÃ­as</option><option value=\"120\">120 dÃ­as</option><option value=\"150\">150 dÃ­as</option><option value=\"180\">180 dÃ­as</option></select></div></div><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Precio</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-dollar\"></i></span><input id=\"totalPriceCtx-900000001\" valor-modificable=\"$207,900\" undefined]=\"\" name=\"totalPriceCtx-900000001\" data-type=\"COP\" data-value=\"207900\" data-place=\"Nacional\" data-money=\"COP\" data-typeproduct=\"Servicio\" data-id=\"900000001\" data-proveedor=\"externo\" data-priceu=\"[object Undefined]\" type=\"text\" class=\"form-control\" disabled=\"\" lol=\"\" value=\"$207,900\" style=\"border-color: rgb(210, 214, 222);\"></div></div></div></div><div class=\"box-footer\"><button type=\"button\" onclick=\"DeleteProduct(900000001)\" class=\"btn btn-danger btn-xs-block\">Cancelar</button></div></div></form>', 1),
(33, 1, 'Consumibles', 'aceite', '76', '76', '876', 7, 7, 'USD', '768000', '240,00', 'Angola', '<p>sdsfdsfsdfsdf</p>', '20', 1, 'proveedor', 5, '5 dias, salvo venta previa', 'Internacional', 1, '0', ' <div class=\"box-header with-border\"><h3 class=\"box-title \" id=\"item-value-1\" data-id-producto=\"id-1\"><b>2</b> - aceite <br><p class=\"text-center\"><b>876</b></p><p></p></h3></div><form class=\"form-horizontal\"><div class=\"box-body\"><input type=\"hidden\" id=\"product-id\" value=\"0\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"priceUnitario-1\" value=\"768000\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"priceUnitarioCOP-1\" value=\"768000\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"precioColombiano-1\" value=\"768000\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"type-1\" value=\"Internacional\" style=\"border-color: rgb(210, 214, 222);\"><div class=\"row\"><div class=\"col-sm-12\"><div style=\"border-radius:5px;margin:5px;\" class=\" bg-null text-center\"> <input type=\"checkbox\" name=\"checkbox\" style=\"margin-top: 5px; border-color: rgb(210, 214, 222);\" class=\"hidden checked\" id=\"IVA-1\" data-iva=\"1\" disabled=\"\" onclick=\"OnClickIVA(1)\" checked=\"\" data-check=\"true\" 2=\"\" <=\"\" div=\"\"></div></div><div class=\"row\" data-row-index=\"0\" id=\"content-1\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Cantidad</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-cube\"></i></span><input id=\"quantityCtx-1\" name=\"quantityCtx\" type=\"number\" min=\"0\" onchange=\"return CountquantityCtx(1)\" class=\"form-control\" value=\"1\" style=\"border-color: rgb(210, 214, 222);\"></div></div><input id=\"unitePriceCtx-1\" name=\"unitePriceCtx\" type=\"hidden\" class=\"form-control\" data-price=\"768000\" value=\"100\" style=\"border-color: rgb(210, 214, 222);\"><input id=\"precioOriginalCtx-1\" name=\"precioOriginalCtx\" type=\"hidden\" class=\"form-control\" data-price=\"240\" value=\"100\" style=\"border-color: rgb(210, 214, 222);\"><input id=\"precioVentaCtx-1\" name=\"precioVentaCtx\" type=\"hidden\" class=\"form-control\" data-price=\"240\" style=\"border-color: rgb(210, 214, 222);\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Ganancia</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"  fa fa-bookmark-o\"></i></span><input id=\"porcent-1\" name=\"porcentCtx\" type=\"number\" min=\"0\" class=\"form-control\" data=\"1\" onchange=\"ValidateChangePercents(1)\" value=\"20\" style=\"border-color: rgb(210, 214, 222);\"></div></div></div><br><div class=\"row\" data-row-index=\"0\" id=\"contentProductQxt\"><input type=\"hidden\" name=\"precioUnitarioConGanancia\" id=\"precioUnitarioConGanancia-1\" value=\"768000153600\" style=\"border-color: rgb(210, 214, 222);\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Tiempo Entrega</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-history\"></i></span><select name=\"timeDelivery\" id=\"timeDelivery-1\" data=\"1\" class=\"form-control\" style=\"border-color: rgb(210, 214, 222);\"><option value=\"5 dias, salvo venta previa\" selected=\"selected\">5 dias, salvo venta previa</option></select></div></div><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Precio</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-dollar\"></i></span><input id=\"totalPriceCtx-1\" valor-modificable=\"$921,600\" undefined]=\"\" name=\"totalPriceCtx-1\" data-type=\"COP\" data-value=\"921600\" data-place=\"Internacional\" data-money=\"USD\" data-typeproduct=\"Consumibles\" data-id=\"1\" data-proveedor=\"proveedor\" data-priceu=\"[object Undefined]\" type=\"text\" class=\"form-control\" disabled=\"\" lol=\"\" value=\"$921,600\" style=\"border-color: rgb(210, 214, 222);\"></div></div></div></div><div class=\"box-footer\"><button type=\"button\" onclick=\"DeleteProduct(1)\" class=\"btn btn-danger btn-xs-block\">Cancelar</button></div></div></form>', 1),
(34, 1, 'Servicio', 'aplicar aceite a maquinas', ' - ', ' - ', '180', 0, 0, 'COP', '173250', '50000', ' - ', '<p>aplicar aceite a la maquina</p>', '20', 1, ' - ', 5, ' - ', ' - ', 1, '0', ' <div class=\"box-header with-border\"><h3 class=\"box-title \" id=\"item-value-900000001\" data-id-producto=\"id-900000001\"><b>1</b> - Aplicar Aceite A Maquinas <br><p class=\"text-center\"><b>180 minutos</b></p><p></p></h3></div><form class=\"form-horizontal\"><div class=\"box-body\"><input type=\"hidden\" id=\"product-id\" value=\"undefined\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"priceUnitario-900000001\" value=\"173250\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"priceUnitarioCOP-900000001\" value=\"173250\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"precioColombiano-900000001\" value=\"173250\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"type-900000001\" value=\"Nacional\" style=\"border-color: rgb(210, 214, 222);\"><div class=\"row\"><div class=\"col-sm-12\"><div style=\"border-radius:5px;margin:5px;\" class=\" bg-null text-center\"> <input type=\"checkbox\" name=\"checkbox\" style=\"margin-top: 5px; border-color: rgb(210, 214, 222);\" class=\"hidden checked\" id=\"IVA-900000001\" data-iva=\"900000001\" disabled=\"\" onclick=\"OnClickIVA(900000001)\" checked=\"\" data-check=\"true\" 1=\"\" <=\"\" div=\"\"></div></div><div class=\"row\" data-row-index=\"undefined\" id=\"content-900000001\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Cantidad</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-cube\"></i></span><input id=\"quantityCtx-900000001\" name=\"quantityCtx\" type=\"number\" min=\"0\" onchange=\"return CountquantityCtx(900000001)\" class=\"form-control\" value=\"1\" style=\"border-color: rgb(210, 214, 222);\"></div></div><input id=\"unitePriceCtx-900000001\" name=\"unitePriceCtx\" type=\"hidden\" class=\"form-control\" data-price=\"173250\" value=\"100\" style=\"border-color: rgb(210, 214, 222);\"><input id=\"precioOriginalCtx-900000001\" name=\"precioOriginalCtx\" type=\"hidden\" class=\"form-control\" data-price=\"173250\" value=\"100\" style=\"border-color: rgb(210, 214, 222);\"><input id=\"precioVentaCtx-900000001\" name=\"precioVentaCtx\" type=\"hidden\" class=\"form-control\" data-price=\"173250\" style=\"border-color: rgb(210, 214, 222);\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Ganancia</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"  fa fa-bookmark-o\"></i></span><input id=\"porcent-900000001\" name=\"porcentCtx\" type=\"number\" min=\"0\" class=\"form-control\" data=\"900000001\" onchange=\"ValidateChangePercents(900000001)\" value=\"20\" style=\"border-color: rgb(210, 214, 222);\"></div></div></div><br><div class=\"row\" data-row-index=\"undefined\" id=\"contentProductQxt\"><input type=\"hidden\" name=\"precioUnitarioConGanancia\" id=\"precioUnitarioConGanancia-900000001\" value=\"207900\" style=\"border-color: rgb(210, 214, 222);\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Tiempo Entrega</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-history\"></i></span><select name=\"timeDelivery\" id=\"timeDelivery-900000001\" data=\"900000001\" class=\"form-control\" style=\"border-color: rgb(210, 214, 222); border-style: solid;\"><option value=\"--Tiempo de Entrega--\"> --Tiempo de Entrega-- </option><option value=\"5\">5 dÃ­as</option><option value=\"15\">15 dÃ­as</option><option value=\"30\">30 dÃ­as</option><option value=\"45\">45 dÃ­as</option><option value=\"60\">60 dÃ­as</option><option value=\"90\">90 dÃ­as</option><option value=\"120\">120 dÃ­as</option><option value=\"150\">150 dÃ­as</option><option value=\"180\">180 dÃ­as</option></select></div></div><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Precio</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-dollar\"></i></span><input id=\"totalPriceCtx-900000001\" valor-modificable=\"$207,900\" undefined]=\"\" name=\"totalPriceCtx-900000001\" data-type=\"COP\" data-value=\"207900\" data-place=\"Nacional\" data-money=\"COP\" data-typeproduct=\"Servicio\" data-id=\"900000001\" data-proveedor=\"externo\" data-priceu=\"[object Undefined]\" type=\"text\" class=\"form-control\" disabled=\"\" lol=\"\" value=\"$207,900\" style=\"border-color: rgb(210, 214, 222);\"></div></div></div></div><div class=\"box-footer\"><button type=\"button\" onclick=\"DeleteProduct(900000001)\" class=\"btn btn-danger btn-xs-block\">Cancelar</button></div></div></form>', 1);
INSERT INTO `elementsmainteince` (`id`, `idProduct`, `typeProduct`, `name`, `brand`, `reference`, `presentation`, `amount`, `minimumAmount`, `typeCurrency`, `priceUnit`, `priceUnitIndividual`, `country`, `description`, `percent`, `quantity`, `nameCompany`, `idQuotation`, `timeDelivery`, `namePlace`, `IVA`, `FLETE`, `nodo`, `isFromCompany`) VALUES
(35, 1, 'Consumibles', 'aceite', '76', '76', '876', 7, 7, 'USD', '768000', '240,00', 'Angola', '<p>sdsfdsfsdfsdf</p>', '20', 1, 'proveedor', 5, '5 dias, salvo venta previa', 'Internacional', 1, '0', ' <div class=\"box-header with-border\"><h3 class=\"box-title \" id=\"item-value-1\" data-id-producto=\"id-1\"><b>2</b> - aceite <br><p class=\"text-center\"><b>876</b></p><p></p></h3></div><form class=\"form-horizontal\"><div class=\"box-body\"><input type=\"hidden\" id=\"product-id\" value=\"0\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"priceUnitario-1\" value=\"768000\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"priceUnitarioCOP-1\" value=\"768000\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"precioColombiano-1\" value=\"768000\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"type-1\" value=\"Internacional\" style=\"border-color: rgb(210, 214, 222);\"><div class=\"row\"><div class=\"col-sm-12\"><div style=\"border-radius:5px;margin:5px;\" class=\" bg-null text-center\"> <input type=\"checkbox\" name=\"checkbox\" style=\"margin-top: 5px; border-color: rgb(210, 214, 222);\" class=\"hidden checked\" id=\"IVA-1\" data-iva=\"1\" disabled=\"\" onclick=\"OnClickIVA(1)\" checked=\"\" data-check=\"true\" 2=\"\" <=\"\" div=\"\"></div></div><div class=\"row\" data-row-index=\"0\" id=\"content-1\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Cantidad</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-cube\"></i></span><input id=\"quantityCtx-1\" name=\"quantityCtx\" type=\"number\" min=\"0\" onchange=\"return CountquantityCtx(1)\" class=\"form-control\" value=\"1\" style=\"border-color: rgb(210, 214, 222);\"></div></div><input id=\"unitePriceCtx-1\" name=\"unitePriceCtx\" type=\"hidden\" class=\"form-control\" data-price=\"768000\" value=\"100\" style=\"border-color: rgb(210, 214, 222);\"><input id=\"precioOriginalCtx-1\" name=\"precioOriginalCtx\" type=\"hidden\" class=\"form-control\" data-price=\"240\" value=\"100\" style=\"border-color: rgb(210, 214, 222);\"><input id=\"precioVentaCtx-1\" name=\"precioVentaCtx\" type=\"hidden\" class=\"form-control\" data-price=\"240\" style=\"border-color: rgb(210, 214, 222);\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Ganancia</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"  fa fa-bookmark-o\"></i></span><input id=\"porcent-1\" name=\"porcentCtx\" type=\"number\" min=\"0\" class=\"form-control\" data=\"1\" onchange=\"ValidateChangePercents(1)\" value=\"20\" style=\"border-color: rgb(210, 214, 222);\"></div></div></div><br><div class=\"row\" data-row-index=\"0\" id=\"contentProductQxt\"><input type=\"hidden\" name=\"precioUnitarioConGanancia\" id=\"precioUnitarioConGanancia-1\" value=\"921600\" style=\"border-color: rgb(210, 214, 222);\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Tiempo Entrega</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-history\"></i></span><select name=\"timeDelivery\" id=\"timeDelivery-1\" data=\"1\" class=\"form-control\" style=\"border-color: rgb(210, 214, 222);\"><option value=\"5 dias, salvo venta previa\" selected=\"selected\">5 dias, salvo venta previa</option></select></div></div><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Precio</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-dollar\"></i></span><input id=\"totalPriceCtx-1\" valor-modificable=\"$921,600\" undefined]=\"\" name=\"totalPriceCtx-1\" data-type=\"COP\" data-value=\"921600\" data-place=\"Internacional\" data-money=\"USD\" data-typeproduct=\"Consumibles\" data-id=\"1\" data-proveedor=\"proveedor\" data-priceu=\"[object Undefined]\" type=\"text\" class=\"form-control\" disabled=\"\" lol=\"\" value=\"$921,600\" style=\"border-color: rgb(210, 214, 222);\"></div></div></div></div><div class=\"box-footer\"><button type=\"button\" onclick=\"DeleteProduct(1)\" class=\"btn btn-danger btn-xs-block\">Cancelar</button></div></div></form>', 1),
(36, 1, 'Servicio', 'aplicar aceite a maquinas', ' - ', ' - ', '180', 0, 0, 'COP', '173250', '50000', ' - ', '<p>aplicar aceite a la maquina</p>', '20', 1, ' - ', 5, ' - ', ' - ', 1, '0', ' <div class=\"box-header with-border\"><h3 class=\"box-title \" id=\"item-value-900000001\" data-id-producto=\"id-900000001\"><b>1</b> - Aplicar Aceite A Maquinas <br><p class=\"text-center\"><b>180 minutos</b></p><p></p></h3></div><form class=\"form-horizontal\"><div class=\"box-body\"><input type=\"hidden\" id=\"product-id\" value=\"undefined\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"priceUnitario-900000001\" value=\"173250\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"priceUnitarioCOP-900000001\" value=\"173250\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"precioColombiano-900000001\" value=\"173250\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"type-900000001\" value=\"Nacional\" style=\"border-color: rgb(210, 214, 222);\"><div class=\"row\"><div class=\"col-sm-12\"><div style=\"border-radius:5px;margin:5px;\" class=\" bg-null text-center\"> <input type=\"checkbox\" name=\"checkbox\" style=\"margin-top: 5px; border-color: rgb(210, 214, 222);\" class=\"hidden checked\" id=\"IVA-900000001\" data-iva=\"900000001\" disabled=\"\" onclick=\"OnClickIVA(900000001)\" checked=\"\" data-check=\"true\" 1=\"\" <=\"\" div=\"\"></div></div><div class=\"row\" data-row-index=\"undefined\" id=\"content-900000001\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Cantidad</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-cube\"></i></span><input id=\"quantityCtx-900000001\" name=\"quantityCtx\" type=\"number\" min=\"0\" onchange=\"return CountquantityCtx(900000001)\" class=\"form-control\" value=\"1\" style=\"border-color: rgb(210, 214, 222);\"></div></div><input id=\"unitePriceCtx-900000001\" name=\"unitePriceCtx\" type=\"hidden\" class=\"form-control\" data-price=\"173250\" value=\"100\" style=\"border-color: rgb(210, 214, 222);\"><input id=\"precioOriginalCtx-900000001\" name=\"precioOriginalCtx\" type=\"hidden\" class=\"form-control\" data-price=\"173250\" value=\"100\" style=\"border-color: rgb(210, 214, 222);\"><input id=\"precioVentaCtx-900000001\" name=\"precioVentaCtx\" type=\"hidden\" class=\"form-control\" data-price=\"173250\" style=\"border-color: rgb(210, 214, 222);\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Ganancia</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"  fa fa-bookmark-o\"></i></span><input id=\"porcent-900000001\" name=\"porcentCtx\" type=\"number\" min=\"0\" class=\"form-control\" data=\"900000001\" onchange=\"ValidateChangePercents(900000001)\" value=\"20\" style=\"border-color: rgb(210, 214, 222);\"></div></div></div><br><div class=\"row\" data-row-index=\"undefined\" id=\"contentProductQxt\"><input type=\"hidden\" name=\"precioUnitarioConGanancia\" id=\"precioUnitarioConGanancia-900000001\" value=\"17325034650\" style=\"border-color: rgb(210, 214, 222);\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Tiempo Entrega</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-history\"></i></span><select name=\"timeDelivery\" id=\"timeDelivery-900000001\" data=\"900000001\" class=\"form-control\" style=\"border-color: rgb(210, 214, 222);\"><option value=\"--Tiempo de Entrega--\"> --Tiempo de Entrega-- </option><option value=\"5\">5 dÃ­as</option><option value=\"15\">15 dÃ­as</option><option value=\"30\">30 dÃ­as</option><option value=\"45\">45 dÃ­as</option><option value=\"60\">60 dÃ­as</option><option value=\"90\">90 dÃ­as</option><option value=\"120\">120 dÃ­as</option><option value=\"150\">150 dÃ­as</option><option value=\"180\">180 dÃ­as</option></select></div></div><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Precio</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-dollar\"></i></span><input id=\"totalPriceCtx-900000001\" valor-modificable=\"$1,039,500\" undefined]=\"\" name=\"totalPriceCtx-900000001\" data-type=\"COP\" data-value=\"1039500\" data-place=\"Nacional\" data-money=\"COP\" data-typeproduct=\"Servicio\" data-id=\"900000001\" data-proveedor=\"externo\" data-priceu=\"[object Undefined]\" type=\"text\" class=\"form-control\" disabled=\"\" lol=\"\" value=\"$1,039,500\" style=\"border-color: rgb(210, 214, 222);\"></div></div></div></div><div class=\"box-footer\"><button type=\"button\" onclick=\"DeleteProduct(900000001)\" class=\"btn btn-danger btn-xs-block\">Cancelar</button></div></div></form>', 1),
(37, 2, 'Servicio', 'sdfds3', ' - ', ' - ', '233', 0, 0, 'COP', '3827', '233', ' - ', '<p>dfsdffsdfsd</p>', '20', 1, ' - ', 5, ' - ', ' - ', 1, '0', ' <div class=\"box-header with-border\"><h3 class=\"box-title \" id=\"item-value-900000002\" data-id-producto=\"id-900000002\"><b>2</b> - Sdfds3 <br><p class=\"text-center\"><b>233 minutos</b></p><p></p></h3></div><form class=\"form-horizontal\"><div class=\"box-body\"><input type=\"hidden\" id=\"product-id\" value=\"undefined\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"priceUnitario-900000002\" value=\"3827\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"priceUnitarioCOP-900000002\" value=\"3827\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"precioColombiano-900000002\" value=\"3827\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"type-900000002\" value=\"Nacional\" style=\"border-color: rgb(210, 214, 222);\"><div class=\"row\"><div class=\"col-sm-12\"><div style=\"border-radius:5px;margin:5px;\" class=\" bg-null text-center\"> <input type=\"checkbox\" name=\"checkbox\" style=\"margin-top: 5px; border-color: rgb(210, 214, 222);\" class=\"hidden checked\" id=\"IVA-900000002\" data-iva=\"900000002\" disabled=\"\" onclick=\"OnClickIVA(900000002)\" checked=\"\" data-check=\"true\" 2=\"\" <=\"\" div=\"\"></div></div><div class=\"row\" data-row-index=\"undefined\" id=\"content-900000002\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Cantidad</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-cube\"></i></span><input id=\"quantityCtx-900000002\" name=\"quantityCtx\" type=\"number\" min=\"0\" onchange=\"return CountquantityCtx(900000002)\" class=\"form-control\" value=\"1\" style=\"border-color: rgb(210, 214, 222);\"></div></div><input id=\"unitePriceCtx-900000002\" name=\"unitePriceCtx\" type=\"hidden\" class=\"form-control\" data-price=\"3827\" value=\"100\" style=\"border-color: rgb(210, 214, 222);\"><input id=\"precioOriginalCtx-900000002\" name=\"precioOriginalCtx\" type=\"hidden\" class=\"form-control\" data-price=\"3827\" value=\"100\" style=\"border-color: rgb(210, 214, 222);\"><input id=\"precioVentaCtx-900000002\" name=\"precioVentaCtx\" type=\"hidden\" class=\"form-control\" data-price=\"3827\" style=\"border-color: rgb(210, 214, 222);\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Ganancia</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"  fa fa-bookmark-o\"></i></span><input id=\"porcent-900000002\" name=\"porcentCtx\" type=\"number\" min=\"0\" class=\"form-control\" data=\"900000002\" onchange=\"ValidateChangePercents(900000002)\" value=\"20\" style=\"border-color: rgb(210, 214, 222);\"></div></div></div><br><div class=\"row\" data-row-index=\"undefined\" id=\"contentProductQxt\"><input type=\"hidden\" name=\"precioUnitarioConGanancia\" id=\"precioUnitarioConGanancia-900000002\" value=\"3827765.4000000000001\" style=\"border-color: rgb(210, 214, 222);\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Tiempo Entrega</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-history\"></i></span><select name=\"timeDelivery\" id=\"timeDelivery-900000002\" data=\"900000002\" class=\"form-control\" style=\"border-color: rgb(210, 214, 222); border-style: solid;\"><option value=\"--Tiempo de Entrega--\"> --Tiempo de Entrega-- </option><option value=\"5\">5 dÃ­as</option><option value=\"15\">15 dÃ­as</option><option value=\"30\">30 dÃ­as</option><option value=\"45\">45 dÃ­as</option><option value=\"60\">60 dÃ­as</option><option value=\"90\">90 dÃ­as</option><option value=\"120\">120 dÃ­as</option><option value=\"150\">150 dÃ­as</option><option value=\"180\">180 dÃ­as</option></select></div></div><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Precio</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-dollar\"></i></span><input id=\"totalPriceCtx-900000002\" valor-modificable=\"$4592\" undefined]=\"\" name=\"totalPriceCtx-900000002\" data-type=\"COP\" data-value=\"4592.4\" data-place=\"Nacional\" data-money=\"COP\" data-typeproduct=\"Servicio\" data-id=\"900000002\" data-proveedor=\"externo\" data-priceu=\"[object Undefined]\" type=\"text\" class=\"form-control\" disabled=\"\" lol=\"\" value=\"$4592\" style=\"border-color: rgb(210, 214, 222);\"></div></div></div></div><div class=\"box-footer\"><button type=\"button\" onclick=\"DeleteProduct(900000002)\" class=\"btn btn-danger btn-xs-block\">Cancelar</button></div></div></form>', 1),
(38, 1, 'Consumibles', 'aceite', '76', '76', '876', 7, 7, 'USD', '768000', '240,00', 'Angola', '<p>sdsfdsfsdfsdf</p>', '20', 5, 'proveedor', 5, '5 dias, salvo venta previa', 'Internacional', 1, '0', ' <div class=\"box-header with-border\"><h3 class=\"box-title \" id=\"item-value-1\" data-id-producto=\"id-1\"><b>3</b> - aceite <br><p class=\"text-center\"><b>876</b></p><p></p></h3></div><form class=\"form-horizontal\"><div class=\"box-body\"><input type=\"hidden\" id=\"product-id\" value=\"0\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"priceUnitario-1\" value=\"768000\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"priceUnitarioCOP-1\" value=\"768000\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"precioColombiano-1\" value=\"768000\" style=\"border-color: rgb(210, 214, 222);\"><input type=\"hidden\" id=\"type-1\" value=\"Internacional\" style=\"border-color: rgb(210, 214, 222);\"><div class=\"row\"><div class=\"col-sm-12\"><div style=\"border-radius:5px;margin:5px;\" class=\" bg-null text-center\"> <input type=\"checkbox\" name=\"checkbox\" style=\"margin-top: 5px; border-color: rgb(210, 214, 222);\" class=\"hidden checked\" id=\"IVA-1\" data-iva=\"1\" disabled=\"\" onclick=\"OnClickIVA(1)\" checked=\"\" data-check=\"true\" 3=\"\" <=\"\" div=\"\"></div></div><div class=\"row\" data-row-index=\"0\" id=\"content-1\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Cantidad</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-cube\"></i></span><input id=\"quantityCtx-1\" name=\"quantityCtx\" type=\"number\" min=\"0\" onchange=\"return CountquantityCtx(1)\" class=\"form-control\" value=\"1\" style=\"border-color: rgb(210, 214, 222);\"></div></div><input id=\"unitePriceCtx-1\" name=\"unitePriceCtx\" type=\"hidden\" class=\"form-control\" data-price=\"768000\" value=\"100\" style=\"border-color: rgb(210, 214, 222);\"><input id=\"precioOriginalCtx-1\" name=\"precioOriginalCtx\" type=\"hidden\" class=\"form-control\" data-price=\"240\" value=\"100\" style=\"border-color: rgb(210, 214, 222);\"><input id=\"precioVentaCtx-1\" name=\"precioVentaCtx\" type=\"hidden\" class=\"form-control\" data-price=\"240\" style=\"border-color: rgb(210, 214, 222);\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Ganancia</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"  fa fa-bookmark-o\"></i></span><input id=\"porcent-1\" name=\"porcentCtx\" type=\"number\" min=\"0\" class=\"form-control\" data=\"1\" onchange=\"ValidateChangePercents(1)\" value=\"20\" style=\"border-color: rgb(210, 214, 222);\"></div></div></div><br><div class=\"row\" data-row-index=\"0\" id=\"contentProductQxt\"><input type=\"hidden\" name=\"precioUnitarioConGanancia\" id=\"precioUnitarioConGanancia-1\" value=\"768000153600\" style=\"border-color: rgb(210, 214, 222);\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Tiempo Entrega</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-history\"></i></span><select name=\"timeDelivery\" id=\"timeDelivery-1\" data=\"1\" class=\"form-control\" style=\"border-color: rgb(210, 214, 222);\"><option value=\"5 dias, salvo venta previa\" selected=\"selected\">5 dias, salvo venta previa</option></select></div></div><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Precio</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-dollar\"></i></span><input id=\"totalPriceCtx-1\" valor-modificable=\"$4,608,000\" undefined]=\"\" name=\"totalPriceCtx-1\" data-type=\"COP\" data-value=\"4608000\" data-place=\"Internacional\" data-money=\"USD\" data-typeproduct=\"Consumibles\" data-id=\"1\" data-proveedor=\"proveedor\" data-priceu=\"[object Undefined]\" type=\"text\" class=\"form-control\" disabled=\"\" lol=\"\" value=\"$4,608,000\" style=\"border-color: rgb(210, 214, 222);\"></div></div></div></div><div class=\"box-footer\"><button type=\"button\" onclick=\"DeleteProduct(1)\" class=\"btn btn-danger btn-xs-block\">Cancelar</button></div></div></form>', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `event`
--

CREATE TABLE `event` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `isAllow` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `event`
--

INSERT INTO `event` (`id`, `name`, `isAllow`) VALUES
(4, 'Correos de Fechas de Visitas', 1),
(5, 'correos de cotizaciones enviadas', 1),
(6, 'Correos de seguimiento de prospecto', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `historymodification`
--

CREATE TABLE `historymodification` (
  `id` int(11) NOT NULL,
  `idUser` int(11) NOT NULL,
  `modificationDate` datetime NOT NULL,
  `idQuotation` int(11) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `historymodification`
--

INSERT INTO `historymodification` (`id`, `idUser`, `modificationDate`, `idQuotation`, `description`) VALUES
(1, 1, '2019-09-11 11:36:46', 2, 'Se creo la cotizaciÃ³n'),
(2, 1, '2019-09-11 11:37:14', 3, 'Se creo la cotizaciÃ³n'),
(3, 1, '2019-09-22 14:22:11', 2, 'Ha cambiado el estado a Cancelada');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `historymodificationmainteince`
--

CREATE TABLE `historymodificationmainteince` (
  `id` int(11) NOT NULL,
  `idUser` int(11) NOT NULL,
  `modificationDate` datetime NOT NULL,
  `idQuotation` int(11) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `historymodificationmainteince`
--

INSERT INTO `historymodificationmainteince` (`id`, `idUser`, `modificationDate`, `idQuotation`, `description`) VALUES
(1, 1, '2019-09-11 18:56:21', 1, 'Se creo la cotizaciÃ³n'),
(2, 1, '2019-09-12 11:42:59', 2, 'Se creo la cotizaciÃ³n'),
(3, 1, '2019-09-12 11:44:52', 3, 'Se creo la cotizaciÃ³n'),
(4, 1, '2019-09-12 11:46:02', 4, 'Se creo la cotizaciÃ³n'),
(5, 1, '2019-09-12 11:47:05', 5, 'Se creo la cotizaciÃ³n');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `historytrm`
--

CREATE TABLE `historytrm` (
  `id` int(11) NOT NULL,
  `generation` date NOT NULL DEFAULT '0000-00-00',
  `date` date NOT NULL DEFAULT '0000-00-00',
  `money` varchar(50) NOT NULL DEFAULT '0',
  `typeMoney` varchar(3) NOT NULL DEFAULT '0',
  `mailSend` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `historytrm`
--

INSERT INTO `historytrm` (`id`, `generation`, `date`, `money`, `typeMoney`, `mailSend`) VALUES
(1, '2019-08-13', '2019-11-21', '3200', 'USD', 0),
(2, '2019-08-13', '2020-11-27', '3840', 'EUR', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `impost`
--

CREATE TABLE `impost` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `percent` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `impost`
--

INSERT INTO `impost` (`id`, `name`, `percent`) VALUES
(1, 'IVA', '19');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inventory`
--

CREATE TABLE `inventory` (
  `id` int(11) NOT NULL,
  `typeProduct` varchar(45) NOT NULL,
  `name` varchar(150) NOT NULL,
  `brand` varchar(45) NOT NULL,
  `reference` varchar(45) NOT NULL,
  `presentation` varchar(45) NOT NULL,
  `amount` int(11) NOT NULL,
  `minimumAmount` int(11) NOT NULL,
  `typeCurrency` varchar(45) NOT NULL,
  `priceUnit` varchar(50) NOT NULL,
  `country` varchar(50) NOT NULL,
  `nameCompany` varchar(50) NOT NULL,
  `namePlace` varchar(50) NOT NULL,
  `description` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish2_ci NOT NULL,
  `IVA` int(11) NOT NULL,
  `FLETE` varchar(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `inventory`
--

INSERT INTO `inventory` (`id`, `typeProduct`, `name`, `brand`, `reference`, `presentation`, `amount`, `minimumAmount`, `typeCurrency`, `priceUnit`, `country`, `nameCompany`, `namePlace`, `description`, `IVA`, `FLETE`) VALUES
(1, 'Consumibles', 'aceite', '76', '76', '876', 7, 7, 'USD', '240,00', 'Angola', 'proveedor', 'Internacional', '<p>sdsfdsfsdfsdf</p>\r\n', 1, '0'),
(2, 'Equipos', 'equipo', '76', '76', '876', 7, 7, 'USD', '25000', 'Angola', 'proveedor', 'Nacional', '<p>sdsfdsfsdfsdfdcsdcdcssd</p>\r\n', 1, '0'),
(3, 'Equipos', 'equipo test', 'fdsjk', 'jjkh', 'hjkhkj', 1, 1, 'USD', '5000', 'Angola', 'proveedor', 'Nacional', '<p>dsdsfdfsd</p>\r\n\r\n<p>fsd</p>\r\n\r\n<p>f</p>\r\n\r\n<p>sdf</p>\r\n\r\n<p>sd</p>\r\n\r\n<p>f</p>\r\n\r\n<p>sdf</p>\r\n\r\n<p>ds</p>\r\n\r\n<p>fsd</p>\r\n\r\n<p>f</p>\r\n\r\n<p>dsf</p>\r\n\r\n<p>ds</p>\r\n\r\n<p>f</p>\r\n\r\n<p>dsf</p>\r\n\r\n<p>ds</p>\r\n\r\n<p>f</p>\r\n\r\n<p>ds</p>\r\n\r\n<p>f</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n', 1, '0');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `licence`
--

CREATE TABLE `licence` (
  `id` int(11) NOT NULL,
  `initDate` date NOT NULL,
  `endDate` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `licence`
--

INSERT INTO `licence` (`id`, `initDate`, `endDate`) VALUES
(1, '2018-09-12', '2018-09-14'),
(3, '2018-09-12', '2019-09-19');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mailallows`
--

CREATE TABLE `mailallows` (
  `id` int(11) NOT NULL,
  `idUser` int(11) NOT NULL DEFAULT 0,
  `module` int(11) NOT NULL DEFAULT 0,
  `mail` varchar(200) NOT NULL DEFAULT '0',
  `isAllow` int(11) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `mailallows`
--

INSERT INTO `mailallows` (`id`, `idUser`, `module`, `mail`, `isAllow`) VALUES
(1, 1, 1, 'javier00vela@gmail.com', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `module`
--

CREATE TABLE `module` (
  `id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `subName` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `module`
--

INSERT INTO `module` (`id`, `name`, `subName`) VALUES
(1, 'Inventory', 'Modulo Inventario'),
(2, 'Users', 'Modulo Usuarios'),
(3, 'Company', 'Modulo Empresas'),
(4, 'GenerateQuotes', 'Modulo Cotizaciones Generadas'),
(5, 'Quotation', 'Modulo Generar Cotizaciones'),
(6, 'ReportHistorialPerson', 'Modulo Reportes Usuario'),
(7, 'ReportQuotation', 'Modulo Reporte Cotizaciones'),
(8, 'Serial', 'Modulo de Seriales'),
(9, 'Impost', 'Modulo Impuestos'),
(10, 'HistoryModifyQuotation', 'Modulo Cotizaciones Modificadas'),
(11, 'AccessHistory', 'Modulo Acceso Usuario'),
(12, 'Contacts', 'Modulo Contactos'),
(13, 'HistoryProducts', 'Modulo Historico Productos'),
(14, 'Visits', 'Modulo Visitas'),
(15, 'VisitsDescription', 'Modulo Descripciones'),
(16, 'TracingQuotation', 'Modulo Seguimiento Cotizaciones'),
(17, 'Prospect', 'Modulo Prospecto'),
(18, 'Modules', 'Modulo Permisos'),
(19, 'ModifyPrices', 'Modulo Modificar Precios'),
(20, 'Providers', 'Modulo de Proveedores'),
(21, 'Managers', 'Modulo de Configuraciones'),
(22, 'Clients', 'Modulo De Lista Clientes'),
(23, 'CalendarTRM', 'Modulo De TRM'),
(24, 'ConfigData', 'Modulo De permisos Cotizaciones'),
(25, 'Code', 'Modulo de Codigos '),
(26, 'Mails', 'Modulo de correos y notifiaciones'),
(27, 'Maintenance', 'Modulo de mantenimiento de equipos'),
(28, 'ListDiagnosis', 'Modulo de lista de diagnosis'),
(29, 'Diagnosis', 'modulo de diagnostico de un equipo'),
(30, 'ModulesActions', 'modulo de acciones de modulos'),
(31, 'MailsAllows', 'Modulo de Permisos de Correos'),
(32, 'MyCalendar', 'Modulo de Calendario '),
(33, 'Tutorials', 'Modulo De Tutoriales'),
(34, 'PricesMaintence', 'modulo de precios mantenimiento'),
(35, 'Viatics', 'Modulo de lista de viaticos'),
(36, 'ListViatics', 'modulo de precios viaticos'),
(37, 'ModifyEventsStatics', 'modulo de calendario'),
(38, 'ListQuotesMainteince', 'modulo de listas de cotizaciones de mantenimiento'),
(39, 'HistoryModifyQuotationMainteince', 'Modulo de historial de cotizaciones de mantenimiento'),
(40, 'BuyListQuotes', 'modulo de compras de cotiazvfdvfd'),
(41, 'BuyListQuotesCompany', 'referger');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `moduleaction`
--

CREATE TABLE `moduleaction` (
  `id` int(11) NOT NULL,
  `idModule` int(11) NOT NULL,
  `idRol` int(11) NOT NULL,
  `state` int(11) NOT NULL,
  `action` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `moduleaction`
--

INSERT INTO `moduleaction` (`id`, `idModule`, `idRol`, `state`, `action`) VALUES
(1, 1, 1, 1, 'insertar'),
(2, 1, 2, 1, 'insertar'),
(3, 1, 3, 1, 'insertar'),
(4, 1, 4, 0, 'insertar'),
(5, 1, 5, 1, 'insertar'),
(6, 1, 6, 1, 'insertar'),
(7, 1, 7, 1, 'insertar'),
(8, 1, 8, 1, 'insertar'),
(9, 1, 9, 1, 'insertar'),
(10, 1, 10, 1, 'insertar'),
(11, 1, 11, 1, 'insertar'),
(12, 1, 12, 1, 'insertar'),
(13, 1, 13, 1, 'insertar'),
(14, 1, 1, 1, 'insertar_excel'),
(15, 1, 2, 1, 'insertar_excel'),
(16, 1, 3, 1, 'insertar_excel'),
(17, 1, 4, 0, 'insertar_excel'),
(18, 1, 5, 1, 'insertar_excel'),
(19, 1, 6, 1, 'insertar_excel'),
(20, 1, 7, 1, 'insertar_excel'),
(21, 1, 8, 1, 'insertar_excel'),
(22, 1, 9, 1, 'insertar_excel'),
(23, 1, 10, 1, 'insertar_excel'),
(24, 1, 11, 1, 'insertar_excel'),
(25, 1, 12, 1, 'insertar_excel'),
(26, 1, 13, 1, 'insertar_excel'),
(27, 1, 1, 1, 'ver_historial_productos'),
(28, 1, 2, 1, 'ver_historial_productos'),
(29, 1, 3, 1, 'ver_historial_productos'),
(30, 1, 4, 0, 'ver_historial_productos'),
(31, 1, 5, 1, 'ver_historial_productos'),
(32, 1, 6, 1, 'ver_historial_productos'),
(33, 1, 7, 1, 'ver_historial_productos'),
(34, 1, 8, 1, 'ver_historial_productos'),
(35, 1, 9, 1, 'ver_historial_productos'),
(36, 1, 10, 1, 'ver_historial_productos'),
(37, 1, 11, 1, 'ver_historial_productos'),
(38, 1, 12, 1, 'ver_historial_productos'),
(39, 1, 13, 1, 'ver_historial_productos'),
(40, 1, 1, 1, 'ver_seriales'),
(41, 1, 2, 1, 'ver_seriales'),
(42, 1, 3, 1, 'ver_seriales'),
(43, 1, 4, 0, 'ver_seriales'),
(44, 1, 5, 1, 'ver_seriales'),
(45, 1, 6, 1, 'ver_seriales'),
(46, 1, 7, 1, 'ver_seriales'),
(47, 1, 8, 1, 'ver_seriales'),
(48, 1, 9, 1, 'ver_seriales'),
(49, 1, 10, 1, 'ver_seriales'),
(50, 1, 11, 1, 'ver_seriales'),
(51, 1, 12, 1, 'ver_seriales'),
(52, 1, 13, 1, 'ver_seriales'),
(53, 1, 1, 1, 'copiar_producto'),
(54, 1, 2, 1, 'copiar_producto'),
(55, 1, 3, 1, 'copiar_producto'),
(56, 1, 4, 0, 'copiar_producto'),
(57, 1, 5, 1, 'copiar_producto'),
(58, 1, 6, 1, 'copiar_producto'),
(59, 1, 7, 1, 'copiar_producto'),
(60, 1, 8, 1, 'copiar_producto'),
(61, 1, 9, 1, 'copiar_producto'),
(62, 1, 10, 1, 'copiar_producto'),
(63, 1, 11, 1, 'copiar_producto'),
(64, 1, 12, 1, 'copiar_producto'),
(65, 1, 13, 1, 'copiar_producto'),
(66, 1, 1, 1, 'editar_producto'),
(67, 1, 2, 1, 'editar_producto'),
(68, 1, 3, 1, 'editar_producto'),
(69, 1, 4, 0, 'editar_producto'),
(70, 1, 5, 1, 'editar_producto'),
(71, 1, 6, 1, 'editar_producto'),
(72, 1, 7, 1, 'editar_producto'),
(73, 1, 8, 1, 'editar_producto'),
(74, 1, 9, 1, 'editar_producto'),
(75, 1, 10, 1, 'editar_producto'),
(76, 1, 11, 1, 'editar_producto'),
(77, 1, 12, 1, 'editar_producto'),
(78, 1, 13, 1, 'editar_producto'),
(79, 1, 1, 1, 'eliminar_producto'),
(80, 1, 2, 1, 'eliminar_producto'),
(81, 1, 3, 1, 'eliminar_producto'),
(82, 1, 4, 0, 'eliminar_producto'),
(83, 1, 5, 1, 'eliminar_producto'),
(84, 1, 6, 1, 'eliminar_producto'),
(85, 1, 7, 1, 'eliminar_producto'),
(86, 1, 8, 1, 'eliminar_producto'),
(87, 1, 9, 1, 'eliminar_producto'),
(88, 1, 10, 1, 'eliminar_producto'),
(89, 1, 11, 1, 'eliminar_producto'),
(90, 1, 12, 1, 'eliminar_producto'),
(91, 1, 13, 1, 'eliminar_producto'),
(92, 2, 1, 1, 'insertar_usuario'),
(93, 2, 2, 1, 'insertar_usuario'),
(94, 2, 3, 1, 'insertar_usuario'),
(95, 2, 4, 1, 'insertar_usuario'),
(96, 2, 5, 1, 'insertar_usuario'),
(97, 2, 6, 1, 'insertar_usuario'),
(98, 2, 7, 1, 'insertar_usuario'),
(99, 2, 8, 1, 'insertar_usuario'),
(100, 2, 9, 1, 'insertar_usuario'),
(101, 2, 10, 1, 'insertar_usuario'),
(102, 2, 11, 1, 'insertar_usuario'),
(103, 2, 12, 1, 'insertar_usuario'),
(104, 2, 13, 1, 'insertar_usuario'),
(105, 2, 1, 1, 'editar_usuario'),
(106, 2, 2, 1, 'editar_usuario'),
(107, 2, 3, 1, 'editar_usuario'),
(108, 2, 4, 1, 'editar_usuario'),
(109, 2, 5, 1, 'editar_usuario'),
(110, 2, 6, 1, 'editar_usuario'),
(111, 2, 7, 1, 'editar_usuario'),
(112, 2, 8, 1, 'editar_usuario'),
(113, 2, 9, 1, 'editar_usuario'),
(114, 2, 10, 1, 'editar_usuario'),
(115, 2, 11, 1, 'editar_usuario'),
(116, 2, 12, 1, 'editar_usuario'),
(117, 2, 13, 1, 'editar_usuario'),
(118, 2, 1, 1, 'modificar_credenciales_usuario'),
(119, 2, 2, 1, 'modificar_credenciales_usuario'),
(120, 2, 3, 1, 'modificar_credenciales_usuario'),
(121, 2, 4, 1, 'modificar_credenciales_usuario'),
(122, 2, 5, 1, 'modificar_credenciales_usuario'),
(123, 2, 6, 1, 'modificar_credenciales_usuario'),
(124, 2, 7, 1, 'modificar_credenciales_usuario'),
(125, 2, 8, 1, 'modificar_credenciales_usuario'),
(126, 2, 9, 1, 'modificar_credenciales_usuario'),
(127, 2, 10, 1, 'modificar_credenciales_usuario'),
(128, 2, 11, 1, 'modificar_credenciales_usuario'),
(129, 2, 12, 1, 'modificar_credenciales_usuario'),
(130, 2, 13, 1, 'modificar_credenciales_usuario'),
(131, 2, 1, 1, 'historial_acceso'),
(132, 2, 2, 1, 'historial_acceso'),
(133, 2, 3, 1, 'historial_acceso'),
(134, 2, 4, 1, 'historial_acceso'),
(135, 2, 5, 1, 'historial_acceso'),
(136, 2, 6, 1, 'historial_acceso'),
(137, 2, 7, 1, 'historial_acceso'),
(138, 2, 8, 1, 'historial_acceso'),
(139, 2, 9, 1, 'historial_acceso'),
(140, 2, 10, 1, 'historial_acceso'),
(141, 2, 11, 1, 'historial_acceso'),
(142, 2, 12, 1, 'historial_acceso'),
(143, 2, 13, 1, 'historial_acceso'),
(144, 2, 1, 1, 'eliminar_usuario'),
(145, 2, 2, 1, 'eliminar_usuario'),
(146, 2, 3, 1, 'eliminar_usuario'),
(147, 2, 4, 1, 'eliminar_usuario'),
(148, 2, 5, 1, 'eliminar_usuario'),
(149, 2, 6, 1, 'eliminar_usuario'),
(150, 2, 7, 1, 'eliminar_usuario'),
(151, 2, 8, 1, 'eliminar_usuario'),
(152, 2, 9, 1, 'eliminar_usuario'),
(153, 2, 10, 1, 'eliminar_usuario'),
(154, 2, 11, 1, 'eliminar_usuario'),
(155, 2, 12, 1, 'eliminar_usuario'),
(156, 2, 13, 1, 'eliminar_usuario'),
(157, 3, 1, 1, 'agregar_empresa'),
(158, 3, 2, 1, 'agregar_empresa'),
(159, 3, 3, 1, 'agregar_empresa'),
(160, 3, 4, 1, 'agregar_empresa'),
(161, 3, 5, 1, 'agregar_empresa'),
(162, 3, 6, 1, 'agregar_empresa'),
(163, 3, 7, 1, 'agregar_empresa'),
(164, 3, 8, 1, 'agregar_empresa'),
(165, 3, 9, 1, 'agregar_empresa'),
(166, 3, 10, 1, 'agregar_empresa'),
(167, 3, 11, 1, 'agregar_empresa'),
(168, 3, 12, 1, 'agregar_empresa'),
(169, 3, 13, 1, 'agregar_empresa'),
(170, 3, 1, 1, 'agregar_empresa_excel'),
(171, 3, 2, 1, 'agregar_empresa_excel'),
(172, 3, 3, 1, 'agregar_empresa_excel'),
(173, 3, 4, 1, 'agregar_empresa_excel'),
(174, 3, 5, 1, 'agregar_empresa_excel'),
(175, 3, 6, 1, 'agregar_empresa_excel'),
(176, 3, 7, 1, 'agregar_empresa_excel'),
(177, 3, 8, 1, 'agregar_empresa_excel'),
(178, 3, 9, 1, 'agregar_empresa_excel'),
(179, 3, 10, 1, 'agregar_empresa_excel'),
(180, 3, 11, 1, 'agregar_empresa_excel'),
(181, 3, 12, 1, 'agregar_empresa_excel'),
(182, 3, 13, 1, 'agregar_empresa_excel'),
(183, 3, 1, 1, 'generar_excel'),
(184, 3, 2, 1, 'generar_excel'),
(185, 3, 3, 1, 'generar_excel'),
(186, 3, 4, 1, 'generar_excel'),
(187, 3, 5, 1, 'generar_excel'),
(188, 3, 6, 1, 'generar_excel'),
(189, 3, 7, 1, 'generar_excel'),
(190, 3, 8, 1, 'generar_excel'),
(191, 3, 9, 1, 'generar_excel'),
(192, 3, 10, 1, 'generar_excel'),
(193, 3, 11, 1, 'generar_excel'),
(194, 3, 12, 1, 'generar_excel'),
(195, 3, 13, 1, 'generar_excel'),
(196, 3, 1, 1, 'editar_empresa'),
(197, 3, 2, 1, 'editar_empresa'),
(198, 3, 3, 1, 'editar_empresa'),
(199, 3, 4, 1, 'editar_empresa'),
(200, 3, 5, 1, 'editar_empresa'),
(201, 3, 6, 1, 'editar_empresa'),
(202, 3, 7, 1, 'editar_empresa'),
(203, 3, 8, 1, 'editar_empresa'),
(204, 3, 9, 1, 'editar_empresa'),
(205, 3, 10, 1, 'editar_empresa'),
(206, 3, 11, 1, 'editar_empresa'),
(207, 3, 12, 1, 'editar_empresa'),
(208, 3, 13, 1, 'editar_empresa'),
(209, 3, 1, 1, 'ver_contactos'),
(210, 3, 2, 1, 'ver_contactos'),
(211, 3, 3, 1, 'ver_contactos'),
(212, 3, 4, 1, 'ver_contactos'),
(213, 3, 5, 1, 'ver_contactos'),
(214, 3, 6, 1, 'ver_contactos'),
(215, 3, 7, 1, 'ver_contactos'),
(216, 3, 8, 1, 'ver_contactos'),
(217, 3, 9, 1, 'ver_contactos'),
(218, 3, 10, 1, 'ver_contactos'),
(219, 3, 11, 1, 'ver_contactos'),
(220, 3, 12, 1, 'ver_contactos'),
(221, 3, 13, 1, 'ver_contactos'),
(222, 3, 1, 1, 'modificar_precios'),
(223, 3, 2, 1, 'modificar_precios'),
(224, 3, 3, 1, 'modificar_precios'),
(225, 3, 4, 1, 'modificar_precios'),
(226, 3, 5, 1, 'modificar_precios'),
(227, 3, 6, 1, 'modificar_precios'),
(228, 3, 7, 1, 'modificar_precios'),
(229, 3, 8, 1, 'modificar_precios'),
(230, 3, 9, 1, 'modificar_precios'),
(231, 3, 10, 1, 'modificar_precios'),
(232, 3, 11, 1, 'modificar_precios'),
(233, 3, 12, 1, 'modificar_precios'),
(234, 3, 13, 1, 'modificar_precios'),
(235, 4, 1, 1, 'agregar_cotizacion'),
(236, 4, 2, 1, 'agregar_cotizacion'),
(237, 4, 3, 1, 'agregar_cotizacion'),
(238, 4, 4, 1, 'agregar_cotizacion'),
(239, 4, 5, 1, 'agregar_cotizacion'),
(240, 4, 6, 1, 'agregar_cotizacion'),
(241, 4, 7, 1, 'agregar_cotizacion'),
(242, 4, 8, 1, 'agregar_cotizacion'),
(243, 4, 9, 1, 'agregar_cotizacion'),
(244, 4, 10, 1, 'agregar_cotizacion'),
(245, 4, 11, 1, 'agregar_cotizacion'),
(246, 4, 12, 1, 'agregar_cotizacion'),
(247, 4, 13, 1, 'agregar_cotizacion'),
(248, 4, 1, 1, 'editar_cotizacion'),
(249, 4, 2, 1, 'editar_cotizacion'),
(250, 4, 3, 1, 'editar_cotizacion'),
(251, 4, 4, 1, 'editar_cotizacion'),
(252, 4, 5, 1, 'editar_cotizacion'),
(253, 4, 6, 1, 'editar_cotizacion'),
(254, 4, 7, 1, 'editar_cotizacion'),
(255, 4, 8, 1, 'editar_cotizacion'),
(256, 4, 9, 1, 'editar_cotizacion'),
(257, 4, 10, 1, 'editar_cotizacion'),
(258, 4, 11, 1, 'editar_cotizacion'),
(259, 4, 12, 1, 'editar_cotizacion'),
(260, 4, 13, 1, 'editar_cotizacion'),
(261, 4, 1, 1, 'ver_cotizacion'),
(262, 4, 2, 1, 'ver_cotizacion'),
(263, 4, 3, 1, 'ver_cotizacion'),
(264, 4, 4, 1, 'ver_cotizacion'),
(265, 4, 5, 1, 'ver_cotizacion'),
(266, 4, 6, 1, 'ver_cotizacion'),
(267, 4, 7, 1, 'ver_cotizacion'),
(268, 4, 8, 1, 'ver_cotizacion'),
(269, 4, 9, 1, 'ver_cotizacion'),
(270, 4, 10, 1, 'ver_cotizacion'),
(271, 4, 11, 1, 'ver_cotizacion'),
(272, 4, 12, 1, 'ver_cotizacion'),
(273, 4, 13, 1, 'ver_cotizacion'),
(274, 4, 1, 1, 'historico_cotizacion'),
(275, 4, 2, 1, 'historico_cotizacion'),
(276, 4, 3, 1, 'historico_cotizacion'),
(277, 4, 4, 1, 'historico_cotizacion'),
(278, 4, 5, 1, 'historico_cotizacion'),
(279, 4, 6, 1, 'historico_cotizacion'),
(280, 4, 7, 1, 'historico_cotizacion'),
(281, 4, 8, 1, 'historico_cotizacion'),
(282, 4, 9, 1, 'historico_cotizacion'),
(283, 4, 10, 1, 'historico_cotizacion'),
(284, 4, 11, 1, 'historico_cotizacion'),
(285, 4, 12, 1, 'historico_cotizacion'),
(286, 4, 13, 1, 'historico_cotizacion'),
(287, 4, 1, 1, 'seguimiento_cotizacion'),
(288, 4, 2, 1, 'seguimiento_cotizacion'),
(289, 4, 3, 1, 'seguimiento_cotizacion'),
(290, 4, 4, 1, 'seguimiento_cotizacion'),
(291, 4, 5, 1, 'seguimiento_cotizacion'),
(292, 4, 6, 1, 'seguimiento_cotizacion'),
(293, 4, 7, 1, 'seguimiento_cotizacion'),
(294, 4, 8, 1, 'seguimiento_cotizacion'),
(295, 4, 9, 1, 'seguimiento_cotizacion'),
(296, 4, 10, 1, 'seguimiento_cotizacion'),
(297, 4, 11, 1, 'seguimiento_cotizacion'),
(298, 4, 12, 1, 'seguimiento_cotizacion'),
(299, 4, 13, 1, 'seguimiento_cotizacion'),
(300, 6, 1, 1, 'generar_excel_usuarios'),
(301, 6, 2, 1, 'generar_excel_usuarios'),
(302, 6, 3, 1, 'generar_excel_usuarios'),
(303, 6, 4, 1, 'generar_excel_usuarios'),
(304, 6, 5, 1, 'generar_excel_usuarios'),
(305, 6, 6, 1, 'generar_excel_usuarios'),
(306, 6, 7, 1, 'generar_excel_usuarios'),
(307, 6, 8, 1, 'generar_excel_usuarios'),
(308, 6, 9, 1, 'generar_excel_usuarios'),
(309, 6, 10, 1, 'generar_excel_usuarios'),
(310, 6, 11, 1, 'generar_excel_usuarios'),
(311, 6, 12, 1, 'generar_excel_usuarios'),
(312, 6, 13, 1, 'generar_excel_usuarios'),
(313, 6, 1, 1, 'ver_ip_usuario'),
(314, 6, 2, 1, 'ver_ip_usuario'),
(315, 6, 3, 1, 'ver_ip_usuario'),
(316, 6, 4, 1, 'ver_ip_usuario'),
(317, 6, 5, 1, 'ver_ip_usuario'),
(318, 6, 6, 1, 'ver_ip_usuario'),
(319, 6, 7, 1, 'ver_ip_usuario'),
(320, 6, 8, 1, 'ver_ip_usuario'),
(321, 6, 9, 1, 'ver_ip_usuario'),
(322, 6, 10, 1, 'ver_ip_usuario'),
(323, 6, 11, 1, 'ver_ip_usuario'),
(324, 6, 12, 1, 'ver_ip_usuario'),
(325, 6, 13, 1, 'ver_ip_usuario'),
(326, 7, 1, 1, 'generar_excel'),
(327, 7, 2, 1, 'generar_excel'),
(328, 7, 3, 1, 'generar_excel'),
(329, 7, 4, 1, 'generar_excel'),
(330, 7, 5, 1, 'generar_excel'),
(331, 7, 6, 1, 'generar_excel'),
(332, 7, 7, 1, 'generar_excel'),
(333, 7, 8, 1, 'generar_excel'),
(334, 7, 9, 1, 'generar_excel'),
(335, 7, 10, 1, 'generar_excel'),
(336, 7, 11, 1, 'generar_excel'),
(337, 7, 12, 1, 'generar_excel'),
(338, 7, 13, 1, 'generar_excel'),
(339, 8, 1, 1, 'agregar_serial_excel'),
(340, 8, 2, 1, 'agregar_serial_excel'),
(341, 8, 3, 1, 'agregar_serial_excel'),
(342, 8, 4, 1, 'agregar_serial_excel'),
(343, 8, 5, 1, 'agregar_serial_excel'),
(344, 8, 6, 1, 'agregar_serial_excel'),
(345, 8, 7, 1, 'agregar_serial_excel'),
(346, 8, 8, 1, 'agregar_serial_excel'),
(347, 8, 9, 1, 'agregar_serial_excel'),
(348, 8, 10, 1, 'agregar_serial_excel'),
(349, 8, 11, 1, 'agregar_serial_excel'),
(350, 8, 12, 1, 'agregar_serial_excel'),
(351, 8, 13, 1, 'agregar_serial_excel'),
(352, 9, 1, 1, 'agregar_costo'),
(353, 9, 2, 1, 'agregar_costo'),
(354, 9, 3, 1, 'agregar_costo'),
(355, 9, 4, 1, 'agregar_costo'),
(356, 9, 5, 1, 'agregar_costo'),
(357, 9, 6, 1, 'agregar_costo'),
(358, 9, 7, 1, 'agregar_costo'),
(359, 9, 8, 1, 'agregar_costo'),
(360, 9, 9, 1, 'agregar_costo'),
(361, 9, 10, 1, 'agregar_costo'),
(362, 9, 11, 1, 'agregar_costo'),
(363, 9, 12, 1, 'agregar_costo'),
(364, 9, 13, 1, 'agregar_costo'),
(365, 9, 1, 1, 'modificar_impuesto'),
(366, 9, 2, 1, 'modificar_impuesto'),
(367, 9, 3, 1, 'modificar_impuesto'),
(368, 9, 4, 1, 'modificar_impuesto'),
(369, 9, 5, 1, 'modificar_impuesto'),
(370, 9, 6, 1, 'modificar_impuesto'),
(371, 9, 7, 1, 'modificar_impuesto'),
(372, 9, 8, 1, 'modificar_impuesto'),
(373, 9, 9, 1, 'modificar_impuesto'),
(374, 9, 10, 1, 'modificar_impuesto'),
(375, 9, 11, 1, 'modificar_impuesto'),
(376, 9, 12, 1, 'modificar_impuesto'),
(377, 9, 13, 1, 'modificar_impuesto'),
(378, 9, 1, 1, 'eliminar_costo'),
(379, 9, 2, 1, 'eliminar_costo'),
(380, 9, 3, 1, 'eliminar_costo'),
(381, 9, 4, 1, 'eliminar_costo'),
(382, 9, 5, 1, 'eliminar_costo'),
(383, 9, 6, 1, 'eliminar_costo'),
(384, 9, 7, 1, 'eliminar_costo'),
(385, 9, 8, 1, 'eliminar_costo'),
(386, 9, 9, 1, 'eliminar_costo'),
(387, 9, 10, 1, 'eliminar_costo'),
(388, 9, 11, 1, 'eliminar_costo'),
(389, 9, 12, 1, 'eliminar_costo'),
(390, 9, 13, 1, 'eliminar_costo'),
(391, 12, 1, 1, 'agregar_contacto'),
(392, 12, 2, 1, 'agregar_contacto'),
(393, 12, 3, 1, 'agregar_contacto'),
(394, 12, 4, 1, 'agregar_contacto'),
(395, 12, 5, 1, 'agregar_contacto'),
(396, 12, 6, 1, 'agregar_contacto'),
(397, 12, 7, 1, 'agregar_contacto'),
(398, 12, 8, 1, 'agregar_contacto'),
(399, 12, 9, 1, 'agregar_contacto'),
(400, 12, 10, 1, 'agregar_contacto'),
(401, 12, 11, 1, 'agregar_contacto'),
(402, 12, 12, 1, 'agregar_contacto'),
(403, 12, 13, 1, 'agregar_contacto'),
(404, 12, 1, 1, 'generar_excel'),
(405, 12, 2, 1, 'generar_excel'),
(406, 12, 3, 1, 'generar_excel'),
(407, 12, 4, 1, 'generar_excel'),
(408, 12, 5, 1, 'generar_excel'),
(409, 12, 6, 1, 'generar_excel'),
(410, 12, 7, 1, 'generar_excel'),
(411, 12, 8, 1, 'generar_excel'),
(412, 12, 9, 1, 'generar_excel'),
(413, 12, 10, 1, 'generar_excel'),
(414, 12, 11, 1, 'generar_excel'),
(415, 12, 12, 1, 'generar_excel'),
(416, 12, 13, 1, 'generar_excel'),
(417, 12, 1, 1, 'agregar_excel'),
(418, 12, 2, 1, 'agregar_excel'),
(419, 12, 3, 1, 'agregar_excel'),
(420, 12, 4, 1, 'agregar_excel'),
(421, 12, 5, 1, 'agregar_excel'),
(422, 12, 6, 1, 'agregar_excel'),
(423, 12, 7, 1, 'agregar_excel'),
(424, 12, 8, 1, 'agregar_excel'),
(425, 12, 9, 1, 'agregar_excel'),
(426, 12, 10, 1, 'agregar_excel'),
(427, 12, 11, 1, 'agregar_excel'),
(428, 12, 12, 1, 'agregar_excel'),
(429, 12, 13, 1, 'agregar_excel'),
(430, 12, 1, 1, 'editar_contacto'),
(431, 12, 2, 1, 'editar_contacto'),
(432, 12, 3, 1, 'editar_contacto'),
(433, 12, 4, 1, 'editar_contacto'),
(434, 12, 5, 1, 'editar_contacto'),
(435, 12, 6, 1, 'editar_contacto'),
(436, 12, 7, 1, 'editar_contacto'),
(437, 12, 8, 1, 'editar_contacto'),
(438, 12, 9, 1, 'editar_contacto'),
(439, 12, 10, 1, 'editar_contacto'),
(440, 12, 11, 1, 'editar_contacto'),
(441, 12, 12, 1, 'editar_contacto'),
(442, 12, 13, 1, 'editar_contacto'),
(443, 12, 1, 1, 'eliminar_contacto'),
(444, 12, 2, 1, 'eliminar_contacto'),
(445, 12, 3, 1, 'eliminar_contacto'),
(446, 12, 4, 1, 'eliminar_contacto'),
(447, 12, 5, 1, 'eliminar_contacto'),
(448, 12, 6, 1, 'eliminar_contacto'),
(449, 12, 7, 1, 'eliminar_contacto'),
(450, 12, 8, 1, 'eliminar_contacto'),
(451, 12, 9, 1, 'eliminar_contacto'),
(452, 12, 10, 1, 'eliminar_contacto'),
(453, 12, 11, 1, 'eliminar_contacto'),
(454, 12, 12, 1, 'eliminar_contacto'),
(455, 12, 13, 1, 'eliminar_contacto'),
(456, 13, 1, 1, 'ver_cotizacion'),
(457, 13, 2, 1, 'ver_cotizacion'),
(458, 13, 3, 1, 'ver_cotizacion'),
(459, 13, 4, 1, 'ver_cotizacion'),
(460, 13, 5, 1, 'ver_cotizacion'),
(461, 13, 6, 1, 'ver_cotizacion'),
(462, 13, 7, 1, 'ver_cotizacion'),
(463, 13, 8, 1, 'ver_cotizacion'),
(464, 13, 9, 1, 'ver_cotizacion'),
(465, 13, 10, 1, 'ver_cotizacion'),
(466, 13, 11, 1, 'ver_cotizacion'),
(467, 13, 12, 1, 'ver_cotizacion'),
(468, 13, 13, 1, 'ver_cotizacion'),
(469, 14, 1, 1, 'agregar_visita'),
(470, 14, 2, 1, 'agregar_visita'),
(471, 14, 3, 1, 'agregar_visita'),
(472, 14, 4, 1, 'agregar_visita'),
(473, 14, 5, 1, 'agregar_visita'),
(474, 14, 6, 1, 'agregar_visita'),
(475, 14, 7, 1, 'agregar_visita'),
(476, 14, 8, 1, 'agregar_visita'),
(477, 14, 9, 1, 'agregar_visita'),
(478, 14, 10, 1, 'agregar_visita'),
(479, 14, 11, 1, 'agregar_visita'),
(480, 14, 12, 1, 'agregar_visita'),
(481, 14, 13, 1, 'agregar_visita'),
(482, 14, 1, 1, 'historico_visitas'),
(483, 14, 2, 1, 'historico_visitas'),
(484, 14, 3, 1, 'historico_visitas'),
(485, 14, 4, 1, 'historico_visitas'),
(486, 14, 5, 1, 'historico_visitas'),
(487, 14, 6, 1, 'historico_visitas'),
(488, 14, 7, 1, 'historico_visitas'),
(489, 14, 8, 1, 'historico_visitas'),
(490, 14, 9, 1, 'historico_visitas'),
(491, 14, 10, 1, 'historico_visitas'),
(492, 14, 11, 1, 'historico_visitas'),
(493, 14, 12, 1, 'historico_visitas'),
(494, 14, 13, 1, 'historico_visitas'),
(495, 15, 1, 1, 'agregar_descripcion'),
(496, 15, 2, 1, 'agregar_descripcion'),
(497, 15, 3, 1, 'agregar_descripcion'),
(498, 15, 4, 1, 'agregar_descripcion'),
(499, 15, 5, 1, 'agregar_descripcion'),
(500, 15, 6, 1, 'agregar_descripcion'),
(501, 15, 7, 1, 'agregar_descripcion'),
(502, 15, 8, 1, 'agregar_descripcion'),
(503, 15, 9, 1, 'agregar_descripcion'),
(504, 15, 10, 1, 'agregar_descripcion'),
(505, 15, 11, 1, 'agregar_descripcion'),
(506, 15, 12, 1, 'agregar_descripcion'),
(507, 15, 13, 1, 'agregar_descripcion'),
(508, 15, 1, 1, 'modificar_descripcion'),
(509, 15, 2, 1, 'modificar_descripcion'),
(510, 15, 3, 1, 'modificar_descripcion'),
(511, 15, 4, 1, 'modificar_descripcion'),
(512, 15, 5, 1, 'modificar_descripcion'),
(513, 15, 6, 1, 'modificar_descripcion'),
(514, 15, 7, 1, 'modificar_descripcion'),
(515, 15, 8, 1, 'modificar_descripcion'),
(516, 15, 9, 1, 'modificar_descripcion'),
(517, 15, 10, 1, 'modificar_descripcion'),
(518, 15, 11, 1, 'modificar_descripcion'),
(519, 15, 12, 1, 'modificar_descripcion'),
(520, 15, 13, 1, 'modificar_descripcion'),
(521, 16, 1, 1, 'agregar_seguimiento'),
(522, 16, 2, 1, 'agregar_seguimiento'),
(523, 16, 3, 1, 'agregar_seguimiento'),
(524, 16, 4, 1, 'agregar_seguimiento'),
(525, 16, 5, 1, 'agregar_seguimiento'),
(526, 16, 6, 1, 'agregar_seguimiento'),
(527, 16, 7, 1, 'agregar_seguimiento'),
(528, 16, 8, 1, 'agregar_seguimiento'),
(529, 16, 9, 1, 'agregar_seguimiento'),
(530, 16, 10, 1, 'agregar_seguimiento'),
(531, 16, 11, 1, 'agregar_seguimiento'),
(532, 16, 12, 1, 'agregar_seguimiento'),
(533, 16, 13, 1, 'agregar_seguimiento'),
(534, 16, 1, 1, 'modificar_seguimiento'),
(535, 16, 2, 1, 'modificar_seguimiento'),
(536, 16, 3, 1, 'modificar_seguimiento'),
(537, 16, 4, 1, 'modificar_seguimiento'),
(538, 16, 5, 1, 'modificar_seguimiento'),
(539, 16, 6, 1, 'modificar_seguimiento'),
(540, 16, 7, 1, 'modificar_seguimiento'),
(541, 16, 8, 1, 'modificar_seguimiento'),
(542, 16, 9, 1, 'modificar_seguimiento'),
(543, 16, 10, 1, 'modificar_seguimiento'),
(544, 16, 11, 1, 'modificar_seguimiento'),
(545, 16, 12, 1, 'modificar_seguimiento'),
(546, 16, 13, 1, 'modificar_seguimiento'),
(547, 17, 1, 1, 'agregar_prospecto'),
(548, 17, 2, 1, 'agregar_prospecto'),
(549, 17, 3, 1, 'agregar_prospecto'),
(550, 17, 4, 1, 'agregar_prospecto'),
(551, 17, 5, 1, 'agregar_prospecto'),
(552, 17, 6, 1, 'agregar_prospecto'),
(553, 17, 7, 1, 'agregar_prospecto'),
(554, 17, 8, 1, 'agregar_prospecto'),
(555, 17, 9, 1, 'agregar_prospecto'),
(556, 17, 10, 1, 'agregar_prospecto'),
(557, 17, 11, 1, 'agregar_prospecto'),
(558, 17, 12, 1, 'agregar_prospecto'),
(559, 17, 13, 1, 'agregar_prospecto'),
(560, 17, 1, 1, 'modificar_prospecto'),
(561, 17, 2, 1, 'modificar_prospecto'),
(562, 17, 3, 1, 'modificar_prospecto'),
(563, 17, 4, 1, 'modificar_prospecto'),
(564, 17, 5, 1, 'modificar_prospecto'),
(565, 17, 6, 1, 'modificar_prospecto'),
(566, 17, 7, 1, 'modificar_prospecto'),
(567, 17, 8, 1, 'modificar_prospecto'),
(568, 17, 9, 1, 'modificar_prospecto'),
(569, 17, 10, 1, 'modificar_prospecto'),
(570, 17, 11, 1, 'modificar_prospecto'),
(571, 17, 12, 1, 'modificar_prospecto'),
(572, 17, 13, 1, 'modificar_prospecto'),
(573, 17, 1, 1, 'eliminar_propecto'),
(574, 17, 2, 1, 'eliminar_propecto'),
(575, 17, 3, 1, 'eliminar_propecto'),
(576, 17, 4, 1, 'eliminar_propecto'),
(577, 17, 5, 1, 'eliminar_propecto'),
(578, 17, 6, 1, 'eliminar_propecto'),
(579, 17, 7, 1, 'eliminar_propecto'),
(580, 17, 8, 1, 'eliminar_propecto'),
(581, 17, 9, 1, 'eliminar_propecto'),
(582, 17, 10, 1, 'eliminar_propecto'),
(583, 17, 11, 1, 'eliminar_propecto'),
(584, 17, 12, 1, 'eliminar_propecto'),
(585, 17, 13, 1, 'eliminar_propecto'),
(586, 19, 1, 1, 'agregar_costo'),
(587, 19, 2, 1, 'agregar_costo'),
(588, 19, 3, 1, 'agregar_costo'),
(589, 19, 4, 1, 'agregar_costo'),
(590, 19, 5, 1, 'agregar_costo'),
(591, 19, 6, 1, 'agregar_costo'),
(592, 19, 7, 1, 'agregar_costo'),
(593, 19, 8, 1, 'agregar_costo'),
(594, 19, 9, 1, 'agregar_costo'),
(595, 19, 10, 1, 'agregar_costo'),
(596, 19, 11, 1, 'agregar_costo'),
(597, 19, 12, 1, 'agregar_costo'),
(598, 19, 13, 1, 'agregar_costo'),
(599, 19, 1, 1, 'modificar_costo'),
(600, 19, 2, 1, 'modificar_costo'),
(601, 19, 3, 1, 'modificar_costo'),
(602, 19, 4, 1, 'modificar_costo'),
(603, 19, 5, 1, 'modificar_costo'),
(604, 19, 6, 1, 'modificar_costo'),
(605, 19, 7, 1, 'modificar_costo'),
(606, 19, 8, 1, 'modificar_costo'),
(607, 19, 9, 1, 'modificar_costo'),
(608, 19, 10, 1, 'modificar_costo'),
(609, 19, 11, 1, 'modificar_costo'),
(610, 19, 12, 1, 'modificar_costo'),
(611, 19, 13, 1, 'modificar_costo'),
(612, 19, 1, 1, 'eliminar_costo'),
(613, 19, 2, 1, 'eliminar_costo'),
(614, 19, 3, 1, 'eliminar_costo'),
(615, 19, 4, 1, 'eliminar_costo'),
(616, 19, 5, 1, 'eliminar_costo'),
(617, 19, 6, 1, 'eliminar_costo'),
(618, 19, 7, 1, 'eliminar_costo'),
(619, 19, 8, 1, 'eliminar_costo'),
(620, 19, 9, 1, 'eliminar_costo'),
(621, 19, 10, 1, 'eliminar_costo'),
(622, 19, 11, 1, 'eliminar_costo'),
(623, 19, 12, 1, 'eliminar_costo'),
(624, 19, 13, 1, 'eliminar_costo'),
(625, 20, 1, 1, 'generar_excel'),
(626, 20, 2, 1, 'generar_excel'),
(627, 20, 3, 1, 'generar_excel'),
(628, 20, 4, 1, 'generar_excel'),
(629, 20, 5, 1, 'generar_excel'),
(630, 20, 6, 1, 'generar_excel'),
(631, 20, 7, 1, 'generar_excel'),
(632, 20, 8, 1, 'generar_excel'),
(633, 20, 9, 1, 'generar_excel'),
(634, 20, 10, 1, 'generar_excel'),
(635, 20, 11, 1, 'generar_excel'),
(636, 20, 12, 1, 'generar_excel'),
(637, 20, 13, 1, 'generar_excel'),
(638, 20, 1, 1, 'ver_lista_precios'),
(639, 20, 2, 1, 'ver_lista_precios'),
(640, 20, 3, 1, 'ver_lista_precios'),
(641, 20, 4, 1, 'ver_lista_precios'),
(642, 20, 5, 1, 'ver_lista_precios'),
(643, 20, 6, 1, 'ver_lista_precios'),
(644, 20, 7, 1, 'ver_lista_precios'),
(645, 20, 8, 1, 'ver_lista_precios'),
(646, 20, 9, 1, 'ver_lista_precios'),
(647, 20, 10, 1, 'ver_lista_precios'),
(648, 20, 11, 1, 'ver_lista_precios'),
(649, 20, 12, 1, 'ver_lista_precios'),
(650, 20, 13, 1, 'ver_lista_precios'),
(651, 20, 1, 1, 'modificar_precios'),
(652, 20, 2, 1, 'modificar_precios'),
(653, 20, 3, 1, 'modificar_precios'),
(654, 20, 4, 1, 'modificar_precios'),
(655, 20, 5, 1, 'modificar_precios'),
(656, 20, 6, 1, 'modificar_precios'),
(657, 20, 7, 1, 'modificar_precios'),
(658, 20, 8, 1, 'modificar_precios'),
(659, 20, 9, 1, 'modificar_precios'),
(660, 20, 10, 1, 'modificar_precios'),
(661, 20, 11, 1, 'modificar_precios'),
(662, 20, 12, 1, 'modificar_precios'),
(663, 20, 13, 1, 'modificar_precios'),
(664, 22, 1, 1, 'agregar_cliente'),
(665, 22, 2, 1, 'agregar_cliente'),
(666, 22, 3, 1, 'agregar_cliente'),
(667, 22, 4, 1, 'agregar_cliente'),
(668, 22, 5, 1, 'agregar_cliente'),
(669, 22, 6, 1, 'agregar_cliente'),
(670, 22, 7, 1, 'agregar_cliente'),
(671, 22, 8, 1, 'agregar_cliente'),
(672, 22, 9, 1, 'agregar_cliente'),
(673, 22, 10, 1, 'agregar_cliente'),
(674, 22, 11, 1, 'agregar_cliente'),
(675, 22, 12, 1, 'agregar_cliente'),
(676, 22, 13, 1, 'agregar_cliente'),
(677, 22, 1, 1, 'generar_excel'),
(678, 22, 2, 1, 'generar_excel'),
(679, 22, 3, 1, 'generar_excel'),
(680, 22, 4, 1, 'generar_excel'),
(681, 22, 5, 1, 'generar_excel'),
(682, 22, 6, 1, 'generar_excel'),
(683, 22, 7, 1, 'generar_excel'),
(684, 22, 8, 1, 'generar_excel'),
(685, 22, 9, 1, 'generar_excel'),
(686, 22, 10, 1, 'generar_excel'),
(687, 22, 11, 1, 'generar_excel'),
(688, 22, 12, 1, 'generar_excel'),
(689, 22, 13, 1, 'generar_excel'),
(690, 22, 1, 1, 'modificar_cliente'),
(691, 22, 2, 1, 'modificar_cliente'),
(692, 22, 3, 1, 'modificar_cliente'),
(693, 22, 4, 1, 'modificar_cliente'),
(694, 22, 5, 1, 'modificar_cliente'),
(695, 22, 6, 1, 'modificar_cliente'),
(696, 22, 7, 1, 'modificar_cliente'),
(697, 22, 8, 1, 'modificar_cliente'),
(698, 22, 9, 1, 'modificar_cliente'),
(699, 22, 10, 1, 'modificar_cliente'),
(700, 22, 11, 1, 'modificar_cliente'),
(701, 22, 12, 1, 'modificar_cliente'),
(702, 22, 13, 1, 'modificar_cliente'),
(703, 22, 1, 1, 'eliminar_cliente'),
(704, 22, 2, 1, 'eliminar_cliente'),
(705, 22, 3, 1, 'eliminar_cliente'),
(706, 22, 4, 1, 'eliminar_cliente'),
(707, 22, 5, 1, 'eliminar_cliente'),
(708, 22, 6, 1, 'eliminar_cliente'),
(709, 22, 7, 1, 'eliminar_cliente'),
(710, 22, 8, 1, 'eliminar_cliente'),
(711, 22, 9, 1, 'eliminar_cliente'),
(712, 22, 10, 1, 'eliminar_cliente'),
(713, 22, 11, 1, 'eliminar_cliente'),
(714, 22, 12, 1, 'eliminar_cliente'),
(715, 22, 13, 1, 'eliminar_cliente'),
(716, 23, 1, 1, 'modificar_trm'),
(717, 23, 2, 1, 'modificar_trm'),
(718, 23, 3, 1, 'modificar_trm'),
(719, 23, 4, 1, 'modificar_trm'),
(720, 23, 5, 1, 'modificar_trm'),
(721, 23, 6, 1, 'modificar_trm'),
(722, 23, 7, 1, 'modificar_trm'),
(723, 23, 8, 1, 'modificar_trm'),
(724, 23, 9, 1, 'modificar_trm'),
(725, 23, 10, 1, 'modificar_trm'),
(726, 23, 11, 1, 'modificar_trm'),
(727, 23, 12, 1, 'modificar_trm'),
(728, 23, 13, 1, 'modificar_trm'),
(729, 25, 1, 1, 'agregar_codigo'),
(730, 25, 2, 1, 'agregar_codigo'),
(731, 25, 3, 1, 'agregar_codigo'),
(732, 25, 4, 1, 'agregar_codigo'),
(733, 25, 5, 1, 'agregar_codigo'),
(734, 25, 6, 1, 'agregar_codigo'),
(735, 25, 7, 1, 'agregar_codigo'),
(736, 25, 8, 1, 'agregar_codigo'),
(737, 25, 9, 1, 'agregar_codigo'),
(738, 25, 10, 1, 'agregar_codigo'),
(739, 25, 11, 1, 'agregar_codigo'),
(740, 25, 12, 1, 'agregar_codigo'),
(741, 25, 13, 1, 'agregar_codigo'),
(742, 25, 1, 1, 'modificar_codigo'),
(743, 25, 2, 1, 'modificar_codigo'),
(744, 25, 3, 1, 'modificar_codigo'),
(745, 25, 4, 1, 'modificar_codigo'),
(746, 25, 5, 1, 'modificar_codigo'),
(747, 25, 6, 1, 'modificar_codigo'),
(748, 25, 7, 1, 'modificar_codigo'),
(749, 25, 8, 1, 'modificar_codigo'),
(750, 25, 9, 1, 'modificar_codigo'),
(751, 25, 10, 1, 'modificar_codigo'),
(752, 25, 11, 1, 'modificar_codigo'),
(753, 25, 12, 1, 'modificar_codigo'),
(754, 25, 13, 1, 'modificar_codigo'),
(755, 25, 1, 1, 'eliminar_codigos'),
(756, 25, 2, 1, 'eliminar_codigos'),
(757, 25, 3, 1, 'eliminar_codigos'),
(758, 25, 4, 1, 'eliminar_codigos'),
(759, 25, 5, 1, 'eliminar_codigos'),
(760, 25, 6, 1, 'eliminar_codigos'),
(761, 25, 7, 1, 'eliminar_codigos'),
(762, 25, 8, 1, 'eliminar_codigos'),
(763, 25, 9, 1, 'eliminar_codigos'),
(764, 25, 10, 1, 'eliminar_codigos'),
(765, 25, 11, 1, 'eliminar_codigos'),
(766, 25, 12, 1, 'eliminar_codigos'),
(767, 25, 13, 1, 'eliminar_codigos'),
(768, 27, 1, 1, 'generar_diagnostico'),
(769, 27, 2, 1, 'generar_diagnostico'),
(770, 27, 3, 1, 'generar_diagnostico'),
(771, 27, 4, 1, 'generar_diagnostico'),
(772, 27, 5, 1, 'generar_diagnostico'),
(773, 27, 6, 1, 'generar_diagnostico'),
(774, 27, 7, 1, 'generar_diagnostico'),
(775, 27, 8, 1, 'generar_diagnostico'),
(776, 27, 9, 1, 'generar_diagnostico'),
(777, 27, 10, 1, 'generar_diagnostico'),
(778, 27, 11, 1, 'generar_diagnostico'),
(779, 27, 12, 1, 'generar_diagnostico'),
(780, 27, 13, 1, 'generar_diagnostico'),
(781, 3, 1, 1, 'eliminar_empresa'),
(782, 3, 2, 1, 'eliminar_empresa'),
(783, 3, 3, 1, 'eliminar_empresa'),
(784, 3, 4, 1, 'eliminar_empresa'),
(785, 3, 5, 1, 'eliminar_empresa'),
(786, 3, 6, 1, 'eliminar_empresa'),
(787, 3, 7, 1, 'eliminar_empresa'),
(788, 3, 8, 1, 'eliminar_empresa'),
(789, 3, 9, 1, 'eliminar_empresa'),
(790, 3, 10, 1, 'eliminar_empresa'),
(791, 3, 11, 1, 'eliminar_empresa'),
(792, 3, 12, 1, 'eliminar_empresa'),
(793, 3, 13, 1, 'eliminar_empresa'),
(794, 27, 1, 1, 'ver_contactos'),
(795, 27, 2, 1, 'ver_contactos'),
(796, 27, 3, 1, 'ver_contactos'),
(797, 27, 4, 1, 'ver_contactos'),
(798, 27, 5, 1, 'ver_contactos'),
(799, 27, 6, 1, 'ver_contactos'),
(800, 27, 7, 1, 'ver_contactos'),
(801, 27, 8, 1, 'ver_contactos'),
(802, 27, 9, 1, 'ver_contactos'),
(803, 27, 10, 1, 'ver_contactos'),
(804, 27, 11, 1, 'ver_contactos'),
(805, 27, 12, 1, 'ver_contactos'),
(806, 27, 13, 1, 'ver_contactos');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `moduleevent`
--

CREATE TABLE `moduleevent` (
  `id` int(11) NOT NULL,
  `idModule` int(11) NOT NULL,
  `idEvent` int(11) NOT NULL,
  `state` tinyint(4) NOT NULL,
  `idRol` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `moduleevent`
--

INSERT INTO `moduleevent` (`id`, `idModule`, `idEvent`, `state`, `idRol`) VALUES
(1, 14, 4, 1, 1),
(2, 14, 4, 1, 2),
(3, 14, 4, 0, 3),
(4, 14, 4, 1, 4),
(5, 14, 4, 1, 5),
(6, 14, 4, 1, 6),
(7, 14, 4, 1, 7),
(8, 14, 4, 1, 8),
(9, 14, 4, 1, 9),
(10, 14, 4, 1, 10),
(11, 14, 4, 1, 11),
(12, 14, 4, 1, 12),
(13, 4, 5, 1, 1),
(14, 4, 5, 1, 2),
(15, 4, 5, 1, 3),
(16, 4, 5, 1, 4),
(17, 4, 5, 1, 5),
(18, 4, 5, 1, 6),
(19, 4, 5, 1, 7),
(20, 4, 5, 1, 8),
(21, 4, 5, 1, 9),
(22, 4, 5, 1, 10),
(23, 4, 5, 1, 11),
(24, 4, 5, 1, 12),
(25, 17, 6, 1, 1),
(26, 17, 6, 1, 2),
(27, 17, 6, 1, 3),
(28, 17, 6, 1, 4),
(29, 17, 6, 1, 5),
(30, 17, 6, 1, 6),
(31, 17, 6, 1, 7),
(32, 17, 6, 1, 8),
(33, 17, 6, 1, 9),
(34, 17, 6, 1, 10),
(35, 17, 6, 1, 11),
(36, 17, 6, 1, 12);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `modulerole`
--

CREATE TABLE `modulerole` (
  `id` int(11) NOT NULL,
  `idRol` int(255) DEFAULT NULL,
  `idModule` int(255) DEFAULT NULL,
  `state` tinyint(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `modulerole`
--

INSERT INTO `modulerole` (`id`, `idRol`, `idModule`, `state`) VALUES
(1, 1, 1, 0),
(2, 1, 2, 1),
(3, 1, 3, 0),
(4, 1, 4, 1),
(5, 1, 5, 1),
(6, 1, 6, 1),
(7, 1, 7, 1),
(8, 1, 8, 1),
(9, 1, 9, 0),
(10, 1, 10, 1),
(11, 1, 11, 1),
(12, 1, 12, 1),
(13, 1, 13, 1),
(14, 1, 14, 1),
(15, 1, 15, 1),
(16, 1, 16, 1),
(17, 1, 17, 1),
(18, 1, 18, 1),
(19, 2, 1, 1),
(20, 2, 2, 1),
(21, 2, 3, 1),
(22, 2, 4, 1),
(23, 2, 5, 1),
(24, 2, 6, 1),
(25, 2, 7, 1),
(26, 2, 8, 1),
(27, 2, 9, 1),
(28, 2, 10, 1),
(29, 2, 11, 1),
(30, 2, 12, 1),
(31, 2, 13, 1),
(32, 2, 14, 1),
(33, 2, 15, 1),
(34, 2, 16, 1),
(35, 2, 17, 1),
(36, 2, 18, 1),
(37, 3, 1, 1),
(38, 3, 2, 1),
(39, 3, 3, 1),
(40, 3, 4, 1),
(41, 3, 5, 1),
(42, 3, 6, 1),
(43, 3, 7, 1),
(44, 3, 8, 1),
(45, 3, 9, 1),
(46, 3, 10, 1),
(47, 3, 11, 1),
(48, 3, 12, 1),
(49, 3, 13, 1),
(50, 3, 14, 1),
(51, 3, 15, 1),
(52, 3, 16, 1),
(53, 3, 17, 1),
(54, 3, 18, 1),
(55, 4, 1, 1),
(56, 4, 2, 1),
(57, 4, 3, 1),
(58, 4, 4, 1),
(59, 4, 5, 1),
(60, 4, 6, 1),
(61, 4, 7, 1),
(62, 4, 8, 1),
(63, 4, 9, 1),
(64, 4, 10, 1),
(65, 4, 11, 1),
(66, 4, 12, 1),
(67, 4, 13, 1),
(68, 4, 14, 1),
(69, 4, 15, 1),
(70, 4, 16, 1),
(71, 4, 17, 1),
(72, 4, 18, 1),
(73, 5, 1, 1),
(74, 5, 2, 1),
(75, 5, 3, 1),
(76, 5, 4, 1),
(77, 5, 5, 1),
(78, 5, 6, 1),
(79, 5, 7, 1),
(80, 5, 8, 1),
(81, 5, 9, 1),
(82, 5, 10, 1),
(83, 5, 11, 1),
(84, 5, 12, 1),
(85, 5, 13, 1),
(86, 5, 14, 1),
(87, 5, 15, 1),
(88, 5, 16, 1),
(89, 5, 17, 1),
(90, 5, 18, 1),
(91, 6, 1, 1),
(92, 6, 2, 1),
(93, 6, 3, 1),
(94, 6, 4, 1),
(95, 6, 5, 1),
(96, 6, 6, 1),
(97, 6, 7, 1),
(98, 6, 8, 1),
(99, 6, 9, 1),
(100, 6, 10, 1),
(101, 6, 11, 1),
(102, 6, 12, 1),
(103, 6, 13, 1),
(104, 6, 14, 1),
(105, 6, 15, 1),
(106, 6, 16, 1),
(107, 6, 17, 1),
(108, 6, 18, 1),
(109, 7, 1, 1),
(110, 7, 2, 1),
(111, 7, 3, 1),
(112, 7, 4, 1),
(113, 7, 5, 1),
(114, 7, 6, 1),
(115, 7, 7, 1),
(116, 7, 8, 1),
(117, 7, 9, 1),
(118, 7, 10, 1),
(119, 7, 11, 1),
(120, 7, 12, 1),
(121, 7, 13, 1),
(122, 7, 14, 1),
(123, 7, 15, 1),
(124, 7, 16, 1),
(125, 7, 17, 1),
(126, 7, 18, 1),
(127, 8, 1, 1),
(128, 8, 2, 1),
(129, 8, 3, 1),
(130, 8, 4, 1),
(131, 8, 5, 1),
(132, 8, 6, 1),
(133, 8, 7, 0),
(134, 8, 8, 1),
(135, 8, 9, 1),
(136, 8, 10, 1),
(137, 8, 11, 1),
(138, 8, 12, 1),
(139, 8, 13, 1),
(140, 8, 14, 1),
(141, 8, 15, 0),
(142, 8, 16, 1),
(143, 8, 17, 1),
(144, 8, 18, 1),
(145, 9, 1, 1),
(146, 9, 2, 1),
(147, 9, 3, 1),
(148, 9, 4, 1),
(149, 9, 5, 1),
(150, 9, 6, 1),
(151, 9, 7, 1),
(152, 9, 8, 1),
(153, 9, 9, 1),
(154, 9, 10, 1),
(155, 9, 11, 1),
(156, 9, 12, 1),
(157, 9, 13, 1),
(158, 9, 14, 1),
(159, 9, 15, 1),
(160, 9, 16, 1),
(161, 9, 17, 1),
(162, 9, 18, 1),
(163, 10, 1, 1),
(164, 10, 2, 1),
(165, 10, 3, 1),
(166, 10, 4, 1),
(167, 10, 5, 1),
(168, 10, 6, 1),
(169, 10, 7, 1),
(170, 10, 8, 1),
(171, 10, 9, 1),
(172, 10, 10, 1),
(173, 10, 11, 1),
(174, 10, 12, 1),
(175, 10, 13, 1),
(176, 10, 14, 1),
(177, 10, 15, 1),
(178, 10, 16, 1),
(179, 10, 17, 1),
(180, 10, 18, 1),
(181, 11, 1, 0),
(182, 11, 2, 0),
(183, 11, 3, 1),
(184, 11, 4, 1),
(185, 11, 5, 1),
(186, 11, 6, 0),
(187, 11, 7, 0),
(188, 11, 8, 0),
(189, 11, 9, 0),
(190, 11, 10, 1),
(191, 11, 11, 0),
(192, 11, 12, 1),
(193, 11, 13, 0),
(194, 11, 14, 1),
(195, 11, 15, 0),
(196, 11, 16, 1),
(197, 11, 17, 1),
(198, 11, 18, 0),
(199, 12, 1, 1),
(200, 12, 2, 1),
(201, 12, 3, 1),
(202, 12, 4, 1),
(203, 12, 5, 0),
(204, 12, 6, 1),
(205, 12, 7, 0),
(206, 12, 8, 1),
(207, 12, 9, 1),
(208, 12, 10, 1),
(209, 12, 11, 1),
(210, 12, 12, 1),
(211, 12, 13, 1),
(212, 12, 14, 1),
(213, 12, 15, 0),
(214, 12, 16, 1),
(215, 12, 17, 1),
(216, 12, 18, 1),
(217, 1, 19, 1),
(218, 2, 19, 1),
(219, 3, 19, 1),
(220, 4, 19, 1),
(221, 5, 19, 1),
(222, 6, 19, 1),
(223, 7, 19, 1),
(224, 8, 19, 1),
(225, 9, 19, 1),
(226, 10, 19, 1),
(227, 11, 19, 0),
(228, 12, 19, 1),
(229, 3, 20, 1),
(230, 0, 21, 1),
(231, 1, 21, 1),
(232, 2, 21, 1),
(233, 3, 21, 1),
(234, 4, 21, 1),
(235, 5, 21, 1),
(236, 6, 21, 1),
(237, 7, 21, 1),
(238, 8, 21, 1),
(239, 9, 21, 1),
(240, 10, 21, 1),
(241, 11, 21, 0),
(242, 0, 22, 1),
(243, 1, 22, 1),
(244, 2, 22, 1),
(245, 3, 22, 1),
(246, 4, 22, 1),
(247, 5, 22, 1),
(248, 6, 22, 1),
(249, 7, 22, 1),
(250, 8, 22, 1),
(251, 9, 22, 1),
(252, 10, 22, 1),
(253, 11, 22, 1),
(254, 0, 23, 1),
(255, 1, 23, 1),
(256, 2, 23, 1),
(257, 3, 23, 1),
(258, 4, 23, 1),
(259, 5, 23, 1),
(260, 6, 23, 1),
(261, 7, 23, 1),
(262, 8, 23, 1),
(263, 9, 23, 1),
(264, 10, 23, 1),
(265, 11, 23, 0),
(266, 0, 24, 1),
(267, 1, 24, 1),
(268, 2, 24, 1),
(269, 3, 24, 1),
(270, 4, 24, 1),
(271, 5, 24, 1),
(272, 6, 24, 1),
(273, 7, 24, 1),
(274, 8, 24, 1),
(275, 9, 24, 1),
(276, 10, 24, 1),
(277, 11, 24, 0),
(278, 0, 25, 1),
(279, 1, 25, 1),
(280, 2, 25, 1),
(281, 3, 25, 1),
(282, 4, 25, 1),
(283, 5, 25, 1),
(284, 6, 25, 1),
(285, 7, 25, 1),
(286, 8, 25, 1),
(287, 9, 25, 1),
(288, 10, 25, 1),
(289, 11, 25, 0),
(290, 0, 26, 1),
(291, 1, 26, 1),
(292, 2, 26, 0),
(293, 3, 26, 1),
(294, 4, 26, 1),
(295, 5, 26, 1),
(296, 6, 26, 1),
(297, 7, 26, 1),
(298, 8, 26, 1),
(299, 9, 26, 1),
(300, 10, 26, 1),
(301, 11, 26, 0),
(302, 0, 27, 1),
(303, 1, 27, 1),
(304, 2, 27, 1),
(305, 3, 27, 1),
(306, 4, 27, 1),
(307, 5, 27, 1),
(308, 6, 27, 1),
(309, 7, 27, 1),
(310, 8, 27, 1),
(311, 9, 27, 1),
(312, 10, 27, 1),
(313, 11, 27, 0),
(314, 0, 28, 1),
(315, 1, 28, 1),
(316, 2, 28, 1),
(317, 3, 28, 1),
(318, 4, 28, 1),
(319, 5, 28, 1),
(320, 6, 28, 1),
(321, 7, 28, 1),
(322, 8, 28, 1),
(323, 9, 28, 1),
(324, 10, 28, 1),
(325, 11, 28, 0),
(326, 0, 29, 1),
(327, 1, 29, 1),
(328, 2, 29, 1),
(329, 3, 29, 1),
(330, 4, 29, 1),
(331, 5, 29, 1),
(332, 6, 29, 1),
(333, 7, 29, 1),
(334, 8, 29, 1),
(335, 9, 29, 1),
(336, 10, 29, 1),
(337, 11, 29, 0),
(338, 0, 30, 1),
(339, 1, 30, 1),
(340, 2, 30, 1),
(341, 3, 30, 1),
(342, 4, 30, 1),
(343, 5, 30, 1),
(344, 6, 30, 1),
(345, 7, 30, 1),
(346, 8, 30, 1),
(347, 9, 30, 1),
(348, 10, 30, 1),
(349, 11, 30, 1),
(350, 0, 31, 1),
(351, 1, 31, 1),
(352, 2, 31, 1),
(353, 3, 31, 1),
(354, 4, 31, 1),
(355, 5, 31, 1),
(356, 6, 31, 1),
(357, 7, 31, 1),
(358, 8, 31, 1),
(359, 9, 31, 1),
(360, 10, 31, 1),
(361, 11, 31, 1),
(362, 0, 32, 1),
(363, 1, 32, 1),
(364, 2, 32, 1),
(365, 3, 32, 1),
(366, 4, 32, 1),
(367, 5, 32, 1),
(368, 6, 32, 1),
(369, 7, 32, 1),
(370, 8, 32, 1),
(371, 9, 32, 1),
(372, 10, 32, 1),
(373, 11, 32, 1),
(374, 0, 33, 1),
(375, 1, 33, 1),
(376, 2, 33, 1),
(377, 3, 33, 1),
(378, 4, 33, 1),
(379, 5, 33, 1),
(380, 6, 33, 1),
(381, 7, 33, 1),
(382, 8, 33, 1),
(383, 9, 33, 1),
(384, 10, 33, 1),
(385, 11, 33, 1),
(386, 0, 34, 1),
(387, 1, 34, 1),
(388, 2, 34, 1),
(389, 3, 34, 1),
(390, 4, 34, 1),
(391, 5, 34, 1),
(392, 6, 34, 1),
(393, 7, 34, 1),
(394, 8, 34, 1),
(395, 9, 34, 1),
(396, 10, 34, 1),
(397, 11, 34, 1),
(398, 0, 35, 1),
(399, 1, 35, 1),
(400, 2, 35, 1),
(401, 3, 35, 1),
(402, 4, 35, 1),
(403, 5, 35, 1),
(404, 6, 35, 1),
(405, 7, 35, 1),
(406, 8, 35, 1),
(407, 9, 35, 1),
(408, 10, 35, 1),
(409, 11, 35, 1),
(410, 0, 36, 1),
(411, 1, 36, 1),
(412, 2, 36, 1),
(413, 3, 36, 1),
(414, 4, 36, 1),
(415, 5, 36, 1),
(416, 6, 36, 1),
(417, 7, 36, 1),
(418, 8, 36, 1),
(419, 9, 36, 1),
(420, 10, 36, 1),
(421, 11, 36, 1),
(422, 0, 37, 1),
(423, 1, 37, 1),
(424, 2, 37, 1),
(425, 3, 37, 1),
(426, 4, 37, 1),
(427, 5, 37, 1),
(428, 6, 37, 1),
(429, 7, 37, 1),
(430, 8, 37, 1),
(431, 9, 37, 1),
(432, 10, 37, 1),
(433, 11, 37, 1),
(434, 0, 38, 1),
(435, 1, 38, 1),
(436, 2, 38, 1),
(437, 3, 38, 1),
(438, 4, 38, 1),
(439, 5, 38, 1),
(440, 6, 38, 1),
(441, 7, 38, 1),
(442, 8, 38, 1),
(443, 9, 38, 1),
(444, 10, 38, 1),
(445, 11, 38, 1),
(446, 0, 39, 1),
(447, 1, 39, 1),
(448, 2, 39, 1),
(449, 3, 39, 1),
(450, 4, 39, 1),
(451, 5, 39, 1),
(452, 6, 39, 1),
(453, 7, 39, 1),
(454, 8, 39, 1),
(455, 9, 39, 1),
(456, 10, 39, 1),
(457, 11, 39, 1),
(458, 0, 40, 1),
(459, 1, 40, 1),
(460, 2, 40, 1),
(461, 3, 40, 1),
(462, 4, 40, 1),
(463, 5, 40, 1),
(464, 6, 40, 1),
(465, 7, 40, 1),
(466, 8, 40, 1),
(467, 9, 40, 1),
(468, 10, 40, 1),
(469, 11, 40, 1),
(470, 0, 41, 1),
(471, 1, 41, 1),
(472, 2, 41, 1),
(473, 3, 41, 1),
(474, 4, 41, 1),
(475, 5, 41, 1),
(476, 6, 41, 1),
(477, 7, 41, 1),
(478, 8, 41, 1),
(479, 9, 41, 1),
(480, 10, 41, 1),
(481, 11, 41, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ordenbuyclient`
--

CREATE TABLE `ordenbuyclient` (
  `id` int(11) NOT NULL,
  `description` varchar(300) NOT NULL,
  `percent` varchar(5) NOT NULL,
  `pay` varchar(30) NOT NULL,
  `idClient` int(11) NOT NULL,
  `idQuotation` int(11) NOT NULL,
  `type` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ordenbuyprovider`
--

CREATE TABLE `ordenbuyprovider` (
  `id` int(11) NOT NULL,
  `description` varchar(300) NOT NULL,
  `percent` varchar(5) NOT NULL,
  `pay` varchar(30) NOT NULL,
  `idProvider` int(11) NOT NULL,
  `idQuotation` int(11) NOT NULL,
  `checked` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `percentscodes`
--

CREATE TABLE `percentscodes` (
  `id` int(11) NOT NULL,
  `code` varchar(50) NOT NULL,
  `idPerson` int(11) NOT NULL,
  `dateCreate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `percentscodes`
--

INSERT INTO `percentscodes` (`id`, `code`, `idPerson`, `dateCreate`) VALUES
(1, 'RTTR', 4, '2019-06-09 13:59:37');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `person`
--

CREATE TABLE `person` (
  `id` int(11) NOT NULL,
  `names` varchar(80) NOT NULL,
  `lastNames` varchar(80) NOT NULL,
  `documentType` varchar(50) NOT NULL,
  `document` varchar(45) DEFAULT NULL,
  `cellPhone` varchar(30) NOT NULL,
  `telephone` varchar(30) NOT NULL,
  `email` varchar(100) NOT NULL,
  `address` varchar(100) NOT NULL,
  `city` varchar(45) NOT NULL,
  `creationDate` datetime NOT NULL,
  `state` tinyint(1) NOT NULL,
  `idRole` int(11) NOT NULL,
  `idCompany` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `person`
--

INSERT INTO `person` (`id`, `names`, `lastNames`, `documentType`, `document`, `cellPhone`, `telephone`, `email`, `address`, `city`, `creationDate`, `state`, `idRole`, `idCompany`) VALUES
(1, 'Hakem ', 'Duke', 'CC', '123456789', '12000000', '3051100161', 'h.duque@arvack.com', 'carrera 90 # 113 36', 'bogota', '2019-04-16 10:17:21', 1, 3, 1),
(2, 'Daniela', 'Varela', 'CC', '1030551537', '3178942518', '5275224', 'asistenteventas@vortexcompany.co', 'calle 173 # 49b -61', 'bogota', '2019-02-13 11:05:13', 1, 11, 1),
(3, 'Ruby ', 'Herrera', 'CC', '24304882', ' 3142950836', '5275224', 'alfa@alfa.com', 'Calle 173 # 49b-61', 'Bogota', '2019-02-13 11:07:15', 1, 11, 1),
(4, 'Carlos', 'Villallinas', 'CC', '79982738', '3202028391', '5275224', 'cvillallinas@vortexcompany.co', 'Calle 173 # 49b-61', 'Bogota', '2019-02-13 11:07:52', 1, 3, 1),
(5, 'Paola', 'Tello', 'CC', '52.701.695', '3204951060', '5275224', 'ptello@vortexcompany.co', 'Calle 173 # 49b-61', 'Bogota', '2019-02-13 11:11:33', 1, 11, 1),
(6, 'Julian', 'DueÃ±as', 'CC', '79952540', '3108818590', '5275224', 'jdueÃ±as@vortexcompany.co', 'Calle 173 # 49b-61', 'Bogota', '2019-02-14 07:20:31', 0, 11, 1),
(7, 'Jhoston ', 'Zamora', 'CC', '', '314 295 0836', '5275224', ' jzamora@vortexcompany.co', 'Calle 173 # 49b-61', 'Bogota', '2019-02-13 11:16:42', 1, 2, 1),
(8, 'Kenneth', 'Lemus', 'CC', '1', '1', '1', 'f@d.co', '1', 'Cali', '2019-02-22 15:13:59', 0, 2, 1),
(9, 'Monica', 'Duque', 'CC', '52053498', '3118787363', '1', 'compras@vortexcompany.co', '1', 'b', '2019-05-10 18:43:45', 1, 12, 1),
(10, 'Rodrigo', 'Romero ', 'CC', '0', '3136433355', '(6)7494999 ext  62841', 'rromeroz@sena.edu.co', 'Carrera 6 (Av. Centenario) Nro. 44N-15', 'Armenia', '2019-05-15 07:07:44', 1, 7, 147),
(11, 'Ferney', 'Zubieta', 'CC', '1', '2', '2', 'ferneny@quimocsfg.com', 'Calle 73 # 20C-99', 'Bogota', '2019-05-15 17:29:10', 1, 13, 138),
(12, 'Ferney', 'Zubieta', 'CC', '0', '1', '1', 'ferneny@quimocsfg.com', 'Calle 73 # 20C-99', 'Bogota', '2019-06-04 11:33:59', 1, 7, 138),
(13, 'Andrea', 'Barragan', 'CC', '12', '1', '1', 'abarraganf@sena.edu.co', 'Via Mamonal Km 5', 'Cartagena', '2019-05-23 10:54:47', 1, 7, 234),
(14, 'mmmmmmmmmm', ',mmmmmmmmmmm', 'CC', '', '3136433355', '(6)7494999 ext  62841', 'rromeroz@sena.edu.co', 'Carrera 6 (Av. Centenario) Nro. 44N-15', 'Armenia', '2019-05-31 06:25:24', 0, 7, 2),
(15, 'mmmmmmmm', 'mmmmmmm', 'CC', '', '3136433355', '(6)7494999 ext  62841', 'rromeroz@sena.edu.co', 'Carrera 6 (Av. Centenario) Nro. 44N-15', 'Armenia', '2019-05-31 06:30:42', 0, 7, 2),
(16, 'Marlon', 'Caceres', 'CC', '2', '314 2770897', '6344000', 'proyecto8730@gmail.com', 'Car. 27 # 9-0', 'Bucaramanga', '2019-05-31 18:25:58', 1, 7, 193),
(17, 'si se puede', 'no pide ', 'CC', '', '3136433355', '(6)7494999 ext  62841', 'rromeroz@sena.edu.co', 'Carrera 6 (Av. Centenario) Nro. 44N-15', 'Armenia', '2019-06-04 11:02:44', 0, 7, 2),
(18, 'mmmmmmmm', ',mmmmmmmmmmm', 'CC', '', '3136433355', '(6)7494999 ext  62841', 'rromeroz@sena.edu.co', 'Carrera 6 (Av. Centenario) Nro. 44N-15', 'Armenia', '2019-06-04 15:24:16', 0, 7, 134),
(19, 'Paola', 'Perez', 'CC', '1', '3003475690', '1', 'kas2@quimifex.net', '1', 'Barranquilla', '2019-06-04 11:54:52', 1, 7, 140),
(20, 'Maria Camila', 'Rodriguez', 'CC', '1', '1', '1', 'mc.rodriguez15@uniandes.edu.co', 'Cra. 1 ##18a-12', 'Bogota', '2019-06-04 15:41:12', 1, 7, 224),
(21, 'Luis Fernando', 'Mejia Caro', 'CC', '12', '3016', '3394949', 'lmejia@uniandes.edu.co', 'Carrera 1 Este No. 19A - 40', 'BogotÃ¡', '2019-06-05 20:13:23', 1, 7, 224),
(22, 'Melva ', 'Rivera V', 'Otro', '1', '3045235019', '3045235019', 'melvarivera@mryciasas.com', 'Cra 50 FF. 8 su of.0412', 'MedellÃ­n', '2019-06-06 16:09:01', 1, 7, 118),
(23, 'Laura', 'Ospina Diaz', 'Otro', '1', '3103862590', '3103862590', 'ventas1@importecnical.com', 'Cra 48 No. 48 Sur Centro mÃºltiple Las Vegas int148', 'MedellÃ­n', '2019-06-06 16:15:15', 1, 7, 85),
(24, 'Aurora', 'Vargas', 'Otro', '1', '1', '8065785', 'medimalco@gmail.com', 'Cra 7 C No. 146-93 of.203-204', 'BogotÃ¡', '2019-06-06 16:22:03', 1, 7, 114),
(25, 'Fabian', 'Ochoa', 'Otro', '1', '3104388060', '(2) 6657406', 'ventas@merquimica.com', '1', 'Cali', '2019-06-06 16:24:24', 1, 7, 115),
(26, 'Diana', 'Vargas', 'Otro', '1', '3124603083', '7030101', 'dvargas@genproductscompany.com', '7C No. 146-93 of 203-204', 'Bogota', '2019-06-06 16:38:26', 1, 7, 80),
(27, 'Victor', 'Arciniegas', 'Otro', '1', '1', '5523040', 'ventas3@elementosquimicos.com.co', 'Cra 68 D No. 11-76', 'BogotÃ¡', '2019-06-06 16:47:30', 1, 7, 61),
(28, 'marÃ­a Nora', 'Alvarez M', 'Otro', '1', '3103106674', '3103106674', 'asimel@asimel.com', 'Calle 165 No. 54c-48', 'Bogota', '2019-06-06 17:15:05', 1, 7, 20),
(29, 'Elias', 'CastaÃ±eda V', 'Otro', '1', '1', '6709141', 'asimel@asimel.com', 'Calle 165 No.54C-48', 'BogotÃ¡', '2019-06-06 17:17:50', 1, 7, 20),
(30, 'Adiela', 'Bedoya', 'Otro', '1', '1', '3045235019', '1', 'Cra 40FF 8sur of 0412', 'Medellin', '2019-06-06 17:22:24', 1, 7, 118),
(31, 'Rodrigo', 'Escandon', 'Otro', '1', '3124603083', '7030101', 'dvargas@genproductscompany.com', '7C No.146-93 of 203', 'BogotÃ¡', '2019-06-06 17:27:07', 1, 7, 80),
(32, 'Diego', 'Zapata', 'Otro', '1', '3104141549', '2507554', 'dzapata@filtraciÃ³nyanalisis.com', 'Calle 33B No.83C-36', 'MedellÃ­n', '2019-06-06 17:31:05', 1, 7, 69),
(33, 'John', 'Hernandez Novoa', 'Otro', '1', '1', '2917922', 'info@alfaquimica.com', 'Cra 90 No.68-41 Br. La Florida', 'Bogota', '2019-06-06 17:49:37', 1, 7, 8),
(34, 'Gustavo', 'Auza', 'Otro', '1', '3165271235', '6130906', 'adtltda2@gmail.com', 'Cra 57 No. 138-66', 'BogotÃ¡', '2019-06-06 17:53:40', 1, 7, 5),
(35, 'Jaime Andres', 'Morales O.', 'Otro', '1', '3006002044', '2381646', 'dbiocientifica@gmail.com', 'Calle 21A No.77-30 of 301', 'MedellÃ­n', '2019-06-06 18:02:13', 1, 7, 54),
(36, 'Esneyder', 'Cardozo', 'Otro', '1', '3203344249', '2850010', 'esneyder.cardozo@pajonales.com', 'Cra 5a.No.29-32 of 292', 'Ibague', '2019-06-07 10:07:20', 1, 7, 123),
(37, 'Camilo Fernando', 'Rodriguez M', 'Otro', '1', '1', '4010000 ext 134', 'ambientalaqm.com.co', 'Cra 65 No.15a-23', 'BogotÃ¡', '2019-06-07 18:36:50', 1, 7, 13),
(38, 'Julieth', 'Herrera', 'CC', '1', '1', '1', 'ljulieth.herrera@udea.edu.co', 'Medellin', 'Medellin', '2019-06-09 13:38:23', 1, 7, 175),
(39, 'Jose Antonio', 'Henao', 'CC', '1', '1', '1', 'direccionrx@uis.edu.co', 'Complejo Guatiguara', 'Bucaramanga', '2019-06-09 13:42:00', 1, 7, 193),
(40, 'Lady Julieth', 'Herrera Cortes', ' ', ' ', '1', '1', 'julieth.herrera@udea.edu.co', '1', 'MedellÃ­n', '2019-06-11 17:55:48', 1, 7, 175),
(41, 'Eider Ansisar', 'Erazo Moreno', ' ', ' ', '3', '3394949', 'ea.erazo', 'Cra. 1 # 18a-12', 'Bogota', '2019-06-12 11:24:29', 1, 7, 224),
(42, 'Hissel Lorena', 'Cadena DÃ­az ', ' ', ' ', '3123919749', '8370987', 'cdathlaplata@sena.edu.co', 'Cr.7 No. 5-67', 'La plata', '2019-06-12 12:25:48', 1, 7, 242),
(43, 'Dagoberto', 'Oyola', 'CC', '123', '300', '2771212', 'doyola@ut.edu.co', 'Barrio Santa Helena Parte Alta ', 'IbaguÃ©', '2019-06-13 14:35:37', 1, 7, 186),
(44, 'Vanessa ', 'Soto', ' ', ' ', '3108488920', '(2) 6642260', 'vanessasoto', 'Aveida 3 N No.49-07', 'Cali', '2019-06-13 17:43:20', 1, 7, 115),
(45, 'Stivenson', 'Sandoval Bohorquez', 'CC', '2', '3173794547', '6344000', 'stivens_004@hotmail.com', 'Car. 27 # 9-0', 'Bucaramanga', '2019-06-14 14:21:39', 1, 7, 193),
(46, 'Victor Alfonso', 'Mayor Torrez', ' ', ' ', '1', '(6)7359300 ext 1056', 'laboratoriosquimica@uniquindio.edu.co', 'Carrera 15 calle12 norte.', 'Armenia', '2019-06-14 19:04:45', 1, 7, 185),
(47, 'Jorge Hernan', 'Sanchez', ' ', ' ', '1', '1', 'jorgeh.sanchez@upb.edu.co', 'Cir 1 70 01 Blo 11 Of 317', 'Medellin', '2019-06-18 13:54:55', 1, 7, 201),
(48, 'Jorge Mario', 'Olivar', ' ', ' ', '1', '1', 'olivar.barreto82@gmail.com', '38', 'Angostura', '2019-06-18 21:29:06', 1, 7, 153),
(49, 'Isabel', 'Puche S', ' ', ' ', '3012807764', '3012807764', 'MedellÃ­n3avantika.com.co', 'Cra82 #48-03 local 101', 'Medellin', '2019-06-19 15:40:40', 1, 7, 21),
(50, 'Brandon', 'Vargas Suaza', ' ', ' ', '1', '2196546', 'cienmateequipos@gmail.com', 'calle 62 # 52-69', 'Medellin', '2019-06-20 19:46:01', 1, 7, 175),
(51, 'MarÃ­a Teresa ', 'CortÃ©s', ' ', ' ', '1', '3394949', 'marcorte@uniandes.edu.co', 'Cra. 1 # 18a-12', 'Bogota', '2019-06-20 20:41:09', 1, 7, 224),
(52, 'Sandra Lorena', 'Aranzazu Giraldo', ' ', ' ', '1', '3394949', 'sl.aranzazu@uniandes.edu.co', 'Cra. 1 # 18a-12', 'BogotÃ¡', '2019-06-20 21:04:29', 1, 7, 224),
(53, 'Daniel ', 'Zuluaga', ' ', ' ', '3104091289', '(4) 2198332', 'Daniel.Zuluagac@udea.edu.co', 'Cl. 67 # 53-10', 'Medellin', '2019-06-21 18:32:05', 1, 7, 175),
(54, 'Paula Andrea', 'Monsalve Z', ' ', ' ', '1', '1', '1', 'Universidad de Antioquia', 'Medellin', '2019-06-26 11:40:32', 1, 7, 175),
(55, 'Gerson Dirceu', 'Lopez MuÃ±oz', ' ', ' ', '3', '1', 'gd.lopez@uniandes.edu.co', 'Cra. 1 # 18a-12', 'Bogota', '2019-06-27 14:09:15', 1, 7, 224),
(56, 'Martha Yaneth', 'Guzman', ' ', ' ', '3013235755', '5712200200', 'mguzman@sgc.gov.co', 'Carrera 50 N 26 20', 'Bogota', '2019-06-27 18:49:21', 1, 7, 159),
(57, 'prueba', 'hjkn', ' ', ' ', 'hjh', 'jhk', 'jhkj', 'hj', 'hk', '2019-08-13 17:56:15', 1, 7, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personcreatequotation`
--

CREATE TABLE `personcreatequotation` (
  `id` int(11) NOT NULL,
  `completeName` varchar(100) NOT NULL,
  `rolName` varchar(30) NOT NULL,
  `cellphone` varchar(30) NOT NULL,
  `email` varchar(100) NOT NULL,
  `idQuotation` int(11) NOT NULL,
  `state` tinyint(1) NOT NULL,
  `idPerson` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `personcreatequotation`
--

INSERT INTO `personcreatequotation` (`id`, `completeName`, `rolName`, `cellphone`, `email`, `idQuotation`, `state`, `idPerson`) VALUES
(1, 'Hakem  Duke', 'Coordinador General', '12000000', 'h.duque@arvack.com', 2, 1, 1),
(2, 'Hakem  Duke', 'Coordinador General', '12000000', 'h.duque@arvack.com', 3, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personcreatequotationmainteince`
--

CREATE TABLE `personcreatequotationmainteince` (
  `id` int(11) NOT NULL,
  `completeName` varchar(100) NOT NULL,
  `rolName` varchar(30) NOT NULL,
  `cellphone` varchar(30) NOT NULL,
  `email` varchar(100) NOT NULL,
  `idQuotation` int(11) NOT NULL,
  `state` tinyint(1) NOT NULL,
  `idPerson` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `personcreatequotationmainteince`
--

INSERT INTO `personcreatequotationmainteince` (`id`, `completeName`, `rolName`, `cellphone`, `email`, `idQuotation`, `state`, `idPerson`) VALUES
(1, 'Hakem  Duke', 'Coordinador General', '12000000', 'h.duque@arvack.com', 1, 1, 1),
(2, 'Hakem  Duke', 'Coordinador General', '12000000', 'h.duque@arvack.com', 2, 1, 1),
(3, 'Hakem  Duke', 'Coordinador General', '12000000', 'h.duque@arvack.com', 3, 1, 1),
(4, 'Hakem  Duke', 'Coordinador General', '12000000', 'h.duque@arvack.com', 4, 1, 1),
(5, 'Hakem  Duke', 'Coordinador General', '12000000', 'h.duque@arvack.com', 5, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `photodiagnosis`
--

CREATE TABLE `photodiagnosis` (
  `id` int(11) NOT NULL,
  `photo1` longtext DEFAULT NULL,
  `photo2` longtext DEFAULT NULL,
  `photo3` longtext DEFAULT NULL,
  `idDiagnosis` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `photodiagnosis`
--

INSERT INTO `photodiagnosis` (`id`, `photo1`, `photo2`, `photo3`, `idDiagnosis`) VALUES
(1, 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAMCAgICAgMCAgIDAwMDBAYEBAQEBAgGBgUGCQgKCgkICQkKDA8MCgsOCwkJDRENDg8QEBEQCgwSExIQEw8QEBD/2wBDAQMDAwQDBAgEBAgQCwkLEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBD/wAARCADIAMgDASIAAhEBAxEB/8QAHgABAAICAwEBAQAAAAAAAAAAAAgJAgoBBgcEBQP/xABEEAABAwMDAgQCBQgIBgMBAAABAgMEAAUGBwgREiEJEzFBFFEiMlJhkhUjM0JTVHGBFhc4YnJ0sbUYJHaCobNEVWay/8QAFAEBAAAAAAAAAAAAAAAAAAAAAP/EABQRAQAAAAAAAAAAAAAAAAAAAAD/2gAMAwEAAhEDEQA/ALU6UpQKUpQKUpQKUpQKUpQKUpQKUpQKUpQKUpQKUpQKUpQKUpQKUpQKxVz7HisqxVQY9/tGnf7RrmlBx3+0ad/tGuaUHHf7Rp3+0a5pQcd/tGnf7RrmlBx3+0ad/tGuaUHHf7Rp3+0a/lMmRLfFdnT5LUeNHQXHXnVhCG0AclSlHsAB7mqyt6XiywLC5ctM9r0ti4T0BUaXlykByOyv0UIaVDh0juA6odHPdIUOFEJWbqt9Wi21aG3AyW4uXzKJK0BrH7YtK5SGifpPOknpaSE8kBRBUeABxypMgrLeLbkNng36yz2plvuUZqXEkMq6m3mXEhSFpI9QUkEH761dLze7xkd1lXy/3SXcrjNdU/JlynlOuvOKPJUtaiSok+5q6nwjNwn9Z+gz2k98neZfdOHExWutXK3bW6VGMr7/ACyHGu3olDfPqKCd3f7Rp3+0a5pQcd/tGnf7RrmlBx3+0ad/tGuaUHHf7Rp3+0a5pQZ0pSgViqsqxVQcUpSgUpSgUpSgUpSgV0TWrW7Tjb7gUzUfVC/otdniKSyjhJW9KkKBKGGWx3ccV0kgD0CVKUQlKlDvdeX7mtFbXuE0My7Sa5FCHL1AV8A+o8CPObIcjOk8E9IdQjqA7lPUn3oKYt5PiJan7oJD+J2IvYpp+2s9Fpju/np/B+iuW4Pr/c2OED3CiAREevsvVmumO3idj97gvQrjbJLsOXGeSUuMvNqKVoUD3CgoEEfMV8dAqQmxDcCdue5HGswuE0sY9c3PyLf+SekQX1JBcIHr5Sw276cnyyPeo90oNqRCgtAWkghQ5BHpXNRP8NDcJ/Xxtms0a7zEu5Jg5Tjl0BUOtxDSB8K+RyVcLZ6AVHjqcbd49KlhQKUpQKUpQKUpQZ0pSgViqsqxVQcUpSgUpSgUpSgUpXX88z7DtMsVuGbZ7kUKx2S2NebJmzHAhtA9AB7qUTwAkclRIABJoOwE8V8tuuduvEQTrVPjTYylrbD0d1LiCpCihaepJI5SpKkkexBHtVL+9HxSs01kcm6eaFuzcWwogsP3PqLVyuo7gnlJ/wCXZI9EA9ZHdShz0JkD4Mu4NeSYPkW3W/SyubiylXyydSySq3vOBMhoDjhKWn1oVzzyfiiOAE0EcvF029/1Y69Mas2OD5di1GaVJfKEcIaurXCZCe3p5iS27ye6lKd+RqCFbD+/Lb6Nx223JMPt8RL2Q2tH5bsBCApZnRwSGk8kceaguM888DzAe/HFa8FApSlBMHwutwg0S3KwMdvMwtY5qGhuwTupzpbalFfMN4jjuQ4S134ATIWee1XyevpWq6w+9FfbkxnltOtKC23EKKVIUDyCCO4IPvWxdsp19Z3H7dcX1CekIcvLTP5KvqU+qLiwlKXSR7dYKHQPZLqRQe6UpSgUpSgUpSgzpSlArFVZViqg4pSlApSlApSvjvESXPtM2DAuC4EmRHcaZlISFKYcUkhLgB7EpJBAPbtQeG7rN6ekO0/Hy/l843TJZbCnLXjkJxPxUo+iVLPcMNc+rih7HpCyOmqQdzG7fWDdPkovGoN6LNqiuqcttiiKUmFC5BAKU/rr4JHmK5V3PoDxXTtcYGo9p1fy+06u3SZcsxg3aRFvEyW6p1ch9tRT1hSgCUEBJR2A6OngAcCuj0CvU9r2tty28a7YlqvBce+GtM5KLmy0OoyLe7+bkthJIClFpSink8BYQfavLKUG0zZ7vbcgtMK+2eazMgXKM3LiyGVBTbzLiQpC0keqSkgg/I1Ql4le3s6C7mr09aLeI+MZsDkVo6AfLbU6o/EsDhISnof8whCeeltxr51Yd4R+4T+tHQJ3Sy+zy7f9OHUQmgsjqdtTnJiqHz6CHGuOOyW2+/Kq7H4pu3wa07a52VWeEXsk07Uu+QulJUtyHwBNZHfsC0lLvoSTHSB60FEVKUoFWAeD/uFGnutFw0Vv88t2bUBnqgBawENXZhJLfcnhPmteYjtyVLSyKr/r9LGcjvWHZHastxucuFdrJNYuMCSgAqYkMrC23Bz25SpIPft2oNpPnmled7e9YrPr7ozimrVlS223kNvQ/IjoWViLKTyiQx1EDny3UuI54HPTz716JQKUpQKUpQZ0pSgViqsqxVQcUpSgUpQkAcmgUqLm8Pf5pVtUtTlnS+1k2eSWz8Fj8V39B27PS1j9C3z7d1rPZKeOpafFfDP32ZxuLznOsB1jvEV6/SlJv9iSywGWm4qQhp+I0kfqt/mlpBJUet1RJ4JoPD/GZ2+/0dzrHtxNht/RByhAst9WhICU3FlBMdxR55Up1hKkdhwBEHJ5UOa162U9zuidu3DaF5bpNPLTb15gk2+Q56RpzRDkZwngkJDqEdXA5KCse9a293tNysN1m2O8wnYc+3SHIsqO6nhbLzailaFD2IUCD/Cg+SlKUEiNhG4JW3PcnjeVXCb8Pjt5cFiv/UrhCYUhaR5qux7NOBt3sOeGyPc1sMSY8eZGciSmW3mX0FtxtxIUhaSOCkg9iCDxwa1X/Sr+vDW3CHXzbJZBd5peyXCuMbu5Wfpu+ShPwz55UVK62C31LVx1Ooe4HAoKc952gb227cRlOm7LDibN5wuVicWD+ctz/K2gCTyroPW0SfVTSq8Rq53xiNvqc60btuudjhBV4wF8MXFSB9N21SFhB54BKi0+WlAcgJQ48qqYqBSlKC0DwYtwhg3rI9t1+mkM3ILyCwBauwfQlKZTCeftICHAB2/Nun1NWy1rEaRal3/R3U3GtT8ZdKLjjdxZntJ5IDqUq+m0rj9VaCpCvuUa2VtPM5sGpuC2DULFpPn2nIrcxcoayR1Bt1AUEq4JAUnnpUPYgj2oOw0pSgUpSgzpSlArFVZViqg4pShPA5oP4Tp0K2RHrhcZbMWLHQXHnnlhDbaAOSpSj2AA9zVXm9fxY2oirlpftZnofd+nFnZl08oQfRSYCT2V7jzyOPdsEdLleA+I9vA13z/VnKtDrwh/EMUxe4vW/wDI0VxSVXJKVHolSXCAXEuIKVoSOEBCk8AklRhFQfXdrvdb9cpN5vdylXCfMcU9IlSXVOuvOH1UpaiSon5mu66A6v3zQbWLFdWLApRfx+4IfeZSrj4mMr6D7JPyW0pafu6ufavP6UG0ji+S2XM8ZtWXY7MTMtV7hM3CFISOA6w6gLQrg9xylQPBqlrxc9vv9V+vzOq1kg+VYtSGlS3ChP0GroyEpkp7enWC2737lS3OPSpZ+DzuFGe6NXLQ+/3BC7zp+8HbchxxIcetMhRUnpTz1L8p7zEqVxwlLrCfcVIDflt8/wCI7bdkmH2yAmRklrbF6x7sOv45gEhpJJABdbLjPJPA80E+lBrwUoQUkgjgjtSgVMjwsdwR0W3KQ8VvFw8jHNRkN2KYlXAQib1EwnSeOeQ4pTXqABIUT6DiG9f0iyZEKS1MiPrZfYWl1pxCilSFpPIUCO4IIB5oNozKcZsebYvdsPyaAmbaL5Bft0+MpRSHo7zZQ4jlJBHKVEcggjnsa1rNeNJL3oTq/lWk9/CzJx24uRm3lI6fiY5+kw+B8nGlNrH+Kr+NmOvkfcft5xbUV19K7x5H5OvqEjjy7kwAl48egC/ougeyXU1CPxn9vfmxca3JY/A+mwU4/kKm0fqHqVEfVx8j5jRUftND5UFU1KUoFW/eDRuF/pJgV/27ZBcCufiizeLEhxXdVueXw+2kAejT6gvkkk/FcDsntUDXq+1nXC47ddd8U1VhqcVFtswM3RhHJ+It7o8uQjgEdR8tRUkHsFpQfag2TqV8tqulvvdrh3m0y25UKew3JjPtnlDrS0hSFpPuCCCP419VApSlBnSlKBWKqyrFVBxSlKCpzxnNva4t7xrcdj1vUW7klOP37ym+eH0JUqK8rj3U2FtknsPKbHuKrB+Fk/u7v4DW1ApCFjpWkKHyI5rD4dj9i3+EUGrF8LJ/d3fwGnwsn93d/Aa2nfh2P2Lf4RT4dj9i3+EUGulsv1yuG2/cViuozvxLdmU/+TL8hIUA5bZBCXSoAEq8s9DwT7qZTWxQzcIMhhD7E1hxtxAWhaXAQoEcggj1Ff1Mdjj9C3+EVrO68PPDXXURIdWAMtu4ACj++O0Ht3iW7fRoPubvT1nhKYxvN+rI7VwkBDa3Vn4phPCQkBD/AFlKB9VtbQPzqKqGHnB1NtLUPTkJJq0zxy/02iw/u5F/rbq9N8FRtte2zMOttKiM4keo5/8AgQqCmf4WT+7u/gNPhZP7u7+A1tO/DsfsW/winw7H7Fv8IoKa/B517l4Jq/dNDb68tuy52wqVA80KCWbpGQVdiSEpDjAcBPBKlNMpH32v6z6a4zrfpXk2lGRuoMHJbe7CK09KlMOEctPJB/WbcCHB96BX4O61lpG1zWJaGkJUnAchIISAQfyc/VJ/hqPPL3vaYJU6sgyrh2Kj/wDXSqCP2ZYVkWB5de8IyK3rYutguEi2TWwCQl9lxTawD7jqSeD7jvX43wsn93d/Aa2niwyo8llBJ9ykVx8Ox+xb/CKDVi+Fk/u7v4DT4WT+7u/gNbTvw7H7Fv8ACKfDsfsW/wAIoIP+Evr+7qjt/OmN/kLVftN1ogJ8367tsc6lRVd/Xo6Vs9vRLaOfWpyVilptB5Q2lJPyHFZUCnIrqOqt0nWrDiq3XNFuen3S1WkylK6VNImT2Iq1Nn2d6HleWe4DnQSCOQY97X80yt66W9d2BbF9uy4K4aRJQ2YzlsVOZl9D8iQoqSplTPmpW2lwOglpBSnkJa0pSgViqsq+G+Xm2Y7Zp1/vUxqJb7ZGdmS5DqulDLLaSta1H2ASCT/Cg6zqpq9pzonij+a6n5XCsNoZPQHpK+FOuEEhttA+k4s8HhKQT2J9ATULb340O2+3XdyDasHzu6w21lImtRYzSVjn6yUOPBXH+IJP3VWpvF3TZZus1cn5bcZUlrG4DrsXGrSokIhwur6KigEjznAEqcV35PCeelCQO6afeGNu81GxFrMrbgcO2RZLIfix7vcERJT6D3HDSu6eR3HX0+tBaLZvE62vZBpbftSbRe7k5IxyM1Km464whq6FC3m2uWm1rDbvSXASULIABPtX9NuPiRaL7nNTWNLMHxbL4F0fhyJqXrnGjoYCGkgqBLby1c9+3aqNNS9L890fy2Xg2pGMzbFeoXBcjSkcdSDz0rQodloPB4UkkHg1KrwiP7ZFt/6fun/rTQXmy5cWBFenTpLUeNHbU6886sIQ2hI5UpSj2AABJJqFmrHi2bW9N707YrG5fc2kMKKHXrHHb+FSoewedWgL/igKHb1qOXi57wL0vIV7W8Buqo1ujR2pGWvsL4VIdWAtuESPRCUdC1j9YrSk9kkGFW3/AGda/bmEyJel2HF+2RV+U9dJzwjQ0r90hxX11D3CQSKC2vSDxYNreqV7Yx27TLzhMyUtLTK8gYbRGWs+xfaWtKB6d19I71THrs4h3XPUN1paVoXll2UlSTyCDMd4IPvXb9wOzzXzbMI0nVPD/h7ZMc8mPdYTwkw3HOCQjzE/VUQCQlQBPB7V4un6w/jQXxeIJqrtP0yewQbntJLhmxuQun5EMSOh34TyzG+I6up5vjr62fTn6h9PftuwzUHbvqPpbfL1ts05mYZjse/uRZkKUyhpTs0RmFKdAS64OC2tpPPI+r6VD7xy/wBNot/hyP8A1t1eneCmQNtWYkkADOJHc/5CFQTnz3ULC9L8Yl5nqBkkGxWWCnl+ZMdCEAn0SPdSj7JHJPsKhHlPjNbarJd3bdYcSzbIIzS+n46PFYYacH2kJddSs/zSmq/PEB3eXvc/q7Og2m5OIwDF5bsTH4aFkNyOklCpyx25W7wSnkcpQQn16ifk0c8OfdPrfh7Gd4phcWDZprfnQX7xNTDMxBHIW0hQKik+yiAk+xoLJcm8QHbnuT22avY3iWRybRkb+AZF5NlvbQjyXuLdIJDRClNuHgE8JUTx7VW34aX9t/S//N3D/bpVeOav6J6n6D5Y5heqeJy7Hc0p8xsOgKafb5462nE8pcTz25STXsfhpf239L/83cP9ulUGwQ44hpBccUEpSOSSeAB86hzrP4qu1vSS8vY7b7jds2uEZam3xjzLbsdtY9QX3FoQrvyOUFVeJeLnu+vGHR422bTq8ORJt5hfF5XKjrAWiG5yGoQUlXKS4AtTgIHLZaAJS4oVXRoHtZ1u3LXKTA0mw924sQSEy7g+4GIcdRHISt1Xbq479I5Vx7UFuGlni5bW9Qry1Y7+Mhwp6QoIbkXqM2YpUT6F1la+gfeoAffXy514vW3DBMzvuEy8XzW5P2K4yLcubb48N2LJLThR5rK/iB1tq6epKuByCDVXO4DZDuJ21W1u/ak4ej8iOOBr8q22QJUVCz2Slak92yT2HUByewrwag2ksbv8TJsateUQm3W4t1gsT2kugBaW3WwtIVwSAeFDnvUSdZvFW2t6S3x/HLfcbtms+KtTT5x1lt2O2sdiPPcWhC+/I5QVelec+IXrtftJNjmn+H4rOMO5ahWuBaH3kK4cRbkQUKlBB9irqabP91xXvxVfGy3ZVlW8jJL7AtWWwsas2MssOXK4PxlSXAt8ueS20yFICyfKc5JWkAD3JAIWP4t4te03VNbmGZzZ8qxaHdEGM5JucdCo46uwJcjuKW2RzyF8DpI5BBAqXOmuFYVERFzjGshk5EifAS1b7k9NTJbEJfSoJZKAEdCulB5HJPSnv2qmneX4aGW7VMDY1PtGoDGZY6iU3DuSvyYqFIgrc7NLKQ44lbZV9Eq6kkKUgcHqJEgPBg3AX2ZNyXbjfZq5Fuhwl5DYQ4vkxuHUIkso5/VUp5DgA9CHD+saC1elKUCoo+J9qG5gGzjNW4cz4edkaY1jY+a0PvoD6f5secP51K6oh+KjgC832c5VPix3H5mMPw7yylHshD6EPKP3JZW4r/toKz/C10Zx/WLdZbjlMREu2YdbXsmVGdQFNvvsutNMJUD7JcfQ5x7+XwexNX0gADgVRT4TGqdi023Yx7ZkUtMWPmllk48w84QlCJSnGX2Qok9uoxy2PmpxI96vW5HrQQM8X/RKyZpt0GrrUJpF9wGbHUJKUjzHYMl5DDjJPqUhxxpwc+nSrj6x5hB4RB43kW7/AKfun/rTU9PFy1as2E7Vpmn65bZvGfT4sGLHCh1iOw8iS87x9keUhBPzdTUC/CJHO8i3D/8AP3T/APhNBHTUW45BrnuHyCbEHxN4zfLX0RUFXYuyZZS02D7JHWhI+QArYx0p0yxTRvTuwaZYVAREs+PQm4cdKUJSpwgcrdX0gBTjiypa1eqlKUT61rmZEb5oVuHuLjLIRd8BzFx1pDg+j58KaVI5+YJbH8Qa2Q8HzTHNRcQs+dYhcm7hZr9CanwZKOQHGXEhSSQe6T34KSAQQQQCKD87VjTLF9Y9Och0yzGGmTachguQ3wUhRbURyh1HPYLbWErSfZSQfatZi/2WXjeRXLHZ6emVapr0J8cccONLKFf+UmtnbPc3x3TbDL1nuWzkw7PYIL1wmvK4+i02kqIA91HjgD1JIA7mtY7K8gk5Zll4yqaOJF5uMi4PD++86pav/KjQWc+OX+m0W/w5H/rbq/B2VZ6dNPC73BZY2XEvIu9wt8dba+lTb8yBBiNLB9ulb6Vfyr97xy/02i3+HI/9bdXVtn+Duah+FbuFxxl0ocbvM26t8DkrVBhQJgQPvUY4T/3UEV9iGjVl133S4VgeTspesnxDtzuTChymQxFaU8WVf3XFIQ2r+6s1sSttoaQlttAQlIACQOAB8hWvZ4eOqtj0f3b4Nk2TPpj2mc89ZZT6lBKWPi2lMtuKUogBCXVNlRJ7JCj7VsKgg0ER/E90MtWru1fJMgbtbDuRYCyrIrZKVwlbTDXCprfVwT0KjpcV0dgVttE/VFVW+Gn/AG39MP8ANXD/AG2VVtPiR6wWvSXaTmyHZ8VF1y+EvGLZFdJ6pKpY8qR0ge6Iyn18ngcpA55IBqW8NP8AtvaX/wCauH+2yqDpO8rL7pnW6zVbILs75j/9K7hAbIHHEeK6YzCf5NMtj+VX37ZtFLBt+0SxbTKxQ2ml26C2u4vISAqXPWkKkPLPqSpZPHPokJT6AVQPu5xu64juk1Ws12iuRpCcvukpCVjgqYfkreZX/BTTjah9yhWwZoRqtYdbdI8V1Ox6U07Gv1sYkOIQoEsSOkB5lXHoptwKQR80/Lg0HYc3wzHdRMRu+D5bbWp9nvkN2DMjuDlK2nElJ/gRzyD6ggEdxWs9qnhS9NtTcu07ckmQrF75PsxeKeku/Dvra6+Pbno5/nWzdf79aMXsk/I7/cGYNttcZyZLkvK6UMstpKlrUfYAAmtZjWLNmdStW811EjMOMs5RkNxvDTTnHW2iRIW6lJ47cgLA/lQWaeKNpxdsi2daL6jW6M6+xiEeFHndA5DLEyEykOq+QDrDSOfm4Kr325botW9reTzMl0su7DIubSGbjBls+dFmIQSUdaOQepJUrpUCCOpXsSDe/Gy3ReBoZiGFayZLi8O25FiMJh233yY00iZHMVtLg6XCOod+CR6c+1QYyzwldFtWFz8i2wa/QhBbfKFQ3nEXONGWR1Brz2lBY7EcdQUeOPX1oOu2nxhLBn1lcwjcjtytGR45cC0me1BeS82sIWlYUqLJBQ4QtCVAFaeCkd+1Tl2n/wDBdqNbW9WttGA4da5zTZiyVwLKxCuMEuD6TLyUDqRzx8ylXHIKh3qoncb4dG43bVjcjOMnt9nv2LxFJTKu1jlqdRF6lhCC824hDiQVKA6glSQSASCRXHhw61XHRndRizomPosuUOKsN2joPKXm3gQyop+aHg0oH14Ch6E0GwTSlKBX5GXYxZs1xe7YfkUNEu13uC/bprC/R1h5BQtJ/ilRFfr1iqg1styegWa7XNY7rp1kKZKDAf8AirPc0pU2mdDKuWJDavnwOFAH6K0qTzympIab+L/uWwbDY+K32045lsqEyGI12ujTqZSkgcJLxbWA6QOPpcBR45USSTVtG4PbPpJuaxE4jqjjyZQa6lQbjHV5U2A4R9dl3g8felQKT7pNVv5r4KuaRcrix8C1QiTsefc/PybhG8qRGR1enQkkOED3BTz8hQQN1t121O3DZxI1A1TyJy6XJ5IaZbSny48NkH6LLDQ7NoHPoO5JJUSokmSnhEf2yLb/ANP3T/1pqbWHeD7oTj2m1+xe8ZLdLxlF9jtx05E9HQDbUpdQ4r4WPz0pUoI6CtRUrpUeCkEg9t2u+Ghgu13VePqvj+pV8vUuPCkwRElxGm2yl5IBPKTzyOKCHvi7bVLtiGo3/EjidqW7jmVeUxfiynkQbklAQlxYHoh5CU/S448xKuTytPMfdru//XXaxbnsZxeTBv2LvrLqbNd0rcajOE8qWwtKgprk+qQSknk9PPer/cjxuwZfY5uM5TZod2tNxZVHlwpjKXWX21eqVoUCCKra198GbFrrLm5DoBmMizl9ZcasFz/PR2iT3S3IJ60oA9AsLP8AeNBCbdHv9103UQGMaymTBsWMR1h02W0JWhmQ6PqrfWolTpT7AkJHr0896jWn6w/jVve37wbMGx2bb8n15yyRkb0ch1ePwk+RDUsHsl14HrcR6HpT0c+5I5B/VyTwX9Jb/kl1yCPqvf7a3cpz8xuFGt0cMxkuOFYaQOeyE89I+4Cg898cv9Not/hyP/W3V6X4LLDMrbLmsaS0h1l3NpSHG1pCkrSbfCBBB7EEe1e7bx9kGK7x14irJs3umP8A9ERPDPwMdt3z/ivI6urrPbp+HTxx9o12PaJtUx/aLp/ddPsbyqffo11vLl5XImsIaWhamGWigBHYjhkH+JNBSXvW2vX7azrXdMVXEfVjFydXPxqeoEpehKUSlsq9PNaP0FjsewVxwpNeqaIeK7uM0dweLgdxh2bMoltb8m3y7yHTLYaA4S2p1Ch5iE8cDqHUB26uAALlNatC9MdwWGP4JqnjLF3trvK2lElD8V3jgOsuj6Tax8x2PoQQSDWlqZ4KuSQ74wdJtTm7haH3j5qLwylp+K324+kj6Lp9e4Sn09KCDO4bcxq1udzBOYapX1MhUdBagW+KjyoUBonkpZb5PHPblSiVK4HJPA49H8NL+2/pf/m7h/t0qrEdJfCB0RwfGb6zmuQzsryS82iZbItwfjpbj2lUhhbXxDEfkhbyOvlKnFEAgEBJANfrbffCs0+2+6w45rDZdU7/AHWbjjr7jUOTDZQ06XY7jJClJPI4DpPb3AoPBPGG2rXQ3qHuiwu0F6I+wzbMsRHbKlNONgpjzV8c/RKOllR7BPls+vUSIcbXd7mtm1CTLjYJcItxx+4LDsuxXRCnIineOPNb6SFNOcdipJ4UAOoK6U8bDd0tdtvduk2i7wI82FMaUxIjyGw4262ocKSpJ7EEHgg1XNuH8HLAspnXDKtBsmfxiVJ5dTYJSPOghwn0acJ62k+p6T1geg4HAAQl3N+I1r3ubx0YReF27GcYcUFy7bZ0rT8cod0h91aipaAe4QOE88EgkAiK9W06D+DHjlvlwb/r7m0i6eQvrdsNqHlMO8HslyRz1lB9wgJV8lCu35n4Muj2U5declt2pl8sUS6T35bFrhW9gR4La1lSWG+Tz0IBCRz7AUHQvE70SveYbV9HdZbBDckpwiyxYd2Q2nkohyo0fpfPv0odaSk8ftufQE1FbYRvmkbP77fbdfsek33EsmLLkyNGdCX4shrqCXmgr6J5SohSTxz0o7jp73q2fDbTbsHg4DPZbudtiWtq0uolNJWiSyhoNELQexCkjuPTvVf+vfg3ab5Vc7hk+iWWysUdk9TybFIa+IhJcP6rThIW0gn9U9fHPbgcAB5XvP8AFTwDWjRS86RaRYhkEd3J2m41wuN4bZZEeOHErWhtCFr61LCejkkABRI78V4L4XmjFw1X3W4/eXbYZFhwpDt8ubq0/m0qShSI6OT2Ki8psgevShZ9qkDpb4KuSz5Jf1e1PatUZt0f8vZ46X3Hm/fhxZAQfv6VfwqyPQfb3pZtwwprBtLMeRb4fIclSXFeZKnPccF19w91qPy7JA7JCR2oPS6UpQKUpQKUpQKUpQKUpQKUpQKUpQKUpQKUpQKUpQKUpQKUpQKUpQKUpQKUpQKUpQKUpQKUpQKUpQKUpQKUpQKUpQKUpQKUpQKUpQKUpQKUpQf/2Q==', 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAMCAgICAgMCAgIDAwMDBAYEBAQEBAgGBgUGCQgKCgkICQkKDA8MCgsOCwkJDRENDg8QEBEQCgwSExIQEw8QEBD/2wBDAQMDAwQDBAgEBAgQCwkLEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBD/wAARCADIAMgDASIAAhEBAxEB/8QAHQAAAgIDAQEBAAAAAAAAAAAABgcFCAADBAIJAf/EADwQAAIBAwMDAwIEBAUDBAMBAAECAwQFEQYSIQAHMRMiQRRRCDJhgRUjQnEWkaGxwTNS8CQlYvE00eFy/8QAHAEAAgMBAQEBAAAAAAAAAAAABQYDBAcCCAEA/8QAOhEAAQMDAgQEBAQEBgMBAAAAAQIDEQAEIQUxBhJBURNhcYEikaHwFLHB0QcjMuEVQlJicvE0gpKy/9oADAMBAAIRAxEAPwCvs1MPGMdR1VTecDz0V1VDnPt6iqmlI8jn469OqRNeZre6BoTqaYnIx1FVEBGcg9FNVTHnI6i56Yc5Hnqspmj1vcUPNDzjr8FPt6lmowSf/wBdfi0XPjqLwRvRAXIqMSnHnHW5KbPhepJKPnkZ63pTL4A67S1UarquGGkPBOeu6KgZlyqE454H7ddtPQyzHEcTHCljtGcADJPX0v8Awj/hx7eW7tXbdU37TUdfeLzTpPO9Sm/Clt6KFYYXHtP9wPt0N1nWGdDYDriZJMACrWlWDutPlptUACSa+YklC6Eq6Mp+xHXO1ICcdfQ78Yf4TKy7SU2uNA0IMsUJjrKWMKqlASVZckYIyc+SRj7dUMqLZJTzSU8sbpJGxV1ZSCCPuPg9T6RqtvrNuHmt+o6ioNTs39JuCy7t0PQ1BfRZzkeOvMlufYXVCVHk446YehtBVGp6qqqGXFJbkEs+RzISwVYwAQSWYgceM58dNfuDpWgt34f7X/AbBIM1lPPUVk0AimFMY2Me87skNJM5XxkcgYweu7m7aYcQzuVED0moGFLeSpY2SJqrhoznBXr8NEQDx0QtSr/2deGpAR46tFquBeVCRUpLDPUhTU2Pj+3XXHSAHx/p1209J49vX1LNQvXcivNLS5I46maanzjb8eevNNTHgBf79TNJR4HA6tJTFArq5rzS0mSvt8dSUVMPOOt9LTDjC9SUdID/AE/6dTJEUvv3Wa4Y6UsBgHrshpB5PXdHS5HjrrgpfuOpQIoa7dVG1MtPa6Ge51b7IKaNpXbGSFAyePn+3WdBfdPV9W1TT6B07bHr6y6SejMA2xWQZ9RQfkADDEEY3AZBPGdIGv8AGqtMuvw1okLjck9e3t1861fhH+Hbes6eL3UuZJUZSB/pgQT69PKKJKmgIHjjqHrKA/bpgVVtJUkL1CVls8jb0+8k1l1rqAxml7VUZHDL1FVFIeTjno7rbcTn29QtRbm3HC9cKaIpmtr4EUJijLE9e/oT8gdTv8PwThc9YtIfleoiiKIG8naoZaM/I46nJNE6ghslNqJ7VN/D6osIpguVO0kHn9MH/T7jr3HbZpCAsZOTjq/3ZfsVeotA1WnNTLFJDchFLHtDAwxIsQQof6dwTJHySc5yegutasnR2kuKzJ28utFdFsXNbeU02dhM9Aek+tKb8Nnba2XvRMF5qdGVQuNI/wBNIZYdsdfC8xf1FyOWQLsP6L84A6vHo64TQad+jpUjT6eMqEGBswPavH6dcths/wDg+xx2rEE08cWIX2BcADB/056Dr1q2DR2m7xdqqqjiEKNUFGYDOB8H7nHjrINTvV6zcFUYJwJmtb06xRo1qEk7DJiNutEc98q9aWysstZTtQ0koWN5pWCs3wQB+vVJ+72ge16dwzaqphQW+lphJLLCm2onbYH3/PqcLJkYBGBzyMh9Z+J/XNXqJ7nWzVElPFUGeKD1SoJUHYG4wRk5IH7YwMB+vNdar1lqWHUd/jCR1ccbrCpG0R4CsF/7VYhjj/5dOeicP3enulZVygjoetZ7r/FFrqTIQ2iVBWJ6gfvV4H7RduLRb7fYNK0dqSS5z081wemRFkWkiYSb8MSVGI03N5IHHPQv+KrTNmqtF/xXTlzoY7dGIKua0STrECiLtjKp4BGEUA8FS3yB0pNM/iPp6a4zQC0W63wzUiUU9wKu9T6KtlgjHPDkkBSMKD84AMH3y7kQdy9N26XSqyUlBbfSobiksgM9XKUdo5GI/Oo9OTPwCf1GOLXR79u+aW8TAMyc79/kAK7u+IdNfsHW2gAoiIAI+XpMnvSBua09bc6mqpKRKaGaVnWFPyxgnO1f0Hgdc/0OBkp0+OzGkOz01PUJ3Hu8huVay09FCpKwUykZaokk8e34X7j5z05+5lv/AAd6StTWKki/jNxjRpvWoWL7pNu0R7wcKBgHGePPnpuuNYTbPi2S0tR7gY9ZpWtbQXVubnx0JSO6s/KKo+KMA4x12U1ExbBGP26IWtK3i9SQ2G3SJFNIxhgL7zGnJ5bA4ABJY44BJ67LPpy5XGtjttvt81TUykrHDChd3IznAHJ8dGwUxKjGJz0oI7dHZOaiqW34wQOB1K01Gf26fK/hE7iRdtl1uaJo6yNpnqLZMuyWOFCw3DPljjxxx0J6W7Ma61NbornaLM00ElZJQH3YZJEXc24H4Azzzyp6otaxYOpUpDohJg561DeaXqjakIUyqVjmGNx9nPUUDU1LtOAOpKCl+cc9ds9qNDXVFE0kcpp5WiLxnKPtJG4ZxkHGeuuno8eOiSVgjmHWk66uClRSrcVxxUpIPHHX7cKqkstsqbrXyrHBSxGR2I+AP/B1LpSleSvS6756jtVk0xS2qWoia5XKtpfo6Tf75mWVWXj7FlAyeOf06qahfI0+0cuVn+kE+p6D3qfQbNWt6oxYiYWoAx0HU/KaV1sh1JH3Fi1RatNVFy1AaeM2m2RxsV9SUM9ZMyPuzHHIZoj8Eq2Qu3rOrG2+3DtL21OpdVtF/jDUcFdTU7UcEs8tdVTMkqIqRqCI0k9UHC8bkGc5HWdeY7u9cdeUtW5M17btrRpDYSDAEUc6w7Y6h0i6pd6HCSRpIJY/dH7hnGfv8ft0EVlpXBwvUJdvxlXPVeo7wNRU0VPFW2a3elCobG+KVi23/wD16p4xx8njPRXqDVGmLfqu16Wpq5aqpvCSyxbBxGEQuA39wDj74P269B8NcU2+r2qXnVCSYx1nAwdie1eOeKeErrQLuLNKlNkEidxyiT6gDM+1B9fayCcoOoKqtg846OLNcaDV9gpdQW9AsVWm8KGDbRnHkf2/165qy0kHIT/TpwZWi4bDrZkGg6Lp20dUw+OVaTBHYigF7djOF4+3X5SWaorKhaamgeWV87URck4+w6K5LWcEBf8ATrfYpqrT16or5SIfXoZ0nT9SpBx/Y+Oon0lKSUb0UZ1BKlALOOtGGiPwrdw9UXW3xPCKGgqZyklQ+QyQqeZNpHzzgcHg5wOvpHpizU1FZKGlEvqtS00dMz5zu2KBnr5vVPfPutPeJbmup6yOKaTcaaM7Iwm7PpjAyFxxxz0/eyffy/XOtt2hLJbK24TyrmSaZ/8ApqAC7kjJOBnOBzjAGcZyvi7TdWvWg88U8qATA2A6yTWu8FcQaHaPG2Y5gtZAlWSo9AANt6tdX2SOsQ+i21mAQt9lzyB0ndVWTTtwlvWh9Z6cSWwTrEgnMpMk0hlTbGAuGVuc4Hu9wI/N03rbdqS4WpFtNd9Q6IIQ7cncABuOcZ85P7jzx0me72o5a7V9p0fNYppKWruEdPPURxKrOdgJZSSSDllUHAPu4OM9Zgy64hUpMRmtgcbQ4koWJBxHrVUtafhlur6zvFJpDTlRbaWCFZYaWvqELAO7IF3MRknjaRxwRngnoJp9KXjQ1FBfI4PXmlMwiZWVpQqPslcRnPtjMUgZwcH3DnacW37wV1r0tTxUV7s9bKLkTRxV5nQCYovulAkYE+5UP5lUtsJf7V/uFqXWlzs9tR6dBcZTJQ1R3RRplgQ+wt7YhuZvcSQCMkEcnneI9RuLcWzi5T9THc0AtuFNLtrs3bTcK6ZwJ7DYb+1L7UOldP6atcNfX19DGsUSV09VNNK2d6IDDIqpxiX25UH8xYEjhNeiNF2nX1qqrxQXqO3zR1EkEtvn2etEw24LKCMIdxwcfHzzg41H2L1RdrLdRbaSumt8U5qqqlpTtaCAkyIgQPl0aOInGRlW5OAT1z2OyU2mTMLaFmrpsmrjrKQqaDYojLStuX2kRqxbY/tLBSc4WW14m1FlqEOmRtMHGO9cXvCGkXThU4yM7xIz7Go7WfZf/CmnaC923U1JevqIzJVJSIStKMqAGfOCcsoI+Cw+4JXpt7n7nos03Hc6i83mLUVZWgF45Y45pWeOpn9EZDbDyrMWG5McNxn46FtNPVRevRurgMVl9JT6auGKsFJycbgfJJGQCSc9PXC3FX41f4S/V/MJ+ExAM9PXt39ay/jjgxWkNf4jpif5KR8QkkpjrnJHftvtRH2Ep+01ruldd+5tdUoKaNfpYoEJMjHIYEeCCDgj+/x5YPZvWvbbRvdi73ytrZK1HrjLaqqWLbFDBKWaZsEZVwNq58YDffpT2jRtXdKqOCH04w7hNzngEkAcefJA8fPVh9I9rdGT2K02JtHfXpcpKdK+4TK8c9O+4eo3JVo1GGRVxztbdknCz8R6hp1qXEvuqUXISUpI+EDc+XpOZoZwlp2taoGnrVlKUtkrStYI5idh1nyMYiZnFMTV/wCLfQJia02dPq0qJ/ppJ5DiIRnO5scn4xyMcj456IrR3Z7V0GjLhVabrKSNbdC5CqqpuYLjcAcZyfnHVftefhep9MQ1N3t2oWmoYKWScb0A3NtJQBs4Kk8ZOD54+SlKalmXdtdxkYPP5h+vQmx4a0jVGAqxcVygifPyIq9rPHfEHDlyUaqyiVA8v7gicTHyrbVsa+uqKogk1EzSsTjOSc/GOpSx26kmuVJBXzGGmkmRJpQOUjLAMw/UDJ69WuzSVMcrK6q0aghSDlznwP25/brtuApdO2Cuv91cRxUMDzHcwUNtHABPGT4HPz0/uPNtNkFUQPlWHBNzePpLaOYqO3czt7mi7vBqb8OXa/S31VBQ196vH836KjWoZfqnKkL6pyBHGhZCSOThR8nFL9C6d1X3S1PV95dcXGK43OYVElB9O4f6RQpQzRxjBkWHHpxhQN88iIpwDiBul/vPc/U8SXIzS2+rf6mpmglzso43bcYmAZYlQggM2QGU5DNkBiWnuRZe2f4f7lcrbUqdV6zElDbTGm+a32oPsXc+fbujWSTeCN0lSSd7b8ef9X1d2+dWQ4pTaT8PMZJ8/ft7V7J4Y0BnSLdC3GUIdUPj5BA9B1MDE9TJ60P2bu9Uay7laRS63yktemNH1sf8HS47ylHDGqKPUeABnkKxKSxzuY8kKcDOtn4WdOdqrh3Zg093GmoZaeT1WozVVQaGpqjIvps5yBkqWO1wMkjjOOs6WEhS/immcKB3IpJ3CKoq3t9W0r0ppYKcwGRwfV25STGT/SFyBzwAfGOuxO42pblqxtUx71pre0kkW07/AEj9P6aMQvGAMZxgjn46HqKpvCSrqFWpqkb4wh+nLKWwDtLkqik5UZJySMYz1yV12p6CqgpKameFaqKQzRuRI53erGp8ZX2bBjgk4P26itbi5sxyNKwCFDtI/boe5oa5aM3A5XUg4I9juKZ/YPvM3buCTS1+gmalWVZUxGzbUd42YBRgA7C5z8n+wHVptMaotGrYaWCGcfXPbqatliPBAliWQf24YcZ+eqE3ladoKW5CnZ2lVjI8bH8wWVQD8bQV2jxj/XqWsvdO92G+R3+nlCs9jp6L2KXU+nRpTIzLkDzGOfg5OD460jhbjl/Sm0sXPxIEADtvPrmKzvjL+HFtxC4q7tvgeIye5ERPtM1fWS1DONv+nXM1lB8r+vjpd2v8Slhj0xaa6408c9dU0VXLVRJIEZJYDt8HPDbSf06ZGide6Y1jcrpbIK+BJKGrnhjBP544lTc/2xufz9uthtuJLC9A8NYMx9RNed9R4a13SAtT7J5Uk56YVyk+k59M1pFjBP5eua49zLp2Eull1jZrilNcI6gO1MysyyUpBDGQJklScZXgkYI8g9HlVHZ4Glhhu1E1UkAnjieQD1M524+SCVIBHkjpO1mmLb3N743GnvzTVVks1CYaxqaTcGlKbgo5wvMgRTjHsQ8jd0B4n1hgs/hBsoEq9BED3P5U9fwv0O6vLtWpugjwyAjsVEGT/wCo+pFfRnQPdXQvc2jp7+9lqbZVpUtBPunEShlUOXDbl9WM5HuAPBGRg81N/E3+JOWm/EG1Lom6ToLdDTW+OQv6dO9SzAs8ko8QhJVxyPeAc4GOlprPWeo9BaBumkdSXJahZKCE6fqatUlqKWRwwmhRifUw5SMEAhRlvOcsrO3ej7rrm+2PUtbTTmx0UsMdTGaoqzNuO0v5Zt8hZlwCAkZXdwM5kxprLSVXCz8MGPlP9q9BrvHnnAwkQRv+X96sRrjXF47nmmgoaquoGphDJWAVIkpoy8j5jWXI2OzxFiS/uz84wOaz3qu05Taeq1kkligULTsTtPoxkKQiNgAtjkA4wWBAxxHXOitv1F6ekpVo7NIVmpInJVnqkdJBJ7TnCuJcKxBwHPnzBXnUl0obetnG6ea3My0MaSsypHudxEFOcnL84XndgDGB0FUUxR1KSNqcGiNZa8SoM6UsU9LcIiaqKZYhGtFTxzsAyuw35QsgznCB1/oAXu1P3A0xedFLp1ZrLYLvcKqT6qdKT0i9IsZ9FCxBOf5boqE4ztyAdo6XGne4MupKq7i+II6Ouo/VnFLGYTHIYZfSjjVVBWNWkYYJwPcclsdRut9G1D3u4ivu1BLDWVtFWmqaACXbFKmxXA/6bFVAG4FslSWGWbrlISVZrlUgYrs7baQqbjfZLFdUhNDQKK2eWqcxS/TiJvUIdxggKNyqCRkFlxzg1r07f0ElQNNmUwUtTGBLVKjirfjecKuNpdcKGBO3OTznqNpK2urJY7BQU8M6y0ywunrqBMHjRWjDsfzMUcc/GD4z1u0lp64LqSe00lH9PR/Wera0mUzMsW47F24xIxUjxkk/26/FZSrmGDXYQHE8qsjtR1QdrbvdLnUVcdFS0/oLHVQxieLdMR6fEW3IIVc4JBQEHGQMHk1Tryk7fdxrvbtEwVtBRx2xhPGpeaOpkTckrQKAC2FkwvIJIyMgAg0udlscdmqWeS50F0rIfTgqppjDAfc0TPEYxtBXhcbR4JPOV65dN9vLJUU407cxVC9UtW08UbBWgqIy2DCC+Q7MGkZ1Q7TwNo3DrpLkkqczNceGEgIQIA2qXo9P1t9t1JJebtFU2W6xyV9PHCp2/TyRIY5UQt6hYbiNrKDkkYHI6BaftBNJU1klHTGaOBiRj3YX7/Yj9R0ytI1M9vr/APDNfTCSCWB44al4XHqT5HqxZbG1c4KkHyHwOchm6euGkbbAumY7n6tZWtJDIduHZ4wAS3jHHGfnIPz0W0zXrnSQpLGQrp+v6UrcRcIafxMW1Xg+JEwRvnp6Tmq202jfpXMRp9h3ktkdIP8AGzdqbTHbu1WV3rVN2uS+rFTSBPVgjB9QEnjjepXP9QU/HX0R07p2hqaeF6eWkqo6OeYVSOqs5mXK7WYjJCvu8/H6dfL38e3c+1Xzu/Npm01lNR02mYZYp6qnPrw0omXbLIoIH8zeBGFy3jcAM9Eb7i4PW7jakmVAj1nH5UuaX/D1GlXjVyhQUlBmIg7GPrSO7fXa46P1ILhYK2n2XnTtZYZ6OR46VnppY/b6y52AruwVkO4vCJGyHHQxV3yxXxIK+3UM5uHrp6f8smJadGeOJdvyUEcQweMbuPjrRV2ytqq1hTNVTp6cr+rURMd0bROTNgtywGGAywGP1522SqodMU1PadO3MXKQiaOonJyJFnD4KsOWGyVGxjcJEI+/WZvuF08537fv9K0sGrS/g2/D3au4d9TX2tYainotLPDU0dJPPlZRtc+sQwyY3dTICOMrxx5zpA3XVmqNBC79t7bqSvhapqVN2lgMkXqSQggUw5b2RszhiCAXf52q3Wddsq8NMVYBSnBFLQNZ7JP9DRIElmpXmLVs7emMk8IQhyMhgScjnGSMnrnu9JZI7QtwgrGF1BiqWhbgFCpJCZPuAIzuAIwfsBubdm0BoruhLHUaevqUN/urJWUlpiLPS0qRJIGi3Mu8IQgVhnK5BG4HAFtZaKGjGm0rfoDbqgW6MGqWENA7BTIUZl5ABAG4EkgAkA7h0LZ1Nhx4MhR8Qbg7x3jqOmKhUgpE0KU9BZ67tzU3KeWWW406U7NIGXao+oEeADjccffHkZx8itdDPCqRNLgBWgI5UrsIYZXnBO4gZ/0+J/TFzlntVzp/XkqKX6RYmQxI7JsOd5wOBzkZzkgj7EZFYL7VmWFhMKWeJpEPp+xWWWTHLDLYC78gn2gefHRJpRYUsLOJnPYgY9qiCsmajrjV1k9I9DHKPVRZWQiQMXLyEMFwBnPH34/fok0F3IvulrHdKmhqpIKiZZYy5bORLtVshgR45GMHODk4x0JWuZlqJqmSRJTHRxuxY4CfzVU8fJ+MD4Ofjr00YzcEooiKVJ6gxpu3lOUwN2Bu+3P3zjq604tlUNmP771Hc2zV0ktOgEHvTZru9F/tt7s9dRXbIoYaGRRu3YcyLUtgsQc75HU/Ycc5z02OxOrX1BY9U1VTXolxXWlkqZDGVFTVJNW00ZBCjLqCm4/cyHg5z1U+8zT22WGqqI8tXrTTxvuIVkC7WGFIONysCP0/t0QaM1nVaatt3ngnEJrhFGAgG3K1UUqknkZAhbAxyN36jqT8Y+pXi80zE+2PpUVraNWiQltMAT9Tmre95L4e7eoNNV1rqPpU9SI1CMBtpnZKeTk5PuRGUN/8kP64sZ2Y7X0Gl7Pa2lmFVOh9e4rMcI+/YWc7c4UFdoA8gsfJHVDO0fdSCx3WgpjBFUUU7LMZJ3y0SOghZADnOPSQ59xCr5I6t5pb8SOhrtNrZLPQpFR2ySCpq65pPY8WyGMIBvwZHcj2cLiInPJHRQ6u3dWyGEmCBkedcWzSUPLeO5PyxU5qf+Ma2vN7qLdSrSwenUvTl6f8z7WkO0k5YYKg5OckZx0vbBb/AOJaga2W2ie41EcJlljSQyYTDBo9gHBDPA4PBIGV/wC0tLtz3K7e3zvFUaEtt5iMZo1gghqI19GoZ6dtzxgSe5WB5G1Tk5HHUxqL8PtfbtVPftMXqSapWJZaqCHMM0qAtgoASGOPbjjx8nkVVqKXOVXYUXbKVtlST1P0pO3PTNPW2GsotNS/SVFcR6c+HdIpHwgiMnG1ecZx8qx93j3abRrXSF6aLUc9HPVzyQyxpBMvpGI87uGJIcPwwHnAIHPTwsOjdJ6cq6KK4UFetu1B/wCkgoYHBeF9hlAQEAkFfWwMDIySMYPWzVGhLlYNJQX2ngqBHYaoU63W4GOHfRmRSDgDCBYyVKkDOASRliJufMVDGJqAlsU1JaqCojh9AySPVJMyYdfUVdyBSQAMKWHJA9Q4+5OLTsFpstbcknpyE9JWIAn9XduVxjjGwOBnJBySepjfoi6aot2m9Va4tkNxqbdR1VJb0LoZvUQMAjNgPkKSNpPhRjnlnQUFhqSt6v1MkEk1b9PRn1iRGIzuRhg/ODuBz/Vjz1wtUZIrpCgrApYVqVMUtRoShgmrKmjRK2IjDPGSA0kSr/Vy28MACNsjAZJBJtF2Whvtgss2r4qiFrVcnpUnZTG8u5DJHvHkjLbxgjyB446k56GzaL1/Brl2danUu611ClsqtWo3HAxwSqseeODjzyG/iF7t0HZjQdzu080UlR/EIBQ07sV3TeqrFVIzjEUcrA48KB89RhcxBivq4QCpXSjO66c/wnRSrcK9JYJqpTBVbuULSocHjG7AOPJ9h8Y5XOotfW6y6npIIQtxlr7kaUPSykrAY4zIwLAAINsbHa2PJycdU979fjb1RrptT6X0xHTy2SKm9SnqIQwYDb6YYbuQSJwSG8MPjx0ubp3culNqzRXcPT0Gbnp6iC3RfUdA08RUAOGO0s0QkRlyDtycZOBWOoNokzt/b9KrrdJMIE1dL8Tfeu79te3F3/gIrLfqC9yxUMTRyelsgkLOZ2YD+lCqEnOM5Gfn50611HpuD/2qnjS7o9QJKy5N/JFdUqoVWJKgbEJwFLeXz5wOuv8AEH381r3u1XU3QVZp6tAIEo4lJWngSJcKrZwSSkfOOSuS2Ol9oyBovoa2+RQVFtklBmhZcKxDjKKFGC7CNfJB8eAcmndK/EHxl7DYdT99KkBipga8gppIrBCI5q8yLClSgZjNFsCopXkbgmMAed/nIBEhorUNFoS+U2pdT6corzSrH61LT1BkXZO+142AUrv2lkZgw2cEEEsMRFq0rcrvU1NXaKO1x/R1syR0lMpkljhMe0yjGWWJSMjyd2FCkYHW7VGs6G/fRR0HrPTUVG0fqT7FIndyZpMgrud2JY7icKRGPyKxgATPwe/rXSO9QSyfxV3p/q6RWlqllklZGZ5H3EEb+XPJbgjkk5zgdZ0Q0FgjrbLvq3roamfYKCjpIVNSZNwPqMAMIrHGCTvww2ghsjOuS7zEgGI++1d8qjmpPtXp/Sd81Ba3qtT1+kp5t2yWjT2Lldw9+4bCApzhW8njPRFV2rVuptfUktTR0YDJTwNIKpZ6esgh9ox7mYO0SLkHaDtcEBlPQVV1tA601IaoU0cdeHNS0hciMqiEEDGAFG7g845yeeimv7mVWiL5QS2cWy52+giX0pHpg8sEn/TlWJ2HtyrDlcHkH9CAuW31ulTOSpJAnod9/PGD2rkKTISvH31pd26wDQPeOtsT2ievZJZ2WnZsF4VBlXKqCf6FYEHxjx01u9AoNL2+3UiVFdTNV0sTTROR6MSGGnR6VfYGUoFTcTgBg35hgAqoO5faXWGrqDUOpbBHbLpRRnNzeEFKeQBvYGHLDYdwB+c/OMh/f2aydxBartpO5isXEsbrGGDAhoz7kPuUYBAbGCeBzjI9nUX7q/YTdsqRCYUo5TIn2zG5z+dRuoCEd/2pJTWRLfYRqO071pLnMtHIksqD6d92cEMNzqdhOQo2BlDEkjMfSUdXSRXUTNTO1G8y1cTAM5PAzwRkZHBH/PRrU9v9Z1lojtnrxUcFvmeV6eV/TIQtuE5kYD1IywfBycYb7568906mut9VT6diMdaGoIvSucMJ9WtPpKsm5gxDAEEA/bBIOc9OLV62twNoIJMz3gf2/evvOg5FRmp7NNXaZ0OtOKhp62meKNMq0e/6khUBxw5DbiCcEFPnoYrrY/8ADrjT0R9UQTh2bPlUcoCQfynL/p/r1PahuMtFpnSVoWQ0rU8kk82JGEkM4bC5XgKQAD8HkZPjr126mpp473Z7olLFUyCFjV1SvJ6ZNVECFA9q5Yqd5HABx5wZkS01I6H81V0pI2T61AWu61NOlEaUzLPEHicqpOXwSpBIx/X458Ejz0RUN2qaW2yRwjYxqqZ6gjKbt2cHHg49w5H7cg9MvUXYTU3bqyNUR6ptc10imj2UqVQgjkTYxcSmXaNu1TtLbQccdJiz1VYt0nsN1M1HTs8kVRBsO4OFbAK/owXzz+3XJ5XgpaRt9ep/tURY5ckUaaI7q3iwdwLTrGmqy9xt9fBVKXOFYRiRQoHhRtwAB4zwPGPodffxaw23uJYNlRA1LNodL/VuH2N6jL6uwnJGCsYwACcyeT4Hy4uenwayKWwVorqeFCWmZfROVY5IVj4/f4P26mKfUl8rasPHGJTR2uqpEcU5cmMsy5JXyAsoAJ4AAHxg2OczzJM1+QfDkDavpZ3b/FBp6mtlg7i2SEzV1DrKcx0s5VWYpRTRzEBTnarVKfbORjgjMiv4ho+9VmodYm8pQaXhoa+1aptc20LV5pJpaeaLcCu8mBo1DA4PDBhw3zZt2qrpWW6kst1q2lQ1ldUmOdtwjnl2KGY+RkxoDkjG39ejLT+utS6W7O11JR3K2rHX1kZloncmpaVY5jHMIxjCgS43MDhpU24JOLn4tIQEgfEAfr9mqpLpeM/0mPp/1Ts/EV3M0trCo0R3I7Z3mopJKa3JZzSLLsqKJqGTELtg558jk/kBz1Yvs/8AiXq9cdm5rLNNPPWUsry+skq+u8kcbSyLFnC5Uxb03DG704/ByPnGlNU2rTBqLrHU08E5mekZ43xMucggYAbktnByMHqKsN4u8Fno/pK6WnQzSZAYt6kZIwNo8kMT558/36nd1JtbLSDmJnuM1Ttmn2rhx6N4x0ON6+kt9/HDprUNju+l6qupXu1ujjEsqD1FlnFLJMs0LDb70eHYxwoJcMvgdVY/EH+IO+d7bNp3TMt2nWcXOunuVOimOON5PTihClsl9kby45JGcEk46rvRRV9DWVVXVl56iZkMUcaBmY7Cp3Jg7vzDgeeecjousfbXWGptR0Vut1NULJV1cEj3apX06Sn3qrMWkbantf1uCScqMDP5g9xdDOQE0VSXV4VXPTXW1WOripaGuqZZq90gnclWLKHQe0KPcCFLYIH5iDu4Jk6K2aivtxuXrSVywWiE7XgiDK8LPgbwo9h9zPnj+/8AX1IXTsRqmx6XGoqegN2pTPGonFCy1azNGQh/LmNMk5VmH5RuJwFDPsOlL6nbSS2JoihN1u1PFSV073GOLMaiHCudreovDBlJXG0+chugj97bs8riVgk4kkfSflXQbKVRSi/xwltuktVRabo1Y0i0dXG4CRygRBdiFCpBYAt+bIJGfy5El2m0Jeu4Nxgs9DZ0nt9ctXGpryKenVljcNMGB9zRBkYjBbKjx5Wx/aT8GtBdrZLddYUdI1rpInq2qVo97Ki5zHTqGZmOWyPPu2kjOB0xe82j73Ho9NL9pbPbLFZYrTJHcq2tiiG2ndFM8cKKckBMl25H8nau48iVl9txkrQCE4EnqPIb/fWrbLBUKp3qGn0ratT/AOANLJb6KGg2w3a+wVA3VnkyHKlso28IEG5iEUDBL7g2rt+k6B6eSmqah/WpxI8DRB4xL7ticH+Zjgk4AzuwOsqpzR0dRHT0siTPD6URO5tifLjxyR5JyPjg9RtsvtBQaoSsu8cEklRMYKqnm9sjqx9jBlXnAIH+v366SVqBUgnA+dfCoA4FfsWuNU01R9bZaqWWqpZZJTW5CzRSOBnbIR7TxncDng889Z1OWTQus7v9TV2yyPU09HUMxjpa+H040BIw4ZiC5wMLjJx7Qcg9Z19VdMtnllPua/BCjmg+i0/Ld1uFPZ3lnEKer6MowwiXGW25y2OPynwfPRpYu3+qO4EVMNMWusulZbGWCpSmTaUgcFMEbgDyuc/DOu7II67aDsX3Do5BWVNtjoTAgb1Ipp97kHgkkkfYcADgfPPUzpS0909KUlTp626kqKCCVWLfTqEV9q7tpYAcsE+ckkAH79VndRYWf5TgMftmg72rWlsqHgZ9KgLt+H3u5TXqOxVEVNFA1YIB61ZFFgRZjifYXyVZSoBVTkHGOujU96n0tdLJpTT8lVCbSyL69Y0R9zH05WVonIVX2o233EZzwCc8uqqbUEmoITcpfWqPW3yE12/MhAyeBxnjj46965gpbfb6CSWyUK1BwXYMCzfqcrnr4HF3AAWARHbvQ57idhCkstIyrzFGlvqrtTa2t+pqnuFZaejnBp6i2CdZkGIhvZA2I2yzZAJBGOfdx0Qai1poSjr2Wo/g31Cwhoo44cxLkHI9m3HxgLgcnJGMtWe46qrPXQQ2+nQocLjH/HUPqPU18nmBeGMYQDO3OMD9euFcPLu+T/KAAIGB9xirDd3cPkJDYE+dNe803bqaG53WsikrJ7pVTbXWIMYwcYwCwHHGD5JB/XqD0ZeO3mldRi7Vdmu1RDBSyxvH9QAkxYke701UkAEDG7HH6dLyouN+awwMZWQF2I2qBnrq0NR3O41kqyTSsrL7hnPz56YbPQ3myEFRM10bh9ltTpUAE9vKrIdze8+n+4mn4rbabPX3GrgjkhMwjd94em9MZMhJJBA5bcc5Y5zwsRpW/az17NLFpCpUtLBPsqZcRnYiqQ3g8hR858/c9Xf/AAydhrNT6ehqb7So7yOsjbl4GR8/55/b9+iHu3oqw6aubVFuoirygKdoC4BOP7c8dGmdMaQOQz86hXfXbqOcneqCat/D3qa100dxitsKI0jgU8UxKoCf8z+5PXin7fXKy00FrqaWCIVDvUNMrukycLkblYAjIzhgQOcYyc3A15SxU9lpBtOAqliDkn5z/t446UGpnpv4pTAiMqIW5JyDkcf8dGLfTLRQlaZ96DXd/etSlC/s/tWdurfSrbx9RS0Q9QCGRSzgSbB+dwGAZiSSxx7jjOcDEvd3dNISCnqqWOpepIknQfzdg8KrNkqAAB7cHgdTOg7fbpKNWZIslyfy5yTg84PjH+/Xdqm126HRzSJSxY9Qsp2jB8/P/wDOrzOlaalchlPyFD7i6vfCJ8UzHc0rqfWetKyglsdRqaaeikkLmGba68Agfm/Qnz9z9+t2nqa6JTIJa6OpAqRMoqUWQq4PBBYEjGeOvWnLJa6y5KJzuQgkgDA/z6a9g0fpgx08L0qFjLhm3Zz48/bjn/Po9b2WntKA8FPyFLdw/qFwgw8fcmg1LJeq2WoERpSCpVjsT8pByPH6nonOhNefw2gWh1ZXQQSVeY4qavljiD/fahC84OeOcfPTTt+j9NwfUGno4dy8/wAxdxU48D7H9OPjoyhsdFMLbRKFZRP+VeAgwCM4/Q+eep7hqxUY8FJHmkftUdsxfhMrfVPko0jdQab7l2u3tS1PcbUkcUm2RlprzUJ7wMD8sg+55/U/r1FVZ7gWX6P6DuHqh0lUPg3uqPuGMHmTzx07O6Fmp4ogJJwWA42YwcfIx/bpbVtIM02M7lX+rx0W07SdNu2QpbCP/kftSdrmt6lpl74CH18vYqPaoKt7+919NUs1uu9ZNfaORGjeC5SmojdSQSCr5ByR8569ao/HBWX3R0mite9uKC70E0DQNIZnjqghBUgTZLflJHOcf5dQWt6YuG3IDkkePPSY1dbo5IyGQA4wP16qavwNpLyS820EqI6Y+m1OHC/FOoLSgKeV85/OaJKfvF+G7+C1NrqtCatshdyyGjvonjjbfkMqyx5DYyp921lOCueetute4v4ZtX2b+G2vU2o9PCWtM0u2zwzyiDaQI0kM67SMj3Y4CqMcDpBXKxK7uAvHQ/VaccElSP0x1ll9wmhlZUgQfWtZttVW4mFOfMD9BV4O23dnsDp20x2m365rWgWPZ6stNGsze3BySPPjnJ+c53HrOqWaf01LJIFMg5b5PWdJDvAbL6udayfUJP6V27xELZXh8w+VP/UVPcE1Clmg1deKiaqUNEsTF0fCjd+/z+/HjrfXfxXTMQuMF+qJTCpELPKY2D7Q25GJxnOB85+Oeenxqnsvoe06Xqb9pBK2W4T+qDTzIZmgEm5f5b7jt9rcH4GeOeq06gi1FQXoWe7UpggpU9WWWtjDj0vzRyEHx/SP98cELGmX7WpEBo4T/UDy5x/tn/uol6DZLe5kEyIgY9Z86g6vTOrNQvba+C5io2UyqkkRCgqOcBiAWwTjJ58D7da9QaQ1VHDH9XTo5X5L7z/v0aaLNDervpvT1xuU06VdWPp4Y2EQhEjYZVDAYG0AnGcbB54PVuKf8JelrhRLW6b1Dc53hQMYp9hY5x+g8fv1oWiXLK1Ft1PKR5YImgOs6RqtipL9qkOIHkAoeW4r52tpi9NMrSUkmB49gUf38fr1H3jTddJMPUjfIH6/7dXZ1J2kpNN1EjVUUxIZlIJVQoHBblQCB584/XoTuPba21kkm2nLPGxV2UhhuH9J489PtuLRahbmOYiR5jy9OtJg44VbuHxmynlwfI+dVkfS8jadiLrgJ5GOeepvt1ZDFWxhojhmC88eccZ/56cDaGpotTLG1t9OmaBY0lDDIGcuSD/YL+46M9L6C0ylxgiI2BpFUyJwFGfOT/YdXmW+dxZUmAkwPb7+vlXF7xggMhoAkrz8+lW2/D7LTU2mqWeomiAkRMgefGPBPA4HxnnoS79zNNqCCCGVmXePdtXc3u+w4+P+Mnp+doNCaPodO0xokJk2ghywYbRnnHxyc8jPS67y6FstdqilMcjIDzgcKuft+4/16CpUjxTTn+OUm1QojJjaq3d1KuYUUWAYwIxjceMnHHHzyB/z1XvUVxke6o6uRhcHGMknq23dHtvQVcUaRXOacqmGLp7VxjySPsT/AOZ6Rd+7WQLUbzVocnkDHIIznozaoSWuYUkatxMwxem3dkVwaPvlQKSP04ppFXdkIcDyByeepbU9bOdNskoAIlIJ3ZyR8D5HGD/99b7J28qIV20k5L7SOCMn9vn4PP6deNTaNvlHQvBM5Pvycoc5+Tnz/wDXV9poTzUPPFNo5/JCsnFAlhrZaeuVwGdlJcKWPP3/AN+mxpq6mOCOOZxG6zDDA4BPn3Z+Mc4/58q63aerqeoUGME54YD/AH56MrXRJRCImP1PTfADD244OD8H56KW6As5qtd6iGsNqmacFt1VBBDV3OO3T1rBz6dPGBmQL7tqg4O75C5xgc48Fm2QrcZaWtp55JYsLMAQG9KM4wRtBLHDhfnnH3z0kblcZrnav4PSXEWtREjhrfOkM0i7jhjtxvYcg53c44GOGj2ko6erjfVNQsMfpN9PNI0zOTINw9pZvYpKKSBnDZ4bIYKN09ds6l4pI8JQI/qCoIKQAEgA8ypiJOSPSmCwu0PWwaM8+DlPLIIJJmTgR9KlO5kcdPRRhIoZGBI+TgHO05x+nz5P79LZ6MNTxMUXOAA3POfGP8/PTE7n3WOuT0Yw0LsoBjcYDe7buAI5yC3z+3noHhKtGqgMMAKeOOB9vPWkaMCGBNZNxjdB3UCUGQKXmsaAGnaRN2Mtk/8A30kNdosKAOeccn46f+r2X6Z5FBG7H2/z8/qOkB3FAX8x8jgYxnoxeri3Jo/wgtS1pBpT3GqiExAds4zj98HqHmqQFZi2AowR9uui5pI0kjK20gE4zjx0P1RYRFSyc4O7P2/++sq1J9RJrd7S3SpIohsFcqzgsxH28Zz1nQ3ZJnFUvJPI46zpa55qG+sAXcV9brDboktNUJLnlzUNESVTlQSp5XhsHP68ngDoO1vY9CGtNmv9v/iTS00k6/kM0ojYKqYwS2ck8cDY5I46T2gu7EunbZbdPU1cHqLlBFW21KmUSu9THUum6SUng7kgZlA5G7znrR3S13p28ayk1veNQzV1xmV6WqoV9NJBGx9kUZHHqj6jeZBnJBIwDjry1a8Mvs3ylFRBJMcoiexkbZx2mnspYKRyifXpRN2zpe2dxsWndWQWaGiq6O4U1TTU6M8kkUBmCxEk+HBc+CARjOAObO9sNRW7VUX8X09PJ6YqXp5GZiCHBztAGfynKkjjII8g4p5aLzaqqt1VpwwwS1M9zighaGkd3pkIdo3Hpcq4qoqhucAmRQCN3R92e7jUvaHuFeKG50008+o64wxstS0yxxIs81NNlRtDSKSHbj+Zu/qDgaFoF2dPulpcUSlWYJkgESN8wJrp15AtwmPP3p/fiUFPaNJm51EMNdTRo8VXDGFDgBW92S4/r2gjnx4Iz1VfTlVExkmIjnp6ogwJCCyrEEduTlgX9qg8555A8Aq759x79T2q7aWvwFVQXWeS5UBliCMsR2FY2VVAXLEgFsZBHB6VnbW8WS5aaq5qNKmEx0UkcSes7xsMFVkP5QzDnPk4Ckt8FsZ1Jw6k1dNlQDcg9R1JjPUR9isU4otWb5l5aURJAnz8/TapWloaiee5XW5zST0zVLU1NDHG0BUoxQlWYEN7lc5HB2EDB66NAXSoqbjIFraad462ZFjDbitOspSN3QsCB7Wwx2g7DjAAJDaW6XqhstDfrkscNkoJ56+pAY4d/RZQEBXCAoQMgeUPkseo/TN+qLBXXS/6WsM7y09aYnetpAYpo5ZpDHEyhSoVUqyWLZb+UAAArdH169c2QStYM5kGfiBGCCcf1fSK+WPC7OoNupCgQOQAiPhUlQBTA/2588mvo12I10+oNB2u60dvenWupxIadJQ/pBsZGVUKxGSCcDxk8Z64e4Uj196jWCvmCkbVLu55xgZHH2H+f98g3ZPUVbpW1af0/dAtBS1sclHb6aXLzSTrJK0hDEYCLGYjn2gsx9wwOhXvx3ltFn1jpaSw1jXGknr5/qKeGRQRAkTI0jEZCqhLALxkgY4xn7ba6y5ZIu1HJAkdek/nPpTTc2BQ6bZPcx6CaKtZQGlpVBlG2R8bQhAZeSpySOPy/HPP7J3UdDcWrFIpxJEeTs55/wCPjo01bqGg1biG11Pqx0Hp+pK7BpFEqb4wBgHlSGzwCD+mOhako7mPfDPJOjru3MeGB8EffjnP6/r08adcsv24CDMzt5GDWDcWIca1Rb56BIM9JEj0nPyrusducUzBUCqRlsrk/wBv0589e73JC9M6zSLnPvBH5mPn/bqTt0ldBEFlp1zjjJGCMHqO1JPFPb+IkWTGGxjj/g/vjo62lKUSKR0OuP3IByJ6GgsJQSStsijB4xk469y0VKP5oMargs4c7RsA/qJIH+fUOzyU9WQwlDDOAVPHPJ//AJ1NXQ2ynt1LJbayWetqWEH0kiMGaUqT7dobjCnk4Pjj46/Lv2bFvxXjAOJgkDtMdKcmLF51YS2Z8pj1iajqma3ajpo7bFXjIqkRPp5R6hcjJIIO5cIH8AEttXkZBc2jEWx2y30101Usy0kSVK0NaCZJC3Gw4O2IspIycZO9gApPVf303q2DV6abvMlMIrMv8QrxJSR+pGUJCYLj3DeYx59x3f8AaOmFpS9vW6et9HBVxvSwQxxzNtP86VF2OSzncTuDADd7QAMDHSolnUNU1hKDyFtRKuYgTyjlBCRKu4gyCJJGacru5Y0jTQWlGRAAkxJKiCcA5g42wARFGOpbia/DerM8hyXWVuEbI/L7RkYwCcc84xzmMNUv0zIQHYAgYU+Sfn/LrluM1QyGWSKNZcqpOFGxQoHJ/wCT+/yOol6qJRKAFOMYPJ3AA/c+3nA48561y1SG0gCsrW2q7PiK3qP1PPmk2gEIWOQBn+/jj46R3cPa0ZIU+7O4Zzx8D/Tp0XxttOyGAcjGA/A+/wDr9/8AnpM67COhXcoGMkD4/wDM9WL3/wAYinrhVPI8PWkxcImjkfjAbOCDgeOhi4DCMQDg5wMfHRfdE3E7CD5UjHI8dCl1T2fm4xxk56yvUkwCK3bT1zFcVl9tQAMjBHjz1nX7aTmbKkkAgjnyc9Z0sARVu7EuU66W2aqfSsupKuOEy2aGrpnaSMB0qDKYoiV2E+7exDeQ0Y5HHUXFFW0VlFXNb43qfr2W2mWRjHWsJUWRIyQNo9FIf1IZTwGwZvW16s9qs9XbKFzOKvZVTx4CFT60ZdJiMEe/ZnaTneoBGDiC+rkmrLR9bDT2m3W9o6ZRJL6hLwMGZo0zuBZI8Y8E5AIzjpHYbKkc6xAJkem8e5n286Z2my4MDp+sTTCsF4tVbf5hpjfHPLDSh2QhpFhpZ6ZhJFK+ElYrI8pBAGNnI3Z6k9L9xdN3G4G0PYYYq6j/APxhSh42jqvppGX1VyTIqu+0KMDe4J8jKxpKSjoJI7PU3Kdrel1UrPT1MLwtDC7gS4dd8W4sAuV9+OQOMa9Xaho9B6rpaSy1JnaKKSoaeLEckysI2RJQpALs0HuIJ/OQOR0Of0dDpLaZkjGSD7kfKK6LPKnmwIxTj7i3Bq6nFy1Rc5a5KyjhEDRVCbJJjG7Mynx5R9qsNpyMckY3dttDXe3aDr6Old6kymFJmmg5K7yxVCTu3bSQy+P5Zzt5BhNIdxrZqOwRQ3m2UslPZlcKiyJTmJYiyx+nkckSMzDHO7ZknJ64qWtuMOr5tPaSjqhG1JJC9MjMrLVmrQpPkcMScZUZBCkYx1QtnHbMpZ5YCCCdoIGD6SKVL/SWbhtxvbm+IeoyPXP0pw6kt9ntvbeS3U1NA8UNvkD0lS2WlpcEKwUNlicMMg4yvkkMekl2l1FFM1VWV1+anhuV6pKWOsCoXYYCFY4yCd+HfnjAjP8AfrsnpdXyXinoa293GCkFojttFUrH6c240MpKOmQWJcH2kMPco3EZPQDRLJf7zcKy209oqBamQU0cMJBEUZeUuqoSXwXAOc8LjwDhlvNTTqiZOeUAd/hJ29dx0x5iqeh8OJ0xlSJJ5yFSMfGCIPuADnrOc1cm496qDSVnmvAWGovdnWtslKjtG7vMs8cO+OPBwS8gkIYH9MZOUtV6sqNW6luNFY7hTyW2wwi4GZV9XbLKJRICWJDR+6MkAZARMZOSVxcEr9PvNa7kaKoNhepr6hUT0mmadBK5fHuU/wAtODhhnA+MbNHasSzx3LVd79SFrvVEtSrJkzQeiIVkY8hQWEhLEA8HBHPS6bRRaJTkCSkdPiIjH/EGPlTKtICysCVQAT18/md6ckOoZ7Labvr+DU0FSl7v1Lb5qhwrlUWIQKBEMFSH9wUYAHGeOT/TUE+mNH0S3OjWganpFnq0G7ZFn3u3J4Uls/YZGOMdVSp6+utmn9O6Vt8yVcd4uENdIsTl2Sb/AKhdtp253eOc4i+3TW1Zqm83GG0WeCqq4P4/WhGjADTQrKkTzhiPBMzTKAcj+XL4IA6adJ4hXo76fFHMAhQTnAAVMx3MEe1Z1xTwMNZZ8JghPM4FLMZgJ5Uieycn3p62e9G62GiuzRIjVdLHMVU5A3oDxnPjPn+/QPrrUVRLYdVU6BaKe205jpikh9VnkhHpsABkHe20fcj/ADNtPyQVNrpvoZVaDbsiA4G1Mjj7jjgjyMffpW9179LGbdbKS2NV0d+roIauR32RQR00paQuPK7kU/bAByQcdaLrWqKttLaebXJKZP8AuBQRPzUkxWI8K6OL3XV2hbiF/wDzCwSJ/wCKVCe+OtEVPpqaSkpWleaWpWGMTs7lmLbeWJ+SSOT1yaiq6i20RrqqGsmjtabkWGMM6jOCwUcFgCec5xxnHRTbK3F9cLDPUrX+nFRyPKHWSn3SSGQccMPVII44VB/aXu+jaXU9ObLVSywQVXEjQMUbbnJwQQfH2IJ+/PVjQtVZ4h0cvWiRzJEDmEgKAwdoMYNd6q2/w9q6bS+JKVQZ2JSTnE+o9jSmvFN/FLPedY0lcaukoYo6SmNZI8JkUxtkJuICn3y7SGbkqeM9NrtxQWyXQ9hpHswM9xrXpa5oDgQVcgIiGAdoYbjwRk7FPlielfqXRMFjoFbTlor5IRVpJNEK4h0i3e7YWGAuPawyBtyfIA6bfbdNJSWiksFbFW1d2tbyU5WKMDdN7Nm5t3vziXbz8eMNt6QNTduNI1BoakhXiDmUFNhUmQrYJgDzkAYCu9PNs8zf6efwriSkmIXyiI5RBmT1xvM43oX1qgtmKNw8lUlTNA/oqwQLHtVSePJIkBzzwp+c9DttkB3GoiZVYYBOVLck4/QY/wBv36ZeubjY7tX1FTqqoqIZnjaU1cNIpL+1MO6k8YVdpXackA5XcSFvfrSLZUhoaujqaeV3EctOxAkI5zt8rwRwcefnHGwcK8VJ1dpCbhtbbpAMLTAV/wASJSfSZwcYpTvtKasuZthaVonBBz7jcHv07Goe9PJ7kdn3Bs8jHH+ZwT0qtbFypdVJB4JJwfngfH69MWvd6yRmUqUZs5ViQB4Hz5z0vdWp6REYLNsJJOM8/wDh6c7xYLJotoKQ26kdaVFzpTGSQM592fsfP+/H7dCt1p2j3BuM/tx8f26PLvTuG35HuPAI8kgfP9yehS60xJLGPIJxkf26zvUkAg1runP7UL0ftnBVCoY8Dz1nW6CB1nOI8c/b56zpQKTNHHSFKmiqPSq3/TzVs90mpFiljppCUeRJIzMv85gCdoXeCQATgA4OevOl6ymQRx01dNTPbgTNMsfvxlsEZOQMgg5wvK5xt6lL33GS50tfbq630VBSXEIZY4olgKIUHpqkcOwMf5UchJGxW3eN2CL1NNf4v/Rw06U9JPPF9Qw9wmaNQURkBO5FDKAvPJ55PS+3IBS4euPSKZDypV8FO7Q1jtUNTctX3jUVF9JNcKWMxLGYqedA8S5kidfcztL+ZiQqiYnBx0Da/FRJfazUcJtkdZNVPE4i9xjJkjb1RuYkuWkLEn8uWz5BOy+6kqdTVdVYqaL0aSm2QwvcZ3kWNjMjPHCDt2gSCQnBPO1QeSXF46tZ7ZXusEgqJqhYooCgeQKjhdsm1VDEpwwxlm2nHnEKW5+M7np8qncWOTw07d/zpj6cslVfrPctU1NfS2qqqfViq4HRIkZXCbgdvu/KARheJHLHyev2wauqbbq6Wxpc6iOjuaSUlxWsdlYPDEdjFgfJBRtuTznH26ztxJZJNOXsasun1Mbr9OrR4CSSwhFjYKUbPMo924ZVMADGegjS0NbDr2SiuMbPIzMY/VBOBloiGPlWIK45A4GPKkA/w4eW+hzISMY8sesH9KHOtFKtulNda7UP+JbDRfxVK2lgeCnhrxIm2DeqK9RGSBhCJZSu5TyefsRiw26e21V+raixUs1xtBmaWqMUhiaYgoJIiV9kpcDkgDPx1J6UnTVF0ksdNcFt1atphobaplSBIS6l442YkByApD85beFAycmQs89+oqGq0gppFtl8SSWuvFehiJQo0bycuG2u6wYyUVmkK8cYpZaloQDAnpiZJxvInHU1MzbhaQ2skb7d/vfy9KgP8OSQwbdTVdNSUeorbSywVYJeGST0sSR7gw94EjHB+eCeo+vpLqLXUaZio6R5KuWC0080asHIeaYbVce3O12fkk4OMtz0R1euaK52OejorYJKfT6wPFBVRyPLCDtDyxYYKm3fvWMnPuCszYA6Du1lNJW3K8ahN1/hVuEvqp6+WVAWGAB9lVvgflbjB8XW+YMqccxEY88Rt5R3r8ttIgJyKONPpSds9d260XFYrk0jPBBRohWSnpzHlGZwD7srxjJHGQepG5XWn9KPZXGG4PUpTx0zcFJSHjVo2DBvRaSukkJPloH5PHQHedMah0xr+muEU8FRVTJ/6bFQXM4XKZdjjO6PawyMMrIRkMepnTNyv+tNQW623Flkr2qK2qjpmjCBalfUhCIzHhAEYtJ4yAOGG7qpcWgKU3Clc3wgk99zt0Hc/M1E80QMSCdqf9Z3Ltc/cjTsVoqRbLRNY0jakaNlijkKZb1QcmNh6TAEcHeg2n3HqE1ffL1qGxV8trtVLmsYi2ziZYZKmGopgrBiGwWWeSFdpxncnnIPUJrC/U1bfKTXRqJUt1kZ6OCGpErC4zJBCuxJVGWH9O34CqNxGcyNhplks1lsFc8UUVFdJXoIHpyfVRpHkRt5IMTZjpuSMMAAoyhUhkXd67aIZK1KQMlKswQDsd5gJEbRih1rodjaXqr5psJWUkcwEYJk/WTO9FnaS705vlbp2vrXnlsX/t6u7BhPVGJXY5GQHWKIAAElQzKSSCemfd7utso5J4qWeoBQlljIDAA7vkjHjpNaNvFluWp9SW22W6rpWr5Ja6gqKoBilVHiOSYgKqhgJREAo8sVz7ck+vGprRp+ur7DfUqZam02W2S1bJCWeVH9UFXBxuk3sxwRz6nn5LtwRxWjRV/4OpH9ZKyf9HwiTABkFQ26Gsq/iXwSvV3jrjJw2hKeX/XK1DeRywCPX84/VmqaSq0tWyUlCsdYFiiSlq2QM7zFVA2k5IG/BIBHB+Oemvpbtw1tsdTWVrvTio9S41rSIqid9h2srDKsRhlXxwhHjkqLVVhrrVV2O40M9NXUqXQRQRVLD1ITJKlQxDLl2RfQYjP39pKrjps6d1rS3y10Vqa519RX2msqlmjUvvpqaYNCqbyuGAChP6cF9vtIB6J6vxs41cqbcTLkqSOVMwAkqCs9ScCBERMdVS04XtvwLTrY/llPiGVblS+TlwQQABJGZzE4jtpNPx9z9KpXXVZYr1StHK0nouZR6xDNG0inaSGMpY/dWwSM9Bd17ZtSwz04NMvpSnciy7hGAAQx5zgZOTj4znplWHTc1tonqbNcZ7nXiBJZJJ5AYpCU2l1AG0EBmARy2Q+ctkdQV71hWVdsloqiKKJ3ClTCpjYpuLFJMks4yQRk8bRnnGL3B2r647cNOWikKs3DzQpIQ4ArMhEj4STO8gz70eIba109jlvAtFykRKSVIJGIKj/mxB8onJwirlpySBWKRhk3AE7TjPwf8+gHUmmKupaQiMYYhSozyMH5x9+n5BJRVshpzBlvJPjP/wCuuGusUDSGLYdrYC8ZJx/5+/W2u3HiIiaVLDiFy0c+MZqrVx0rVM3tiJAz+b4Jz5/fPQpd9I1Ow5Q5PJx/5x1aK96XSV3IK43Z/Xz4yMY8/PUFW6KR8q8OABypwPnpavE8wIIp9seMgmCaqk2nahZCfSxuOc46zqxFd2+o1YkRYA8Db/5+nWdKTqFJUaaEcZNOJBriun4U59MXaf8AxfSlhBE2ZjEpGQyowbIxkLEwJB8luc8jgrNMWCtjq6y3W57n9NIqRULHap8b8t8FdrKPbz54+b0/iLs2mILlUHUd2qLRSVztT01JTQkLGWTDEuSNxy7gKCOGHKcZQs2mdO2lZaR7NOogknhrFqN0brK/PkN4xu+RjwR+brAtQ1S8tbhSHlFXKYGRt5xG/tWq3Fu9aupKHMDuZP5R9+VIyx9oaS11E11vtuplgep+o9KskeolhZl5COjLGxL5G7zlBwGJPRA/4dqOuhR7ZcGp491XHNFLGq5hleT03/qAKEtg887sYyenFJp66z0FMy2tYZY62NqCmVldFDSNhpdzEgkK+MEgenjI/N1I3HT1+tFRJc6q1/TSwbcwV5ON7ESSxkFiWw7nBPBBAIGehtzxRfrAcbcj6jtVv/EnHEwWwBjIn3O/WkVbezNdpamrbMoN/N0rjHS0xkInhRmAVsqpJdSpwFAOPhc8k+kfwS95Hv6X+9WG1Uc1ZSBZFqazeUJZVVWUBsttUnJ3EErkZ5DIt1ztD6gstdRXJIapZUZ5xEiLC5bAY5JJwsjNj5B+Cclm601BeLZ3Jo9R0SXu4QW+kFLHQVYEVOjtw0rxsSd5MgYE4KqcAFAAD2j600phb2oLMkgHpM/9dB7mrFspLiQtatvf0qoVx7DXHTmoYam53dauuqZmqZmpowYKiTaqKaRkdmf3QSOWKR+wDPHu649K6F1fT6HqbXdqhKelgppqcTGkWRgC7REvCqt6pxHGc7wvk+4Dc7bvdi1RZgl3FFJc6eZfoo3Sk9KOCoIUemuWIjJjEj7Vb2gK2CBjrdeNP3CO2RXe6yVEdHbKR5K6rEssTZSnnY52h1lJYMpQqPdFHu4ZV6gf1W4WQSQBgpx2JEJB9evpRhi1BZLqJiCPv6fOq5VlbftO2kw3eglpLpX2mrmYur+uxpzIkayF8qFIiB2A7f5hIX3Z6WVLW1tBUWGC3WlEpKaRYlgWQyPVSyFgTkYyMZAIHGPk89Wdu9us2rNQJLTVAlt4o5KGlp7hQCORqlkp/SIOWKKwb8zOrbpAMNz0pu6WmNH26hkqaO50tqqaK6SxJSQNG0aVMavthMgO8bRhTvCjJbGRnpk02/S4rkcRClZO+NxtvgY/WhzzS0JCox5e2ajrdqevuOprXdri9JTyzQvJCtUypGAsLwtLI5DbcNFgZGfcBnBHU5SNVdtZLXrClrY7tUVkkcVPUf8ASi9INE6JsjyUEkkzyhmyW9MHG4EAc1Rcm1J29huFJZqcxmrmjqKpxtaRUjEsjRheeSjqfuFJOOcE1FNY7xSaPk1VUG32gI1Y6wN/MjigIWKTHAXEKrtO0YL8DwBJcNfy0qWnlTkFO5ITJG2e/wCu9cpWShPQgiPp9+9Emto7rqzR18tVljkt8dkp7fPHb1DPFTx1Mby1JUFQGkaSpiYN7WBhHGQR046Sl0ZY+2UV3v8AdKhLbbwKtapSrzySSpEUKAqQyEOFO5sn3EFgQOgOhutNS6Mu1dbZfVtj07RXYq5X6maBaWX1AUYuu9FVSyjaG2gEb+V93r7jajr9LrYK2gp4Pq56I1tQSiu08ZZI0VtoAUQJFxjGQT4I6WWmH755DLXwpC5wcjCZPrIJ98zNcKcQVHnyPofL8vyq0dN/hWl+roKHUdKLtcKUG2VEVOmYfYxZyQMkbpMFWOMxlvzA9LPQNRdrtd7z/H6qOe83a5xVNRTSj06j6K2uq7FfjKtLToS2fCltuSD1s0FLS6Wsy6uvoivFVS2uChphMpBMwdEjxtIPGHKuQW2IcD3BmYVfquli7fjvLXR0cVwsrVdBDFHE0ZmeSJCipyAwLSMqggMTLk/lBAiz5NPuCpSfEBKUycSoH+naYJids7b1HqKBfacpLSuRW2BkCTnOJ7fPFAcGppNRyLV0kz01dJdaqX0JQ0foh4jBAtOGHOI2O4DkNUI3Uh2HrLyujqKjuVukSe5PVR3k7Ix6cc4kqfXmUD1A3rlFQNhSCcEkqOg3vxLoVK7TdrS8T0UV4pxqi6VkoZahd+JBTqqg5JiijyWJPA8BeRzRnc7UAobvqBjBTwVOn1rZ0Z3kjjhjnZYUY4PpypTIyq2MmSROVJJ6J31rcalaKVmVnE9JJwD6knygTvQ9Nhb2SEMNoATAyMemPSra1mtLRcrDSQ2SGRai3wy0C1sYbLRwtLHE2SBlwMHcBnKjDcEEbs9XKqVBmM4gnLAQMuY48ZGQvjOSTu5OT5xx150pPUNbp7hTpKaKI1l3opZJCWqPXb1MxkLj+XI5UqBtxETkbsCLnS5U+GqIXG7kMAdrDJGQRwwO0+OOOti4Fesr7T7Vp0jmaQBBMmchJHaEjbpOwIrzHxqxdJ1a5WMBajsIGwn1k7nrHnUlItPSzGWNjzngkcftnjrYpavVpCMFRjznP6fr1GUjyzLlWb08kZwSuf7nrolqiisqMqkD/uwCf7/t1qaXSN9qQHWjzQN681b09PA0kw2uf6Sfj/P746gqmVPSYpIMHgDgnx/4R1IvIZlKzRoT8c+T+g/bqEq5SjlcNlT7s/BPjqtcOA0QtG4x1qKrJmDsk20MoxnwCOs60XJo5XZlByDg5PPWdLr39dMLSAUgmrZ91eyT3e+SQ09BcbpVTE1yTVtYiU1MqkMVEagM42kDAIxk8jPKQobBpGy6hkpdYXk3CGQRiKGijZxCvqKrSgHDhS0hDHkcF8MOs6zrAdc0q3bWHUzlWR07nzz1zNe1W7G3u1IU6nI/XvRXR9vpJYZbvZbzPd7VTNJJJFC5T6ZlkPpl2LHco2jbh8EKW4OCQbuhdLRcbPcbpXXqOaKh2yiNXUOEAIK8BUTcQH4JwHHGSwGdZ0t3OmMsXSPDJAJGJxmPvM1xd6VbMqLCR8Ofyqb1Tc5qTSOmtS6aqaCitU8UciQpG0lV6uEkYschROShzyyINw3E7eg/uRrervFwh1Fd/Xqqynh3ghAkcZIYpGMPh02IPg8lt2PJzrOmLVLVq2ufDaEJXBIGBPt61Q1seC2Uoxv9CIp2Ulp07qWwaRvunqO6Ttd6aop3KGN3pUUO1PUtBKP5zCQemuTyAhPBZukTetbXyj12aXVcNfbqSohnSWFGkYV0iSvTmm2H3AlYj6bA7iCrgtuwM6zo7qVoxeI5XUzBIHlKJx5yKNMvrb+EbEfqP3qO1Q0bXKRaXUcckdLSxfRVAWOXFdI602wYG7e+2QK6r+Urtz1Xvude6O56q1HpkaZSe4U1bG9ulBUSRNHDGjA7eHYjDEZ2gqdoyzZzrOhehIAdKjkpTjptydo96p6x/JSCjpXLZLbqO8aVp9LNe54KOnttRcKerWM+mVT1TJgNj2sJnX4GWYeSF6FLVvrnht95vktsWx/zIqp4s+nJIIhGrIecewgscgZHB+c6zpjZdUtbgPSD7kkH6ULbPjLSpff+1Wd7WaYu1bpS/wCmlu1Nc6e31Fe1Tc0nX05lmKzuIXc4BkgZSxZPaGTleR0vu43bDVcVzvOoJ2a5FLtR3eSAOD6YV5YiJvduCAxtt8EK4J24OM6zpGa1F611NxDcQeXp/qOa4fTyICh0J/MUSa8jv2m7Tp/QkF/mhprPQVFyjqHpsbqmMgGBFyVX+WzDBOOeTuyQd9sjpjWWiodGXW5TUMrUskVFTwejI9XXKok9ZsgA8xFQOPykYPk51nXy+lzRk3JwsHnkd/i+/wAq7WslwJPUp+gIpY9xr3br7cazSusaZbfXUVNTz1Ao4Wd6yVUb+UHJAhi2cM65yIUG08kxfdUWjTNqOk9NzJFbr/Mk7VCE5FNshyrFmOU2ISxGBulOAAQOs6zo3ZpC1WqP8pHNHSeUH6H72qqT8ZJzA/On1Yb4iWahv1FbaOaoQJHVukg2SU0jhYkfbjOwzCE4wThzk8ERM2sLpqHubb9FWCvWkpaOOvqa8VEakOq74hECMqN5XcOcjCYwRznWdfdHvnWLJaG8BBUB6CFj5KJjyJFZHq2nMKvX7tWVlCj5ZPh//mPOQDNEySBYTPERhcAkA+0nOBxx8H/LrWteOHdwx++fHWdZ16BtLlb7fMruaxa4tG2ymOoB+dajWxkPvYAtghsnnH2+D+/UfWCZtzyYZMHyv+ues6zqwpRKZqNCQhWKFLrctjyA4XHGQf8APrOs6zoHcK/mGnC1t21NgkV//9k=', 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAMCAgICAgMCAgIDAwMDBAYEBAQEBAgGBgUGCQgKCgkICQkKDA8MCgsOCwkJDRENDg8QEBEQCgwSExIQEw8QEBD/2wBDAQMDAwQDBAgEBAgQCwkLEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBD/wAARCADIAMgDASIAAhEBAxEB/8QAHQAAAgIDAQEBAAAAAAAAAAAABwgFBgAECQMCAf/EADoQAAICAgIBAwMDAwMCBgICAwECAwQFEQYSBwATIQgiMRQyQRUjURZCYQlxJDNDUmKBJaGR8BeC4f/EABsBAAMAAwEBAAAAAAAAAAAAAAQFBgIDBwEA/8QAPBEAAQMCBQIEBAUDAwMFAQAAAQIDEQQhAAUSMUFRYQYTInEUgZGhMrHB0fAjQuEVUvEHM2IWQ3KCotL/2gAMAwEAAhEDEQA/AHLq81r42urWJF7hX337dSEjaRj9oJ+FRj8Ak60ASQPWrk8pi3ka09uGuRE80sUj9WiVHKOxDaPUMrDtrR1v8ehfzaxWj4dk7uRj91MfTe0V6gk+2pO1JB035035G9j8eqL574NmYfHOL8jZKC7Py3k1hIcdx2MtKtGqkPZ0Efw7yhlDNth7ZkIOynYmq1t1KdVkGBO8GQPfEbQpazLLVkf91JsOoid9t8WbzHncrghLBgpIQcti7S05e3zHeC7hXqPkhwJW7AgAQt8/I9Rvj/hUvnrwJQwWfuovIYxYuYjITOzGCdZnCIx/c0bJpG3s602u6rpYOJfUByytfp8Es1nlq0Wjjxty7XiNuGZ3PuiBupDRoh2Pg7PVQQCNu9xCOTjM9OCKpHXWFUkPs6MYZz2ddgAdg7MD/wA/n5JAIqactrLThBSqY+1u3+caKGrcyoN11KkocQpJJG43Mg87j6XwuPirlWV8IeSL3AvJVKzj6l1xBZilXt7bsxCTJ86ZQwLhx8FC2t6OmS5dleVYji1fkXCKX+qKk2nW1iZUChV2Cx7LIfn5BUJpdHbfx6t3mfxdw3y1xmH/AFJi1nDRGOK1GSliux0Q0cg+V+4KdfIOtEEfHpM879PXmHi/Ipo+LZqzksBv3BLDko6FmuR+fc7KO2v/AHKw3rZ1+PSN9l+iZUKQEg8CJB5Im38746uc/wAj8W17b+eBKH0iJVOhY4ukggjgGe0xc3ePPMeE5nZ/oMosUMzJGwWpckjf9UD/ADBJGqq7a+fbKhj89SxGjWLHmnjfMMNzjxTV4EM/yHOR3bmJpurxi6r1RXaN5Nr1Jkj6ONjr7gU/I2Qm3PMPXjfhPOeN8grX8TcLw5itZifIVpg3YSe47bnUnq4JbRGtHROzz4cn4Jy+XI3shk8fk8xmo4I795EMVpjDro7RPv2XDN3HTspl2/YsdBaWK3Oab4apJHIJEEkbAi/zg7czjPOcqybw46c0y1vVIA0glSQCRKkmBNuCBfiL4rlTG8M+iPj9GDjWCzHJ8/zPKQYqsuQhaSzUjmTUmyY+rBY0cbVfn24gV6qxZgfH+D4mvC3kpcRxNRcrCmRtRLUijWWUhZVlk2PuPzvsdn/nfpO+a/UXgPGPH8x4qxVHEXbnEDZ49B716cWoZYTJDIzLLvurhFVACyadSdhW9MJ4d5Ssn07VclyjOF5sbXWtNaQgPK6rHNBCCQNt7ckUYPz+3f8Ak+g6J2oQXVOJj0KnUYkjpMwB7xfAtdRJqQwUqKiXGwCm9jbYb+0TOLZmcTai5LUSnaryY+5cMrUFleSVI2kryrYCnYjQbsJ0OgEaP8k+ihyluiV522Q0Sfz/AM//APfVFwfHbeHWNs5bS1k4pJnkmhZ+jGR1LaBOiNRoN6/2/wAbPqf8nZ2fDcVrWqdF7N6Z0rUYF/M8rk6X/AUBWZiSNKN+meT0isopC/UWUuDHS23vM4mfFOaNZm8aWmOpDMiep1XIHQgDicCPzf5UoYDGWuHVqM9zKXqZmiSNtBUJIUkgEgErrYH8j8fn0H8J9UWU47Tp8TveObP6b+mRxJaFkPKXQD3ff7AfB2wRx2G30VA2fQ1898O5RjMi/MeZ86t2OcYzKUaOWqUqft0cdjbEkiqnuFVWxGxiJbRIDggKo2zfvHPHduz45yHlNsrNftZPPTY6qZY4wEoovXS72Il9xJlA3pgyH9x9ePByocVULUIIj25j2vvhC01V0cJSsaJmCL6uL/Iftg05jL8R+oThOOiwmYqY3muHgMcdOZ197qqxyGGRV/I6Srtl30cnW9MpT+Xk+Y4N5RynDc1irdGR2WxPWmQdkbW3ZdH5BB38fB2rglTv0zninipx0FKwcLg/0d7I1bzZSSsVyFJYEiYRJO5+2ORFRih+WQzfPyoO59aPjrC8p4Nj/MvFGqWczxCxAFnjkBW1UdVjaEkAltuwAHx8ud/j04yjMvOR5QVtEdSP8YZMvrpXWwqdDoCriNJPHtx74UrnmZ/1L4e5O8EvtyVLtqJlBO19m7sD/uUVT/8Afpgvqi8P8D5N4K4Zk+LU8bxyT9RDdhspX+GqSVWdw7D7nOzEQWJPwSftDMqnf17HW/FPK7UM/aTN5W9P7YXQjkfp1QHf3bBVt/8Ay1/36icC4FgOVePMDwXk9TIV24hia0DWlrn7ljrhNgsASHT/AI/wfxokjMakNtzI1FNp2+uN9ew+660tngn7jnFM+hLjdbw3xx+OXslj+XZG+ZTep15FnQSDox/Tsw0RGpZGVd93YaPwB6BHkvh+I8l+YOTcYr8ubGYfAWvaoV5a56XvfgWSukzlgIawRpI267P2/CsxVSR4JK/hXwhkcxxDkUeYyfI454LOdyFmtMsdphMlerGkjBIY1eGREPfsXIJH3Ki0/wAncNw3FvG/HeU4TjXIMRnspHWbKy5e11eUyK08j16gZjHC8s8xOwpX2GGjti089JaCp9XFuTf8sNGaB2scaQVhBSQSqCYixgDckTAteMU3wX4MiwHmvA4DEq1jGxZlMxFdnOy8cKMTDofH4UnZH8gH10p41jzcy8URX7E0x9IH4HymWx2C5X5DgzlT9RSWniROtoFq6SvIZUDqddzJHXUnQYBid60fT+eL80K/Dv8AUeXdZ53eeGJoyrGwscrIrqRoNtVB2AB8/gfj0fQuGlYPxCvUbk/bAWeMnOKxt2nb0oT6AmQTuVAmAACQRqA2VPGJ3yPzYcSxcKVa36nIXZBBTgBA7N8Dsf8A4gkb/wD6Qt2Y8wvwPj+bzXJMoLpxEhR3Uk+5O4Ej7bXwqtLrQUAAKoBYhfWl5855Gc/PkvdgfJ46dKWGV7jhTbEfuTv1jB7FUMa9X6giVvg9gAEfNdl48DUwn9Vqy3WjS9DGyK8di1KvvGeV+umSIyQjetl0Ib7k9MPM0slZBCgYvza/0kfPtifzRCgvQpQLenVI3TeE++qDHaZEjBR8Z+QOe5bl3DstyvkEjNko77wUSoEkYCNsk6A9l9EjYDbji/ABDZ6GuCycOIp4rFiNJch/Uqt69LUeRo6s3vwzeypdCSP7ciR67ACUheyn5z0JTZg02kh2Z+vA/XBlZ4ezJ0NqYIAAi/pNjyDyQQT74sPmTy3nOKc341Tix7ZDDxpTvXqCAatBZpGKM2iybMEQBHwQZAQR8ep/zp9Tli7z7xtn4uDZbFTYf/8ANTQ2AjpYrWZKzq0Dq2iDFHIN66sWIOyT6o/nzj9mzj8dybHd47VQrTeVCO0ak90YAjX7xo/8N/P4Kw4XMctivJLmhk8u1Ssy4yzYeSyyrBLIn2oqk9EIZgqdV+1ixHyfVOzSM+elTyyLn2Egx+v0v3nKF92pykOUbYIgBQ5KkkavzTfoe1i3w1r+F8gYvmMkeNe1BP7hSYL+njc/IOzrqFX/AHD5DD+NfBN5d9UcdPzdxjDtb/pmEinmxmVgu1lQd5h3jmjl/lA8RPyA394A6BAFIi4weE4nMYnk+JuV+aSUssf0izxOuPmeCGSsjddh5SliZWjBOpFUDZGjTfJ3i+jiqdbm/AszZtSo05srkJveNiKOoLcXVnXsTOkTgneu0wUDR6+kWZOIYfBKyUr9SeYuZv8ASNvbFmylGZUuhbQSplOlZ2KgQIMbAAe9yYJGOnXB+Q0uTYCXHx247C+2GidGDAgjYIP8/wAH/t6qedkjpxWrlxVEEUcv6jZ0OqqSxPwf4H+P49KD/wBP3yfPjsTmeKMbgFGtVzNAW42V568qFHCE/vjjki9pT/gDX40GG8eeSMh5T4/lLvIeOjFPNtLFcSidEhkQGN2cKF2wY7A2B+Cd9guKn0rQW1mCsH2xLuoFK+grk+UpPuRY2H2wqn1DyVB5fzmax5jlqP8Apde0wYNF+khH8fBYEHev9wbX59UuR71IxZbC3JK88Q7RzxOVZdj8gj5/B/8A4PoleesZbXybl1koxwpaMMlZIgAXjMMalio/BMqyj/nr6r0k3jjw/wAIkynJ4qvJstkTL3qG7KtPAwKepac12EkkzMeqxqR0+e2mAU+UFA9oQwn1FIAJ4tuT0HM465VeI6CmoUVi7IcA0psVXH4Y5I2tzi0+FuV+N/IPIuUZbn/DcNDzTKYgYu9nr76rzwMUhPZHbpHKyj7nVQQF/wAfBvVvzn4+8eca4j4Fo3a2YxqSTQ5/P2KCp7ccjdK86Qh9HoDFvbdgI12uyVC5cGv+NOSGfO0uVZDFVJ42WeOKn+pWyilmCr3lV0Ya/nvsaDaBJNjl8J+NPLOPsN4n51K/I2RPbr8hsQ1XvCQHRiVXQxsToAP3UqQAQCfWyoyKrQ6664gFMAAgyCnnsZJ44jCGn8S5G55KGnFIVqKikjSUr4kiYgAEDqTh4uK8745yDD2LGNnWGhip0oRBojGxURJ0AXsxO2LKoBJPUfkn1C5D6luLR4jIU8xQ961jctPTrRuogdDGOhRldizv7nuptQPzrrr7mRbM+T/I/jLieMwmRw1qhyVElsX1hY/qaUtOWWuS5A0qsrKW+dH3FB/PxQrvPs5cuSZuk16NYJmbU3YCwZFkBP3gl9yHZ+dHT6/wNuRpTW0aTXfiEggdQSJ9oG3tie8X0SMtzd0ZVdtelQKp2UkKid9UqAn3x0D8n+NPF3meGhR5TzG1janM2rQU7lWREMVmNvfjC7X73PZ2AbfYF9fLKwU655D5F4+wmW8Z0sjbytXhfI56+RvQVwsbV4bQqz3jIdKEPRumj9ruinZ9b3CMtzDy/wDS2OOPLJLY43nI7FWz+LCxv3DEEHbAvOVLH4KyALoDYcDxN4qxPIvo7nxGSxEOO/reByMdhHhRevve6BM38dtt2LfP+QT+fSmuaahdMRIAmD0j+RgdipSr+gohSheOY2nv0wuWU4dguQ5GlkadvLjFYWdZ33KqwTysQBPKhK+42pHKuFJR5dDWiFHWcz31C8q4dlMhwvjEycQL5DItkpRH+lvUa0kgjmIP7H92HswOnP2kdgfi1cO8p8PxHKcZkbfBqdq3ZrRVpcR+sU/rTKVRQI2UhWO1K/lV6qCrD7fRm8itcrcJniinsYLitSnLWxGLk9vrZJgmXoqqCfaSNwzd9klEPaMkKZvJaVaHEqCSVAQNUWM3MzsZ3E+18Z5dD6POKxCbEGTG5t+nthEOJ8QifyhhqtbEzXsDm+Q4XNJRRgjyVzaRbNZSw6o3Z4x8jQHXf4PrrPxzk2eqcNzVq3FJVavTazXrMYmsVuxZhE0iDqyAq37R+WOhrS+uVviLlAxfIKsF9oJ7eLvR5EIkel9l7AMiqdAt1dR3GwNkDY6n10O5BJlfH/gC4LFenRymYqzpPJBGFeNvbZ+zKCfuAYaHzr/AA9UOeKDbSXUk34/l8V2WNioqA2rYjAOo+QKHMrNDH0ccMbQq5uB4q2ShjjrGwjieGYFgvRV6uzD22K9y/wAOGU3Ly3x2hmMUmRxnHLdzF/0l80tqn9r0U3H7iyh+xdVQMAy6LaCsCNbXmXO4e1zHj+DxVl7GKh92ys1tjIHthHV1Jb8OjOG6/ka+f2/Dqfr8VjfplvY4GtBcyDpjZLUVaNW9mzFGxiVwAWHaQE9vnat/Gj6TUqV1Si3Ok6ZAvNzAH89sVOd0jXh8IcZPmIKwlSrQCAFEyOIt8iecKf4T4Tx/g2SkGStidZhHeoVbMIaOG1OsSxQMFDAsje8za+3qOo0ZFIdjHxxcH4GstpljetA1idnUjbaLyfau/wDB9Az6YPpJajfxPkXlHlFcrxoh8lTqvXkktiZdQkTN+wdWjYBRo738t17Ga+q7l3ISYuI8FjadzWaS2A6B0i0DIWPcBF0D8sRsbA7E69anGXKavQKo6vUNQtMDf2t1xjUP0+a0jpysaJR6ZNgqDF+knj37YpHhqvR8ieV8nzSzeis43ACXINKgVkLyM4gDDXwQBI+h9wMY2dHqJby/wilwW4+fymWlx0qxyVoKFABmx9X2jGRLN/slYfPRSerMzFg35vP0kcboHxTk7s6Q3f6rekErNH98sPtrGBJsfI7rMf8AHyw/IPqJ8v8ADsJZxGRw+IsWKDLVmWGeWw1iGKcKfamdSHcJ7haLqPl3UKFP59VtYtCmQWwIvpG9yZ+m56Y5vlyYzUirJJASFqHpskATIi8wByR3OAR46v8AIOOVuTYfB4TI38JXy9CSE2YzKuZLNuskIbTe12ZpmchtKiL9m21no6WJq3L+A0M4eTw2KM1RKNCfHsjTWJ/eCSRxsAVRT7ZiCKNdS5fejrPW3LKhTDAD7CSo3vc/Yc74WeIw1W1yizVKCUwLbGAByRMCBN9vrr+Qoc7Uw8eKxPDoOR3rsjQpRnm9tHKRPKDoKzMe0aqAqnRYMSqKzqF8+ON8k4RhOd+P1lxwxtV8TNBFIfere5AxlhbTH7+k0yvs/JYht/ILKfUTWzdLiOH5jw3IQ0crjMxTZzIN+5FJ2ikVV2AzdZOwDEDsin8gek8yGVxPAs3lOOcE48keLy92aaSrDkS/Q9on6P8Au79VlRiNg6jYfAPUGKq1rqSgxASFCeCDzHUkD54D8P5O3RUrdQkqMuFCgD+JLiYJAJgFKZIO8pG+PHgUHNeUcwo8dqUYDjqtxZprkUqCSQfDKpWRgZDuRE6ggkt8aC69Wn+iZXhfNcnwXnOGvHx8McycdaUuZopHZVRYpiF90qqMUZtlBAgBABHr28cy4jiPkKldvIYqOZhkqWpJJyqqXAaCwg0dESLGx1r4jC7BYgnLI4nGc4NinmI5aF1amQlxxm7Nqb3Imhdg2i6yRs2wwBZWk/HbfpBn1Q0qEsA+XuJMlJUTMHtH3w/raLMqB97/AFJclQSiUjSFBABBgcgETtEbbEhvwnh6/hTmvNeK2Q9puI4sDB2JZIw1upPOCI5yNN/ZmMhDDQCSSfG+o9QeE/1dkrlGzkselizjYpYpH/R9IYvaikR+/bboPcHUdAF2fgnR7afljjvNp/JmDztWtHWy0Fy7Rzrhlk+yaLX9tCBvoGkeMSAI5eJj9rAeib4yoWvIdHK1Tm8MiZdbFS9kKlt2q3YYUIZhGUEteYdI1KueygMANEFljdSVtArIJi56HY/WJwhp3H8wJpwAXFp0EC6gqSBMWuL8CR7Yl/H3Es75i45X5lyPjeSxtCnTmmj98stqrSX8Ovt9G+CC8aF+5DuRodNJP9TnjXHcH5DL4y4P/Vci2DtWP1l+xkWnS4djpWrJoBo64SQdyAXLE6G1HrpXx/mVvxrTox1KNi1VyEFfHY7GNZmcVEaRokMrljHuQLX6ghSg+0b0xZD/ADTxbklXzZyawqQDCwTtVpv7ch/sxBV77AP7iGYn8bJ6lgAfVB4febrHlUjxKUcEWtsZJ3v/AC2KjMcpdyijaVTDWUiNN1ERcWEkbk39+cDTivL+N8b8NzjIY6DITYq2sMNadO8f6yR5CGH+dIHf+Pg9d/k+ijxnDnO8Ko8hw/lsUbg/TtFVoYRJv1Gwof8AUTyy7XqgP/pnQQnQA0Fq5pUt8fjyfBzTZNZdcjAFbuJImQqmiAO2uwHwPyT6LHi3B8ry1Gjx3H8Ulq2o679oRY9tZVOgZSZGCxaVX7ufgKzfwxHqlfzpxnL3Qgx5bYCSJHr1QmSPcWMSBaL4SZb4Tbrc5ZbeFlvFSwoBX9MJ1LAB2MA3AJBVeYGHM47T4fjfGmT80c+w39VHN8mnHcSWIEeOqlljaSX3GX+y8qQ7KklVUHagOwJXAv8Ap6+F+P4CvZ5i129lpcQ1a8EnCQR2pRL7s0S6+Cvu9U/hQinRYkm5+LeI8GzHgXDeGuVRULEValBFZrdiALVomYtGx+5l96aRFc/uZGB2QR6r2b5V58/UVU4rkMLJwmnb/QvNLU925JVhAXs0hkVfv0OxC7Ab7dkE+uU/EvolphZTFiSd4HXqTJ746w5RIzCoU8pAIKpAA/CFG1uABA7AYW3wJ4zyGC5j524BwzJW8rXwmKGPrTrKTXXdyOQI32jrOUif9pIB9zf4G3a5tRyL/R7nKsWOavYThLqa0a7Zgtb5XQ+fuH/2N+gX9FfO+BYyLk/EjkY2zeb5FkMu0xX7bQlWMFewOthvtH+0grr5JHp0MNdpZGvYxTRABoGiZSB10QV1r/H/AB6qlNGpmoCpJRp+f84xzWqy/wD07NFBxJTNgDsUySCDzvvzjjR45fmuF5pxPHZPj9+T3rtO1JfklZ4ZYkkAHtjfViZm6MdaGmH8DqUfqx8h1sblIr0d0LhKcstCoiylk6uH7gMfk9x3+T89dD+B6LX1MR4Pxh5AxPG8djYP1dXh9SjDKrdGhiSxKXeJdEHsW0SfkbB+SdhHfPORXLUJKM3J7laCSRWavbqB49g7BDABl1/lAxIJB+N+saAANF3TCjM9iDj1mlbYcKWxANz/AD2xC0reJObxvLocpHAlKGeplURGMEiE6Eq9R9qSE+58/Hu91J3oene8tcn5XzvwT4yOBszV47tdTlp5WJE01dmrr7rqpCs3VJSrdft7bI/PrmEkvIOK3IbVS3JA2m9meCTtHIp/doj7WB+Nj/6I9PJ4x5xQrfTpkMXex+QrZKhnZpe9es8gqQyp7awBTsfqPfRD3P7DG29AaPlalL1P6xOkhUdeI+dsUuUBZqkttqgqtJj0jcm/SJxHcc4bksf5YzPGcrk8Zeo2oYr9FVlMCv3i97uj9F/uK3vbLfk9wdnQ9M/y/wAPeRvLHAMb4Q4FkK+Gtv7WVymRtyMf0UJeNYyGiB7SdY/tAKhghO/jfoOeLeKcR8s86xePzU4xnJeOLHk6Bsqr+7UErqPcjf5cowDAk7Ugt8hyPTeeJvJ9XIeSs7jsBbqwuGhgse7RaX9QkLOFdWV0KKA3XenA1+Afj1NfEN/GBTsp0xqt7EfeMU1Y7UKoFUqYVpJg7kkyCZ9iY+2LJwLD0/F3ivBcSyDiVuKxWYbludFX3pDKxchSW6guWITbdewGyF7MqnMfI/HeW+TsV46wmPjyS8ssT0ZLN6KKVo2khkEMihiQUR2WU/nZRdL8jbmec4Zc1xv9Bh7WFxX9SO7Mt09QSQfvXr8v+ewB6gkfPpOq3jnxrxnn2HzGG5RnLy1bvWxHPDATWtwzQCO1uNV7R7sa+O2zIrA6Un0HVqU7WKecVI69Sdv0xjRNtU2XTpgkgDoBIH3kx0xaPIWQzHhTxlytqMdiS9Xx+LS9SmtSWo4JHuTLBcSdk6ye7MxeWDqo+JTpRIvqm4rlfKc/yLg/Es9iMVhaUsNylYFCaWb3ZBVdDKzgdx2VEILk6BVR/wC7168sz/6/AJxpKMEy8yyDT5Kkl2zMZ4kVxHEw2FhCTxGRPntGBpX37bPXcx4+5PW5sMRDGsWNwZZFyqSt7VyVtmeJidmSETBBJobZUEIYHt6oxWJoqPzHT6QN/cjb5nEpVUqamohr8RNulk3nqMXLlnPpOP8ABYbQeBJFks1a3Rw0cjvI7T22BDf2yrEADv8AHuIfbf8AbnoLeQMjWu+SL+B4vftvx39fFlZKF9gFeKNVJErIdyvJM8jhCW6x+0rMrIQ2etrFUytAW+mZ2tNrRuk/8dcY/wDpeuqv6lOm3N4kzf8AuH63w0X1N5nJYvwplocbBFJZtoscQkYqVdQZAy6+dgxj/wCtn0o1aoueyNXIZuya1+GOSzJZtRu/u2CoDMx3vbAAbII/OwCfhtfqb4vlOUcVhx2E5RTwFmlTlvCfIUzLWsggIYgwOxKF7MoUEkkfGiT6WTN8WvwY8mLJUMuFrQPM2Ks/qIrTRKCkkQGmOw+iug4J110wPqqoXEfEqQ+BoUNMm0SQTf5DtMd8RjlORk7S8vKviW1lZAg6gAQkgHeNRkbxO9iJK23F62AsZDkeQsJPXswV61FUM7WVYn3PtP2qfjrr8l1Ya+Aw+OLfWqct5Cx2B5FxeqOOJkYaOPuSEtkBUBVY7BYEKxRFXaJ9hD6HZtt6pvCnxHNue1OPcxtF8Jjf/wAlLHMB7WlZSqSyABvvl7N8gDrpNf3Cwj/qN8OtwvNZC5x6TH24lK3r2NxcixtgTJNqOJm+GdldoYywAAY9Nsdj0E+aRmsNEbm9+L7D3NjbtOHqqnMc4oxmTn4RFuRAGr/6gyL3iY3w4HnbitC+45niOrXKz/oshDVlFgIpQhWYoWMLIxRgsgVtq4AAZyF0xmezHin6Z69CGWGll+U0J8kRcg1KPdm9wxFdAyiUyVowPll9zt2CqygZY/yfyvxhxcYHjPNrtfJ526ozjyysGEKskkLozbAPumQlzt9q2jpg3qTv+XMlnxi7GHxlCNKck9zH5e6IZ5JWZgJmkjlDrES8fbeuzKsZ+AzdgK3I05c2txBtuR0ABNuwONXhd5qrzAhkaXHgEpVxq1aQdpkgkj27Yav6aOQ8l8ncAzcnI7EEuRsy1uQVekLQwx2IZEKL1/IjDw1/+W2x+SST+/UN45XN+O7/ACz9P/TuQ0LJljllZZO0Ni5/cgk18Oo91yg3tTrRG22u3irzv5f5VlbWU41ncplHosWnWJD+ij/HYdR/bjUf8BRrZ0F2AXPI3mLOeQ8BDxyURVYEEFi+1cr1sTBvc9oaLfYjhSW7DsVX7dE+hMvq2luFOhQUJAtaDf2t3xW5l4Yr2H2naaqbWhSg4qFDWCPSq25CgbxIteBfC8XMBncVwy7NQ4wnIOVZRRisTHHXEyVFGnsWh2HwFQRpsj4MxIIOiLd9GXHbPky9ybFTRwY7NYivHj8ojCRP0UhtFVaCMHSu33qfnSsqtr400nwzN8hyGYm4/UpY2xItxKMEH6aOcWHkkAiVkmDRlyXQdivwUABALepf6dclx/6efqI5lnPImfg49x/ltKNlTIRyK4yomQiOTa7RtvYf5+AGTZ/HoSqz5yocqMoZTsJ9ykpJgdR+U4bOeGRRfD+IHnACoiBNwhaVJST/AOJP/wCiPmxnKsVyHjGFzlujUnGGcSC/epWD+ppo+RazXcKoJ9uJZd90VmT861tlHvkTyRT8b+L6HGKV3IskskdOAWJTPZuIylmfto7eQK423Uj+4wH2gevCbzhwfEcvyUuD8hSSTyRvBYqRVZrcCgH5DOnWHuoVUBVyQFAYHQ0AeR8ss+VedCSxI9fDUr9mt+klKtJeZEZJJZmH47EABANKi9QNE+ssm8MZnm7qGUNEJN9SgQn3k7x23wTmni/JPDFI9VO1CXFJj0JIUowNrG0xuYAxGcMzWN4TLDncbLia11RYbHVL19v08Pb7Hd2TfYKHAKH42QG/kej94i+pnknjrlWUxuWsWsxyBQpt0Wt+wvRl7K/syxdmBDBldW+Rr5IJ9KLJ5EwvhvyfyLBTwV72GyNFVkpvXHWtY13V10Ptfo7qGXW+wVvtLH0wtDiEXmTh/HIMTTycnNuGYTIw1mowNJDbxvuzS0wZ0O4WEJZIg4O2AAYfwyqfB9TRpdXS1JLiFEKsIB2k32JmBM9ycImf+qFFnL9OxmlCkMOJTpMmSN4EAQUiJMAHoBgwebvI3DPJHHZPIORxGKPIsVj5K84sSMJIqgcSHoRrsAe3woLa36QLG5rBeX+cX+J2cJRmjXH5PJQ26U1iIulKlNbMYDt8l1gMYJGgWB18eihnM5yCDCRpmpv1MQqj32MYDyIy6lTevzregfzv8gj1qeFfpjyfjazQ8o+T+WYviuRyIlpce4vYb3clfa5E8C+5EP8AyU6TFvv0SB8gAjY2VJrggtVgTIO6eZ698bPF1Jk9E+Hsq1BKwTpV/abbb2+Z2x8eOPGf0r1ps9415ieT5jnYppJToJd9qjDkXIVqjyIny1dSzyyHQJV402VUyELI+R7f0qckx9zEXIhh+aUxDfEiSGubVOCKF0aMErJ7gPvOXjf77BH+T6T3H5rNeH/JX9ZrUZoVEjSQJZG2kqNJ8H4PydL/AJ/IPpn+Bcgxn1J82xmF/wBPWrlbjstXMWIq0w/VCIIzWjBsMrGMGJ2VgO4hKggsCGaCACOcSy9Qg7iMWPE/U/wrNciPPcB4hpQ301UORp13SRwA/aUdmMZT4RTuNdBvyCAA0f0Y/wCnc/Xy3kzj81qzWsXZHr/rIRGV9zTsmgzAp87Hyfgg/B+AvHkP6WIeNUYOReNsleuVrv6onATUUdZKijsxQxqG2NSHQVg661on0yvgPimA+mH6cEtZs3bVj9IuUt0bP9p47MyR9oI1YA9EVNKrfP8A7vk+pnOKqncpfiWzcKiYuY4/KMUeTl6mfUw5cFO0zE89jYg9sSP1jxYPkuGp5TD8yx+IGDja9LGll0lt90UJH/bPbWnUgEFfuBb4HpZrnl+XD8z8USWkihj5HxyuuSZ+qR15Cp99u/Xbse0Miq3wrBSWChtFnkXh7yUM1lJOSV8PkbN7CWZK1atfsxySMPbT2ZOoP9tVs/tQFCYh8L2JKmeVuK8i49ynjfGOVx2aE2CP63J20bt+gWAmxC0cikfc0LBPyCBFvW1I9C1zLeaMtuAQ2QoGOukjjvcReRhVU1T1Ig0a3JIUCJ3iJB62j2vg7c05biEmw6cFx1S1l7Vh5cfKtYypXyVivIknusxX20rtMxGjroV/cxb188o5vT4VxiZbPMw1Cwsj0Zr2uwquSzTHY0ZZGIBGmJdWYb6g+gkmPntcitW+P2cjhOJNN/XMzmjb9oR1UgT3aUTHY95pJPaIQHp2QtsE6HnPsDlfLPP1zP8AW/bjsrGqm+5hhrw661a8UZ+VBjVSik9mUh269hs4UozBphpY9KRKhtO5Qk/I+qN7TvjBirW2FvbFU6e07n9B0+2JnLeSX8mc0xdzKZa3apxI1WCOTcEcdSKE9IFVdHqzBt9uxGx1I2w9Z6+KHEKHAMWvJuQGvJYiuPjfaYtqJSHU+2oG5Xbqw1+NNv8A59Z6JfWp1Z0Da28Yq8spvgaVKX9IKvUNUTB236wf+cdBPq04zjuR8J4fh81cvx0rnIY67w1JkiaWVqNow7LfBAkVNrpi2+qqWK+gJ9PfIaviD6m08Y825ybPH8cxdJ7WMjWsZJq/au0hf+5WlZp12Ts7QDfVz0bzzvWp3fCnLf1dlKjVsNPar2mGmrzxxlo5EP5VwwHVh8g/I9ITj8ng/M16r475NiHucyzMz4hu9kxjHtXmjSCxP7Y11YqVLfJ3ZPQfA9OHEupe8wGUxt3kQfzxzzLVU7lGWVCFBX4uIgyPyxIVMlzbg/1tcv4hiYI8FYzuYtYllavHNUWrdsxSVSEY6ZH9yAkAjqJD8fBUuxJR8QZWvncHaOOvcg5F+ppXP1VbrJclj0LLxxNthCJYWBP7fcjIBJA9JL5o4d5DzXk3i3A6ryU+btao4aXIrYklh/uCPrIZBt1VOkTgkE9D+AQB6JfhLzhR5TyeXjXI5MivkXLznE5RrEiJSihgYiW1ArEOJJuiIUGyrAMqrGNek/iJrzwl9mdWm8cQQJ9h+Y74qPCS4Kqd8jRqGnYySCY9zv7HCV89wVnjGVyfE60diazisvcopOs6rNKplaMaUbOmUtrfwQ2v8D0eaH0wcYyHEsXT8lc8nqyWgZkxuDqqn6RWj20b2ZGIldQhLAKRtG077C+vHzpJxLMfWVhsXgsd+kjj5PjsTkrUjgI08FlVlmK/7UG+jH5Go2JG2OjT5rFfxfjcdJmRZIoRztMiVl/8REsr99CQdl0zINqpJHbX7h6dLrXq1pptJKAtKSowJjSSeD0HB3xPu0DeXPPOoRrUhagjcDVqSkDcX9RtI274uv08+IvBeE4TLwB8pZowSQurfqVUPNIwA7tOhH519wCD4A/OhpbanB8942yGf4pyCKZLUeYtWY5ZG7i1WkI9qZJPxIjKugynW0I/gj0yHivN8c5lwyKzayVGIhPYWhJWkl2WijmDQy9wI+xlfYJVSVY6+RodeZsNFHC/JcVI1l6haC0rEGT2uxZTsAAhCzD/ACfcB1oE+hafKk0FQtbaiQZmeTO/8GGuWZ2qoca+JRpJAjiJEgEfTnAB8Tciu4XyBlHr3RDPWzssUcnbTRzKsUkUij+fvU//AGR6bX6hOI8L89+J6XlW7hKbW6NqOjlowdPF26ojKR93uJK8YU7XcchLfGgE145gsZe8i8orwZSGrkHrRZCpXRz7jWAymQN8fGx8gD8lVHphvGHkGCpDaiy1WW1hsxAcZnqEchRuh1/cjI+RIm+6EaOiRsE7HM/F1O5S5p8aySDMkjqb/cbdxjuORU6M6yRkoSFOMynSb60AwpMGxkCR/nAuj8c2OF8luXJ+YyZ/Fhkjrt7YATYB7hgiEr89SCo0QTsgbKxeWrvKeH+S8m+Pt3sckltbtbqxVWbXy6jeiOxb/wCvz/PrpvmPEHj3g3GOQ8m5ny2DK4G7jkXj84ULOpf+57h19jv9sYDDSlGkDAK5HpHfNeQoy4/AWcwlMVI8rCqpZTvN7XZSzA/BUFQ3YNvY67AY6HSfBHiqs8UZW/R1K1FVOQsOQYvYoUq17yOdxxjhfjrw7k+QZsxmWUIAafHlKbNiVAavMQm9raVjYHSQfVin4HxjdvzYvyl5Bum5UlLW8ks8gZph0HsBdfnZ6qVOh8AD86DT8B5L5FwvB8jJ4+yOds2aFBsq9bFxt70zL0T9kS/PXYGj8ABv5J2oFfledmo5LxdlcvDTqJkP0vvyIxeJIQxCjXx8mFFA/wAt/Proz9BvHctkfFF6jayBqRXqrZbKWkRkaxPa/uRoNEBlRZSOpJVSrnqxkDR35zvL8opFuNNyCBJVc69jO89R3Jvvjm7nh3MM8q0NvOxB9KUSB5cSkA2iT6Vdgm22FN82+UMjl6FXmEsMOGuwvWvZJKkYhneYqGToh2IpTJ8sdaGmIU6KnV8L4LzV9Qycj8g463RwHHeG0p7d7N3meWV5VjMorxyM3aSZzGCx2Pj5Yn7QS/8AWD4oxK8/qcT5NNXxy5XGLXXJokcb2HgiaOvM0Y0ZT7oBdRuUr313IDMt/E/qG534oxd/xDyi9bhwWIikgq4rF9I6s1rv988xGmlZ9fvZjofbrWgs5Xu0tQ6ioprIWAYGwP8AIOKuhTVtsGmqiVLbJSCrfT+sGR7Yj/Jmd8e81kgPILMmEnqW5a008Vb9TdjSOIsIjB2RdGaUKSXUr1YgPrr6nvprg5lRlHPvF2QTDQ4GzC2WmklYzLB+spiVyQADF0l0U2CVEm9/x+fT15P4S3mLC5bmeJ49PVtWpzkIs1UhevY/UVUTR9xSq9LUSSBvg9SRv5YFz/NvHeGV8KvE+O47jnHZvIUkGCtVMDWWtLaLKzPOqIAqqkSyDbduxO9LodpytzAs1CWQmSqL/O/0GKjL8qNRSrfJhKJn3iR8icGXwfAuY5Lx+LFE/wBP41wjFUmc/Dva6I0rMP8AcQ/2bO/nZBBGxefP2Hw/NbWH48+RhtRYkPeydW1FIzzVw0QBX8D4brv4I22tfJ9fXFeN8U8V+NsVyPj6S1cdNi61pTM5LshjDadj+XZj2OwB2b4A9Ld5l895QZTHy4GzjacWRtWKcLz2DCZUr7MsxPxsM1lAELdT2YkH7dSbi3G33qZSRqUfpFxHHE/LDNqnbWhurbUdIiT1JEGfywRsW/B/HfN78+IozqseFtXGv3rstlY/cnhJiUsPsg2yyFk2ulbejGSUy8w88PKeb3sbyrECpau5SrBar2Z2kmeqGdApH4CsJnI6/ADkAgD5Ifl+XkcHjR8IchjrN+8iZS9DA0bPBH220UbB2ZmcAElf3Fh+Pu1Bc95nheacK4b5hWg1/mOAijw8E8GmDW3ljhSaUa3J7ErCVV2D7hj/ACGb14ipdT5CPxtkqSSD+Fe4J4IMR23G5xP52yldeoEabAgx/aP584jB68T+HOLZ7hkFDlnu26WLJSxXexpHumRprJkK67gNM0bEk7YSaJAjIpnk7G+Pcn5U5VyqfJ42zFWpx5mzeI91Kcwir00A3tHcr10WVkUSBtgr6+cZm8fj+G4rE8ezgh97CU5cvkLM85hS4Kie5F0V1VizkuwfsD/cBAAHq4+H+O8a8oceyHPy+J/SXW/p0bLGUcTSqa6qVliCOsrSxx/a6fOmVvjfq0pT6Ph1JvyevQ/zaIwIKEsqTmCXbW0pI2HM95E998KfzDHY3lMORyfM5HXFYa/YtrFDIAtmwC+2LINMN9iCmlI/H59Z6ICeJ/JlPye8/KIs7xPhl2vJejmk6p+rrFjI6LIh+1xFofwyk7IHwRnpO7V0dI6pp0rKt7JVAm8SN+uOma3qqnZdZbajSBKikk6bTeIFoAvt8z0A83cMyWX8WcrxmHjje5cwtqKssiB1EvtEoep+CQwGt/zr1yr+nblljjvk2hyVMlSXN5XL/rJlkh6V3xcRluyRe64Zoy0kSNvr2AVRv5IHZHIXqGa4pfjtsVRqciylXKHqUIOmGiPg/kekw8zfTP4xvct8ay2MpT4nPmbFqO3HWYiaWgplswQqSNLpW9oux7sD0BbZBo3Hw2fXsBOOU0dMFJhq+swAMTvl/HU+QU8f52wfFMjU5LhLsbYyjPaSGPLmufcHYg600aSqmyCSYu2gpUK5X8ZeXcp5Ss8y4/xXkjZbP5u1ew88NcQKa8s8jmQSnQjVopE2x1rsd72R6KXkr6gs3X8xVPGWdsWL2Ow1qhHYoYlUctEUMwMYcqFVkibszHsnuR62Br0UMj9QPK69V/8ATUUTtj80tWwZHWQXUklLWniffwF6TMn4Ch4R9wBBnM9qUpSny1f9wDsImRPuTi68ItOUrhJZu3qHqvBIi3QpHfY8YAHgP6feVeRvMN/zJ5JqRxYkZhsvcgJlWGasEWSCEKU9tl7qAw7b6r2IG0LHnyNzGrzXKYPMWIKc/H8e625USmhsO88cypaSUjbJJ+o2ypvQkIBLKChA8kUoaXjHJYzLWJo7E9FqUkUBFeCvcJLTJAn+9kKSL3+dh3BJ+4BIvqI+oyrx/wA0cdwVLGPJxmHAQLkP00Xaauzt1UgD9wiaMkA/DCZ9aJUgLLcwXU1q2wDAEEdBEW7jG6vomjliXCYKVSk7SZBkx1MSe2CNist/ozPubuNyM1PM5SSrdsWoiyT2UP2Sjq4KI8X2E72GJ+fu16k8ryerXyyZGapYu44xMkq2H9wL218hxoN9386/DN+T8+tKrNFksTBi5Mg7w2x71eyp9wP9wMbKCAGUjR/96n8FSPVtteKOCXKjZQZu3TR2YSgSxSISpIb5PRl/2/DdiC+vuA2ao17GXsI86TPIBIIgRsLE8jr74hn6d/Naxa2oEcKICgSSSLm4EgA2GmOhOERyGYscV855DkduF4K8mSegsQ17klRirVtHY1+2Ndn/AG/879G7nPIsPxXJUOZ463FFVy6lMnXI6JFKv5kX/CknYHzr71+PwBZ9VPjihV5JX5Bx67Mce6LFeUOJGEkakpLtQF/Zv4H8Jv8AJ9DvkvknL8q5hhKHLKUIirw1682ogqW5uoBsOP8AeWOyWOySdn8ABLmOUHPqlioR6W1ApUT04t73Ht3x0Lw54qPhWjeZc9bjfqQEmZJAlM7RYE9MTnOfOfOfMGdxXDcDlrceExcm66u7aKiQuWYfwgJ+F/wF/wAACi+XsvNkc1SxZsO4ox9TEw+Q76LO/wD83/J18AdR+QfV9wmN45wnJz3cWFhe0ulEpBABPzon+B/+vjfoeZihFfvy2qrT2b+WuRQwSEfZ90n538a+VCj/AIB/7+rlvJmchyv4GnIkqlUG1vzJP+eMcyqs8qPEucHMqpMAD02iCd4HAH79TggyeK2r4bE8/t3onvY+OPIzwEEmaCJiUH893ZE0PgfC/O979dH/AKWeQ4Gx4IwE0ZtYyhyvHBFOgj1zFCkLdO/wR7kMhDa0Ro/I9JNPyGDi+Jmydt0FSOKOOPSgwxgIBHHH9vY/A2o+SO38616NvnryZV8ReBPHeL5LVuRS3eKmPCmlKqSSzFI26yFVHsrCJB1KNs/j8dgZ7xC6p4CkZslSp+g/hxWeGsup6ZCa6tJ1hBniAokiOhGwtgsfW14Xfy348u5jER0sryCJqcOHjryvJPNZkkihcCIoQFYBSR2ATq7bIZl9LNyv6GuWWeUcU47yh61CxyTjbfp0tWoPcTL1olklql43b4Ke57bKugBtkYIT6oPDPrC8irg7XIrNCq8ODtRQe2k8ry+3ZSSMtGZmkCyL10GA7Hu2yQWBYOk/jT6meOY7leG8g8kwvP8Ait1LmIu3LJlZJl0BG1SMddtKvT4VGYNs9vgeljSn6QBC40zuJt+18FP01HXBTlKolYH4SACepnmBhLPMf02cy8Gc0o8Z5dD+upZpXGNvYwNMJXDdTH10D7qtoNH/APIaPyD6PP0j8UzmC8h1ZfKLZutmuPY+fJR0spFMk1OiKNiKJx7n/pasEprQDbH8erF4t+o/K+Z/LGUzHLMNSwPHuIY/I5nKCVDLJX7CosyRuRsMzQzD4APttJvfyDb/AKlvqi8d5DDjLePp6Oa5DyPj9nj+MkqMDNF+qlh2jfB+F6dup/kaGuxPoxTn9pF4/PCMJUqw2/bDifS3yHOeRvoi4be5lae5kc9iLdWaZkVWkj/UTxIdKAB9ip+AB657ZTkfOuKyY3J8jVnqcSyxyOOYqLVmGS1HHGsI+PnQ/ugaOjpm3sba3iHnOLw54U4V4gsYZql3jPHsPUso83XvdkrRz2R2Qhl6SSMDo6Oj8ro7V/KeTeH855LmvJmR/QY7EYW7Kxx0NuZpctdEXQMse+vRvcjLNvaiBVAKjQ05lSlaUPJE6Z97xt9x88Z0lemlbqErkyAUx/uExPuSPpiwcyhxGc8g4vlicws3MbkMcYbVYSot+nErEyJoo0bJ1KRLGSrK1xdHY0w947kMjg89W4slWPF4an+iu43DNG72pejwSdZSxJHY2JLBb8bTrv5AG1xbikleOzlHsMXz81jLJisczlI0sKhgRgAOwZ1LEIChjETdnHR1mOI+HOReMOY4znvlDI495chDLnLcsDe836qWRtRtIBodR2cRr/MibOxoJ2aRDqFomUpttzxHHafpvhdmCa/yUuVKp8xNp6CTJ6D8zg8eUeNcf459PvNJrCvUzOQx6M9wRSskZinSGMRuFZUJFeJPccn/AHRoE+0G6fThxq5yXD8U49PhoKsGLweLuZtwrhZGKsyVZ1LaaVDtdfLJ0BZtgJ6XDnNXyd56x3HeK8Jxj0Vv539fekbZMliN5CkJP+8R/H2g9Q33HX5U4+YPN2C+mzi2H8S8PmFpK9U5TmWShcGSQSOQ8EchIAlncOvcbZF0VBYgitpFBDbbM+opH2F/kOcDtUz7yS6BMT1gCee/8GLPzjyCPJEuMyWJgtWMflrduP3Mf/cb+n1pUMCBhook39qRz+R7gXf7fWegr9OnifG+TMViuUcbuX4dmY3zbgc08d3k2kdOFwAZGXRBGo4ojCunbsXz0bTtN05WvXpKzJETwBv8sb362mSEsrURoEWnqTxzfDZ+df8AVo8VZOnwuK69+5GsCyU13LCSPscDY+BIEDf4UsfyB6Wv/wDyfkvMPNsxFx+yuRzeNir4jHOGiWJbVn7YNd+zKEijmkfoQRuTZI3tzcliYeTcbyGEFyWsuToy1Hmi13iEkZXsu/5G9j0k2K8IYzwj405TD5C5Bj0qNnhZv5OWBlWuj2RHW9lCjNoRoJQ0emVXYqdb2E8wFq1H/Fp/fAOXVXlslsdd+bx+2Ld5o8QeNKPi17GN5pjJeeYuN79rNV4DD/UlZVUwM8ZKqpWGGJQSf2Kg6mTfpe8Ly/KwycbwFHHbT9DPks1LNMGT2msiFJIdEBl96I7GgwDAaO/Vh8zcl4xiMVbpY61DnLuTuJXr2sfKxr2wr/aVO9MgSN99uw6jt8bX0Kb/AI4nx3i/G+SMBWsWasBu0LpkSSYwu9l2EyAKWCRukQ0Nj7W/LPr0lqUIq6HWtIkL0JiQLCYv7HbmOuLvw+tVLmYYcWdCk+YuYkSYntM7EXE9MSXkfz15LkyUnjySdsnVxXuVKKsWN3TyyzmcjZZmEjSKxH49qE/wD6OHgrwxhfOHHk8o5WfMjLGWbHWoqMaNHYrJK7QhFCmTqUdW7AEBSD/PpXcv5B5J5KaaphMnWwGZasK2S/UoiO8PUMJEk32O99i43vv23/mX8Ncj8h+MuK+QMPR5qAkfGZrVyIyd4DqICuEZhsHqGAK/w/X+PQ1Gy7SrFVo8vgxdUm09Ppim8Q0zNZlpy5tYdQPUDASmxnTvqO5mY+2HX+pFL+NynAYuLcbTJX8zBkYWrUIQ0lpqvsKj6BUgxr2IO1YAdd/x6olrG5eHj2eGYxUtawH3ahkMVuaFo5iGeN9qOvZWDdew0DpjrfoS4Xy1zLK53D8Oz1yKxy7haziHKVpnnN6D9Q0MsJkkI7aJWJSB2AXr9/2+p/BfUH/qTyZiuO8Ps1eUTZZErSRyow/TIzSSSBowNNGvuH7lCkgDQ1r0xqUFh3+g6QT0v2ki4BMTJxG5a0mspR8VSgobSTJOk76oCragAQIExGNfyVgZMzwLOZS7bx9WYYWpawyzadbh/UwJKrsyhY2SP9T9v3EyR6V/wCnmQx8+JSSPNY1cnjKzGutyPTvWjABjBI+4EA73rRDA/wDHrp59U2F4vj/F3JM3d4qIclZxT151EYSGGwtiuFnhTQVWmaWHvoDqwlA3sluYVrmjRTQ1s1UvQy17Hs2pICPblYJpZNH4+VILRttSfka2ylllHmoYKqtUkqICvpuNouNtowhr3EOhHwLekoT6k/7rmCk7zAvPJMbAGv8AIb5s0orOOe5PHGSiys5CpGPz8AD93x8n/Hoj+GcVjL/McJDeTIS2rUbWzHKAIa1KKEk2Ao/Y2wyoSTv8/wC9T6ruRx+I4rZOQyeKlaKxr9LVkiYVpJN7VlJBXqDo9S51r+R9pJPEbGOwGQrrg7CXclnW65jJAaT2kiLCGD/4dlXs34b/ALfhkw2X3UpbUFEkbXAExP7YHq1mlp3HqpBQlsEwr0lRgkJA3vFz0x7ebuG+Q+I3eDcq8i8Pnxni/KZKtLVyUckUkd+It7knVEYuukD/AGsNtrt/KgQv1o/UvjPqE5diK/Fajwca4zWavj/cj9tpC/Xu3T/YNIqgfP7d/wA6DN/VtyvhfkD/AKb3AY8Pkov6jwrk9ChNC++47VrqBF+P5Ve3/aI/8b5sD5Ov8+lVawn4orIukmPY/wCMH5bXuuUCUkzqCdR5kDb5HjDTfRnxa5dwHMeSIivFDdxtRY5l/tybjtOzKdfLqVjGgfxKSf49XD6NU4evKOdcc5/lrNLl9CqcZjJq114XgQTFZmhbYUu08iN92wdnY0W3ZOR5Sl4S8P8Ai3C0vcrnH5isuZiUDr7ViBTYlfQDE9w5AO9fP41r1VOQpxTiXnngOQ5JYo1OKRVrdK+9hAvaOKTbKxPwQQ8Oj+dEkfketEa0EnHocKFgpMHEl5Y8LYzxb9PH63j/ACBK3IuU3K9Xld2a4ZI3V2UNrqu1BkAJ/I1PKNlSCKF9PH05yYvyxxfP+Q79NsHQzcM1mGAe/wC/RR+sk+130XsQF2NkggfIG3Zm8t/TLzTgvJXm4rxq3gKNIxyXp8bCUazJ1EKxMBt5ezqR+erMv4JG0y4v5tksZKPCYDi0XGIUKl/1Ze1YsEKQFBbQQ62NsWP3fA/Pr5pOvcxj5ThQLCcXH6lbWE5H5N8h+U44s/jYMvk40rRHRMlRFVfb18iP3P3dANjt8bB9CPhmO4ny7JZbN5CtWTimBKVqtNp3Hdtt0hjBIV2lmlTbFh0Uu/8AHz6ZK5znyVy+LE3MhBHx3ETx5l5Y3LqVjmcSf3NHblhMdfChY2bQVd+jR4wb6dvGXkO54e8j4aWxjZqr/pMyhCMcu69mQOSOgRZIgn5B+4a/ulTqzGpDTZQncgnvA3OM6FrW6CvaR/ge9sbn08+P8zzzzFJbzRE2Bq33ygRI0gksyIjpUYuEfpFGk7EAh1UL0JAUepX6xeVWs3Fxrxvw6/J+rwmTQ2e0kKmeZ5IoUjbqOvYSO7s34LEtrfybdwK/ifpx4dakw9jN53k3Jrn9NxGNijElu3OszSLGFK9esYZB2Gl/tsykiQKRFx/wnyTmXk7JWvNUEnC6+PsR5W/VlLGY2LBLxRIQ4DStvY/cBpifkdCkp6rS2XdkC0xZXQAbk+2D8xQ5mT6aOiQVK5G8dZO0cTsMOP4du57h3j6nzGpxDECR8alesixTTWq88rdWnXsSVVy3yq6UkfABJBoPjv6WKnOvKXIeZ+YuSYvLW7MgvY3DSuJhE5jjQ+6gbrJ7SxJGq7KgL20SQR4+QfPdbDcZvTcfp2K1Wt/aq6/f7YJQtr8lQD+P8D+fXt9HPIrHmTynayVtrMsGCtPkY705kR3iMLxMjfj5Z5lbqQQejb+NAnUlPWVLRfr0aAdgLHSYgcQLX3JIEmIw2zJuhyhryKVwF0cbjUBuqZBN4HAnbDf4Hj+L4dgqdSOKnVWJVgrxQwCGNm7BftQfClncAfn7n+d/J9Z68ad9OeZ08gr9hisXLJXxvyOth17RtYHUnajtIqn437j7H2qfWeiSsuwQqBiKVppFFsoClTKievOPt5VxONN9544kiRR3lbqiliFBY/42R6Xfzv4jy3l7hfF8jl8nLj+Kchyt05iuq+5HUq20RKNgDaqixCJHDP26PKPhh9pYTyHQe7j8Jwana9i1yqwKh/tglqqruywY/CukRaRf5JQAfn0O/qu+pPMfS1ynx8aHBxyTi3JIslUyNCuRHZgNf9MYpISftI1K4KN8HS6K/wAt3SVpKRucBZez5BClC0yRjnp5ijxdbyDyRuG1q0mNxrCDHyJD1igsSdkkSNQPtjQ9Ix8kkJ2Ou2vRu+lqvTfxXhOM5zkOTyAK++1GzW9lULFnlq92VXmj2SxOmX4Khip16DOW5PjWhuUK+Nkom9mrOTuJKgQxGWQyIqov7IwG0NEj4GtAD0R/px5fUqcm45hp5bC421lXgnrxSMWlheR5JZNHZGj2A18HR0AVJOnxHky3Mkp26b+1RtvYySsRvsCe1+CA68P5rGb1NRVf3AAHbaAEGdo2Hf3kgPyZ4fsnk+To8PydOCnjWkmjhuTBRjkjjeR0Zz89OrH/AOS912CvVwS/HPjfG+UfJmE8RplZ7X6qtHd5HLAiBykcUgRGKjqi944tqB8jX57Hd1+uHhPEOLLyDPcAkO7HLLHv1LH2mK3LVeFxIGdvehZq5IBULr7WHztr5/0uuEV8nx/K+Yr2NgrW8sZKMcaRaSOGCeUj2yST13KI9b+BAv516U5g8EUodmeB/wDLb7XOKOi80PeQ5NgFGeRANx/5SB7YG+V+m/MfThdzVzN8linw2Eihbj/tlUMrS2FexGdjun2qCE237V05Jb1T/o+ar495e3knES2YcjdW3EsUMSPqmldySWc9Qu4v5VjsLoEkAn764eZULeDzdIyl3FiExoF+fsjlJ0f/APeI/H+PQRzvEch4Ri46nMUsGlJg8ZbruqsP/BzRK6MUALFg8TKdDZfX7RvWeUKcrmVuL3SOm8/sL+5xn4hFPkXwzCkymoJm5/p6RYgDlSjF7QnY4K/1O+T6nKvBOIrRXkee1l1pTdG/8wwiR59gfx7sMR/P5B9IJzur+ku24JaQcyvGqn/3JKFUN/3DQj/+R6Yb6qrjcY8SeORiniL38O2dtGFenSee9IjxMCB8osSxne9t3+fS+eXL5EWLuRvpbUJYa+NkFXU//wB/z6tmqVprJAn+5ICvkpShGOcvVTi89WpAhtalJA6FATi38G+kL6ifMXiDKeSOA0H5DgOPW/0/9LitdrkpQsHMMP8AvKLr4320+lDHY9Vm2efeOvYjznCbnF+scqVVzVSeBpxoRnr3UbYEbI+ANeugf/Tq5Le4x9M2HzSyS/8AjOV5FFCS9OwiRJ3VyP8A0yIiD8H9x/HpdvK+Rp824tPg8pjIslksdnjauXblpZp47RayZo4yvYCNleAN1IVzWQnZB9TeWVylVyqNhOlUj5giQf39sNc1ZSuiFTXHUmFRPQEpj6WGJ3J/R19SfPPpg8fcX8fcTTLWOTyJy/KQvlqsAhgSp0xyf3nTtK62LsrAb6iWMMQR6Av05+J8bjvKefynmDEsmM8Yl5stj3dWD5BHZYq0hVvx3jctrY/t9T8E+unfJ/J2W8RfQXL5OwdpKnJJ+EY+LHy917QyTV4VMygAaKiVSoPyDo/I9cXqfMM3QweRwdO7NFHl7CT33Eh7WOgYKrf5ALuf+S3/AB6Eafeqklx4QSTHsLfpgxLLbCQ2ztE/M3P54b2LzKnkmHJ8mxFSm3v3bIerNGD1iV3KRgD+GRgdH4JJ/nZ9fOag4V5T4Rdwt7DvjbVzEzZbFzs0U8/aPosyokbMwMftQKYz1bqvQAdfSueM+TWeK5V7QrS3K9hTG9OInvIQrFWH+NFdE/wGPolYvI8ntXMTk+A8IirZLK5ZaGLu2pYY5LV6TYYR/OpB+Azdup7IG320SUaUmcYLSTtiEzvOsTisNgcUtCa5fwGNjU07KqlWtOHAPaKPSmRu3dmP370rsSWVYLiPA+bc8yUGZvQXquOnKqbkdcr7ikrEEiCgAliUjB/aCRsgbPpofp78DeK+VecLI8lTZDH4/krWI0rmrGVp3CRKIACCzbeNkRuqtvqOp7EhovFnjbg3hjDZC/5dryckyv8AWIL1fj1Zo3ixagutCrcLFURgLMbtDGpKmISNsMV9eKcaZTrcVAGMFrI2t3wmnLeF5fiuFx3hbjd2zXm5fAjz1rKmNsXjYniNgup+QZLsUwVtgsqaI+8eojK+I6vkTyJao4LIsMTw1qUeWGtfa0Z7yCTf7iYootfka/wmvU8nOM75R5rkPKt2muS5RzDKTRYmCCMKTG8/tJO671Ep6xRqCQvYA/LOT6a7zL9MGF8Q/STynjRy1RPIGUX+pXr/AOphgE0oLytTjkkZAzMvuuVUM3XsANMCQdayoqO5Mfz2H5YAbS+7VFaPwIB+s3P0sO5OBj4/8j5jEZgc14zi6F+7FRnq4W5ZhaxPVqzgPNPWUfDTPDGrqrfHyyfJYg1GPyPn/I1ynBx3EULNKSy8stu1J7965Non3ZHB0rKn5ZtBV7HWj6Gvgfl+ch43ex16J5b/ABu9ArwTjRjCaEQYf4RokUj+FDf8+jriuRzeOsRyjk/jnhNO9DdjepUvz1A1LHQXAHjhi7D25JVjmQyKvc/7eug+9uXssJrQ4+SfLEpTMJnv8jbGdNnOZM09RltOkanF3WB69MAwO0n5YrfP+DU1rSUq9anyv9DZo0F/TK0ccMjGF5PY/wCymSLbfGlZh8soUx08jf8ABfj7FY/i/Nkl5TzbI0hLDWjQrVoNE5jjLMDosklck/boa1ojbjrGcR5TjeM0+M8XzliLOpkqNKzSjVAbda5VP6doWJG3ViO4bQ267CnYOvzG0/LfPFh6VqVMBiLz04Qh2kUftCKoCFHx1Ec67PxutGdkj5LzbMHEJ1cKnfmBb72+XXGWXZd8QpQJ/AJJ3MkgRfrY/wCBhrePfUBVxqYynDbWrRjWCzLCKwValNGQPEg67cdXUfGyPzv+PWehti+McTwtHI5rldi61ZI4/wBPPZkH9jS/Ptgf7f5+dnT+s9RtNVvqBCRqA5JjvihVlzZubYY36h+UZ7gGHl5Zio3a7dxy8awBjnCGvdtGR5p2DEAdI68ZVhs/u3oAn0C+HQL5Z5DL5R+p65kXrcJ48zYTB/pClLIw14f1E9mSYkiaeTqjvEoRSqx77qCAyflWpTs4a02Tordh/pNmamLADRVrsNeXpJ8nQZlldf5J0P8AHpO/D/n/AMe5+1geH2p45OPZTI51mW6AFarDHcDRyA6PuTRXS3UaA7Kv8eqd9bzb6VJmP05nA1I3Sv0a0rAChe/PYdeuFf8AMeWhz1fL3Fw9OO7bnqNFCyNKsWrCKwVm0CrM4JH8AAbI+TRPHXPOY+NfI+MiWlVgzWNyFdse0ECqTMJV6N2HwybA+D8f51o+jp5T+nTk1u1i5+AZI5mhkOZyYihRro8skMS9ZYXaYkj2mPVe0murAbOvhbvxjjPH+LvzTAJxOCfl1rHx46hBmITHfxUplrvOiq6/BMJnIcE+4FjA+1pCWdZmVAsITRApGkJHI9Ij1Em1og8xxhbT5dVkrNWdR1FR4NzJ0gC95npOKVnRieRPbuZiKTJRZGZKtmpOe7wsJC7OToH3fuDduxPwP2g+n08JcVxfibw01HFAw1XWWzXR2UmP9VK9gqWAAPUzn+B+PwPx6RPG+NuV3ueYyncr0sdHbyFaNoPcZrNuTSKWO9jZK9d7A0pI18H02nnLy1iaeLwnizEh2zXK8/Yw08EYINSCMO1qcfjtGkKP9ykgMf8AKMvqSzOjUhKGmzMkT0BAI/L6xi1yetFU+pTydMQIMyRIN5nkbbCYFsArzpx3k3kjGV48NUfJGtar5Ywr2aQzXXdalYoBtCteOKdgdb9qT8ltNTPOX+tczFHznP8ANMBcxPHshS4NT4+l3V+NKjeyshhZdt7xieYqCde6T2IUj1ZPLP1l4y/4Vl4jwTj0WA5TlsuTfyVuYBpUZnEY91VUHVUmLTA9RsbJG2C1TyMnkHgOG47isFXHJamReRM/bEfWwo9yRFaTt3eaRw8OtHUaAjZAAZ0CimKdi43kfSPpF8YeI6IonMaz0qBgBR/tEkKHAvMg9QZiMbXnvEZ6DkON4JzJhLYxGPnpKoftG0bdpopI9/lH7mVT/IcH+fS55meld51xbB8gmX+kVRShmYkgGNunuliPx+Dsj/Hoj+buYzZzGY7mNDOe/einlCwCaSUQVo5ZI4kHYnqhR2+1dKPka2CSCuT43J467XGTnMz2IFmjfe9p2YA/9j1JH/BHq2q6kIoE06EzpiTwQDYfUHHNaVpVVWKq3DpK5hPIURJPvB3x0g8KU+B+Nvp1w3Bea3Z1iu/rMhZmEfv+zNPZerLGYkId1CVDuMbZklkIBHwfnx9guI+audZ7Ec6zsnF+JcJgt8iuxpFWRf0MRkWSEhEEncQQ1VLORIoimUgsvf0OstDz3HX+FQZSxPWihuw8px9dpUmLxvGwRinz17jTBW/2EtodifXjzfyx+m5DyCbPVEyGBvSNmshVxzLA9mMKHWAvolEMoQs2+3VT8MZCDCCm8nM1qSZVqUQbiE/htbpO0Hvziu8t6oypt9wDQQkFNiZ/FcW3VJvME/LB3/6ifO6/MsL43+lbgv6Kjf5bJXyF1iwEdKlWWRACND5MgnPx+4V1A/cB65a53GYnH5zN1sdalnx9C5LXpu5X3JlEjLGza+B9q9jr4+NfyD6cz6jebYDjeZyfmRbMuTzPK+NUMNxy1UgENajGtAQyWYQT2CvIkzJ8DWu2vuGhV4v8B8cpYbjXLudZOu+RyV1nlwlhSP0lXYWB5l2GLSMJD0/AT2ye3cqpr7iKRsqA9KeBj3LKN3NalLCCApXJ2HvgdYHw9yK7xSvySfL1sfUvOpeEh/eNUuqtKdDRAB79SRtRv+RtiPpK8f5vyhXr8kfAizmeJRQRcZSWZ1griuzPNMyfzJI4Eat+AUOx9o2R/IPjDg1XxCctjeIW8pnK002WNqAWe6wQStLJG3x7LQ+zUUaOwu3OwWG9r/p/+ScHxDxjoYHK5DMZK5Mk0kITtM6fsjQysiBRENklguwRvsdetVO/8Q3rRckAx0n9Mbs3ozl9QphUAJJTIJvpMSehPa2DLzjiWI8ncWPmOhi5OMWaFCDIwxCD9PNNckRZZvf+P3IshXY+e5be9a9JZzDxx5xxwxIh53ev08tlJrqVPekawrEs7WbBRdq391FEsjAkSRlSF0VcflHmWtzXxbTxeRkevfz1uWjbrxALLCjzSr31+NrDG8h1/CH1WfKWXwvHcZmeV8egiq4/IvRIRGADoI9sH6/av90TPr/k/j50kzjMXsvU2EI1STaPYC/G8z2wb4ZyRrxDWGlccCBEzvF9x1jb54n8p4G8V/SJ9NOfu2Z8XlPJPKcJao4OKPIGT2Usxfp3sQH/AHFIpncyhQB39tCO/wB+9xLy3L9VH068p8ceZ+O3kgnwb4yDKx4ySzG+cqqFS1XaHZl/8RHKzEHoDGF2A+iNPPXLvIB8deJcdi4Ex8keLyceMzpgFu5G0toPJXgZv/K9uKGIELtgGRQAFcegr9G/DuCeYudTeN+WW/IeItcjdpsLyGlknEK20WaQtMgUKSzq3XR/d2BPzv0xQ+5WMJfZ9Oxvfcbcbe+4wirqN3Lat2gbM+WoieFRae87742fFPGszPk+MY6s1HMvYuHH5+5jW7kTFCipM/VRIyj2wWG/gk7JBJcE8N8bcN8Scv4hbWzLN49mpcoQVITPLEfbRf0yIP3iRK6I3bYHuFjrWxHDwva+kvh/KJ8b7HIeQWxLYmeLfZisXeSboBvv0PZuo0Afn8/KzY/yhy3/AE5juT2uS4q4/KuQVqAJYznKVK7TOFtQEBhG0kiRhtnbwtr8BvQ9O44/UKWv8Cd7fz+RjW1lhpFthj/uu2F43Nt+JJHsegxdPMXmG1jLvEeTRSxzXOR4aKWPILjkS3OXkd2jTZdY5GsMp7xadT3BdHV1X54hZ4r43wH6nNVKsWfy1uC5JNDGCtSFI5SVKb0EGyex1267BUHXobcr8n8h5V5L/wBW8rweLlxnGLUEFIrEsTRvC8SRoYFIEaqNt1RVU9f+T61f6hkrmcnnzWDtWchkOt4amUd/1LOkYDfj7FigAHwO7uBrYBfM0qXVpqVKOwInYci3b8xvhu7WKomlUaUpBStQMRJUjknnew798M34+5bw3kec49iua46MYiwzoTralJRJHC7BmPYDaMV/g7AUL1HrPVN4rwHn/KcXh8xwXjmUzGPqUa9ywsLI7xXZIELrIhPbtsv/ABr42Dr8Z6mFKdZUQ2Ikk3nGow7dRw+PlitBkPG+b/Ub1Vxs1iMqqFlkWM6K9gQPjan4/DH1zV5P4v8APXlTNy5PxtwKJZeP4OP3cjTiNZC6wJCscDHQlszQxwl/3H5OmVCCenuRxOP5ThLOAyzS/o8nRlpzrE/Ryki9WKuPlWA3og/B+f8AHrn55D4vj/A/lTN4niuczAx2EyNHJQI+Wkeau6wq0UpDkJIU9z7RJ2XTa1+fTupq/hQHDsOOs4HySh/1QLpmxKzeegG/f/GL14k8nY/yHy3xpkoJIqDY6vjKWbrCcq8NhrXt14yjJ8yPHSpdgp/9Qj5JG9HnnkulzXy1kuS5HHSxRROa2Mty1/7kiwieOKFT86BEjsW0QW2D+PQeXFcL4L5Ow3P+E423St0KUuRr2a+TnWs/6gzASFQ4kQwCPSxED3A8bEsnUvdfIWQo271a81ePBZTKNVzmbrx1kkCWJoTcLxuwJSECRXWIaYNLIrs3tx60UAaqH1p3JUFERERtz2n2NxhpXUlRQstPhMIIUkEkEnk26eqJ6ixxs05LeezlbKC+GXDwI7yTN1LnsNlWUk70pO9g6Ojo/HqR5xl3inzOBpW0aSnkco9WzOvzILNd0M9d9Dr3il38aCsxI+0hFFfGeUZjM3rGIx94YWhmq6x1b+T7wo8ptvGCrlTtEdYIX670HYaB+VzmvO8xjeGNmYK0WVyOVVIsdDYrKZ/elhhiRYkVjqQdoVPUjaRHQB9bK2hFWtAQYg37dI++NGXZkKAF5YkkW7nb5cYXHM8VzPkK1lsvYM+OgxleJjWhgBjr2oq0aTdwv4YvHIQBtiAT8dlLXvi3IL2J4Re49dqYyfkczVKuKZYx2Wec+0LG9bUIOzbPwHYfgt6tXkI8o4t49pcMMlCpfzNp7QKsE9mtKA0ySMBpUE9nZY7JWI6PydVjwfhp+R8myUzZDBTWcNB/UmsCeQNKyOEjgiZwEGgzS/n7ghXZ7gFnTN/CvJcaH4QofUR9t/cDAWZViczy1+lrJUtxTZnslQUQfppEf2qUO2Og31M/RNgOS+G+S8J8V+McBxOvx+CPKLlIYIxc5BPBWJKD29sF08n7yO0qL8BSX9cpOVePo5c5x7DjPRzWjXSK6rkIUVHjiHRSdliG11/J6E+ukOV+qby1hJP6ddzeWxtuXHQYmatMiOY4QuzZigdOxn6xOGdn0S+gA2usb4H+j/x55Q8hYnk0lhscaMK372Js0AliSMSKUDE6YAuFIUqGH5Yn8Nm1nNLStKpX0lRXGkiTsZi1r8E2F7gkSlqKKqeeQ/THSBOqY5Ec8jkAybWIGKT9VPGcXx/IDCYuTETCrxbHVlimV3aOM0oFEsTAHvsvJ1O9F3G/gaKl+XMXZ4bw7GSzTNegty36IWaQKzRyqjxlvb0O0fVG0SQW+R/w2f1l5anZ8+cR8a+M7ET43CQvgYrNqYPPHMLbtKkMnb3CkIcwKDrqY9kkKG9BLzf4vy/L/PPC/pf44sMd2zkh/VekwMUVyaYpIzyAHfSMFidE/f8Agk/KtpDiFwoymSZ6TNvvitaUx8EpSjLhAQBO9xBEf7QnTEX6kmMVv6ZOKpzPyDxjB8pzFqvQwMNOxHaljketj5JZTIJZFbXuFYnHVVIADbH4LeugfPvBPEm8Oc75nzjEe5K92qcDlBchuy5GZZ5x7wf8+3K1nbgkE9GcKOqj1NfTdyb6X+L8xrfSu/F8Rc5Tj60siZYVElp5JlHb20kk+8yJH9rArrcMg2SPQ/8AKF7yD5czTWOI8SxuG4FT5S+HxftOlagJwknu3p2UDvqOBmZtHrtUXbfLi1dS+0guIRKlABKf/wCuLc/TDLLKKiqHUU5d0obUStwwB09H91wIEzYkwNsBXjWY43Pmr3DLecrwPmcVdq5nJStuJazVHRq8I19xCSs5ClSFDsuvnYcxXlzCcYyWGw+NaHGYSa1O/sCDTIZHWYkhf8JMIxr52H/Px6Z/AeFOH8c5v5Z4Bznk+Mn49xjh2YgizUESx+xLNVgQzr2Yj3VM/tkb0ZNjQ+fXPS5iK1LI0s5ypMjnsPFDG/6uk4RZuygRREn9hBVgf5Ovx8ElhQ5YcrTCjKlAEn37cDp2jCLPM6bz+p81pIShPpA5t1PJ6nrOGz41alilTlmIpXTNG1u/iyZyjBJ5QpUld/DblkH8qqqPjevRAPGOGc48V8oj5xmrCWqZktTRrcgjRJJYDPrTr7hXuLDn22UFK5DMoYn1V/DUOX5Qf61yGf8AW3ZshYxlDEx1VgFE+yYt9f3F1aRwoP3BRptsNL+cp8E4K/y29/q3KCKaKn7s8VG72SZQqFIW6MO4Y9WJYdAgfW9b9Sua5mycxL6Uny2xpKo3VOw9pxf5DkTyMnTQOrCah5XmIQT/AGW7GCdM+wneYqiZLmmdzMM0ebyEWKXC5PDX4LuPjSjSiuxMJv0zidy8jr2PusCT8N+3YRifHeO8uV+HcA8V+Hc9x04qnIDjqkjNDfZk/v2JJrJLhQHeV+qKNlQFOioId4NyFPIeOzHjjCv/AFW9jq9ipauxyD4XqPacICQwUgE6XXZdf59Er6f/ACnmPEFzkiQ4GxNPDJHBPkJUe1UxUXsBoljRW0BI7v8AcD2bqsap22PWdFWh8KadSElAmO24I2O6ukicCZ7kLeUJFWy4VqWqAYH4pIgxItpveDHScbHEOXea+ceUbGSvG9Zhw0EjXzJuQVY5WEVn9PIob2pWiiEPdgfiRiBsL6j/AC3498B0cxez3Fas2AGUuR5RK7QgripZHBZ1jQ7ZGcR6A0Ix32GX4F++lnyThuHR57LZRB/p7kKzZO2bCF5YKJindwT2PX7ljLAj5aZUCglQU7xvLv8AXuYvcnxOK5FZoXs3/ctX5i6MLEFgPo6A+5w5Cdth0VxrRUF0TS22FOvTAvG2/X9Z9sLavRX5q3T0RCNgFG4BTcwBzOwHuRM4lMx47i4JyHP43mGRW7ZzkiWq0tdvf96tKveKeNfhwDFpiCARrRHyqttYuCnga+LtctnrV7WHnj9iRwWQEyyLFI4Gz2CxlSx/BbY/PYed2jHQkv2Z7MtNHtRqIoiDKsL7fSFj9+/bcDZG2JX9zKCRsFh/H0Xj6q+Raax5MpZ2e9UlMKSJdr2TExinX8khjIqABgsm9fD9vVExVBbSQgSnSB9Rz+e+JvMGHaatW2+ZWFK+smb+/btxhkvpXkytvJ5XK8Ss46jxmdBHma8oJIlMcgglgIH8tvY31CnWt6C562Pp9orw/gN+3eeOvV5DbWQJ1CGMRvIvRFHwASf/ANfjWtZ6jsycKXy2BOm0nsfv79MMmadUSswcEfyk2RseNctXxfJ7PHrKxQSnIVXCTxRLLGzrGx+A7p2Rf52416Tf6l0zeUxNbnGO45YyuRiv4ypZybUQU9yBIIzYsMVZPbaRWXakEFlXr89i2HmTNNS4tdq4a27XFEL+1FGsjhUdHPYNoBdI2/kfH8g6PpSPLXkGhU45lsblZVFbMTrlcpLLpY5mkJCQt86KKep6kkaVGH7vhjWVa0rlIkAT3m0W+eNnhbLUvOJbJAKjBJMDTuq/sCe2+BTyzO2IMj/TzlKk1k+9atTKiqHZIJHRWUED8hVCggbYfBPwbFzjmmSj5hk8tyijFnkWeqMrlcdOoqV7FjvHCqgqncP946KFCgBjv49VLkGYp4+1x7kdOoYsdIyydIQWaTrKshQLr+egO9fgH/7JvE5ZZ62S4ZuOt/VK1R545K3WQPM8Viu+yde2vdSq6IZAN6Hyd/hplVRTegErUTPXeDt/Bhr/ANSalvL8zT5qwhltsFP+0bkRO4Nr84/afOuE8i41zTkPkuzbt2eIGnZxMBMhSGPUftRxg91RPeSMsg2P7Z117s3qu8n5FzHnvNX5xzHD11ZUWfC1oq0VeskVeJ66AhWLBftkZfcILKO3yjoDu4fG+OONJkIsguUnxU+Ouy3KcpVrLKI2hnhikXRVJGiATQ2pPUnr95pMvMMny/hHIJ589h5pbgmr8eiXHWIzcpkvCscKFesCxD7NM2gE6r20fVO22U+iII/S38++Oel3zP6sgz6pGxm/t3xQfqAs5vK2+N5B70tdsnU/TRl2KhYpZC5DH8gftYg7/wDMP+NCY8U8X5lyDmPFvHWCkkp4XnBjhoTWTp7CyzpDPNJ8fHuAltfJCMmj+B6t/CIfGvkfyXl+K+bbE1TiuIw8cS3qtn2Z8faeBVFtQQRIkSFi6n89lOjrqWg/6d3Cf9fth+ethp5OKcMjlx+Fyt+t7Mt+VD7amNNfARUUsysR2VR8nsE2eWzodU4vQQmU9zIEfn+eFb71al2mDLQWlSoXeNKYJCvkY/Idoz6uuNPwny3eq8fzNCbHXqdb/wAHLMVlpSbZpFJXR+Q6MAfyH2T8aLFeJ8hxbk3inE+ZsVy2CrkOK1DPl5o4lLfo4ImeSu42NLIvV+x/APwqn8QnJeAUPH3mVcvnOL4vkkOcsyWaV69qY05joJK6MSFZPx2AXts6O/2hTnGIOMx/Ic543yIx+F5XTgxPJMLV6JXXVr7CibLxxyiX5RdhfedQSjDUM49lzbyfOSA4j8PabQO5j54rmqmprEt0JVCNUX41KmSekmcUXL+G58F9N2K+pLlEFiHlWY5BSmxSKhkmixUdSwHVVGz/AHpDJZZhv+0kJOureqN/0/nz3KvPnJvMCim8a05rEVu4rTNXvW7JRGEZPRXWGGyO3xpOm/yd9EPN8nCuOcM45xHM0J0wGCx036crVkFK+JMXdoR0Vni/8h2E40ToaIAbZ9Jl4/u+Pvp/fjf+mcxHl6l+pbt8oacfhpE1FHD8fEtcfukIBaUb2Qfg7NKp1VKtphelxWx3j5e04xapmmQiqcugHSe5uf2J+XfFh4zQxtPzhibcvFkxi4/NvYSZcZqeRBO7RRiSPserz9weu+yTNoKFZCw3nHyHxbkHDKvCMXislgK1GKfK3K0lWOmsSwo3SLuUeMA9mk2o0Og2ynakU8m8g46XyXX5PdrZAV72JMmHl7xRRmX2V26EJ3+HMahmYqT3+emuov575Xw/JuK8Vz+fnzFTP0I61bldeGpMkVrEiyDMUKBlAR0jk+/Q6Fgex0PUbk+Z1xfabdTLTiU6iL6VGebcjSqdjHfFFX0CksfFFJhNxaxuBvt7fTFN+q6ng6nivj17gc3IMnN5Au13ydV2aB4WjjJnV2fYb/xDM6tsL2Kj5EY2sPjXERcdyF/l2do2VxmIyFajJIa4QRTSsGMgjduquAkSuF/b740NEaai74/89214pRpSYqxwXkZMOFyFyyghx0jSoskz/Ic93kLFQCCTsEdXPqE8w4DjOO4Ja8YY3DZOasYxeTL3InhbIiF1e1IsY+HkkRPchAClYK6xHft/N1mtez5CW1qkqhPcAAT9B+mE3h2hfTWKqEpgI1LncaiTE+6t+0njEjZ8g4oiPIeOYsQ8sZksTL+qdP1kjRmORpJEG5O69lZVKk92PYE79RPkryFy7lGEoCo1ZOQ52kYlh6ukVOpDIyCCM/cXIdmdixdm28js33giXxlQyGZix1Dicf62uiNYno2YBLSJYt/5cqFSOndiUdidbAAAPp++BeM/BXA/FtDJcnyuPu8ui/TzZFZjq2K33SR0q0YBPSSV4yxA2wk7tpVRFn6bK/hYDitSAZg7mP8AceYJB/fHQs98R0zbQfYRoqFI0hQ/CkclPPJE2PYThMbFel48pUs3wnEK1eenFj7s9Y/p7cmTgcLM3QsAUAkZPyQxi3/uYCG8Wcc8n+RfNFrG0uY5exgKt2JbbrM1ZZ/c+6JJYx/6SyqocEFRoj5J9MD464Rj/K3O+XcK4zhqGOkle3Lj3tMZFoV6s8oZnfsHHaYdA69v/NU/PyPW9gOW53FXH4ucc9HPWcsuOkmgQQvJOzsQxUxo0kbTggM4Yn2j+Oij0zGhepwtgrVaedh/P0xzxjOn1tIoHFkMoJI6TJInjnnkb3xVr3AuT57mK8BjKV81j7U1bKRQye+Jo6sckxliVdq0rQo+h+DqNRoSfJ586cH8VcJ8G8J4p40aE0rHIYL0tyCULNYnmp25A0zR67ExvK+j+0a+PkehbfxuY49yPM+eaFlpEj5GtDKxRQkWap2AlksB0YCyhQEAdJVTfwR6rfm3y1mvKlzieL4Zw1cfWa5lp8tYRgwtXpYe0t0AD4VIR9gPwunTZUBjmtkuAoComRO94No5iNsbMsq3Kd9tpSSttopWRcSCRJngyqNXSDjdwXhfI4znHDcZ5GnydLBc8hhvUchibAWf9OXlKROzAqTuSORxogCVNDagC9r4c8d4/Pc14li5Za9av7R4/I8neybSRKJiz/uch41JX4X73KhdDW3Yw/mXzlWwOYydfGcY4nQStLisfZkkltn2vhJm+1fbLJogA/aCBrez6LOP4xLl1a1dRIVnl++WOQky/gffJoE/tUbGtAa9bVVCwgJSbxFuL/e1saToW+48UwFKJHz3AGKH4cymQ5PxyTjnI7kVexxyQ1pVWVS5lEkndugPwCPlf/iy6O/WerFm0p4rI23rU4WuVIukrVUBfoNkAkfOvs/n/HrPQLrTTqy4oXNz74LDzkWxo/UrFasckxPH+M4i7mM1mq7WBUqoiskLD21dy5UIqkOS7EfJVfyQQGeT+B8xYwXLfHeVw4y+VwVKhn4qtOw036gwSVnkjUvtpF9uzaYxgaaRFA3oBmX8f8Yeby/y7yxybLSSWbuLp4XE1ije3FCsaNLo/tHdk7a+NF5B/OyMfPHmTHeEPLnG+XCldt5BIpYchRrqU92qtaeSFe5HVtytFI3TswESaH8et6n5lhAkEEzzO+/HbvgWjbebdQ8kepJFtwR7cg89sJbg3z2Yw8daStZ/S1LMtUWRCxAjaHodfx+6WH/GiR/z6uvNKrVMk0M9OZhgcVj8bfuxI7Qw3jVdV3KQNsqxMq/Gj7RBLFWLUnO/UZnuUZjKf165WxfFMgLWRGOgqGKvLM0sE5jiVAWUMasESM2wWDbIUkrBN5L5FJncpz2tXH+m+RtPavYYkvHDM3uFCdkbKb2rfnZ+PlgQ4yZScqdSpOxN52vvtgjxZr8U060OJOtCREGTAiCZ6XFp+mL3LW/1ZichLm2HG8RFLFA1r3D703V+jKG/9ONVQnW/lXHySNtBeSFdML/XcbSlxcV6aGtDHJH2KUWgliSukY11Yo7S60CJGPyWKk79GLkfOsfg8lyKhBBTkuMuRqyTsH9mGzYHsiN/heuh+ATtTsgfAs3nXmVrg1HBJiMjRy6VMgcimqboHaSOFU9z5+0o1UAa+Pu/52KRSgXf6pISu+qO37n79sRVSpykpgmmQCsAAJJieI+k3iJid8Q2f4ly7GeLZfGuDW/lMlyeSkjxKUbtkJrETMw7a0XYLEHGm6jqdgEB4OPeQr/0/wDjHi3jUmGq/F8DBRqS1FESy32M/a31lISWOTqm+xDbkYgfPpfqlfidjHyZK5bgh5BiA0dS0ZOhVip6v119/Y9B8lOqlyNhtMLMx5MtScWvwckp5abNzTR4is4rys8SuYTEsKl2USJFNIq9h2+5h+WJCCspF5it1psFAbO5ttcRPaDHPS+GSa+mpfLcKgsObBN+3He07DDKYS6vNDjXfJ2K/KlimtWZ553kr5SdUZHAVmHw7KpJEhQ9WJHyT616lieTPtx//SAtcqnuCxloJCUkgqwR15wELsz1iqKV+C3ufcQW0vof0eNQYetiuX57nsuSS7YkpwyRSJO8MPtwy1TMQo6uwkl6hkBRk+4nYHqabL5e/lKHC5OQYevPko2glyklYTRw9H7zNHN8O8hbuu2UbVm2SpJMwjK2QtTbqiQr7Xk3N/bpg+kDjZ89MEpuBuZtBt0kYkeSeSjzXF5jx5ncjPM8fH4TjKzSH+1JXsS2CwkZlEZVFKfPdiGVQSOoC/UaeRtV7FXI5NMYTB3iH3a+QSSdHqQQj9gd9dn+SR6s+clsY+/U4k2StCz7iVVryVlM0UJjll9xZew7oqMo2PglAfu2T6tfILfGuQYPC8iq048dE8P9Hs4eIsqpPIUSKwwBIZ2iQOR+S6SH/dr0xXRpYBUlUpIETuMetJVWN+SbKRqnpa8/OI94HOPvxTz/AI8fGsuB8hp/cpw2ZsVcusthnJkI9mDv8pIZF6iEMoYKWU/LdAB9RfKcXyPG4CzhcPHh8hfeQ3ada3PKA6xooCh2P2uJCfgAbLADXolZSsf9H0cThp5fcmmkr/J7kt2aRlOhtgAHOwN/aT60uI+E15cb3OMpj7KTcOy2KivI50pqWbMw7v8AjSEQyBdAEh1PzsaAyihaZqnXxbUSY4HFh3Nziw8SJZy7LWqRt0lakNqjiFCTtY7flvg6Ttm8/wDRFV53R5NDQThWLo4psaajOs8VyauJXbb9FJaZip6FgI1IP3bAg8637U/GanI8Bdlhfi9WG/UkDCQGxEq6Uk/uUjupHz8E/wCfTBecMXjPBv0wYbE+01duY8cwVK7SDBoobq1wJJHU67faHIUkjYLDRjQBZcVgX5DFDx3EQWM3xOKKKLO3BMVSraPd4oSy6IaRo/2b3oPv/HpVmwmpbeT/AO0STaxEAn5xaDvth/4K8teX1LLsS7CE3vqIIAI3jUdwLXONX6f48XDy/JYzx7woY1qNirkrktq01mxHOJx7lNGKrGirEzKQAWOyGYt16n/jXFqfN+S8gzkudytrGY2MIXQqs0/aMpGunB6l0hd3C9m6KEB193oeeIaGJ4Ra5X/UJZGPvG6GqxuZE7JJK6qF2equwYs29BRvQ/E/4/4r1zL0r0U12lTifJ3kglD2Zq1Z0WKCHqCsjzu0UZj31ZZpV7abRJGaMaFvKBKRtzNha/Mj7gYms98MOMP+UuwZbCl9JKyAm1pMgd4mMFTyRxalxepQ4940wNCliDXfL5KGSTVq4iyxgM8kshJDqZQgAGgr/Ym9eoKbhvIJLi+XKfFor9viAqZKx7RLJCxh7RloV37hiZfvb/KFyNFh6qvHsvmuZX3yfPMxZwteS5VrXamWh9+eaKR2ZKzpIgHSPbO3tkEGPY+fTlUMbj8JTNKjNHDC4UyCFApkIGgzHfyT8/P/AD6woat1DinXwFLIAtOkdt725GJ6pyYpUA256SBJE7wDAtEpJE/5wIeT8hoeFPEHDFqYFMzylKB/UCzIFeaaR1t2nYt+VjlUdXYbVimjskNSfDvA8tWkn57zbjhbJZFZRFFZPcwRyNIXaQMAA7+510daUvsfcR6muRcQznK/NF7kWeR2weKjhhxydw8VgBUbqVUE6DqzMDrZOhsE6s10278c0UlyxFEqsexR17OdEdVI2FGgPj/9aB9MnICAhF1Kuo9OgHtzjfTLcbUomQkekCYkck9Z6HEriWbkedjpZV1lSaQgxBmb21A+ASD12Sv4A/8Asn59avlPzFjvFEM2EeKVKVGONpmjhDyS+435XZ11AOiSRoj+dj1QrnkD/QUll/1s896SAQwKkXURBtNI+2+fc6B+vwfwB/JHoI84+oLA57KVKmO49kb0Tx1lEWStKySOrHsqKRrqWUL2BAIZvhST6ybQYCBjxRAOo4+eVfUByPnfIbFGpnDx/AojGGuVjUiKMqSe/UlXZS7bUH+B87J9Z6j7/gfkn6c5S7axssWRqxS9YoxtZXAZlAZB0UfgddELoDWtes9Gt06IwMt5QOHC4tzGXAXpsPmiv6RtNDMVJ/t62C3/AAPkb/gj/kelJ+vHnNCfyCuBjoqYsHXrRyMi6MhZRKkjMvyVX3+oH8feR8v6z1npe0gfExhuw4UNLUNwCcDnx19P+Nn5Pi+X/ULfrQcfnsVprWBoS+5lHqSV1mgZupCwwFZVLsrGT5KgK4JSseSOHYvBc+5RV8a1LknCMdl56sYtI7wJXMkixR+6xJc+0R1dz8joD8hSc9Z6NYdU648hWyCAPv8AfGb7Apfg30KOp0Eqk+374uH0+WP9f82Tx9lJ8MKdTC3Xit5BCriMRnc3b227PHG8rdwRsRq2yw03l5jsTcj4lybKYzDSQ0IIoqUMskxY/pocpIYSQWY+51kjQjSjSk/z92es9OUPuOJAUdh+mJTMW0Je1AbkfK/GClw3jWCp4ulZ5nWr5DKy1K1iS5FY7+67x9vtCnQbbsD20oC7J2QDBZbx3IvMxeqRzNRtt7s8lYdO0Bj6QiP5+09g357b7bAH856z0qfrKhmveSFkiNiZFwP+MPHKGmdyGkcDaUmFGyQCSlRiTEnqZO+ClyXA5bL5Cd8XgMDjGs166+1XeRIyneRkkkfYMbqjKrHZC+2T+CR69eM00tz18upOIXCTJfoNk4zGl/FCFZk9p2UmSV5Pnptgfcft+STnrPRLzaFaVKEmJ+2ElC6vU4kGOLfLFL5XwrK1/NlLPVTLPgRYNSeN4VeIRbnMakdep94QyE/zp2X1A5LEGqsCRTf2K9tT7skQ24BeJpFJHbQKkHR1tRvZX4z1noJ5I8oR/IGHNKY81HGn6Sf8D6YlOB+M8/z3m/GfHVNaNebkEVjK4ieaTStDEtxWZgNnqXimT8bBOyNfPpoPqT8YXPF3jKQUrou3OcZLAYe5Xih9uJWp4ueIHtvbFpEWQfA/aN71856z14lIQ0pad/8AjG7MXV1Bpg5eGwPkCoD7Y2fNHgKj9UHLrPHZudXcVhsbj4p5oY4zMta0rmOsVjLDTGON1c7IIVwAOwIDHkng/BfpK8bYrxvHzVcoLGXnzWUsWYitqxZaD2qoWFXcgDUrADfZhv8AO/Wes9S9K6uspy04bLUqY3uuPyw7y1fwdc28gToAIB2kI1XiDv3xRvH1blv+mOR5mjxrMz2eX1TRilFYmqtOdDIS2tswVEgAZCPvc72oIJap0uZ+OaOHweBwNTI5LIVhk8jYWTvJFMJUZVCKdtGgijGhvbMeg+SDnrPRobQlPlgWGwx6rM38yDq6mCVKUo730pGkG+wkmOpxO5/xzjeTV8Jb5gqUrlB4Lox+IcIk06o6sZ5Cvd9iR/jYCbbqfxqy53yJWpsHu36MFWFQ6xJGGZlA1oliF/P+Cd/AHz8+s9Z6zaaQhMJEDt9cJwooToTtMx3O/wCQ+gxQM/5flyGZTF8Oyc3fZHWOktiJuutjWwQPkf7gNf8A16+YPL3MpsouIarRsTE6MJrLAyAa23YzNvWztQPj8b/G89Z6PbbTE40KWqd8Dry7yLLTx5KTNWlXpGI1auolf2GYq6qioQCft/drTfk6XfoH8b43e59Xjo07Zq4qtYZoXaALdlm7KxRARvpvWypKgnehv5z1npiwgBOrC+oWoqCZwyEN8w1a1GwsjJUjWPp7h+wKNaGxv4/HrPWes9bgkY8QZF8f/9k=', 1);
INSERT INTO `photodiagnosis` (`id`, `photo1`, `photo2`, `photo3`, `idDiagnosis`) VALUES
(2, 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAMCAgICAgMCAgIDAwMDBAYEBAQEBAgGBgUGCQgKCgkICQkKDA8MCgsOCwkJDRENDg8QEBEQCgwSExIQEw8QEBD/2wBDAQMDAwQDBAgEBAgQCwkLEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBD/wAARCADIAMgDASIAAhEBAxEB/8QAHQAAAgIDAQEBAAAAAAAAAAAABQYABAIDBwEICf/EAEEQAAIBAwQABQIDBgQDBwUBAAECAwQFEQAGEiEHEyIxQRRRMmFxFSNCgZGhCBZS0SRisRczcoKSweElNENT8IP/xAAbAQABBQEBAAAAAAAAAAAAAAAFAQIDBAYAB//EADkRAAEDAgQDBgMHBAIDAAAAAAEAAhEDIQQSMUEFUWETcYGRwfAiobEGFBUyM9HhIzRS8ULCYoKS/9oADAMBAAIRAxEAPwD7LpL9uOspWqqC209TTIwjSURFCzY7HAyZ9zjQ+nl3PuOvivNtmcJaTNFUUkbhIZGIHcil+RK4OMEDs5z1jCqvFxs+1ITTRN58da03nhiOGMAYGPVnlgg/fRXam561Uiof8vQAVkcjzFTwKPksSQw7JLH59+teVt4uHV20qld4kAmOZ0Asd+c231jYnBxTL2sBv9Od/wBv32PcdwpBUV9JSUtdSUrDzTHGyPjGWwpY+2sK3cd1NL9ba7QjxhxEFlzykcjPXYAGAezpj2rQ7jmo6uGFaVIql15GcsHUFR6gAvfWt0HhncLafKo7sKyAoQoqWKmFjj1AKCGOBjv/AH1c7TilakKmHLyCDOljNom5Hn13UBGFpvLakSPn37JOp90XuqpDXJaIQQ5jenOeaHAIbl7Ed6xtm5NyVtVBRTWinp1qyTDUsrMmMZAKg5B/npoqfCa9zExRX5IYivF5EJ5SY9srj0/bIbVip8NL7VsksdfT0rw8eEcTvwBH8XIr75HYxqq0cfs5wfaLfDfn3eG/5bJ5dw/QRfvty9+aVxeN11tTNS2qx08j0acpyT0//g7HfXsdb2vd3rLU90t1tiURcFMUilmkZj7DscR+Z04WPw8ve3zNPS3WCqqKwf8AENUBgFPt6MDsdfYawTwprKTIpL00sbEuY5iwWNyMchx98AnAPQ1cZS43lBOeTMiW2/xj1+e6iNXAzFrRGt+c+ip7Wje/UPm1lF9LUqcSRcuWB8EH5B/30bG20H8P9tWNt7Kr7HXrM9fHNCIjGxwwaTvIyPYY7x+p01/Tpg4UZ1quGjEvw7RiQQ8WMxfrayFYp9JtQ9kZHvmkG60luslI1ZcquGmhX+ORgoz9hn5/LXOrtvK93SU0mx7DNVDsGpeFioPx1/vrul6tCV1GwWCmeZFPAzpkD7jI7XP3GuO7q3NDZa6ioXoj5xflFMGDOhX29RGWGff2P6+2gX2ixeIwcDtMjDyFz43A8vFEuFUqWImG5nddPLfzS5ad07xttyih3dSrSxzs3U1MUwoUHr+/9NUJd479uN7qZNrUEtZb1fjGTS+nGcA56wP10Vvm47luOgo56uFamFSZHYyqxRgfjCAqffrvI1iu7q2z7eVLeZYIKRf3WKtOLHOccTGSTk5xnWTbxV7n9kcQ/sxcG+Y2FtYhHXYMNbnFJuc2jb/aLWXxEhSRaTd1tNqqGbiHlBVD+Zz7adopbfPGk0DxSxyYKujhgR9wRrnVs3bR3mqqKIWpw0fFpZQcSSFvxeodquft3ro1rtxpKaGmNMsfH+GJeKL89Dv+uffW24DjKmMaZfnaN4g+Ox8lnOI0W0HWblPKZCk7U8Kc+AP5A65vf903qndqqloURUB90HNXzjge8HBwM/z11262s1VLwUmM5BDj4I71zS47FhlvJuFfcXR/N8zyfKZoC+c8uPL3z374z3jVjibcY8tbhgY3IIHuFDhX0Wyap+SBUG79yVS1BrY4IpIIw8aAErP3ggH4IOPjHY1Qrtz7sahkvCUVGKRJTCYy55de7ZxjA7+fjTXL4YTzyFZLnIEmJLSxrhiGHYBDdA4HuD7DW1vCrzLSbF9eht4ziPyz5h/85P8A7aHsw3FCMrs1gb5hc7T0G/0KsurYQXEeR03XAK65bojFbuGnFI9Xe5zQ0RbJ8qNX8sMB85ck/mANNu0pr3YrMtDKkUa0MZBYDkXYZ+/wffP56Z7f/h7o6KtSs+qmBpP/ALQPJLIkI7/CjOVUdn8IHRI9icmp/BtKyoa4yXVRJIytKghYRMRgA8A/uMD5+BqWvS4mT8DXd2Ycu+eviUym/CD8xHkUBh3LuuOB5p4YImMTOixjkq4985Hv8j46OmCGq3DW0rVlHW4iEvkqkwXmzYH2TH/xr28eGUcFllgN3doYg86jySJA4U+zg+x+2PnQO71dVRbXkpEkgRzVmV14HmR6ccfsffvHwdCMRUxmBDnYovgNn82pHUae7WV2mKGIgUQ2ZjRGrbNuu5GqFPUiNqOQxuZEwrke/E8OwCCNTVSy3rcVIaeOpkhjR4287zkxyzkgqRj3J9//AG1NS4LEUcRSDqj6gdvJI+QPgmV6b6b4Y1sdAD6Jt3PZhb6aJvNo6oOV50z0iqmMY5McnB/PVuC3XBkEjW2NaZ8ZXkZcKAMYY9gAatX+tkt9yE1PIIH8hBIzjIcZOFUe+f5apPRV1dCq0VPHRyk8lmlYQhPzyCT/AG0MxjKbcXVawE9B06mTfcWHep6LnOpNLo7/AHHncp9slHB9E0sNY87uRyJ/hIAGPv1j570Qhd48RTOC/wAde40G2dAY6BozK7SLKRMx9nfrLD8jplWEY9WTrfcNPaYVjgIss/ifhquErwYwD8azUgH31PSvwcDXvEk9e2rwCrFZDW3A1owBjWwE4+DqQJpCywAM69JGM6x5D2J1Cwx31pybC8bLqR99fPvjBYrjTTxV8EDlqKUtlVz6Djv+w19Agg/xar11sobioWqiVyPY+x/roDx/g/4xh+ya6HDQonwzH/h9bORIXzxbLtbq22SoecLzpxV0Qllkx3jHetF3qKO07eC1NQZ5IlwHaMhmbPQIPf8AX412Dc+yKaOxTw2G3xc0VpFjVey2P4QOsn++hG1tmz/5Sdd50UReVuaRcAGQfZh7e/5dawp+zuPbiBhSL5Sc0HL3LR/iuGdSNbadJErm/hZt6tuU0la9O4NbIBkggBAT3/f+2u6rQyxrxIXC/fOTrfbLVQ0EZagiCchgsSSSP1OrDvF0rHLZ1teA8IPCcP2bnS46rP8AEsf99q5wICHu9Rx8qQZX/wAJ1VmiEowIs49jg50YlL8OLRLj76Hyso65K36DsaOgIaXLUEyxwrE9DHH/AKa1+VMWwC4CnIyB2NehnDEee36YGo0wQn9+/wDIA41IGToonPherG/FmeNmc+2fYD7e+sysjAFIiQo79Ixn+utKzdqPMlIzn8P/AMazC8u/Pmx9icf+2mkQnAzdZTQPOhWWmVkZSCCBg/30h7qtdXHIEssM5qYgodlHmMkfwcEkn++ntQkhIzMTjI0m7tqEpK7lI1VT8lVYyjAeafsT8D8/7aAfaAMbhC53PXcdx297wiPDy41YCBvba4SKlVW0yrUHkadSsZc/cKcHP6f01Ne3OinraZYqm4tSoSDIV/ff0/D/ANNTWRqOh3w0wR1e1p8nXRlgkXcQejSfpZNG5YqGoqIVusLz0xiHBYm4ujZ9yfgaF1MlGlIghr5JyM+XDhXY/kcDl/POi+47ZJdKmOmtjca1oVLBxlGQE4BHz3nWxod52mlNV9DQYiUl/KIBCj7AjH8s6lx2HfUxdZxacv8AkG5vCxGnI6JtCoG0mCb8iY/dMOynlezq0oCjmeMee4x/pP550xA/fGdL2z6ilqba80Afm8paYt/rIGcf20weo+w1uOEwcFTgzYIHi/1nTzWTY+e/jUX8XXvrx+vb768RsnPxogNVW2WToGXAJGPtrntwn8Rv2td62g25UySRxutmFTcYoqJR5Q7kSOQs7tJn8SkKoGCpLZ6FgdnHR0H3Ltaj3NFSxVlTUQrSzCYeSVHMj2DBgQRnB/lj2JBd1XApOutr8T71seS3SXhKC9zVNQhmpJ0Aip25iMswCkFFZH9HZZAMgMdJturfFzblnWyT3aZKpWgooJKv/iTPUzS4Z0llJMkaLllXkSFwHblka6HXeEFhudPBBV3e8FoUmUypOiNIZS5ctxQA55nrAGABjHWtEngxbvO82Leu7IVD+YkUdeqxo2HB4rwwMhz/ADAPv3pQlkJZudl/xAVKvSm52mVJFaEVMNS0EkccgUMwUKFZ1DvxJx3CuOPmHjouOxfF6CpguVDW2WWpomBo0lq3ihDBZXHpRF9TSyRq7DHNIyCBzIF+4bU25aqysslXvLfcs1NRrUPwqTIoQugGOMfFmY5BBB6dyfYFfLlbduVVHQz1F234Bbag00aqSZHkwXaUngTgqxTmCBg8RjOuSo7R0/i0m6KAVl1oDYYGdqrnLH9RMCjYBVYsDDsuOJGFj7LlsgBc9u+LVzv73yruFrhoyVWARVAm+miBkJMStEFV3VkVnJYgKeOA2B5uK1bQmuddXsd3xzXqnjllNHSKeUfHPlKxjLL0SSufc498DQKsoqN6aOy0ly8UJBGqpLUPIqyxYPp7MfqLEjJ9gFySOuSETquThX7e3BX3K8S1Vyl+lxEbRFT3WakjjZVDN5wiKuxaRe8lgUwABluStctteK1feYKy47mtNXRwys1NHM8aNTSOqcWVfJMckiPHlGYEqJn6cqpPl1sNpuBElFU7xkra+CB4UaoNMkWf3Qkd1T0kheTA8scRlQcA3bn4aW663Ge4x3i60UtVhalqSdY+YByuSF5DiC4BBziRgc4XDQEixW0+OcCqf85WuqkRlzGxjhWXDDOf3DFcrGOh3mocAjy1ZgNNsbxlsYo6eybvtNWZGeWdauQRyySt9OhdpFgzO4ijqWDOD6pEyGEYyxy+GttmYzVV+vj8kSKRhWcS4Vi4JKgHPqIyMEgnRjZex7XYbtJdkrbjXVLRvEklZN5phjYpyRTgEA+WnWf4Rp0JpV/adp3PTbatlPu+4RVl6ipkSvngI4SzAetlwiDGfb0jRQ0RBySTg+5OiTywgsCQOhhV1r82IkAtgkf00rSVE5sqgIiMnm3v99eN5SkB5mT8yT3+uiBVGUkEEg5x99RqePngrxDD5GRpCUrRCpoKfPdRx/MMe9LO5XoYqxjUxx1ieWvKOXtUHeCB8505ilDAx+nA9sDB0q7gtaXO4C3LO1LKiq4qA2CezgD9NBOONc/DZWfmkR3+NvOyIYEgVJOnv3ZKtxp56ykRrZRVMjLjy4/XEqD80bAI/UHU0Tv1Bd7bTCZNxQqEGG8xsNIfsCPY/wAjqaxONp0aFSMRna6Ni0D1+Z+SO4dz3smmAR1n+EZ3NJVSrFSWwLDWLGsnnFgp4ZPQJ6z186pLTUs1MguO4qoqMZWsSXhn/mLZT+ftrdu+rjohBPVwtUQpEOMaNxZWz+Jjg4XGqSxrW0Mf1U2aaUZK08R5YPwGYkfzI1LxCoPv1ZpGY8iSBHcCPOT3BNoNIoMOg99/onva1RTzUTRwRIoik4Fk7D9A8gfnR0OCeiOtLOyovLtbRQtxgSUiJD2yLgdH89MhwOutbrhTy/B03HkgWKAFZwCkr9HCk941zk7/ALjdZb9WWq7Wu2UG27gaOdaumeWWcQqj1BADpw9JcJ02eIbtTjXR2Kn3x76qLabUK97ittpRVTL5cs/kr5jrj2LYyR+R0QJ3VcLhe5f8RyrXxWaWCrsQWo8xpqcx1MtVBwg4RxdMvmGarhjbiJAOMuGyMqwVf+IOCkUwNZadqihpZ6m7otxV/okiplnZgVU+YgEka8vT6pFAB9WOrSWe1vS/RC30yQiPyVURLhU+wGMY6HWMdaUrbsi37CtVVeDNeNzVlJRTKgqEgaolU8WeNVijjV3kaNcswLEgZONOSggqjsrxWrNy3uhsEdpWpVqSZqu4QzArHPEIuQ4KCAjNKVUlgSY2wpUctB/F3xypNm3KPbsVUtIVbncawSJ51PAsMk7mKJg3NuMQX1AANKmOXYFePxjuj2PdFzptuUdLNby8dPJDIZoy8acZZpm4riNJA0Y/ikMT8QcDV+w+LcNPDT2zc9puFVX5gp6qrWhjiQSzJyjjdC+fNK4LIgbjyGcL3pQugK7Tbquj7Kn3tadu22jq5Zg1ZM88czfSR5zPIyceUgjGfLJBUnHwRpftfjNuu2W6g/zNZYzdqimpa6to5nWlNJBM7gMoXzDIUVHd8lQqoScHrV5PGjw+3NGqybWr6z9mzCV4mhgkSkiAfFU5EhRYsI5DZyRggYZSWXbPivtrdNJX3QW2vtlJa4FlrKi5wrAsIK8yjZbKkJxYg4wGXPvrlyVp/HHc9EaCOs2BD9RX01JVeTFdctAlQw4iTlEuCESoc4yAID32NBW/xU2Wpt1JVpa6YpJTGetiW4KZYAYPOUIpUByVaMHJQAyd/hbBm8+NVPV36SzW2iuNLBEp4NFSxyVtxmEYl8qliYkDjGQXMigjmiAcm6Yt9+IkG27ZQQQUUcF2ukayCCpjEhpIsqJJJEjOX4llQIhJd2RQe8jtVySrz4yPR3WGhrBaFeakEhSkuizxs8lSIIOUjRrhc82ZhnjxIwxxkBs/xbvV2v8ARbeu23oopaqqeneaSrIHIRPMeEZiTICGDHzxmQk5DDXUdrbxhrNuXm6w2u+XIWiaeJZJo6YyVrp2UhWDCkK37sZAOVIJJBOkGXxou8t1jrV2vSLaP2pDai7RSn6qRs+Y1NMQFl4sAgCqeTByeCLzLdLQuiUI3Z403Hbt1vxpbXDcoLPJLHLCK4RtEkVMJmmb92SObyRQgHPqxjPeMafxyuVNcK6kFttTTG4NbrfCbj5QLJAHkM0jKQg5EhRjkQMgHTRfvGShglhpoNv11nqDIn1LXGhikSJCrSMrcahODLEolfJJVCuVywGq43Ta/EEXfbUltu9LS0ixLUVqcIQXZRIUUBi4IUqWVl6DhWByV1NRpGqYCgr1RREuQhPHm83W8JbNr7To69KqeGlp5qi4SxCSR2nyDwgcDhHTSSN30OIPqPEUIfHi6bxlg29Yo6a1y3CqttOlcKgzFBMZZZo8eWFEgp4G49n1yqCMqw1nb7v/AJQ3bX269181po71LHLa7giqY6tlhSPynJUiOUBAVXAQgjCg5ywG6b/tNQGt96t9+SQlxDXQCjlQY9g6ZU/zQf7GfwSsAJc0SJGsHxiLaGSINkOPEqQOh+X0mfkuk0wNLSx0sbykRKFDSSF2I+5Zskn8yc6tCpkP4mY4/wD77a5kPGZKedbfuW3GxVZPELUj9yx/5ZVyjf1B69tdF25cmuEBnMasvXFsdH++qWL4biMI3NVEDbke46HwVjD42liHZaa2PUShsjkM/wDMf9tK27vqax0pDMYIwAwdiw8w/KZAyBp9d+8+WvX5aU9zz1SVOaSlSpkEY9BTlwGT6gPk6yfHv7Mg6GPfPy+iN4Ef1gQlyamstBSxPPT1MMa4y0Lqyx/r7Ej9Bqa3VdTDPTRxVpWUk4EcpERY/YgAZ/TU1i31XAjsWsiBqHf9bI0xgI/qEz0j1RrcdSaWqgqKeb6eoWn7kYZUrk+kDB7zoYbPcLnGDS0FNT1BwyTSskTAn+LKsWH39s6O7jgpqtaamuw8u3MmWlX8Qf7Z/wD7+egk8W1hTsKHctUy04PFEAlyfhT0W/6fy1NxOiDjKvaEZeWYNPWx+oTcM8ik3Lr3E/T6Ju2RHJT0E9PNKZqhJj50xJIkbA7H3+NMpXlgnI0ubJlrZrUDVwhF5jyusZXHvplAx8Z6+dbTg4H3KnGkb+/nvqguMJ7Z081jJ6QxLcR9/tqvBX0FUDJTVkMq5K8kcMMj3HXzrZXU0ddTSUsvaSgqwAByp9x399JNP4P7BtjtUpRVUaoCx5V83FR5jS5ILYyGYkMex0M4A0TiRCrBNtwr0hWBI6lY2lkC9EZx+h1lKtOSlHPeeL1AZUQsqs+B3x+eh9tcNq08HYKgVFJtK5SedXERVVOzvDVmSRFUo/MI4JjUoFJC8euLMFJ2w23YS0bXG37C3Qk1C0lTClY1TzkYTK//AOWQFjyIficg4bGfmqKYdUdnJ23IUsw0QnNvDbY1PYaXZqW+KO1QSx1EdJGOKGSJw6sxBBY8gGJYnJGTnSRcPCC1boW4Wm/1tlWzUdfNUKlGSXiSUN5wcHHlzOjsrSFnIVjwCZBAexybNko6a8VGwL9X3GOpNTVkU8hEtX2WYxciqnljrAAbAOCpwVt1XYp5NwVVP4bboimqAZKhAksq1MYbmQol9AZmJyiDlk59+xGIDjlnX/y96pTMX9E7U3h94fqs7UtCjU9xMNRJGsrGBzGqqhCcuA9KIDgAMFAORq6dmbGNuqrR+zqb6S5zGaqQAYnkMhkJc+59RJwT849utItvs21a+aaiPhfuDMVJJPHNVmXhL6cYEjtyErcz795GT2vWratk2Pero9APD2uop6bHnxvPKY4ZeXIq654+4HE4OQB7DGXAHcH5rvH6JroPDnwxstT+2bZaaKh+mil5zxN5cbRyOZH8zB4uCxLerOCcjGrd82f4aX+4QXLc1Fa6ysYRCB6ngzkRsxjC5+xdjgffSLUbfsKUktpp/Da/yUdRUPO/K5ValpIJPLjGQxYKR6lBwnHB+Oid1jswtol/7NL6Wgkkt9JHTiZJDEcc5eSepBlnwxOT3j8R06Oh+a49/wBE40W1djUdgNhs9JBS2uL0GCicQplRxIJjIyRxAOT8YPtqjLtHYVLTWy2LSUsVNaf3lDFz4rAAhTCjPtxYrj2wdKlNtfb8ssUKeG18jirDJSsz1lUvkxqMM7DlhSSCoK5LAZzg6H023NoChnqR4V7hjitCmCmiP1RkkjZgpCozZb2U/IChe8rhecwHQH5rgeqbLnsDwpmggpLrt2zyU0zvHDHLTx8C7sruAuMEsYlJ+/Dvoayodp+HNm86x7bp7VaZ5eU0tNSCOJpMk5Z0TBbsns/fXP63/ILR2+2V3g7utIbezinhamqBFE0jMzcQH4uT30vLpsAd41avdt2xeb9Qeb4W7mNfc6cmWvjhl40kZYDgzcvLVjjkUP2JPqIBfShjx/KbUaHtg6LZ4lbCqtw7ar7JFe1ppK9RDEXKTRlgeRxG2PhTjGCMZ+M6q2e1Q7epLTtuqvsMlQadEhWeoBlmwFQMAT6s5Xv8wPnQ6r2vtSiljtlJ4R32tSirTTx1hkrIuKk8WlGQvpJJ5Y6YAtlzjLnszb9m3W8lfdfDm42T6OGGliFXVTDkYmKjCEgEp5cbLLgkgxkNlcKao8Xr02GgIy66b6eiFVOE0XnPJlDrpT2qCkYVNgqNwRpIYaunt0cdW8Wf9cfLoHB9gT+Wl+21Vz23fbfH4Y0O4Gt1TUJFWWK52yqiip42PqlimkQCPj7lS3E947wNdosG2rLt6jhpLHFNHSxRLFDGaqWVEQewUOxAH6aJMMdhdWaXFy1hZUZmnafhPeL6bEEEc0o4c2mQaZjrv5/7CqcnVQWBz86Vd01BhmE0nKJAoxIpxyOfwk/b/fTfI3wRpa3JNSQOJriiyU3HBT5z99Y7jbc+EN467Dv6dyN4MxVFpS5WUn1tMBNc4YBIMMRl+vy6HeprG5T2eSiJorPUKT+Dy8xlT8E5x1qawGNNBrxmY2pbUF/pAR+h2hbZxb/8ph3TTyVUdPTIHm8yMgQgE99eoj5GqYmqKGmjZtv0lT5IwUVzGMAd4Xvv3/pq9upqlY6Y0cv0swiPGozjHt6f56B08dv+lzcairXzSPMlnDyrn88ZUD9MfGrnEqgp8QqFpg8zEaD/ACB/ZQYZuag2bjlefknXZNalfRT1KehXlyICe4uvbTMcYBJGlnZiU4pZxTsJYvMBSoX8Mox8fp7aPzunFk84IxGBk62vBi44GmXG8ep92tysguMA7Z0Ifc7/AGCkvFFZKu+UtNcq5ZJKSjadVmqFjAMhRD2wUEZIHWRn31S2tv8A2PvepuNPtDd9ovUtmqTR3BKCsSdqWdfeOUKTwYd9HB6Ovhjx82NePDzxYtu/fBzdfiBunxFjnp7ffJaa21t0p4xMIi8auQ1JTqSxl+kkljRUqWIaMCAp2Hw+tHj7sex703LTeD21KbctfbqeO0x0m41maWOnylNRvG8USjy0klcyPOxd2I5InBYy2W0qqDsu97gvezTu+17drai3SbkMElbSU7KrVKUyuqSSL1lE5Mik9AnA70dqWpvMd2aFeAyxkZh8fl7a+OJIf8SlurNv+Iu5NnXq6brpduGgqpUks/mRtLcVaaLilQsbOIkideGFZYiCVc4PW/AjxF8VN27evNs8U9hV1sudDPNDFUXKKClpqyEO0aKqpPMzOQnNmwEIkXiSND6tHO517W6qdroAW/cHiTsPdlRMNjeNm0o2pl/fRruKNI42hY8lPlEthpDGj5OQowME500rtnxkW2LQ0+6bZNEYBD3UtG2OUXqEiwFwSnnAHJIPA5YkkfIeyLduq4eMNw2j42eBtddKzc1tuMa0kVwtgswphPFCKkBZw6rFBOkatgzIjPwVmkkJ7PbtweP+xL3Ztn7A8J6Sl2VarjJbHhatonV7eEk8mrSoatM+S6xmXzIS+JGIDsMtZw9MU2EA7lRvcXFPrXnxYg3DHtdt22CW+ijavloCzpC0X7xGMb/TliEd6frJPeWI5KGMbe2v4xUlsuEtzv1lkr2pZWoo2lklikqnVSGmby1YKr8wAuQUKjCle+NTN/iXum667drbctFPdzt76K1NV11JAlvkeoDVUZeKSfzMiOJ4SY8ZUiTHsfL3fPH/AGzcdr7gvXiAIqeqlsFkqoVqqWXzqpq5Eq3eEQBEWaJmPJGynEYI/DpZD35XGyW4Ehdut23PE+hqJ6u6bit1XH9DKI4GwFWr4x+WSyQqxTkJsnIOCvROdJm9t2b68PbMt+3V4jbX2/QkU8LT3i5iOH6hnfkgkNOg7UjieOfQMjHLNCx37/ExT70hn3XBtWs2vXuIaqKC7IkluIVszQgQ8po2KJhHYODK3ZAGEv8AxRV++TFU3S30Dy2KxWyaupqqNLVUUxqmgnibzhVypNGyc4ipiDKyNKrBiwC1qrM1cU2ABSNdDC4rpO09y7z3veq237U8WrRe1tYhnqDSASQorioCRtPHB5bOeEDMgbmoLEhQ0ZLdeqLxMpNv0lNBelmr57gY6iop4wzRU7yEKy5j4+hCC2R/CcE/Pzt4G7A8bfCvwRt21Np/sK1XG5yVNwro5btTx1VDO8cQUKwiqIJAXSQsO8B0APRIdNmVX+Je5y2o733XQWqIWymrK+MT0EhNYFmWWjPCNgVYmGQupHHhxVn5Ei6zSJ0UJ5rqdtsPik9RTyXDdtvnpkjcs1PxEjuUQKORhI48vMPQB6T3GRpxpiYqUhpWyrHLdZP5/bXzHbqH/Ek9Btqgt+5NsbStNPtk0tXRU9VRo9LcjDKmSkcUkYjVzA8flPgcHVgQwIcLBNvfZ3h9eqm9+JtLuncMlOZKGiqaqkihpnEefJWULH5p5kgSSYyAmQDyZoqoBcL805swVtvf+LjwSoLvXbWi3LX3C+U8pp6W2U1rqHnucyyNGyUQ4BarjIjq7RllQq3MqASO109WskSuVKFlB4t7j8jr8zPB/ZVv3H/iCtXiHt7e+z0S0cbheLj+0qCMy17w1cUqQ0tEzQBz58bzFXOWcuTgouu6Utl8X67bVmsW4vG3ZdJV019NwutfTb5rCKyF5ebcI44oZIwo6Sn8/wAkAkNyAA0rGta8wdh6rpJFwvrmO6UNTU1FHBVRtNS8RNGGGU5DK5Hxkd63E8h02f018VTbO8Ra61XKyXL/ABMbSt0Qgp6W3zpu+av8wR1c7l6mOpUh+cMqBkLNz8sIzFcNr6S2/wCJPhjY7XSWqTxW2vU/SwJAG/adKmeK4zxVgB7ewGBqZNT7Ir/B0r7ipGqalUjYeYU+egAD7j89MNFcqC60UNytlbDV0tSokhnhkDxyKfYqw6I/MaXd2xNOAjuURlwCvvyz/wBNBuOR9zcSJ09zt3q1g/1RdBa2e4UVKZIrlCZAPeZRg/l1js6mtcoho6NTPQOsajBEDZ/ng41NYbEVqoIy1sltJPqjlJjSDLJ8Aj+6q36VKao+n+tEcbH6X2z7er+WhtKK25W4y4iVJslY6aNTxz8BiTk/GcaO3ThLLRxwIwrGUmBwcYHWc/21rqdt3irgJq6mibAJLGNcr+YPHIP56IcQwdWvjKrmAuHIc4Gp18LjdV6FZrKLQYB6/sr+xqZaOmqaRecYR1/cP7x9e5/XRuosNirJ2qauzUM0zAAySU6M5x7dkZ0I2e1GtPNTQzPLPEw86Uvz5k5x6vn50wVFXBSQmepnjhjX3d2CqP5nWq4IA3A0w3r9Tbw05crIVjpdXcSk2i8EfB+2bwrN/wBF4cbfj3FXMHmuH0SNLyCBOSkj0EoApK4JA7zpop6fbzMwp4KBjG3Bwip6Wx7HHsex1ofurcNZZ6BZ7VQU9wmPImFqkxsVCFvQFVmduh6VGTk49sHnFNt2O51UF1vmxKGgjNKIIXl3JOJI0XzGiRk6Hsw5L8ciPUB0XkqoAF06+PZaW2tVzNRwwQtzeRuKqgHuSfYAa8qpbMac1zTW4UyqrCVyOOD7HI6OcjXJrtbTaILS9TY7K1RLRLSrQG8MMKsmOMZAJZCxUuW5dlcn0jJT/I236FGt1vtNlZYXVudTVNJLIpkRw0hDcm9QmwD0cAdZbQ+qwOqOJ6evVTtMNEIvW7C8NLzfP2iNv7b/AG8iPV0dw+gp5KmnnyF85GdSeQKp2c9oo+ANC9m27xpoNhC07o3BZK7diXCn824StF5MlGZImqDEkMCAMsZlSPmnqdVZuIbiKFNT01TdqSePZ+3wlAFp4fqLuwkjKv5i8lCkF+g+CSeyc6u3KwUxhf63bm0oKhstAkte0gaRsAtkgHAUOcD3PyOzqbCOhhk7pKgkiEf3JJuy7bHep2fV2Si3NU06xozVfnUkExwHxJ5RLhPUQTH6uIBC5yOT7r2V4/7ntO5LdXVfh3cfIlhj27TXOVpoS0UweOvqQKUFJ14qfLXnGW7yoHEttZR+XL51ms21eJpVXkZCCsrLxccs9rjiB6QSDjr58WOoqKgrPtzZwkLs0pNb6hIB0fwZ/hGc9jPzx7qOxVPtYp3UopOy/ErkMXjTDvITXS8bPm2pHaPLjSGWRaiW4cIvUyGL0pzE/qEp9LIPLBBYg9v7c8ZNybPpafxG3N4eVFweSolaekpnqaOSKWAIkJhkC818ySUFg6lkRPl3UXbdZrVSQTUlVtzZrOFLUzJMrBnP4uZK5+T2M5A7xqzVWmKaqjWaz7FWnrI/NnLlWaWRj6sen1BQq/m3HvHWLNPENc8ndRupkAKpDs/xF254ebe2/wCH25fD7albQyxx1cUNiZ6Axs2ZVp4opIgGDMW/CA56xHkkZXuj8bFu9/rqDxX2SLWbYiWyke2PTyR1nEl2knMkgjXIBDeXJ05ynoy9qiutPTV0S11DsdBGG4PDN68tyZguR8gcj/P7Z1RRrjEvnVFP4d0pkVJEkZCFWQrh2/5h+PHYODg+2WdSxNInLmukdTdrFk37Ckqtq7LpqPf2/YL1d6NGkuFwm8qMKXYuEJVI1wisqBuClgoYgFsaM025NuVMU6ftWjIiYCX96o48gCAc/JBBx9iPvpPSqooZ7rMKjY8C1bxtG6zKWlRWUs8vQyQGJHZGSvY9yr0VXZUrJGqqnw7m5zzwgfT8JO1bkpJ5elnKcm9vVj1EjK1j8bT3rmaFO0Vi8Irbuis35bLbtyHcdwpIqKpusSQrUzU6H0RtIPUVHEdZ/hUfwjDKN3bVQvGb7b+UPJZR56YjKDLBu/TgEE51ycipkpw1RN4bxTU8cNT5a4dRTBuTuGKqVGDAVbGBkk5yp15U1KPStVV0nhXUyVEyebNU1K4lbBVWY8O25KFAPfpxyONJSjOY5D1SuFl2Ggutru8ZlttbBUBccgjAlc/ce4/nq2Y0x2oP8tcssm4ktk1NPbLj4d09LW1TLWSW+oILhcNyHEYd8SKSGIC+YOznvodJuWyVwpTS3CKT60sKfif+94jJK/cY7z7ataKNW3AU4C6WdyPWLMrW5Fao4dBhkYz8fnprdlPTaA3phJNHTHCBgT5p+Py0L4s3Phi2Y00112O3erGFOWoDCVpo2lpV+rqXEjY5ebmIZ+3xqaI3Cgo4oHkqrriPHfI+/wCXvqaxWMwxpPAqhsxu4T6ozRqZhLZ8AUVvpdqSnpKSJWrJQTC5OOOMZ7PX9dBvpZHoeF0vVSuDiVJi7rn88ZXH9tX93v8AT0VLLNGxgUMXKHD5wMAH7e+dB6VErrcsklbxif3MIZmVft2cZ/lpeLVR9/qMIkxpMCIGwie+ekWSYVp7BrtpTrtOqoXhko6WFAYOPJ07Vwc4II6+NE71t+y7johQXy3R1lOsiyiOTOOa+x6+2gGxIqeniqYaHl9IrL5bSf8AeE95z/b8tN64x1rYcDe6pgabnddNNT7nfVB8a0NruAS5L4d7FmlaebbdGXby8nB/gChfn4CKP/KNYp4cbEAdV23SYldJHBB9TKcqTk9kfGmUpGMkkZ0sb4pN3V9DBRbU+lBedDVvLXPSv5CnLIjpG5BbHEkYIBJBzjRaSqoWq+bD2dJaKegNipUgpGUQpEDGIxknC8SMA5OR7H51lNszYnBaafalFIkShUBpQ6qMDAHRxjAH5aW6m2+LSU94qKq4WaKNaRYbJRxzyTRrL5aeqpkeLzHIkDjkrAFCCV5e1C47d8Y55o+O7Y6eKIIx8p4pnkBZWdW/4dACBzVWA7UrleQLEfVLm1XQRtr4qdoGUT19E20OzdnieO3DbtAKQr5qwGABA6k4biegezrfcNh7Nr6xaao29QyKiKwMkKtwCgBQmR6QOI6GuP26weKd6uFBepdwUK3yCHhQpcSYGZEpWVnKeSJF51DlmHFcpHHkHHHTbUbY8WKWto7jW74ilpBNGtdGRDkQLGhYofphlml5g5AHAjBB1GxhFFwm032tZOJGcHojlb4dbRo4WpU25QJTsVJ8uFUB4sGAOB8MAf5akuw9j19GYa/bVDUc5TKxdcs7nJJY+7Ds+5/6aXLtZ9wbnucsdZuOzSbeq6xHgoIqpeVRSxxkuhCx5djKByXmUZMqR2RoPa9t+K19L3Sj3xNZ6WWonNLQTUUa+XCvmrCrBoQ6g5iJHZwpwfV1TfRNCqC0wDopg/O24T1U7K2ZUyOKja1okEp9YajjIbrj319gB+mr1VsPZdVQta6iw0QpuCFFSMRlAH54Urgj1d4HzpLbYvjHyp8b3ogiKgqGyjO+W5SNGfICoR+FQysCACSDnPsmyPFmpqfqqbxDQLFI00KmJAoPGXijhI1LIGeMEZywjzkEnVilQfTJzm/uUx7w4WTzU7K2RFTxJHtO1Rx0+OGaSMce+sdfl7/lqvDtLaRkVm2/a3YDirNSx5VRxAwcewCJ/wCkfYaAXzZPiDdtu01k/wA3OXYt+0KlzxeVGcEojKihRwLqOgfwnJwcq9s2F4mSw1lml3RLSUdMBTqYo1SOoDBi7JwUMgCyKoIIIaL2wcmY089XOSmB0NhdKn2Rs+UoW27a+JdDiKmVe1PJex+Y9vb+WdaoNn7Mpi8y7Xs6EORyFHGpxgDGQPsAP0A0jwbR3a00NmofFKKino4lkko6SGIKigoBhGUlVPFxj45+5xkkrJtDxIpoa9qzfkVVK1GYKRzTdJNg4lZfZj0p9vlvggLM5t/hPP6JoPNN72HbYjlnhsVGjsqqW+nUZAxge3t0P6ayi2ptPhj/AC1asPgsPpI+z1gnr8h/QaTLpsfxAqUZl8Q56SJOBAjjVmxyBdSWXB65YbHWR0ADylm2J4kU09MLh4nz1dJHJDK5FMiyvwMRZSQMFX4SAjHQlY9kKVmptIf4fumOMhP4tFpUgrbaVeLs4IhUYZsZPt7nAyfy1jBY7LTSrLBaaOJ0/AyQICvt7EDr2H9Bq3jAxnsanJgcYzqwmr1xkHSzugyTxCiEghWQHMp9h+X5aY3JPeMHSpuwzegwx+cwBxFjOT9/5aFcYMYN59xPn5XVjCCawQf6SgoaQM9M2V92hII/XvB1NaJDRiiBr/NIPusn7rv7dYOprBVXhuUMyNECxmfkCjjBM5pPd/JTRuWf6eOhnRl81eXESDKEYGcj/poSKStuVMzxWMPNN2vNVRD/AM33H9NGtyRR1NFSUk6AQTHjJNjJjGPj9dCv2RYqCieKPebLx9iZFdlx/DjOT+nvq5xSk9+OqbtgT8TW3gf5dN/DZRYZzRRbz7id+iL7Eo6ihlrKW4Tc6xBH5nFsqF7wAfk++nQdDAGkzw+mrHhnSenIQcSsxUhnPfRz2Mfb89Ooxj9dan7O5fw6nkBi+uup8+/fVC+Iz94dPuyAb7j3Y207mmxZaCO+vAVoZK8kQJIegz4BOB7+x9tfLu/63ebXjc+6bDuXcU9joJbftyhqaS+1BlluLSAVVVTUqssVQ6u6ReXlYw0cg4nDKfrq4UFvutDPbLrQ09ZR1KGOeCojWSOVD7qytkMD9joFt3w/8P8AaNTJV7T2Nt+yzSjjJJb7bDTM4+xKKCRo5YKkFwveX+Ia6RXC3WiwXjblHEdytY5UureZUVEEAb6qq9MsYgCvFMihg3NlGMZAJqHxxuVTtNvFF7nt+h25PRVs9JbJeTXKrELEQyIwcL6wM+VwJXkvqzkDrlVtbbUCmqotu2yGqSWaojljpI1cSynMrhgMhnI9R92+c6ysm39mUtDJBbdtWimiqphU1EUNFEiyT5DeY4A9T8gDyPeQDqo4t7fKbWHr+6lvkkL5h2PuncOwNxTU+8v8qm92/b819v1fPK0twkqp2aRYVc8SESOOTMYUhUSIA4I1VpvE637rtGw9u3zcNvhTcEtRundEK18ksMlOg5LRgTSyEBpJIg0QbjiOQcQMrr63mtNtlkqT+zKRluC8asmFczALx9fXrHEY7z117a1pao1dJUoKYMkfkg+WgIj/ANI6/D0OtI5+Rxyg+UrhcXXx3sLxNlELVWzKHb7bi3DBVbgatuBRqO0UPm+XS02BIhRRHguFbKkueLFsa6ttndXihuzfNfabbe9twWiwxUIrapLZPL9RUyL5k0cTeeAo8soQxDY8wZDY12/9jWozUs5s9G01IvCCRqdMwqR6ghxlR0Pb8tW0jgido46WNA5LEooAY/JP56Y6iyodY8E4PLVxrxJulXdLvQ2Hb19juktOsz3LbtprTBX1TFB5JaZJENPErHk7MQD6VGSwU8k8Od/P4WyeIFTcbvXS1qbjt9hoqS7XR5Vp5Xij5kl5ZfLhWSZz07elQMlu9fWMdktdJJPUW+jhpJZ3MkrwxhPMf/U2Pc/mda47LZ+UjT0VNM9S3KUmJcO33brs9D3+2pgMnwFvjPspCZvK4lffG2s2+1vtNTvXb13qbrLVypcaakZaamp6eNWdPKEzGaXkwAAcdNnHpwyXD/ie3LNsiCeqrrRDdZbFU32ScU7FPLDlYIY4RJlnIB5kPhCpyO+vq5qel5lPpYiFTC+gex9x+mq700TAq0MYIOAQB2NPFtEw3Xylt3e13ko4PC+1Vdihq/pKOqul0us4YXCqqW8yUKiyI8qkBvw8lbkF9Kg5Lw+Kfi9LXLbttz7beC4bka02kPQSgyUdOrGrmIEvQQoyhh0SAOI5Ln6TMNHHIZG8onHHJxnQSp3HYaW8RWAMfOaF52kVR5UKqVGGc9BiWGF9yATjA12a8m57wljZfOF38bdybi8P3huO4Nvv/mHdL7cppEt8whNEZGjZmYT/ALuTgkjj1HPoHEcgQ67V8aY78bpUWzcVroKTbFbVUrWFYPPuNVT0qOjsV5hoeUgUqeJAVRyzz9HS9x712/aFhpVYT1NUGMEccbEEKMs5YAhVA7Lf9SQDnR7lqqK1LVXmzGCokYiKOPjmUE+gAciQeOORbAXDEkAZ05jg55jRIQQLrnfhf4pbu8SLzta72u5W2ts11tE9feqejiEkNrkby2pqczg9zgO4dT78C3FOs9tB6799K9lum5JPqrld0tcFC2PI8upPlhOzz5FASCCBk4BwSBggm/TXyarq44aalE8J7kqIyRGq4OCrEYfJGPSTjPeNTSmorKfTnGlnckhgEcw5Rlc/vB8fl/PTDI+fnQW+8fLVqnulGfNB0L4s0vwjwLf7VjDGKoS9URVdZTd1lNEXH4y3Pr+3/XU1qq325JSlaOCoIHarGGj7/I9amsLieyBGfK8xqHu/6iEap5oMSPAeqZN2vJ+yYIGmaOlmyk7oAWAx1gaA0S2uiolV7ZFOkY4qyTPEz/nxwcH3+caNbq+o+loFoSGrCxEKvjgTx/iz1/XS5CZ1pZBeBOtQp/erJy8sn5/AOPH9OtJxh8cRe4jQASRIFgYvI6xE7zsn4QTh2id9tffVHNp7rkrKme3WGhlqoUVWCyMESnJ9w79+/wABQT84A71RpPFfcUG86zbl5sFsp6Ki5LJLFcZJKgkRCT93D5I8wcWXJ5DHeM6w2M0tnv8Ad6wW76mmrxHUFqEh1gCxhCBGPj0KRw5ElmyBgZlLQ+HF+3fuCO8W2hkWoSinL1kRjkMvGRSnrAIAVFyv/Oc55a2XAHZsAwkzr9eW3dsg+PAFcwEfm8Z/DyJI5huKKSGe3i6xTRRu8UlLnHmK6gqQM94OR7nVRvGHbs9yqbVRmcyx0aVsE0kLCGdJMiPiQCxyQRgAk4JAIwSDu20tmUtXM21dlbGqI441jPnJTgmP1M65/g7CjHEj1A59PHQGq23YZb/cI/8AIOxqWmpYaCCSaeGKamcyFiycQF5OgEQUH3Dr7Z6NRKpCFhZvH26MtRV3ZLPU0Eq1NTTSUqVkUscMEixSiWFomlBjcgOSiheaH2JKuVR4ibQWqjirq6GKpklpKfFOszr51RkxAOFAYMPnAx84yNJV725si31cTwbO8Np7QYi9I0kaRM6OqmNiQjLxJ80exBCgj2I1jQUuw1rV+t8PPD6ihdIlTzY4ctCSTzQCHtSQ5GSM8fjOm1KTKoh4BT2uLdEeXxbsFTW/su23eqeujM0P08turFeSeIAuuOOFGCpGSfS2ckd632Hxl2zU2+krbxJUxtXRmSljgpqiR5CsfmMOlI/D2O/YHOOJAW7zbtm22gunn7H8N4qylZasw/SROBS49RbEauHyeI4hiSy5ADYC0LK182ZDLt7w22DUXAwtMIoYIKmrRuJAwPLKgeYyDJPSt3x7KxjCUBfKEvaOO661WeMWwbfRx11TcLlTxzF0jY00vFpFQPwDYKlipDKAfWO15DSxc/F267eutLd6Grjudiu1M1dIKx3g+khQKC8RMXrU8lJ5FcH8yBpt2j4UbOo7HRU182NtmStigEMzpQxS88ZAJd05MSCc5+WOmum2btGjp1pqPbFpgiBYiOOjjRQWXixwBjtej9x1pwoU2/lskzk6oZZN6teZTSVG37hRnirs83l8VRgeJIDlhniRjGR8gaPrJDGQkamZ8D27/vrGhtlvt1OaWgoqemiLZKRRhVJ/Qflq5GkcIHBQox8DSdm8nX9/2S5gAq0zVHEvNMsKY+Pf+ulqvvJlEsVhSCrqIXKM1XO0MOR7jmASf1VWGQQcHOG6pp4KqJoqiJJEPurKCD/I6W7rsnbFwWQPZqKOSbAeZKaPzCo+OWM+3WfcZ6xp4oM/5X70zOdklS3fdtznlt9VBa4IULRVNVQV7skWcYAzEH8wA+49Izk/bQ6qnhtf7Wmh3DZbJJPUw2+jqzwfryhJ5jNKSCeUj+4y3Ffvq/uvZ21hcaCntVwsFlajkCPFJaoZ3l5IeIDMMqw/FkHOBk9aG26GDa90ulGm8KRpZ65qmSnNnZIwVhQvGj5IGVVe1yAzN0WONO7Ng0AS5ipWbeaFop33FTXuqJM8E0tqqGLSAdHzoZAkY+3pwMniPjV07Jp7hTSx1N4vK1cqiNqs16qFUEEeWJGlKAYGDx5ZALEt6tApN111EzTS+I1uhoqemWomNRZCZF9XEAYKhsscZwMcGHHOStil33bxa7rXv4kwOsJiVJ3tHlLCxbCrhunZip9I4nDZ9PR0oEWCSV0Ogs9joBDPUVi1lVEvEVNVIkkv/qwNX3u1qi9LXGnH/wDqv++uS3He1VQXFKGu8V6amklgSRAdtMsZV1LIeTMRy4jJ7+D0OgOn2meCutdJUrOtUJYUbz/LC+ZkD1YHtn3xpy5bRebZLIIo7hTszHAUSAknQ7cXB441fMinOYwcFtGRBGPVwUfbrQHc8TSxxpHIYHOcTfCaF8Xn7m/w+o8PO3Oynwv6oQmWpakozwSLCf8A7owevt0RqarxRPRUufLVpActJCQc/wDN3jU1hKtaoMsuLbaRPqEaYxpm0pn3d5i2iCV4mamT1TsgHNRjrH20s22egraDzheFhjfocFeRkH/qHf8A5dNW5mEVrpqp51EcLh3iY480Y9tAEu9lr4nmg2JzjYd4QLyP3+D/ADxp3GWM/EXFzgLCxDjPUZbwOSdgyfu4gHXUR6o/sKjhoZ6qK2tJNQEAieXHN5M9jr2GPjTXb7TQ216yopo+MlfP9TOT7s/BU/pxRR/LSd4eUdRDLUyTTeSCMLSFyxjGejk9n7afAMKM61n2bM8PYYjW3jtvHQ3QniX9wbytVXQUNdC1NW0cFRC49ccqBlb9QetD12dtJY3hTa9qWOR0dkFHGAzK3JSRjshuwfg96L8R2ADqKvfvo8CqCSty+GNnr6Omg23bLDapaZkHrs8MyPCM/usEDiPUex7d/fVd/CzmXLtt1lfB4tt6E5OO8+rsZCkfPXuesdA4jPec68Pp6z/LSSQulLcmydlyPKX2jaJJZ14zN9BFmQdfiPHv2Hv9tYR7G2TE5lh2fZkc5BdaGIHs5PfH76Yi6AnrHzoddb5arLClRda2GlidwgllbinI+wLHofz066csaLb9joFMVBaaWlQknjBEIwSfc4XGsK7a9prvVNJco8HP7i5VMP8AZJBoNuXfdDarYJbPUUldcKr0UUCShvNY/wAWFOSoGScfA9xoPF4sWFLysV03RbaaBVIaEyqMAAHzGY+2eyBn8PqPv12yWCU+01ElGpSOWeRegBLIXI/me/6nVkKD7rnHtpWfxO2IqRyndVt4ykBT9Qp9zjvvrvrvVCl8T7HW7gkp4L3bUtlNT5eV5lDTSM3pKd/gAV8n5yMdDtQkgp5cAd8fj31UkwTgjQKfxL2XGsrvuGj4xoXLB8hgOWeJ/ixwbIGccTqhUb/oa2imqttmnuDwypE8csrQep8cMHgc5yMHGCDnOlSAFEr/AGSuupg/Z97ltvlMWkMUKOZRjAB5A4we+tBP8n7qFO0R8QqvmzK3mi3U3IdYI/Bg5PftkH7jrTbFKzoPMwrYyQGzg/rr1zkY12i5Jlz2Vf62saoh8QLlSIfLPCKmgPEpnsclI9QPYIIJGQAOtaY9mbljUp/2i3MgsGz9HTZ9wSBlMYPfxkZ6IA05OckjWAX++kvK5J/+TNwFucviDdWbJwRTUy4BIOMeXg+2MkZwTppih8tAueRAwSfnW8of5a8OBpySVrfOMDQDc6zCmEiKJAucx/6j8aPuSMnQHcMyQokxJ8yMkovwxx3oZxYA4R88vf8ArfRWML+qIS3HPbvpPMuYnWNvxKxEWPyBwD/fU0Tjrr9VUzSRUdKC6nhyPL+o/wDnU1iH0SQOzbmEamlP1KNseBOYx/7R9Aie7I0lttEhUeY0oERY+gHifxZ+MZ0Cp3qrPFJAu5DEU/CBKHRP0J/30f3bG0tlgUufJLgSquOZGD+H886W7RTyUdG0MVvSWEZORyWST9RgjP8AM6g40cnEnEWMa3G3IXPf07k/B3wwnnp/tMXh/X1txuVTU3QeZUtFhZlTirxgj4++n8LxQD4GkTYFTTVNdUvb4ZKaBU4yQTMC/mA+/wCmnvBMfWtZ9miTgGkuzGTfnfXmfG6E8T/uDAjSy9zgEZ1A2PbOki4bs39SVVZFT+GdRWxRs60ssFwhHmhQcFxJxKZIXGOXRz8Y0Qe+7pW30tQmyap6ibzDNAKqFTEFOFyeWCWHYAPXyQdaAFUEyvKScDoY/nrFnJ0i128N922uqIZPDqpqYGqPKo5aWsR/NT/U4IHl95+SMfPuAUoL3u6Y1DVuypqdYoGkjArYXMzjGIx2ME99nrod99ISlhHp4hOjxycuLgg8WKnB+xHY0ObbttdTHIlS6sMENVSsCPt22ha7g3oLmsM2xpRRFELTpWxMyMVBYceWWwSV+Pw59j17ddxbqhnijtOy6qqjYZkeSoij4nIGAORz1k5/Ie+euzLoVCt8NLZNVRjyTPA7eViVyxp4D6nVSxJPIqq/kPb217uDw1oatE/y9aLJTzTPiqkqqUOZIwuAPbv4HfwT+hKWa9blloUkvW2KmGrYsXjgkiZEHI8RkuCTjGTgd561Vrr9v2K5FaDZi1FH0qs9bHG+cjJPZwOz1gnr89LnPNdCTB4TXwVryJtrYaRx9Jm1nDtkHn0SR0B89EexwDohF4Y3mOGkeptm062s8ySWqmahZRgsOKRg8iPTkZJ+F60wTXfxDa4SxRbXpo6RccJXq1YuQMnoH5OVH26ODkge0l+39Krms2TDTlELLi5o/Ju8D8Ixn0/3+wz3aLlsh8MdpR01Ks1ityzQFJHeGkjjEkgHuQB/qw36garW3w12zbJncW2jdCSQi0qIGJBHKQj8bYJGT8H20WoLluU2iKovNj4VzEh4KadHCj4OWIH9zpOrqvxQSvnq6G01EkQnzFTT1FMsZiwOiRlsk5IOeuvfS5wuCeIbRa6V1eCggjZPwlUAI1vYZA1zz/MPi+gCnZNJOTITy+sji4p7gEc27+CQfzx8a8mvfi08kLjZkCJG7l0SviPmLgBck/hHbHoZyq94J1wISwugGDLHLf01D74X20pRXPxCltFRUPYKKG4mUJBA1SGQIcZcsD6sdnHpzjHXvrTWV3id56Cjs1tWI04dy83Iibj2i4Ychk+5x+H88h0ppCciPzOtTdaVqO8b2FZObltmb6YECAQzwFiMdl8v0c/Y/P8APRihuFbVuy1VoqaMAZDSyRMD+Q4M2llJCtyN8DQLcHDykMh4Jy9Un+jRqRsk/fQO/ossSIQXLEgR/wCvr20M4rfCvj3f3pfldWML+qEvSPbXpHho5qgry7eEMgJ/Juh/fU1m61NFEIRIiGMZCTgHA+3WNTXndZrRAqCDHL+StDTJvlPz/hF9zXem+kpo6Z2aqhn/AHbcG4I4U/iOMEf76D264zV8crXarqWncnmxR0Uf+EpgEY/PU1NQ8VxL6mOcXaaRtpqRzU2GotbQAH8pp2WaeluXllAfMiKRSRxlY+uypOO26z9+vnT0Pw4DamprXfZ1x+6QNifQoJxNsVp6JVi25vdaqolk32rxnkYENvT0khsB+xyALKRjifQASeyZcLJvqRIhQb1p43CqJC9uUjOBlgOWfcH059jjII5ampo9mIQ9t1SrBuSxyRR3HeM1Q1UZUp4ae1LkkRu2M5PYwDk4B44+dAKO7b/nmihqq+7xCSY4eO0RlViwpHMluj2c4B+QM4yZqaUElLsrdPX7nrfIqFud+WNInjcNaBGSzB8SFT36eK4UDPY9+XVX9o7wSkoqsV+4pUMZV1S0KGJVwMsjnkCeXXXsufvymppwK5aprhuyDzWa+bplVeQCJYUyW4gr3x9vufYk4yMatLU7jnlpy1x3IDVo1Kv/ANMCLC//AO5+uhkgDP26+Tqaml3XKm8u8krHEVXuYRScVDfQQsiDscgCxb4yes9jr4Gw1e6bdXz1MlXuitjoSgeFbYnl1HIAEIAORx75B9wfcYzNTXLlWqp98rXzLPXbrWlDlYjBbaduRGf1IHtgkY+Cc5GtD3bcM1VNLGm9aeOUnA/Z8RVDgDADDIwQTnsHOfbU1NOZdIs46u/1UYpee8iKuOTLvQxxNT8WPqyFxyPAgDvpgcHI0Orq/ccFFSq9NvaMSYqGeGCIlWyE8o8znHpZsH3D9ZOAJqaeuXkVbuYV1JTC073Wliq+bTOIycAN+7YdlkACjlk8vMBySpIZ7ftTcddDTV7b33BSpKolkpqiKm8xAQfQeMeARkZ7P4ej2dTU0hJCRXZtl1Un1oO7bx/xcqyj96o8nH8MeAMA9ZH5fmcyybUlsM7Snct2r0KFfLrJhIoOfcdZz18k6mppuYpYRSR+IOTpT3Je4whpQHjZWKiVSvJTjOQM5+fy1NTQfjdV7MKQDqruBY11USgFpuNKFbzoSZmPcgILt+ZJ7z+WpqamsG2u5rQAjxpgklf/2Q==', 'null', 'null', 2),
(3, 'null', 'null', 'null', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `placeproduct`
--

CREATE TABLE `placeproduct` (
  `id` int(11) NOT NULL,
  `namePlace` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `placeproduct`
--

INSERT INTO `placeproduct` (`id`, `namePlace`) VALUES
(1, 'Nacional'),
(2, 'Internacional');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pricescompany`
--

CREATE TABLE `pricescompany` (
  `id` int(11) NOT NULL,
  `typeMoney` varchar(3) DEFAULT NULL,
  `tpm` varchar(50) DEFAULT NULL,
  `ganancia` varchar(50) DEFAULT NULL,
  `idCompany` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `pricescompany`
--

INSERT INTO `pricescompany` (`id`, `typeMoney`, `tpm`, `ganancia`, `idCompany`) VALUES
(0, 'COP', '3200', '0', 0),
(1, 'USD', '3200', '7', 225),
(2, 'USD', '3200', '0', 225),
(3, 'COP', '0', '0', 226),
(7, 'COP', '3299', '0', 227),
(8, 'USD', '3200', '0', 227),
(9, 'USD', '3200', '0', 227),
(10, 'USD', '3200', '0', 227),
(11, 'USD', '3200', '0', 227),
(12, 'USD', '3200', '0', 227),
(13, 'COP', '0', '0', 228),
(14, 'USD', '3200', '0', 229),
(15, 'USD', '3200', '0', 225),
(16, 'USD', '3200', '0', 225),
(17, 'USD', '3200', '0', 225),
(18, 'USD', '3200', '0', 225),
(19, 'USD', '3200', '0', 225),
(20, 'USD', '3200', '0', 225),
(21, 'USD', '3200', '0', 225),
(22, 'USD', '3200', '0', 225),
(23, 'USD', '3200', '0', 225),
(24, 'USD', '3200', '0', 225),
(25, 'USD', '3200', '0', 225),
(26, 'USD', '3200', '0', 225),
(27, 'USD', '3200', '0', 230),
(28, 'USD', '3200', '0', 231),
(29, 'COP', '0', '0', 232),
(30, 'USD', '3200', '0', 233),
(31, 'USD', '3200', '0', 226),
(32, 'USD', '3200', '0', 235),
(33, 'USD', '3200', '0', 236),
(34, 'COP', '0', '0', 237),
(35, 'USD', '3200', '', 10),
(36, 'USD', '3200', '0', 238),
(37, 'COP', '0', '0', 239),
(38, 'COP', '3288', '0', 240),
(39, 'USD', '3200', '0', 241),
(40, 'USD', '3200', '0', 243),
(41, 'USD', '3200', '0', 244),
(42, 'USD', '3200', '0', 245),
(43, 'USD', '3200', '0', 246),
(44, 'USD', '3200', '0', 247),
(45, 'COP', '3288', '0', 248),
(46, 'USD', '3200', '0', 249),
(47, 'USD', '3200', '0', 2),
(49, 'USD', '3200', '0', 13),
(50, 'USD', '3200', '0', 14);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pricesmainteince`
--

CREATE TABLE `pricesmainteince` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `ganance` varchar(20) NOT NULL,
  `time` int(11) NOT NULL,
  `price` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `pricesmainteince`
--

INSERT INTO `pricesmainteince` (`id`, `name`, `description`, `ganance`, `time`, `price`) VALUES
(1, 'aplicar aceite a maquinas', '<p>aplicar aceite a la maquina</p>\r\n', '15,5', 180, '50000'),
(2, 'sdfds3', '<p>dfsdffsdfsd</p>\r\n', '323', 233, '233');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `idProduct` int(11) NOT NULL,
  `typeProduct` varchar(45) NOT NULL,
  `name` varchar(150) NOT NULL,
  `brand` varchar(45) NOT NULL,
  `reference` varchar(45) NOT NULL,
  `presentation` varchar(45) NOT NULL,
  `amount` int(11) NOT NULL,
  `minimumAmount` int(11) NOT NULL,
  `typeCurrency` varchar(45) NOT NULL,
  `priceUnit` varchar(50) NOT NULL,
  `priceUnitIndividual` varchar(50) NOT NULL,
  `country` varchar(50) NOT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish2_ci NOT NULL,
  `percent` varchar(50) NOT NULL,
  `quantity` int(11) NOT NULL,
  `nameCompany` varchar(300) NOT NULL,
  `idQuotation` int(11) NOT NULL,
  `timeDelivery` varchar(35) NOT NULL,
  `namePlace` varchar(300) NOT NULL,
  `IVA` int(11) DEFAULT NULL,
  `FLETE` varchar(11) DEFAULT NULL,
  `nodo` text NOT NULL,
  `isFromCompany` int(4) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `product`
--

INSERT INTO `product` (`id`, `idProduct`, `typeProduct`, `name`, `brand`, `reference`, `presentation`, `amount`, `minimumAmount`, `typeCurrency`, `priceUnit`, `priceUnitIndividual`, `country`, `description`, `percent`, `quantity`, `nameCompany`, `idQuotation`, `timeDelivery`, `namePlace`, `IVA`, `FLETE`, `nodo`, `isFromCompany`) VALUES
(1, 3, 'Equipos', 'equipo test', 'fdsjk', 'jjkh', 'hjkhkj', 1, 1, 'USD', '16000000', '5000', 'Angola', '<p>dsdsfdfsd</p><p>fsd</p><p>f</p><p>sdf</p><p>sd</p><p>f</p><p>sdf</p><p>ds</p><p>fsd</p><p>f</p><p>dsf</p><p>ds</p><p>f</p><p>dsf</p><p>ds</p><p>f</p><p>ds</p><p>f</p><p>&nbsp;</p><p>&nbsp;</p>', '20', 1, 'proveedor', 3, '5 dias, salvo venta previa', 'Nacional', 1, '0', '<div class=\"box-header with-border\"><h3 class=\"box-title \" id=\"item-value-3\" data-id-producto=\"id-3\"><b>2</b> - equipo test <br><p class=\"text-center\"><b>hjkhkj</b></p><p></p></h3></div><form class=\"form-horizontal\"><div class=\"box-body\"><input type=\"hidden\" id=\"product-id\" value=\"0\"><input type=\"hidden\" id=\"priceUnitario-3\" value=\"16000000\"><input type=\"hidden\" id=\"priceUnitarioCOP-3\" value=\"16000000\"><input type=\"hidden\" id=\"precioColombiano-3\" value=\"16000000\"><input type=\"hidden\" id=\"type-3\" value=\"Nacional\"><div class=\"row\"><div class=\"col-sm-12\"><div style=\"border-radius:5px;margin:5px;\" class=\" bg-null text-center\"> <input type=\"checkbox\" name=\"checkbox\" style=\"margin-top: 5px\" class=\"hidden checked\" id=\"IVA-3\" data-iva=\"3\" disabled=\"\" onclick=\"OnClickIVA(3)\" checked=\"\" data-check=\"true\" 2=\"\" <=\"\" div=\"\"></div></div><div class=\"row\" data-row-index=\"0\" id=\"content-3\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Cantidad</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-cube\"></i></span><input id=\"quantityCtx-3\" name=\"quantityCtx\" type=\"number\" min=\"0\" onchange=\"return CountquantityCtx(3)\" class=\"form-control\" value=\"1\"></div></div><input id=\"unitePriceCtx-3\" name=\"unitePriceCtx\" type=\"hidden\" class=\"form-control\" data-price=\"5000\" value=\"100\"><input id=\"precioOriginalCtx-3\" name=\"precioOriginalCtx\" type=\"hidden\" class=\"form-control\" data-price=\"5000\" value=\"100\"><input id=\"precioVentaCtx-3\" name=\"precioVentaCtx\" type=\"hidden\" class=\"form-control\" data-price=\"5000\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Ganancia</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"  fa fa-bookmark-o\"></i></span><input id=\"porcent-3\" name=\"porcentCtx\" type=\"number\" min=\"0\" class=\"form-control\" data=\"3\" onchange=\"ValidateChangePercents(3)\" value=\"20\"></div></div></div><br><div class=\"row\" data-row-index=\"0\" id=\"contentProductQxt\"><input type=\"hidden\" name=\"precioUnitarioConGanancia\" id=\"precioUnitarioConGanancia-3\" value=\"19200000\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Tiempo Entrega</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-history\"></i></span><select name=\"timeDelivery\" id=\"timeDelivery-3\" data=\"3\" class=\"form-control\" style=\"border-color: rgb(210, 214, 222);\"><option value=\"5 dias, salvo venta previa\">5 dias, salvo venta previa</option></select></div></div><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Precio</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-dollar\"></i></span><input id=\"totalPriceCtx-3\" valor-modificable=\"$19,200,000\" undefined]=\"\" name=\"totalPriceCtx-3\" data-type=\"COP\" data-value=\"19200000\" data-place=\"Nacional\" data-money=\"USD\" data-typeproduct=\"Equipos\" data-id=\"3\" data-proveedor=\"proveedor\" data-priceu=\"[object Undefined]\" type=\"text\" class=\"form-control\" disabled=\"\" lol=\"\" value=\"$19,200,000\"></div></div></div></div><div class=\"box-footer\"><button type=\"button\" onclick=\"DeleteProduct(3)\" class=\"btn btn-danger btn-xs-block\">Cancelar</button></div></div></form>', 1),
(2, 2, 'Equipos', 'equipo', '76', '76', '876', 7, 7, 'USD', '80000000', '25000', 'Angola', '<p>sdsfdsfsdfsdfdcsdcdcssd</p>', '20', 2, 'proveedor', 3, '5 dias, salvo venta previa', 'Nacional', 1, '0', '<div class=\"box-header with-border\"><h3 class=\"box-title \" id=\"item-value-2\" data-id-producto=\"id-2\"><b>1</b> - equipo <br><p class=\"text-center\"><b>876</b></p><p></p></h3></div><form class=\"form-horizontal\"><div class=\"box-body\"><input type=\"hidden\" id=\"product-id\" value=\"1\"><input type=\"hidden\" id=\"priceUnitario-2\" value=\"80000000\"><input type=\"hidden\" id=\"priceUnitarioCOP-2\" value=\"80000000\"><input type=\"hidden\" id=\"precioColombiano-2\" value=\"80000000\"><input type=\"hidden\" id=\"type-2\" value=\"Nacional\"><div class=\"row\"><div class=\"col-sm-12\"><div style=\"border-radius:5px;margin:5px;\" class=\" bg-null text-center\"> <input type=\"checkbox\" name=\"checkbox\" style=\"margin-top: 5px\" class=\"hidden checked\" id=\"IVA-2\" data-iva=\"2\" disabled=\"\" onclick=\"OnClickIVA(2)\" checked=\"\" data-check=\"true\" 1=\"\" <=\"\" div=\"\"></div></div><div class=\"row\" data-row-index=\"1\" id=\"content-2\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Cantidad</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-cube\"></i></span><input id=\"quantityCtx-2\" name=\"quantityCtx\" type=\"number\" min=\"0\" onchange=\"return CountquantityCtx(2)\" class=\"form-control\" value=\"1\"></div></div><input id=\"unitePriceCtx-2\" name=\"unitePriceCtx\" type=\"hidden\" class=\"form-control\" data-price=\"25000\" value=\"100\"><input id=\"precioOriginalCtx-2\" name=\"precioOriginalCtx\" type=\"hidden\" class=\"form-control\" data-price=\"25000\" value=\"100\"><input id=\"precioVentaCtx-2\" name=\"precioVentaCtx\" type=\"hidden\" class=\"form-control\" data-price=\"25000\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Ganancia</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"  fa fa-bookmark-o\"></i></span><input id=\"porcent-2\" name=\"porcentCtx\" type=\"number\" min=\"0\" class=\"form-control\" data=\"2\" onchange=\"ValidateChangePercents(2)\" value=\"20\"></div></div></div><br><div class=\"row\" data-row-index=\"1\" id=\"contentProductQxt\"><input type=\"hidden\" name=\"precioUnitarioConGanancia\" id=\"precioUnitarioConGanancia-2\" value=\"250005000\"><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Tiempo Entrega</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-history\"></i></span><select name=\"timeDelivery\" id=\"timeDelivery-2\" data=\"2\" class=\"form-control\" style=\"border-color: rgb(210, 214, 222);\"><option value=\"5 dias, salvo venta previa\">5 dias, salvo venta previa</option></select></div></div><div class=\"col-sm-6 col-xs-12\"><label for=\"\">Precio</label> <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-dollar\"></i></span><input id=\"totalPriceCtx-2\" valor-modificable=\"$60,000\" undefined]=\"\" name=\"totalPriceCtx-2\" data-type=\"COP\" data-value=\"60000\" data-place=\"Nacional\" data-money=\"USD\" data-typeproduct=\"Equipos\" data-id=\"2\" data-proveedor=\"proveedor\" data-priceu=\"[object Undefined]\" type=\"text\" class=\"form-control\" disabled=\"\" lol=\"\" value=\"$60,000\"></div></div></div></div><div class=\"box-footer\"><button type=\"button\" onclick=\"DeleteProduct(2)\" class=\"btn btn-danger btn-xs-block\">Cancelar</button></div></div></form>', 1),
(3, 3, 'Equipos', 'MI EQUIPO EXTERNO editado', '56756', '678', '88779', 1, 0, '0', '0', '', 'Nacional', 'HOLA A TODOS Y EL MUNDO', '0', 1, 'tertret', 0, 'Sin establece', 'Nacional', 0, '0', ' ', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `quotation`
--

CREATE TABLE `quotation` (
  `id` int(11) NOT NULL,
  `idUser` int(11) NOT NULL,
  `code` int(11) NOT NULL,
  `typeMoney` varchar(3) NOT NULL,
  `state` varchar(50) NOT NULL,
  `generationDate` datetime NOT NULL,
  `total` double NOT NULL,
  `idCompany` int(11) NOT NULL,
  `offerValidity` varchar(60) NOT NULL,
  `placeDelivery` varchar(80) NOT NULL,
  `payment` varchar(45) NOT NULL,
  `typeQuotation` varchar(45) NOT NULL,
  `version` int(11) NOT NULL,
  `IVA` int(11) NOT NULL,
  `reminder` int(11) NOT NULL,
  `isDDP` int(11) NOT NULL,
  `checkeable` int(11) DEFAULT 0,
  `dateUpdate` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `quotation`
--

INSERT INTO `quotation` (`id`, `idUser`, `code`, `typeMoney`, `state`, `generationDate`, `total`, `idCompany`, `offerValidity`, `placeDelivery`, `payment`, `typeQuotation`, `version`, `IVA`, `reminder`, `isDDP`, `checkeable`, `dateUpdate`) VALUES
(0, 1, 1, 'COP', 'Compra', '2019-08-13 11:57:15', 114240000, 3, '60', 'En sus Instalaciones', '30 dÃ­as fecha de facturaciÃ³n', 'FCA', 2, 19, 0, 0, 0, '2019-08-13'),
(2, 1, 2, 'COP', 'Cancelada', '2019-09-11 12:36:47', 22919400, 3, '30', 'En sus Instalaciones', '50% anticipado - 50% contra Entrega', 'DDP', 2, 19, 0, 1, 0, '2019-09-11'),
(3, 1, 3, 'COP', 'Creada', '2019-09-11 12:37:15', 22919400, 3, '30', 'En sus Instalaciones', '50% anticipado - 50% contra Entrega', 'DDP', 1, 19, 0, 1, 0, '2019-09-11');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `quotationmainteince`
--

CREATE TABLE `quotationmainteince` (
  `id` int(11) NOT NULL,
  `idUser` int(11) NOT NULL,
  `code` int(11) NOT NULL,
  `typeMoney` varchar(3) NOT NULL,
  `state` varchar(50) NOT NULL,
  `generationDate` datetime NOT NULL,
  `total` double NOT NULL,
  `idCompany` int(200) NOT NULL,
  `offerValidity` varchar(60) NOT NULL,
  `placeDelivery` varchar(80) NOT NULL,
  `payment` varchar(45) NOT NULL,
  `typeQuotation` varchar(45) NOT NULL,
  `version` int(11) NOT NULL,
  `IVA` int(11) NOT NULL,
  `reminder` int(11) NOT NULL,
  `isDDP` int(11) NOT NULL,
  `checkeable` int(11) DEFAULT 0,
  `dateUpdate` date DEFAULT NULL,
  `idViatic` int(11) NOT NULL,
  `totalViatic` varchar(50) NOT NULL,
  `idMainteince` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `quotationmainteince`
--

INSERT INTO `quotationmainteince` (`id`, `idUser`, `code`, `typeMoney`, `state`, `generationDate`, `total`, `idCompany`, `offerValidity`, `placeDelivery`, `payment`, `typeQuotation`, `version`, `IVA`, `reminder`, `isDDP`, `checkeable`, `dateUpdate`, `idViatic`, `totalViatic`, `idMainteince`) VALUES
(1, 1, 1, 'COP', 'Creada', '2019-09-11 19:56:21', 2667155, 3, '30', 'En sus Instalaciones', '30 dÃ­as fecha de facturaciÃ³n', 'DDP', 1, 19, 0, 1, 0, '2019-09-11', 1, '$1323050', 1),
(2, 1, 2, 'COP', 'Creada', '2019-09-12 12:43:00', 16588510, 3, '30', 'En sus Instalaciones', '30 dÃ­as fecha de facturaciÃ³n', 'DDP', 1, 19, 0, 1, 0, '2019-09-11', 1, '$1,323,050', 1),
(3, 1, 3, 'COP', 'Creada', '2019-09-12 12:44:52', 16588510, 3, '30', 'En sus Instalaciones', '30 dÃ­as fecha de facturaciÃ³n', 'DDP', 1, 19, 0, 1, 0, '2019-09-11', 1, '$1,323,050', 1),
(4, 1, 4, 'COP', 'Creada', '2019-09-12 12:46:02', 16588510, 3, '30', 'En sus Instalaciones', '30 dÃ­as fecha de facturaciÃ³n', 'DDP', 1, 19, 0, 1, 0, '2019-09-11', 1, '$1,323,050', 1),
(5, 1, 5, 'COP', 'Creada', '2019-09-12 12:47:06', 16588510, 3, '30', 'En sus Instalaciones', '30 dÃ­as fecha de facturaciÃ³n', 'DDP', 1, 19, 0, 1, 0, '2019-09-11', 1, '$1,323,050', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `role`
--

CREATE TABLE `role` (
  `id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `icon` varchar(15) DEFAULT NULL,
  `color` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `role`
--

INSERT INTO `role` (`id`, `name`, `icon`, `color`) VALUES
(1, 'Servicio Tecnico', 'user', 'navy'),
(2, 'Coordinador de Ventas', 'user', 'aqua'),
(3, 'Coordinador General', 'user', 'teal'),
(4, 'Gestor de Compras', 'user', 'maroon'),
(5, 'Asistente Contable', 'user', 'purple'),
(6, 'Asistente Comercial', 'user', 'olive'),
(7, 'Cliente', 'user', 'green'),
(8, 'Proveedor', 'user', 'orange'),
(9, 'Prospecto', 'user', 'yellow'),
(10, 'asistente de Mantenimiento', 'user', 'red'),
(11, 'Asesor Comercial', 'user', 'fuchsia'),
(12, 'Administrativo', 'user', 'blue'),
(13, 'SubDistribuidor', 'user', 'red');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rolecompany`
--

CREATE TABLE `rolecompany` (
  `id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `rolecompany`
--

INSERT INTO `rolecompany` (`id`, `name`) VALUES
(1, 'Empresa'),
(2, 'Cliente'),
(3, 'Proveedor'),
(4, 'Prospecto'),
(5, 'SubDistribuidor');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `serial`
--

CREATE TABLE `serial` (
  `id` int(11) NOT NULL,
  `numberSerial` varchar(50) NOT NULL,
  `numberRow` int(11) NOT NULL,
  `idInventary` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tracingquotation`
--

CREATE TABLE `tracingquotation` (
  `id` int(11) NOT NULL,
  `idQuotation` int(11) NOT NULL,
  `idResponsable` int(11) NOT NULL,
  `subject` varchar(50) NOT NULL,
  `description` varchar(1500) NOT NULL,
  `dateRegistry` datetime NOT NULL,
  `prediction` varchar(100) DEFAULT 'AA'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tracingquotation`
--

INSERT INTO `tracingquotation` (`id`, `idQuotation`, `idResponsable`, `subject`, `description`, `dateRegistry`, `prediction`) VALUES
(1, 1, 4, 'Negociacion', '<p>Se solicito un mejor descuento</p>\r\n', '2019-05-31 00:00:05', 'AA | El Cliente tiene dinero y quiere comprarle a vortex | 95%'),
(2, 1, 4, 'Llamada', '<p>NNN</p>\r\n', '2019-06-07 00:00:06', 'AC | El Cliente tiene dinero, pero quiere comprarle a nuestra competencia | 15%'),
(3, 4, 4, 'jhdkjwfdbwejkufbeuif lkwndoiqf lkndoiqwfn lkcnewih', '', '2019-05-20 00:00:05', 'AC | El Cliente tiene dinero, pero quiere comprarle a nuestra competencia | 15%'),
(4, 1, 4, 'Llamada', '<p>Debemos buscar un descuento</p>\r\n', '2019-06-01 00:00:06', 'AC | El Cliente tiene dinero, pero quiere comprarle a nuestra competencia | 15%'),
(5, 5, 1, 'd', '<p>uufdhjgfj</p>\r\n', '2019-06-07 00:00:06', 'AB | El CLiente tiene el dinero , y esta inseguro de cual vendedor escoger   | 75%');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `nickname` varchar(60) NOT NULL,
  `password` varchar(60) NOT NULL,
  `state` int(11) NOT NULL,
  `photo` varchar(100) NOT NULL,
  `idPerson` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `user`
--

INSERT INTO `user` (`id`, `nickname`, `password`, `state`, `photo`, `idPerson`) VALUES
(1, 'hakem', 'hakem', 1, 'view/img/users/1/1034.jpeg', 1),
(2, 'danielaV', 'daniela03', 1, 'view/img/users/default/anonymous.png', 2),
(3, 'ruby', 'ruby07', 1, 'view/img/users/default/anonymous.png', 3),
(4, 'alberto', 'alberto07', 1, 'view/img/users/default/anonymous.png', 4),
(5, 'PaolaT', 'paola07', 1, 'view/img/users/default/anonymous.png', 5),
(7, 'JhostonZ', 'Jhoston', 1, 'view/img/users/default/anonymous.png', 7),
(9, 'mduque', 'm123456@', 1, 'view/img/users/default/anonymous.png', 9);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usersquotation`
--

CREATE TABLE `usersquotation` (
  `id` int(11) NOT NULL,
  `idPerson` int(11) NOT NULL,
  `completeName` varchar(60) NOT NULL,
  `idQuotation` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usersquotation`
--

INSERT INTO `usersquotation` (`id`, `idPerson`, `completeName`, `idQuotation`) VALUES
(1, 57, 'prueba hjkn', 2),
(2, 57, 'prueba hjkn', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usersquotationmainteince`
--

CREATE TABLE `usersquotationmainteince` (
  `id` int(11) NOT NULL,
  `idPerson` int(11) NOT NULL,
  `completeName` varchar(60) NOT NULL,
  `idQuotation` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usersquotationmainteince`
--

INSERT INTO `usersquotationmainteince` (`id`, `idPerson`, `completeName`, `idQuotation`) VALUES
(1, 57, 'prueba hjkn', 1),
(2, 57, 'prueba hjkn', 2),
(3, 57, 'prueba hjkn', 3),
(4, 57, 'prueba hjkn', 4),
(5, 57, 'prueba hjkn', 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `viatics`
--

CREATE TABLE `viatics` (
  `id` int(11) NOT NULL,
  `place` varchar(200) NOT NULL,
  `days` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `viatics`
--

INSERT INTO `viatics` (`id`, `place`, `days`) VALUES
(1, 'viaticos a bucaramanga', 1),
(3, 'viaticos a medellin', 1),
(5, 'viaticos a barranquilla aaaa', 4),
(6, 'gfdgfd', 34324);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `viaticspropety`
--

CREATE TABLE `viaticspropety` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `percent` varchar(10) NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` varchar(10) NOT NULL,
  `idViatic` int(11) NOT NULL,
  `typeViatic` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `viaticspropety`
--

INSERT INTO `viaticspropety` (`id`, `name`, `percent`, `quantity`, `price`, `idViatic`, `typeViatic`) VALUES
(1, 'ticketes aeropuerto ', '5', 3, '200000', 1, 'transport'),
(3, 'hotel mi casita', '1', 5, '125000', 1, 'hospedaje'),
(5, 'taxis', '3', 10, '6000', 1, 'transport'),
(6, 'vuelos', '2', 2, '240000', 3, 'transport'),
(7, 'fd', '3', 1, '1500', 6, 'transport'),
(8, 'dfgg', '1', 45, '5345', 6, 'transport'),
(9, 'dfsdfds', '32', 32, '32333', 6, 'transport');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `visits`
--

CREATE TABLE `visits` (
  `id` int(11) NOT NULL,
  `idPerson` int(11) DEFAULT NULL,
  `idClient` int(11) DEFAULT NULL,
  `dateCreate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `visits`
--

INSERT INTO `visits` (`id`, `idPerson`, `idClient`, `dateCreate`) VALUES
(1, 2, 10, '2019-05-21 12:11:45'),
(2, 4, 37, '2019-06-28 00:07:52');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `visitsdescriptions`
--

CREATE TABLE `visitsdescriptions` (
  `id` int(255) NOT NULL,
  `description` varchar(10000) DEFAULT NULL,
  `idVisits` int(255) DEFAULT NULL,
  `dateCreate` datetime DEFAULT NULL,
  `dateCita` varchar(100) DEFAULT NULL,
  `responsable` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `visitsdescriptions`
--

INSERT INTO `visitsdescriptions` (`id`, `description`, `idVisits`, `dateCreate`, `dateCita`, `responsable`) VALUES
(1, '<p>mcvnjfvnjfln bflkmvl&ntilde;scenvirw jeknvlke</p>\r\n', 1, '2019-05-21 12:14:40', '2019-05-22', 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `accesshistory`
--
ALTER TABLE `accesshistory`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_accessHistory_user1_idx` (`idUser`);

--
-- Indices de la tabla `aditionalsimposts`
--
ALTER TABLE `aditionalsimposts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`name`),
  ADD KEY `percent` (`percent`),
  ADD KEY `idPricesCompany` (`idPricesCompany`),
  ADD KEY `CIF` (`CIF`),
  ADD KEY `FCA` (`FCA`),
  ADD KEY `DDP` (`DDP`),
  ADD KEY `id` (`id`);

--
-- Indices de la tabla `alert`
--
ALTER TABLE `alert`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `clientuser`
--
ALTER TABLE `clientuser`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_company_roleCompany1_idx` (`idRoleCompany`),
  ADD KEY `name` (`name`),
  ADD KEY `nit` (`nit`),
  ADD KEY `phone` (`phone`),
  ADD KEY `email` (`email`),
  ADD KEY `webSite` (`webSite`),
  ADD KEY `address` (`address`),
  ADD KEY `city` (`city`),
  ADD KEY `country` (`country`),
  ADD KEY `creationDate` (`creationDate`),
  ADD KEY `state` (`state`);

--
-- Indices de la tabla `diagnosis`
--
ALTER TABLE `diagnosis`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `elementsmainteince`
--
ALTER TABLE `elementsmainteince`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idProduct` (`idProduct`),
  ADD KEY `typeProduct` (`typeProduct`),
  ADD KEY `name` (`name`),
  ADD KEY `brand` (`brand`),
  ADD KEY `IVA` (`IVA`),
  ADD KEY `FLETE` (`FLETE`),
  ADD KEY `country` (`country`),
  ADD KEY `amount` (`amount`),
  ADD KEY `priceUnitIndividual` (`priceUnitIndividual`),
  ADD KEY `typeCurrency` (`typeCurrency`),
  ADD KEY `priceUnit` (`priceUnit`),
  ADD KEY `minimumAmount` (`minimumAmount`),
  ADD KEY `presentation` (`presentation`),
  ADD KEY `reference` (`reference`);

--
-- Indices de la tabla `event`
--
ALTER TABLE `event`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `historymodification`
--
ALTER TABLE `historymodification`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `historymodificationmainteince`
--
ALTER TABLE `historymodificationmainteince`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `historytrm`
--
ALTER TABLE `historytrm`
  ADD PRIMARY KEY (`id`),
  ADD KEY `date` (`date`),
  ADD KEY `money` (`money`),
  ADD KEY `typeMoney` (`typeMoney`),
  ADD KEY `generation` (`generation`),
  ADD KEY `id` (`id`);

--
-- Indices de la tabla `impost`
--
ALTER TABLE `impost`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `inventory`
--
ALTER TABLE `inventory`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_index` (`id`),
  ADD KEY `brand_index` (`brand`),
  ADD KEY `reference_index` (`reference`),
  ADD KEY `presentation_index` (`presentation`),
  ADD KEY `amount_index` (`amount`),
  ADD KEY `minimumAmount_index` (`minimumAmount`),
  ADD KEY `typeProduct` (`typeProduct`),
  ADD KEY `typeCurrency` (`typeCurrency`),
  ADD KEY `description` (`description`(255)),
  ADD KEY `nameComapany_index` (`nameCompany`),
  ADD KEY `price_index` (`priceUnit`),
  ADD KEY `name_index` (`name`),
  ADD KEY `country` (`country`),
  ADD KEY `namePlace` (`namePlace`),
  ADD KEY `IVA` (`IVA`),
  ADD KEY `FLETE` (`FLETE`);

--
-- Indices de la tabla `licence`
--
ALTER TABLE `licence`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `mailallows`
--
ALTER TABLE `mailallows`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `module`
--
ALTER TABLE `module`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `subName` (`subName`),
  ADD KEY `name` (`name`);

--
-- Indices de la tabla `moduleaction`
--
ALTER TABLE `moduleaction`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `moduleevent`
--
ALTER TABLE `moduleevent`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `modulerole`
--
ALTER TABLE `modulerole`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `idRol` (`idRol`),
  ADD KEY `idModule` (`idModule`),
  ADD KEY `state` (`state`);

--
-- Indices de la tabla `ordenbuyclient`
--
ALTER TABLE `ordenbuyclient`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `ordenbuyprovider`
--
ALTER TABLE `ordenbuyprovider`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `percentscodes`
--
ALTER TABLE `percentscodes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `person`
--
ALTER TABLE `person`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_person_role_idx` (`idRole`),
  ADD KEY `fk_person_company1_idx` (`idCompany`),
  ADD KEY `names` (`names`),
  ADD KEY `lastNames` (`lastNames`),
  ADD KEY `documentType` (`documentType`),
  ADD KEY `document` (`document`),
  ADD KEY `cellPhone` (`cellPhone`),
  ADD KEY `telephone` (`telephone`),
  ADD KEY `email` (`email`),
  ADD KEY `address` (`address`),
  ADD KEY `city` (`city`),
  ADD KEY `creationDate` (`creationDate`),
  ADD KEY `state` (`state`);

--
-- Indices de la tabla `personcreatequotation`
--
ALTER TABLE `personcreatequotation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `completeName` (`completeName`),
  ADD KEY `rolName` (`rolName`),
  ADD KEY `cellphone` (`cellphone`),
  ADD KEY `email` (`email`),
  ADD KEY `idQuotation` (`idQuotation`),
  ADD KEY `state` (`state`);

--
-- Indices de la tabla `personcreatequotationmainteince`
--
ALTER TABLE `personcreatequotationmainteince`
  ADD PRIMARY KEY (`id`),
  ADD KEY `completeName` (`completeName`),
  ADD KEY `rolName` (`rolName`),
  ADD KEY `cellphone` (`cellphone`),
  ADD KEY `email` (`email`),
  ADD KEY `idQuotation` (`idQuotation`),
  ADD KEY `state` (`state`);

--
-- Indices de la tabla `photodiagnosis`
--
ALTER TABLE `photodiagnosis`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `placeproduct`
--
ALTER TABLE `placeproduct`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `pricescompany`
--
ALTER TABLE `pricescompany`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `pricesmainteince`
--
ALTER TABLE `pricesmainteince`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idProduct` (`idProduct`),
  ADD KEY `typeProduct` (`typeProduct`),
  ADD KEY `name` (`name`),
  ADD KEY `brand` (`brand`),
  ADD KEY `IVA` (`IVA`),
  ADD KEY `FLETE` (`FLETE`),
  ADD KEY `country` (`country`),
  ADD KEY `amount` (`amount`),
  ADD KEY `priceUnitIndividual` (`priceUnitIndividual`),
  ADD KEY `typeCurrency` (`typeCurrency`),
  ADD KEY `priceUnit` (`priceUnit`),
  ADD KEY `minimumAmount` (`minimumAmount`),
  ADD KEY `presentation` (`presentation`),
  ADD KEY `reference` (`reference`);

--
-- Indices de la tabla `quotation`
--
ALTER TABLE `quotation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idUser` (`idUser`),
  ADD KEY `code` (`code`),
  ADD KEY `typeMoney` (`typeMoney`),
  ADD KEY `state` (`state`),
  ADD KEY `generationDate` (`generationDate`),
  ADD KEY `total` (`total`),
  ADD KEY `idCompany` (`idCompany`),
  ADD KEY `offerValidity` (`offerValidity`),
  ADD KEY `placeDelivery` (`placeDelivery`),
  ADD KEY `typeQuotation` (`typeQuotation`),
  ADD KEY `payment` (`payment`),
  ADD KEY `IVA` (`IVA`),
  ADD KEY `version` (`version`),
  ADD KEY `reminder` (`reminder`),
  ADD KEY `isDDP` (`isDDP`),
  ADD KEY `checkeable` (`checkeable`);

--
-- Indices de la tabla `quotationmainteince`
--
ALTER TABLE `quotationmainteince`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idUser` (`idUser`),
  ADD KEY `code` (`code`),
  ADD KEY `typeMoney` (`typeMoney`),
  ADD KEY `state` (`state`),
  ADD KEY `generationDate` (`generationDate`),
  ADD KEY `total` (`total`),
  ADD KEY `idCompany` (`idCompany`),
  ADD KEY `offerValidity` (`offerValidity`),
  ADD KEY `placeDelivery` (`placeDelivery`),
  ADD KEY `typeQuotation` (`typeQuotation`),
  ADD KEY `payment` (`payment`),
  ADD KEY `IVA` (`IVA`),
  ADD KEY `version` (`version`),
  ADD KEY `reminder` (`reminder`),
  ADD KEY `isDDP` (`isDDP`),
  ADD KEY `checkeable` (`checkeable`);

--
-- Indices de la tabla `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `rolecompany`
--
ALTER TABLE `rolecompany`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `serial`
--
ALTER TABLE `serial`
  ADD PRIMARY KEY (`id`),
  ADD KEY `numberSerial` (`numberSerial`),
  ADD KEY `id` (`id`),
  ADD KEY `numberRow` (`numberRow`),
  ADD KEY `idInventary` (`idInventary`);

--
-- Indices de la tabla `tracingquotation`
--
ALTER TABLE `tracingquotation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idQuotation` (`idQuotation`),
  ADD KEY `idResponsable` (`idResponsable`),
  ADD KEY `subject` (`subject`),
  ADD KEY `description` (`description`(767)),
  ADD KEY `prediction` (`prediction`),
  ADD KEY `dateRegistry` (`dateRegistry`),
  ADD KEY `id` (`id`);

--
-- Indices de la tabla `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_user_person1_idx` (`idPerson`);

--
-- Indices de la tabla `usersquotation`
--
ALTER TABLE `usersquotation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idPerson` (`idPerson`),
  ADD KEY `completeName` (`completeName`),
  ADD KEY `idQuotation` (`idQuotation`);

--
-- Indices de la tabla `usersquotationmainteince`
--
ALTER TABLE `usersquotationmainteince`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idPerson` (`idPerson`),
  ADD KEY `completeName` (`completeName`),
  ADD KEY `idQuotation` (`idQuotation`);

--
-- Indices de la tabla `viatics`
--
ALTER TABLE `viatics`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `viaticspropety`
--
ALTER TABLE `viaticspropety`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `visits`
--
ALTER TABLE `visits`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `idPerson` (`idPerson`),
  ADD KEY `idClient` (`idClient`),
  ADD KEY `dateCreate` (`dateCreate`);

--
-- Indices de la tabla `visitsdescriptions`
--
ALTER TABLE `visitsdescriptions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `description` (`description`(767)),
  ADD KEY `idVisits` (`idVisits`),
  ADD KEY `dateCreate` (`dateCreate`),
  ADD KEY `dateCita` (`dateCita`),
  ADD KEY `responsable` (`responsable`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `accesshistory`
--
ALTER TABLE `accesshistory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=217;

--
-- AUTO_INCREMENT de la tabla `aditionalsimposts`
--
ALTER TABLE `aditionalsimposts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;

--
-- AUTO_INCREMENT de la tabla `alert`
--
ALTER TABLE `alert`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `clientuser`
--
ALTER TABLE `clientuser`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT de la tabla `company`
--
ALTER TABLE `company`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `diagnosis`
--
ALTER TABLE `diagnosis`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `elementsmainteince`
--
ALTER TABLE `elementsmainteince`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT de la tabla `event`
--
ALTER TABLE `event`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `historymodification`
--
ALTER TABLE `historymodification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `historymodificationmainteince`
--
ALTER TABLE `historymodificationmainteince`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `historytrm`
--
ALTER TABLE `historytrm`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `impost`
--
ALTER TABLE `impost`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `inventory`
--
ALTER TABLE `inventory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `licence`
--
ALTER TABLE `licence`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `mailallows`
--
ALTER TABLE `mailallows`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `module`
--
ALTER TABLE `module`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT de la tabla `moduleaction`
--
ALTER TABLE `moduleaction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=807;

--
-- AUTO_INCREMENT de la tabla `moduleevent`
--
ALTER TABLE `moduleevent`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT de la tabla `modulerole`
--
ALTER TABLE `modulerole`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=482;

--
-- AUTO_INCREMENT de la tabla `ordenbuyclient`
--
ALTER TABLE `ordenbuyclient`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `ordenbuyprovider`
--
ALTER TABLE `ordenbuyprovider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `percentscodes`
--
ALTER TABLE `percentscodes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `person`
--
ALTER TABLE `person`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT de la tabla `personcreatequotation`
--
ALTER TABLE `personcreatequotation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `personcreatequotationmainteince`
--
ALTER TABLE `personcreatequotationmainteince`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `photodiagnosis`
--
ALTER TABLE `photodiagnosis`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `placeproduct`
--
ALTER TABLE `placeproduct`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `pricescompany`
--
ALTER TABLE `pricescompany`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT de la tabla `pricesmainteince`
--
ALTER TABLE `pricesmainteince`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `quotation`
--
ALTER TABLE `quotation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `quotationmainteince`
--
ALTER TABLE `quotationmainteince`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `role`
--
ALTER TABLE `role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `rolecompany`
--
ALTER TABLE `rolecompany`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `serial`
--
ALTER TABLE `serial`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tracingquotation`
--
ALTER TABLE `tracingquotation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `usersquotation`
--
ALTER TABLE `usersquotation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `usersquotationmainteince`
--
ALTER TABLE `usersquotationmainteince`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `viatics`
--
ALTER TABLE `viatics`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `viaticspropety`
--
ALTER TABLE `viaticspropety`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `visits`
--
ALTER TABLE `visits`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `visitsdescriptions`
--
ALTER TABLE `visitsdescriptions`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
